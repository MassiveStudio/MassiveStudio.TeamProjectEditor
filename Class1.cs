﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MassiveStudio.ChunkSystem
{
    public class Chunk
    {
        public Vector3D pos;

    }

    public struct Vector3D
    {
        double x;
        double y;
        double z;

        public Vector3D(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public static Vector3D operator +(Vector3D a, Vector3D b)
        {
            a.x += b.x;
            a.y += b.y;
            a.z += b.z;
            return a;
        }

        public static Vector3D operator -(Vector3D a, Vector3D b)
        {
            a.x -= b.x;
            a.y -= b.y;
            a.z -= b.z;
            return a;
        }

        public static Vector3D operator /(Vector3D a, double b)
        {
            a.x /= b;
            a.y /= b;
            a.z /= b;
            return a;
        }

        public static Vector3D operator *(Vector3D a, double b)
        {
            a.x *= b;
            a.y *= b;
            a.z *= b;
            return a;
        }

        public static Vector3D operator *(double b, Vector3D a)
        {
            a.x *= b;
            a.y *= b;
            a.z *= b;
            return a;
        }
    }
}