﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace MassiveStudio.TeamProjectEditor
{
    [ExecuteInEditMode]
    public class TeamView : MonoBehaviour
    {
        public Bounds bound;
        public bool isnew = false;//Set to true after getting ServerID
        public bool update = false;
        public bool transformupdate = false;
        public bool gameobjectupdate = false;

        public ObjectIdentify id = new ObjectIdentify();

        //var pos : Vector3;
        public List<TeamObject> childObjs = new List<TeamObject>();
        public List<Component> updateComponent = new List<Component>();

        public Transform obj;

        protected int _instance_id;
        public int instance_id
        {
            get
            {
                return _instance_id;
            }
            private set
            {
                _instance_id = value;
            }
        }

        public bool destroyflag = false;

        public void Start()
        {
            instance_id = (int)ReflectDllMethods.GetField(typeof(UnityEngine.Object), this, "m_InstanceID");
            obj = this.transform;
            GetBounds(this.gameObject);
            SetIDs();
        }

        public void Update()
        {
            if (transform.hasChanged && !destroyflag)
            {
                transform.hasChanged = false;
                if (TeamProjectEditor.TeamSettings.isDebug)
                    Debug.Log("CHANGED");
                SetIDs();
                transformupdate = true;
                GetBounds(this.gameObject);
            }
            else if (destroyflag)
            {
                DestroyImmediate(this);
            }
        }

        public void SetIDs()
        {
            int c = 0;
            foreach (Transform a in this.transform)
            {
                if (TeamProjectEditor.TeamSettings.isDebug)
                    Debug.Log("NAME: " + a.gameObject.name);
                if (a != null)
                {
                    if (a.GetComponent<TeamObject>() == null)
                        a.gameObject.AddComponent<TeamObject>().UpdateName("" + c);
                    else
                        a.gameObject.GetComponent<TeamObject>().UpdateName("" + c);

                    if (a.GetComponent<TeamView>())   //Only the root object
                        Destroy(a.GetComponent<TeamView>());
                    //a.SendMessage("UpdateName", ""+c);
                    c++;
                }
            }
            /*foreach (RectTransform a in this.transform)
            {
                if (a != null)
                {
                    if (a.GetComponent<TeamObject>() == null)
                        a.gameObject.AddComponent<TeamObject>().UpdateName("" + c);

                    if (a.GetComponent<TeamView>() != null)   //Only the root object
                        Destroy(a.GetComponent<TeamView>());
                    //a.SendMessage("UpdateName", ""+c);
                    c++;
                }
            }*/
            CheckChilds();
        }

        public void UpdateBounds()
        {
            GetBounds(this.gameObject);
        }

        public void CheckChilds()
        {
            childObjs.Clear();
            //var objs: TeamObject[]=GetComponentsInChildren(TeamObject) as TeamObject[];
            //if(objs!=null && objs.Length>0)
            //	childObjs.AddRange(objs);

            foreach (Transform a in this.transform)
            {
                TeamObject t = a.GetComponent<TeamObject>();
                if (t == null)
                {
                    SetIDs();
                }
                else
                {
                    if (TeamProjectEditor.TeamSettings.isDebug)
                        Debug.LogWarning("CHILD: " + a.gameObject.name);
                    t.id.ServerID = id.ServerID;
                    childObjs.AddRange(t.CheckChilds());
                }
            }
            if (TeamProjectEditor.TeamSettings.isDebug)
                Debug.Log("CHILD COUNT " + childObjs.Count);
        }

        public void GetBounds(GameObject obj)
        {
            if (obj.GetComponentInChildren(typeof(Collider)) != null)
            {
                var totalBounds = obj.GetComponentInChildren<Collider>().bounds;
                var colliders = obj.GetComponentsInChildren<Collider>();
                foreach (Collider col in colliders)
                    totalBounds.Encapsulate(col.bounds);
                this.bound = totalBounds;
            }
            // return null;
        }
    }
}
