﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace MassiveStudio.TeamProjectEditor
{
    public class TeamObject : MonoBehaviour 
    {
        public bool isnew =true;//ContainerModel
        public bool update =false;//ComponentUpdate
        public bool transformupdate =false;//TransformUpdate
        public bool gameobjectupdate =false;//GameObjectUpdate

        public ObjectIdentify id;

        public List<Component> updateComponent;

        void Start()
        {
            id = new ObjectIdentify();
            updateComponent = new List<Component>();
            //To Add at all Childs the TeamObject Script
            this.transform.root.GetComponent<TeamView>().CheckChilds();
        }

        public void Update()
        {
            if (transform.hasChanged)
            {
                transformupdate = true;
                update = true;
                transform.hasChanged = false;
                UpdateName(id.ObjectID);
            }
        }

        void OnDestroy()
        {
            updateComponent.Clear();
        }
        /*
        void OnComponentEdit()
        {//Recive from new Inspector
            update = true;
        }
        */
        public List<TeamObject> CheckChilds()
        {
            List<TeamObject> list = new List<TeamObject>();
            list.Add(this);
            foreach (Transform a in this.transform)
            {
                TeamObject t = a.GetComponent<TeamObject>();
                if (t == null)
                {
                    TeamView tt = this.transform.root.GetComponent<TeamView>();
                    if (tt != null)
                        tt.Update();
                    else
                        break;
                }
                else
                {
                    Debug.LogWarning("CHILD: " + a.gameObject.name);
                    t.id.ServerID = id.ServerID;
                    list.AddRange(t.CheckChilds());
                }

            }
            /*foreach (TeamObject t in this.transform.GetComponentsInChildren<TeamObject>())
            {
                t.id.ServerID = id.ServerID;
                list.AddRange(t.CheckChilds());
            }*/
            return list;
        }

        public void UpdateName(string r)
        {
            this.id.ObjectID = r;
            int c = 0;
            foreach (Transform a in this.transform)
            {
                if (a != null)
                {
                    if (a.GetComponent<TeamObject>() == null)
                        a.gameObject.AddComponent<TeamObject>().UpdateName("" + c);
                    c++;
                }
            }
        }
    }
}