﻿using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Security;
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Unity2gether")]
[assembly: AssemblyDescription("Realtime TeamEditor Extension")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("MassiveStudio")]
[assembly: AssemblyProduct("TeamProjectEditor")]
[assembly: AssemblyCopyright("Copyright © MassiveStudio 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("d570c171-6e57-4785-8fc8-62f9b4ead695")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

[assembly: InternalsVisibleTo("MassiveStudio.TeamProjectEditor.Tools")]
[assembly: InternalsVisibleTo("MassiveStudio.TeamScripting")]
[assembly: InternalsVisibleTo("MassiveStudio.TeamProjectEditor.Server")]
[assembly: InternalsVisibleTo("MassiveStudio.Serializer")]
[assembly: InternalsVisibleTo("MassiveStudio.TeamTileMapPainter")]

[assembly: AssemblyFlags(AssemblyNameFlags.EnableJITcompileOptimizer)]
[assembly: ObfuscateAssembly(true)]
[module: UnverifiableCode]