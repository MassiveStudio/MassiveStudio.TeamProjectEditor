﻿using System;
using UnityEditor;
using UnityEngine;
using MassiveStudio.TeamProjectEditor.Utility;

namespace MassiveStudio.TeamProjectEditor
{
    public static class Preferences
    {
        public static EditorPrefsBool aktivate_LogSaver = new EditorPrefsBool("Console", "Activate LogSaver", false);
        public static EditorPrefsBool aktivate_FastMenu = new EditorPrefsBool("FastMenu", "Activate FastMenu", false);

        [UnityEditor.PreferenceItem("TeamEditor")]
        public static void EditorPreferences()
        {
            //EditorGUILayout.HelpBox(HOME_FOLDER_HINT, MessageType.Info);
            EditorGUILayout.Separator();
            aktivate_FastMenu.Draw();
            aktivate_LogSaver.Draw();
            GUILayout.FlexibleSpace();
            EditorGUILayout.LabelField("Version " + "1.0.1f1", EditorStyles.centeredGreyMiniLabel);
        }

        public abstract class EditorPrefsItem<T>
        {
            public string Key;
            public string Label;
            public T DefaultValue;

            protected EditorPrefsItem(string key, string label, T defaultValue)
            {
                if (string.IsNullOrEmpty(key))
                {
                    throw new ArgumentNullException("key");
                }

                Key = key;
                Label = label;
                DefaultValue = defaultValue;
            }

            public abstract T Value { get; set; }
            public abstract void Draw();

            public static implicit operator T(EditorPrefsItem<T> s)
            {
                return s.Value;
            }
        }

        public class EditorPrefsString : EditorPrefsItem<string>
        {
            public EditorPrefsString(string key, string label, string defaultValue)
                : base(key, label, defaultValue)
            {
            }

            public override string Value
            {
                get { return EditorPrefs.GetString(Key, DefaultValue); }
                set { EditorPrefs.SetString(Key, value); }
            }

            public override void Draw()
            {
                EditorGUIUtility.labelWidth = 100f;
                Value = EditorGUILayout.TextField(Label, Value);
            }
        }

        public class EditorPrefsBool : EditorPrefsItem<bool>
        {
            public EditorPrefsBool(string key, string label, bool defaultValue)
                : base(key, label, defaultValue)
            {
            }

            public override bool Value
            {
                
                get
                {
                    bool ret=false;
                    //Dispatcher.ToMainThread(() =>
                    //{
                        ret = EditorPrefs.GetBool(Key, DefaultValue);
                    //});
                    return ret;
                }
                set
                {
                    //Dispatcher.ToMainThread(() =>
                    //{
                        EditorPrefs.SetBool(Key, value);
                    //});
                }

            }

            public override void Draw()
            {
                EditorGUIUtility.labelWidth = 200f;
                Value = EditorGUILayout.Toggle(Label, Value);
            }
        }
    }

    public class UPreferenceItem : Attribute
    {
        public UPreferenceItem()
        {

        }
    }
}
