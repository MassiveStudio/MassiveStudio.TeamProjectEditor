﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor.PropertyDrawers
{
    [CustomPropertyDrawer (typeof(ObjectComponent))]
    public class PropertyDrawer_ObjectComponent : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Using BeginProperty / EndProperty on the parent property means that
            // prefab override logic works on the entire property.
            EditorGUI.BeginProperty(position, label, property);
            
            // Draw label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            
            // Don't make child fields be indented
            int indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            // Calculate rects
            Rect Name = new Rect(position.x, position.y, 100, position.height);
            Rect Type = new Rect(position.x + 100, position.y, 100, position.height);
            Rect Id = new Rect(position.x + 200, position.y, 50, position.height);
            Rect State = new Rect(position.x + 250 + position.height, position.y, position.height, position.height);
            Rect isMono = new Rect(position.x + 250 + position.height * 2, position.y, position.height, position.height);

            EditorGUI.PropertyField(Name, property.FindPropertyRelative("componentName"), GUIContent.none);
            EditorGUI.PropertyField(Type, property.FindPropertyRelative("comtype"), GUIContent.none);
            EditorGUI.PropertyField(Id, property.FindPropertyRelative("id"), GUIContent.none);

            EditorGUI.PropertyField(State, property.FindPropertyRelative("state"), GUIContent.none);
            EditorGUI.PropertyField(isMono, property.FindPropertyRelative("isMono"), GUIContent.none);

            // Set indent back to what it was
            EditorGUI.indentLevel = indent;
            
            EditorGUI.EndProperty();
        }
    }

    [CustomPropertyDrawer(typeof(ObjectType))]
    public class PropertyDrawer_ObjectType : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Using BeginProperty / EndProperty on the parent property means that
            // prefab override logic works on the entire property.
            EditorGUI.BeginProperty(position, label, property);

            // Draw label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            // Don't make child fields be indented
            int indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;
            // Calculate rects
            Rect Type = new Rect(position.x, position.y, 100, position.height);
            EditorGUI.PropertyField(Type, property.FindPropertyRelative("_type"), GUIContent.none);
            // Set indent back to what it was
            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }
    }
}
