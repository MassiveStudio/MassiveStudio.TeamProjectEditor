﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MassiveStudio.TeamProjectEditor
{
    [System.Serializable]
    public class SceneCheckList
    {
        public int SceneID;
        public List<SceneCheckListItem> sceneObjects;

        public SceneCheckList(int sceneID)
        {
            this.SceneID = sceneID;
            sceneObjects = new List<SceneCheckListItem>();
        }
    }

    [System.Serializable]
    public class SceneCheckListItem
    {
        public string SceneServerIDs;
        public long synctimes;

        public SceneCheckListItem(ModelContainer mc)
        {
            this.SceneServerIDs = mc.serverid;
            this.synctimes = mc.syncTime;
        }
    }
}
