﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MassiveStudio.TeamProjectEditor
{
    [System.Serializable]
    public class SceneAssetUpdate
    {
        public ModelContainer asset;//Contains last SyncTime
        public int sceneID;
        public string serverID;

        public SceneAssetUpdate(ModelContainer _asset, int _sceneID, string _serverID)
        {
            this.asset = _asset;
            this.sceneID = _sceneID;
            this.sceneID = _sceneID;
        }
    }
}
