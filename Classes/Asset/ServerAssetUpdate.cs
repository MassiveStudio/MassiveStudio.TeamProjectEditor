﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MassiveStudio.TeamProjectEditor
{
    [System.Serializable]
    public class ServerAssetUpdate
    {
        public ShareAssetType assettype;
        public object asset;
        public long synctime;
        public int id;

        public ServerAssetUpdate(ShareAssetType t, object a, long st, int _id)
        {
            this.assettype = t;
            this.asset = a;
            this.synctime = st;
            this.id = _id;
        }
    }
}
