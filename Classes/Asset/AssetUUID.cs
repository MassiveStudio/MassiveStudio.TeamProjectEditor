﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using System.IO;

using MassiveStudio.TeamProjectEditor.Attributes;

namespace MassiveStudio.TeamProjectEditor
{
    [Serializable]
    public class AssetUUID
    {
        public string UUID ;
        public string Creator;//UserID
        public string ServerID;
        public long SyncTime;//Timestamp
        //public int inid;
        public ShareAssetType type;

        /* public AssetUUID(UnityEngine.Object o)
         {
             //this.inid = o.GetInstanceID();
             this.UUID = GetUUIDFromPath(o);
             this.Creator = TeamServerConnection.currentServer.user.UserID;
             this.ServerID = "";
             SetAssetType(o);
         }*/

        public AssetUUID(UnityEngine.Object o, string serverID = "", bool isDefaultUnityAsset = false)
        {
            if (!isDefaultUnityAsset)
            {
                //this.inid = o.GetInstanceID();
                this.UUID = GetUUIDFromPath(o);
                this.Creator = TeamServerConnection.currentServer.user.UserID;
                this.ServerID = serverID;
            }
            else
            {
                this.UUID = string.Empty;
                this.ServerID = string.Empty;
                this.Creator = string.Empty;
            }
            //Set current Time
            this.SyncTime = TeamSceneManager.GetCurrentTime();
            if(o !=null)
                this.type = TeamAssetSystem.TeamAssetCore.GetAssetType(o);
            //SetAssetType(o);
        }

        public bool HasTeamID()
        {
            if (this.ServerID.Length > 0)
                return true;
            return false;
        }

        /*moved ->TeamAssetCore
        public void SetAssetType(UnityEngine.Object obj)
        {
            System.Type type = obj.GetType();
            if (type == typeof(Material))
                this.type = ShareAssetType.Material;
            else if (type == typeof(Texture2D))
                this.type = ShareAssetType.Texture;
            else if (type == typeof(Shader))
                this.type = ShareAssetType.Shader;
        }*/

        [OptimizedForBackroundMainDispatch]
        public static string GetUUIDFromPath(UnityEngine.Object o)
        {
            string path = "";


            Console.TeamConsole.Message("GET UNITY ASSET PATH", "AssetUUID", Color.yellow, true, false);
            Utility.Dispatcher.ToMainThread(() =>
            {
                //Debug.LogWarning("GET UNITY ASSET PATH");
                path = AssetDatabase.GetAssetPath(o);

                Console.TeamConsole.Message("OBJ NAME: " + o.name + " \t "+ path, "AssetUUID", Color.white, true, false);
            });
            Console.TeamConsole.Message("GetAssetUUID PATH: " + path, "AssetUUID", Color.white, true, false);
            //Debug.Log("GetAssetUUID PATH: " + path);
            if (path == "Resources/unity_builtin_extra")//Unity Standard Assets
            {
                path = "Standard/Standard" + GetDefaultExtension(o);
                return path;
            }
            else if (path == string.Empty)//TempObject
            {
                path = "Assets/ServerLibraryFiles/" + Path.GetRandomFileName() + GetDefaultExtension(o);
                Console.TeamConsole.Message("Save TempAsset - EMPTY", "AssetUUID", Color.yellow, true, false);
                Utility.Dispatcher.ToMainThread(() =>
                {
                    //Debug.LogWarning("Save TempAsset");
                    Console.TeamConsole.Message("Save TempAsset EMPTY PATH", "AssetUUID", Color.cyan, true, false);
                    AssetDatabase.CreateAsset(o, path);
                });
                return path;
            }
            else if (path.StartsWith("Assets/ServerLibraryFiles"))   //ShareAsset from other
            {
                return Path.GetFileName(path);
            }
            else//Asset.Where(x=> x.Creator== me);
            {
                if (File.Exists(path))
                    return TeamAssetSystem.TeamAssetCore.ToHash(path) + "_" + Path.GetFileName(path);// AssetDatabase.AssetPathToGUID(path);
                else
                {
                    Utility.Dispatcher.ToMainThread(() =>
                    {
                        path = "Assets/ServerLibraryFiles/" + o.name + GetDefaultExtension(o);
                        Console.TeamConsole.Message("Save TempAsset", "AssetUUID", Color.yellow, true, false);
                        //Debug.LogWarning("Save TempAsset");
                        AssetDatabase.CreateAsset(o, path);
                    });
                    return TeamAssetSystem.TeamAssetCore.ToHash(path) + "_" + Path.GetFileName(path);// AssetDatabase.AssetPathToGUID(path);
                }
            }
        }

        public new string ToString()//Debug
        {
            return "UUID: "+ UUID + "\t" +"Creator: "+ Creator + "|\t" +"ServerID: "+ ServerID + "|\t" + "SyncTime: " + SyncTime + "|\t" + "Type: " +type.ToString();
        }

        public static string GetDefaultExtension(UnityEngine.Object o)
        {
            Type t = o.GetType();
            if (t == typeof(Material))
                return ".mat";
            else if (t == typeof(Texture2D))
                return ".png";
            else if (t == typeof(Shader))
                return ".shader";
            else
                return ".asset";
        }

        public System.Type GetAssetType()
        {
            switch (this.type)
            {
                case ShareAssetType.Material:
                    return typeof(Material);
                case ShareAssetType.GUISkin:
                    return typeof(GUISkin);
                case ShareAssetType.Script:
                    return typeof(MonoScript);
                case ShareAssetType.Texture:
                    return typeof(Texture2D);
                default:
                    return null;
            }
        }
    }
}
