﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

namespace MassiveStudio.TeamProjectEditor
{
    [System.Serializable]
    public class ShareAsset
    {
        public AssetUUID uuid;
        public string localpath;

        [NonSerialized]
        private object _obj;// UnityEngine.Object _obj;

        [NonSerialized]
        private int _InstanceID=-1;
        public int InstanceID
        {
            get
            {
                if (_InstanceID == -1)
                    if (HasObject())
                        _InstanceID = ReflectDllMethods.GetInstanceID((UnityEngine.Object)_obj);
                return _InstanceID;
            }
            set
            {
                _InstanceID = value;
            }
        }

        [NonSerialized]
        public bool loaded = false;//if is Default (not downloaded) and changes happened -> no Update

        [NonSerialized]
        public static readonly ShareAsset empty = new ShareAsset(null, new AssetUUID(null, "", true));

        public UnityEngine.Object obj
        {
            get
            {
                if (_obj == null)
                    LoadObject();
                return (UnityEngine.Object)_obj;
            }
            set
            {
                _obj = value;
            }
        }

        public bool HasObject()
        {
            if (_obj == null)
                LoadObject();
            return _obj==null?false:true;
        }

        public ShareAsset(UnityEngine.Object Object, AssetUUID Assetuuid, string localPath = "")
        {
            this.uuid = Assetuuid;
            this.obj = Object;
            this.localpath = localPath;
        }

        internal void LoadObject()
        {
            if (System.IO.File.Exists(localpath))
            {
                Utility.Dispatcher.ToMainThread(() =>
                {
                    this.obj = AssetDatabase.LoadAssetAtPath(localpath, GetAssetType());

                    if (this.obj != null)
                        this.InstanceID = this.obj.GetInstanceID();
                });
            }
        }

        internal bool HasToBeRegisteredOnline()
        {
            if (!localpath.StartsWith("Assets/ServerLibraryFiles"))
                return true;
            else
                return false;
        }

        private System.Type GetAssetType()
        {
            switch (uuid.type)
            {
                case ShareAssetType.Material:
                    return typeof(Material);
                case ShareAssetType.GUISkin:
                    return typeof(GUISkin);
                case ShareAssetType.Script:
                    return typeof(MonoScript);
                case ShareAssetType.Shader:
                    return typeof(Shader);
                default:
                    return null;
            }
        }
    }
}
