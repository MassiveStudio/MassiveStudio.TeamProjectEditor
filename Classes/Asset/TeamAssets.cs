﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
namespace MassiveStudio.TeamProjectEditor
{
    [System.Serializable]
    public class TeamAssets
    {
        public List<ShareAsset> Materials;
        public List<ShareAsset> Textures;
        public List<ShareAsset> Scripts;
        public List<ShareAsset> Shaders;

        public const ushort listCount=4;

        public TeamAssets()
        {
            Materials = new List<ShareAsset>();
            Textures = new List<ShareAsset>();
            Scripts = new List<ShareAsset>();
            Shaders = new List<ShareAsset>();
        }

        public List<ShareAsset> GetList(UnityEngine.Object obj)
        {
            ShareAssetType _type;
            System.Type type = obj.GetType();
            if (type == typeof(Material))
                _type = ShareAssetType.Material;
            else if (type == typeof(Texture) || type == typeof(Texture2D))
                _type = ShareAssetType.Texture;
            else if (type == typeof(MonoBehaviour))
                _type = ShareAssetType.Script;
            else if (type == typeof(Shader))
                _type = ShareAssetType.Shader;
            else
            {
                throw new System.ArgumentNullException("LIST FOR OBJ TYPE " + obj.GetType().ToString() + " not found");
                return null;
            }

            return GetList(_type);
        }

        public List<ShareAsset> GetList(ShareAssetType type)
        {
            switch (type)
            {
                case ShareAssetType.Texture:
                    return Textures;
                case ShareAssetType.Material:
                    return Materials;
                case ShareAssetType.Script:
                    return Scripts;
                case ShareAssetType.Shader:
                    return Shaders;
            }
            throw new System.ArgumentNullException("LIST TYPE " + type.ToString() + " not found");
            return null;
        }

        public void Add(UnityEngine.Object obj, ShareAsset tempasset)
        {
            tempasset.obj = obj;
            switch (tempasset.uuid.type)
            {
                case ShareAssetType.Texture:
                    Textures.Add(tempasset);
                    break;
                case ShareAssetType.Material:
                    Materials.Add(tempasset);
                    break;
                case ShareAssetType.Script:
                    Scripts.Add(tempasset);
                    break;
                case ShareAssetType.Shader:
                    Shaders.Add(tempasset);
                    break;
            }
        }

        public void Clear()
        {
            Materials.Clear();
            Textures.Clear();
            Scripts.Clear();
            Shaders.Clear();
        }

        public int ShareAssetCount
        {
            get
            {
                return Materials.Count + Textures.Count + Scripts.Count + Shaders.Count;
            }
        }

    }
}