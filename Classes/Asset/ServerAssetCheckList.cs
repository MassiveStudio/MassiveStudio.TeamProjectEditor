﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor
{
    [Serializable]
    public class ServerAssetCheckList//List of downloaded Assets with their last sync time
    {
        //public List<string> assetIDs;// = new List<string>();
        //public List<ShareAssetType> asssettype;
        //public List<long> synctimes;// = new List<long>();
        public List<ServerAssetCheckListElement> assetlists;

        public int SceneID;

        public ServerAssetCheckList(int sceneid)
        {
            this.SceneID = sceneid;
            assetlists = new List<ServerAssetCheckListElement>();
        }
    
        internal void AddAsset(AssetUUID uuid)
        {
            byte t = (byte)uuid.type;

            if (assetlists.Count > t)
                assetlists.Capacity = (t + 1);

            assetlists[t].AddAsset(uuid);
        }

        internal void Sort()
        {
            for (int i = 0; i < assetlists.Count; i++)
                assetlists[i].assets.OrderBy(x=> x.assetIDs);
        }
    }

    [Serializable]
    public class ServerAssetCheckListElement
    {
        public List<ServerAssetCheckListItem> assets;// = new List<string>();

        public ShareAssetType asssetlisttype;

        public ServerAssetCheckListElement(ShareAssetType type)
        {
            assets = new List<ServerAssetCheckListItem>();
            this.asssetlisttype = type;
        }

        internal void AddAsset(AssetUUID uuid)
        {
            //Sort ALGORYTHM
            /*int id = int.Parse(uuid.ServerID);
            if (assets.Count>1)
            {
                int s=0;
                int sid = assets[s].assetIDs;
                int e = assets.Count;
                int eid = assets[e].assetIDs;

                Sort(s, sid, e, eid, id, uuid);
            }
            */
            this.assets.Add(new ServerAssetCheckListItem(uuid));
        }

        //LATER MAYBE
        /*
        internal void Sort(int s, int sid, int e, int eid, int id, AssetUUID uuid)
        {
            if (id < sid)
                assets.Insert(sid, new ServerAssetCheckListItem(uuid));
            if (id < eid)
            {
                if (e - s > 1)
                {
                    Sort(id, uuid);
                }
            }
            else
                assets.Insert(e, new ServerAssetCheckListItem(uuid));
        }*/
    }

    [Serializable]
    public class ServerAssetCheckListItem
    {
        public int assetIDs;// = new List<string>();
        public long synctimes;// = new List<long>();

        public ServerAssetCheckListItem(AssetUUID uuid)
        {
            Debug.Log("ASSETCHECKLIST ITEM " + uuid.ToString());
            if(uuid.ServerID!=string.Empty)
                this.assetIDs=int.Parse(uuid.ServerID);
            this.synctimes=uuid.SyncTime;
        }
    }
}