﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace MassiveStudio.TeamProjectEditor
{
    [Serializable]
    public class MaterialData
    {
        public string name;
        public AssetUUID mtUUID; //UUID [Server/local]
        public AssetUUID shader;

        public string materialtag;
        public Dictionary<string, object> shaderdata;
    }
}
