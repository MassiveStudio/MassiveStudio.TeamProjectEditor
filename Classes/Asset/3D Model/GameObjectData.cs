﻿using System;

namespace MassiveStudio.TeamProjectEditor
{
    [Serializable]
    public class GameObjectData : System.Object
    {
        public string name;
        public bool state =true;
        public int layer;//Default
        public string tag;
        public bool isstatic;

        public GameObjectData(){}

    }
}
