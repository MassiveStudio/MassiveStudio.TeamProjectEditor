﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor
{
    [Serializable]
    public class MeshData
    {
        public List<Vector3> ver;
        public List<int> tri;
        public List<Vector2> uvs;
        public List<Vector2> uv2;


        public MeshData()
        {
            ver = new List<Vector3>();
            tri = new List<int>();
            uv2 = new List<Vector2>();
            uvs = new List<Vector2>();
        }

        public Mesh ToMesh()
        {
            Mesh mesh = null;
            Utility.Dispatcher.ToMainThread(() =>
            {
                mesh = new Mesh();
                mesh.subMeshCount = 2;
                mesh.SetVertices(ver);
                mesh.uv = uvs.ToArray();
                mesh.uv2 = uv2.ToArray();
                mesh.SetTriangles(tri.ToArray(), 0);
                mesh.RecalculateNormals();
                mesh.RecalculateBounds();
                //mesh.Optimize();
            });
            return mesh;
        }

        public void ToMeshData(MeshFilter mesh)
        {
            Utility.Dispatcher.ToMainThread(() =>
            {
                this.ver.AddRange(mesh.sharedMesh.vertices);
                this.tri.AddRange(mesh.sharedMesh.triangles);
                this.uvs.AddRange(mesh.sharedMesh.uv);
                this.uv2.AddRange(mesh.sharedMesh.uv2);
            });
        }
    }
}