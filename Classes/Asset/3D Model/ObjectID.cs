﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace MassiveStudio.TeamProjectEditor
{
    [Serializable]
    public class ObjectID : System.Object//OBSOLED
    {
        public GameObject obj;
        public ObjectIdentify id;
        public ObjectID(GameObject o, ObjectIdentify i)
        {
                this.obj = o;
                this.id = i;
            }
        }
}
