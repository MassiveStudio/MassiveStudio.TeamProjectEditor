﻿using UnityEngine;

namespace MassiveStudio.TeamProjectEditor
{
    [System.Serializable]
    public class TransformData
    {
        public Vector3 pos;
        public Vector3 rot;
        public Vector3 scal;
        public string parent; //TEAMVIEW / TEAMOBJECT . OBJECTID

        public TransformData(){ }

        public void ToTransform(Transform t)
        {
            t.position = this.pos;
            t.rotation = Quaternion.Euler(this.rot);
            t.localScale = this.scal;
        }
    }
}
