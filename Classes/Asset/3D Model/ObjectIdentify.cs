﻿namespace MassiveStudio.TeamProjectEditor
{
    [System.Serializable]
    public class ObjectIdentify
    {
        public string ObjectID;
        public string ServerID;
        public ObjectIdentify() {}

        public ObjectIdentify (string _ObjectID, string _ServerID)
        {
            this.ObjectID = _ObjectID;
            this.ServerID = _ServerID;
        }

        public new string ToString()
        {
            return ObjectID + " : " + ServerID;
        }
    }
}
