﻿using System;
using System.Collections.Generic;

namespace MassiveStudio.TeamProjectEditor
{
    [Serializable]
    public class ModelContainer
    {
        public List<Model3d> models;
        public string userid;
        public string serverid;
        public long syncTime;//For SceneAssetCheck OnLoadScene
                             //public ObjectIdentify id; //'cause Model3d has id proberty too (for each obj);

        public ModelContainer()
        {
            models = new List<Model3d>();
            syncTime = DateTime.Now.Ticks;
            userid = "";
            serverid = "";
        }
    }
}
