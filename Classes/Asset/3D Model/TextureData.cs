﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using System.IO;
using MassiveStudio.TeamProjectEditor.TeamAssetSystem;

namespace MassiveStudio.TeamProjectEditor
{
    [Serializable]
    public class TextureData
    {
        public AssetUUID uuid;
        //public string TextureID = ""; ->UUID
        //public string TextureName = ""; ->UUID
        public byte[] data;


        public TextureData()
        {

        }

        [Obsolete("USELESS FOR CURRENT TIME")]
        public TextureData(Texture2D tex)
        {
            string path = AssetDatabase.GetAssetPath(tex);
            //TextureName = Path.GetFileName(path);
            //TextureID = AssetUUID.GetUUIDFromPath(tex);
                //TeamServerConnection.currentServer.user.UserID + ":" + AssetDatabase.AssetPathToGUID(path);
            data = File.ReadAllBytes(path);
        }

        [Obsolete()]
        public void ToTexture()
        {
            //File.WriteAllBytes("Assets/Server/Textures/" + TextureName, data);
        }

        [Obsolete("Use TeamAssetCore.GetAssetUUID instead")]
        public static string GetTextureUUID(Texture tex)
        {
            return TeamServerConnection.currentServer.user.UserID + ":" + AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(tex));
        }

    }
}

