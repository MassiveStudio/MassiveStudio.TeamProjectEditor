﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MassiveStudio.TeamProjectEditor
{
    [System.Serializable]
    public class ShaderData
    {
        public string name;
        public AssetUUID uuid;
        public string data;
    }
}
