﻿using System.Collections.Generic;
using System;
namespace MassiveStudio.TeamProjectEditor
{
    [Serializable]
    public class ObjectComponent
    {
        public string componentName;
        public Dictionary<string, object> fields;
        public object componentData;
        public ObjectType comtype;
        public bool state;
        public bool isMono;
        public int id;//Pos in Inspector - cause if multiple excists, the first on will be overriden
        public ObjectComponent()
        {
            fields = new Dictionary<string, object>();
        }
    }
}