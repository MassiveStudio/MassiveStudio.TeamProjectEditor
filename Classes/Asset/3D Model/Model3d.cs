﻿using System.Collections.Generic;

namespace MassiveStudio.TeamProjectEditor
{
    [System.Serializable]
    public class Model3d
    {
        public MeshData geo;
        public TransformData tran;
        public GameObjectData game;
        public ObjectIdentify id;
        public List<ObjectComponent> comp;
  
        public Model3d()
        {
            comp = new List<ObjectComponent>();
        }
    }
}
