﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEditor.VersionControl;
using UnityEditorInternal;
using UnityEditorInternal.VersionControl;
using UnityEngine;
using UnityEditor;
using MassiveStudio.TeamProjectEditor.Utility;
using MassiveStudio.TeamProjectEditor.EditorWindows.Inspector;
using UnityEngine.Profiling; //v5.5
using MassiveStudio.TeamProjectEditor.Utility;

namespace MassiveStudio.TeamProjectEditor.Basic_System_Classes
{
    internal class TeamInspector : EditorWindow, IHasCustomMenu
    {
        internal class Styles
        {
            public readonly GUIStyle preToolbar = "preToolbar";
            public readonly GUIStyle preToolbar2 = "preToolbar2";
            public readonly GUIStyle preDropDown = "preDropDown";
            public readonly GUIStyle dragHandle = "RL DragHandle";
            public readonly GUIStyle lockButton = "IN LockButton";
            public readonly GUIContent preTitle = new GUIContent("Preview");
            public readonly GUIContent labelTitle = new GUIContent("Asset Labels");
            public readonly GUIContent addComponentLabel = new GUIContent("Add Component");

            //Internal EditorGUIStyles Fields
            public readonly GUIStyle inspectorTitlebar;

            public GUIStyle preBackground = "preBackground";
            public GUIStyle addComponentArea = EditorStyles.inspectorFullWidthMargins;
            public GUIStyle addComponentButtonStyle = "LargeButton";
            public GUIStyle previewMiniLabel = new GUIStyle(EditorStyles.whiteMiniLabel);
            public GUIStyle typeSelection = new GUIStyle("PR Label");
            public GUIStyle lockedHeaderButton = "preButton";
            public GUIStyle stickyNote = new GUIStyle("VCS_StickyNote");
            public GUIStyle stickyNoteArrow = new GUIStyle("VCS_StickyNoteArrow");
            public GUIStyle stickyNotePerforce = new GUIStyle("VCS_StickyNoteP4");
            public GUIStyle stickyNoteLabel = new GUIStyle("VCS_StickyNoteLabel");

            public Styles()
            {
                this.typeSelection.padding.left = 12;

                //InitStyle()
                EditorStyles current = null;
                current = (EditorStyles)ReflectDllMethods.GetField(typeof(UnityEditor.EditorStyles), null, "s_Current");//EditorStyles.inspectorTitlebar.normal.background
                inspectorTitlebar = (GUIStyle)ReflectDllMethods.GetField(typeof(UnityEditor.EditorStyles), current, "m_InspectorTitlebar");//EditorStyles.inspectorTitlebar.normal.background
            }
        }
        private const float kBottomToolbarHeight = 17f;
        internal const int kInspectorPaddingLeft = 14;
        internal const int kInspectorPaddingRight = 4;
        private const long delayRepaintWhilePlayingAnimation = 150L;
        public Vector2 m_ScrollPosition;
        public InspectorMode m_InspectorMode;
        private static readonly List<TeamInspector> m_AllInspectors = new List<TeamInspector>();
        private static bool s_AllOptimizedGUIBlocksNeedsRebuild;
        private long s_LastUpdateWhilePlayingAnimation;
        private bool m_ResetKeyboardControl;
        protected ActiveEditorTracker m_Tracker;
        private Editor m_LastInteractedEditor;
        //private bool m_IsOpenForEdit;
        private static TeamInspector.Styles s_Styles;
        private double m_lastRenderedTime;
        //private bool m_InvalidateGUIBlockCache = true;
        public static TeamInspector s_CurrentTeamInspector;

        public static Dictionary<string, bool> winshows = new Dictionary<string, bool>();

        public static int temp_count = 0;

        public static GameObject selectedObj;
        public static bool teamflag;
        private Transform selectedObjParent;


        public ActiveEditorTracker tracker
        {
            get
            {
                this.CreateTracker();
                return this.m_Tracker;
            }
        }
        bool isInspectorDirty;
        internal static Color kPlayModeDarken = new Color(0.8f, 0.8f, 0.8f, 1f);

        internal static TeamInspector.Styles styles
        {
            get
            {
                TeamInspector.Styles arg_17_0;
                if ((arg_17_0 = TeamInspector.s_Styles) == null)
                {
                    arg_17_0 = (TeamInspector.s_Styles = new TeamInspector.Styles());
                }
                return arg_17_0;
            }
        }
        public bool isLocked
        {
            get
            {
                this.CreateTracker();
                return this.m_Tracker.isLocked;
            }
            set
            {
                this.CreateTracker();
                this.m_Tracker.isLocked = value;
            }
        }
        private void Awake()
        {
            if (!TeamInspector.m_AllInspectors.Contains(this))
            {
                TeamInspector.m_AllInspectors.Add(this);
            }
        }
        private void OnDestroy()
        {
            if (this.m_Tracker != null && !this.m_Tracker.Equals(ActiveEditorTracker.sharedTracker))
            {
                this.m_Tracker.Destroy();
            }

            SceneView.onSceneGUIDelegate -= OnSceneGUI;
        }
        protected virtual void OnEnable()
        {
            this.RefreshTitle();
            base.minSize = new Vector2(275f, 50f);
            if (!TeamInspector.m_AllInspectors.Contains(this))
            {
                TeamInspector.m_AllInspectors.Add(this);
            }
        }
        protected virtual void OnDisable()
        {
            TeamInspector.m_AllInspectors.Remove(this);
        }
        internal static void RepaintAllInspectors()
        {
            foreach (TeamInspector current in TeamInspector.m_AllInspectors)
            {
                current.Repaint();
            }
        }
        internal static List<TeamInspector> GetInspectors()
        {
            return TeamInspector.m_AllInspectors;
        }
        private void OnSelectionChange()
        {
            base.Repaint();

            selectedObj = Selection.activeGameObject;
            if (selectedObj != null)
                teamflag = selectedObj.transform.root.GetComponent(typeof(TeamView)) != null ? true : selectedObj.transform.root.GetComponent(typeof(TeamObject)) ? true : false;
            else
                teamflag = false;


            if (selectedObj != null)
            {
                SceneView.onSceneGUIDelegate -= OnSceneGUI;
                SceneView.onSceneGUIDelegate += OnSceneGUI;
            }

        }
        void OnSceneGUI(SceneView sceneView)
        {
            if (selectedObj != null)
            {
                if (selectedObj.transform.hasChanged || selectedObj.transform.parent != selectedObjParent)
                {
                    if (teamflag)
                    {
                        selectedObjParent = selectedObj.transform.parent;
                        if (selectedObj.GetComponent<TeamView>() != null)
                            selectedObj.transform.GetComponent<TeamView>().Update();
                        else if (selectedObj.GetComponent<TeamObject>() != null)
                            selectedObj.transform.GetComponent<TeamObject>().Update();
                        else
                            selectedObj.transform.root.GetComponent<TeamView>().SetIDs();

                        TeamSceneManager.TeamObject_isDirty = true;
                    }
                }
            }
        }
        public static TeamInspector[] GetAllTeamInspectors()
        {
            return TeamInspector.m_AllInspectors.ToArray();
        }
        private void OnInspectorUpdate()
        {
            if (this.m_Tracker != null)
            {
                this.m_Tracker.VerifyModifiedMonoBehaviours();
                if (!this.m_Tracker.isDirty || !this.ReadyToRepaint())
                {
                    return;
                }
            }
            base.Repaint();


            if (Selection.gameObjects.Length > 0)
            {
                if (selectedObj != Selection.activeGameObject)
                {
                    selectedObj = Selection.activeGameObject;

                    if (selectedObjParent != selectedObj.transform.parent)
                    {
                        // teamflag = selectedObj.transform.root.GetComponent(typeof(TeamView)) != null ? true : selectedObj.transform.root.GetComponent(typeof(TeamObject)) ? true : false;
                        if (selectedObj.transform.root.GetComponent<TeamView>() != null)
                        {
                            selectedObj.transform.root.GetComponent<TeamView>().Update();
                            teamflag = true;
                        }
                        else if (selectedObj.transform.root.GetComponent<TeamObject>() != null)
                        {
                            selectedObj.transform.root.GetComponent<TeamObject>().Update();
                            teamflag = true;
                        }
                        else
                            teamflag = false;
                    }
                    selectedObjParent = selectedObj.transform.parent;

                    //custominspector = new Editor[comps.Length];

                }
            }
            else
                selectedObj = null;

            if (selectedObj != null)
            {
                SceneView.onSceneGUIDelegate -= OnSceneGUI;
                SceneView.onSceneGUIDelegate += OnSceneGUI;
            }
        }
        public virtual void AddItemsToMenu(GenericMenu menu)
        {
            menu.AddItem(new GUIContent("Normal"), this.m_InspectorMode == InspectorMode.Normal, new GenericMenu.MenuFunction(this.SetNormal));
            menu.AddItem(new GUIContent("Debug"), this.m_InspectorMode == InspectorMode.Debug, new GenericMenu.MenuFunction(this.SetDebug));
            if (Unsupported.IsDeveloperBuild())
            {
                menu.AddItem(new GUIContent("Debug-Internal"), this.m_InspectorMode == InspectorMode.DebugInternal, new GenericMenu.MenuFunction(this.SetDebugInternal));
            }
            menu.AddSeparator(string.Empty);
            menu.AddItem(new GUIContent("Lock"), this.m_Tracker != null && this.isLocked, new GenericMenu.MenuFunction(this.FlipLocked));
        }
        private void RefreshTitle()
        {
            string icon = "UnityEditor.TeamInspector";
            if (this.m_InspectorMode == InspectorMode.Normal)
            {
                base.titleContent = new GUIContent("Inspector", icon);
            }
            else
            {
                base.titleContent = new GUIContent("Debug", icon);
            }
        }
        private void SetMode(InspectorMode mode)
        {
            this.m_InspectorMode = mode;
            this.RefreshTitle();
            this.CreateTracker();
            this.m_Tracker.inspectorMode = mode;
            this.m_ResetKeyboardControl = true;
        }
        private void SetDebug()
        {
            this.SetMode(InspectorMode.Debug);
        }
        private void SetNormal()
        {
            this.SetMode(InspectorMode.Normal);
        }
        private void SetDebugInternal()
        {
            this.SetMode(InspectorMode.DebugInternal);
        }
        private void FlipLocked()
        {
            this.isLocked = !this.isLocked;
        }
        private static bool Dragging(Rect rect)
        {
            return (Event.current.type == EventType.DragUpdated || Event.current.type == EventType.DragPerform) && rect.Contains(Event.current.mousePosition);
        }
        public ActiveEditorTracker GetTracker()
        {
            this.CreateTracker();
            return this.m_Tracker;
        }
        protected virtual void CreateTracker()
        {
            if (this.m_Tracker != null)
            {
                this.m_Tracker.inspectorMode = this.m_InspectorMode;
                return;
            }
            ActiveEditorTracker sharedTracker = ActiveEditorTracker.sharedTracker;
            bool flag = TeamInspector.m_AllInspectors.Any((TeamInspector i) => i.m_Tracker != null && i.m_Tracker.Equals(sharedTracker));
            this.m_Tracker = ((!flag) ? ActiveEditorTracker.sharedTracker : new ActiveEditorTracker());
            this.m_Tracker.inspectorMode = this.m_InspectorMode;
            this.m_Tracker.RebuildIfNecessary();
        }
        protected virtual void ShowButton(Rect r)
        {
            bool flag = GUI.Toggle(r, this.isLocked, GUIContent.none, TeamInspector.styles.lockButton);
            if (flag != this.isLocked)
            {
                this.isLocked = flag;
                this.m_Tracker.RebuildIfNecessary();
            }
        }
        protected virtual void OnGUI()
        {
            Profiler.BeginSample("TeamInspector.OnGUI");

            if (selectedObj != null)
                DrawTeamHeader();

            if (m_Tracker != null)
                if (temp_count != m_Tracker.activeEditors.Length)
                {
                    temp_count = m_Tracker.activeEditors.Length;
                    RepaintAllInspectors();
                }

            this.CreateTracker();
            TeamInspector.FlushAllOptimizedGUIBlocksIfNeeded();
            this.ResetKeyboardControl();
            this.m_ScrollPosition = EditorGUILayout.BeginScrollView(this.m_ScrollPosition, new GUILayoutOption[0]);
            if (Event.current.type == EventType.Repaint)
            {
                this.m_Tracker.ClearDirty();
            }
            TeamInspector.s_CurrentTeamInspector = this;
            Editor[] activeEditors = this.m_Tracker.activeEditors;
            //this.AssignAssetEditor(activeEditors);
            Profiler.BeginSample("TeamInspector.DrawEditors()");

            this.DrawEditors(activeEditors);

            Profiler.EndSample();
            if (this.m_Tracker.hasComponentsWhichCannotBeMultiEdited)
            {
                if (activeEditors.Length == 0 && !this.m_Tracker.isLocked && Selection.objects.Length > 0)
                {

                }
                else
                {
                    GUILayout.Label("Components that are only on some of the selected objects cannot be multi-edited.", EditorStyles.helpBox, new GUILayoutOption[0]);
                    GUILayout.Space(4f);
                }
            }
            TeamInspector.s_CurrentTeamInspector = null;
            EditorGUI.indentLevel = 0;
            //EditorGUILayout.Space();
            EditorGUILayout.Separator();
            this.AddComponentButton(this.m_Tracker.activeEditors);
            GUILayout.Space(20f);
            GUI.enabled = true;
            //this.CheckDragAndDrop(this.m_Tracker.activeEditors);
            //this.MoveFocusOnKeyPress();
            GUILayout.EndScrollView();
            Profiler.BeginSample("TeamInspector.DrawPreviewAndLabels");
            //this.DrawPreviewAndLabels();
            Profiler.EndSample();
            Profiler.EndSample();
        }
        public virtual Editor GetLastInteractedEditor()
        {
            return this.m_LastInteractedEditor;
        }
        public UnityEngine.Object GetInspectedObject()
        {
            if (this.m_Tracker == null)
            {
                return null;
            }
            Editor firstNonImportInspectorEditor = this.GetFirstNonImportInspectorEditor(this.m_Tracker.activeEditors);
            if (firstNonImportInspectorEditor == null)
            {
                return null;
            }
            return firstNonImportInspectorEditor.target;
        }
        private Editor GetFirstNonImportInspectorEditor(Editor[] editors)
        {
            for (int i = 0; i < editors.Length; i++)
            {
                Editor editor = editors[i];
                if (!(editor.target is AssetImporter))
                {
                    return editor;
                }
            }
            return null;
        }
        /*private void MoveFocusOnKeyPress()
        {
            KeyCode keyCode = Event.current.keyCode;
            if (Event.current.type != EventType.KeyDown || (keyCode != KeyCode.DownArrow && keyCode != KeyCode.UpArrow && keyCode != KeyCode.Tab))
            {
                return;
            }
            if (keyCode != KeyCode.Tab)
            {
                EditorGUIUtility.MoveFocusAndScroll(keyCode == KeyCode.DownArrow);
            }
            else
            {
                EditorGUIUtility.ScrollForTabbing(!Event.current.shift);
            }
            Event.current.Use();
        }*/
        private void ResetKeyboardControl()
        {
            if (this.m_ResetKeyboardControl)
            {
                GUIUtility.keyboardControl = 0;
                this.m_ResetKeyboardControl = false;
            }
        }

        private UnityEngine.Object[] GetInspectedAssets()
        {
            if (this.m_Tracker != null)
            {
                Editor firstNonImportInspectorEditor = this.GetFirstNonImportInspectorEditor(this.m_Tracker.activeEditors);
                if (firstNonImportInspectorEditor != null && firstNonImportInspectorEditor != null && firstNonImportInspectorEditor.targets.Length == 1)
                {
                    string assetPath = AssetDatabase.GetAssetPath(firstNonImportInspectorEditor.target);
                    bool flag = assetPath.ToLower().StartsWith("assets") && !Directory.Exists(assetPath);
                    if (flag)
                    {
                        return firstNonImportInspectorEditor.targets;
                    }
                }
            }
            return Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets);
        }

        /*private void DrawPreviewAndLabels()
        {
            if (this.m_PreviewWindow && Event.current.type == EventType.Repaint)
            {
                this.m_PreviewWindow.Repaint();
            }
            IPreviewable[] editorsWithPreviews = this.GetEditorsWithPreviews(this.m_Tracker.activeEditors);
            IPreviewable editorThatControlsPreview = this.GetEditorThatControlsPreview(editorsWithPreviews);
            bool flag = editorThatControlsPreview != null && editorThatControlsPreview.HasPreviewGUI() && this.m_PreviewWindow == null;
            UnityEngine.Object[] inspectedAssets = this.GetInspectedAssets();
            bool flag2 = inspectedAssets.Length > 0;
            bool flag3 = inspectedAssets.Any((UnityEngine.Object a) => !(a is MonoScript) && AssetDatabase.IsMainAsset(a));
            if (!flag && !flag2)
            {
                return;
            }
            Event current = Event.current;
            Rect position = EditorGUILayout.BeginHorizontal(GUIContent.none, TeamInspector.styles.preToolbar, new GUILayoutOption[]
            {
                GUILayout.Height(17f)
            });
            Rect position2 = default(Rect);
            GUILayout.FlexibleSpace();
            Rect lastRect = GUILayoutUtility.GetLastRect();
            GUIContent content;
            if (flag)
            {
                GUIContent previewTitle = editorThatControlsPreview.GetPreviewTitle();
                content = (previewTitle ?? TeamInspector.styles.preTitle);
            }
            else
            {
                content = TeamInspector.styles.labelTitle;
            }
            position2.x = lastRect.x + 3f;
            position2.y = lastRect.y + (17f - TeamInspector.s_Styles.dragHandle.fixedHeight) / 2f + 1f;
            position2.width = lastRect.width - 6f;
            position2.height = TeamInspector.s_Styles.dragHandle.fixedHeight;
            if (editorsWithPreviews.Length > 1)
            {
                Vector2 vector = TeamInspector.styles.preDropDown.CalcSize(content);
                Rect position3 = new Rect(lastRect.x, lastRect.y, vector.x, vector.y);
                lastRect.xMin += vector.x;
                position2.xMin += vector.x;
                GUIContent[] array = new GUIContent[editorsWithPreviews.Length];
                int selected = -1;
                for (int i = 0; i < editorsWithPreviews.Length; i++)
                {
                    IPreviewable previewable = editorsWithPreviews[i];
                    GUIContent gUIContent = previewable.GetPreviewTitle() ?? TeamInspector.styles.preTitle;
                    string text;
                    if (gUIContent == TeamInspector.styles.preTitle)
                    {
                        string str = ObjectNames.GetTypeName(previewable.target);
                        if (previewable.target is MonoBehaviour)
                        {
                            str = MonoScript.FromMonoBehaviour(previewable.target as MonoBehaviour).GetClass().Name;
                        }
                        text = gUIContent.text + " - " + str;
                    }
                    else
                    {
                        text = gUIContent.text;
                    }
                    array[i] = new GUIContent(text);
                    if (editorsWithPreviews[i] == editorThatControlsPreview)
                    {
                        selected = i;
                    }
                }
                if (GUI.Button(position3, content, TeamInspector.styles.preDropDown))
                {
                    EditorUtility.DisplayCustomMenu(position3, array, selected, new EditorUtility.SelectMenuItemFunction(this.OnPreviewSelected), editorsWithPreviews);
                }
            }
            else
            {
                float a2 = position2.xMax - lastRect.xMin - 3f - 20f;
                float width = Mathf.Min(a2, TeamInspector.styles.preToolbar2.CalcSize(content).x);
                Rect position4 = new Rect(lastRect.x, lastRect.y, width, lastRect.height);
                position2.xMin = position4.xMax + 3f;
                GUI.Label(position4, content, TeamInspector.styles.preToolbar2);
            }
            if (flag && Event.current.type == EventType.Repaint)
            {
                TeamInspector.s_Styles.dragHandle.Draw(position2, GUIContent.none, false, false, false, false);
            }
            if (flag && this.m_PreviewResizer.GetExpandedBeforeDragging())
            {
                editorThatControlsPreview.OnPreviewSettings();
            }
            EditorGUILayout.EndHorizontal();
            if (current.type == EventType.MouseUp && current.button == 1 && position.Contains(current.mousePosition) && this.m_PreviewWindow == null)
            {
                this.DetachPreview();
            }
            float height;
            if (flag)
            {
                Rect position5 = base.position;
                if (EditorSettings.externalVersionControl != ExternalVersionControl.Disabled && EditorSettings.externalVersionControl != ExternalVersionControl.AutoDetect && EditorSettings.externalVersionControl != ExternalVersionControl.Generic)
                {
                    position5.height -= 17f;
                }
                height = this.m_PreviewResizer.ResizeHandle(position5, 100f, 100f, 17f, lastRect);
            }
            else
            {
                if (GUI.Button(position, GUIContent.none, GUIStyle.none))
                {
                    this.m_PreviewResizer.ToggleExpanded();
                }
                height = 0f;
            }
            if (!this.m_PreviewResizer.GetExpanded())
            {
                return;
            }
            GUILayout.BeginVertical(TeamInspector.styles.preBackground, new GUILayoutOption[]
            {
                GUILayout.Height(height)
            });
            if (flag)
            {
                editorThatControlsPreview.DrawPreview(GUILayoutUtility.GetRect(0f, 10240f, 64f, 10240f));
            }
            if (flag2)
            {
                using (new EditorGUI.DisabledScope(inspectedAssets.Any((UnityEngine.Object a) => EditorUtility.IsPersistent(a) && !Editor.IsAppropriateFileOpenForEdit(a))))
                {
                    this.m_LabelGUI.OnLabelGUI(inspectedAssets);
                }
            }
            if (flag3)
            {
                this.m_AssetBundleNameGUI.OnAssetBundleNameGUI(inspectedAssets);
            }
            GUILayout.EndVertical();
        }*/
        private void DrawEditors(Editor[] editors)
        {
            MaterialEditor materialEditor = null;
            bool rebuildOptimizedGUIBlock=false;
            Type typ;

            if (editors.Length == 0)
            {
                return;
            }
            UnityEngine.Object inspectedObject = this.GetInspectedObject();
            string empty = string.Empty;
            GUILayout.Space(0f);
            if (inspectedObject is Material)
            {
                /*EditorGUILayout.LabelField("MATERIAL " + (editors!=null?editors.Length.ToString():"NULL"));
                if (editors != null)
                {
                    editors.ToList().ForEach(x => { EditorGUILayout.LabelField("Type: " + x.GetType().ToString()); });
                }*/
                int num = 0;
                while (num <= 1 && num < editors.Length)
                {
                    //MaterialEditor materialEditor 
                    materialEditor = editors[num] as MaterialEditor;
                    if (materialEditor != null)
                    {
                        //materialEditor.forceVisible = true;
                        break;
                    }
                    num++;
                }
                if (materialEditor != null)
                {
                    /*
                    EditorGUI.BeginChangeCheck();
                    materialEditor.DrawHeader();
                    DrawEditor(materialEditor);
                    */


                    if (EditorGUI.EndChangeCheck())
                    {
                        if (TeamServerConnection.connected && TeamProjectEditor.TeamAssetSystem.TeamAssetCore.IsTeamAsset(inspectedObject, ShareAssetType.Material))
                        {
                            TeamProjectEditor.TeamServerConnection.AddToTransportStreamWriterList(LibaryClasses.CreateMaterialUpdateRequest((Material)inspectedObject));
                        }
                    }
                }
                else
                {
                    /*if (editors[0].GetType() != typeof(MaterialEditor))
                    {
                        //materialEditor = new MaterialEditor();
                        //materialEditor.Initialize(new UnityEngine.Object[] { inspectedObject });
                       // editors[0] = (MaterialEditor)Editor.CreateEditor(inspectedObject as Material, typeof(MaterialEditor));
                    }*/
                    //materialEditor.target = inspectedObject;
                    //materialEditor.isVisible = true;
                    //Debug.LogError("MAT NULL");
                }

                //materialEditor.PropertiesGUI();
                //DrawEditor(editors[0] as MaterialEditor);
            }
            else
            {
                try
                {
                    bool flag = false;
                    Rect position = default(Rect);
                    for (int i = 0; i < editors.Length; i++)
                    {
                        if (editors[i].target == null)
                        {
                            //   Debug.Log("ta lost");
                            editors[i].Repaint();
                        }
                        //if (editors[i].serializedObject == null)
                        //    Debug.LogError("REFERENCE LOST");


                       // System.Type type = editors[i].serializedObject.targetObject.GetType();// editors[i].GetType();



                        if (Event.current.type == EventType.ExecuteCommand && Event.current.commandName == "EyeDropperUpdate")
                        {
                            rebuildOptimizedGUIBlock = true;
                        }


                        /*
                        if (inspectedObject.GetType() == typeof(GameObject) && i == 0)
                        {
                            DrawGameObjectHeader(inspectedObject as GameObject, teamflag);
                        }*/

                        typ = inspectedObject.GetType();
                        this.DrawEditor(editors[i], i, rebuildOptimizedGUIBlock, ref flag, ref position);
                        if (i == 0 && typ == typeof(GameObject) )
                            if (GUI.changed)
                            {
                                GUI.changed = false;
                                //Debug.LogWarning("MAKE DIRTY");
                                //EditorUtility.SetDirty(editors[i]);
                                ScriptChanges(typ, selectedObj.GetComponent(typ));
                            }

                        /*else if (type == typeof(Material))
                        {
                            materialEditor = editors[i] as MaterialEditor;
                            /*EditorGUILayout.TextField("IS TPYE MATERIALEDITOR HEADER: " + (materialEditor == null ? "NULL" : "TRUE"));
                            try
                            {
                                materialEditor.DrawHeader();
                            }
                            catch (Exception e)
                            {
                                Debug.LogError("ERROR MATERIAL EDITOR: " + e.Message + "\n" + e.StackTrace);
                            }
                            finally
                            {
                                DrawEditor(editors[i] as MaterialEditor);
                                EditorGUILayout.TextField("IS TPYE MATERIALEDITOR: " + (materialEditor == null ? "NULL" : "TRUE"));
                            }*/
                        //if (ActiveEditorTracker.sharedTracker.isDirty)
                        //    Debug.Log("MATERIAL DIRTY");

                        //}
                        /*else if (type == typeof(MonoImporter))
                        {
                            Editor ed = editors[i];// as AssetImporterInspector;

                            ed.DrawHeader();
                            ed.OnInspectorGUI();
                            //Aed.OnPreviewGUI();
                            //DrawEditor();
                            Debug.Log("ASSET IMPORTER");

                        }*/
                        /*                       else if (type != typeof(TeamObject) && type != typeof(TeamView))
                                               {
                                                   bool showw = true;

                                                   //Debug.Log("TYPE " + type.ToString());
                                                   if (!winshows.ContainsKey(type.ToString()))
                                                   {
                                                       winshows.Add(type.ToString(), true);
                                                   }
                                                   else
                                                   {
                                                       showw = winshows[type.ToString()];
                                                   }

                                                   /* if (type == typeof(Material))
                                                    {
                                                        //Debug.Log("MAT2");
                                                    }
                                                    else*/
                        /* {
                             showw = EditorGUILayout.InspectorTitlebar(showw, editors[i].target);
                             if (GUI.changed)
                             {
                                 GUI.changed = false;
                                 //Debug.LogWarning("MAKE DIRTY");
                                 EditorUtility.SetDirty(editors[i]);
                                 ScriptChanges(type, selectedObj.GetComponent(type));
                             }
                         }*/
                        /*
                                                    winshows[type.ToString()] = showw;
                                                    if (showw)
                                                    {
                                                        //this.DrawEditor(editors[i]);

                                                        this.DrawEditor(editors[i], i, rebuildOptimizedGUIBlock, ref flag, ref position);

                                                        if (GUI.changed)
                                                        {
                                                            GUI.changed = false;
                                                            EditorUtility.SetDirty(editors[i]);

                                                            if (teamflag)
                                                            {
                                                                //Debug.Log("CHANGES");
                                                                //Debug.LogWarning("TYPE: " + editors[i].GetType() + "  TypeComp: " + editors[i].serializedObject.targetObject.GetType());
                                                                ScriptChanges(type, selectedObj.GetComponent(type));
                                                            }
                                                        }
                                                    }
                                                }*/
                    }


                    if (position.height > 0f)
                    {
                        GUI.BeginGroup(position);
                        GUI.Label(new Rect(0f, 0f, position.width, position.height), "Imported Object", "OL Title");
                        GUI.EndGroup();
                    }

                }
                catch (System.Exception e)
                {
                    //Debug.LogError("L: " + e.Message);
                }
            }
        }


        private void DrawEditor(Editor editor, int editorIndex, bool rebuildOptimizedGUIBlock, ref bool showImportedObjectBarNext, ref Rect importedObjectBarRect)
        {
            if (!(editor == null))
            {
                UnityEngine.Object target = editor.target;
                GUIUtility.GetControlID(target.GetInstanceID(), FocusType.Passive);
                ResetGUIState();
                //GUILayoutGroup topLevel = GUILayoutUtility.current.topLevel;
                int visible = this.tracker.GetVisible(editorIndex);
                bool flag;
                if (visible == -1)
                {
                    flag = InternalEditorUtility.GetIsInspectorExpanded(target);
                    this.tracker.SetVisible(editorIndex, (!flag) ? 0 : 1);
                }
                else
                {
                    flag = (visible == 1);
                }
               /* rebuildOptimizedGUIBlock |= editor.isInspectorDirty;
                if (Event.current.type == EventType.Repaint)
                {
                    editor.isInspectorDirty = false;
                }*/
                //ScriptAttributeUtility.propertyHandlerCache = editor.propertyHandlerCache;
                bool flag2 = AssetDatabase.IsMainAsset(target) || AssetDatabase.IsSubAsset(target) || editorIndex == 0 || target is Material;
                if (flag2)
                {
                    string empty = string.Empty;
                    bool flag3 = false;// editor.IsOpenForEdit(out empty);
                    if (showImportedObjectBarNext)
                    {
                        showImportedObjectBarNext = false;
                        GUILayout.Space(15f);
                        importedObjectBarRect = GUILayoutUtility.GetRect(16f, 16f);
                        importedObjectBarRect.height = 17f;
                    }
                    flag = true;
                    using (new EditorGUI.DisabledScope(!flag3))
                    {
                        if(editor.target!=null)
                            editor.DrawHeader();
                    }
                }
                if (editor.target is AssetImporter)
                {
                    showImportedObjectBarNext = true;
                }
                bool flag4 = false;
                /*if (editor is GenericInspector && CustomEditorAttributes.FindCustomEditorType(target, false) != null)
                //  {
                    if (this.m_InspectorMode != InspectorMode.DebugInternal)
                    {
                        if (this.m_InspectorMode == InspectorMode.Normal)
                        {
                            flag4 = true;
                        }
                        else if (target is AssetImporter)
                        {
                            flag4 = true;
                        }
                    }
                }*/
                if (!flag2)
                {
                    using (new EditorGUI.DisabledScope(!IsEnabled(editor)))
                    {
                        bool flag5 = EditorGUILayout.InspectorTitlebar(flag, editor.targets, CanBeExpandedViaAFoldout(editor));
                        if (flag != flag5)
                        {
                            this.tracker.SetVisible(editorIndex, (!flag5) ? 0 : 1);
                            InternalEditorUtility.SetIsInspectorExpanded(target, flag5);
                            if (flag5)
                            {
                                this.m_LastInteractedEditor = editor;
                            }
                            else if (this.m_LastInteractedEditor == editor)
                            {
                                this.m_LastInteractedEditor = null;
                            }
                        }
                    }
                }
                if (flag4 && flag)
                {
                    GUILayout.Label("Multi-object editing not supported.", EditorStyles.helpBox, new GUILayoutOption[0]);
                }
                else
                {
                    this.DisplayDeprecationMessageIfNecessary(editor);
                    ResetGUIState();
                    Rect rect = default(Rect);
                    using (new EditorGUI.DisabledScope(!IsEnabled(editor)))
                    {
                        /*GenericInspector genericInspector = editor as GenericInspector;
                        if (genericInspector)
                        {
                            genericInspector.m_InspectorMode = this.m_InspectorMode;
                        }*/
                        EditorGUIUtility.hierarchyMode = true;
                        EditorGUIUtility.wideMode = (base.position.width > 330f);
                       // ScriptAttributeUtility.propertyHandlerCache = editor.propertyHandlerCache;
                        //OptimizedGUIBlock optimizedGUIBlock;
                        float num;
                        /*if (editor.GetOptimizedGUIBlock(rebuildOptimizedGUIBlock, flag, out optimizedGUIBlock, out num))
                        {
                            rect = GUILayoutUtility.GetRect(0f, (!flag) ? 0f : num);
                            this.HandleLastInteractedEditor(rect, editor);
                            if (Event.current.type == EventType.Layout)
                            {
                                return;
                            }
                            if (optimizedGUIBlock.Begin(rebuildOptimizedGUIBlock, rect))
                            {
                                if (flag)
                                {
                                    GUI.changed = false;
                                    //editor.OnOptimizedInspectorGUI(rect);
                                }
                            }
                            optimizedGUIBlock.End();
                        }
                        else**/
                        {
                            if (flag)
                            {
                                GUIStyle style = (!editor.UseDefaultMargins()) ? GUIStyle.none : EditorStyles.inspectorDefaultMargins;
                                rect = EditorGUILayout.BeginVertical(style, new GUILayoutOption[0]);
                                this.HandleLastInteractedEditor(rect, editor);
                                GUI.changed = false;
                                try
                                {
                                    editor.OnInspectorGUI();
                                }
                                catch (Exception exception)
                                {
                                    if (ShouldRethrowException(exception))
                                    {
                                        throw;
                                    }
                                    Debug.LogException(exception);
                                }
                                EditorGUILayout.EndVertical();
                            }
                            if (Event.current.type == EventType.Used)
                            {
                                return;
                            }
                        }
                    }/*
                    if (GUILayoutUtility.current.topLevel != topLevel)
                    {
                        if (!GUILayoutUtility.current.layoutGroups.Contains(topLevel))
                        {
                            Debug.LogError("Expected top level layout group missing! Too many GUILayout.EndScrollView/EndVertical/EndHorizontal?");
                            GUIUtility.ExitGUI();
                        }
                        else
                        {
                            Debug.LogWarning("Unexpected top level layout group! Missing GUILayout.EndScrollView/EndVertical/EndHorizontal?");
                            while (GUILayoutUtility.current.topLevel != topLevel)
                            {
                                GUILayoutUtility.EndLayoutGroup();
                            }
                        }
                    }*/
                    //this.HandleComponentScreenshot(rect, editor);
                }
            }
        }
        internal bool CanBeExpandedViaAFoldout(Editor editor)
        {
            SerializedProperty iterator = editor.serializedObject.GetIterator();
            bool enterChildren = true;
            bool result;
            while (iterator.NextVisible(enterChildren))
            {
                if (EditorGUI.GetPropertyHeight(iterator, null, true) > 0f)
                {
                    result = true;
                    return result;
                }
                enterChildren = false;
            }
            result = false;
            return result;
        }

        internal virtual bool IsEnabled(Editor editor)
        {
            UnityEngine.Object[] targets = editor.targets;
            int i = 0;
            bool result;
            while (i < targets.Length)
            {
                UnityEngine.Object @object = targets[i];
                if ((@object.hideFlags & HideFlags.NotEditable) != HideFlags.None)
                {
                    result = false;
                }
                else
                {
                    if (!EditorUtility.IsPersistent(@object))// || Editor.IsAppropriateFileOpenForEdit(@object))
                    {
                        i++;
                        continue;
                    }
                    result = false;
                }
                return result;
            }
            result = true;
            return result;
        }

        private void DisplayDeprecationMessageIfNecessary(Editor editor)
        {
            if (editor && editor.target)
            {
                ObsoleteAttribute obsoleteAttribute = (ObsoleteAttribute)Attribute.GetCustomAttribute(editor.target.GetType(), typeof(ObsoleteAttribute));
                if (obsoleteAttribute != null)
                {
                    string message = (!string.IsNullOrEmpty(obsoleteAttribute.Message)) ? obsoleteAttribute.Message : "This component has been marked as obsolete.";
                    EditorGUILayout.HelpBox(message, (!obsoleteAttribute.IsError) ? MessageType.Warning : MessageType.Error);
                }
            }
        }

        internal static void ResetGUIState()
        {
            GUI.skin = null;
            Color white = Color.white;
            GUI.contentColor = white;
            GUI.backgroundColor = white;
            GUI.color = ((!EditorApplication.isPlayingOrWillChangePlaymode) ? Color.white : TeamInspector.kPlayModeDarken);
            GUI.enabled = true;
            GUI.changed = false;
            EditorGUI.indentLevel = 0;
            //EditorGUI.ClearStacks();
            EditorGUIUtility.fieldWidth = 0f;
            EditorGUIUtility.labelWidth = 0f;
            //EditorGUIUtility.SetBoldDefaultFont(false);
            //EditorGUIUtility.UnlockContextWidth();
            EditorGUIUtility.hierarchyMode = false;
            EditorGUIUtility.wideMode = false;
            //ScriptAttributeUtility.propertyHandlerCache = null;
        }

        private void HandleLastInteractedEditor(Rect componentRect, Editor editor)
        {
            if (editor != this.m_LastInteractedEditor && Event.current.type == EventType.MouseDown && componentRect.Contains(Event.current.mousePosition))
            {
                this.m_LastInteractedEditor = editor;
                base.Repaint();
            }
        }

        internal static bool ShouldRethrowException(Exception exception)
        {
            while (exception is TargetInvocationException && exception.InnerException != null)
            {
                exception = exception.InnerException;
            }
            return exception is ExitGUIException;
        }


        void ScriptChanges(System.Type type, Component c)
        {
            Debug.LogWarning("TYPE: " + type + " Comp: " + c);
            TeamSceneManager.TeamObject_isDirty = true;
            type = c.GetType();
            //							Debug.Log("EDIT: "+ type.ToString()); 
            // changed = true;
            if (selectedObj.GetComponent<TeamView>() != null)
            {

                TeamView view = selectedObj.GetComponent<TeamView>();
                if (type == typeof(Transform) || type == typeof(RectTransform))
                {
                    //Debug.Log("TRANSFORM CHANGES");
                    view.Update();
                    //view.transformupdate = true;
                }
                else if (view.updateComponent.IndexOf(c) == -1)
                {
                    Debug.LogWarning("TeamView ADDED " + c.GetType() + " To UpdateList");
                    view.updateComponent.Add(c);
                    view.update = true;
                }
                else
                {
                    Debug.LogWarning("ERROR " + "Exists||Transform " + c.GetType());
                }

                if (type == typeof(BoxCollider) || type == typeof(MeshCollider) || type == typeof(CapsuleCollider) || type == typeof(SphereCollider))
                {//Update Bounds
                    view.UpdateBounds();
                }
            }
            else if (selectedObj.GetComponent<TeamObject>() != null)
            {
                TeamObject obj = selectedObj.GetComponent<TeamObject>();
                if (type == typeof(Transform) || type == typeof(RectTransform))
                {
                    //if transform changed
                    obj.Update();
                    //obj.transformupdate = true;
                }
                else if (obj.updateComponent.IndexOf(c) == -1) //if any component has changed
                {
                    Debug.LogWarning("TeamObject ADDED " + c.GetType() + " To UpdateList");
                    obj.updateComponent.Add(c);
                    obj.update = true;
                }
            }
            //else
            //   Debug.LogError("NOT TEAM OBJECT");

        }
        void DrawGameObjectHeader(GameObject selectedObj, bool teamflag)
        {
            //	this.title = "TeamInspector -  RealtimeUpdater";
            //GAMEOBJECT
            GUI.changed = false;
            EditorGUI.BeginChangeCheck();
            GUILayout.BeginHorizontal();
            selectedObj.SetActive(EditorGUILayout.Toggle(selectedObj.activeSelf, GUILayout.MaxWidth(20)));
            GUILayout.Label("Name");
            selectedObj.name = EditorGUILayout.TextField(selectedObj.name);
            selectedObj.isStatic = EditorGUILayout.Toggle(selectedObj.isStatic, GUILayout.MaxWidth(20));
            GUILayout.Label("Static");
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Tag", GUILayout.MaxWidth(25));
            selectedObj.tag = EditorGUILayout.TagField(selectedObj.tag);
            GUILayout.Label("Layer", GUILayout.MaxWidth(40));
            selectedObj.layer = EditorGUILayout.LayerField(selectedObj.layer);
            GUILayout.EndHorizontal();
            bool f = EditorGUI.EndChangeCheck();
            if ((f || GUI.changed) && teamflag)
            {
                //Debug.Log("Changed GameObject Properties");
                if (selectedObj.GetComponent(typeof(TeamView)) != null)
                    selectedObj.GetComponent<TeamView>().gameobjectupdate = true;
                else if (selectedObj.GetComponent(typeof(TeamObject)) != null)
                   selectedObj.GetComponent<TeamObject>().gameobjectupdate = true;

                TeamSceneManager.TeamObject_isDirty = true;
            }
            GUI.changed = false;
        }
        void DrawTeamHeader()
        {
            TeamView view;

            if (TeamServerConnection.connected)
                if (GUILayout.Button(teamflag ? "Unmark as TeamObject" : "Mark as TeamObject"))
                {
                    if (!teamflag)
                    {
                        view = selectedObj.transform.root.GetComponent<TeamView>();
                        if (view == null)
                        {
                            view = selectedObj.transform.root.gameObject.AddComponent<TeamView>();
                            view.hideFlags = HideFlags.DontSaveInEditor | HideFlags.DontSaveInBuild;
                            view.Start();
                        }
                        teamflag = true;
                        TeamSceneManager.Register(view);
                        //container.TeamObjects.Add( selectedObj.transform.root.GetComponent(TeamView));
                        //selectedObj.transform.root.GetComponent(TeamView).Update();
                    }
                    else
                    {
                        view = selectedObj.transform.root.GetComponent<TeamView>();
                        if (view != null)
                        {
                            view.destroyflag = true;
                            TeamSceneManager.Unregister(view);
                        }
                        else
                        {
                            Debug.Log("NO UNREGISTER");
                        }
                        teamflag = false;
                    }

                }
            if (teamflag)
            {
                if (GUILayout.Button("Upload/Update Object to Server"))
                {
                    if (selectedObj.GetComponent<TeamView>() != null)
                        selectedObj.GetComponent<TeamView>().update= true;
                    else if (selectedObj.GetComponent<TeamObject>() != null)
                        selectedObj.GetComponent<TeamObject>().update = true;
                }
            }
        }
        private void DrawEditor(Editor editor)
        {
            if (editor == null)
            {
                return;
            }
            UnityEngine.Object target = editor.target;
            GUIUtility.GetControlID(target.GetInstanceID(), FocusType.Passive);
            Rect rect = default(Rect);
            using (new EditorGUI.DisabledScope(false))
            {
                EditorGUIUtility.hierarchyMode = true;
                EditorGUIUtility.wideMode = (base.position.width > 330f);
                float num;
                {
                    GUIStyle style = (!editor.UseDefaultMargins()) ? GUIStyle.none : EditorStyles.inspectorDefaultMargins;
                    rect = EditorGUILayout.BeginVertical(style, new GUILayoutOption[0]);
                    GUI.changed = false;
                    try
                    {

                        if (editor.serializedObject != null)
                        {
                            editor.OnInspectorGUI();
                        }
                        else
                        {
                            editor.DrawDefaultInspector();
                        }
                    }
                    catch (System.Exception exception)
                    {
                        Debug.LogError("TEAM Inspector: " + exception);
                    }
                    EditorGUILayout.EndVertical();

                    if (Event.current.type == EventType.Used)
                    {
                        return;
                    }
                }
            }
        }
        private void AddComponentButton(Editor[] editors)
        {
            Editor firstNonImportInspectorEditor = this.GetFirstNonImportInspectorEditor(editors);
            if (firstNonImportInspectorEditor != null && firstNonImportInspectorEditor.target != null && firstNonImportInspectorEditor.target is GameObject)
            {
                EditorGUILayout.BeginHorizontal(new GUILayoutOption[0]);
                GUIContent addComponentLabel = TeamInspector.s_Styles.addComponentLabel;
                Rect rect = GUILayoutUtility.GetRect(addComponentLabel, TeamInspector.styles.addComponentButtonStyle, null);
                rect.y += 10f;
                rect.x += (rect.width - 230f) / 2f;
                rect.width = 230f;

                if (Event.current.type == EventType.Repaint)
                {
                    this.DrawSplitLine(rect.y - 11f);
                }
                Event current = Event.current;
                bool flag = false;
                EventType type = current.type;
                if (type == EventType.ExecuteCommand)
                {
                    string commandName = current.commandName;
                    if (commandName == "OpenAddComponentDropdown")
                    {
                        flag = true;
                        current.Use();
                    }
                }
                if (UtilityGUI.ButtonMouseDown(rect, addComponentLabel, FocusType.Passive, TeamInspector.styles.addComponentButtonStyle) || flag)
                {
                    if (AddComponentWindow.Show(rect, (
                        from o in firstNonImportInspectorEditor.targets
                        select (GameObject)o).ToArray<GameObject>()))
                    {
                        GUIUtility.ExitGUI();
                    }
                }
                EditorGUILayout.EndHorizontal();

            }
        }
        private void DrawSplitLine(float y)
        {
            /*GUIStyle titlebar = null;
            EditorStyles current = null;


            try
            {
                current = (EditorStyles)ReflectDllMethods.GetField(typeof(UnityEditor.EditorStyles), null, "s_Current");//EditorStyles.inspectorTitlebar.normal.background
                titlebar = (GUIStyle)ReflectDllMethods.GetField(typeof(UnityEditor.EditorStyles), current, "m_InspectorTitlebar");//EditorStyles.inspectorTitlebar.normal.background
            }
            catch (System.Exception e)
            {
                Debug.LogError("TEAM INSPECTOR " + e.Message);
            }
            if (titlebar != null)
            {*/
            Rect position = new Rect(0f, y, this.position.width + 1f, 1f);
            Rect texCoords = new Rect(0f, 1f, 1f, 1f - 1f / (float)TeamInspector.styles.inspectorTitlebar.normal.background.height);
            GUI.DrawTextureWithTexCoords(position, TeamInspector.styles.inspectorTitlebar.normal.background, texCoords);
            //}
        }
        private bool ReadyToRepaint()
        {
            if (AnimationMode.InAnimationMode())
            {
                long num = DateTime.Now.Ticks / 10000L;
                if (num - this.s_LastUpdateWhilePlayingAnimation < 150L)
                {
                    return false;
                }
                this.s_LastUpdateWhilePlayingAnimation = num;
            }
            return true;
        }
        [MenuItem("Team/Inspector")]
        internal static void ShowWindow()
        {
            EditorWindow window = EditorWindow.GetWindow(typeof(TeamInspector));
            window.titleContent = new GUIContent("TeamInspector");
        }
        private static void FlushAllOptimizedGUIBlocksIfNeeded()
        {
            if (!TeamInspector.s_AllOptimizedGUIBlocksNeedsRebuild)
            {
                return;
            }
            TeamInspector.s_AllOptimizedGUIBlocksNeedsRebuild = false;
            foreach (TeamInspector current in TeamInspector.m_AllInspectors)
            {
                if (current.m_Tracker != null)
                {
                    Editor[] activeEditors = current.m_Tracker.activeEditors;
                    for (int i = 0; i < activeEditors.Length; i++)
                    {
                        Editor editor = activeEditors[i];
                    }
                }
            }
        }
        private void Update()
        {
            if (this.m_Tracker == null)
            {
                return;
            }
            Editor[] activeEditors = this.m_Tracker.activeEditors;
            if (activeEditors == null)
            {
                return;
            }
            bool flag = false;
            Editor[] array = activeEditors;
            for (int i = 0; i < array.Length; i++)
            {
                Editor editor = array[i];
                if (editor.RequiresConstantRepaint())
                {
                    flag = true;
                }
            }
            if (flag && this.m_lastRenderedTime + 0.032999999821186066 < EditorApplication.timeSinceStartup)
            {
                this.m_lastRenderedTime = EditorApplication.timeSinceStartup;
                base.Repaint();
            }
        }
    }
}