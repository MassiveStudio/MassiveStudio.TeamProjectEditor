﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using UnityEditorInternal;
using System.Reflection;
using System.ComponentModel;

namespace MassiveStudio.TeamProjectEditor.EditorWindows.Inspector
{
  // [assembly:]
    internal abstract class AssetImporterInspector : Editor
    {
        private ulong m_AssetTimeStamp;

        private bool m_MightHaveModified;

        private Editor m_AssetEditor;

        internal virtual Editor assetEditor
        {
            get
            {
                return this.m_AssetEditor;
            }
            set
            {
                this.m_AssetEditor = value;
            }
        }

        internal string targetTitle
        {
            get
            {
               
                return string.Format("{0} Import Settings", (!(this.assetEditor == null)) ? m_AssetEditor.GetType().GetField("targetTitle").GetValue(m_AssetEditor) : string.Empty);//this.assetEditor.targetTitle
            }
        }

        internal int referenceTargetIndex
        {
            get
            {
                return (int)this.GetType().GetField("referenceTargetIndex").GetValue(this);
               // return base.referenceTargetIndex;
            }
            set
            {
                this.GetType().GetField("referenceTargetIndex").SetValue(this,value);
                //base.referenceTargetIndex = value;
                if (this.assetEditor != null)
                {
                    this.assetEditor.GetType().GetField("referenceTargetIndex").SetValue(this.assetEditor, value);
                    //this.assetEditor.referenceTargetIndex = value;
                }
            }
        }

        internal IPreviewable preview
        {
            get
            {
                if (this.useAssetDrawPreview && this.assetEditor != null)
                {
                    return (IPreviewable)this.assetEditor;
                }

                return (IPreviewable)this.GetType().GetField("preview").GetValue(this);
               // return base.preview;
            }
        }

        protected virtual bool useAssetDrawPreview
        {
            get
            {
                return true;
            }
        }

        internal virtual bool showImportedObject
        {
            get
            {
                return true;
            }
        }

        internal void OnHeaderIconGUI(Rect iconRect)
        {
            if (this.assetEditor != null)
            {
                Debug.Log("OnHeaderIconGUI");
                this.assetEditor.GetType().GetMethod("OnHeaderIconGUI").Invoke(this.assetEditor, new object[] { iconRect });
                //this.assetEditor.OnHeaderIconGUI(iconRect);
            }
        }

        internal SerializedObject GetSerializedObjectInternal()
        {
            if (this.GetType().GetField("m_SerializedObject").GetValue(this)  == null)
            {
                this.GetType().GetField("m_SerializedObject").SetValue(this, typeof(SerializedObject).GetMethod("LoadFromCache").Invoke(null, new object[] { base.GetInstanceID() }));//SerializedObject.LoadFromCache(base.GetInstanceID())
                //this.m_SerializedObject = SerializedObject.LoadFromCache(base.GetInstanceID());
            }
            if (this.GetType().GetField("m_SerializedObject").GetValue(this) == null)
            {
                this.GetType().GetField("m_SerializedObject").SetValue(this, new SerializedObject(base.targets));
                //this.m_SerializedObject = new SerializedObject(base.targets);
            }
            return (SerializedObject)this.GetType().GetField("m_SerializedObject").GetValue(this);
        }

        public virtual void OnDisable()
        {
            AssetImporter assetImporter = this.target as AssetImporter;
            if (Unsupported.IsDestroyScriptableObject(this) && this.m_MightHaveModified && assetImporter != null && !InternalEditorUtility.ignoreInspectorChanges && this.HasModified() && !this.AssetWasUpdated())
            {
                string message = "Unapplied import settings for '" + assetImporter.assetPath + "'";
                if (base.targets.Length > 1)
                {
                    message = "Unapplied import settings for '" + base.targets.Length + "' files";
                }
                if (EditorUtility.DisplayDialog("Unapplied import settings", message, "Apply", "Revert"))
                {
                    this.Apply();
                    this.m_MightHaveModified = false;
                    AssetImporterInspector.ImportAssets(this.GetAssetPaths());
                }
            }
            
            if (this.GetType().GetField("m_SerializedObject").GetValue(this) != null && (bool)this.GetType().GetField("m_SerializedObject").GetType().GetField("hasModifiedProperties").GetValue(this))//this.m_SerializedObject.hasModifiedProperties
            {
                this.GetType().GetField("m_SerializedObject").GetType().GetMethod("Cache").Invoke(this, new object[] { base.GetInstanceID() });
               // this.m_SerializedObject.Cache(base.GetInstanceID());

                this.GetType().GetField("m_SerializedObject").SetValue(this, null);
                //this.m_SerializedObject = null;
            }
        }

        internal virtual void Awake()
        {
            this.ResetTimeStamp();
            this.ResetValues();
        }

        private string[] GetAssetPaths()
        {
            UnityEngine.Object[] targets = base.targets;
            string[] array = new string[targets.Length];
            for (int i = 0; i < targets.Length; i++)
            {
                AssetImporter assetImporter = targets[i] as AssetImporter;
                array[i] = assetImporter.assetPath;
            }
            return array;
        }

        internal virtual void ResetValues()
        {
            base.serializedObject.SetIsDifferentCacheDirty();
            base.serializedObject.Update();
        }

        internal virtual bool HasModified()
        {
            return (bool)base.serializedObject.GetType().GetField("hasModifiedProperties").GetValue(base.serializedObject);
            //return base.serializedObject.hasModifiedProperties;
        }

        internal virtual void Apply()
        {
            base.serializedObject.ApplyModifiedPropertiesWithoutUndo();
        }

        internal bool AssetWasUpdated()
        {
            AssetImporter assetImporter = this.target as AssetImporter;
            if (this.m_AssetTimeStamp == 0uL)
            {
                this.ResetTimeStamp();
            }
            return assetImporter != null && this.m_AssetTimeStamp != assetImporter.assetTimeStamp;
        }

        internal void ResetTimeStamp()
        {
            AssetImporter assetImporter = this.target as AssetImporter;
            if (assetImporter != null)
            {
                this.m_AssetTimeStamp = assetImporter.assetTimeStamp;
            }
        }

        internal void ApplyAndImport()
        {
            this.Apply();
            this.m_MightHaveModified = false;
            AssetImporterInspector.ImportAssets(this.GetAssetPaths());
            this.ResetValues();
        }

        private static void ImportAssets(string[] paths)
        {
            for (int i = 0; i < paths.Length; i++)
            {
                string path = paths[i];
                AssetDatabase.WriteImportSettingsIfDirty(path);
            }
            try
            {
                AssetDatabase.StartAssetEditing();
                for (int j = 0; j < paths.Length; j++)
                {
                    string path2 = paths[j];
                    AssetDatabase.ImportAsset(path2);
                }
            }
            finally
            {
                AssetDatabase.StopAssetEditing();
            }
        }

        protected void RevertButton()
        {
            this.RevertButton("Revert");
        }

        protected void RevertButton(string buttonText)
        {
            if (GUILayout.Button(buttonText, new GUILayoutOption[0]))
            {
                this.m_MightHaveModified = false;
                this.ResetTimeStamp();
                this.ResetValues();
                if (this.HasModified())
                {
                    Debug.LogError("Importer reports modified values after reset.");
                }
            }
        }

        protected bool ApplyButton()
        {
            return this.ApplyButton("Apply");
        }

        protected bool ApplyButton(string buttonText)
        {
            if (GUILayout.Button(buttonText, new GUILayoutOption[0]))
            {
                this.ApplyAndImport();
                return true;
            }
            return false;
        }

        protected virtual bool ApplyRevertGUIButtons()
        {
            bool result;
            using (new EditorGUI.DisabledScope(!this.HasModified()))
            {
                this.RevertButton();
                result = this.ApplyButton();
            }
            return result;
        }

        protected void ApplyRevertGUI()
        {
            this.m_MightHaveModified = true;
            EditorGUILayout.Space();
            GUILayout.BeginHorizontal(new GUILayoutOption[0]);
            GUILayout.FlexibleSpace();
            bool flag = this.ApplyRevertGUIButtons();
            if (this.AssetWasUpdated() && Event.current.type != EventType.Layout)
            {
                IPreviewable preview = this.preview;
                if (preview != null)
                {
                    preview.ReloadPreviewInstances();
                }
                this.ResetTimeStamp();
                this.ResetValues();
                base.Repaint();
            }
            GUILayout.EndHorizontal();
            if (flag)
            {
                GUIUtility.ExitGUI();
            }
        }
    }

    internal interface IPreviewable
    {
        UnityEngine.Object target { get; }

        void DrawPreview(Rect previewArea);
        string GetInfoString();
        GUIContent GetPreviewTitle();
        bool HasPreviewGUI();
        void Initialize(UnityEngine.Object[] targets);
        bool MoveNextTarget();
        void OnInteractivePreviewGUI(Rect r, GUIStyle background);
        void OnPreviewGUI(Rect r, GUIStyle background);
        void OnPreviewSettings();
        void ReloadPreviewInstances();
        void ResetTarget();
    }
}