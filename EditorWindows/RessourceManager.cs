﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using MassiveStudio.TeamProjectEditor.Properties;

namespace MassiveStudio.TeamProjectEditor.Manager
{
    public static class RessourceManager
    {
        public static Texture2D MainCircle
        {
            get
            {
                if (_MainCircle == null)
                    _MainCircle = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.MainCircle);
                return _MainCircle;
            }
        }
        public static Texture2D _MainCircle;

        public static Texture2D Back
        {
            get
            {
                if (_Back == null)
                    _Back = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.Back);
                return _Back;
            }
        }
        public static Texture2D _Back;

        public static Texture2D FastSelect_2D
        {
            get
            {
                if (_FastSelect_2D == null)
                    _FastSelect_2D = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources._2D);
                return _FastSelect_2D;
            }
        }
        public static Texture2D _FastSelect_2D;

        public static Texture2D FastSelect_3D
        {
            get
            {
                if (_FastSelect_3D == null)
                    _FastSelect_3D = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources._3D);
                return _FastSelect_3D;
            }
        }
        public static Texture2D _FastSelect_3D;

        public static Texture2D GUI_Arrow_Down
        {
            get
            {
                if (_GUI_Arrow_Down == null)
                    _GUI_Arrow_Down = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.GUI_Arrow_Down);
                return _GUI_Arrow_Down;
            }
        }
        public static Texture2D _GUI_Arrow_Down;

        public static Texture2D UI_Button
        {
            get
            {
                if (_UI_Button == null)
                    _UI_Button = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.Button);
                return _UI_Button;
            }
        }
        public static Texture2D _UI_Button;

        public static Texture2D UI_Image
        {
            get
            {
                if (_UI_Image == null)
                    _UI_Image = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.Image);
                return _UI_Image;
            }
        }
        public static Texture2D _UI_Image;

        public static Texture2D UI_Canvas
        {
            get
            {
                if (_UI_Canvas == null)
                    _UI_Canvas = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.Canvas);
                return _UI_Canvas;
            }
        }
        public static Texture2D _UI_Canvas;

        public static Texture2D UI_Toggle
        {
            get
            {
                if (_UI_Toggle == null)
                    _UI_Toggle = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.Toggle);
                return _UI_Toggle;
            }
        }
        public static Texture2D _UI_Toggle;

        public static Texture2D UI_Panel
        {
            get
            {
                if (_UI_Panel == null)
                    _UI_Panel = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.Panel);
                return _UI_Panel;
            }
        }
        public static Texture2D _UI_Panel;

        public static Texture2D Cube
        {
            get
            {
                if (_Cube == null)
                    _Cube = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.Cube);
                return _Cube;
            }
        }
        public static Texture2D _Cube;

        public static Texture2D Sphere
        {
            get
            {
                if (_Sphere == null)
                    _Sphere = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.Sphere);
                return _Sphere;
            }
        }
        public static Texture2D _Sphere;

        public static Texture2D Terrain
        {
            get
            {
                if (_Terrain == null)
                    _Terrain = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.Terrain);
                return _Terrain;
            }
        }
        public static Texture2D _Terrain;

        public static Texture2D Quad
        {
            get
            {
                if (_Quad == null)
                    _Quad = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.Quad);
                return _Quad;
            }
        }
        public static Texture2D _Quad;

        public static Texture2D Cylinder
        {
            get
            {
                if (_Cylinder == null)
                    _Cylinder = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.Cylinder);
                return _Cylinder;
            }
        }
        public static Texture2D _Cylinder;

        public static Texture2D Capsule
        {
            get
            {
                if (_Capsule == null)
                    _Capsule = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.Capsule);
                return _Capsule;
            }
        }
        public static Texture2D _Capsule;

        public static Texture2D FastSelect_Light
        {
            get
            {
                if (_Light == null)
                    _Light = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.Lights);
                return _Light;
            }
        }
        public static Texture2D _Light;

        public static Texture2D DirectionalLight
        {
            get
            {
                if (_DirectionalLight == null)
                    _DirectionalLight = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.DirectionalLight);
                return _DirectionalLight;
            }
        }
        public static Texture2D _DirectionalLight;

        public static Texture2D AreaLight
        {
            get
            {
                if (_AreaLight == null)
                    _AreaLight = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.AreaLight);
                return _AreaLight;
            }
        }
        public static Texture2D _AreaLight;

        public static Texture2D SpotLight
        {
            get
            {
                if (_SpotLight == null)
                    _SpotLight = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.SpotLight);
                return _SpotLight;
            }
        }
        public static Texture2D _SpotLight;

        public static Texture2D PointLight
        {
            get
            {
                if (_PointLight == null)
                    _PointLight = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.PointLight);
                return _PointLight;
            }
        }
        public static Texture2D _PointLight;

        public static Sprite ToSprite(Texture2D tex)
        {
            tex.filterMode = FilterMode.Bilinear;
            tex.Apply();
            Sprite s= Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
            return s;
        }
    }
}
