﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace MassiveStudio.TeamProjectEditor.EditorWindows.Sequenzer
{
    internal class SequenzerWindow : EditorWindow
    {
        bool play = false;
        float playtime = 0.0f;

        Vector2 Sequenz_ScrollView;

        List<Sequenze> sequenzes = new List<Sequenze>();

        List<SequenzLayer> layers = new List<SequenzLayer>();

        float zoom = 1f;
        float scrollx = 0f;
        Vector2 scrollxstart = Vector2.zero;

        int faktor = 1;

        [MenuItem("Team/Sequenzer")]
        static void Init()
        {
            EditorWindow window = EditorWindow.GetWindow(typeof(SequenzerWindow));
            window.titleContent = new GUIContent("Sequenzer");

            //Rect SceneWindow = SceneView.currentDrawingSceneView.position;

            //SceneView.currentDrawingSceneView.BeginWindows();
        }

        void Update()
        {
            Repaint();

        }

        public Event e;
        void OnGUI()
        {
            e = Event.current;
            //DrawToolBar();
            DrawTimeLine();

            if (e.type == EventType.ScrollWheel)
                if (e.delta.y > 0)
                {
                    zoom = Mathf.Lerp(zoom, 100f, e.delta.y * 0.1f);
                }
                else
                {
                    zoom = Mathf.Lerp(zoom, 0.00001f, -e.delta.y * Time.deltaTime * 1000f);
                }
            // zoom -= e.delta.y/25f;

            zoom = Mathf.Clamp(zoom, 0.00001f, 100.0f);

            if (e.type == EventType.MouseDown && e.button == 2)
            {
                scrollxstart = e.mousePosition;
            }
            if (e.type == EventType.MouseDrag && e.button == 2)
            {
                scrollx -= (e.mousePosition - scrollxstart).x;
                scrollxstart = e.mousePosition;
            }

            /* Sequenz_ScrollView = EditorGUILayout.BeginScrollView(Sequenz_ScrollView);
             EditorGUILayout.BeginVertical();
             foreach(SequenzLayer sl in layers)
             {
                 EditorGUILayout.BeginHorizontal();


                 EditorGUILayout.EndHorizontal();
             }

             EditorGUILayout.EndVertical();
             EditorGUILayout.EndScrollView();*/
        }

        public void DrawToolBar()
        {
            GUI.BeginGroup(new Rect(0, 0, 300, position.height));

            EditorGUILayout.BeginHorizontal();
            play = GUILayout.Button("Play");
            play = !GUILayout.Button("Stop");
            EditorGUILayout.EndHorizontal();
            GUI.EndGroup();
        }

        public float GetTicks(int i)
        {
            float[] ticks = new float[]
            {
                0.1f,
                0.5f,
                1f,
                5f,
                10f,
                25f,
                50f,
                100f,
                200f,
                500f,
                1000f,
                5000f,
                10000f,
                50000f,
                100000f,
                500000f
            };
            if (i < ticks.Length - 1 && i >= -2)
                return ticks[2 + i];
            else
                return ticks[ticks.Length - 1];
        }

        public void DrawTimeLine()
        {
            GUI.BeginGroup(new Rect(300, 0, position.width - 300, 500));
            float d = position.width - 300f;

            int boxes = Mathf.CeilToInt(d / 25f);
            int sizes = Mathf.CeilToInt(d / 25f * zoom);

            int f = Mathf.CeilToInt(boxes / sizes);

            float t = GetTicks(0);
            int count = 0;
            if (t * 25 * zoom > 50f)
            {
                while (t * 25 * zoom > 100f && count > -2)
                {
                    count--;
                    t = GetTicks(count);
                }
            }
            else
            {
                while (t * 25 * zoom < 50f && t != 10000f)
                {
                    count++;
                    t = GetTicks(count);
                }
            }

            // int wz = Mathf.CeilToInt(Mathf.Abs((d / zoom) - (50f / zoom)));
            /*GUI.Label(new Rect(0, 0, 100, 25), "F: " + f);
             GUI.Label(new Rect(100, 0, 100, 25), "Boxes: " + boxes);
             GUI.Label(new Rect(200, 0, 100, 25), "Sizes: " + sizes);
             GUI.Label(new Rect(300, 0, 100, 25), "Zoom: " + zoom);
             GUI.Label(new Rect(400, 0, 100, 25), "T: " + t);
             GUI.Label(new Rect(500, 0, 100, 25), "Count: " + count);*/

            GUI.Box(new Rect(0, 0, position.width - 300, 25), "");
            GUI.color = Color.black;
            for (int i = 0; i < boxes; i++)
            {
                GUI.Label(new Rect(1f + t * i * 25 * zoom, 0, 100, 25), string.Format("{0:0}", t * (float)i));
            }
            GUI.color = Color.white;

            /*

            int f = Mathf.CeilToInt(d / (25 * zoom))+1;

            //int s = a / f;

            int f2 = f;
            int g;

            if (f < 10)
                g = 10;
            else
            {
                f = (f - (f % 10));
                g = f / (f / 10);

            }

            float faktor = Mathf.CeilToInt(25 / (25 * zoom));
            faktor = 0.0078425f;

            while ((25 * zoom * faktor) < 25)
                faktor *= 2;


           /* for(int i =0; i< f; i++)
            {
                GUI.Box(new Rect(i * (25 * zoom), 25, (25 * zoom), 25), string.Format("{0:0}", i));

            }*//*

            for (int i =0; i< g; i++)
            {
                GUI.Box(new Rect((i*10) * (25 * zoom), 0, (25 * zoom*10), 25), string.Format("{0:0}",  (i*10f*25f)));
            }

            for (int i = 0; i < a; i++)
            {
                GUI.Box(new Rect((faktor * i * 25* zoom), 50, (25), 25), string.Format("{0:0}", (faktor*(float)i*25f)));
            }


            GUI.Box(new Rect(0 * zoom, 75, 50, 25), "0");
            GUI.Box(new Rect(100 * zoom, 75, 50, 25), "100");
            GUI.Box(new Rect(500 * zoom, 75, 50, 25), "500");
            GUI.Box(new Rect(1000 * zoom, 75, 50, 25), "1000");

            float wz = Mathf.CeilToInt(Mathf.Abs((d / zoom)-(50f/zoom)));
            GUI.Box(new Rect(wz * zoom, 75, 50, 25), ""+wz);



            GUILayout.Label("Z: " + zoom +"  F: " + f +" F2: " +f2+" G: " +g+" S: "+s+" A: "+a+" FAKTOR: " +faktor+"  FAKTORS: "+ faktors);
            */
            GUI.EndGroup();
        }
    }
}