﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using UnityEngine;
using UnityEditor;
using Accessibility;
using Microsoft.Win32;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Diagnostics;
using System.Drawing.Drawing2D;
using System.Globalization;

using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security;
using System.Security.Permissions;
using System.Threading;
using System.Windows.Forms.Layout;

namespace MassiveStudio.TeamProjectEditor.EditorWindows
{
    public class Drag_nDropWindow : MonoBehaviour
    {
        NativeWindow windows = new NativeWindow();
        CreateParams param = new CreateParams();

        Form form = new Form();

        public delegate bool Win32Callback(IntPtr hwnd, IntPtr lParam);

        [DllImport("user32.dll")]
        public static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        [DllImport("user32.Dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool EnumChildWindows(IntPtr parentHandle, Win32Callback callback, IntPtr lParam);

        public static List<IntPtr> GetRootWindowsOfProcess(int pid)
        {
            List<IntPtr> rootWindows = GetChildWindows(IntPtr.Zero);
            List<IntPtr> dsProcRootWindows = new List<IntPtr>();
            foreach (IntPtr hWnd in rootWindows)
            {
                uint lpdwProcessId;
                GetWindowThreadProcessId(hWnd, out lpdwProcessId);
                if (lpdwProcessId == pid)
                    dsProcRootWindows.Add(hWnd);
            }
            return dsProcRootWindows;
        }

        public static List<IntPtr> GetChildWindows(IntPtr parent)
        {
            List<IntPtr> result = new List<IntPtr>();
            GCHandle listHandle = GCHandle.Alloc(result);
            try
            {
                Win32Callback childProc = new Win32Callback(EnumWindow);
                EnumChildWindows(parent, childProc, GCHandle.ToIntPtr(listHandle));
            }
            finally
            {
                if (listHandle.IsAllocated)
                    listHandle.Free();
            }
            return result;
        }

        private static bool EnumWindow(IntPtr handle, IntPtr pointer)
        {
            GCHandle gch = GCHandle.FromIntPtr(pointer);
            List<IntPtr> list = gch.Target as List<IntPtr>;
            if (list == null)
            {
                throw new InvalidCastException("GCHandle Target could not be cast as List<IntPtr>");
            }
            list.Add(handle);
            //  You can modify this to check to see if you want to cancel the operation, then return a null here
            return true;
        }

        // Use this for initialization
        [UnityEditor.MenuItem("MassiveStudio/GETMAINWINDOW")]
        public static void Start()
        {
            System.Diagnostics.Process currentProcess = System.Diagnostics.Process.GetCurrentProcess();
            currentProcess.Refresh();
            UnityEngine.Debug.Log("PROCESSNAME: " + currentProcess.ProcessName + " PID " + currentProcess.Id + " HANDLE "+ currentProcess.Handle.ToInt64());
            IntPtr main = currentProcess.MainWindowHandle;

            List<IntPtr> windows = GetRootWindowsOfProcess(currentProcess.Id);

            if (main == IntPtr.Zero)
                UnityEngine.Debug.Log("NULL POINTER");

            UnityEngine.Debug.Log("Es wurden " + windows.Count + " Fenster gefunden");
            UnityEngine.Debug.Log("MAIN " + main.ToInt32());
            UnityEngine.Debug.Log("MAIN " + main.ToInt64());


            Control mainCon = Control.FromHandle(main);

            UnityEngine.Debug.Log("MAIN: " + (mainCon == null ? "NULL" : "NOTNULL"));

            Control nc = Control.FromHandle(new IntPtr(337642681));
            UnityEngine.Debug.LogWarning("MAIN CONTROL: " + (nc == null ? "NULL" : "NOTNULL"));



            foreach (IntPtr i in windows)
            {
                UnityEngine.Debug.Log("WINDOW: "+ (NativeWindow.FromHandle(i)==null?"NULL":"NOTNULL"));
                UnityEngine.Debug.Log("Handle " + i.ToString() + " / " + i.ToInt64() + " / " + i.ToInt32() + "  Control? " + (Control.FromHandle(i) == null ? "NULL" : "NOTNULL------------------------------------------------"));
            }

            /*
            Window window = Editor.CreateInstance<Window>();
            window.Show();

            param.Width = 200;
            param.Height = 200;
            param.X = 40;
            param.Y = 50;
           // windows.AssignHandle();
            //windows.CreateHandle(param);
            form.Show();
           // window.GetHandle();

           // form.Parent = this;
           //Control*/
        }

        public void Update()
        {
            form.Update();
           
        }

        public void OnDestroy()
        {
            
            windows.ReleaseHandle();
        }
    }

    [DefaultEvent("Click"), DefaultProperty("Text"), DesignerSerializer("System.Windows.Forms.Design.ControlCodeDomSerializer, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", "System.ComponentModel.Design.Serialization.CodeDomSerializer, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"), Designer("System.Windows.Forms.Design.ControlDesigner, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"), ToolboxItemFilter("System.Windows.Forms"), ClassInterface(ClassInterfaceType.AutoDispatch), ComVisible(true)]
    public class Window : EditorWindow, IDropTarget, ISynchronizeInvoke, IWin32Window, IBindableComponent, IComponent, IDisposable, IWindowTarget, IContainerControl, IContainer
    {
        [SerializeField]
        public NativeWindow nwindow;
        [SerializeField]
        public Control control;

        public Form form;

        [UnityEditor.MenuItem("MassiveStudio/Window")]
        public static void Init()
        {
            Window window = Editor.CreateInstance<Window>();
            window.Show();
            window.Start();
 

            //UnityEngine.Debug.Log("Handle " + window.Handle.ToInt64());

//           UnityEngine.Debug.Log(Control.FromHandle(window.Handle));

        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern IntPtr GetForegroundWindow();


        public void Start()
        {
            UnityEngine.Debug.Log("START");

            IntPtr fgw = GetForegroundWindow();


            nwindow = new NativeWindow();
            CreateParams param = new CreateParams();
            param.Width = (int)this.position.width;
            param.Height = (int) this.position.height;
            param.X = (int)this.position.x;
            param.Y = (int)this.position.y;

            param.ClassName = "DragnDrop";
            param.Param = fgw;
            nwindow.CreateHandle(param);

            //fgw = nwindow.Handle;
            UnityEngine.Debug.Log("INTPTR: " + fgw.ToInt64());

            control = Control.FromHandle(fgw);
            if (control == null)
                UnityEngine.Debug.LogError("NULL CONTROL");
            else
            {
                UnityEngine.Debug.Log("CONTROL ADDED");
            }

            form = new Form();
            form.Size = new Size(500, 500);
            form.StartPosition = FormStartPosition.CenterScreen;
            Button button = new Button();
            button.Size = new Size(100, 20);
            button.Left = 0;
            button.Top = 0;
            button.Text = "TEXT";
            form.Controls.Add(button);
            form.Show();
        }

        void OnDestroy()
        {
            if(nwindow!=null)
                nwindow.ReleaseHandle();

            form.Dispose();

        }

        public void GetHandle()
        {
            UnityEngine.Debug.Log(Control.FromHandle(this.Handle));
        }

        public BindingContext BindingContext
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public ControlBindingsCollection DataBindings
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public IntPtr Handle
        {
            get
            {
                return nwindow.Handle;
                
                //throw new NotImplementedException();
            }
        }

        public bool InvokeRequired
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public ISite Site
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public Control ActiveControl
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public ComponentCollection Components
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public event EventHandler Disposed;

        public IAsyncResult BeginInvoke(Delegate method, object[] args)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public object EndInvoke(IAsyncResult result)
        {
            throw new NotImplementedException();
        }

        public object Invoke(Delegate method, object[] args)
        {
            throw new NotImplementedException();
        }

        public void OnDragDrop(DragEventArgs e)
        {
            UnityEngine.Debug.Log("DRAGED FILE");
            throw new NotImplementedException();
        }

        public void OnDragEnter(DragEventArgs e)
        {
            throw new NotImplementedException();
        }

        public void OnDragLeave(EventArgs e)
        {
            throw new NotImplementedException();
        }

        public void OnDragOver(DragEventArgs e)
        {
            throw new NotImplementedException();
        }

        public void OnHandleChange(IntPtr newHandle)
        {
            throw new NotImplementedException();
        }

        public void OnMessage(ref Message m)
        {
            throw new NotImplementedException();
        }

        public bool ActivateControl(Control active)
        {
            UnityEngine.Debug.Log("CONTROLL " + active.ToString());
            throw new NotImplementedException();
        }

        public void Add(IComponent component)
        {
            throw new NotImplementedException();
        }

        public void Add(IComponent component, string name)
        {
            throw new NotImplementedException();
        }

        public void Remove(IComponent component)
        {
            throw new NotImplementedException();
        }
    }
}
