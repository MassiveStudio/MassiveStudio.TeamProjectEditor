﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Security.Permissions;
using System.Reflection;
using System.Collections.Generic;
using System.Threading;

namespace MassiveStudio.TeamProjectEditor
{
//  [InitializeOnLoad]
    public static class MaterialFileWatcher
    {
        [SerializeField]
        public static FileSystemWatcher fsw;
        [SerializeField]
        public static bool fswstate = false;
        [SerializeField]
        public static Dictionary<string, MonoEvent> fileschanged = new Dictionary<string, MonoEvent>();
        [SerializeField]
        public static bool _changed = false;
        [SerializeField]
        public static bool unityready = false;
        [SerializeField]
        public static bool editorisloading = false;
        [SerializeField]
        public static bool updatematerials =false;

//      [InitializeOnLoadMethod]
        [MenuItem("Team/Background FileWatcher")]
        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        public static void StartWatcher()
        {
            if (!fswstate)
            {
                Debug.Log("Material BackgroundFileWatcher started");
                fsw = new FileSystemWatcher(TeamSceneManager.DataPath);
                fsw.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;
                fsw.Filter = "*.mat";
                
                fsw.Changed += new FileSystemEventHandler(OnChanged);
                fsw.Created += new FileSystemEventHandler(OnChanged);
                fsw.Deleted += new FileSystemEventHandler(OnChanged);
                fsw.Renamed += new RenamedEventHandler(OnRenamed);

                fsw.EnableRaisingEvents = true;
                fswstate = true;
                EditorApplication.update -= Update;
                EditorApplication.update += Update;
            }
            else
                StopWatcher();
        }

        public static void Update()
        {
            if (EditorApplication.isCompiling || EditorApplication.isUpdating)
            {
                editorisloading = true;
                if (!unityready)
                    unityready = true;
            }
            else
                editorisloading = false;

            if (updatematerials)
            {
                


                updatematerials = false;
            }
        }

        public static void StopWatcher()
        {
            Debug.Log("STOP WATCHER");
            if (fsw != null)
            {
                fsw.EnableRaisingEvents = false;
                fsw = null;
                fswstate = false;
            }
            EditorApplication.update -= Update;
        }

        static void OnDestroy()
        {
            Debug.Log("Destroy watcher");
            StopWatcher();
        }

        public static void OnChanged(object source, FileSystemEventArgs e)
        {
            _changed = true;
            // Specify what is done when a file is changed, created, or deleted.
            Debug.Log("File: " + e.FullPath + " " + e.ChangeType);

            if (e.ChangeType == WatcherChangeTypes.Deleted)
            {
                if (fileschanged.ContainsKey(e.FullPath))
                    fileschanged.Remove(e.FullPath);

                //SERVER CALL DESTROY FILE IF EXISTS
                TeamServerConnection.AddToWriterList(LibaryClasses.CreateMonoScriptUpdateRequest(e.FullPath, MonoEvent.Deleted));
            }
            else if (e.ChangeType == WatcherChangeTypes.Changed || e.ChangeType == WatcherChangeTypes.Created)
            {
                if (!fileschanged.ContainsKey(e.FullPath))
                    fileschanged.Add(e.FullPath, e.ChangeType == WatcherChangeTypes.Created ? MonoEvent.Created : MonoEvent.Changed);
            }
        }

        private static void OnRenamed(object source, RenamedEventArgs e)
        {
            _changed = true;
            Debug.Log(string.Format("File: {0} renamed to {1}", e.OldFullPath, e.FullPath));
            if (fileschanged.ContainsKey(e.OldFullPath))
            {
                RenameKey(fileschanged, e.OldFullPath, e.FullPath+"*"+ e.OldFullPath);
            }
        }

        public static void RenameKey<TKey, TValue>(this IDictionary<TKey, TValue> dic, TKey fromKey, TKey toKey)
        {
            TValue value = dic[fromKey];
            dic.Remove(fromKey);
            dic[toKey] = value;
        }
    }
}