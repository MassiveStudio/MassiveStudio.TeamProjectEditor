﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace MassiveStudio.TeamProjectEditor.Chat
{
    internal class TeamChat : EditorWindow
    {
        public static TeamChat teamchatwindow;

        public string inputstring = "";
        public static List<TeamMessage> messages = new List<TeamMessage>();
        public float scrollvalue;
        public float maxscroll;
        public GUIStyle style;
        public bool autoscroll = true;
        public bool pushsound = false;
        public int menu = 0;

        public static ChatRoomList roomlist = null;
        public static int roomselect;
        public string chatroom = "";


        public static ChatMembers chatmembers = null;

        protected static bool valid = false;

        public ChatRoom temp;
        public string tempstring;


        public TeamChat()
        {

        }

        [MenuItem("Team/TeamChat")]
        static void Init()
        {
            if (teamchatwindow == null)
                teamchatwindow = EditorWindow.GetWindow<TeamChat>();

            teamchatwindow.position = new Rect(0, 0, 200, 400);
            teamchatwindow.minSize = new Vector2(200, 100);
            teamchatwindow.Show(true);
            teamchatwindow.titleContent = new GUIContent("TeamChat");

            valid = TeamServerConnection.connected;
        }

        public void Update()
        {
            valid = TeamServerConnection.connected;
        }

        public static void ReciveData(TeamMessage tm)
        {
            messages.Add(tm);
        }

        public static void ReciveChatMemberlist(ChatMembers cm)
        {
            chatmembers = cm;
        }

        public static void ReciveChatroomlist(ChatRoomList cr)
        {
            roomlist = cr;
        }

        public void OnGUI()
        {
            if(style==null)
                style = new GUIStyle(GUI.skin.label);

            DrawToolBar();


            if (menu == 0)
            {
                float currenty = 0f;
                GUI.BeginGroup(new Rect(0, 25, position.width, position.height - 75));
                scrollvalue = GUI.VerticalScrollbar(new Rect(position.width - 20, 0, 20, position.height - 75), scrollvalue, (position.height - 75), 0, maxscroll);
                if (!valid)
                    GUI.Label(new Rect(0, 25, position.width - 25, position.height - 75), "No Connection to TeamServer");
                else
                    foreach (TeamMessage m in messages)
                    {
                        GUI.Label(new Rect(0, currenty - scrollvalue, position.width - 25, 25), m.sender + ":");
                        currenty += 25f;

                        float h = style.CalcHeight(new GUIContent(m.text), position.width - 25);
                        float w = style.CalcSize(new GUIContent(m.text)).x / (position.width - 25);
                        h = Mathf.CeilToInt(w) * h;

                        GUI.TextArea(new Rect(0, currenty - scrollvalue, position.width - 25, h), m.text);
                        currenty += h;
                    }
                maxscroll = currenty;
                GUI.EndGroup();

                inputstring = GUI.TextArea(new Rect(0, position.height - 50, position.width - 50, 50), inputstring);
                if (GUI.Button(new Rect(position.width - 50, position.height - 50, 50, 50), "Send"))
                {
                    if (inputstring != string.Empty)
                    {
                        Send();
                        inputstring = string.Empty;
                    }
                }
            }
            else if(menu == 1){
                GUI.BeginGroup(new Rect(0, 25, position.width, position.height -25));
                scrollvalue = GUI.VerticalScrollbar(new Rect(position.width - 20, 0, 20, position.height - 50), scrollvalue, (position.height - 50), 0, maxscroll);
                float currenty = 0f;
                if (roomlist != null && roomlist.rooms.Count > 0)
                    foreach (ChatRoom r in roomlist.rooms)
                    {
                        if (GUI.Button(new Rect(0, currenty - scrollvalue, position.width - 25, 50), r.RoomName))
                        {
                            currenty += 50f;
                        }
                    }
                else
                    GUI.Label(new Rect(0, 0, position.width - 25, 50), "No Chatrooms found");
                maxscroll = currenty;

                if (GUI.Button(new Rect(0, position.height - 50, position.width - 50, 25), "+ New ChatRoom"))
                {
                    temp = new ChatRoom();
                    menu = 2;
                }


                if (GUI.Button(new Rect(position.width - 50, position.height - 50, 50, 25), "back"))
                {
                    menu = 0;
                }

                GUI.EndGroup();
            }
            else if(menu == 2)
            {
                GUI.BeginGroup(new Rect(0, 25, position.width, position.height - 25));

                GUI.Label(new Rect(0, 25, 75, 25),"RoomName:");
                temp.RoomName = GUI.TextField(new Rect(75, 25, position.width - 75, 25), temp.RoomName);

                GUI.Label(new Rect(0, 50, 75, 25), "RoomType:");
                temp.permission.roomstate = (ChatRoomState)EditorGUI.EnumPopup(new Rect(75, 50, position.width - 75, 25),"", temp.permission.roomstate);

                if( temp.permission.roomstate != ChatRoomState.Public)
                {
                    GUI.Label(new Rect(0, 75, 75, 25), "Password:");
                    tempstring = EditorGUI.PasswordField(new Rect(75, 75, position.width-75, 25), "", tempstring);
                }
                else
                {
                    GUI.Label(new Rect(0, 75, position.width, 25), "No Password Option");
                }

                if (GUI.Button(new Rect(0, position.height-50, position.width-50,25), "Create ChatRoom"))
                {
                    bool a = string.IsNullOrEmpty(tempstring);
                    temp.permission.password = tempstring;
                    menu = 1;
                }

                if (GUI.Button(new Rect(position.width - 50, position.height - 50, 50, 25), "back"))
                {
                    temp = null;
                    menu = 1;
                }

                GUI.EndGroup();
            }
        }

        public void DrawToolBar()
        {
            GUI.Box(new Rect(0, 0, position.width, 25), "");

            //GUI.Toolbar(new Rect(25,0,25,25), )
            if (GUI.Button(new Rect(0, 0, 25, 25), (autoscroll ? "A" : "D")))
            {
                autoscroll = !autoscroll;
            }
            if (GUI.Button(new Rect(25, 0, 25, 25), "G"))
            {
                if (menu != 1)
                    menu = 1;
                else
                    menu = 0;//BACK TO PUBLIC CHAT
            }
        }

        public void DrawGroupWindow()
        {

        }

        public void Send()
        {
            if (TeamServerConnection.connected && TeamServerConnection.authenticated)
            {
                TeamMessage m = new TeamMessage(inputstring, TeamServerConnection.currentServer.user.Username, "");
                messages.Add(m);
                TeamServerConnection.AddToWriterList(m);
            }
        }

        public void Save()
        {

        }
    }
}