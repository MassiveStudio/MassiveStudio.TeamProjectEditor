﻿using MassiveStudio.TeamProjectEditor.Utility;
using UnityEditor.AnimatedValues;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEditorInternal;
using System.Reflection;
using UnityEngine;
using UnityEditor;
using System.Text;
using System.Linq;
using System;

namespace MassiveStudio.TeamProjectEditor.EditorWindows.SceneWindow
{

    public enum DraggingLockedState
    {
        NotDragging,
        Dragging,
        LookAt
    }


    public class _4ViewScene : EditorWindow
    {
        public Camera cam_main;
        public static Shader s_ShowOverdrawShader;
        public static Shader s_AuraShader;


        public DrawCameraMode m_RenderMode;
        private RenderTexture m_SceneTargetTexture;

        [NonSerialized]
        private Light[] m_Light = new Light[3];

        internal static Color kSceneViewFrontLight = new Color(0.769f, 0.769f, 0.769f, 1f);

        internal static Color kSceneViewUpLight = new Color(0.212f, 0.227f, 0.259f, 1f);

        internal static Color kSceneViewMidLight = new Color(0.114f, 0.125f, 0.133f, 1f);

        internal static Color kSceneViewDownLight = new Color(0.047f, 0.043f, 0.035f, 1f);

        private static readonly Color kSceneViewBackground = new Color(0.278431f, 0.278431f, 0.278431f, 0f);

        private static readonly Color kSceneViewWire = new Color(0f, 0f, 0f, 0.5f);

        private static readonly Color kSceneViewWireOverlay = new Color(0f, 0f, 0f, 0.25f);

        private static readonly Color kSceneViewSelectedOutline = new Color(1f, 0.4f, 0f, 0f);

        private static readonly Color kSceneViewSelectedWire = new Color(0.368627459f, 0.466666669f, 0.607843161f, 0.2509804f);

        private double m_StartSearchFilterTime = -1.0;

        [SerializeField]
        private AnimVector3 [] m_Position = new AnimVector3[4];//kdefaultPivoit

        [SerializeField]
        public AnimQuaternion[] m_Rotation = new AnimQuaternion[4];//(_4ViewScene.kDefaultRotation);

        [SerializeField]
        private AnimFloat[] m_Size = new AnimFloat[4];//(10f);

        [SerializeField]
        internal AnimBool [] m_Ortho = new AnimBool[4];

        [NonSerialized]
        private static readonly Quaternion kDefaultRotation = Quaternion.LookRotation(new Vector3(-1f, -0.7f, -1f));

        [NonSerialized]
        private static readonly Vector3 kDefaultPivot = Vector3.zero;

        [SerializeField]
        public bool[] m_lookedRotation = new bool[4];

        Transform OBJ;

        [SerializeField]
        private SceneViewGrid grid;
        public object gridParam;
        Type sceneGrid;

        private static Material s_AlphaOverlayMaterial;//FOR SEARCHING

        private static Material s_DeferredOverlayMaterial;

        private static Shader s_ShowMipsShader;


        public static OnSceneFunc onSceneGUIDelegate;
        public delegate void OnSceneFunc(_4ViewScene sceneView);


        public SceneViewRotation svRot;
        private bool m_2DMode=false;
        private bool m_isRotationLocked=false;
        public bool viewIsLockedToObject;
        public bool in2DMode
        {
            get
            {
                return this.m_2DMode;
            }
            set
            {
                if (this.m_2DMode != value && UnityEditor.Tools.viewTool != ViewTool.FPS && UnityEditor.Tools.viewTool != ViewTool.Orbit)
                {
                    this.m_2DMode = value;
                    this.On2DModeChange();
                }
            }
        }

        public bool isRotationLocked
        {
            get
            {
                return this.m_isRotationLocked;
            }
            set
            {
                if (this.m_isRotationLocked != value)
                {
                    this.m_isRotationLocked = value;
                }
            }
        }

        public bool RotateMotionLocked
        {
            get
            {
                return m_lookedRotation[computedCamera];
            }
            set
            {
                m_lookedRotation[computedCamera] = value;
            }
        }

        public Vector3 pivot
        {
            get
            {
                return this.m_Position[computedCamera].value;
            }
            set
            {
                this.m_Position[computedCamera].value = value;
            }
        }

        public Quaternion rotation
        {
            get
            {
                return this.m_Rotation[computedCamera].value;
            }
            set
            {
                this.m_Rotation[computedCamera].value = value;
            }
        }

        public float cameraDistance
        {
            get
            {
                float num = this.m_Ortho[computedCamera].Fade(90f, 0f);
                float result;
                if (!this.computed_camera.orthographic)
                {
                    result = this.size / Mathf.Tan(num * 0.5f * 0.0174532924f);
                }
                else
                {
                    result = this.size * 2f;
                }
                return result;
            }
        }

        public float size
        {
            get
            {
                return this.m_Size[computedCamera].value;
            }
            set
            {
                if (value > 40000f)
                {
                    value = 40000f;
                }
                this.m_Size[computedCamera].value = value;
            }
        }

        public bool orthographic
        {
            get
            {
                return this.m_Ortho[computedCamera].value;
            }
            set
            {
                this.m_Ortho[computedCamera].value = value;
            }
        }

        public DraggingLockedState m_DraggingLockedState;

        public DraggingLockedState draggingLocked
        {
            get
            {
                return this.m_DraggingLockedState;
            }
            set
            {
                this.m_DraggingLockedState = value;
            }
        }


        //public Vector3 FocusPoint;

        public Camera[] sceneCameras = new Camera[4];

        public RenderTexture[] renderTextures = new RenderTexture[4];

        public int selectedCamera = 0;


        private Quaternion[] m_lastSceneViewRotation = new Quaternion[4];
        private Quaternion lastSceneViewRotation
        {
            get
            {
                return m_lastSceneViewRotation[computedCamera];
            }
            set
            {
                m_lastSceneViewRotation[computedCamera] = value;
            }
        }


        private RectSelection m_RectSelection;

        public bool showCameraView = true;
        private bool m_LastSceneViewOrtho;
        private bool m_ViewIsLockedToObject;

        private static UnityEditor.Tool s_CurrentTool;


        public static _4ViewScene currentView;

        public Camera selected_camera
        {
            get
            {
                return sceneCameras[selectedCamera];
                //return this.cam_main;
            }
        }

        public Camera computed_camera
        {
            get
            {
                return sceneCameras[computedCamera];
                //return this.cam_main;
            }
        }

        public int computedCamera = 0;

        public Rect GetCameraRect;

        public int my = 0;

        [SerializeField]
        static Delegate action;

        [SerializeField]
        protected static MethodInfo _DrawCameraStep1;
        static MethodInfo DrawCameraStep1
        {
            get
            {
                if(_DrawCameraStep1==null)
                    _DrawCameraStep1 = typeof(UnityEditor.Handles).GetMethod("DrawCameraStep1", BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic);
                return _DrawCameraStep1;
            }
        }


        [SerializeField]
        protected static MethodInfo _DrawCameraStep2;
        static MethodInfo DrawCameraStep2
        {
            get
            {
                if (_DrawCameraStep2 == null)
                    _DrawCameraStep2 = typeof(UnityEditor.Handles).GetMethod("DrawCameraStep2", BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic);
                return _DrawCameraStep2;
            }
        }

        [SerializeField]
        protected static MethodInfo _Push;
        static MethodInfo Push
        {
            get
            {
                if (_Push == null)
                    _Push = ReflectDllMethods.GetType("UnityEngine.GUIClip").GetMethod("Push", BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic);
                return _Push;
            }
        }

        [SerializeField]
        protected static MethodInfo _Pop;
        static MethodInfo Pop
        {
            get
            {
                if (_Pop == null)
                    _Pop = ReflectDllMethods.GetType("UnityEngine.GUIClip").GetMethod("Pop", BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic);
                return _Pop;
            }
        }

        [SerializeField]
        protected static MethodInfo _HandlesGetCameraRect;
        static MethodInfo HandlesGetCameraRect
        {
            get
            {
                if (_HandlesGetCameraRect == null)
                    _HandlesGetCameraRect = typeof(UnityEditor.Handles).GetMethod("GetCameraRect", BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic);
                return _HandlesGetCameraRect;
            }
        }


        [SerializeField]
        protected static MethodInfo _IsGizmosAllowedForObject;
        static MethodInfo IsGizmosAllowedForObject
        {
            get
            {
                if (_IsGizmosAllowedForObject == null)
                    _IsGizmosAllowedForObject = typeof(UnityEditor.EditorGUIUtility).GetMethod("IsGizmosAllowedForObject", BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic);
                return _IsGizmosAllowedForObject;
            }
        }


        private object m_DragEditorCache;//EditorCache

        private GUIContexMenu[] _cm;

        public GUIContexMenu cm
        {
            get
            {
                if (_cm == null)
                    _cm = new GUIContexMenu[4];

                if (_cm[computedCamera] == null)
                    _cm[computedCamera] = new GUIContexMenu();

                return _cm[computedCamera];
            }
        }



        [MenuItem("MassiveStudio/4ViewScene")]
        internal static void Init()
        {
            _4ViewScene renderer = GetWindow<_4ViewScene>();
            renderer.titleContent = new GUIContent("4/1 Scene");
            renderer.Show();
            currentView = renderer;


            BindingFlags flags = BindingFlags.Public |
            BindingFlags.NonPublic |
            BindingFlags.Static |
            BindingFlags.Instance |
            BindingFlags.DeclaredOnly;


            FieldInfo info = typeof(UnityEditor.Tools).GetField("onToolChanged", flags);
            MethodInfo method = typeof(_4ViewScene).GetMethod("OnToolChanged");

            Delegate d = (Delegate)info.GetValue(null);
            if (d == null)
            {
                action = Delegate.CreateDelegate(info.FieldType, null, method);
                d = action;
            }
            else
            {
                action = Delegate.CreateDelegate(info.FieldType, null, method);
                d= Delegate.Combine(d, action);
            }
            info.SetValue(null, d);

         }

        public Rect GetViewRect(int id, float x, float y)
        {
            switch (id)
            {
                case 0:
                    return new Rect(5, 17, x, y);
                case 1:
                    return new Rect(x+10, 17, x, y);
                case 2:
                    return new Rect(5, y+17, x, y);
                case 3:
                    return new Rect(x+10, y+17, x, y);
                default:
                    return new Rect(5, 17, x, y);
            }
        }

        public Rect GetViewPixelRect(int id, float x, float y)
        {
            return EditorGUIUtility.PointsToPixels(GetViewRect(id, x, y));
        }

        public Vector2 sizeOfScreenView;

        public Vector2 verschiebung;

        public Event e;
        private bool pushedClip = false;
        Rect textureRect;
        public void OnGUI()
        {
            if (cam_main == null)
                return;
            e = Event.current;

            pushedClip = false;

            if (showCameraView)
            {
                float x =(base.position.width / 2f) -10f;
                float y = (base.position.height / 2f) -17f;
                sizeOfScreenView = new Vector2(x, y);
                Rect rect = new Rect(5, 17, x, y);

                Rect cameraRect = EditorGUIUtility.PointsToPixels(rect);
                GetCameraRect = new Rect(5, 17, x, y);//x,y,x,y

                if (e.type == EventType.Repaint)
                {

                    RenderTexture.active = null;
                    GL.wireframe = true;
                    //UpdateRenderTextures(cameraRect);
                    for (int i = 0; i < 4; i++)
                    {
                        computedCamera = i;
                        UpdateRenderTextures(i);

                        if (i == 2)
                        {//LAST CAMERA WITHOUT WIREFRAME
                            GL.wireframe = false;
                            this.DoClearCamera(cameraRect, computed_camera);
                        }
                        else
                            GL.wireframe = true;


                        SetupCamera(i);
                        if(i!=2)
                            sceneCameras[i].Render();
                    }
                    GL.wireframe = false;

                    GL.sRGBWrite = (QualitySettings.activeColorSpace == ColorSpace.Linear);
                    Graphics.SetRenderTarget(null);
                }//Repaint End
                GL.sRGBWrite = false;

                SelectView(x, y);

                /*
                //Normal Camera with Rotation Navigation
                computedCamera = 2;
                HandleUtility.PushCamera(computed_camera);
                Handles.SetCamera(EditorGUIUtility.PointsToPixels(new Rect(5, 17, x, y)), sceneCameras[computedCamera]);
                GUI.BeginGroup(new Rect(5f, 17 + y, x, y));

                /*if (SceneCameraRendersIntoRT())
                {
                    ReflectDllMethods.GetMethodInvokedVoid("UnityEngine", "GUIClip", "Push", new object[] { EditorGUIUtility.PointsToPixels(new Rect(0, 0, x, y)), Vector2.zero, Vector2.zero, true });
                    pushedClip = true;
                }*/

                /*

                if (e.type == EventType.Repaint)
                {
                    ReflectDllMethods.GetMethodInvokedVoid("UnityEditor", "Handles", "DrawCameraStep1", new object[] { EditorGUIUtility.PointsToPixels(new Rect(0, 0, x, y)), this.computed_camera, DrawCameraMode.Textured, gridParam });
                }

                PrepareCameraReplacementShader();
                ReflectDllMethods.GetMethodInvokedVoid("UnityEditor", "Handles", "DrawCameraStep2", new object[] { computed_camera, DrawCameraMode.RealtimeAlbedo });
                GUI.EndGroup();

                Graphics.SetRenderTarget(null);

                GUI.BeginGroup(new Rect(5f, 17 + y, x, y));
                if (selectedCamera == computedCamera)
                {
                    //Handles.DrawCamera(EditorGUIUtility.PointsToPixels(new Rect(5f, 17 + y, x, y)), computed_camera);
                    bool sRGBWrite = GL.sRGBWrite;
                    GL.sRGBWrite = false;
                    Handles.SetCamera(EditorGUIUtility.PointsToPixels(new Rect(5, 17, x, y)), sceneCameras[computedCamera]);
                    this.HandleSelectionAndOnSceneGUI(new Rect(5f, 17 + y, x, y));
                    GL.sRGBWrite = sRGBWrite;
                }
                GUI.EndGroup();

                Handles.SetCamera(EditorGUIUtility.PointsToPixels(new Rect(5, y + 17, x, y)), sceneCameras[computedCamera]);
                this.CallOnSceneGUI();
                //DrawViewGrid(new Rect(5, y + 17, x, y));
                DefaultHandles(new Rect(5, y + 17, x, y));

                this.RepaintGizmosThatAreRenderedOnTopOfSceneView(new Rect(5, y + 17, x, y));
                HandleUtility.PopCamera(computed_camera);
                Graphics.SetRenderTarget(null);

                */

                BeginWindows();
                GUI.backgroundColor = selectedCamera == 0 ? Color.grey : Color.white;
                GUI.Window(0, new Rect(0, 0, x + 10, y + 17), DrawSceneCamera, "FRONT");

                GUI.backgroundColor = selectedCamera == 1 ? Color.grey : Color.white;
                GUI.Window(1, new Rect(10 + x, 0, x + 10, y + 17), DrawSceneCamera, "TOP");

                GUI.backgroundColor = selectedCamera == 3 ? Color.grey : Color.white;
                GUI.Window(3, new Rect(10 + x, y + 17, x + 10, y + 17), DrawSceneCamera, "Left");

                GUI.backgroundColor = selectedCamera == 2 ? Color.grey : Color.white;
                GUI.Window(2, new Rect(0, y + 17, x + 10, y + 17), DrawSceneCamera, "Perpective");
                EndWindows();

                for (int i = 0; i < 4; i++)
                {
                    if (i == 2)
                        continue;

                    Rect r = GetViewRect(i, x, y);
                    r.x += 5;
                    r.y += 17;

                    Rect r2 = EditorGUIUtility.PointsToPixels(r);

                    computedCamera = i;
                    HandleUtility.PushCamera(computed_camera);
                    sceneCameras[computedCamera].targetTexture = null;
                    sceneCameras[computedCamera].rect = new Rect(0, 0, 1, 1);
                    if (selectedCamera == computedCamera)
                    {
                        Handles.SetCamera(r2, sceneCameras[computedCamera]);//new Rect(x, position.height, x, y),
                        GUI.BeginGroup(r);
                        bool sRGBWrite = GL.sRGBWrite;
                        GL.sRGBWrite = false;
                        this.HandleSelectionAndOnSceneGUI(r);
                        GL.sRGBWrite = sRGBWrite;
                        GUI.EndGroup();
                    }
                    Handles.SetCamera(r2, sceneCameras[computedCamera]);//new Rect(x, position.height, x, y),
                    this.CallOnSceneGUI();
                    DefaultHandles(r);

                    HandleUtility.PopCamera(computed_camera);
                    Graphics.SetRenderTarget(null);
                    sceneCameras[computedCamera].rect = new Rect(0, 0, 1, 1);
                }

                HandleDragging();

                SelectedViewMove();
                DebugScreen();

            }
            #region 1View
            else
            {
                float x = (base.position.width) - 10f;
                float y = (base.position.height) - 17f;
                sizeOfScreenView = new Vector2(x, y);
                Rect rect = new Rect(5, 17, x, y);

                Rect cameraRect = EditorGUIUtility.PointsToPixels(rect);
                GetCameraRect = new Rect(5, 17, x, y);//x,y,x,y

                if (e.type == EventType.Repaint)
                {
                    RenderTexture.active = null;
                    GL.wireframe = true;
                    //UpdateRenderTextures(cameraRect);

                    computedCamera = selectedCamera;
                    UpdateRenderTextures(selectedCamera);

                    if (selectedCamera == 2)
                    {//LAST CAMERA WITHOUT WIREFRAME
                        GL.wireframe = false;
                        this.DoClearCamera(cameraRect, computed_camera);
                    }
                    else
                        GL.wireframe = true;

                    SetupCamera(selectedCamera);
                    if (selectedCamera != 2)
                        sceneCameras[selectedCamera].Render();

                    GL.wireframe = false;

                    GL.sRGBWrite = (QualitySettings.activeColorSpace == ColorSpace.Linear);
                    Graphics.SetRenderTarget(null);
                }//Repaint End
                GL.sRGBWrite = false;

                DrawMainCamera();//Rendering of the camera Data to GUI

                if (selectedCamera != 2)
                {
                    Rect r = GetViewRect(selectedCamera, x, y);
                    r.x += 5;
                    r.y += 17;

                    Rect r2 = EditorGUIUtility.PointsToPixels(r);

                    computedCamera = selectedCamera;
                    HandleUtility.PushCamera(computed_camera);
                    sceneCameras[computedCamera].targetTexture = null;
                    sceneCameras[computedCamera].rect = new Rect(0, 0, 1, 1);

                    Handles.SetCamera(r2, sceneCameras[computedCamera]);//new Rect(x, position.height, x, y),
                    GUI.BeginGroup(r);
                    bool sRGBWrite = GL.sRGBWrite;
                    GL.sRGBWrite = false;
                    this.HandleSelectionAndOnSceneGUI(r);
                    GL.sRGBWrite = sRGBWrite;
                    GUI.EndGroup();

                    Handles.SetCamera(r2, sceneCameras[computedCamera]);//new Rect(x, position.height, x, y),
                    this.CallOnSceneGUI();
                    DefaultHandles(r);

                    HandleUtility.PopCamera(computed_camera);
                    Graphics.SetRenderTarget(null);
                    sceneCameras[computedCamera].rect = new Rect(0, 0, 1, 1);
                }

                HandleDragging();
                SelectedViewMove();
                DebugScreen();

                /*GUI.BeginGroup(new Rect(0f, 17f, base.position.width, base.position.height - 17f));
                Rect rect = new Rect(0f, 0f, base.position.width, base.position.height - 17f);
                GUI.Label(rect, "Size: " + rect.size.ToString());
                Rect cameraRect = EditorGUIUtility.PointsToPixels(rect);
                GUI.Label(new Rect(0, 50, position.width, 50), "Rect: " + cameraRect.ToString());

                //Update Size Of RenderTexture
                this.PrepareCameraTargetTexture(cameraRect);
                //this.DoClearCamera(cameraRect, cam_main);

                // Handles.DrawCamera(cameraRect, cam_main, DrawCameraMode.RealtimeAlbedo);

                RenderTexture rt = RenderTexture.active;
                rt = m_SceneTargetTexture;

                GL.wireframe = true;

                cam_main.Render();
                //Handles.DrawCamera(cameraRect, cam_main, DrawCameraMode.RealtimeAlbedo);
                GL.wireframe = false;

                if (e.type == EventType.Repaint)
                {
                    //cam_main.RenderWithShader(s_AuraShader, "");
                    Graphics.SetRenderTarget(null);
                    GL.sRGBWrite = (QualitySettings.activeColorSpace == ColorSpace.Linear);
                    GUI.DrawTexture(rect, this.m_SceneTargetTexture, ScaleMode.StretchToFill, false);
                    GL.sRGBWrite = false;
                }


                GUI.backgroundColor = Color.white;

                RenderTexture.active = rt;

                GUI.EndGroup();*/

                if (e.type == EventType.MouseDown && e.button == 2)
                {
                    if (rect.Contains(e.mousePosition))
                    {
                        showCameraView = true;
                        Repaint();
                        e.Use();
                    }
                }
            }
            #endregion
        }

        public void DebugScreen()
        {
            if (e.keyCode == KeyCode.F1 && e.type == EventType.KeyDown)
            {
                GUI.Box(new Rect(0, 0, 200, 50), "ROT: " + rotation.eulerAngles.ToString());
                GUI.Box(new Rect(0, 50, 200, 50), "PIV: " + pivot.ToString());
                GUI.Box(new Rect(0, 100, 200, 50), "Sel C: " + selectedCamera);
                GUI.Box(new Rect(0, 150, 200, 50), "SCR: " + position.width + ":" + position.height);
                GUI.Box(new Rect(0, 200, 200, 50), "2D: " + m_2DMode);
            }
        }

        public void SelectedViewMove()
        {
            computedCamera = selectedCamera;
            Handles.SetCamera(sceneCameras[selectedCamera]);
            SceneViewMotion.ArrowKeys(this);
            Handles.SetCamera(sceneCameras[selectedCamera]);
            SceneViewMotion.DoViewTool(this);
            Graphics.SetRenderTarget(null);
        }

        private void RepaintGizmosThatAreRenderedOnTopOfSceneView(Rect r)
        {
            this.svRot.OnGUI(this, r);
        }

        public void OnFocus()
        {
            currentView = this;
        }

        public void SelectView(float x, float y)
        {
            // Debug.Log("CLICKED");
            if (new Rect(0f, 0f, x, y).Contains(e.mousePosition))
            {
                selectedCamera = 0;
                if (e.button == 2 && e.type == EventType.MouseDown)
                    showCameraView = false;
                Repaint();
            }
            else if (new Rect(x, 0f, x, y).Contains(e.mousePosition))
            {
                selectedCamera = 1;
                if (e.button == 2 && e.type == EventType.MouseDown)
                    showCameraView = false;
                Repaint();
            }
            else if (new Rect(0f, y, x, y).Contains(e.mousePosition))
            {
                selectedCamera = 2;
                if (e.button == 2 && e.type == EventType.MouseDown)
                    showCameraView = false;
                Repaint();
            }
            else if (new Rect(x, y, x, y).Contains(e.mousePosition))
            {
                selectedCamera = 3;
                if (e.button == 2 && e.type == EventType.MouseDown)
                    showCameraView = false;
                Repaint();
            }

            if(Input.GetKeyDown(KeyCode.I))
                Selection.activeTransform = sceneCameras[selectedCamera].transform;

        }

        public void OnEnable()
        {
            #region Catch ToolChange Event
            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic |BindingFlags.Static | BindingFlags.Instance | BindingFlags.DeclaredOnly;
            FieldInfo info = typeof(UnityEditor.Tools).GetField("onToolChanged", flags);
            Delegate d = (Delegate)info.GetValue(null);
            Delegate.Remove(d, action);
            info.SetValue(null, d);

            MethodInfo method = typeof(_4ViewScene).GetMethod("OnToolChanged");
            if (d == null)
            {
                action = Delegate.CreateDelegate(info.FieldType, null, method);
                d = action;
            }
            else
            {
                action = Delegate.CreateDelegate(info.FieldType, null, method);
                d = Delegate.Combine(d, action);
            }
            info.SetValue(null, d);
            #endregion

            currentView = this;

            OBJ = GameObject.CreatePrimitive(PrimitiveType.Sphere).transform;

            computedCamera = 0;

            s_AuraShader = (EditorGUIUtility.LoadRequired("SceneView/SceneViewAura.shader") as Shader);

            CreateSceneCameraAndLights();

            #region Create Cameras or TOP, Right, Front, Normal View
            m_Rotation[0] = new AnimQuaternion(Quaternion.Euler(Vector3.zero));
            m_Rotation[1] = new AnimQuaternion(Quaternion.Euler(new Vector3(90, 0, 0)));
            m_Rotation[2] = new AnimQuaternion(Quaternion.Euler(Vector3.zero));
            m_Rotation[3] = new AnimQuaternion(Quaternion.Euler(new Vector3(0, 90, 0)));

            m_Position[0] = new AnimVector3(new Vector3(0, 0, 0));
            m_Position[1] = new AnimVector3(new Vector3(0, 10, 0));
            m_Position[2] = new AnimVector3(new Vector3(0, 0, -10));
            m_Position[3] = new AnimVector3(new Vector3(-10, 0, 0));


            for (int i = 0; i < 4; i++)
            {
                m_Size[i] = new AnimFloat(10);
                m_Ortho[i] = new AnimBool(true);
                m_lookedRotation[i] = true;
            }
            m_Ortho[2].value = false;
            m_lookedRotation[2] = false;
            #endregion

            svRot = new SceneViewRotation();
            svRot.Register(this);

            m_RectSelection = new RectSelection(this);

            grid = new SceneViewGrid();
            grid.Register(this);
        }

        public void OnDisable()
        {
            Delegate arg_23_0 = EditorApplication.modifierKeysChanged;
            //Debug.Log("Destroy SceneCamera + Lights");
            if (this.cam_main)
            {
                UnityEngine.Object.DestroyImmediate(this.cam_main.gameObject, true);
            }
            if (this.m_Light[0])
            {
                UnityEngine.Object.DestroyImmediate(this.m_Light[0].gameObject, true);
            }
            if (this.m_Light[1])
            {
                UnityEngine.Object.DestroyImmediate(this.m_Light[1].gameObject, true);
            }
            if (this.m_Light[2])
            {
                UnityEngine.Object.DestroyImmediate(this.m_Light[2].gameObject, true);
            }

            if (OBJ)
                UnityEngine.Object.DestroyImmediate(OBJ.gameObject, true);


            for (int i = 0; i < 4; i++)
            {
                if (sceneCameras[i])
                    UnityEngine.Object.DestroyImmediate(this.sceneCameras[i].gameObject, true);
            }

            /*if (_4ViewScene.s_MipColorsTexture)
            {
                UnityEngine.Object.DestroyImmediate(_4ViewScene.s_MipColorsTexture, true);
            }
            _4ViewScene.s_SceneViews.Remove(this);
            if (_4ViewScene.s_LastActiveSceneView == this)
            {
                if (_4ViewScene.s_SceneViews.Count > 0)
                {
                    _4ViewScene.s_LastActiveSceneView = (_4ViewScene.s_SceneViews[0] as _4ViewScene);
                }
                else
                {
                    _4ViewScene.s_LastActiveSceneView = null;
                }
            }
            this.CleanupEditorDragFunctions();*/
            //OnDisable();
        }

        public void Awake()
        {

        }

        public void OnSelectionChange()
        {
            Repaint();
        }

        private void DefaultHandles(Rect viewRect)
        {
            EditorGUI.BeginChangeCheck();
            bool flag = Event.current.GetTypeForControl(GUIUtility.hotControl) == EventType.MouseDrag;
            bool flag2 = Event.current.GetTypeForControl(GUIUtility.hotControl) == EventType.MouseUp;
            if (GUIUtility.hotControl == 0)
            {
                _4ViewScene.s_CurrentTool = ((!SceneViewMotion.viewToolActive) ? UnityEditor.Tools.current : Tool.View);
            }
            Tool tool = (Event.current.type != EventType.Repaint) ? _4ViewScene.s_CurrentTool : UnityEditor.Tools.current;
            switch (tool + 1)
            {
                case Tool.Rotate:
                    MoveTool.OnGUI(this, viewRect);
                    break;
                case Tool.Scale:
                    RotateTool.OnGUI(this);
                    break;
                case Tool.Rect:
                    ScaleTool.OnGUI(this);
                    break;
                case (Tool)5:
                    RectTool.OnGUI(this);
                    break;
            }
            if (EditorGUI.EndChangeCheck() && EditorApplication.isPlaying && flag)
            {
                //Physics2D.SetEditorDragMovement(true, Selection.gameObjects);
            }
            if (EditorApplication.isPlaying && flag2)
            {
                //Physics2D.SetEditorDragMovement(false, Selection.gameObjects);
            }
        }

        public void SetCameraCullingLayer(string[] Layers, int camera)
        {
            for (int i = 0; i < Layers.Length; i++)
                sceneCameras[camera].cullingMask |= 1 << LayerMask.NameToLayer(Layers[i]);
        }

        void DrawSceneCamera(int id)//EVENT.REPAINT
        {
            //SceneView Window Overlay
            computedCamera = id;
            RenderTexture.active = null;

            Rect r = GetCameraRect;
            float x = r.width;
            float y = r.height;
            if (id == 2)//Currently Main View 
            {
                gridParam = grid.PrepareGridRender(this.computed_camera, this.pivot, this.rotation, this.size, this.orthographic, true);

                Rect r2 = EditorGUIUtility.PointsToPixels(new Rect(5,17,x,y));
                //computedCamera = 2;
                HandleUtility.PushCamera(computed_camera);
                Handles.SetCamera(r2, sceneCameras[computedCamera]);
                //GUI.BeginGroup(new Rect(5f, 17 + y, x, y));

                if (SceneCameraRendersIntoRT())
                {
                    ReflectDllMethods.GetMethodInvokedVoid(Push, new object[] { r2, Vector2.zero, Vector2.zero, true });
                    pushedClip = true;
                }

                if (e.type == EventType.Repaint)
                {
                    ReflectDllMethods.GetMethodInvokedVoid(DrawCameraStep1, new object[] { r2, this.computed_camera, DrawCameraMode.Textured, gridParam });
                }

                PrepareCameraReplacementShader();
                ReflectDllMethods.GetMethodInvokedVoid(DrawCameraStep2, new object[] { computed_camera, DrawCameraMode.RealtimeAlbedo });
                //GUI.EndGroup();

                Graphics.SetRenderTarget(null);


                ReflectDllMethods.GetMethodInvokedVoid(Pop, null);
                GUI.DrawTexture(r, renderTextures[id], ScaleMode.StretchToFill, false);
                Graphics.SetRenderTarget(null);


                //GUI.BeginGroup(new Rect(5f, 17 + y, x, y));
                if (selectedCamera == computedCamera)
                {
                    //Handles.DrawCamera(EditorGUIUtility.PointsToPixels(new Rect(5f, 17 + y, x, y)), computed_camera);
                    bool sRGBWrite = GL.sRGBWrite;
                    GL.sRGBWrite = false;
                    Handles.SetCamera(r2, sceneCameras[computedCamera]);
                    this.HandleSelectionAndOnSceneGUI(new Rect(5f, 17 + y, x, y));
                    GL.sRGBWrite = sRGBWrite;
                }
                //GUI.EndGroup();

                Handles.SetCamera(r2, sceneCameras[computedCamera]);
                this.CallOnSceneGUI();
                DefaultHandles(new Rect(5, y + 17, x, y));
                if (id == 2)
                    this.RepaintGizmosThatAreRenderedOnTopOfSceneView(r2);
                HandleUtility.PopCamera(computed_camera);
                Graphics.SetRenderTarget(null);

            }
            else
            {
                GUI.DrawTexture(r, renderTextures[id], ScaleMode.StretchToFill, false);
                Graphics.SetRenderTarget(null);

            }

            GUI.BeginGroup(new Rect(0, 0, x+10, y+17));

            //Menu
            if (GUI.Button(new Rect(0, 0, 100, 17), "Mode"))
            {
                Debug.Log(" BUTTON " + id);
                cm.elementSize = new Vector2(100, 25);
                cm.ShowContexMenu(new Vector2(0, 17));
            }

            GUI.Button(new Rect(100, 0, 100, 17), "View");
            GUI.EndGroup();
        }

        void DrawMainCamera()//EVENT.REPAINT
        {
            //SceneView Window Overlay
            computedCamera = selectedCamera;
            RenderTexture.active = null;

            Rect r = GetCameraRect;
            float x = r.width;
            float y = r.height;
            if (selectedCamera == 2)//Currently Main View 
            {
                gridParam = grid.PrepareGridRender(this.computed_camera, this.pivot, this.rotation, this.size, this.orthographic, true);

                Rect r2 = EditorGUIUtility.PointsToPixels(new Rect(5, 17, x, y));
                //computedCamera = 2;
                HandleUtility.PushCamera(computed_camera);
                Handles.SetCamera(r2, sceneCameras[computedCamera]);
                //GUI.BeginGroup(new Rect(5f, 17 + y, x, y));

                if (SceneCameraRendersIntoRT())
                {
                    ReflectDllMethods.GetMethodInvokedVoid(Push, new object[] { r2, Vector2.zero, Vector2.zero, true });
                    pushedClip = true;
                }

                if (e.type == EventType.Repaint)
                    ReflectDllMethods.GetMethodInvokedVoid(DrawCameraStep1, new object[] { r2, this.computed_camera, DrawCameraMode.Textured, gridParam });

                PrepareCameraReplacementShader();
                ReflectDllMethods.GetMethodInvokedVoid(DrawCameraStep2, new object[] { computed_camera, DrawCameraMode.RealtimeAlbedo });
                //GUI.EndGroup();

                Graphics.SetRenderTarget(null);

                ReflectDllMethods.GetMethodInvokedVoid(Pop, null);
                GUI.DrawTexture(r, renderTextures[selectedCamera], ScaleMode.StretchToFill, false);
                Graphics.SetRenderTarget(null);

                //GUI.BeginGroup(new Rect(5f, 17 + y, x, y));
                if (selectedCamera == computedCamera)
                {
                    //Handles.DrawCamera(EditorGUIUtility.PointsToPixels(new Rect(5f, 17 + y, x, y)), computed_camera);
                    bool sRGBWrite = GL.sRGBWrite;
                    GL.sRGBWrite = false;
                    Handles.SetCamera(r2, sceneCameras[computedCamera]);
                    this.HandleSelectionAndOnSceneGUI(new Rect(5f, 17 + y, x, y));
                    GL.sRGBWrite = sRGBWrite;
                }
                //GUI.EndGroup();

                Handles.SetCamera(r2, sceneCameras[computedCamera]);
                this.CallOnSceneGUI();
                DefaultHandles(new Rect(5, y + 17, x, y));
                if (selectedCamera == 2)
                    this.RepaintGizmosThatAreRenderedOnTopOfSceneView(r2);
                HandleUtility.PopCamera(computed_camera);
                Graphics.SetRenderTarget(null);

            }
            else
            {
                GUI.DrawTexture(r, renderTextures[selectedCamera], ScaleMode.StretchToFill, false);
                Graphics.SetRenderTarget(null);
            }

            GUI.BeginGroup(new Rect(0, 0, x + 10, y + 17));

            //Menu
            if (GUI.Button(new Rect(0, 0, 100, 17), "Mode"))
            {
                Debug.Log(" BUTTON " + selectedCamera);
                cm.elementSize = new Vector2(100, 25);
                cm.ShowContexMenu(new Vector2(0, 17));
            }

            GUI.Button(new Rect(100, 0, 100, 17), "View");
            GUI.EndGroup();
        }

        private void SetupCamera(int selcamera)
        {

            if(selcamera==2)
                sceneCameras[selcamera].cullingMask = 1 << LayerMask.NameToLayer("UI");//Only UI

            if (this.m_RenderMode == DrawCameraMode.Overdraw)
            {
                this.sceneCameras[selcamera].backgroundColor = Color.black;
            }
            else
            {
                this.sceneCameras[selcamera].backgroundColor = kSceneViewBackground;// SceneRenderer.kSceneViewBackground;
            }

            this.ResetIfNaN(selcamera);
            this.sceneCameras[selcamera].transform.rotation = this.m_Rotation[selcamera].value;
            float num = this.m_Ortho[selcamera].Fade(90f, 0f);
            if (num > 3f)
            {
                this.sceneCameras[selcamera].orthographic = false;
                this.sceneCameras[selcamera].fieldOfView = this.GetVerticalFOV(num);
            }
            else
            {
                this.sceneCameras[selcamera].orthographic = true;
                this.sceneCameras[selcamera].orthographicSize = this.GetVerticalOrthoSize( sceneCameras[selcamera]);
            }
            this.sceneCameras[selcamera].transform.position = this.m_Position[selcamera].value + this.sceneCameras[selcamera].transform.rotation * new Vector3(0f, 0f, -this.cameraDistance);
            float num2 = Mathf.Max(1000f, 2000f * this.size);
            this.sceneCameras[selcamera].nearClipPlane = num2 * 5E-06f;
            this.sceneCameras[selcamera].farClipPlane = num2;
            this.sceneCameras[selcamera].renderingPath = _4ViewScene.GetSceneViewRenderingPath(sceneCameras[selcamera]);
            if (!this.CheckDrawModeForRenderingPath(this.m_RenderMode, sceneCameras[selcamera]))
            {
                this.m_RenderMode = DrawCameraMode.Textured;
            }

            this.m_Light[0].transform.position = this.sceneCameras[selcamera].transform.position;
            this.m_Light[0].transform.rotation = this.sceneCameras[selcamera].transform.rotation;


            if (this.m_ViewIsLockedToObject && Selection.gameObjects.Length > 0)
            {
                DraggingLockedState draggingLockedState = this.m_DraggingLockedState;
                if (draggingLockedState != DraggingLockedState.Dragging)
                {
                    if (draggingLockedState != DraggingLockedState.LookAt)
                    {
                        if (draggingLockedState == DraggingLockedState.NotDragging)
                        {
                            this.m_Position[selcamera].value = Selection.activeGameObject.transform.position;
                        }
                    }
                    else if (!this.m_Position[selcamera].value.Equals(Selection.activeGameObject.transform.position))
                    {
                        if (!EditorApplication.isPlaying)
                        {
                            this.m_Position[selcamera].target = Selection.activeGameObject.transform.position;
                        }
                        else
                        {
                            this.m_Position[selcamera].value = Selection.activeGameObject.transform.position;
                        }
                    }
                    else
                    {
                        this.m_DraggingLockedState = DraggingLockedState.NotDragging;
                    }
                }
            }
        }

        private void ResetIfNaN(int selcamera)
        {
            if (float.IsInfinity(this.m_Position[selcamera].value.x) || float.IsNaN(this.m_Position[selcamera].value.x))
            {
                this.m_Position[selcamera].value = Vector3.zero;
            }
            if (float.IsInfinity(this.m_Rotation[computedCamera].value.x) || float.IsNaN(this.m_Rotation[computedCamera].value.x))
            {
                this.m_Rotation[computedCamera].value = Quaternion.identity;
            }
        }

        /* private void HandleMouseCursor()
         {
             Event current = Event.current;
             if (GUIUtility.hotControl == 0)
             {
                 this.s_DraggingCursorIsCached = false;
             }
             Rect rect = new Rect(0f, 0f, base.position.width, base.position.height);
             if (!this.s_DraggingCursorIsCached)
             {
                 MouseCursor mouseCursor = MouseCursor.Arrow;
                 if (current.type == EventType.MouseMove || current.type == EventType.Repaint)
                 {
                     foreach (SceneView.CursorRect current2 in SceneView.s_MouseRects)
                     {
                         if (current2.rect.Contains(current.mousePosition))
                         {
                             mouseCursor = current2.cursor;
                             rect = current2.rect;
                         }
                     }
                     if (GUIUtility.hotControl != 0)
                     {
                         this.s_DraggingCursorIsCached = true;
                     }
                     if (mouseCursor != SceneView.s_LastCursor)
                     {
                         SceneView.s_LastCursor = mouseCursor;
                         InternalEditorUtility.ResetCursor();
                         base.Repaint();
                     }
                 }
             }
             if (current.type == EventType.Repaint && SceneView.s_LastCursor != MouseCursor.Arrow)
             {
                 EditorGUIUtility.AddCursorRect(rect, SceneView.s_LastCursor);
             }
         }*/

        public void UpdateRenderTextures(int i)
        {
            RenderTextureFormat renderTextureFormat = (!SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGBHalf)) ? RenderTextureFormat.ARGB32 : RenderTextureFormat.ARGBHalf;
            bool flag = QualitySettings.activeColorSpace == ColorSpace.Linear;
            int num = Mathf.Max(1, QualitySettings.antiAliasing);
            if (this.IsSceneCameraDeferred())
                num = 1;

            //for (int i = 0; i < 4; i++)
            {
                Rect rr = GetViewPixelRect(i, sizeOfScreenView.x, sizeOfScreenView.y);
                //Rect vr = GetViewRect(i, sizeOfScreenView.x, sizeOfScreenView.y);
                //sceneCameras[i].rect = vr;
                // Handles.SetCamera(rr, sceneCameras[i]);
                Rect cameraRect2 = ReflectDllMethods.GetMethodInvoked<Rect>(HandlesGetCameraRect, new object[] {rr});//"UnityEditor", "Handles", "GetCameraRect"
                //Debug.Log("CAMERA RECT{" + i + "}: " + cameraRect2.ToString()+"\t\t" + " PR " + rr.ToString());

                //sceneCameras[i].pixelRect = rr;

                //float verticalFOV = this.GetVerticalFOV(90f, sceneCameras[i]);
                //float fieldOfView = sceneCameras[i].fieldOfView;
                //sceneCameras[i].fieldOfView = verticalFOV;
                //Handles.ClearCamera(cameraRect, this.cam_main);
                //sceneCameras[i].fieldOfView = fieldOfView;


                if (this.renderTextures[i] != null)
                {
                    bool flag2 = this.renderTextures[i] != null && flag == this.renderTextures[i].sRGB;
                    if (this.renderTextures[i].format != renderTextureFormat || this.renderTextures[i].antiAliasing != num || !flag2)
                    {
                        UnityEngine.Object.DestroyImmediate(this.renderTextures[i]);
                        this.renderTextures[i] = null;
                    }
                }
                //Rect cameraRect2 = Handles.GetCameraRect(cameraRect);
                int num2 = (int)cameraRect2.width;
                int num3 = (int)cameraRect2.height;
                if (this.renderTextures[i] == null)
                {
                    this.renderTextures[i] = new RenderTexture(0, 0, 24, renderTextureFormat);
                    this.renderTextures[i].name = "SceneView RT";
                    this.renderTextures[i].antiAliasing = num;
                    this.renderTextures[i].hideFlags = HideFlags.HideAndDontSave;
                }
                if (this.renderTextures[i].width != num2 || this.renderTextures[i].height != num3)
                {
                    this.renderTextures[i].Release();
                    this.renderTextures[i].width = num2;
                    this.renderTextures[i].height = num3;
                }
                this.renderTextures[i].Create();

                sceneCameras[i].targetTexture = renderTextures[i];
            }
        }

        private void CreateSceneCameraAndLights()
        {
            //Debug.Log("CREATE CAMERA AND LIGHT");

            GameObject gameObject = EditorUtility.CreateGameObjectWithHideFlags("MassiveStudio_SceneCamera", HideFlags.HideAndDontSave, new Type[]
            {
                typeof(Camera),
            });

            gameObject.AddComponent<FlareLayer>();
            //gameObject.AddComponent<>();

            //gameObject.AddComponentInternal("FlareLayer");
            //gameObject.AddComponentInternal("HaloLayer");
            this.cam_main = gameObject.GetComponent<Camera>();
            this.cam_main.enabled = false;
            this.cam_main.cameraType = CameraType.SceneView;
            this.cam_main.orthographic = true;

            for (int i = 0; i < 3; i++)
            {
                GameObject gameObject2 = EditorUtility.CreateGameObjectWithHideFlags("MassiveStudio_SceneLight", HideFlags.HideAndDontSave, new Type[]
                {
                    typeof(Light)
                });
                this.m_Light[i] = gameObject2.GetComponent<Light>();
                this.m_Light[i].type = LightType.Directional;
                this.m_Light[i].intensity = 1f;
                this.m_Light[i].enabled = false;
            }
            this.m_Light[0].color = _4ViewScene.kSceneViewFrontLight;
            this.m_Light[1].color = _4ViewScene.kSceneViewUpLight - _4ViewScene.kSceneViewMidLight;
            this.m_Light[1].transform.LookAt(Vector3.down);
            this.m_Light[1].renderMode = LightRenderMode.ForceVertex;
            this.m_Light[2].color = _4ViewScene.kSceneViewDownLight - _4ViewScene.kSceneViewMidLight;
            this.m_Light[2].transform.LookAt(Vector3.up);
            this.m_Light[2].renderMode = LightRenderMode.ForceVertex;

            HandleUtility.handleMaterial.SetColor("_SkyColor", _4ViewScene.kSceneViewUpLight * 1.5f);
            HandleUtility.handleMaterial.SetColor("_GroundColor", _4ViewScene.kSceneViewDownLight * 1.5f);
            HandleUtility.handleMaterial.SetColor("_Color", _4ViewScene.kSceneViewFrontLight * 1.5f);



            for (int i = 0; i < 4; i++)
            {
                GameObject g = EditorUtility.CreateGameObjectWithHideFlags(string.Format("MassiveStudio_SceneCamera{0}", i), HideFlags.HideAndDontSave, new Type[]
                {
                typeof(Camera)
                });
                g.AddComponent<FlareLayer>();

                sceneCameras[i] = g.GetComponent<Camera>();
                sceneCameras[i].enabled = false;
                sceneCameras[i].cameraType = CameraType.SceneView;
                sceneCameras[i].orthographic = true;
                if (i != 2) {
                    sceneCameras[i].clearFlags = CameraClearFlags.SolidColor;
                    sceneCameras[i].backgroundColor = new Color32(60, 60, 60, 255);
                }
                /*
                switch (i)
                {
                    case 0:
                        g.transform.position = new Vector3(-10, 0, 0);
                        g.transform.eulerAngles = Vector3.zero;
                        break;
                    case 1:
                        g.transform.position =  new Vector3(0,10,0);
                        g.transform.eulerAngles = new Vector3(90, 0, 0);
                        break;
                    case 2:
                        g.transform.position = new Vector3(-10, 0, 0);
                        g.transform.eulerAngles = new Vector3(0, 90, 0);
                        break;
                    case 3:
                        g.transform.position = new Vector3(0, 0, -10);
                        g.transform.eulerAngles = Vector3.zero;
                        break;
                }*/

            }
        }

        private void DoClearCamera(Rect cameraRect, Camera cam)
        {
            float verticalFOV = this.GetVerticalFOV(90f);
            float fieldOfView = cam.fieldOfView;
            cam.fieldOfView = verticalFOV;
            Handles.ClearCamera(cameraRect, cam);
            cam.fieldOfView = fieldOfView;
        }

        private void PrepareCameraReplacementShader()
        {
            if (Event.current.type == EventType.Repaint)
            {
                ReflectDllMethods.GetMethodInvokedVoid("UnityEditor", "Handles", "SetSceneViewColors", new object[]{ _4ViewScene.kSceneViewWire, _4ViewScene.kSceneViewWireOverlay, _4ViewScene.kSceneViewSelectedOutline, _4ViewScene.kSceneViewSelectedWire });
                //Handles.SetSceneViewColors(_4ViewScene.kSceneViewWire, _4ViewScene.kSceneViewWireOverlay, _4ViewScene.kSceneViewSelectedOutline, _4ViewScene.kSceneViewSelectedWire);
                if (this.m_RenderMode == DrawCameraMode.Overdraw)
                {
                    if (!_4ViewScene.s_ShowOverdrawShader)
                    {
                        _4ViewScene.s_ShowOverdrawShader = (EditorGUIUtility.LoadRequired("SceneView/SceneViewShowOverdraw.shader") as Shader);
                    }
                    this.computed_camera.SetReplacementShader(_4ViewScene.s_ShowOverdrawShader, "RenderType");
                }
                else if (this.m_RenderMode == DrawCameraMode.Mipmaps)
                {
                    if (!_4ViewScene.s_ShowMipsShader)
                    {
                        _4ViewScene.s_ShowMipsShader = (EditorGUIUtility.LoadRequired("SceneView/SceneViewShowMips.shader") as Shader);
                    }
                    /*if (_4ViewScene.s_ShowMipsShader != null && _4ViewScene.s_ShowMipsShader.isSupported)
                    {
                        _4ViewScene.CreateMipColorsTexture();
                        this.computed_camera.SetReplacementShader(_4ViewScene.s_ShowMipsShader, "RenderType");
                    }*/
                }
            }
        }
        /*
        private static void CreateMipColorsTexture()
        {
            if (!SceneView.s_MipColorsTexture)
            {
                SceneView.s_MipColorsTexture = new Texture2D(32, 32, TextureFormat.RGBA32, true);
                SceneView.s_MipColorsTexture.hideFlags = HideFlags.HideAndDontSave;
                Color[] array = new Color[]
                {
            new Color(0f, 0f, 1f, 0.8f),
            new Color(0f, 0.5f, 1f, 0.4f),
            new Color(1f, 1f, 1f, 0f),
            new Color(1f, 0.7f, 0f, 0.2f),
            new Color(1f, 0.3f, 0f, 0.6f),
            new Color(1f, 0f, 0f, 0.8f)
                };
                int num = Mathf.Min(6, SceneView.s_MipColorsTexture.mipmapCount);
                for (int i = 0; i < num; i++)
                {
                    int num2 = Mathf.Max(SceneView.s_MipColorsTexture.width >> i, 1);
                    int num3 = Mathf.Max(SceneView.s_MipColorsTexture.height >> i, 1);
                    Color[] array2 = new Color[num2 * num3];
                    for (int j = 0; j < array2.Length; j++)
                    {
                        array2[j] = array[i];
                    }
                    SceneView.s_MipColorsTexture.SetPixels(array2, i);
                }
                SceneView.s_MipColorsTexture.filterMode = FilterMode.Trilinear;
                SceneView.s_MipColorsTexture.Apply(false);
                Shader.SetGlobalTexture("_SceneViewMipcolorsTexture", SceneView.s_MipColorsTexture);
            }
        }*/

        private bool SceneCameraRendersIntoRT()
        {
            return this.computed_camera.targetTexture != null;
        }

        internal float GetVerticalFOV(float aspectNeutralFOV)
        {
            float f = Mathf.Tan(aspectNeutralFOV * 0.5f * 0.0174532924f) * 0.707106769f / Mathf.Sqrt(this.cam_main.aspect);
            return Mathf.Atan(f) * 2f * 57.29578f;
        }

        internal float GetVerticalFOV(float aspectNeutralFOV, Camera cam)
        {
            float f = Mathf.Tan(aspectNeutralFOV * 0.5f * 0.0174532924f) * 0.707106769f / Mathf.Sqrt(cam.aspect);
            return Mathf.Atan(f) * 2f * 57.29578f;
        }

        internal bool IsSceneCameraDeferred()
        {
            return !(this.cam_main == null) && (this.cam_main.actualRenderingPath == RenderingPath.DeferredLighting || this.cam_main.actualRenderingPath == RenderingPath.DeferredShading);
        }

        private void PrepareCameraTargetTexture(Rect cameraRect)
        {
            bool hdr = false;// this.SceneViewIsRenderingHDR();
            this.CreateCameraTargetTexture(cameraRect, hdr);
            this.cam_main.targetTexture = this.m_SceneTargetTexture;
        }

        private void CreateCameraTargetTexture(Rect cameraRect, bool hdr)
        {
            bool flag = QualitySettings.activeColorSpace == ColorSpace.Linear;
            int num = Mathf.Max(1, QualitySettings.antiAliasing);
            if (this.IsSceneCameraDeferred())
            {
                num = 1;
            }
            RenderTextureFormat renderTextureFormat = (!hdr || !SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGBHalf)) ? RenderTextureFormat.ARGB32 : RenderTextureFormat.ARGBHalf;
            if (this.m_SceneTargetTexture != null)
            {
                bool flag2 = this.m_SceneTargetTexture != null && flag == this.m_SceneTargetTexture.sRGB;
                if (this.m_SceneTargetTexture.format != renderTextureFormat || this.m_SceneTargetTexture.antiAliasing != num || !flag2)
                {
                    UnityEngine.Object.DestroyImmediate(this.m_SceneTargetTexture);
                    this.m_SceneTargetTexture = null;
                }
            }

            Rect cameraRect2 = ReflectDllMethods.GetMethodInvoked<Rect>(HandlesGetCameraRect, new object[] { cameraRect });// "UnityEditor", "Handles", "GetCameraRect"
            //Rect cameraRect2 = Handles.GetCameraRect(cameraRect);
            int num2 = (int)cameraRect2.width;
            int num3 = (int)cameraRect2.height;
            if (this.m_SceneTargetTexture == null)
            {
                this.m_SceneTargetTexture = new RenderTexture(0, 0, 24, renderTextureFormat);
                this.m_SceneTargetTexture.name = "SceneView RT";
                this.m_SceneTargetTexture.antiAliasing = num;
                this.m_SceneTargetTexture.hideFlags = HideFlags.HideAndDontSave;
            }
            if (this.m_SceneTargetTexture.width != num2 || this.m_SceneTargetTexture.height != num3)
            {
                this.m_SceneTargetTexture.Release();
                this.m_SceneTargetTexture.width = num2;
                this.m_SceneTargetTexture.height = num3;
            }
            this.m_SceneTargetTexture.Create();
        }

        private void HandleSelectionAndOnSceneGUI(Rect rect)
        {
            UnityEngine.Profiling.Profiler.BeginSample("4View", this);
            this.m_RectSelection.OnGUI(rect);
            UnityEngine.Profiling.Profiler.EndSample();
        }

        private void HandleDragging()
        {
            Event current = Event.current;
            EventType type = current.type;
            if (type != EventType.DragPerform && type != EventType.DragUpdated)
            {
                if (type == EventType.DragExited)
                {
                    this.CallEditorDragFunctions();
                    this.CleanupEditorDragFunctions();
                }
            }
            else
            {
                this.CallEditorDragFunctions();
                if (current.type != EventType.Used)
                {
                    bool flag = current.type == EventType.DragPerform;
                    if (DragAndDrop.visualMode != DragAndDropVisualMode.Copy)
                    {
                        GameObject dropUpon = HandleUtility.PickGameObject(Event.current.mousePosition, true);
                        DragAndDrop.visualMode = InternalEditorUtility.SceneViewDrag(dropUpon, this.pivot, Event.current.mousePosition, flag);
                    }
                    if (flag && DragAndDrop.visualMode != DragAndDropVisualMode.None)
                    {
                        DragAndDrop.AcceptDrag();
                        current.Use();
                        GUIUtility.ExitGUI();
                    }
                    current.Use();
                }
            }
        }

        private void CallEditorDragFunctions()
        {
            Event current = Event.current;

            Type t = ReflectDllMethods.GetType("UnityEditor.SpriteUtility");
            if (t == null)
                Debug.LogError("TYP NULL");
            MethodInfo info = t.GetMethod("OnSceneDrag", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
            if (info == null)
                Debug.LogError("METHOD NULL");

            ReflectDllMethods.GetMethodInvokedVoid(info, new object[] { this});
            //SpriteUtility.OnSceneDrag(this);
            if (current.type != EventType.Used)
            {
                if (DragAndDrop.objectReferences.Length != 0)
                {
                    if (this.m_DragEditorCache == null)
                    {
                        this.m_DragEditorCache = Activator.CreateInstance(ReflectDllMethods.GetType("UnityEditor.EditorCache"), 4);//. new EditorCache(EditorFeatures.OnSceneDrag);
                    }
                    UnityEngine.Object[] objectReferences = DragAndDrop.objectReferences;
                    for (int i = 0; i < objectReferences.Length; i++)
                    {
                        UnityEngine.Object @object = objectReferences[i];
                        if (!(@object == null))
                        {

                            object editorWrapper = ReflectDllMethods.GetType("UnityEditor.EditorCache").GetProperty("Item").GetValue(this.m_DragEditorCache, new object[] { @object });
                            //EditorWrapper editorWrapper = this.m_DragEditorCache[@object];

                            if (editorWrapper != null)
                            {
                                //editorWrapper.OnSceneDrag(this);
                                ReflectDllMethods.GetMethodInvokedVoid(ReflectDllMethods.GetType("UnityEditor.EditorWrapper").GetMethod("OnSceneDrag", BindingFlags.Public), editorWrapper, new object[] { this });
                            }
                            if (current.type == EventType.Used)
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }

        private void CleanupEditorDragFunctions()
        {
            if (this.m_DragEditorCache != null)
            {
                ReflectDllMethods.GetMethodInvokedVoid(ReflectDllMethods.GetType("UnityEditor.EditorCache").GetMethod("Dispose", BindingFlags.Public), this.m_DragEditorCache);
                //this.m_DragEditorCache.Dispose();
            }
            this.m_DragEditorCache = null;
        }

        private void CallOnSceneGUI()
        {
            Editor[] activeEditors = this.GetActiveEditors();
            for (int i = 0; i < activeEditors.Length; i++)
            {
                Editor editor = activeEditors[i];
                bool allowed = ReflectDllMethods.GetMethodInvoked<bool>(IsGizmosAllowedForObject, new object[] { editor.target });//"UnityEditor", "EditorGUIUtility", "IsGizmosAllowedForObject"
                if (allowed)//EditorGUIUtility.IsGizmosAllowedForObject(editor.target)
                {
                    MethodInfo method = editor.GetType().GetMethod("OnSceneGUI", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
                    if (method != null)
                    {

                        ReflectDllMethods.SetField(ReflectDllMethods.GetType("UnityEditor.Editor"), null, "m_AllowMultiObjectAccess", true);

                        for (int j = 0; j < editor.targets.Length; j++)
                        {
                            this.ResetOnSceneGUIState();
                            //editor.referenceTargetIndex = j;
                            //ReflectDllMethods.SetField(ReflectDllMethods.GetType("UnityEditor.Editor"), editor, "referenceTargetIndex",j);
                            EditorGUI.BeginChangeCheck();
                            bool val = (bool)ReflectDllMethods.GetField(typeof(UnityEditor.Editor), null, "m_AllowMultiObjectAccess");
                            ReflectDllMethods.SetField(typeof(UnityEditor.Editor), null, "m_AllowMultiObjectAccess", !val);
                            //Editor.m_AllowMultiObjectAccess = !editor.canEditMultipleObjects;
                            //Debug.Log("INVOKE ONSCENEGUI  at " + editor.target.GetType().ToString());
                            method.Invoke(editor, null);
                            ReflectDllMethods.SetField(ReflectDllMethods.GetType("UnityEditor.Editor"), null, "m_AllowMultiObjectAccess", true);

                            if (EditorGUI.EndChangeCheck())
                            {
                                editor.serializedObject.SetIsDifferentCacheDirty();
                            }
                        }
                        this.ResetOnSceneGUIState();
                    }
                }
            }
            if (_4ViewScene.onSceneGUIDelegate != null)
            {
                _4ViewScene.onSceneGUIDelegate(this);
                this.ResetOnSceneGUIState();
            }
        }

        private void ResetOnSceneGUIState()
        {
            ReflectDllMethods.GetMethodInvokedVoid("UnityEditor", "Handles", "ClearHandles",null);
            //Handles.ClearHandles();
            ReflectDllMethods.SetField(ReflectDllMethods.GetType("UnityEditor.HandleUtility"), null, "s_CustomPickDistance", 5f);
            //HandleUtility.s_CustomPickDistance = 5f;
            ReflectDllMethods.GetMethodInvokedVoid("UnityEditor", "EditorGUIUtility", "ResetGUIState", null);
            //EditorGUIUtility.ResetGUIState();
            GUI.color = Color.white;
        }

        public static void OnToolChanged(Tool from, Tool to)
        {
            //Debug.LogError("TOOL CHANGED");
            currentView.Repaint();
        }

        public void OnDestroy()
        {
            BindingFlags flags = BindingFlags.Public |
            BindingFlags.NonPublic |
            BindingFlags.Static |
            BindingFlags.Instance |
            BindingFlags.DeclaredOnly;

            FieldInfo info = typeof(UnityEditor.Tools).GetField("onToolChanged", flags);
            Delegate d = (Delegate)info.GetValue(null);
            Delegate.Remove(d, action);
            info.SetValue(null, d);
        }

        #region DEFAULTS

        internal bool CheckDrawModeForRenderingPath(DrawCameraMode mode, Camera c)
        {
            RenderingPath actualRenderingPath = c.actualRenderingPath;
            return (mode != DrawCameraMode.DeferredDiffuse && mode != DrawCameraMode.DeferredSpecular && mode != DrawCameraMode.DeferredSmoothness && mode != DrawCameraMode.DeferredNormal) || actualRenderingPath == RenderingPath.DeferredShading;
        }

        internal static RenderingPath GetSceneViewRenderingPath(Camera c)
        {
            Camera mainCamera = c;
            RenderingPath result;
            if (mainCamera != null)
            {
                result = mainCamera.renderingPath;
            }
            else
            {
                result = RenderingPath.UsePlayerSettings;
            }
            return result;
        }

        internal float GetVerticalOrthoSize(Camera c)
        {
            return this.size * 0.707106769f / Mathf.Sqrt(c.aspect);
        }

        public void LookAt(Vector3 pos)
        {
            this.FixNegativeSize();
            this.m_Position[computedCamera].target = pos;
        }

        public void LookAt(Vector3 pos, Quaternion rot)
        {
            this.FixNegativeSize();
            this.m_Position[computedCamera].target = pos;
            this.m_Rotation[computedCamera].target = rot;
            this.svRot.UpdateGizmoLabel(this, rot * Vector3.forward, this.m_Ortho[computedCamera].target);
        }

        public void LookAtDirect(Vector3 pos, Quaternion rot)
        {
            this.FixNegativeSize();
            this.m_Position[computedCamera].value = pos;
            this.m_Rotation[computedCamera].value = rot;
            this.svRot.UpdateGizmoLabel(this, rot * Vector3.forward, this.m_Ortho[computedCamera].target);
        }

        public void LookAt(Vector3 pos, Quaternion rot, float newSize)
        {
            this.FixNegativeSize();
            this.m_Position[computedCamera].target = pos;
            this.m_Rotation[computedCamera].target = rot;
            this.m_Size[computedCamera].target = Mathf.Abs(newSize);
            this.svRot.UpdateGizmoLabel(this, rot * Vector3.forward, this.m_Ortho[computedCamera].target);
        }

        public void LookAtDirect(Vector3 pos, Quaternion rot, float newSize)
        {
            this.FixNegativeSize();
            this.m_Position[computedCamera].value = pos;
            this.m_Rotation[computedCamera].value = rot;
            this.m_Size[computedCamera].value = Mathf.Abs(newSize);
            this.svRot.UpdateGizmoLabel(this, rot * Vector3.forward, this.m_Ortho[computedCamera].target);
        }

        public void LookAt(Vector3 pos, Quaternion rot, float newSize, bool ortho)
        {
            this.LookAt(pos, rot, newSize, ortho, false);
        }

        public void LookAt(Vector3 pos, Quaternion rot, float newSize, bool ortho, bool instant)
        {
            this.FixNegativeSize();
            if (instant)
            {
                this.m_Position[computedCamera].value = pos;
                this.m_Rotation[computedCamera].value = rot;
                this.m_Size[computedCamera].value = Mathf.Abs(newSize);
                this.m_Ortho[computedCamera].value = ortho;
            }
            else
            {
                this.m_Position[computedCamera].target = pos;
                this.m_Rotation[computedCamera].target = rot;
                this.m_Size[computedCamera].target = Mathf.Abs(newSize);
                this.m_Ortho[computedCamera].target = ortho;
            }
            this.svRot.UpdateGizmoLabel(this, rot * Vector3.forward, this.m_Ortho[computedCamera].target);
        }

        public void FixNegativeSize()
        {
            float num = 90f;
            if (this.size < 0f)
            {
                float num2 = this.size / Mathf.Tan(num * 0.5f * 0.0174532924f);
                Vector3 a = this.m_Position[computedCamera].value + this.rotation * new Vector3(0f, 0f, -num2);
                this.size = -this.size;
                num2 = this.size / Mathf.Tan(num * 0.5f * 0.0174532924f);
                this.m_Position[computedCamera].value = a + this.rotation * new Vector3(0f, 0f, num2);
            }
        }

        private void On2DModeChange()
        {
            if (this.m_2DMode)
            {
                this.lastSceneViewRotation = this.rotation;
                this.m_LastSceneViewOrtho = this.orthographic;
                this.LookAt(this.pivot, Quaternion.identity, this.size, true);
                if (UnityEditor.Tools.current == Tool.Move)
                {
                    UnityEditor.Tools.current = Tool.Rect;
                }
            }
            else
            {
                this.LookAt(this.pivot, this.lastSceneViewRotation, this.size, this.m_LastSceneViewOrtho);
                if (UnityEditor.Tools.current == Tool.Rect)
                {
                    UnityEditor.Tools.current = Tool.Move;
                }
            }
            /*HandleUtility.ignoreRaySnapObjects = null;
            UnityEditor.Tools.vertexDragging = false;
            UnityEditor.Tools.handleOffset = Vector3.zero;*/
        }

        public bool FrameSelected()
        {
            return this.FrameSelected(false);
        }

        private Editor[] GetActiveEditors()
        {
            return ActiveEditorTracker.sharedTracker.activeEditors;
        }

        public bool FrameSelected(bool lockView)
        {
            this.viewIsLockedToObject = lockView;
            this.FixNegativeSize();
            Bounds bounds = InternalEditorUtility.CalculateSelectionBounds(false, UnityEditor.Tools.pivotMode == PivotMode.Pivot);
            Editor[] activeEditors = this.GetActiveEditors();
            for (int i = 0; i < activeEditors.Length; i++)
            {
                Editor editor = activeEditors[i];
                MethodInfo method = editor.GetType().GetMethod("HasFrameBounds", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
                if (method != null)
                {
                    object obj = method.Invoke(editor, null);
                    if (obj is bool && (bool)obj)
                    {
                        MethodInfo method2 = editor.GetType().GetMethod("OnGetFrameBounds", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
                        if (method2 != null)
                        {
                            object obj2 = method2.Invoke(editor, null);
                            if (obj2 is Bounds)
                            {
                                bounds = (Bounds)obj2;
                            }
                        }
                    }
                }
            }
            return this.Frame(bounds);
        }

        internal bool Frame(Bounds bounds)
        {
            float num = bounds.extents.magnitude * 1.5f;
            bool result;
            if (num == float.PositiveInfinity)
            {
                result = false;
            }
            else
            {
                if (num == 0f)
                {
                    num = 10f;
                }
                this.LookAt(bounds.center, this.m_Rotation[computedCamera].target, num * 2.2f, this.m_Ortho[computedCamera].value, EditorApplication.isPlaying);
                result = true;
            }
            return result;
        }
        #endregion
    }
}


#if false
if (false && selectedCamera == 1)
                {
                    sceneCameras[1].rect = new Rect(0, 0, 1, 1);

                    Debug.Log("RECT AFTER CHANGE: " + sceneCameras[computedCamera].rect.ToString());
                    Debug.LogWarning("PRE: " + sceneCameras[computedCamera].pixelRect.ToString());


                    Graphics.SetRenderTarget(null);
                    //sceneCameras[computedCamera].pixelRect = EditorGUIUtility.PointsToPixels(new Rect(x, 0, x, y));
                    HandleUtility.PushCamera(computed_camera);
                    /*
                    //computed_camera.rect = new Rect(0.5f, 0f, 0.5f, 0.5f);
                    Rect rr = GetViewRect(computedCamera, (int)sizeOfScreenView.x, (int)sizeOfScreenView.y);
                    Rect rp = GetViewPixelRect(computedCamera, (int)sizeOfScreenView.x, (int)sizeOfScreenView.y);
                    Handles.SetCamera(new Rect((int)sizeOfScreenView.x+ verschiebung.x, (int)sizeOfScreenView.y + verschiebung.y, (int)sizeOfScreenView.x, (int)sizeOfScreenView.y), computed_camera);
                    sceneCameras[1].rect = new Rect(0, 0, 1, 1);
                   // sceneCameras[1].pixelRect = rp;

                    GUI.Box(rr, sceneCameras[computedCamera].rect.ToString() + "\n" + sceneCameras[computedCamera].pixelRect.ToString());

                    /* GUI.color = Color.green;
                     GUI.Box(rr, "");
                     GUI.color = Color.white;*/

                    /*

                    //GUILayout.BeginArea(new Rect(sizeOfScreenView.x, 0, sizeOfScreenView.x, sizeOfScreenView.y));

                    //sceneCameras[computedCamera].rect = GetViewRect(computedCamera, (int)x, (int)y);

                    //Handles.SetCamera(new Rect(0, 0, sizeOfScreenView.x, sizeOfScreenView.y), computed_camera);

                    Debug.Log("RE: " + sceneCameras[computedCamera].rect.ToString());
                    Debug.LogWarning("PRE: " + sceneCameras[computedCamera].pixelRect.ToString() + " : " + rp.ToString());

                    //sceneCameras[computedCamera].rect = new Rect(0.5f, 0, 1, 1);
                    //sceneCameras[computedCamera].pixelRect = rp;
                    */
                    /* GUI.color = Color.blue;
                     GUI.Box(new Rect(0, 0, rr.width, rr.height), "");
                     GUI.color = Color.white;*/

                    //Handles.SetCamera(rp, sceneCameras[computedCamera]);
                    /*Handles.BeginGUI();
                    Handles.Label(sceneCameras[computedCamera].ViewportToWorldPoint(new Vector3(0.1f, 0.1f)), "START");
                    Handles.Label(sceneCameras[computedCamera].ViewportToWorldPoint(new Vector3(0.5f, 0.5f)), "TEST");
                    Handles.Label(sceneCameras[computedCamera].ViewportToWorldPoint(new Vector3(0.9f, 0.9f)), "END");
                    //Handles.Label(sceneCameras[computedCamera].ViewportToWorldPoint(new Vector3(1.9f, 1.9f)), "END2");
                    Handles.EndGUI();*/
                    Handles.SetCamera(sceneCameras[1]);//new Rect(x, 0, x, y),

                    Handles.BeginGUI();
                    Handles.Label(sceneCameras[computedCamera].ViewportToWorldPoint(new Vector3(0.1f, 0.1f)), "SPS");
                    Handles.Label(sceneCameras[computedCamera].ViewportToWorldPoint(new Vector3(0.9f, 0.9f)), "SPE");
                    Handles.Label(sceneCameras[computedCamera].ViewportToWorldPoint(new Vector3(0.5f, 0.5f)), "SPM");
                    Debug.Log("SCREEN POINT OF LABEL: " + sceneCameras[computedCamera].ViewportToScreenPoint(new Vector3(0.1f, 0.1f)).ToString() + "\n" + sceneCameras[computedCamera].ViewportToScreenPoint(new Vector3(0.9f, 0.9f)).ToString() + "\n" + sceneCameras[computedCamera].pixelWidth + ":" + sceneCameras[computedCamera].pixelHeight);
                    Handles.EndGUI();

                    //DefaultHandles(new Rect(x, y, sizeOfScreenView.x, sizeOfScreenView.y));
                    //GUILayout.EndArea();

                    HandleUtility.PopCamera(computed_camera);
                }

if (false)
                {
                    Handles.BeginGUI();
                    computedCamera = 1;

                    //Debug.Log("CAMERA RECT BEFORE " + sceneCameras[computedCamera].pixelRect.ToString());
                    Rect rr = GetViewPixelRect(computedCamera, (int)sizeOfScreenView.x, (int)sizeOfScreenView.y);

                    Rect or = sceneCameras[computedCamera].pixelRect;
                    sceneCameras[computedCamera].pixelRect = rr;
                    // Handles l = new Handles();
                    // Debug.Log("CUR CAM: "+ l.currentCamera.name);

                    RenderTexture.active = null;
                    Graphics.SetRenderTarget(null);

                    //GUILayout.BeginArea(new Rect(0,0, sizeOfScreenView.x, sizeOfScreenView.y));

                    Debug.Log("TS: " + sceneCameras[computedCamera].targetTexture.width+"/"+ sceneCameras[computedCamera].targetTexture.height +"  ISN? " +(sceneCameras[computedCamera].targetTexture==null?"NULL":"NOTNULL"));

                    sceneCameras[computedCamera].pixelRect = rr;
                    sceneCameras[computedCamera].rect = GetViewRect(selectedCamera, (int)sizeOfScreenView.x, (int)sizeOfScreenView.y);
                    //Debug.Log("DRAW RECT " + rr.ToString());

                    Handles.SetCamera(new Rect(0,0,x,y), sceneCameras[computedCamera]);
                    sceneCameras[computedCamera].rect = GetViewRect(selectedCamera, (int)sizeOfScreenView.x, (int)sizeOfScreenView.y);

                    //GUI.Box(new Rect(200, 0, 200, 50), "PR: " + sceneCameras[computedCamera].pixelRect.ToString());

                    //Debug.Log("CAMERA RECT AFTER " + sceneCameras[computedCamera].pixelRect.ToString());
                    //Debug.Log("PIXEL RECT: " + sceneCameras[computedCamera].pixelRect.ToString());

                    //sceneCameras[0].rect = new Rect(0, 0, 1, 1);
                    //Debug.Log("VIEW: " + sceneCameras[computedCamera].rect.ToString());
                    /* try
                     {
                         Handles.BeginGUI();
                         Handles.Label(sceneCameras[computedCamera].ViewportToWorldPoint(new Vector3(0.5f, 0.5f)), "TEST");
                         Handles.EndGUI();
                     }
                     catch(Exception e)
                     {
                         Debug.LogError("LABEL DRAW ERROR " + e.Message);
                     }*/
                    Handles.SetCamera(new Rect(x, 0, x, y), computed_camera);
                    DefaultHandles(GetViewRect(selectedCamera, (int)sizeOfScreenView.x, (int)sizeOfScreenView.y));

                    //Handles.SetCamera(sceneCameras[computedCamera]);
                    //GUILayout.EndArea();
                    //DefaultHandles(new Rect(0, y + 17, x, y));


                    /*Vector3 start = sceneCameras[computedCamera].ViewportToScreenPoint(Vector3.zero);
                    Vector3 end = sceneCameras[computedCamera].ViewportToScreenPoint(Vector3.one);

                    Debug.Log("SCEEN: " + start.ToString() + "  :  " + end.ToString());

                    Handles.SetCamera(new Rect(0, 0, end.x, end.y), sceneCameras[1]);

                    start = sceneCameras[1].ViewportToScreenPoint(Vector3.zero);
                    end = sceneCameras[computedCamera].ViewportToScreenPoint(Vector3.one);

                    Vector3[] pp = new Vector3[4];
                    sceneCameras[1].CalculateFrustumCorners(new Rect(0, 0, 1, 1), 0, Camera.MonoOrStereoscopicEye.Mono, pp);

                    Debug.Log("C SCEEN: " + start.ToString() + "  :  " + end.ToString());*/
                    // sceneCameras[1].
                    //DefaultHandles(new Rect((Vector2)start, (Vector2)(end-start)));
                    //OBJ.position= sceneCameras[1].ViewportToWorldPoint(Vector3.zero);
                    Debug.Log("RE: " + sceneCameras[computedCamera].rect.ToString());
                    sceneCameras[computedCamera].pixelRect = or;
                    Handles.EndGUI();
                }

#endif

/*
computedCamera = 0;
HandleUtility.PushCamera(computed_camera);
sceneCameras[computedCamera].targetTexture = null;
sceneCameras[computedCamera].rect = new Rect(0, 0, 1, 1);
if (selectedCamera == computedCamera)
{
    Handles.SetCamera(EditorGUIUtility.PointsToPixels(new Rect(5, 17, x, y)), sceneCameras[computedCamera]);//new Rect(x, position.height, x, y),
    GUI.BeginGroup(new Rect(5, 17, x, y));
    bool sRGBWrite = GL.sRGBWrite;
    GL.sRGBWrite = false;
    this.HandleSelectionAndOnSceneGUI(new Rect(5, 17, x, y));
    GL.sRGBWrite = sRGBWrite;
    GUI.EndGroup();
}
Handles.SetCamera(EditorGUIUtility.PointsToPixels(new Rect(5, 17, x, y)), sceneCameras[computedCamera]);//new Rect(x, position.height, x, y),
this.CallOnSceneGUI();
DefaultHandles(new Rect(5,17, x, y));

HandleUtility.PopCamera(computed_camera);
Graphics.SetRenderTarget(null);
sceneCameras[computedCamera].rect = new Rect(0, 0, 1, 1);
*/

/*
                computedCamera = 1;
                HandleUtility.PushCamera(computed_camera);
                sceneCameras[computedCamera].targetTexture = null;
                sceneCameras[computedCamera].rect = new Rect(0, 0, 1, 1);
                //sceneCameras[computedCamera].pixelRect = new Rect(x, position.height, x, y);
                //Debug.Log("PR: 1  b:" + sceneCameras[1].pixelRect.ToString());
                Handles.SetCamera(EditorGUIUtility.PointsToPixels(new Rect(15+x, 17, x, y)), sceneCameras[computedCamera]);//new Rect(x, position.height, x, y),
                if (selectedCamera == computedCamera)
                {
                    GUI.BeginGroup(new Rect(15+x,17, x, y));
                    bool sRGBWrite = GL.sRGBWrite;
                    GL.sRGBWrite = false;
                    this.HandleSelectionAndOnSceneGUI(new Rect(15+x, 17, x, y));
                    GL.sRGBWrite = sRGBWrite;
                    GUI.EndGroup();
                }
                Handles.SetCamera(EditorGUIUtility.PointsToPixels(new Rect(15+x, 17, x, y)), sceneCameras[computedCamera]);//new Rect(x, position.height, x, y),
                this.CallOnSceneGUI();
                DefaultHandles(new Rect(15+x, 17, x, y));
                //this.RepaintGizmosThatAreRenderedOnTopOfSceneView(new Rect(x, y + 17, x, y - 17));
                //Handles.DrawCamera(new Rect(position.width / 2f, position.width / 2f, position.width / 2f, position.height / 2f), sceneCameras[computedCamera]);
                //Debug.Log("PR: 1  " + sceneCameras[1].pixelRect.ToString());
                HandleUtility.PopCamera(computed_camera);
                Graphics.SetRenderTarget(null);
                sceneCameras[computedCamera].rect = new Rect(0, 0, 1, 1);



                computedCamera = 3;
                HandleUtility.PushCamera(computed_camera);
                sceneCameras[computedCamera].targetTexture = null;
                sceneCameras[computedCamera].rect = new Rect(0, 0, 1, 1);
                Handles.SetCamera(EditorGUIUtility.PointsToPixels(new Rect(5+x, 17+y, x, y)), sceneCameras[computedCamera]);//new Rect(x, position.height, x, y),
                if (selectedCamera == computedCamera)
                {
                    GUI.BeginGroup(new Rect(5+x, 17+y, x, y));
                    bool sRGBWrite = GL.sRGBWrite;
                    GL.sRGBWrite = false;
                    this.HandleSelectionAndOnSceneGUI(new Rect(5+x,y, 17+x, y));
                    GL.sRGBWrite = sRGBWrite;
                    GUI.EndGroup();
                }

                Handles.SetCamera(EditorGUIUtility.PointsToPixels(new Rect(5+x, 17+y, x, y)), sceneCameras[computedCamera]);//new Rect(x, position.height, x, y),
                this.CallOnSceneGUI();
                DefaultHandles(new Rect(5+x, 17+y, x, y));
                HandleUtility.PopCamera(computed_camera);
                Graphics.SetRenderTarget(null);
                sceneCameras[computedCamera].rect = new Rect(0, 0, 1, 1);
                */
