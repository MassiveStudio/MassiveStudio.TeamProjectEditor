﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.Rendering;

namespace MassiveStudio.TeamProjectEditor.EditorWindows.SceneWindow
{
    internal abstract class ManipulationTool
    {
        protected virtual void OnToolGUI(_4ViewScene view)
        {
            if (Selection.activeTransform && !HandlesUtil.s_Hidden)
            {
                bool flag = !HandlesUtil.s_Hidden && EditorApplication.isPlaying && HandlesUtil.ContainsStatic(Selection.gameObjects);
                using (new EditorGUI.DisabledScope(flag))
                {
                    Vector3 handlePosition = UnityEditor.Tools.handlePosition;
                    this.ToolGUI(view, handlePosition, flag);
                    HandlesUtil.ShowStaticLabelIfNeeded(handlePosition);
                }
            }
        }

        public abstract void ToolGUI(_4ViewScene view, Vector3 handlePosition, bool isStatic);
    }

    internal class MoveTool : ManipulationTool
    {
        private static MoveTool s_Instance;
        private static Rect viewRect;

        public static void OnGUI(_4ViewScene view, Rect _viewRect)
        {
            viewRect = _viewRect;
            if (MoveTool.s_Instance == null)
            {
                MoveTool.s_Instance = new MoveTool();
            }
            MoveTool.s_Instance.OnToolGUI(view);
        }

        public override void ToolGUI(_4ViewScene view, Vector3 handlePosition, bool isStatic)
        {
            TransformManipulator.BeginManipulationHandling(false);
            EditorGUI.BeginChangeCheck();


            //Handles.SetCamera(new Rect(viewRect.x, viewRect.y, viewRect.width, viewRect.height), view.computed_camera);//viewRect.x + viewRect.width - 500f

            Vector3 a = Handles.PositionHandle(handlePosition, UnityEditor.Tools.handleRotation);
            if (EditorGUI.EndChangeCheck() && !isStatic)
            {
                Vector3 positionDelta = a - TransformManipulator.mouseDownHandlePosition;
                ManipulationToolUtility.SetMinDragDifferenceForPos(handlePosition);
                if (HandlesUtil.vertexDragging)
                {
                    ManipulationToolUtility.DisableMinDragDifference();
                }
                TransformManipulator.SetPositionDelta(positionDelta);
            }
            TransformManipulator.EndManipulationHandling();
        }
    }

    internal class RotateTool : ManipulationTool
    {
        private static RotateTool s_Instance;

        public static void OnGUI(_4ViewScene view)
        {
            if (RotateTool.s_Instance == null)
            {
                RotateTool.s_Instance = new RotateTool();
            }
            RotateTool.s_Instance.OnToolGUI(view);
        }

        public override void ToolGUI(_4ViewScene view, Vector3 handlePosition, bool isStatic)
        {
            Quaternion handleRotation = UnityEditor.Tools.handleRotation;
            EditorGUI.BeginChangeCheck();
            Quaternion quaternion = Handles.RotationHandle(handleRotation, handlePosition);
            if (EditorGUI.EndChangeCheck() && !isStatic)
            {
                float angle;
                Vector3 point;
                (Quaternion.Inverse(handleRotation) * quaternion).ToAngleAxis(out angle, out point);
                Undo.RecordObjects(Selection.transforms, "Rotate");
                Transform[] transforms = Selection.transforms;
                for (int i = 0; i < transforms.Length; i++)
                {
                    Transform transform = transforms[i];
                    if (UnityEditor.Tools.pivotMode == PivotMode.Center)
                    {
                        transform.RotateAround(handlePosition, handleRotation * point, angle);
                    }
                    else if (TransformManipulator.individualSpace)
                    {
                        transform.Rotate(transform.rotation * point, angle, Space.World);
                    }
                    else
                    {
                        transform.Rotate(handleRotation * point, angle, Space.World);
                    }

                    //ReflectDllMethods.GetMethodInvoked<Vector3>("UnityEditor", "Transform", "GetLocalEulerAngles", )
                    //transform.SetLocalEulerHint(transform.GetLocalEulerAngles(transform.rotationOrder));
                    if (transform.parent != null)
                    {
                        ReflectDllMethods.GetMethodInvoked("UnityEngine", "Transform", "SendTransformChangedScale", new object[] { transform });
                        //transform.SendTransformChangedScale();
                    }
                }
                UnityEditor.Tools.handleRotation = quaternion;
            }
        }
    }

    internal class ScaleTool : ManipulationTool
    {
        private static ScaleTool s_Instance;

        private static Vector3 s_CurrentScale = Vector3.one;

        public static void OnGUI(_4ViewScene view)
        {
            if (ScaleTool.s_Instance == null)
            {
                ScaleTool.s_Instance = new ScaleTool();
            }
            ScaleTool.s_Instance.OnToolGUI(view);
        }

        public override void ToolGUI(_4ViewScene view, Vector3 handlePosition, bool isStatic)
        {
            Quaternion quaternion = (Selection.transforms.Length <= 1) ? HandlesUtil.handleLocalRotation : UnityEditor.Tools.handleRotation;
            TransformManipulator.DebugAlignment(quaternion);
            if (Event.current.type == EventType.MouseDown)
            {
                ScaleTool.s_CurrentScale = Vector3.one;
            }
            EditorGUI.BeginChangeCheck();
            TransformManipulator.BeginManipulationHandling(true);
            ScaleTool.s_CurrentScale = Handles.ScaleHandle(ScaleTool.s_CurrentScale, handlePosition, quaternion, HandleUtility.GetHandleSize(handlePosition));
            TransformManipulator.EndManipulationHandling();
            if (EditorGUI.EndChangeCheck() && !isStatic)
            {
                TransformManipulator.SetScaleDelta(ScaleTool.s_CurrentScale, quaternion);
            }
        }
    }

    internal class RectTool : ManipulationTool
    {
        private static RectTool s_Instance;

        internal const string kChangingLeft = "ChangingLeft";

        internal const string kChangingRight = "ChangingRight";

        internal const string kChangingTop = "ChangingTop";

        internal const string kChangingBottom = "ChangingBottom";

        internal const string kChangingPosX = "ChangingPosX";

        internal const string kChangingPosY = "ChangingPosY";

        internal const string kChangingWidth = "ChangingWidth";

        internal const string kChangingHeight = "ChangingHeight";

        internal const string kChangingPivot = "ChangingPivot";

        private const float kMinVisibleSize = 0.2f;

        private static int s_ResizeHandlesHash = "ResizeHandles".GetHashCode();

        private static int s_RotationHandlesHash = "RotationHandles".GetHashCode();

        private static int s_MoveHandleHash = "MoveHandle".GetHashCode();

        private static int s_PivotHandleHash = "PivotHandle".GetHashCode();

        private static Rect s_StartRect = default(Rect);

        private static Vector3 s_StartMouseWorldPos;

        private static Vector3 s_StartPosition;

        private static Vector2 s_StartMousePos;

        private static Vector3 s_StartRectPosition;

        private static Vector2 s_CurrentMousePos;

        private static bool s_Moving = false;

        private static int s_LockAxis = -1;

        private static Handles.CapFunction cache_0;

        private static Handles.CapFunction cache_1;


        [SerializeField]
        static System.Reflection.MethodInfo _SetPivotSmart;
        static System.Reflection.MethodInfo SetPivotSmart
        {
            get
            {
                if (_SetPivotSmart == null)
                    _SetPivotSmart = ReflectDllMethods.GetType("UnityEditor.RectTransformEditor").GetMethod("SetPivotSmart", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
                return _SetPivotSmart;
            }
        }

        [SerializeField]
        static System.Reflection.MethodInfo _SendTransformChangedScale;
        static System.Reflection.MethodInfo SendTransformChangedScale
        {
            get
            {
                if (_SendTransformChangedScale == null)
                    _SendTransformChangedScale = typeof(UnityEngine.Transform).GetMethod("SendTransformChangedScale", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
                return _SendTransformChangedScale;
            }
        }

        [SerializeField]
        static System.Reflection.MethodInfo _CalculateOffsetSnapValues;
        static System.Reflection.MethodInfo CalculateOffsetSnapValues
        {
            get
            {
                if (_CalculateOffsetSnapValues == null)
                    _CalculateOffsetSnapValues = ReflectDllMethods.GetType("UnityEditor.RectTransformSnapping").GetMethod("CalculateOffsetSnapValues", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
                return _CalculateOffsetSnapValues;
            }
        }

        [SerializeField]
        static System.Reflection.MethodInfo _SnapToGuides;
        static System.Reflection.MethodInfo SnapToGuides
        {
            get
            {
                if (_SnapToGuides == null)
                    _SnapToGuides = ReflectDllMethods.GetType("UnityEditor.RectTransformSnapping").GetMethod("SnapToGuides", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
                return _SnapToGuides;
            }
        }

        [SerializeField]
        static System.Reflection.MethodInfo _PickGameObject;
        static System.Reflection.MethodInfo PickGameObject
        {
            get
            {
                if (_PickGameObject == null)
                    _PickGameObject = ReflectDllMethods.GetType("UnityEditor.SceneViewPicking").GetMethod("PickGameObject", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
                return _PickGameObject;
            }
        }

        [SerializeField]
        static System.Reflection.MethodInfo _CalculatePositionSnapValues;
        static System.Reflection.MethodInfo CalculatePositionSnapValues
        {
            get
            {
                if (_CalculatePositionSnapValues == null)
                    _CalculatePositionSnapValues = ReflectDllMethods.GetType("UnityEditor.RectTransformSnapping").GetMethod("CalculatePositionSnapValues", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
                return _CalculatePositionSnapValues;
            }
        }

        [SerializeField]
        static System.Reflection.MethodInfo _FindNearestVertex;
        static System.Reflection.MethodInfo FindNearestVertex
        {
            get
            {
                if (_FindNearestVertex == null)
                    _FindNearestVertex = ReflectDllMethods.GetType("UnityEditor.HandleUtility").GetMethod("FindNearestVertex", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
                return _FindNearestVertex;
            }
        }

        [SerializeField]
        static System.Reflection.MethodInfo _CalculatePivotSnapValues;
        static System.Reflection.MethodInfo CalculatePivotSnapValues
        {
            get
            {
                if (_CalculatePivotSnapValues == null)
                    _CalculatePivotSnapValues = ReflectDllMethods.GetType("UnityEditor.RectTransformSnapping").GetMethod("CalculatePivotSnapValues", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
                return _CalculatePivotSnapValues;
            }
        }


        public static void OnGUI(_4ViewScene view)
        {
            if (RectTool.s_Instance == null)
            {
                RectTool.s_Instance = new RectTool();
            }
            RectTool.s_Instance.OnToolGUI(view);
        }

        public static Vector2 GetLocalRectPoint(Rect rect, int index)
        {
            Vector2 result;
            switch (index)
            {
                case 0:
                    result = new Vector2(rect.xMin, rect.yMax);
                    break;
                case 1:
                    result = new Vector2(rect.xMax, rect.yMax);
                    break;
                case 2:
                    result = new Vector2(rect.xMax, rect.yMin);
                    break;
                case 3:
                    result = new Vector2(rect.xMin, rect.yMin);
                    break;
                default:
                    result = Vector3.zero;
                    break;
            }
            return result;
        }

        public override void ToolGUI(_4ViewScene view, Vector3 handlePosition, bool isStatic)
        {
            Rect handleRect = HandlesUtil.handleRect;
            Quaternion handleRectRotation = HandlesUtil.handleRectRotation;
            Vector3[] array = new Vector3[4];
            for (int i = 0; i < 4; i++)
            {
                Vector3 point = RectTool.GetLocalRectPoint(handleRect, i);
                array[i] = handleRectRotation * point + handlePosition;
            }
            RectHandles.RenderRectWithShadow(false, array);
            Color color = GUI.color;
            if (Camera.current)
            {
                Vector3 planeNormal = (!Camera.current.orthographic) ? (handlePosition + handleRectRotation * handleRect.center - Camera.current.transform.position) : Camera.current.transform.forward;
                Vector3 vector = handleRectRotation * Vector3.right * handleRect.width;
                Vector3 vector2 = handleRectRotation * Vector3.up * handleRect.height;
                float num = Mathf.Sqrt(Vector3.Cross(Vector3.ProjectOnPlane(vector, planeNormal), Vector3.ProjectOnPlane(vector2, planeNormal)).magnitude);
                num /= HandleUtility.GetHandleSize(handlePosition);
                float num2 = Mathf.Clamp01((num - 0.2f) / 0.2f * 2f);
                Color color2 = color;
                color2.a *= num2;
                GUI.color = color2;
            }
            Vector3 handlePosition2 = HandlesUtil.GetHandlePosition();
            if (!HandlesUtil.vertexDragging)
            {
                RectTransform component = Selection.activeTransform.GetComponent<RectTransform>();
                bool flag = Selection.transforms.Length > 1;
                bool flag2 = !flag && UnityEditor.Tools.pivotMode == PivotMode.Pivot && component != null;
                using (new EditorGUI.DisabledScope(!flag && !flag2))
                {
                    EditorGUI.BeginChangeCheck();
                    Vector3 a = RectTool.PivotHandleGUI(handleRect, handlePosition2, handleRectRotation);
                    if (EditorGUI.EndChangeCheck() && !isStatic)
                    {
                        if (flag)
                        {
                            HandlesUtil.localHandleOffset += Quaternion.Inverse(UnityEditor.Tools.handleRotation) * (a - handlePosition2);
                        }
                        else if (flag2)
                        {
                            Transform activeTransform = Selection.activeTransform;
                            Undo.RecordObject(component, "Move Rectangle Pivot");
                            Transform transform = (!UnityEditor.Tools.rectBlueprintMode || !HandlesUtil.SupportsRectLayout(activeTransform)) ? activeTransform : activeTransform.parent;
                            Vector2 b = transform.InverseTransformVector(a - handlePosition2);
                            b.x /= component.rect.width;
                            b.y /= component.rect.height;
                            Vector2 vector3 = component.pivot + b;

                            ReflectDllMethods.GetMethodInvokedVoid(SetPivotSmart, new object[] { component, vector3.x, 0, true, transform != component.transform });//"UnityEditor", "RectTransformEditor", "SetPivotSmart"
                            ReflectDllMethods.GetMethodInvokedVoid(SetPivotSmart, new object[] { component, vector3.x, 1, true, transform != component.transform });

                            //RectTransformEditor.SetPivotSmart(component, vector3.x, 0, true, transform != component.transform);
                            //RectTransformEditor.SetPivotSmart(component, vector3.y, 1, true, transform != component.transform);
                        }
                    }
                }
            }
            TransformManipulator.BeginManipulationHandling(true);
            if (!HandlesUtil.vertexDragging)
            {
                EditorGUI.BeginChangeCheck();
                Vector3 pivotPosition = handlePosition;
                Vector3 scaleDelta = RectTool.ResizeHandlesGUI(handleRect, handlePosition, handleRectRotation, out pivotPosition);
                if (EditorGUI.EndChangeCheck() && !isStatic)
                {
                    TransformManipulator.SetResizeDelta(scaleDelta, pivotPosition, handleRectRotation);
                }
                bool flag3 = true;
                if (UnityEditor.Tools.rectBlueprintMode)
                {
                    Transform[] transforms = Selection.transforms;
                    for (int j = 0; j < transforms.Length; j++)
                    {
                        Transform transform2 = transforms[j];
                        if (transform2.GetComponent<RectTransform>() != null)
                        {
                            flag3 = false;
                        }
                    }
                }
                if (flag3)
                {
                    EditorGUI.BeginChangeCheck();
                    Quaternion rhs = RectTool.RotationHandlesGUI(handleRect, handlePosition, handleRectRotation);
                    if (EditorGUI.EndChangeCheck() && !isStatic)
                    {
                        float angle;
                        Vector3 vector4;
                        (Quaternion.Inverse(handleRectRotation) * rhs).ToAngleAxis(out angle, out vector4);
                        vector4 = handleRectRotation * vector4;
                        Undo.RecordObjects(Selection.transforms, "Rotate");
                        Transform[] transforms2 = Selection.transforms;
                        for (int k = 0; k < transforms2.Length; k++)
                        {
                            Transform transform3 = transforms2[k];
                            transform3.RotateAround(handlePosition, vector4, angle);


                            //ReflectDllMethods.GetMethodInvoked<Vector3>("UnityEngine", "Transform", "GetLocalEulerAngles", new object[] { rect, pivot, rotation });
                            //transform3.SetLocalEulerHint(transform3.GetLocalEulerAngles(transform3.rotationOrder));
                            if (transform3.parent != null)
                            {
                                //transform3.SendTransformChangedScale();
                                ReflectDllMethods.GetMethodInvokedVoid(SendTransformChangedScale, new object[] { transform3 });//"UnityEngine", "Transform", "SendTransformChangedScale"
                            }
                        }
                        UnityEditor.Tools.handleRotation = Quaternion.AngleAxis(angle, vector4) * UnityEditor.Tools.handleRotation;
                    }
                }
            }
            TransformManipulator.EndManipulationHandling();
            TransformManipulator.BeginManipulationHandling(false);
            EditorGUI.BeginChangeCheck();
            Vector3 a2 = RectTool.MoveHandlesGUI(handleRect, handlePosition, handleRectRotation);
            if (EditorGUI.EndChangeCheck() && !isStatic)
            {
                Vector3 positionDelta = a2 - TransformManipulator.mouseDownHandlePosition;
                TransformManipulator.SetPositionDelta(positionDelta);
            }
            TransformManipulator.EndManipulationHandling();
            GUI.color = color;
        }

        private static Vector3 GetRectPointInWorld(Rect rect, Vector3 pivot, Quaternion rotation, int xHandle, int yHandle)
        {
            Vector3 point = new Vector2(point.x = Mathf.Lerp(rect.xMin, rect.xMax, (float)xHandle * 0.5f), point.y = Mathf.Lerp(rect.yMin, rect.yMax, (float)yHandle * 0.5f));
            return rotation * point + pivot;
        }

        private static Vector3 ResizeHandlesGUI(Rect rect, Vector3 pivot, Quaternion rotation, out Vector3 scalePivot)
        {
            if (Event.current.type == EventType.MouseDown)
            {
                RectTool.s_StartRect = rect;
            }
            scalePivot = pivot;
            Vector3 result = Vector3.one;
            Quaternion rotation2 = Quaternion.Inverse(rotation);
            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    if (i != 1 || j != 1)
                    {
                        Vector3 rectPointInWorld = RectTool.GetRectPointInWorld(RectTool.s_StartRect, pivot, rotation, i, j);
                        Vector3 rectPointInWorld2 = RectTool.GetRectPointInWorld(rect, pivot, rotation, i, j);
                        float num = 0.05f * HandleUtility.GetHandleSize(rectPointInWorld2);
                        int controlID = GUIUtility.GetControlID(RectTool.s_ResizeHandlesHash, FocusType.Passive);
                        if (GUI.color.a > 0f || GUIUtility.hotControl == controlID)
                        {
                            EditorGUI.BeginChangeCheck();
                            EventType type = Event.current.type;
                            Vector3 vector;
                            if (i == 1 || j == 1)
                            {
                                Vector3 sideVector = (i != 1) ? (rotation * Vector3.up * rect.height) : (rotation * Vector3.right * rect.width);
                                Vector3 direction = (i != 1) ? (rotation * Vector3.right) : (rotation * Vector3.up);
                                vector = RectHandles.SideSlider(controlID, rectPointInWorld2, sideVector, direction, num, null, 0f);
                            }
                            else
                            {
                                Vector3 vector2 = rotation * Vector3.right * (float)(i - 1);
                                Vector3 vector3 = rotation * Vector3.up * (float)(j - 1);
                                int arg_1AB_0 = controlID;
                                Vector3 arg_1AB_1 = rectPointInWorld2;
                                Vector3 arg_1AB_2 = rotation * Vector3.forward;
                                Vector3 arg_1AB_3 = vector2;
                                Vector3 arg_1AB_4 = vector3;
                                float arg_1AB_5 = num;
                                if (RectTool.cache_0 == null)
                                {
                                    RectTool.cache_0 = new Handles.CapFunction(RectHandles.RectScalingHandleCap);
                                }
                                vector = RectHandles.CornerSlider(arg_1AB_0, arg_1AB_1, arg_1AB_2, arg_1AB_3, arg_1AB_4, arg_1AB_5, RectTool.cache_0, Vector2.zero);
                            }
                            bool flag = Selection.transforms.Length == 1 && HandlesUtil.SupportsRectLayout(Selection.activeTransform) && Selection.activeTransform.parent.rotation == rotation;
                            if (flag)
                            {
                                Transform activeTransform = Selection.activeTransform;
                                RectTransform component = activeTransform.GetComponent<RectTransform>();
                                Transform parent = activeTransform.parent;
                                RectTransform component2 = parent.GetComponent<RectTransform>();
                                if (type == EventType.MouseDown && Event.current.type != EventType.MouseDown)
                                {
                                    ReflectDllMethods.GetMethodInvokedVoid(CalculateOffsetSnapValues, new object[] { parent, activeTransform, component2, component, i, j }); //"UnityEditor", "RectTransformSnapping", "CalculateOffsetSnapValues"
                                    //RectTransformSnapping.CalculateOffsetSnapValues(parent, activeTransform, component2, component, i, j);
                                }
                            }
                            if (EditorGUI.EndChangeCheck())
                            {
                                ManipulationToolUtility.SetMinDragDifferenceForPos(rectPointInWorld2, 0.1f);
                                if (flag)
                                {
                                    Transform parent2 = Selection.activeTransform.parent;
                                    RectTransform component3 = parent2.GetComponent<RectTransform>();
                                    Vector2 snapDistance = Vector2.one * HandleUtility.GetHandleSize(vector) * 0.05f;
                                    snapDistance.x /= (rotation2 * parent2.TransformVector(Vector3.right)).x;
                                    snapDistance.y /= (rotation2 * parent2.TransformVector(Vector3.up)).y;
                                    Vector3 vector4 = parent2.InverseTransformPoint(vector) - (Vector3  )component3.rect.min;
                                    Vector3 vector5 = ReflectDllMethods.GetMethodInvoked<Vector3>(SnapToGuides, new object[] { vector4, snapDistance }) + Vector3.forward * vector4.z;//"UnityEditor", "RectTransformSnapping", "SnapToGuides"
                                    //Vector3 vector5 = RectTransformSnapping.SnapToGuides(vector4, snapDistance) + Vector3.forward * vector4.z;
                                    ManipulationToolUtility.DisableMinDragDifferenceBasedOnSnapping(vector4, vector5);
                                    vector = parent2.TransformPoint(vector5 + (Vector3)component3.rect.min);
                                }
                                bool alt = Event.current.alt;
                                bool actionKey = EditorGUI.actionKey;
                                bool flag2 = Event.current.shift && !actionKey;
                                if (!alt)
                                {
                                    scalePivot = RectTool.GetRectPointInWorld(RectTool.s_StartRect, pivot, rotation, 2 - i, 2 - j);
                                }
                                if (flag2)
                                {
                                    vector = Vector3.Project(vector - scalePivot, rectPointInWorld - scalePivot) + scalePivot;
                                }
                                Vector3 vector6 = rotation2 * (rectPointInWorld - scalePivot);
                                Vector3 vector7 = rotation2 * (vector - scalePivot);
                                if (i != 1)
                                {
                                    result.x = vector7.x / vector6.x;
                                }
                                if (j != 1)
                                {
                                    result.y = vector7.y / vector6.y;
                                }
                                if (flag2)
                                {
                                    float d = (i != 1) ? result.x : result.y;
                                    result = Vector3.one * d;
                                }
                                if (actionKey && i == 1)
                                {
                                    if (Event.current.shift)
                                    {
                                        result.x = (result.z = 1f / Mathf.Sqrt(Mathf.Max(result.y, 0.0001f)));
                                    }
                                    else
                                    {
                                        result.x = 1f / Mathf.Max(result.y, 0.0001f);
                                    }
                                }
                                if (flag2)
                                {
                                    float d2 = (i != 1) ? result.x : result.y;
                                    result = Vector3.one * d2;
                                }
                                if (actionKey && i == 1)
                                {
                                    if (Event.current.shift)
                                    {
                                        result.x = (result.z = 1f / Mathf.Sqrt(Mathf.Max(result.y, 0.0001f)));
                                    }
                                    else
                                    {
                                        result.x = 1f / Mathf.Max(result.y, 0.0001f);
                                    }
                                }
                                if (actionKey && j == 1)
                                {
                                    if (Event.current.shift)
                                    {
                                        result.y = (result.z = 1f / Mathf.Sqrt(Mathf.Max(result.x, 0.0001f)));
                                    }
                                    else
                                    {
                                        result.y = 1f / Mathf.Max(result.x, 0.0001f);
                                    }
                                }
                            }
                            if (i == 0)
                            {
                                ManipulationToolUtility.DetectDraggingBasedOnMouseDownUp("ChangingLeft", type);
                            }
                            if (i == 2)
                            {
                                ManipulationToolUtility.DetectDraggingBasedOnMouseDownUp("ChangingRight", type);
                            }
                            if (i != 1)
                            {
                                ManipulationToolUtility.DetectDraggingBasedOnMouseDownUp("ChangingWidth", type);
                            }
                            if (j == 0)
                            {
                                ManipulationToolUtility.DetectDraggingBasedOnMouseDownUp("ChangingBottom", type);
                            }
                            if (j == 2)
                            {
                                ManipulationToolUtility.DetectDraggingBasedOnMouseDownUp("ChangingTop", type);
                            }
                            if (j != 1)
                            {
                                ManipulationToolUtility.DetectDraggingBasedOnMouseDownUp("ChangingHeight", type);
                            }
                        }
                    }
                }
            }
            return result;
        }

        private static Vector3 MoveHandlesGUI(Rect rect, Vector3 pivot, Quaternion rotation)
        {
            int controlID = GUIUtility.GetControlID(RectTool.s_MoveHandleHash, FocusType.Passive);
            Vector3 vector = pivot;
            float num = HandleUtility.GetHandleSize(pivot) * 0.2f;
            float num2 = 1f - GUI.color.a;
            Vector3[] array = new Vector3[]
            {
                rotation * new Vector2(rect.x, rect.y) + pivot,
                rotation * new Vector2(rect.xMax, rect.y) + pivot,
                rotation * new Vector2(rect.xMax, rect.yMax) + pivot,
                rotation * new Vector2(rect.x, rect.yMax) + pivot
            };

            ReflectDllMethods.GetMethodInvokedVoid("UnityEditor", "VertexSnapping", "HandleKeyAndMouseMove", new object[] { controlID });
            //VertexSnapping.HandleKeyAndMouseMove(controlID);

            bool flag = Selection.transforms.Length == 1 && HandlesUtil.SupportsRectLayout(Selection.activeTransform) && Selection.activeTransform.parent.rotation == rotation;
            Event current = Event.current;
            EventType typeForControl = current.GetTypeForControl(controlID);
            Plane plane = new Plane(array[0], array[1], array[2]);
            switch (typeForControl)
            {
                case EventType.MouseDown:
                    {
                        bool flag2 = HandlesUtil.vertexDragging || (current.button == 0 && current.modifiers == EventModifiers.None && RectHandles.RaycastGUIPointToWorldHit(current.mousePosition, plane, out RectTool.s_StartMouseWorldPos) && (RectTool.SceneViewDistanceToRectangle(array, current.mousePosition) == 0f || (num2 > 0f && RectTool.SceneViewDistanceToDisc(pivot, rotation * Vector3.forward, num, current.mousePosition) == 0f)));
                        if (flag2)
                        {
                            RectTool.s_StartPosition = pivot;
                            RectTool.s_StartMousePos = (RectTool.s_CurrentMousePos = current.mousePosition);
                            RectTool.s_Moving = false;
                            RectTool.s_LockAxis = -1;
                            int num3 = controlID;
                            GUIUtility.keyboardControl = num3;
                            GUIUtility.hotControl = num3;
                            EditorGUIUtility.SetWantsMouseJumping(1);

                            ReflectDllMethods.SetField(ReflectDllMethods.GetType( "UnityEditor.HandleUtility"), null, "ignoreRaySnapObjects", null);
                            //HandleUtility.ignoreRaySnapObjects = null;
                            current.Use();
                            if (flag)
                            {
                                Transform activeTransform = Selection.activeTransform;
                                RectTransform component = activeTransform.GetComponent<RectTransform>();
                                Transform parent = activeTransform.parent;
                                RectTransform component2 = parent.GetComponent<RectTransform>();
                                RectTool.s_StartRectPosition = component.anchoredPosition;

                                ReflectDllMethods.GetMethodInvokedVoid(CalculatePositionSnapValues, new object[] { parent, activeTransform, component2, component });//"UnityEditor", "RectTransformSnapping", "CalculatePositionSnapValues"
                                //RectTransformSnapping.CalculatePositionSnapValues(parent, activeTransform, component2, component);
                            }
                        }
                        break;
                    }
                case EventType.MouseUp:
                    if (GUIUtility.hotControl == controlID)
                    {
                        if (!RectTool.s_Moving)
                        {
                            Selection.activeGameObject = ReflectDllMethods.GetMethodInvoked<GameObject>(PickGameObject, new object[] { current.mousePosition });//"UnityEditor", "SceneViewPicking", "PickGameObject"
                           //   Selection.activeGameObject = SceneViewPicking.PickGameObject(current.mousePosition);
                        }
                        GUIUtility.hotControl = 0;
                        EditorGUIUtility.SetWantsMouseJumping(0);
                        ReflectDllMethods.SetField(ReflectDllMethods.GetType("UnityEditor.HandleUtility"), null, "ignoreRaySnapObjects", null);
                        //HandleUtility.ignoreRaySnapObjects = null;
                        current.Use();
                    }
                    break;
                case EventType.MouseDrag:
                    if (GUIUtility.hotControl == controlID)
                    {
                        RectTool.s_CurrentMousePos += current.delta;
                        if (!RectTool.s_Moving && (RectTool.s_CurrentMousePos - RectTool.s_StartMousePos).magnitude > 3f)
                        {
                            RectTool.s_Moving = true;
                            RectHandles.RaycastGUIPointToWorldHit(RectTool.s_CurrentMousePos, plane, out RectTool.s_StartMouseWorldPos);
                        }
                        if (RectTool.s_Moving)
                        {
                            if (HandlesUtil.vertexDragging)
                            {
                                if (ReflectDllMethods.GetField(ReflectDllMethods.GetType("UnityEditor.HandleUtility"), null, "ignoreRaySnapObjects") == null)
                                {
                                    ReflectDllMethods.GetMethodInvokedVoid("UnityEditor", "Handles", "SetupIgnoreRaySnapObjects", null);
                                    //Handles.SetupIgnoreRaySnapObjects();
                                }
                                Vector3 vector2 = Vector2.zero;


                                if(ReflectDllMethods.GetMethodInvoked<bool>(FindNearestVertex, new object[] { RectTool.s_CurrentMousePos, null, vector2 }))//"UnityEditor", "HandleUtility", "FindNearestVertex"
                                //if (HandleUtility.FindNearestVertex(RectTool.s_CurrentMousePos, null, out vector2))
                                {
                                    vector = vector2;
                                    GUI.changed = true;
                                }
                                ManipulationToolUtility.minDragDifference = Vector2.zero;
                            }
                            else
                            {
                                ManipulationToolUtility.SetMinDragDifferenceForPos(pivot);
                                Vector3 a;
                                if (RectHandles.RaycastGUIPointToWorldHit(RectTool.s_CurrentMousePos, plane, out a))
                                {
                                    Vector3 vector3 = a - RectTool.s_StartMouseWorldPos;
                                    if (current.shift)
                                    {
                                        vector3 = Quaternion.Inverse(rotation) * vector3;
                                        if (RectTool.s_LockAxis == -1)
                                        {
                                            RectTool.s_LockAxis = ((Mathf.Abs(vector3.x) <= Mathf.Abs(vector3.y)) ? 1 : 0);
                                        }
                                        vector3[1 - RectTool.s_LockAxis] = 0f;
                                        vector3 = rotation * vector3;
                                    }
                                    else
                                    {
                                        RectTool.s_LockAxis = -1;
                                    }
                                    if (flag)
                                    {
                                        Transform parent2 = Selection.activeTransform.parent;
                                        Vector3 vector4 = RectTool.s_StartRectPosition + parent2.InverseTransformVector(vector3);
                                        vector4.z = 0f;
                                        Quaternion rotation2 = Quaternion.Inverse(rotation);
                                        Vector2 snapDistance = Vector2.one * HandleUtility.GetHandleSize(vector) * 0.05f;
                                        snapDistance.x /= (rotation2 * parent2.TransformVector(Vector3.right)).x;
                                        snapDistance.y /= (rotation2 * parent2.TransformVector(Vector3.up)).y;

                                        Vector3 vector5 = ReflectDllMethods.GetMethodInvoked<Vector3>(SnapToGuides, new object[] { vector4, snapDistance });//"UnityEditor", "RectTransformSnapping", "SnapToGuides"
                                        // Vector3 vector5 = RectTransformSnapping.SnapToGuides(vector4, snapDistance);
                                        ManipulationToolUtility.DisableMinDragDifferenceBasedOnSnapping(vector4, vector5);
                                        vector3 = parent2.TransformVector(vector5 - RectTool.s_StartRectPosition);
                                    }
                                    vector = RectTool.s_StartPosition + vector3;
                                    GUI.changed = true;
                                }
                            }
                        }
                        current.Use();
                    }
                    break;
                case EventType.Repaint:
                    if (HandlesUtil.vertexDragging)
                    {
                        RectHandles.RectScalingHandleCap(controlID, pivot, rotation, 1f, EventType.Repaint);
                    }
                    else
                    {
                        Handles.color = Handles.secondaryColor * new Color(1f, 1f, 1f, 1.5f * num2);
                        Handles.CircleHandleCap(controlID, pivot, rotation, num, EventType.Repaint);
                        Handles.color = Handles.secondaryColor * new Color(1f, 1f, 1f, 0.3f * num2);
                        Handles.DrawSolidDisc(pivot, rotation * Vector3.forward, num);
                    }
                    break;
            }
            ManipulationToolUtility.DetectDraggingBasedOnMouseDownUp("ChangingPosX", typeForControl);
            ManipulationToolUtility.DetectDraggingBasedOnMouseDownUp("ChangingLeft", typeForControl);
            ManipulationToolUtility.DetectDraggingBasedOnMouseDownUp("ChangingRight", typeForControl);
            ManipulationToolUtility.DetectDraggingBasedOnMouseDownUp("ChangingPosY", typeForControl);
            ManipulationToolUtility.DetectDraggingBasedOnMouseDownUp("ChangingTop", typeForControl);
            ManipulationToolUtility.DetectDraggingBasedOnMouseDownUp("ChangingBottom", typeForControl);
            return vector;
        }

        private static float SceneViewDistanceToDisc(Vector3 center, Vector3 normal, float radius, Vector2 mousePos)
        {
            Plane plane = new Plane(normal, center);
            Ray ray = HandleUtility.GUIPointToWorldRay(mousePos);
            float distance;
            float result;
            if (plane.Raycast(ray, out distance))
            {
                Vector3 point = ray.GetPoint(distance);
                result = Mathf.Max(0f, (point - center).magnitude - radius);
            }
            else
            {
                result = float.PositiveInfinity;
            }
            return result;
        }

        private static float SceneViewDistanceToRectangle(Vector3[] worldPoints, Vector2 mousePos)
        {
            Vector2[] array = new Vector2[4];
            for (int i = 0; i < 4; i++)
            {
                array[i] = HandleUtility.WorldToGUIPoint(worldPoints[i]);
            }
            return RectTool.DistanceToRectangle(array, mousePos);
        }

        private static float DistancePointToLineSegment(Vector2 point, Vector2 a, Vector2 b)
        {
            float sqrMagnitude = (b - a).sqrMagnitude;
            float magnitude;
            if (sqrMagnitude == 0f)
            {
                magnitude = (point - a).magnitude;
            }
            else
            {
                float num = Vector2.Dot(point - a, b - a) / sqrMagnitude;
                if (num < 0f)
                {
                    magnitude = (point - a).magnitude;
                }
                else if (num > 1f)
                {
                    magnitude = (point - b).magnitude;
                }
                else
                {
                    Vector2 b2 = a + num * (b - a);
                    magnitude = (point - b2).magnitude;
                }
            }
            return magnitude;
        }

        private static float DistanceToRectangle(Vector2[] screenPoints, Vector2 mousePos)
        {
            bool flag = false;
            int num = 4;
            for (int i = 0; i < 5; i++)
            {
                Vector3 vector = screenPoints[i % 4];
                Vector3 vector2 = screenPoints[num % 4];
                if (vector.y > mousePos.y != vector2.y > mousePos.y)
                {
                    if (mousePos.x < (vector2.x - vector.x) * (mousePos.y - vector.y) / (vector2.y - vector.y) + vector.x)
                    {
                        flag = !flag;
                    }
                }
                num = i;
            }
            float result;
            if (!flag)
            {
                float num2 = -1f;
                for (int j = 0; j < 4; j++)
                {
                    Vector3 v = screenPoints[j];
                    Vector3 v2 = screenPoints[(j + 1) % 4];
                    float num3 = RectTool.DistancePointToLineSegment(mousePos, v, v2);
                    if (num3 < num2 || num2 < 0f)
                    {
                        num2 = num3;
                    }
                }
                result = num2;
            }
            else
            {
                result = 0f;
            }
            return result;
        }

        private static Quaternion RotationHandlesGUI(Rect rect, Vector3 pivot, Quaternion rotation)
        {
            Vector3 eulerAngles = rotation.eulerAngles;
            for (int i = 0; i <= 2; i += 2)
            {
                for (int j = 0; j <= 2; j += 2)
                {
                    Vector3 rectPointInWorld = RectTool.GetRectPointInWorld(rect, pivot, rotation, i, j);
                    float handleSize = 0.05f * HandleUtility.GetHandleSize(rectPointInWorld);
                    int controlID = GUIUtility.GetControlID(RectTool.s_RotationHandlesHash, FocusType.Passive);
                    if (GUI.color.a > 0f || GUIUtility.hotControl == controlID)
                    {
                        EditorGUI.BeginChangeCheck();
                        Vector3 outwardsDir = rotation * Vector3.right * (float)(i - 1);
                        Vector3 outwardsDir2 = rotation * Vector3.up * (float)(j - 1);
                        float num = RectHandles.RotationSlider(controlID, rectPointInWorld, eulerAngles.z, pivot, rotation * Vector3.forward, outwardsDir, outwardsDir2, handleSize, null, Vector2.zero);
                        if (EditorGUI.EndChangeCheck())
                        {
                            if (Event.current.shift)
                            {
                                num = Mathf.Round((num - eulerAngles.z) / 15f) * 15f + eulerAngles.z;
                            }
                            eulerAngles.z = num;
                            rotation = Quaternion.Euler(eulerAngles);
                        }
                    }
                }
            }
            return rotation;
        }

        private static Vector3 PivotHandleGUI(Rect rect, Vector3 pivot, Quaternion rotation)
        {
            int controlID = GUIUtility.GetControlID(RectTool.s_PivotHandleHash, FocusType.Passive);
            EventType typeForControl = Event.current.GetTypeForControl(controlID);
            if (GUI.color.a > 0f || GUIUtility.hotControl == controlID)
            {
                EventType eventType = typeForControl;
                EditorGUI.BeginChangeCheck();
                int arg_94_0 = controlID;
                Vector3 arg_94_1 = pivot;
                Vector3 arg_94_2 = rotation * Vector3.forward;
                Vector3 arg_94_3 = rotation * Vector3.right;
                Vector3 arg_94_4 = rotation * Vector3.up;
                float arg_94_5 = HandleUtility.GetHandleSize(pivot) * 0.1f;
                if (RectTool.cache_1 == null)
                {
                    RectTool.cache_1 = new Handles.CapFunction(RectHandles.PivotHandleCap);
                }
                Vector3 a = Handles.Slider2D(arg_94_0, arg_94_1, arg_94_2, arg_94_3, arg_94_4, arg_94_5, RectTool.cache_1, Vector2.zero);
                if (eventType == EventType.MouseDown && GUIUtility.hotControl == controlID)
                {
                    ReflectDllMethods.GetMethodInvokedVoid(CalculatePivotSnapValues, new object[] { rect, pivot, rotation });//"UnityEditor", "RectTransformSnapping", "CalculatePivotSnapValues"
                    //RectTransformSnapping.CalculatePivotSnapValues(rect, pivot, rotation);
                }
                if (EditorGUI.EndChangeCheck())
                {
                    Vector2 vector = Quaternion.Inverse(rotation) * (a - pivot);
                    vector.x /= rect.width;
                    vector.y /= rect.height;
                    Vector2 vector2 = new Vector2(-rect.x / rect.width, -rect.y / rect.height);
                    Vector2 vector3 = vector2 + vector;
                    Vector2 snapDistance = HandleUtility.GetHandleSize(pivot) * 0.05f * new Vector2(1f / rect.width, 1f / rect.height);

                    vector3 = ReflectDllMethods.GetMethodInvoked<Vector3>(SnapToGuides, new object[] { vector3, snapDistance });//"UnityEditor", "RectTransformSnapping", "SnapToGuides"
                    //vector3 = RectTransformSnapping.SnapToGuides(vector3, snapDistance);
                    vector = vector3 - vector2;
                    vector.x *= rect.width;
                    vector.y *= rect.height;
                    pivot += rotation * vector;
                }
            }
            ManipulationToolUtility.DetectDraggingBasedOnMouseDownUp("ChangingPivot", typeForControl);
            return pivot;
        }
    }

    public static class HandlesUtil
    {
        [SerializeField]
        static System.Reflection.MethodInfo _sendReapplyDrivenProperties;
        static System.Reflection.MethodInfo sendReapplyDrivenProperties
        {
            get
            {
                if (_sendReapplyDrivenProperties == null)
                    _sendReapplyDrivenProperties = ReflectDllMethods.GetType("UnityEngine.RectTransform").GetMethod("SendReapplyDrivenProperties", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
                return _sendReapplyDrivenProperties;
            }
        }


        static FuncDel RectTransform_IsDriven = new FuncDel(ReflectDllMethods.GetClassType("UnityEngine", "RectTransform"), "IsDriven", new Type[] { });


        [SerializeField]
        static System.Reflection.MethodInfo _getPermanentControlID;
        static System.Reflection.MethodInfo getPermanentControlID
        {
            get
            {
                if (_getPermanentControlID == null)
                    _getPermanentControlID = ReflectDllMethods.GetType("UnityEngine.GUIUtility").GetMethod("GetPermanentControlID", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
                return _getPermanentControlID;
            }
        }

        public static UnityEditor.ViewTool s_LockedViewTool
        {
            get
            {
                return (ViewTool)ReflectDllMethods.GetField(typeof(UnityEditor.Tools), null, "s_LockedViewTool");
            }
            set
            {
                ReflectDllMethods.SetField(typeof(UnityEditor.Tools), null, "s_LockedViewTool", value);
            }
        }

        public static int s_ButtonDown
        {
            get
            {
                return (int)ReflectDllMethods.GetField(typeof(UnityEditor.Tools), null, "s_ButtonDown");
            }
            set
            {
                ReflectDllMethods.SetField(typeof(UnityEditor.Tools), null, "s_ButtonDown", value);
            }
        }

        public static bool viewToolActive
        {
            get
            {
                return (GUIUtility.hotControl == 0 || s_LockedViewTool != ViewTool.None) && (s_LockedViewTool != ViewTool.None || UnityEditor.Tools.current == Tool.View || Event.current.alt || s_ButtonDown == 1 || s_ButtonDown == 2);
                //return (bool)ReflectDllMethods.GetField(typeof(UnityEditor.Tools), null, "viewToolActive", true);
            }
        }

        public static bool s_Hidden
        {
            get
            {
                return (bool)ReflectDllMethods.GetField(typeof(UnityEditor.Tools), null, "s_Hidden");
            }
            set
            {
                ReflectDllMethods.SetField(typeof(UnityEditor.Tools), null, "s_Hidden", value);
            }
        }

        public static int s_LockHandleRectAxis
        {
            get
            {
                return (int)ReflectDllMethods.GetField(typeof(UnityEditor.Tools), null, "s_LockHandleRectAxis");
            }
            set
            {
                ReflectDllMethods.SetField(typeof(UnityEditor.Tools), null, "s_LockHandleRectAxis", value);
            }
        }

        public static UnityEditor.Tools Tool_get
        {
            get
            {
                UnityEditor.Tools t = (UnityEditor.Tools)ReflectDllMethods.GetField(typeof(UnityEditor.Tools), null, "s_Get");
                if (t == null)
                {
                    t = ScriptableObject.CreateInstance<UnityEditor.Tools>();
                    t.hideFlags = HideFlags.HideAndDontSave;
                    ReflectDllMethods.SetField(typeof(UnityEditor.Tools), null, "s_Get", t);
                }
                return t;
            }
        }

        public static Color s_ColliderHandleColorDisabled
        {
            get
            {
                return (Color)ReflectDllMethods.GetField(typeof(UnityEditor.Handles), null, "s_ColliderHandleColorDisabled");

            }
        }

        public static Color s_ColliderHandleColor
        {
            get
            {
                return (Color)ReflectDllMethods.GetField(typeof(UnityEditor.Handles), null, "s_ColliderHandleColor");

            }
        }

        private static System.Reflection.FieldInfo pivotMode;
        public static UnityEditor.PivotMode Tool_pivotMode(UnityEditor.Tools t)
        {
            //if (pivotMode == null)
            //    pivotMode = t.GetType().GetField("m_PivotMode", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Instance);
            //return (UnityEditor.PivotMode)pivotMode.GetValue(null);
            return (UnityEditor.PivotMode)ReflectDllMethods.GetField(typeof(UnityEditor.Tools), t, "m_PivotMode");
        }

        internal static Quaternion handleLocalRotation
        {
            get
            {
                Transform activeTransform = Selection.activeTransform;
                Quaternion result;
                if (!activeTransform)
                {
                    result = Quaternion.identity;
                }
                else if (UnityEditor.Tools.rectBlueprintMode && SupportsRectLayout(activeTransform))
                {
                    result = activeTransform.parent.rotation;
                }
                else
                {
                    result = activeTransform.rotation;
                }
                return result;
            }
        }

        internal static Vector3 GetHandlePosition()
        {
            Transform activeTransform = Selection.activeTransform;
            Vector3 result;
            if (!activeTransform)
            {
                result = new Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);
            }
            else
            {
                Vector3 b = handleOffset + UnityEditor.Tools.handleRotation * localHandleOffset;
                PivotMode pivotMode = Tool_pivotMode(Tool_get);
                if (pivotMode != PivotMode.Center)
                {
                    if (pivotMode != PivotMode.Pivot)
                    {
                        result = new Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);
                    }
                    else if (UnityEditor.Tools.current == Tool.Rect && UnityEditor.Tools.rectBlueprintMode && SupportsRectLayout(activeTransform))
                    {
                        result = activeTransform.parent.TransformPoint(new Vector3(activeTransform.localPosition.x, activeTransform.localPosition.y, 0f)) + b;
                    }
                    else
                    {
                        result = activeTransform.position + b;
                    }
                }
                else if (UnityEditor.Tools.current == Tool.Rect)
                {
                    result = UnityEditor.Tools.handleRotation * CalculateSelectionBoundsInSpace(Vector3.zero, UnityEditor.Tools.handleRotation, UnityEditor.Tools.rectBlueprintMode).center + b;
                }
                else
                {
                    result = InternalEditorUtility.CalculateSelectionBounds(true, false).center + b;
                }
            }
            return result;
        }

        [Obsolete()]
        public static UnityEngine.Object RectTransformGetDriven(RectTransform r)
        {
          return (UnityEngine.Object)ReflectDllMethods.GetField(typeof(RectTransform), r, "drivenByObject");
        }

        public static bool RectTransform_isDriven(RectTransform r)
        {
            //return (UnityEngine.Object)ReflectDllMethods.GetMethodInvoked("UnityEngine", "RectTransform", "IsDriven", new object[] { r });
            return RectTransform_IsDriven._<bool>(r);
        }

        public static void SendReapplyDrivenProperties(RectTransform r)
        {
            ReflectDllMethods.GetMethodInvokedVoid(sendReapplyDrivenProperties, new object[] { r });//"UnityEngine", "RectTransform", "SendReapplyDrivenProperties"
        }

        public static Vector3 localHandleOffset
        {
            get
            {
                return (Vector3)ReflectDllMethods.GetField(typeof(UnityEditor.Tools), null, "localHandleOffset");
            }
            set
            {
                ReflectDllMethods.SetField(typeof(UnityEditor.Tools), null, "localHandleOffset", value);
            }
        }

        public static Vector3 handleOffset
        {
            get
            {
                return (Vector3)ReflectDllMethods.GetField(typeof(UnityEditor.Tools), null, "handleOffset");
            }
            set
            {
                ReflectDllMethods.SetField(typeof(UnityEditor.Tools), null, "handleOffset", value);
            }
        }

        public static Rect handleRect
        {
            get
            {
                Bounds bounds = CalculateSelectionBoundsInSpace(UnityEditor.Tools.handlePosition, UnityEditor.Tools.handleRotation, UnityEditor.Tools.rectBlueprintMode);
                int rectAxisForViewDir = GetRectAxisForViewDir(bounds, UnityEditor.Tools.handleRotation, _4ViewScene.currentView.computed_camera.transform.forward);
                return GetRectFromBoundsForAxis(bounds, rectAxisForViewDir);
            }
        }

        public static Quaternion handleRectRotation
        {
            get
            {
                Bounds bounds = CalculateSelectionBoundsInSpace(UnityEditor.Tools.handlePosition, UnityEditor.Tools.handleRotation, UnityEditor.Tools.rectBlueprintMode);
                int rectAxisForViewDir = GetRectAxisForViewDir(bounds, UnityEditor.Tools.handleRotation, _4ViewScene.currentView.computed_camera.transform.forward);
                return GetRectRotationForAxis(UnityEditor.Tools.handleRotation, rectAxisForViewDir);
            }
        }

        private static Rect GetRectFromBoundsForAxis(Bounds bounds, int axis)
        {
            Rect result;
            switch (axis)
            {
                case 0:
                    result = new Rect(-bounds.max.z, bounds.min.y, bounds.size.z, bounds.size.y);
                    return result;
                case 1:
                    result = new Rect(bounds.min.x, -bounds.max.z, bounds.size.x, bounds.size.z);
                    return result;
            }
            result = new Rect(bounds.min.x, bounds.min.y, bounds.size.x, bounds.size.y);
            return result;
        }

        private static Quaternion GetRectRotationForAxis(Quaternion rotation, int axis)
        {
            Quaternion result;
            switch (axis)
            {
                case 0:
                    result = rotation * Quaternion.Euler(0f, 90f, 0f);
                    return result;
                case 1:
                    result = rotation * Quaternion.Euler(-90f, 0f, 0f);
                    return result;
            }
            result = rotation;
            return result;
        }

        public static Vector3 s_LockHandlePosition
        {
            get
            {
                return (Vector3)ReflectDllMethods.GetField(typeof(UnityEditor.Tools), null, "s_LockHandlePosition");
            }
            set
            {
                ReflectDllMethods.SetField(typeof(UnityEditor.Tools), null, "s_LockHandlePosition", value);
            }
        }

        public static bool vertexDragging
        {
            get
            {
                return (bool)ReflectDllMethods.GetField(typeof(UnityEditor.Tools), null, "vertexDragging");
            }
            set
            {
                ReflectDllMethods.SetField(typeof(UnityEditor.Tools), null, "vertexDragging", value);
            }
        }

        public static bool s_LockHandlePositionActive
        {
            get
            {
                return (bool)ReflectDllMethods.GetField(typeof(UnityEditor.Tools), null, "s_LockHandlePositionActive");
            }
            set
            {
                ReflectDllMethods.SetField(typeof(UnityEditor.Tools), null, "s_LockHandlePositionActive", value);
            }
        }

        public static bool s_LockHandleRectAxisActive
        {
            get
            {
                return (bool)ReflectDllMethods.GetField(typeof(UnityEditor.Tools), null, "s_LockHandleRectAxisActive");
            }
            set
            {
                ReflectDllMethods.SetField(typeof(UnityEditor.Tools), null, "s_LockHandleRectAxisActive", value);
            }
        }

        public static UnityEditor.ViewTool viewTool
        {
            get
            {
                return (ViewTool)ReflectDllMethods.GetField(typeof(UnityEditor.Tools), null, "viewTool");
            }
            set
            {
                ReflectDllMethods.SetField(typeof(UnityEditor.Tools), null, "viewTool", value);
            }
        }

        internal static bool ContainsStatic(GameObject[] objects)
        {
            bool result;
            if (objects == null || objects.Length == 0)
            {
                result = false;
            }
            else
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    if (objects[i] != null && objects[i].isStatic)
                    {
                        result = true;
                        return result;
                    }
                }
                result = false;
            }
            return result;
        }

        internal static void ShowStaticLabelIfNeeded(Vector3 pos)
        {
            if (!s_Hidden && EditorApplication.isPlaying && ContainsStatic(Selection.gameObjects))
            {
                ShowStaticLabel(pos);
            }
        }

        internal static void ShowStaticLabel(Vector3 pos)
        {
            Handles.color = Color.white;
            Handles.zTest = CompareFunction.Always;
            GUIStyle gUIStyle = "SC ViewAxisLabel";
            gUIStyle.alignment = TextAnchor.MiddleLeft;
            gUIStyle.fixedWidth = 0f;
            Handles.BeginGUI();
            Rect position = HandleUtility.WorldPointToSizedRect(pos,new GUIContent("Static"), gUIStyle);
            position.x += 10f;
            position.y += 10f;
            GUI.Label(position, new GUIContent("Static"), gUIStyle);
            Handles.EndGUI();
        }

        internal static void UnlockHandlePosition()
        {
            s_LockHandlePositionActive = false;
        }

        internal static void UnlockHandleRectRotation()
        {
            s_LockHandleRectAxisActive = false;
        }

        internal static void LockHandlePosition()
        {
            LockHandlePosition(UnityEditor.Tools.handlePosition);
        }

        internal static void LockHandlePosition(Vector3 pos)
        {
            s_LockHandlePosition = pos;
            s_LockHandlePositionActive = true;
        }

        internal static void LockHandleRectRotation()
        {
            Bounds bounds = CalculateSelectionBoundsInSpace(UnityEditor.Tools.handlePosition, UnityEditor.Tools.handleRotation, UnityEditor.Tools.rectBlueprintMode);
            s_LockHandleRectAxis = GetRectAxisForViewDir(bounds, UnityEditor.Tools.handleRotation, _4ViewScene.currentView.selected_camera.transform.forward);
            s_LockHandleRectAxisActive = true;
        }

        public static int GetPermanentControlID()
        {
            return ReflectDllMethods.GetMethodInvoked<int>(getPermanentControlID, null);//"UnityEngine", "GUIUtility", "GetPermanentControlID"
        }

        internal static float RoundBasedOnMinimumDifference(float valueToRound, float minDifference)
        {
            float result;
            if (minDifference == 0f)
            {
                result = DiscardLeastSignificantDecimal(valueToRound);
            }
            else
            {
                result = (float)Math.Round((double)valueToRound, GetNumberOfDecimalsForMinimumDifference(minDifference), MidpointRounding.AwayFromZero);
            }
            return result;
        }

        internal static float DiscardLeastSignificantDecimal(float v)
        {
            int digits = Mathf.Clamp((int)(5f - Mathf.Log10(Mathf.Abs(v))), 0, 15);
            return (float)Math.Round((double)v, digits, MidpointRounding.AwayFromZero);
        }

        internal static int GetNumberOfDecimalsForMinimumDifference(float minDifference)
        {
            return Mathf.Clamp(-Mathf.FloorToInt(Mathf.Log10(Mathf.Abs(minDifference))), 0, 15);
        }

        private static int GetRectAxisForViewDir(Bounds bounds, Quaternion rotation, Vector3 viewDir)
        {
            int result;
            if (s_LockHandleRectAxisActive)
            {
                result = s_LockHandleRectAxis;
            }
            else if (viewDir == Vector3.zero)
            {
                result = 2;
            }
            else
            {
                if (bounds.size == Vector3.zero)
                {
                    bounds.size = Vector3.one;
                }
                int num = -1;
                float num2 = -1f;
                for (int i = 0; i < 3; i++)
                {
                    Vector3 zero = Vector3.zero;
                    Vector3 zero2 = Vector3.zero;
                    int index = (i + 1) % 3;
                    int index2 = (i + 2) % 3;
                    zero[index] = bounds.size[index];
                    zero2[index2] = bounds.size[index2];
                    float magnitude = Vector3.Cross(Vector3.ProjectOnPlane(rotation * zero, viewDir), Vector3.ProjectOnPlane(rotation * zero2, viewDir)).magnitude;
                    if (magnitude > num2)
                    {
                        num2 = magnitude;
                        num = i;
                    }
                }
                result = num;
            }
            return result;
        }

        internal static Bounds CalculateSelectionBoundsInSpace(Vector3 position, Quaternion rotation, bool rectBlueprintMode)
        {
            Quaternion rotation2 = Quaternion.Inverse(rotation);
            Vector3 vector = new Vector3(3.40282347E+38f, 3.40282347E+38f, 3.40282347E+38f);
            Vector3 vector2 = new Vector3(-3.40282347E+38f, -3.40282347E+38f, -3.40282347E+38f);
            Vector3[] array = new Vector3[2];
            GameObject[] gameObjects = Selection.gameObjects;
            for (int i = 0; i < gameObjects.Length; i++)
            {
                GameObject gameObject = gameObjects[i];
                Bounds localBounds = GetLocalBounds(gameObject);
                array[0] = localBounds.min;
                array[1] = localBounds.max;
                for (int j = 0; j < 2; j++)
                {
                    for (int k = 0; k < 2; k++)
                    {
                        for (int l = 0; l < 2; l++)
                        {
                            Vector3 vector3 = new Vector3(array[j].x, array[k].y, array[l].z);
                            if (rectBlueprintMode && SupportsRectLayout(gameObject.transform))
                            {
                                Vector3 localPosition = gameObject.transform.localPosition;
                                localPosition.z = 0f;
                                vector3 = gameObject.transform.parent.TransformPoint(vector3 + localPosition);
                            }
                            else
                            {
                                vector3 = gameObject.transform.TransformPoint(vector3);
                            }
                            vector3 = rotation2 * (vector3 - position);
                            for (int m = 0; m < 3; m++)
                            {
                                vector[m] = Mathf.Min(vector[m], vector3[m]);
                                vector2[m] = Mathf.Max(vector2[m], vector3[m]);
                            }
                        }
                    }
                }
            }
            return new Bounds((vector + vector2) * 0.5f, vector2 - vector);
        }

        internal static bool SupportsRectLayout(Transform tr)
        {
            return !(tr == null) && !(tr.parent == null) && !(tr.GetComponent<RectTransform>() == null) && !(tr.parent.GetComponent<RectTransform>() == null);
        }

        private static Bounds GetLocalBounds(GameObject gameObject)
        {
            RectTransform component = gameObject.GetComponent<RectTransform>();
            Bounds result;
            if (component)
            {
                result = new Bounds(component.rect.center, component.rect.size);
            }
            else
            {
                Renderer component2 = gameObject.GetComponent<Renderer>();
                if (component2 is MeshRenderer)
                {
                    MeshFilter component3 = component2.GetComponent<MeshFilter>();
                    if (component3 != null && component3.sharedMesh != null)
                    {
                        result = component3.sharedMesh.bounds;
                        return result;
                    }
                }
               /* if (component2 is SpriteRenderer)
                {
                    result = ((SpriteRenderer)component2).GetSpriteBounds();
                }
                else if (component2 is SpriteMask)
                {
                    result = ((SpriteMask)component2).GetSpriteBounds();
                }
                else*/
                {
                    result = new Bounds(Vector3.zero, Vector3.zero);
                }
            }
            return result;
        }
    }

    internal class TransformManipulator
    {
        private struct TransformData
        {
            public static Quaternion[] s_Alignments = new Quaternion[]
            {
                Quaternion.LookRotation(Vector3.right, Vector3.up),
                Quaternion.LookRotation(Vector3.right, Vector3.forward),
                Quaternion.LookRotation(Vector3.up, Vector3.forward),
                Quaternion.LookRotation(Vector3.up, Vector3.right),
                Quaternion.LookRotation(Vector3.forward, Vector3.right),
                Quaternion.LookRotation(Vector3.forward, Vector3.up)
            };

            public Transform transform;

            public Vector3 position;

            public Vector3 localPosition;

            public Quaternion rotation;

            public Vector3 scale;

            public RectTransform rectTransform;

            public Rect rect;

            public Vector2 anchoredPosition;

            public Vector2 sizeDelta;

            public static TransformManipulator.TransformData GetData(Transform t)
            {
                TransformManipulator.TransformData result = default(TransformManipulator.TransformData);
                result.SetupTransformValues(t);
                result.rectTransform = t.GetComponent<RectTransform>();
                if (result.rectTransform != null)
                {
                    result.sizeDelta = result.rectTransform.sizeDelta;
                    result.rect = result.rectTransform.rect;
                    result.anchoredPosition = result.rectTransform.anchoredPosition;
                }
                else
                {
                    SpriteRenderer component = t.GetComponent<SpriteRenderer>();
                    if (component != null && component.drawMode != SpriteDrawMode.Simple)
                    {
                        result.sizeDelta = component.size;
                    }
                }
                return result;
            }

            private Quaternion GetRefAlignment(Quaternion targetRotation, Quaternion ownRotation)
            {
                float num = float.NegativeInfinity;
                Quaternion result = Quaternion.identity;
                for (int i = 0; i < TransformManipulator.TransformData.s_Alignments.Length; i++)
                {
                    float num2 = Mathf.Min(new float[]
                    {
                        Mathf.Abs(Vector3.Dot(targetRotation * Vector3.right, ownRotation * TransformManipulator.TransformData.s_Alignments[i] * Vector3.right)),
                        Mathf.Abs(Vector3.Dot(targetRotation * Vector3.up, ownRotation * TransformManipulator.TransformData.s_Alignments[i] * Vector3.up)),
                        Mathf.Abs(Vector3.Dot(targetRotation * Vector3.forward, ownRotation * TransformManipulator.TransformData.s_Alignments[i] * Vector3.forward))
                    });
                    if (num2 > num)
                    {
                        num = num2;
                        result = TransformManipulator.TransformData.s_Alignments[i];
                    }
                }
                return result;
            }

            private void SetupTransformValues(Transform t)
            {
                this.transform = t;
                this.position = t.position;
                this.localPosition = t.localPosition;
                this.rotation = t.rotation;
                this.scale = t.localScale;
            }

            private void SetScaleValue(Vector3 scale)
            {
                this.transform.localScale = scale;
            }

            public void SetScaleDelta(Vector3 scaleDelta, Vector3 scalePivot, Quaternion scaleRotation, bool preferRectResize)
            {
                this.SetPosition(scaleRotation * Vector3.Scale(Quaternion.Inverse(scaleRotation) * (this.position - scalePivot), scaleDelta) + scalePivot);
                Vector3 minDragDifference = ManipulationToolUtility.minDragDifference;
                if (this.transform.parent != null)
                {
                    minDragDifference.x /= this.transform.parent.lossyScale.x;
                    minDragDifference.y /= this.transform.parent.lossyScale.y;
                    minDragDifference.z /= this.transform.parent.lossyScale.z;
                }
                Quaternion ownRotation = (!UnityEditor.Tools.rectBlueprintMode || !HandlesUtil.SupportsRectLayout(this.transform)) ? this.rotation : this.transform.parent.rotation;
                Quaternion refAlignment = this.GetRefAlignment(scaleRotation, ownRotation);
                scaleDelta = refAlignment * scaleDelta;
                scaleDelta = Vector3.Scale(scaleDelta, refAlignment * Vector3.one);
                if (preferRectResize)
                {
                    if (this.rectTransform != null)
                    {
                        Vector2 vector = this.sizeDelta + Vector2.Scale(this.rect.size, scaleDelta) - this.rect.size;
                        vector.x = HandlesUtil.RoundBasedOnMinimumDifference(vector.x, minDragDifference.x);
                        vector.y = HandlesUtil.RoundBasedOnMinimumDifference(vector.y, minDragDifference.y);
                        this.rectTransform.sizeDelta = vector;
                        if (HandlesUtil.RectTransform_isDriven(this.rectTransform))
                        {
                            HandlesUtil.SendReapplyDrivenProperties(this.rectTransform);
                        }
                        return;
                    }
                    SpriteRenderer component = this.transform.GetComponent<SpriteRenderer>();
                    if (component != null && component.drawMode != SpriteDrawMode.Simple)
                    {
                        component.size = Vector2.Scale(this.sizeDelta, scaleDelta);
                        return;
                    }
                }
                this.SetScaleValue(Vector3.Scale(this.scale, scaleDelta));
            }

            private void SetPosition(Vector3 newPosition)
            {
                this.SetPositionDelta(newPosition - this.position);
            }

            public void SetPositionDelta(Vector3 positionDelta)
            {
                Vector3 vector = positionDelta;
                Vector3 minDragDifference = ManipulationToolUtility.minDragDifference;
                if (this.transform.parent != null)
                {
                    vector = this.transform.parent.InverseTransformVector(vector);
                    minDragDifference.x /= this.transform.parent.lossyScale.x;
                    minDragDifference.y /= this.transform.parent.lossyScale.y;
                    minDragDifference.z /= this.transform.parent.lossyScale.z;
                }
                bool flag = Mathf.Approximately(vector.x, 0f);
                bool flag2 = Mathf.Approximately(vector.y, 0f);
                bool flag3 = Mathf.Approximately(vector.z, 0f);
                if (this.rectTransform == null)
                {
                    Vector3 vector2 = this.localPosition + vector;
                    vector2.x = ((!flag) ? HandlesUtil.RoundBasedOnMinimumDifference(vector2.x, minDragDifference.x) : this.localPosition.x);
                    vector2.y = ((!flag2) ? HandlesUtil.RoundBasedOnMinimumDifference(vector2.y, minDragDifference.y) : this.localPosition.y);
                    vector2.z = ((!flag3) ? HandlesUtil.RoundBasedOnMinimumDifference(vector2.z, minDragDifference.z) : this.localPosition.z);
                    this.transform.localPosition = vector2;
                }
                else
                {
                    Vector3 vector3 = this.localPosition + vector;
                    vector3.z = ((!flag3) ? HandlesUtil.RoundBasedOnMinimumDifference(vector3.z, minDragDifference.z) : this.localPosition.z);
                    this.transform.localPosition = vector3;
                    Vector2 vector4 = this.anchoredPosition + (Vector2)vector;
                    vector4.x = ((!flag) ? HandlesUtil.RoundBasedOnMinimumDifference(vector4.x, minDragDifference.x) : this.anchoredPosition.x);
                    vector4.y = ((!flag2) ? HandlesUtil.RoundBasedOnMinimumDifference(vector4.y, minDragDifference.y) : this.anchoredPosition.y);
                    this.rectTransform.anchoredPosition = vector4;
                    if (HandlesUtil.RectTransform_isDriven(this.rectTransform))
                    {
                        
                        HandlesUtil.SendReapplyDrivenProperties(this.rectTransform);
                    }
                }
            }

            public void DebugAlignment(Quaternion targetRotation)
            {
                Quaternion rhs = Quaternion.identity;
                if (!TransformManipulator.individualSpace)
                {
                    rhs = this.GetRefAlignment(targetRotation, this.rotation);
                }
                Vector3 a = this.transform.position;
                float d = HandleUtility.GetHandleSize(a) * 0.25f;
                Color color = Handles.color;
                Handles.color = Color.red;
                Vector3 b = this.rotation * rhs * Vector3.right * d;
                Handles.DrawLine(a - b, a + b);
                Handles.color = Color.green;
                b = this.rotation * rhs * Vector3.up * d;
                Handles.DrawLine(a - b, a + b);
                Handles.color = Color.blue;
                b = this.rotation * rhs * Vector3.forward * d;
                Handles.DrawLine(a - b, a + b);
                Handles.color = color;
            }
        }

        private static EventType s_EventTypeBefore = EventType.Ignore;

        private static TransformManipulator.TransformData[] s_MouseDownState = null;

        private static Vector3 s_StartHandlePosition = Vector3.zero;

        private static Vector3 s_StartLocalHandleOffset = Vector3.zero;

        private static int s_HotControl = 0;

        private static bool s_LockHandle = false;

        public static Vector3 mouseDownHandlePosition
        {
            get
            {
                return TransformManipulator.s_StartHandlePosition;
            }
        }

        public static bool active
        {
            get
            {
                return TransformManipulator.s_MouseDownState != null;
            }
        }

        public static bool individualSpace
        {
            get
            {
                return UnityEditor.Tools.pivotRotation == PivotRotation.Local && UnityEditor.Tools.pivotMode == PivotMode.Pivot;
            }
        }

        private static void BeginEventCheck()
        {
            TransformManipulator.s_EventTypeBefore = Event.current.GetTypeForControl(TransformManipulator.s_HotControl);
        }

        private static EventType EndEventCheck()
        {
            EventType eventType = (TransformManipulator.s_EventTypeBefore == Event.current.GetTypeForControl(TransformManipulator.s_HotControl)) ? EventType.Ignore : TransformManipulator.s_EventTypeBefore;
            TransformManipulator.s_EventTypeBefore = EventType.Ignore;
            if (eventType == EventType.MouseDown)
            {
                TransformManipulator.s_HotControl = GUIUtility.hotControl;
            }
            else if (eventType == EventType.MouseUp)
            {
                TransformManipulator.s_HotControl = 0;
            }
            return eventType;
        }

        public static void BeginManipulationHandling(bool lockHandleWhileDragging)
        {
            TransformManipulator.BeginEventCheck();
            TransformManipulator.s_LockHandle = lockHandleWhileDragging;
        }

        public static EventType EndManipulationHandling()
        {
            EventType eventType = TransformManipulator.EndEventCheck();
            if (eventType == EventType.MouseDown)
            {
                TransformManipulator.RecordMouseDownState(Selection.transforms);
                TransformManipulator.s_StartHandlePosition = UnityEditor.Tools.handlePosition;
                TransformManipulator.s_StartLocalHandleOffset = HandlesUtil.localHandleOffset;
                if (TransformManipulator.s_LockHandle)
                {
                    HandlesUtil.LockHandlePosition();
                }
                HandlesUtil.LockHandleRectRotation();
            }
            else if (TransformManipulator.s_MouseDownState != null && (eventType == EventType.MouseUp || GUIUtility.hotControl != TransformManipulator.s_HotControl))
            {
                TransformManipulator.s_MouseDownState = null;
                if (TransformManipulator.s_LockHandle)
                {
                    HandlesUtil.UnlockHandlePosition();
                }
                HandlesUtil.UnlockHandleRectRotation();
                ManipulationToolUtility.DisableMinDragDifference();
            }
            return eventType;
        }

        private static void RecordMouseDownState(Transform[] transforms)
        {
            TransformManipulator.s_MouseDownState = new TransformManipulator.TransformData[transforms.Length];
            for (int i = 0; i < transforms.Length; i++)
            {
                TransformManipulator.s_MouseDownState[i] = TransformManipulator.TransformData.GetData(transforms[i]);
            }
        }

        private static void SetLocalHandleOffsetScaleDelta(Vector3 scaleDelta, Quaternion pivotRotation)
        {
            Quaternion rotation = Quaternion.Inverse(UnityEditor.Tools.handleRotation) * pivotRotation;
            HandlesUtil.localHandleOffset = Vector3.Scale(Vector3.Scale(TransformManipulator.s_StartLocalHandleOffset, rotation * scaleDelta), rotation * Vector3.one);
        }

        public static void SetScaleDelta(Vector3 scaleDelta, Quaternion pivotRotation)
        {
            if (TransformManipulator.s_MouseDownState != null)
            {
                TransformManipulator.SetLocalHandleOffsetScaleDelta(scaleDelta, pivotRotation);
                for (int i = 0; i < TransformManipulator.s_MouseDownState.Length; i++)
                {
                    TransformManipulator.TransformData transformData = TransformManipulator.s_MouseDownState[i];
                    Undo.RecordObject(transformData.transform, "Scale");
                }
                Vector3 scalePivot = UnityEditor.Tools.handlePosition;
                for (int j = 0; j < TransformManipulator.s_MouseDownState.Length; j++)
                {
                    if (UnityEditor.Tools.pivotMode == PivotMode.Pivot)
                    {
                        scalePivot = TransformManipulator.s_MouseDownState[j].position;
                    }
                    if (TransformManipulator.individualSpace)
                    {
                        pivotRotation = TransformManipulator.s_MouseDownState[j].rotation;
                    }
                    TransformManipulator.s_MouseDownState[j].SetScaleDelta(scaleDelta, scalePivot, pivotRotation, false);
                }
            }
        }

        public static void SetResizeDelta(Vector3 scaleDelta, Vector3 pivotPosition, Quaternion pivotRotation)
        {
            if (TransformManipulator.s_MouseDownState != null)
            {
                TransformManipulator.SetLocalHandleOffsetScaleDelta(scaleDelta, pivotRotation);
                for (int i = 0; i < TransformManipulator.s_MouseDownState.Length; i++)
                {
                    TransformManipulator.TransformData transformData = TransformManipulator.s_MouseDownState[i];
                    if (transformData.rectTransform != null)
                    {
                        Undo.RecordObject(transformData.rectTransform, "Resize");
                    }
                    else
                    {
                        SpriteRenderer component = transformData.transform.GetComponent<SpriteRenderer>();
                        if (component != null && component.drawMode != SpriteDrawMode.Simple)
                        {
                            Undo.RecordObjects(new UnityEngine.Object[]
                            {
                                component,
                                transformData.transform
                            }, "Resize");
                        }
                        else
                        {
                            Undo.RecordObject(transformData.transform, "Resize");
                        }
                    }
                }
                for (int j = 0; j < TransformManipulator.s_MouseDownState.Length; j++)
                {
                    TransformManipulator.s_MouseDownState[j].SetScaleDelta(scaleDelta, pivotPosition, pivotRotation, true);
                }
            }
        }

        public static void SetPositionDelta(Vector3 positionDelta)
        {
            if (TransformManipulator.s_MouseDownState != null)
            {
                for (int i = 0; i < TransformManipulator.s_MouseDownState.Length; i++)
                {
                    TransformManipulator.TransformData transformData = TransformManipulator.s_MouseDownState[i];
                    Undo.RecordObject((!(transformData.rectTransform != null)) ? transformData.transform : transformData.rectTransform, "Move");
                }
                for (int j = 0; j < TransformManipulator.s_MouseDownState.Length; j++)
                {
                    TransformManipulator.s_MouseDownState[j].SetPositionDelta(positionDelta);
                }
            }
        }

        public static void DebugAlignment(Quaternion targetRotation)
        {
            if (TransformManipulator.s_MouseDownState != null)
            {
                for (int i = 0; i < TransformManipulator.s_MouseDownState.Length; i++)
                {
                    TransformManipulator.s_MouseDownState[i].DebugAlignment(targetRotation);
                }
            }
        }
    }

    internal class ManipulationToolUtility
    {
        public delegate void HandleDragChange(string handleName, bool dragging);

        public static ManipulationToolUtility.HandleDragChange handleDragChange;

        public static Vector3 minDragDifference
        {
            get;
            set;
        }

        public static void SetMinDragDifferenceForPos(Vector3 position)
        {
            ManipulationToolUtility.minDragDifference = Vector3.one * (HandleUtility.GetHandleSize(position) / 80f);
        }

        public static void SetMinDragDifferenceForPos(Vector3 position, float multiplier)
        {
            ManipulationToolUtility.minDragDifference = Vector3.one * (HandleUtility.GetHandleSize(position) * multiplier / 80f);
        }

        public static void DisableMinDragDifference()
        {
            ManipulationToolUtility.minDragDifference = Vector3.zero;
        }

        public static void DisableMinDragDifferenceForAxis(int axis)
        {
            Vector2 v = ManipulationToolUtility.minDragDifference;
            v[axis] = 0f;
            ManipulationToolUtility.minDragDifference = v;
        }

        public static void DisableMinDragDifferenceBasedOnSnapping(Vector3 positionBeforeSnapping, Vector3 positionAfterSnapping)
        {
            for (int i = 0; i < 3; i++)
            {
                if (positionBeforeSnapping[i] != positionAfterSnapping[i])
                {
                    ManipulationToolUtility.DisableMinDragDifferenceForAxis(i);
                }
            }
        }

        public static void BeginDragging(string handleName)
        {
            if (ManipulationToolUtility.handleDragChange != null)
            {
                ManipulationToolUtility.handleDragChange(handleName, true);
            }
        }

        public static void EndDragging(string handleName)
        {
            if (ManipulationToolUtility.handleDragChange != null)
            {
                ManipulationToolUtility.handleDragChange(handleName, false);
            }
        }

        public static void DetectDraggingBasedOnMouseDownUp(string handleName, EventType typeBefore)
        {
            if (typeBefore == EventType.MouseDrag && Event.current.type != EventType.MouseDrag)
            {
                ManipulationToolUtility.BeginDragging(handleName);
            }
            else if (typeBefore == EventType.MouseUp && Event.current.type != EventType.MouseUp)
            {
                ManipulationToolUtility.EndDragging(handleName);
            }
        }
    }

    internal class RectHandles
    {
        private class Styles
        {
            public readonly GUIStyle dragdot = "U2D.dragDot";

            public readonly GUIStyle pivotdot = "U2D.pivotDot";

            public readonly GUIStyle dragdotactive = "U2D.dragDotActive";

            public readonly GUIStyle pivotdotactive = "U2D.pivotDotActive";
        }

        private static RectHandles.Styles s_Styles;

        private static int s_LastCursorId;

        private static Vector3[] s_TempVectors = new Vector3[0];

        internal static bool RaycastGUIPointToWorldHit(Vector2 guiPoint, Plane plane, out Vector3 hit)
        {
            Ray ray = HandleUtility.GUIPointToWorldRay(guiPoint);
            float distance = 0f;
            bool flag = plane.Raycast(ray, out distance);
            hit = ((!flag) ? Vector3.zero : ray.GetPoint(distance));
            return flag;
        }

        internal static void DetectCursorChange(int id)
        {
            if (HandleUtility.nearestControl == id)
            {
                RectHandles.s_LastCursorId = id;
                Event.current.Use();
            }
            else if (RectHandles.s_LastCursorId == id)
            {
                RectHandles.s_LastCursorId = 0;
                Event.current.Use();
            }
        }

        internal static Vector3 SideSlider(int id, Vector3 position, Vector3 sideVector, Vector3 direction, float size, Handles.CapFunction capFunction, float snap)
        {
            return RectHandles.SideSlider(id, position, sideVector, direction, size, capFunction, snap, 0f);
        }

        internal static Vector3 SideSlider(int id, Vector3 position, Vector3 sideVector, Vector3 direction, float size, Handles.CapFunction capFunction, float snap, float bias)
        {
            Event current = Event.current;
            Vector3 normalized = Vector3.Cross(sideVector, direction).normalized;
            Vector3 vector = Handles.Slider2D(id, position, normalized, direction, sideVector, 0f, capFunction, Vector2.one * snap);
            vector = position + Vector3.Project(vector - position, direction);
            EventType type = current.type;
            if (type != EventType.Layout)
            {
                if (type != EventType.MouseMove)
                {
                    if (type == EventType.Repaint)
                    {
                        if ((HandleUtility.nearestControl == id && GUIUtility.hotControl == 0) || GUIUtility.hotControl == id)
                        {
                            RectHandles.HandleDirectionalCursor(position, normalized, direction);
                        }
                    }
                }
                else
                {
                    RectHandles.DetectCursorChange(id);
                }
            }
            else
            {
                Vector3 normalized2 = sideVector.normalized;
                HandleUtility.AddControl(id, HandleUtility.DistanceToLine(position + sideVector * 0.5f - normalized2 * size * 2f, position - sideVector * 0.5f + normalized2 * size * 2f) - bias);
            }
            return vector;
        }

        internal static Vector3 CornerSlider(int id, Vector3 cornerPos, Vector3 handleDir, Vector3 outwardsDir1, Vector3 outwardsDir2, float handleSize, Handles.CapFunction drawFunc, Vector2 snap)
        {
            Event current = Event.current;
            Vector3 result = Handles.Slider2D(id, cornerPos, handleDir, outwardsDir1, outwardsDir2, handleSize, drawFunc, snap);
            EventType type = current.type;
            if (type != EventType.MouseMove)
            {
                if (type == EventType.Repaint)
                {
                    if ((HandleUtility.nearestControl == id && GUIUtility.hotControl == 0) || GUIUtility.hotControl == id)
                    {
                        RectHandles.HandleDirectionalCursor(cornerPos, handleDir, outwardsDir1 + outwardsDir2);
                    }
                }
            }
            else
            {
                RectHandles.DetectCursorChange(id);
            }
            return result;
        }

        private static void HandleDirectionalCursor(Vector3 handlePosition, Vector3 handlePlaneNormal, Vector3 direction)
        {
            Vector2 mousePosition = Event.current.mousePosition;
            Plane plane = new Plane(handlePlaneNormal, handlePosition);
            Vector3 worldPos;
            if (RectHandles.RaycastGUIPointToWorldHit(mousePosition, plane, out worldPos))
            {
                Vector2 direction2 = RectHandles.WorldToScreenSpaceDir(worldPos, direction);
                Rect position = new Rect(mousePosition.x - 100f, mousePosition.y - 100f, 200f, 200f);
                EditorGUIUtility.AddCursorRect(position, RectHandles.GetScaleCursor(direction2));
            }
        }

        public static float AngleAroundAxis(Vector3 dirA, Vector3 dirB, Vector3 axis)
        {
            dirA = Vector3.ProjectOnPlane(dirA, axis);
            dirB = Vector3.ProjectOnPlane(dirB, axis);
            float num = Vector3.Angle(dirA, dirB);
            return num * (float)((Vector3.Dot(axis, Vector3.Cross(dirA, dirB)) >= 0f) ? 1 : -1);
        }

        public static float RotationSlider(int id, Vector3 cornerPos, float rotation, Vector3 pivot, Vector3 handleDir, Vector3 outwardsDir1, Vector3 outwardsDir2, float handleSize, Handles.CapFunction drawFunc, Vector2 snap)
        {
            Vector3 b = outwardsDir1 + outwardsDir2;
            Vector2 vector = HandleUtility.WorldToGUIPoint(cornerPos);
            Vector2 b2 = (HandleUtility.WorldToGUIPoint(cornerPos + b) - vector).normalized * 15f;
            RectHandles.RaycastGUIPointToWorldHit(vector + b2, new Plane(handleDir, cornerPos), out cornerPos);
            Event current = Event.current;
            Vector3 a = Handles.Slider2D(id, cornerPos, handleDir, outwardsDir1, outwardsDir2, handleSize, drawFunc, Vector2.zero);
            if (current.type == EventType.MouseMove)
            {
                RectHandles.DetectCursorChange(id);
            }
            if (current.type == EventType.Repaint)
            {
                if ((HandleUtility.nearestControl == id && GUIUtility.hotControl == 0) || GUIUtility.hotControl == id)
                {
                    Rect position = new Rect(current.mousePosition.x - 100f, current.mousePosition.y - 100f, 200f, 200f);
                    EditorGUIUtility.AddCursorRect(position, MouseCursor.RotateArrow);
                }
            }
            return rotation - RectHandles.AngleAroundAxis(a - pivot, cornerPos - pivot, handleDir);
        }

        private static Vector2 WorldToScreenSpaceDir(Vector3 worldPos, Vector3 worldDir)
        {
            Vector3 b = HandleUtility.WorldToGUIPoint(worldPos);
            Vector3 a = HandleUtility.WorldToGUIPoint(worldPos + worldDir);
            Vector2 result = a - b;
            result.y *= -1f;
            return result;
        }

        private static MouseCursor GetScaleCursor(Vector2 direction)
        {
            float num = Mathf.Atan2(direction.x, direction.y) * 57.29578f;
            if (num < 0f)
            {
                num = 360f + num;
            }
            MouseCursor result;
            if (num < 27.5f)
            {
                result = MouseCursor.ResizeVertical;
            }
            else if (num < 72.5f)
            {
                result = MouseCursor.ResizeUpRight;
            }
            else if (num < 117.5f)
            {
                result = MouseCursor.ResizeHorizontal;
            }
            else if (num < 162.5f)
            {
                result = MouseCursor.ResizeUpLeft;
            }
            else if (num < 207.5f)
            {
                result = MouseCursor.ResizeVertical;
            }
            else if (num < 252.5f)
            {
                result = MouseCursor.ResizeUpRight;
            }
            else if (num < 297.5f)
            {
                result = MouseCursor.ResizeHorizontal;
            }
            else if (num < 342.5f)
            {
                result = MouseCursor.ResizeUpLeft;
            }
            else
            {
                result = MouseCursor.ResizeVertical;
            }
            return result;
        }

        public static void RectScalingHandleCap(int controlID, Vector3 position, Quaternion rotation, float size, EventType eventType)
        {
            if (RectHandles.s_Styles == null)
            {
                RectHandles.s_Styles = new RectHandles.Styles();
            }
            if (eventType != EventType.Layout)
            {
                if (eventType == EventType.Repaint)
                {
                    RectHandles.DrawImageBasedCap(controlID, position, rotation, size, RectHandles.s_Styles.dragdot, RectHandles.s_Styles.dragdotactive);
                }
            }
            else
            {
                HandleUtility.AddControl(controlID, HandleUtility.DistanceToCircle(position, size * 0.5f));
            }
        }

        public static void PivotHandleCap(int controlID, Vector3 position, Quaternion rotation, float size, EventType eventType)
        {
            if (RectHandles.s_Styles == null)
            {
                RectHandles.s_Styles = new RectHandles.Styles();
            }
            if (eventType != EventType.Layout)
            {
                if (eventType == EventType.Repaint)
                {
                    RectHandles.DrawImageBasedCap(controlID, position, rotation, size, RectHandles.s_Styles.pivotdot, RectHandles.s_Styles.pivotdotactive);
                }
            }
            else
            {
                HandleUtility.AddControl(controlID, HandleUtility.DistanceToCircle(position, size * 0.5f));
            }
        }

        private static void DrawImageBasedCap(int controlID, Vector3 position, Quaternion rotation, float size, GUIStyle normal, GUIStyle active)
        {
            if (!Camera.current || Vector3.Dot(position - Camera.current.transform.position, Camera.current.transform.forward) >= 0f)
            {
                Vector3 vector = HandleUtility.WorldToGUIPoint(position);
                Handles.BeginGUI();
                float fixedWidth = normal.fixedWidth;
                float fixedHeight = normal.fixedHeight;
                Rect position2 = new Rect(vector.x - fixedWidth / 2f, vector.y - fixedHeight / 2f, fixedWidth, fixedHeight);
                if (GUIUtility.hotControl == controlID)
                {
                    active.Draw(position2, GUIContent.none, controlID);
                }
                else
                {
                    normal.Draw(position2, GUIContent.none, controlID);
                }
                Handles.EndGUI();
            }
        }

        public static void RenderRectWithShadow(bool active, params Vector3[] corners)
        {
            Vector3[] points = new Vector3[]
            {
                corners[0],
                corners[1],
                corners[2],
                corners[3],
                corners[0]
            };
            Color color = Handles.color;
            Handles.color = new Color(1f, 1f, 1f, (!active) ? 0.5f : 1f);
            RectHandles.DrawPolyLineWithShadow(new Color(0f, 0f, 0f, (!active) ? 0.5f : 1f), new Vector2(1f, -1f), points);
            Handles.color = color;
        }

        public static void DrawPolyLineWithShadow(Color shadowColor, Vector2 screenOffset, params Vector3[] points)
        {
            Camera current = Camera.current;
            if (current && Event.current.type == EventType.Repaint)
            {
                if (RectHandles.s_TempVectors.Length != points.Length)
                {
                    RectHandles.s_TempVectors = new Vector3[points.Length];
                }
                for (int i = 0; i < points.Length; i++)
                {
                    RectHandles.s_TempVectors[i] = current.ScreenToWorldPoint(current.WorldToScreenPoint(points[i]) + (Vector3)screenOffset);
                }
                Color color = Handles.color;
                shadowColor.a *= color.a;
                Handles.color = shadowColor;
                Handles.DrawPolyLine(RectHandles.s_TempVectors);
                Handles.color = color;
                Handles.DrawPolyLine(points);
            }
        }

        public static void DrawDottedLineWithShadow(Color shadowColor, Vector2 screenOffset, Vector3 p1, Vector3 p2, float screenSpaceSize)
        {
            Camera current = Camera.current;
            if (current && Event.current.type == EventType.Repaint)
            {
                Color color = Handles.color;
                shadowColor.a *= color.a;
                Handles.color = shadowColor;
                Handles.DrawDottedLine(current.ScreenToWorldPoint(current.WorldToScreenPoint(p1) + (Vector3)screenOffset), current.ScreenToWorldPoint(current.WorldToScreenPoint(p2) + (Vector3)screenOffset), screenSpaceSize);
                Handles.color = color;
                Handles.DrawDottedLine(p1, p2, screenSpaceSize);
            }
        }
    }
}

