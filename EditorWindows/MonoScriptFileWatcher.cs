﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Security.Permissions;
using System.Reflection;
using System.Collections.Generic;
using System.Threading;

namespace MassiveStudio.TeamProjectEditor
{
    public static class MonoScriptFileWatcher //Missing DIFF System
    {
        public static FileSystemWatcher fsw;
        public static bool fswstate = false;

        public static Dictionary<string, MonoEvent> fileschanged = new Dictionary<string, MonoEvent>();
        public static bool _changed = false;

        public static Thread unitywaiter=null;
        public static bool backgroundthreadstate = true;
        public static bool unityready = false;

        public static bool editorisloading = false;

        public static bool updatescripts =false;

        public static List<string> TeamScripts = new List<string>();//Not my scripts -> no local update of this client

        [MenuItem("Team/Background FileWatcher")]
        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        public static void StartWatcher()
        {
            if (!fswstate)
            {
                Debug.Log("BackgroundFileWatcher started");
                fsw = new FileSystemWatcher(TeamSceneManager.DataPath);
                fsw.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;
                fsw.Filter = "*.cs";
                
                fsw.Changed += new FileSystemEventHandler(OnChanged);
                fsw.Created += new FileSystemEventHandler(OnChanged);
                fsw.Deleted += new FileSystemEventHandler(OnChanged);
                fsw.Renamed += new RenamedEventHandler(OnRenamed);

                fsw.EnableRaisingEvents = true;
                fswstate = true;

                backgroundthreadstate = true;
                unitywaiter = new Thread(BackgroundWaiter);
                unitywaiter.IsBackground = true;
                unitywaiter.Start();

                EditorApplication.update += Update;

            }
            else
                StopWatcher();
        }

        public static void Update()
        {
            if (EditorApplication.isCompiling || EditorApplication.isUpdating)
            {
                editorisloading = true;
                if (!unityready)
                    unityready = true;
            }
            else
                editorisloading = false;

            if (updatescripts)
            {
                OnCheck();
                updatescripts = false;
            }
        }

        public static void StopWatcher()
        {
            if (fsw != null)
            {
                fsw.EnableRaisingEvents = false;
                fsw = null;
                fswstate = false;
                Debug.Log("BackgroundFileWatcher stopped");

                backgroundthreadstate = false;
                unitywaiter.Abort();
                Debug.Log("BackgroundUnityThread stopped");
            }
            EditorApplication.update -= Update;
        }

        static void OnDestroy()
        {
            StopWatcher();
        }

        static void BackgroundWaiter()
        {
            Debug.Log("BackgroundUnityThread started");
            while (backgroundthreadstate)
            {
                if (_changed)
                {
                    unityready = false;
                    Debug.Log("Wait for Unity to import changes and let the compiler check the scripts");
                    while (!unityready)
                    {
                            //AFTER editorisloading == TRUE (one time)
                            Thread.Sleep(100);
                    }
                    Debug.Log("Loaded Files");

                   // unityready = false;

                    updatescripts = true;

                    _changed = false;
                }
                else
                {
                    Thread.Sleep(100);
                }
            }   
        }

        public static void OnCheck()//On Mainthread
        {
            if (GetErrorCount() == 0)
            {
                Debug.Log("No Compiler Errors Found");
                foreach (KeyValuePair<string, MonoEvent> a in fileschanged)//Send all file changes to server
                {
                    TeamServerConnection.AddToWriterList(LibaryClasses.CreateMonoScriptUpdateRequest(a.Key, a.Value));
                    Debug.Log("File: " + a.Key + " send to server");
                }
            }
            fileschanged.Clear();
            return;
        }

        public static void OnChanged(object source, FileSystemEventArgs e)
        {
            _changed = true;
            // Specify what is done when a file is changed, created, or deleted.
            Debug.Log("File: " + e.FullPath + " " + e.ChangeType);

            if (e.ChangeType == WatcherChangeTypes.Deleted)
            {
                if (fileschanged.ContainsKey(e.FullPath))
                    fileschanged.Remove(e.FullPath);

                //SERVER CALL DESTROY FILE IF EXISTS
                TeamServerConnection.AddToWriterList(LibaryClasses.CreateMonoScriptUpdateRequest(e.FullPath, MonoEvent.Deleted));
            }
            else if (e.ChangeType == WatcherChangeTypes.Changed || e.ChangeType == WatcherChangeTypes.Created)
            {
                if (!fileschanged.ContainsKey(e.FullPath))
                    fileschanged.Add(e.FullPath, e.ChangeType == WatcherChangeTypes.Created ? MonoEvent.Created : MonoEvent.Changed);
            }
            //UnityEditorInternal.LogEntries.GetCount();
        }

        private static void OnRenamed(object source, RenamedEventArgs e)
        {
            _changed = true;
            // Specify what is done when a file is renamed.
            Debug.Log(string.Format("File: {0} renamed to {1}", e.OldFullPath, e.FullPath));
            if (fileschanged.ContainsKey(e.OldFullPath))
            {
                RenameKey(fileschanged, e.OldFullPath, e.FullPath+"*"+ e.OldFullPath);
            }
        }

        public static void RenameKey<TKey, TValue>(this IDictionary<TKey, TValue> dic, TKey fromKey, TKey toKey)
        {
            TValue value = dic[fromKey];
            dic.Remove(fromKey);
            dic[toKey] = value;
        }

        public static int GetErrorCount()
        {
            /*object[] arguments = new object[3];
            arguments[0] = 0;//Errors
            arguments[1] = 0;//Warning
            arguments[2] = 0;//Info
           ReflectDllMethods.GetMethodInvoked("UnityEditor", "LogEntries", "GetCountsByType", arguments);//internal function -> only on mainthread
           */
            return 0;// (int)arguments[0];
        }

        public static void RegisterTeamFile(string filename)
        {
            TeamScripts.Add(filename);
        }
    }
}