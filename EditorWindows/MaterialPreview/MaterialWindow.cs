﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace MassiveStudio.TeamProjectEditor.MaterialPreview
{
    public class MaterialWindow : EditorWindow
    {
        //public static var MaterialLabels : List.<MaterialList> = new List.<MaterialList>();

        public static MaterialPreviewList List;

        Rect recsize;

        Vector2 materialpreviewsize = new Vector2(100, 100);

        public MaterialList selectedList = null;


        [MenuItem("Window/MaterialWindow")]
        public static void ShowWindow()
        {
            EditorWindow window = EditorWindow.GetWindow(typeof(MaterialWindow));
            window.autoRepaintOnSceneChange = true;
            window.minSize = new Vector2(350, 500);

            if (File.Exists("Assets/MaterialPreviewList.asset"))
                List = AssetDatabase.LoadAssetAtPath("Assets/MaterialPreviewList.asset", typeof(MaterialPreviewList)) as MaterialPreviewList;
            else if (List == null)
            {
                List = CreateMaterialPreviewList.Create();
            }
            //TEST
            List.List.Add(new MaterialList("Materialien"));
            List.List.Add(new MaterialList("Terrain"));
            List.List.Add(new MaterialList("Skybox"));


            List.List[0].child.Add(new MaterialList("Steel"));
            List.List[0].child.Add(new MaterialList("Chrome"));
            List.List[0].child.Add(new MaterialList("Aluminium"));
            List.List[0].child[2].child.Add(new MaterialList("Aluminium wrinkled"));

            List.List[0].child[2].child[0].previews.Add(new MaterialPreview());
            List.List[0].child[2].child[0].previews.Add(new MaterialPreview());
            List.List[0].child[2].child[0].previews.Add(new MaterialPreview());
            List.List[0].child[2].child[0].previews.Add(new MaterialPreview());
            List.List[0].child[2].child[0].previews.Add(new MaterialPreview());
            List.List[0].child[2].child[0].previews.Add(new MaterialPreview());

            List.List[0].child[1].previews.Add(new MaterialPreview());
            List.List[0].child[0].previews.Add(new MaterialPreview());


            List.List[1].child.Add(new MaterialList("Oberfläche"));
            List.List[1].child.Add(new MaterialList("Pflanzen"));

            List.List[1].child[0].child.Add(new MaterialList("Snow"));
            List.List[1].child[0].child.Add(new MaterialList("Gras"));

            //EditorUtility.SetDirty(List);
        }

        void OnReload()
        {
            if (File.Exists("Assets/MaterialPreviewList.asset"))
                List = AssetDatabase.LoadAssetAtPath("Assets/MaterialPreviewList.asset", typeof(MaterialPreviewList)) as MaterialPreviewList;
            else if (List == null)
            {
                List = CreateMaterialPreviewList.Create();
            }
        }

        void OnGUI()
        {
            GUI.BeginGroup(new Rect(0, 0, 200, position.height));
            GUI.Box(new Rect(0, 0, 200, position.height), "");
            if (List == null)
                OnReload();
            else
                DrawMaterialList(List.List, 0);
            GUI.FocusControl(null);
            GUI.EndGroup();

            GUI.BeginGroup(new Rect(200, 0, position.width - 200, position.height));
            if (selectedList != null)
            {
                if (selectedList.previews.Count > 0)
                {
                    Vector2 rec = new Vector2(position.width - 200, position.height);
                    Vector2 p = Vector2.zero;
                    foreach (MaterialPreview a in selectedList.previews)
                    {
                        GUI.Box(new Rect(p.x, p.y, materialpreviewsize.x, materialpreviewsize.y), "");

                        if (p.x + materialpreviewsize.x < (position.width - 200) - materialpreviewsize.x)
                            p.x += materialpreviewsize.x;
                        else
                        {
                            p.x = 0;
                            p.y += materialpreviewsize.y;
                        }
                    }
                }
            }
            GUI.EndGroup();

            //	GUI.VerticalScrollbar(Rect(200,0, position.width-200, position.height)
            //recsize = GUILayoutUtility.GetLastRect();
        }


        public void DrawMaterialList(List<MaterialList> list, int deph)
        {
            Rect r;

            foreach (MaterialList a in list)
            {
                if (a.child.Count > 0)
                {

                    bool s = a.show;

                    if (selectedList == a)
                        a.show = EditorFoldout(a.show, a.name, null, true);
                    else
                        a.show = EditorFoldout(a.show, a.name, null, false);

                    r = GUILayoutUtility.GetLastRect();


                    if (s != a.show)
                    {
                        selectedList = a;
                        Repaint();
                    }

                    if (a.show)
                    {
                        EditorGUI.indentLevel = (deph + 1);
                        DrawMaterialList(a.child, (deph + 1));
                        EditorGUI.indentLevel = (deph);
                    }
                }
                else
                {
                    EditorGUI.indentLevel = (deph + 1);
                    EditorGUILayout.BeginHorizontal();
                    if (selectedList == a)
                        GUI.color = Color.blue;
                    else
                        GUI.color = Color.white;

                    EditorGUILayout.LabelField(a.name);
                    //CLICK EVENT
                    r = GUILayoutUtility.GetLastRect();
                    if (r.Contains(Event.current.mousePosition) && Event.current.type == EventType.MouseDown)
                    {
                        selectedList = a;
                        Repaint();
                    }

                    GUI.color = Color.white;
                    //GUILayout.Label( a.name);	
                    EditorGUILayout.EndHorizontal();
                    EditorGUI.indentLevel = (deph);
                }
            }
        }

        void DownloadMaterial()
        {
        }

        void UpdateMaterialLibary()
        {//TCP connection
        }

        void LoadMaterials()
        {

        }

        bool EditorFoldout(bool state, string text, GUIStyle style, bool sel)
        {
            if (style == null)
                style = EditorStyles.foldout;

            EditorGUILayout.BeginHorizontal();
            bool ret = EditorGUILayout.Toggle(GUIContent.none, state, style);//, GUILayout.MaxWidth(10));

            if (sel)
                GUI.color = Color.blue;
            Rect r = GUILayoutUtility.GetLastRect();
            EditorGUI.LabelField(new Rect(r.x + 15, r.y, r.width - 15, r.height), text);

            GUI.color = Color.white;


            if (Event.current.type == EventType.MouseDown)
            {
                if (r.Contains(Event.current.mousePosition))
                {
                    sel = true;
                }
            }

            EditorGUILayout.EndHorizontal();
            return ret;
        }
    }

    public class MaterialPreviewList : ScriptableObject
    {

        public List<MaterialList> List = new List<MaterialList>();
    }

    public class CreateMaterialPreviewList
    {
        [MenuItem("Assets/Create/MaterialPreviewList")]
        public static MaterialPreviewList Create()
        {
            MaterialPreviewList asset = ScriptableObject.CreateInstance<MaterialPreviewList>();
            AssetDatabase.CreateAsset(asset, "Assets/MaterialPreviewList.asset");
            AssetDatabase.SaveAssets();
            return asset;
        }
    }

    public class MaterialList : System.Object
    {
        public string name;
        public bool show = false;

        public List<MaterialList> child = new List<MaterialList>();

        public List<MaterialPreview> previews = new List<MaterialPreview>();

        public MaterialList(string n)
        {
            this.name = n;
        }
    }

    public class MaterialPreview : System.Object
    {
        Texture2D tex;

        public MaterialPreview() { }
    }
}