﻿/*using UnityEditor.Experimental.AssetImporters;
using UnityEditorInternal.VersionControl;
using System.Runtime.CompilerServices;
using UnityEditor.VersionControl;
using System.Collections.Generic;
using UnityEngine.Profiling;
using UnityEditorInternal;
using System.Reflection;
using UnityEngine;
//using UnityEditor;
using System.Text;
using System.Linq;
using System.IO;
using System;

using static UnityEditor.MonoScript;


//using UnityHelper.UnityEditor;

using MassiveStudio.TeamProjectEditor.EditorWindows.Inspector;
using UnityEditor;

namespace MassiveStudio.TeamProjectEditor.EditorWindows
{
    // Token: 0x020008A8 RID: 2216
    //[EditorWindowTitle(title = "Inspector", useTypeNameAsIconName = true)]
    internal class BetterInspectorWindow : EditorWindow, IHasCustomMenu
    {
        // Token: 0x06004D66 RID: 19814 RVA: 0x00167F0C File Offset: 0x0016610C
        public BetterInspectorWindow()
        {
            this.editorDragging = new EditorDragging(this);
        }

        // Token: 0x17000DCE RID: 3534
        // (get) Token: 0x06004D67 RID: 19815 RVA: 0x00167F74 File Offset: 0x00166174
        internal static BetterInspectorWindow.Styles styles
        {
            get
            {
                BetterInspectorWindow.Styles result;
                if ((result = BetterInspectorWindow.s_Styles) == null)
                {
                    result = (BetterInspectorWindow.s_Styles = new BetterInspectorWindow.Styles());
                }
                return result;
            }
        }

        // Token: 0x06004D68 RID: 19816 RVA: 0x00167FA0 File Offset: 0x001661A0
        private void Awake()
        {
            if (!BetterInspectorWindow.m_AllInspectors.Contains(this))
            {
                BetterInspectorWindow.m_AllInspectors.Add(this);
            }
        }

        // Token: 0x06004D69 RID: 19817 RVA: 0x00167FC0 File Offset: 0x001661C0
        private void OnDestroy()
        {
            if (this.m_PreviewWindow != null)
            {
                this.m_PreviewWindow.Close();
            }
            if (this.m_Tracker != null && !this.m_Tracker.Equals(ActiveEditorTracker.sharedTracker))
            {
                this.m_Tracker.Destroy();
            }
        }

        // Token: 0x06004D6A RID: 19818 RVA: 0x00168018 File Offset: 0x00166218
        protected virtual void OnEnable()
        {
            this.RefreshTitle();
            base.minSize = new Vector2(275f, 50f);
            if (!BetterInspectorWindow.m_AllInspectors.Contains(this))
            {
                BetterInspectorWindow.m_AllInspectors.Add(this);
            }
            this.m_PreviewResizer.Init("InspectorPreview");
            this.m_LabelGUI.OnEnable();
            this.CreateTracker();
        }

        // Token: 0x06004D6B RID: 19819 RVA: 0x0016807D File Offset: 0x0016627D
        protected virtual void OnDisable()
        {
            BetterInspectorWindow.m_AllInspectors.Remove(this);
        }

        // Token: 0x06004D6C RID: 19820 RVA: 0x0016808C File Offset: 0x0016628C
        private void OnLostFocus()
        {
            this.m_LabelGUI.OnLostFocus();
        }

        // Token: 0x06004D6D RID: 19821 RVA: 0x0016809C File Offset: 0x0016629C
        internal static void RepaintAllInspectors()
        {
            foreach (BetterInspectorWindow inspectorWindow in BetterInspectorWindow.m_AllInspectors)
            {
                inspectorWindow.Repaint();
            }
        }

        // Token: 0x06004D6E RID: 19822 RVA: 0x001680F8 File Offset: 0x001662F8
        internal static List<BetterInspectorWindow> GetInspectors()
        {
            return BetterInspectorWindow.m_AllInspectors;
        }

        // Token: 0x06004D6F RID: 19823 RVA: 0x00168112 File Offset: 0x00166312
        private void OnSelectionChange()
        {
            this.m_Previews = null;
            this.m_SelectedPreview = null;
            this.m_TypeSelectionList = null;
            this.m_Parent.ClearKeyboardControl();
            ScriptAttributeUtility.ClearGlobalCache();
            base.Repaint();
        }

        // Token: 0x06004D70 RID: 19824 RVA: 0x00168140 File Offset: 0x00166340
        public static BetterInspectorWindow[] GetAllInspectorWindows()
        {
            return BetterInspectorWindow.m_AllInspectors.ToArray();
        }

        // Token: 0x06004D71 RID: 19825 RVA: 0x0016815F File Offset: 0x0016635F
        private void OnInspectorUpdate()
        {
            this.tracker.VerifyModifiedMonoBehaviours();
            if (this.tracker.isDirty && this.ReadyToRepaint())
            {
                base.Repaint();
            }
        }

        // Token: 0x06004D72 RID: 19826 RVA: 0x00168194 File Offset: 0x00166394
        public virtual void AddItemsToMenu(GenericMenu menu)
        {
            menu.AddItem(new GUIContent("Normal"), this.m_InspectorMode == InspectorMode.Normal, new GenericMenu.MenuFunction(this.SetNormal));
            menu.AddItem(new GUIContent("Debug"), this.m_InspectorMode == InspectorMode.Debug, new GenericMenu.MenuFunction(this.SetDebug));
            if (Unsupported.IsDeveloperBuild())
            {
                menu.AddItem(new GUIContent("Debug-Internal"), this.m_InspectorMode == InspectorMode.DebugInternal, new GenericMenu.MenuFunction(this.SetDebugInternal));
            }
            menu.AddSeparator(string.Empty);
            menu.AddItem(new GUIContent("Lock"), this.isLocked, new GenericMenu.MenuFunction(this.FlipLocked));
        }

        // Token: 0x06004D73 RID: 19827 RVA: 0x00168248 File Offset: 0x00166448
        private void RefreshTitle()
        {
            string icon = "UnityEditor.InspectorWindow";
            if (this.m_InspectorMode == InspectorMode.Normal)
            {
                base.titleContent = EditorGUIUtility.TextContentWithIcon("Inspector", icon);
            }
            else
            {
                base.titleContent = EditorGUIUtility.TextContentWithIcon("Debug", icon);
            }
        }

        // Token: 0x06004D74 RID: 19828 RVA: 0x0016828E File Offset: 0x0016648E
        private void SetMode(InspectorMode mode)
        {
            this.m_InspectorMode = mode;
            this.RefreshTitle();
            this.tracker.inspectorMode = mode;
            this.m_ResetKeyboardControl = true;
        }

        // Token: 0x06004D75 RID: 19829 RVA: 0x001682B1 File Offset: 0x001664B1
        private void SetDebug()
        {
            this.SetMode(InspectorMode.Debug);
        }

        // Token: 0x06004D76 RID: 19830 RVA: 0x001682BB File Offset: 0x001664BB
        private void SetNormal()
        {
            this.SetMode(InspectorMode.Normal);
        }

        // Token: 0x06004D77 RID: 19831 RVA: 0x001682C5 File Offset: 0x001664C5
        private void SetDebugInternal()
        {
            this.SetMode(InspectorMode.DebugInternal);
        }

        // Token: 0x06004D78 RID: 19832 RVA: 0x001682CF File Offset: 0x001664CF
        private void FlipLocked()
        {
            this.isLocked = !this.isLocked;
        }

        // Token: 0x17000DCF RID: 3535
        // (get) Token: 0x06004D79 RID: 19833 RVA: 0x001682E4 File Offset: 0x001664E4
        // (set) Token: 0x06004D7A RID: 19834 RVA: 0x00168304 File Offset: 0x00166504
        public bool isLocked
        {
            get
            {
                return this.tracker.isLocked;
            }
            set
            {
                this.tracker.isLocked = value;
            }
        }

        // Token: 0x06004D7B RID: 19835 RVA: 0x00168314 File Offset: 0x00166514
        private static void DoInspectorDragAndDrop(Rect rect, UnityEngine.Object[] targets)
        {
            if (BetterInspectorWindow.Dragging(rect))
            {
                DragAndDrop.visualMode = InternalEditorUtility.InspectorWindowDrag(targets, Event.current.type == EventType.DragPerform);
                if (Event.current.type == EventType.DragPerform)
                {
                    DragAndDrop.AcceptDrag();
                }
            }
        }

        // Token: 0x06004D7C RID: 19836 RVA: 0x00168364 File Offset: 0x00166564
        private static bool Dragging(Rect rect)
        {
            return (Event.current.type == EventType.DragUpdated || Event.current.type == EventType.DragPerform) && rect.Contains(Event.current.mousePosition);
        }

        // Token: 0x17000DD0 RID: 3536
        // (get) Token: 0x06004D7D RID: 19837 RVA: 0x001683B0 File Offset: 0x001665B0
        public ActiveEditorTracker tracker
        {
            get
            {
                this.CreateTracker();
                return this.m_Tracker;
            }
        }

        // Token: 0x06004D7E RID: 19838 RVA: 0x001683D4 File Offset: 0x001665D4
        protected virtual void CreateTracker()
        {
            if (this.m_Tracker != null)
            {
                this.m_Tracker.inspectorMode = this.m_InspectorMode;
                return;
            }
            ActiveEditorTracker sharedTracker = ActiveEditorTracker.sharedTracker;
            bool flag = BetterInspectorWindow.m_AllInspectors.Any((BetterInspectorWindow i) => i.m_Tracker != null && i.m_Tracker.Equals(sharedTracker));
            this.m_Tracker = ((!flag) ? ActiveEditorTracker.sharedTracker : new ActiveEditorTracker());
            this.m_Tracker.inspectorMode = this.m_InspectorMode;
            this.m_Tracker.RebuildIfNecessary();
        }

        // Token: 0x06004D7F RID: 19839 RVA: 0x00168464 File Offset: 0x00166664
        protected virtual void CreatePreviewables()
        {
            if (this.m_Previews == null)
            {
                this.m_Previews = new List<IPreviewable>();
                if (this.tracker.activeEditors.Length != 0)
                {
                    foreach (Editor editor in this.tracker.activeEditors)
                    {
                        IEnumerable<IPreviewable> previewsForType = this.GetPreviewsForType(editor);
                        foreach (IPreviewable item in previewsForType)
                        {
                            this.m_Previews.Add(item);
                        }
                    }
                }
            }
        }

        // Token: 0x06004D80 RID: 19840 RVA: 0x0016852C File Offset: 0x0016672C
        private IEnumerable<IPreviewable> GetPreviewsForType(Editor editor)
        {
            List<IPreviewable> list = new List<IPreviewable>();
            foreach (Assembly assembly in EditorAssemblies.loadedAssemblies)
            {
                Type[] typesFromAssembly = AssemblyHelper.GetTypesFromAssembly(assembly);
                foreach (Type type in typesFromAssembly)
                {
                    if (typeof(IPreviewable).IsAssignableFrom(type))
                    {
                        if (!typeof(Editor).IsAssignableFrom(type))
                        {
                            object[] customAttributes = type.GetCustomAttributes(typeof(CustomPreviewAttribute), false);
                            foreach (CustomPreviewAttribute customPreviewAttribute in customAttributes)
                            {
                                if (!(editor.target == null) && customPreviewAttribute.m_Type == editor.target.GetType())
                                {
                                    IPreviewable previewable = Activator.CreateInstance(type) as IPreviewable;
                                    previewable.Initialize(editor.targets);
                                    list.Add(previewable);
                                }
                            }
                        }
                    }
                }
            }
            return list;
        }

        // Token: 0x06004D81 RID: 19841 RVA: 0x00168660 File Offset: 0x00166860
        protected virtual void ShowButton(Rect r)
        {
            bool flag = GUI.Toggle(r, this.isLocked, GUIContent.none, BetterInspectorWindow.styles.lockButton);
            if (flag != this.isLocked)
            {
                this.isLocked = flag;
                this.tracker.RebuildIfNecessary();
            }
        }

        // Token: 0x06004D82 RID: 19842 RVA: 0x001686AC File Offset: 0x001668AC
        protected virtual void OnGUI()
        {
            Profiler.BeginSample("InspectorWindow.OnGUI");
            this.CreatePreviewables();
            BetterInspectorWindow.FlushAllOptimizedGUIBlocksIfNeeded();
            this.ResetKeyboardControl();
            this.m_ScrollPosition = EditorGUILayout.BeginVerticalScrollView(this.m_ScrollPosition, new GUILayoutOption[0]);
            if (Event.current.type == EventType.Repaint)
            {
                this.tracker.ClearDirty();
            }
            BetterInspectorWindow.s_CurrentInspectorWindow = this;
            Editor[] activeEditors = this.tracker.activeEditors;
            this.AssignAssetEditor(activeEditors);
            Profiler.BeginSample("InspectorWindow.DrawEditors()");
            this.DrawEditors(activeEditors);
            Profiler.EndSample();
            if (this.tracker.hasComponentsWhichCannotBeMultiEdited)
            {
                if (activeEditors.Length == 0 && !this.tracker.isLocked && Selection.objects.Length > 0)
                {
                    this.DrawSelectionPickerList();
                }
                else
                {
                    Rect rect = GUILayoutUtility.GetRect(10f, 4f, EditorStyles.inspectorTitlebar);
                    if (Event.current.type == EventType.Repaint)
                    {
                        this.DrawSplitLine(rect.y);
                    }
                    GUILayout.Label("Components that are only on some of the selected objects cannot be multi-edited.", EditorStyles.helpBox, new GUILayoutOption[0]);
                    GUILayout.Space(4f);
                }
            }
            BetterInspectorWindow.s_CurrentInspectorWindow = null;
            EditorGUI.indentLevel = 0;
            this.AddComponentButton(this.tracker.activeEditors);
            GUI.enabled = true;
            this.CheckDragAndDrop(this.tracker.activeEditors);
            this.MoveFocusOnKeyPress();
            EditorGUILayout.EndScrollView();
            Profiler.BeginSample("InspectorWindow.DrawPreviewAndLabels");
            this.DrawPreviewAndLabels();
            Profiler.EndSample();
            if (this.tracker.activeEditors.Length > 0)
            {
                this.DrawVCSShortInfo();
            }
            Profiler.EndSample();
        }

        // Token: 0x06004D83 RID: 19843 RVA: 0x00168844 File Offset: 0x00166A44
        public virtual Editor GetLastInteractedEditor()
        {
            return this.m_LastInteractedEditor;
        }

        // Token: 0x06004D84 RID: 19844 RVA: 0x00168860 File Offset: 0x00166A60
        public IPreviewable GetEditorThatControlsPreview(IPreviewable[] editors)
        {
            IPreviewable result;
            if (editors.Length == 0)
            {
                result = null;
            }
            else if (this.m_SelectedPreview != null)
            {
                result = this.m_SelectedPreview;
            }
            else
            {
                IPreviewable lastInteractedEditor = this.GetLastInteractedEditor();
                Type type = (lastInteractedEditor == null) ? null : lastInteractedEditor.GetType();
                IPreviewable previewable = null;
                IPreviewable previewable2 = null;
                foreach (IPreviewable previewable3 in editors)
                {
                    if (previewable3 != null && !(previewable3.target == null))
                    {
                        if (!EditorUtility.IsPersistent(previewable3.target) || !(AssetDatabase.GetAssetPath(previewable3.target) != AssetDatabase.GetAssetPath(editors[0].target)))
                        {
                            if (!(editors[0] is AssetImporterEditor) || previewable3 is AssetImporterEditor)
                            {
                                if (previewable3.HasPreviewGUI())
                                {
                                    if (previewable3 == lastInteractedEditor)
                                    {
                                        return previewable3;
                                    }
                                    if (previewable2 == null && previewable3.GetType() == type)
                                    {
                                        previewable2 = previewable3;
                                    }
                                    if (previewable == null)
                                    {
                                        previewable = previewable3;
                                    }
                                }
                            }
                        }
                    }
                }
                if (previewable2 != null)
                {
                    result = previewable2;
                }
                else if (previewable != null)
                {
                    result = previewable;
                }
                else
                {
                    result = null;
                }
            }
            return result;
        }

        // Token: 0x06004D85 RID: 19845 RVA: 0x001689B4 File Offset: 0x00166BB4
        public IPreviewable[] GetEditorsWithPreviews(Editor[] editors)
        {
            IList<IPreviewable> list = new List<IPreviewable>();
            int num = -1;
            foreach (Editor editor in editors)
            {
                num++;
                if (!(editor.target == null))
                {
                    if (!EditorUtility.IsPersistent(editor.target) || !(AssetDatabase.GetAssetPath(editor.target) != AssetDatabase.GetAssetPath(editors[0].target)))
                    {
                        if (EditorUtility.IsPersistent(editors[0].target) || !EditorUtility.IsPersistent(editor.target))
                        {
                            if (!this.ShouldCullEditor(editors, num))
                            {
                                if (!(editors[0] is AssetImporterEditor) || editor is AssetImporterEditor)
                                {
                                    if (editor.HasPreviewGUI())
                                    {
                                        list.Add(editor);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            foreach (IPreviewable previewable in this.m_Previews)
            {
                if (previewable.HasPreviewGUI())
                {
                    list.Add(previewable);
                }
            }
            return list.ToArray<IPreviewable>();
        }

        // Token: 0x06004D86 RID: 19846 RVA: 0x00168B18 File Offset: 0x00166D18
        public UnityEngine.Object GetInspectedObject()
        {
            Editor firstNonImportInspectorEditor = this.GetFirstNonImportInspectorEditor(this.tracker.activeEditors);
            UnityEngine.Object result;
            if (firstNonImportInspectorEditor == null)
            {
                result = null;
            }
            else
            {
                result = firstNonImportInspectorEditor.target;
            }
            return result;
        }

        // Token: 0x06004D87 RID: 19847 RVA: 0x00168B58 File Offset: 0x00166D58
        private Editor GetFirstNonImportInspectorEditor(Editor[] editors)
        {
            foreach (Editor editor in editors)
            {
                if (!(editor.target is AssetImporter))
                {
                    return editor;
                }
            }
            return null;
        }

        // Token: 0x06004D88 RID: 19848 RVA: 0x00168BA8 File Offset: 0x00166DA8
        private void MoveFocusOnKeyPress()
        {
            KeyCode keyCode = Event.current.keyCode;
            if (Event.current.type == EventType.KeyDown && (keyCode == KeyCode.DownArrow || keyCode == KeyCode.UpArrow || keyCode == KeyCode.Tab))
            {
                if (keyCode != KeyCode.Tab)
                {
                    EditorGUIUtility.MoveFocusAndScroll(keyCode == KeyCode.DownArrow);
                }
                else
                {
                    EditorGUIUtility.ScrollForTabbing(!Event.current.shift);
                }
                Event.current.Use();
            }
        }

        // Token: 0x06004D89 RID: 19849 RVA: 0x00168C2A File Offset: 0x00166E2A
        private void ResetKeyboardControl()
        {
            if (this.m_ResetKeyboardControl)
            {
                GUIUtility.keyboardControl = 0;
                this.m_ResetKeyboardControl = false;
            }
        }

        // Token: 0x06004D8A RID: 19850 RVA: 0x00168C48 File Offset: 0x00166E48
        private void CheckDragAndDrop(Editor[] editors)
        {
            Rect rect = GUILayoutUtility.GetRect(GUIContent.none, GUIStyle.none, new GUILayoutOption[]
            {
                GUILayout.ExpandHeight(true)
            });
            if (rect.Contains(Event.current.mousePosition))
            {
                Editor firstNonImportInspectorEditor = this.GetFirstNonImportInspectorEditor(editors);
                if (firstNonImportInspectorEditor != null)
                {
                    BetterInspectorWindow.DoInspectorDragAndDrop(rect, firstNonImportInspectorEditor.targets);
                }
                if (Event.current.type == EventType.MouseDown)
                {
                    GUIUtility.keyboardControl = 0;
                    Event.current.Use();
                }
            }
            this.editorDragging.HandleDraggingToBottomArea(rect, this.m_Tracker);
        }

        // Token: 0x06004D8B RID: 19851 RVA: 0x00168CE0 File Offset: 0x00166EE0
        private static bool HasLabel(UnityEngine.Object target)
        {
            return BetterInspectorWindow.HasLabel(target, AssetDatabase.GetAssetPath(target));
        }

        // Token: 0x06004D8C RID: 19852 RVA: 0x00168D04 File Offset: 0x00166F04
        private static bool HasLabel(UnityEngine.Object target, string assetPath)
        {
            return EditorUtility.IsPersistent(target) && assetPath.StartsWith("assets", StringComparison.OrdinalIgnoreCase);
        }

        // Token: 0x06004D8D RID: 19853 RVA: 0x00168D34 File Offset: 0x00166F34
        private UnityEngine.Object[] GetInspectedAssets()
        {
            Editor firstNonImportInspectorEditor = this.GetFirstNonImportInspectorEditor(this.tracker.activeEditors);
            if (firstNonImportInspectorEditor != null && firstNonImportInspectorEditor.targets.Length == 1)
            {
                string assetPath = AssetDatabase.GetAssetPath(firstNonImportInspectorEditor.target);
                if (BetterInspectorWindow.HasLabel(firstNonImportInspectorEditor.target, assetPath) && !Directory.Exists(assetPath))
                {
                    return firstNonImportInspectorEditor.targets;
                }
            }
            IEnumerable<UnityEngine.Object> objects = Selection.objects;
            if (BetterInspectorWindow.func1 == null)
			{
                BetterInspectorWindow.func1 = new Func<UnityEngine.Object, bool>(BetterInspectorWindow.HasLabel);
            }
            return objects.Where(BetterInspectorWindow.func1).ToArray<UnityEngine.Object>();
        }

        // Token: 0x06004D8E RID: 19854 RVA: 0x00168DD8 File Offset: 0x00166FD8
        private void DrawPreviewAndLabels()
        {
            if (this.m_PreviewWindow && Event.current.type == EventType.Repaint)
            {
                this.m_PreviewWindow.Repaint();
            }
            IPreviewable[] editorsWithPreviews = this.GetEditorsWithPreviews(this.tracker.activeEditors);
            IPreviewable editorThatControlsPreview = this.GetEditorThatControlsPreview(editorsWithPreviews);
            bool flag = editorThatControlsPreview != null && editorThatControlsPreview.HasPreviewGUI() && this.m_PreviewWindow == null;
            UnityEngine.Object[] inspectedAssets = this.GetInspectedAssets();
            bool flag2 = inspectedAssets.Length > 0;
            bool flag3 = inspectedAssets.Any(new Func<UnityEngine.Object, bool>(BetterInspectorWindow.< DrawPreviewAndLabels > m__0));
            if (flag || flag2)
            {
                Event current = Event.current;
                Rect position = EditorGUILayout.BeginHorizontal(BetterInspectorWindow.styles.preToolbar, new GUILayoutOption[]
                {
                    GUILayout.Height(17f)
                });
                Rect position2 = default(Rect);
                GUILayout.FlexibleSpace();
                Rect lastRect = GUILayoutUtility.GetLastRect();
                GUIContent content;
                if (flag)
                {
                    GUIContent previewTitle = editorThatControlsPreview.GetPreviewTitle();
                    content = (previewTitle ?? BetterInspectorWindow.styles.preTitle);
                }
                else
                {
                    content = BetterInspectorWindow.styles.labelTitle;
                }
                position2.x = lastRect.x + 3f;
                position2.y = lastRect.y + (17f - BetterInspectorWindow.s_Styles.dragHandle.fixedHeight) / 2f + 1f;
                position2.width = lastRect.width - 6f;
                position2.height = BetterInspectorWindow.s_Styles.dragHandle.fixedHeight;
                if (editorsWithPreviews.Length > 1)
                {
                    Vector2 vector = BetterInspectorWindow.styles.preDropDown.CalcSize(content);
                    float a = position2.xMax - lastRect.xMin - 3f - 20f;
                    float num = Mathf.Min(a, vector.x);
                    Rect position3 = new Rect(lastRect.x, lastRect.y, num, vector.y);
                    lastRect.xMin += num;
                    position2.xMin += num;
                    GUIContent[] array = new GUIContent[editorsWithPreviews.Length];
                    int selected = -1;
                    for (int i = 0; i < editorsWithPreviews.Length; i++)
                    {
                        IPreviewable previewable = editorsWithPreviews[i];
                        GUIContent guicontent = previewable.GetPreviewTitle() ?? BetterInspectorWindow.styles.preTitle;
                        string text;
                        if (guicontent == BetterInspectorWindow.styles.preTitle)
                        {
                            string str = GetTypeName(previewable.target);
                            if (previewable.target is MonoBehaviour)
                            {
                                str = UnityEditor.MonoScript.FromMonoBehaviour(previewable.target as MonoBehaviour).GetClass().Name;
                            }
                            text = guicontent.text + " - " + str;
                        }
                        else
                        {
                            text = guicontent.text;
                        }
                        array[i] = new GUIContent(text);
                        if (editorsWithPreviews[i] == editorThatControlsPreview)
                        {
                            selected = i;
                        }
                    }
                    if (GUI.Button(position3, content, BetterInspectorWindow.styles.preDropDown))
                    {
                        EditorUtility.DisplayCustomMenu(position3, array, selected, new EditorUtility.SelectMenuItemFunction(this.OnPreviewSelected), editorsWithPreviews);
                    }
                }
                else
                {
                    float a2 = position2.xMax - lastRect.xMin - 3f - 20f;
                    float width = Mathf.Min(a2, BetterInspectorWindow.styles.preToolbar2.CalcSize(content).x);
                    Rect position4 = new Rect(lastRect.x, lastRect.y, width, lastRect.height);
                    position2.xMin = position4.xMax + 3f;
                    GUI.Label(position4, content, BetterInspectorWindow.styles.preToolbar2);
                }
                if (flag && Event.current.type == EventType.Repaint)
                {
                    BetterInspectorWindow.s_Styles.dragHandle.Draw(position2, GUIContent.none, false, false, false, false);
                }
                if (flag && this.m_PreviewResizer.GetExpandedBeforeDragging())
                {
                    editorThatControlsPreview.OnPreviewSettings();
                }
                EditorGUILayout.EndHorizontal();
                if (current.type == EventType.MouseUp && current.button == 1 && position.Contains(current.mousePosition) && this.m_PreviewWindow == null)
                {
                    this.DetachPreview();
                }
                float height;
                if (flag)
                {
                    Rect position5 = base.position;
                    if (EditorSettings.externalVersionControl != ExternalVersionControl.Disabled && EditorSettings.externalVersionControl != ExternalVersionControl.AutoDetect && EditorSettings.externalVersionControl != ExternalVersionControl.Generic)
                    {
                        position5.height -= 17f;
                    }
                    height = this.m_PreviewResizer.ResizeHandle(position5, 100f, 100f, 17f, lastRect);
                }
                else
                {
                    if (GUI.Button(position, GUIContent.none, GUIStyle.none))
                    {
                        this.m_PreviewResizer.ToggleExpanded();
                    }
                    height = 0f;
                }
                if (this.m_PreviewResizer.GetExpanded())
                {
                    GUILayout.BeginVertical(BetterInspectorWindow.styles.preBackground, new GUILayoutOption[]
                    {
                        GUILayout.Height(height)
                    });
                    if (flag)
                    {
                        editorThatControlsPreview.DrawPreview(GUILayoutUtility.GetRect(0f, 10240f, 64f, 10240f));
                    }
                    if (flag2)
                    {
                        using (new EditorGUI.DisabledScope(inspectedAssets.Any(new Func<UnityEngine.Object, bool>(BetterInspectorWindow.< DrawPreviewAndLabels > m__1))))
                        {
                            this.m_LabelGUI.OnLabelGUI(inspectedAssets);
                        }
                    }
                    if (flag3)
                    {
                        this.m_AssetBundleNameGUI.OnAssetBundleNameGUI(inspectedAssets);
                    }
                    GUILayout.EndVertical();
                }
            }
        }


        internal static string GetTypeName(UnityEngine.Object obj)
        {
            string result;
            if (obj == null)
            {
                result = "Object";
            }
            else
            {
                string text = AssetDatabase.GetAssetPath(obj).ToLower();
                if (text.EndsWith(".unity"))
                {
                    result = "Scene";
                }
                else if (text.EndsWith(".guiskin"))
                {
                    result = "GUI Skin";
                }
                else if (Directory.Exists(AssetDatabase.GetAssetPath(obj)))
                {
                    result = "Folder";
                }
                else if (obj.GetType() == typeof(UnityEngine.Object))
                {
                    result = Path.GetExtension(text) + " File";
                }
                else
                {
                    result = ObjectNames.GetClassName(obj);
                }
            }
            return result;
        }

        // Token: 0x06004D8F RID: 19855 RVA: 0x001693C0 File Offset: 0x001675C0
        protected UnityEngine.Object[] GetTargetsForPreview(IPreviewable previewEditor)
        {
            Editor editor = null;
            foreach (Editor editor2 in this.tracker.activeEditors)
            {
                if (editor2.target.GetType() == previewEditor.target.GetType())
                {
                    editor = editor2;
                    break;
                }
            }
            return editor.targets;
        }

        // Token: 0x06004D90 RID: 19856 RVA: 0x00169430 File Offset: 0x00167630
        private void OnPreviewSelected(object userData, string[] options, int selected)
        {
            IPreviewable[] array = userData as IPreviewable[];
            this.m_SelectedPreview = array[selected];
        }

        // Token: 0x06004D91 RID: 19857 RVA: 0x00169450 File Offset: 0x00167650
        private void DetachPreview()
        {
            Event.current.Use();
            this.m_PreviewWindow = (ScriptableObject.CreateInstance(typeof(PreviewWindow)) as PreviewWindow);
            this.m_PreviewWindow.SetParentInspector(this);
            this.m_PreviewWindow.Show();
            base.Repaint();
            GUIUtility.ExitGUI();
        }

        // Token: 0x06004D92 RID: 19858 RVA: 0x001694A4 File Offset: 0x001676A4
        protected virtual void DrawVCSSticky(float offset)
        {
            string text = "";
            Editor firstNonImportInspectorEditor = this.GetFirstNonImportInspectorEditor(this.tracker.activeEditors);
            if (!EditorPrefs.GetBool("vcssticky") && !Editor.IsAppropriateFileOpenForEdit(firstNonImportInspectorEditor.target, out text))
            {
                Rect position = new Rect(10f, base.position.height - 94f, base.position.width - 20f, 80f);
                position.y -= offset;
                if (Event.current.type == EventType.Repaint)
                {
                    BetterInspectorWindow.styles.stickyNote.Draw(position, false, false, false, false);
                    Rect position2 = new Rect(position.x, position.y + position.height / 2f - 32f, 64f, 64f);
                    if (EditorSettings.externalVersionControl == "Perforce")
                    {
                        BetterInspectorWindow.styles.stickyNotePerforce.Draw(position2, false, false, false, false);
                    }
                    Rect position3 = new Rect(position.x + position2.width, position.y, position.width - position2.width, position.height);
                    GUI.Label(position3, new GUIContent("<b>Under Version Control</b>\nCheck out this asset in order to make changes."), BetterInspectorWindow.styles.stickyNoteLabel);
                    Rect position4 = new Rect(position.x + position.width / 2f, position.y + 80f, 19f, 14f);
                    BetterInspectorWindow.styles.stickyNoteArrow.Draw(position4, false, false, false, false);
                }
            }
        }

        // Token: 0x06004D93 RID: 19859 RVA: 0x00169654 File Offset: 0x00167854
        private void DrawVCSShortInfo()
        {
            if (Provider.enabled && EditorSettings.externalVersionControl != ExternalVersionControl.Disabled && EditorSettings.externalVersionControl != ExternalVersionControl.AutoDetect && EditorSettings.externalVersionControl != ExternalVersionControl.Generic)
            {
                Editor firstNonImportInspectorEditor = this.GetFirstNonImportInspectorEditor(this.tracker.activeEditors);
                string assetPath = AssetDatabase.GetAssetPath(firstNonImportInspectorEditor.target);
                Asset assetByPath = Provider.GetAssetByPath(assetPath);
                if (assetByPath != null && (assetByPath.path.StartsWith("Assets") || assetByPath.path.StartsWith("ProjectSettings")))
                {
                    Asset assetByPath2 = Provider.GetAssetByPath(assetPath.Trim(new char[]
                    {
                        '/'
                    }) + ".meta");
                    string text = assetByPath.StateToString();
                    string text2 = (assetByPath2 != null) ? assetByPath2.StateToString() : string.Empty;
                    if (text == string.Empty && Provider.onlineState != OnlineState.Online)
                    {
                        text = string.Format(BetterInspectorWindow.s_Styles.VCS_NotConnectedMessage.text, Provider.GetActivePlugin().name);
                    }
                    bool flag = assetByPath2 != null && (assetByPath2.state & ~Asset.States.MetaFile) != assetByPath.state;
                    bool flag2 = text != "";
                    float height = (!flag || !flag2) ? 17f : 34f;
                    GUILayout.Label(GUIContent.none, BetterInspectorWindow.styles.preToolbar, new GUILayoutOption[]
                    {
                        GUILayout.Height(height)
                    });
                    Rect lastRect = GUILayoutUtility.GetLastRect();
                    bool flag3 = Event.current.type == EventType.Layout || Event.current.type == EventType.Repaint;
                    if (flag2 && flag3)
                    {
                        Texture2D icon = AssetDatabase.GetCachedIcon(assetPath) as Texture2D;
                        if (flag)
                        {
                            Rect rect = lastRect;
                            rect.height = 17f;
                            this.DrawVCSShortInfoAsset(assetByPath, this.BuildTooltip(assetByPath, null), rect, icon, text);
                            Texture2D iconForFile = InternalEditorUtility.GetIconForFile(assetByPath2.path);
                            rect.y += 17f;
                            this.DrawVCSShortInfoAsset(assetByPath2, this.BuildTooltip(null, assetByPath2), rect, iconForFile, text2);
                        }
                        else
                        {
                            this.DrawVCSShortInfoAsset(assetByPath, this.BuildTooltip(assetByPath, assetByPath2), lastRect, icon, text);
                        }
                    }
                    else if (text2 != "" && flag3)
                    {
                        Texture2D iconForFile2 = InternalEditorUtility.GetIconForFile(assetByPath2.path);
                        this.DrawVCSShortInfoAsset(assetByPath2, this.BuildTooltip(assetByPath, assetByPath2), lastRect, iconForFile2, text2);
                    }
                    string text3 = "";
                    if (!Editor.IsAppropriateFileOpenForEdit(firstNonImportInspectorEditor.target, out text3))
                    {
                        if (Provider.isActive)
                        {
                            float num = 80f;
                            Rect position = new Rect(lastRect.x + lastRect.width - num, lastRect.y, num, lastRect.height);
                            if (GUI.Button(position, "Check out", BetterInspectorWindow.styles.lockedHeaderButton))
                            {
                                EditorPrefs.SetBool("vcssticky", true);
                                Task task = Provider.Checkout(firstNonImportInspectorEditor.targets, CheckoutMode.Both);
                                task.Wait();
                                base.Repaint();
                            }
                        }
                        this.DrawVCSSticky(lastRect.height / 2f);
                    }
                }
            }
        }

        // Token: 0x06004D94 RID: 19860 RVA: 0x001699B0 File Offset: 0x00167BB0
        protected string BuildTooltip(Asset asset, Asset metaAsset)
        {
            StringBuilder stringBuilder = new StringBuilder();
            if (asset != null)
            {
                stringBuilder.AppendLine("Asset:");
                stringBuilder.AppendLine(asset.AllStateToString());
            }
            if (metaAsset != null)
            {
                stringBuilder.AppendLine("Meta file:");
                stringBuilder.AppendLine(metaAsset.AllStateToString());
            }
            return stringBuilder.ToString();
        }

        // Token: 0x06004D95 RID: 19861 RVA: 0x00169A14 File Offset: 0x00167C14
        protected void DrawVCSShortInfoAsset(Asset asset, string tooltip, Rect rect, Texture2D icon, string currentState)
        {
            Rect rect2 = new Rect(rect.x, rect.y, 28f, 16f);
            Rect position = rect2;
            position.x += 6f;
            position.width = 16f;
            if (icon != null)
            {
                GUI.DrawTexture(position, icon);
            }
            Overlay.DrawOverlay(asset, rect2);
            Rect position2 = new Rect(rect.x + 26f, rect.y, rect.width - 31f, rect.height);
            GUIContent guicontent = new GUIContent(currentState);
            guicontent.tooltip = tooltip;
            EditorGUI.LabelField(position2, guicontent, BetterInspectorWindow.styles.preToolbar2);
        }

        // Token: 0x06004D96 RID: 19862 RVA: 0x00169ACD File Offset: 0x00167CCD
        protected void AssignAssetEditor(Editor[] editors)
        {
            if (editors.Length > 1 && editors[0] is AssetImporterEditor)
            {
                (editors[0] as AssetImporterEditor).assetEditor = editors[1];
            }
        }

        // Token: 0x06004D97 RID: 19863 RVA: 0x00169AF8 File Offset: 0x00167CF8
        private void DrawEditors(Editor[] editors)
        {
            if (editors.Length != 0)
            {
                UnityEngine.Object inspectedObject = this.GetInspectedObject();
                string empty = string.Empty;
                GUILayout.Space(0f);
                if (inspectedObject is Material)
                {
                    int num = 0;
                    while (num <= 1 && num < editors.Length)
                    {
                        MaterialEditor materialEditor = editors[num] as MaterialEditor;
                        if (materialEditor != null)
                        {
                            materialEditor.forceVisible = true;
                            break;
                        }
                        num++;
                    }
                }
                bool rebuildOptimizedGUIBlock = false;
                if (Event.current.type == EventType.Repaint)
                {
                    if (inspectedObject != null && this.m_IsOpenForEdit != Editor.IsAppropriateFileOpenForEdit(inspectedObject, out empty))
                    {
                        this.m_IsOpenForEdit = !this.m_IsOpenForEdit;
                        rebuildOptimizedGUIBlock = true;
                    }
                    if (this.m_InvalidateGUIBlockCache)
                    {
                        rebuildOptimizedGUIBlock = true;
                        this.m_InvalidateGUIBlockCache = false;
                    }
                }
                else if (Event.current.type == EventType.ExecuteCommand && Event.current.commandName == "EyeDropperUpdate")
                {
                    rebuildOptimizedGUIBlock = true;
                }
                Editor.m_AllowMultiObjectAccess = true;
                bool flag = false;
                Rect position = default(Rect);
                for (int i = 0; i < editors.Length; i++)
                {
                    if (this.ShouldCullEditor(editors, i))
                    {
                        if (Event.current.type == EventType.Repaint)
                        {
                            editors[i].isInspectorDirty = false;
                        }
                    }
                    else
                    {
                        bool textFieldInput = GUIUtility.textFieldInput;
                        this.DrawEditor(editors, i, rebuildOptimizedGUIBlock, ref flag, ref position);
                        if (Event.current.type == EventType.Repaint && !textFieldInput && GUIUtility.textFieldInput)
                        {
                            BetterInspectorWindow.FlushOptimizedGUIBlock(editors[i]);
                        }
                    }
                }
                EditorGUIUtility.ResetGUIState();
                if (position.height > 0f)
                {
                    GUI.BeginGroup(position);
                    GUI.Label(new Rect(0f, 0f, position.width, position.height), "Imported Object", "OL Title");
                    GUI.EndGroup();
                }
            }
        }

        // Token: 0x06004D98 RID: 19864 RVA: 0x00169CF9 File Offset: 0x00167EF9
        internal override void OnResized()
        {
            this.m_InvalidateGUIBlockCache = true;
        }

        // Token: 0x06004D99 RID: 19865 RVA: 0x00169D04 File Offset: 0x00167F04
        private void DrawEditor(Editor[] editors, int editorIndex, bool rebuildOptimizedGUIBlock, ref bool showImportedObjectBarNext, ref Rect importedObjectBarRect)
        {
            Editor editor = editors[editorIndex];
            if (!(editor == null))
            {
                UnityEngine.Object target = editor.target;
                if (target || target.GetType() == typeof(MonoBehaviour))
                {
                    GUIUtility.GetControlID(target.GetInstanceID(), FocusType.Passive);
                    EditorGUIUtility.ResetGUIState();
                    GUILayoutGroup topLevel = GUILayoutUtility.current.topLevel;
                    int visible = this.tracker.GetVisible(editorIndex);
                    bool flag;
                    if (visible == -1)
                    {
                        flag = InternalEditorUtility.GetIsInspectorExpanded(target);
                        this.tracker.SetVisible(editorIndex, (!flag) ? 0 : 1);
                    }
                    else
                    {
                        flag = (visible == 1);
                    }
                    rebuildOptimizedGUIBlock |= editor.isInspectorDirty;
                    if (Event.current.type == EventType.Repaint)
                    {
                        editor.isInspectorDirty = false;
                    }
                    ScriptAttributeUtility.propertyHandlerCache = editor.propertyHandlerCache;
                    bool flag2 = this.EditorHasLargeHeader(editorIndex, editors);
                    if (flag2)
                    {
                        string empty = string.Empty;
                        bool flag3 = editor.IsOpenForEdit(out empty);
                        if (showImportedObjectBarNext)
                        {
                            showImportedObjectBarNext = false;
                            GUILayout.Space(15f);
                            importedObjectBarRect = GUILayoutUtility.GetRect(16f, 16f);
                            importedObjectBarRect.height = 17f;
                        }
                        flag = true;
                        using (new EditorGUI.DisabledScope(!flag3))
                        {
                            editor.DrawHeader();
                        }
                    }
                    if (editor.target is AssetImporter)
                    {
                        showImportedObjectBarNext = true;
                    }
                    bool flag4 = false;
                    if (editor is GenericInspector && CustomEditorAttributes.FindCustomEditorType(target, false) != null)
                    {
                        if (this.m_InspectorMode != InspectorMode.DebugInternal)
                        {
                            if (this.m_InspectorMode == InspectorMode.Normal)
                            {
                                flag4 = true;
                            }
                            else if (target is AssetImporter)
                            {
                                flag4 = true;
                            }
                        }
                    }
                    Rect dragRect = default(Rect);
                    if (!flag2)
                    {
                        using (new EditorGUI.DisabledScope(!editor.IsEnabled()))
                        {
                            bool flag5 = EditorGUILayout.InspectorTitlebar(flag, editor.targets, editor.CanBeExpandedViaAFoldout());
                            if (flag != flag5)
                            {
                                this.tracker.SetVisible(editorIndex, (!flag5) ? 0 : 1);
                                InternalEditorUtility.SetIsInspectorExpanded(target, flag5);
                                if (flag5)
                                {
                                    this.m_LastInteractedEditor = editor;
                                }
                                else if (this.m_LastInteractedEditor == editor)
                                {
                                    this.m_LastInteractedEditor = null;
                                }
                            }
                        }
                        dragRect = GUILayoutUtility.GetLastRect();
                    }
                    if (flag4 && flag)
                    {
                        GUILayout.Label("Multi-object editing not supported.", EditorStyles.helpBox, new GUILayoutOption[0]);
                    }
                    else
                    {
                        this.DisplayDeprecationMessageIfNecessary(editor);
                        EditorGUIUtility.ResetGUIState();
                        Rect rect = default(Rect);
                        bool flag6 = ModuleMetadata.GetModuleIncludeSettingForObject(target) == ModuleIncludeSetting.ForceExclude;
                        if (flag6)
                        {
                            EditorGUILayout.HelpBox("The module which implements this component type has been force excluded in player settings. This object will be removed in play mode and from any builds you make.", MessageType.Warning);
                        }
                        using (new EditorGUI.DisabledScope(!editor.IsEnabled() || flag6))
                        {
                            GenericInspector genericInspector = editor as GenericInspector;
                            if (genericInspector)
                            {
                                genericInspector.m_InspectorMode = this.m_InspectorMode;
                            }
                            EditorGUIUtility.hierarchyMode = true;
                            EditorGUIUtility.wideMode = (base.position.width > 330f);
                            ScriptAttributeUtility.propertyHandlerCache = editor.propertyHandlerCache;
                            OptimizedGUIBlock optimizedGUIBlock;
                            float num;
                            if (editor.GetOptimizedGUIBlock(rebuildOptimizedGUIBlock, flag, out optimizedGUIBlock, out num))
                            {
                                rect = GUILayoutUtility.GetRect(0f, (!flag) ? 0f : num);
                                this.HandleLastInteractedEditor(rect, editor);
                                if (Event.current.type == EventType.Layout)
                                {
                                    return;
                                }
                                if (optimizedGUIBlock.Begin(rebuildOptimizedGUIBlock, rect))
                                {
                                    if (flag)
                                    {
                                        GUI.changed = false;
                                        editor.OnOptimizedInspectorGUI(rect);
                                    }
                                }
                                optimizedGUIBlock.End();
                            }
                            else
                            {
                                if (flag)
                                {
                                    GUIStyle style = (!editor.UseDefaultMargins()) ? GUIStyle.none : EditorStyles.inspectorDefaultMargins;
                                    rect = EditorGUILayout.BeginVertical(style, new GUILayoutOption[0]);
                                    this.HandleLastInteractedEditor(rect, editor);
                                    GUI.changed = false;
                                    try
                                    {
                                        editor.OnInspectorGUI();
                                    }
                                    catch (Exception exception)
                                    {
                                        if (GUIUtility.ShouldRethrowException(exception))
                                        {
                                            throw;
                                        }
                                        Debug.LogException(exception);
                                    }
                                    EditorGUILayout.EndVertical();
                                }
                                if (Event.current.type == EventType.Used)
                                {
                                    return;
                                }
                            }
                        }
                        this.editorDragging.HandleDraggingToEditor(editorIndex, dragRect, rect, this.m_Tracker);
                        if (GUILayoutUtility.current.topLevel != topLevel)
                        {
                            if (!GUILayoutUtility.current.layoutGroups.Contains(topLevel))
                            {
                                Debug.LogError("Expected top level layout group missing! Too many GUILayout.EndScrollView/EndVertical/EndHorizontal?");
                                GUIUtility.ExitGUI();
                            }
                            else
                            {
                                Debug.LogWarning("Unexpected top level layout group! Missing GUILayout.EndScrollView/EndVertical/EndHorizontal?");
                                while (GUILayoutUtility.current.topLevel != topLevel)
                                {
                                    GUILayoutUtility.EndLayoutGroup();
                                }
                            }
                        }
                        this.HandleComponentScreenshot(rect, editor);
                    }
                }
            }
        }

        // Token: 0x06004D9A RID: 19866 RVA: 0x0016A24C File Offset: 0x0016844C
        internal void RepaintImmediately(bool rebuildOptimizedGUIBlocks)
        {
            this.m_InvalidateGUIBlockCache = rebuildOptimizedGUIBlocks;
            base.RepaintImmediately();
        }

        // Token: 0x06004D9B RID: 19867 RVA: 0x0016A25C File Offset: 0x0016845C
        public bool EditorHasLargeHeader(int editorIndex, Editor[] trackerActiveEditors)
        {
            UnityEngine.Object target = trackerActiveEditors[editorIndex].target;
            return AssetDatabase.IsMainAsset(target) || AssetDatabase.IsSubAsset(target) || editorIndex == 0 || target is Material;
        }

        // Token: 0x06004D9C RID: 19868 RVA: 0x0016A2A4 File Offset: 0x001684A4
        private void DisplayDeprecationMessageIfNecessary(Editor editor)
        {
            if (editor && editor.target)
            {
                ObsoleteAttribute obsoleteAttribute = (ObsoleteAttribute)Attribute.GetCustomAttribute(editor.target.GetType(), typeof(ObsoleteAttribute));
                if (obsoleteAttribute != null)
                {
                    string message = (!string.IsNullOrEmpty(obsoleteAttribute.Message)) ? obsoleteAttribute.Message : "This component has been marked as obsolete.";
                    EditorGUILayout.HelpBox(message, (!obsoleteAttribute.IsError) ? MessageType.Warning : MessageType.Error);
                }
            }
        }

        // Token: 0x06004D9D RID: 19869 RVA: 0x0016A338 File Offset: 0x00168538
        private void HandleComponentScreenshot(Rect contentRect, Editor editor)
        {
            if (ScreenShots.s_TakeComponentScreenshot)
            {
                contentRect.yMin -= 16f;
                if (contentRect.Contains(Event.current.mousePosition))
                {
                    Rect contentRect2 = GUIClip.Unclip(contentRect);
                    contentRect2.position += this.m_Parent.screenPosition.position;
                    ScreenShots.ScreenShotComponent(contentRect2, editor.target);
                }
            }
        }

        // Token: 0x06004D9E RID: 19870 RVA: 0x0016A3B8 File Offset: 0x001685B8
        public bool ShouldCullEditor(Editor[] editors, int editorIndex)
        {
            bool result;
            if (editors[editorIndex].hideInspector)
            {
                result = true;
            }
            else
            {
                UnityEngine.Object target = editors[editorIndex].target;
                if (target is SubstanceImporter || target is ParticleSystemRenderer)
                {
                    result = true;
                }
                else if (target != null && target.GetType() == typeof(AssetImporter))
                {
                    result = true;
                }
                else
                {
                    if (this.m_InspectorMode == InspectorMode.Normal && editorIndex != 0)
                    {
                        AssetImporterEditor assetImporterEditor = editors[0] as AssetImporterEditor;
                        if (assetImporterEditor != null && !assetImporterEditor.showImportedObject)
                        {
                            return true;
                        }
                    }
                    result = false;
                }
            }
            return result;
        }

        // Token: 0x06004D9F RID: 19871 RVA: 0x0016A46C File Offset: 0x0016866C
        private void DrawSelectionPickerList()
        {
            if (this.m_TypeSelectionList == null)
            {
                this.m_TypeSelectionList = new TypeSelectionList(Selection.objects);
            }
            GUILayout.Space(0f);
            Editor.DrawHeaderGUI(null, Selection.objects.Length + " Objects");
            GUILayout.Label("Narrow the Selection:", EditorStyles.label, new GUILayoutOption[0]);
            GUILayout.Space(4f);
            Vector2 iconSize = EditorGUIUtility.GetIconSize();
            EditorGUIUtility.SetIconSize(new Vector2(16f, 16f));
            foreach (TypeSelection typeSelection in this.m_TypeSelectionList.typeSelections)
            {
                Rect rect = GUILayoutUtility.GetRect(16f, 16f, new GUILayoutOption[]
                {
                    GUILayout.ExpandWidth(true)
                });
                if (GUI.Button(rect, typeSelection.label, BetterInspectorWindow.styles.typeSelection))
                {
                    Selection.objects = typeSelection.objects;
                    Event.current.Use();
                }
                if (GUIUtility.hotControl == 0)
                {
                    EditorGUIUtility.AddCursorRect(rect, MouseCursor.Link);
                }
                GUILayout.Space(4f);
            }
            EditorGUIUtility.SetIconSize(iconSize);
        }

        // Token: 0x06004DA0 RID: 19872 RVA: 0x0016A5B8 File Offset: 0x001687B8
        private void HandleLastInteractedEditor(Rect componentRect, Editor editor)
        {
            if (editor != this.m_LastInteractedEditor && Event.current.type == EventType.MouseDown && componentRect.Contains(Event.current.mousePosition))
            {
                this.m_LastInteractedEditor = editor;
                base.Repaint();
            }
        }

        // Token: 0x06004DA1 RID: 19873 RVA: 0x0016A60C File Offset: 0x0016880C
        private void AddComponentButton(Editor[] editors)
        {
            Editor firstNonImportInspectorEditor = this.GetFirstNonImportInspectorEditor(editors);
            if (firstNonImportInspectorEditor != null && firstNonImportInspectorEditor.target != null && firstNonImportInspectorEditor.target is GameObject && firstNonImportInspectorEditor.IsEnabled())
            {
                EditorGUILayout.BeginHorizontal(new GUILayoutOption[0]);
                GUILayout.FlexibleSpace();
                GUIContent addComponentLabel = BetterInspectorWindow.s_Styles.addComponentLabel;
                Rect rect = GUILayoutUtility.GetRect(addComponentLabel, BetterInspectorWindow.styles.addComponentButtonStyle);
                if (Event.current.type == EventType.Repaint)
                {
                    this.DrawSplitLine(rect.y - 11f);
                }
                Event current = Event.current;
                bool flag = false;
                EventType type = current.type;
                if (type == EventType.ExecuteCommand)
                {
                    string commandName = current.commandName;
                    if (commandName == "OpenAddComponentDropdown")
                    {
                        flag = true;
                        current.Use();
                    }
                }
                if (EditorGUI.DropdownButton(rect, addComponentLabel, FocusType.Passive, BetterInspectorWindow.styles.addComponentButtonStyle) || flag)
                {
                    if (AddComponentWindow.Show(rect, firstNonImportInspectorEditor.targets.Select(new Func<UnityEngine.Object, GameObject>(BetterInspectorWindow.< AddComponentButton > m__2)).ToArray<GameObject>()))
                    {
                        GUIUtility.ExitGUI();
                    }
                }
                GUILayout.FlexibleSpace();
                EditorGUILayout.EndHorizontal();
            }
        }

        // Token: 0x06004DA2 RID: 19874 RVA: 0x0016A75C File Offset: 0x0016895C
        private bool ReadyToRepaint()
        {
            if (AnimationMode.InAnimationPlaybackMode())
            {
                long num = DateTime.Now.Ticks / 10000L;
                if (num - this.s_LastUpdateWhilePlayingAnimation < 150L)
                {
                    return false;
                }
                this.s_LastUpdateWhilePlayingAnimation = num;
            }
            return true;
        }

        // Token: 0x06004DA3 RID: 19875 RVA: 0x0016A7B4 File Offset: 0x001689B4
        private void DrawSplitLine(float y)
        {
            Rect position = new Rect(0f, y, this.m_Pos.width + 1f, 1f);
            Rect texCoords = new Rect(0f, 1f, 1f, 1f - 1f / (float)EditorStyles.inspectorTitlebar.normal.background.height);
            GUI.DrawTextureWithTexCoords(position, EditorStyles.inspectorTitlebar.normal.background, texCoords);
        }

        // Token: 0x06004DA4 RID: 19876 RVA: 0x0016A832 File Offset: 0x00168A32
        internal static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(BetterInspectorWindow));
        }

        // Token: 0x06004DA5 RID: 19877 RVA: 0x0016A845 File Offset: 0x00168A45
        private static void FlushOptimizedGUI()
        {
            BetterInspectorWindow.s_AllOptimizedGUIBlocksNeedsRebuild = true;
        }

        // Token: 0x06004DA6 RID: 19878 RVA: 0x0016A850 File Offset: 0x00168A50
        private static void FlushAllOptimizedGUIBlocksIfNeeded()
        {
            if (BetterInspectorWindow.s_AllOptimizedGUIBlocksNeedsRebuild)
            {
                BetterInspectorWindow.s_AllOptimizedGUIBlocksNeedsRebuild = false;
                foreach (BetterInspectorWindow inspectorWindow in BetterInspectorWindow.m_AllInspectors)
                {
                    foreach (Editor editor in inspectorWindow.tracker.activeEditors)
                    {
                        BetterInspectorWindow.FlushOptimizedGUIBlock(editor);
                    }
                }
            }
        }

        // Token: 0x06004DA7 RID: 19879 RVA: 0x0016A8F0 File Offset: 0x00168AF0
        private static void FlushOptimizedGUIBlock(Editor editor)
        {
            if (!(editor == null))
            {
                OptimizedGUIBlock optimizedGUIBlock;
                float num;
                if (editor.GetOptimizedGUIBlock(false, false, out optimizedGUIBlock, out num))
                {
                    optimizedGUIBlock.valid = false;
                }
            }
        }

        // Token: 0x06004DA8 RID: 19880 RVA: 0x0016A92C File Offset: 0x00168B2C
        private void Update()
        {
            Editor[] activeEditors = this.tracker.activeEditors;
            if (activeEditors != null)
            {
                bool flag = false;
                foreach (Editor editor in activeEditors)
                {
                    if (editor.RequiresConstantRepaint() && !editor.hideInspector)
                    {
                        flag = true;
                    }
                }
                if (flag && this.m_lastRenderedTime + 0.032999999821186066 < EditorApplication.timeSinceStartup)
                {
                    this.m_lastRenderedTime = EditorApplication.timeSinceStartup;
                    base.Repaint();
                }
            }
        }

        // Token: 0x06004DA9 RID: 19881 RVA: 0x0016A9C0 File Offset: 0x00168BC0
        static BetterInspectorWindow()
        {
            // Note: this type is marked as 'beforefieldinit'.
        }

        // Token: 0x06004DAA RID: 19882 RVA: 0x0016A9CC File Offset: 0x00168BCC
        [CompilerGenerated]
        private static bool DrawPreviewAndLabels(UnityEngine.Object a)
        {
            return !(a is MonoScript) && AssetDatabase.IsMainAsset(a);
        }

        // Token: 0x06004DAB RID: 19883 RVA: 0x0016A9F4 File Offset: 0x00168BF4
        [CompilerGenerated]
        private static bool DrawPreviewAndLabels1(UnityEngine.Object a)
        {
            return EditorUtility.IsPersistent(a) && !Editor.IsAppropriateFileOpenForEdit(a);
        }

        // Token: 0x06004DAC RID: 19884 RVA: 0x0016AA20 File Offset: 0x00168C20
        [CompilerGenerated]
        private static GameObject AddComponentButton2(UnityEngine.Object o)
        {
            return (GameObject)o;
        }

        // Token: 0x04002A4B RID: 10827
        public Vector2 m_ScrollPosition;

        // Token: 0x04002A4C RID: 10828
        public InspectorMode m_InspectorMode = InspectorMode.Normal;

        // Token: 0x04002A4D RID: 10829
        private static readonly List<BetterInspectorWindow> m_AllInspectors = new List<BetterInspectorWindow>();

        // Token: 0x04002A4E RID: 10830
        private static bool s_AllOptimizedGUIBlocksNeedsRebuild;

        // Token: 0x04002A4F RID: 10831
        private const float kBottomToolbarHeight = 17f;

        // Token: 0x04002A50 RID: 10832
        internal const int kInspectorPaddingLeft = 14;

        // Token: 0x04002A51 RID: 10833
        internal const int kInspectorPaddingRight = 4;

        // Token: 0x04002A52 RID: 10834
        private const long delayRepaintWhilePlayingAnimation = 150L;

        // Token: 0x04002A53 RID: 10835
        private long s_LastUpdateWhilePlayingAnimation = 0L;

        // Token: 0x04002A54 RID: 10836
        private bool m_ResetKeyboardControl;

        // Token: 0x04002A55 RID: 10837
        protected ActiveEditorTracker m_Tracker;

        // Token: 0x04002A56 RID: 10838
        private Editor m_LastInteractedEditor;

        // Token: 0x04002A57 RID: 10839
        private bool m_IsOpenForEdit = false;

        // Token: 0x04002A58 RID: 10840
        private static BetterInspectorWindow.Styles s_Styles;

        // Token: 0x04002A59 RID: 10841
        [SerializeField]
        private PreviewResizer m_PreviewResizer = new PreviewResizer();

        // Token: 0x04002A5A RID: 10842
        [SerializeField]
        private PreviewWindow m_PreviewWindow;

        // Token: 0x04002A5B RID: 10843
        private LabelGUI m_LabelGUI = new LabelGUI();

        // Token: 0x04002A5C RID: 10844
        private AssetBundleNameGUI m_AssetBundleNameGUI = new AssetBundleNameGUI();

        // Token: 0x04002A5D RID: 10845
        private TypeSelectionList m_TypeSelectionList = null;

        // Token: 0x04002A5E RID: 10846
        private double m_lastRenderedTime;

        // Token: 0x04002A5F RID: 10847
        private bool m_InvalidateGUIBlockCache = true;

        // Token: 0x04002A60 RID: 10848
        private List<IPreviewable> m_Previews;

        // Token: 0x04002A61 RID: 10849
        private IPreviewable m_SelectedPreview;

        // Token: 0x04002A62 RID: 10850
        private EditorDragging editorDragging;

        // Token: 0x04002A63 RID: 10851
        public static BetterInspectorWindow s_CurrentInspectorWindow;

        // Token: 0x04002A64 RID: 10852
        [CompilerGenerated]
        private static Func<UnityEngine.Object, bool> func1;

		// Token: 0x04002A65 RID: 10853
		[CompilerGenerated]
        private static Func<UnityEngine.Object, bool> func2;

		// Token: 0x04002A66 RID: 10854
		[CompilerGenerated]
        private static Func<UnityEngine.Object, bool> func3;

		// Token: 0x04002A67 RID: 10855
		[CompilerGenerated]
        private static Func<UnityEngine.Object, GameObject> func4;

		// Token: 0x020008A9 RID: 2217
		internal class Styles
        {
            // Token: 0x06004DAD RID: 19885 RVA: 0x0016AA3C File Offset: 0x00168C3C
            public Styles()
            {
                this.typeSelection.padding.left = 12;
            }

            // Token: 0x04002A68 RID: 10856
            public readonly GUIStyle preToolbar = "preToolbar";

            // Token: 0x04002A69 RID: 10857
            public readonly GUIStyle preToolbar2 = "preToolbar2";

            // Token: 0x04002A6A RID: 10858
            public readonly GUIStyle preDropDown = "preDropDown";

            // Token: 0x04002A6B RID: 10859
            public readonly GUIStyle dragHandle = "RL DragHandle";

            // Token: 0x04002A6C RID: 10860
            public readonly GUIStyle lockButton = "IN LockButton";

            // Token: 0x04002A6D RID: 10861
            public readonly GUIStyle insertionMarker = "InsertionMarker";

            // Token: 0x04002A6E RID: 10862
            public readonly GUIContent preTitle = EditorGUIUtility.TextContent("Preview");

            // Token: 0x04002A6F RID: 10863
            public readonly GUIContent labelTitle = EditorGUIUtility.TextContent("Asset Labels");

            // Token: 0x04002A70 RID: 10864
            public readonly GUIContent addComponentLabel = EditorGUIUtility.TextContent("Add Component");

            // Token: 0x04002A71 RID: 10865
            public GUIStyle preBackground = "preBackground";

            // Token: 0x04002A72 RID: 10866
            public GUIStyle addComponentArea = EditorStyles.inspectorTitlebar;

            // Token: 0x04002A73 RID: 10867
            public GUIStyle addComponentButtonStyle = "AC Button";

            // Token: 0x04002A74 RID: 10868
            public GUIStyle previewMiniLabel = new GUIStyle(EditorStyles.whiteMiniLabel);

            // Token: 0x04002A75 RID: 10869
            public GUIStyle typeSelection = new GUIStyle("PR Label");

            // Token: 0x04002A76 RID: 10870
            public GUIStyle lockedHeaderButton = "preButton";

            // Token: 0x04002A77 RID: 10871
            public GUIStyle stickyNote = new GUIStyle("VCS_StickyNote");

            // Token: 0x04002A78 RID: 10872
            public GUIStyle stickyNoteArrow = new GUIStyle("VCS_StickyNoteArrow");

            // Token: 0x04002A79 RID: 10873
            public GUIStyle stickyNotePerforce = new GUIStyle("VCS_StickyNoteP4");

            // Token: 0x04002A7A RID: 10874
            public GUIStyle stickyNoteLabel = new GUIStyle("VCS_StickyNoteLabel");

            // Token: 0x04002A7B RID: 10875
            public readonly GUIContent VCS_NotConnectedMessage = new GUIContent("VCS Plugin {0} is enabled but not connected");
        }

        // Token: 0x020008AA RID: 2218
        [CompilerGenerated]
        private sealed class CreateTracker
        {
            // Token: 0x06004DAE RID: 19886 RVA: 0x000032BA File Offset: 0x000014BA
            public CreateTracker()
            {
            }

            // Token: 0x06004DAF RID: 19887 RVA: 0x0016ABB8 File Offset: 0x00168DB8
            internal bool m__0(BetterInspectorWindow i)
            {
                return i.m_Tracker != null && i.m_Tracker.Equals(this.sharedTracker);
            }

            // Token: 0x04002A7C RID: 10876
            internal ActiveEditorTracker sharedTracker;
        }
    }


    internal class PreviewWindow : BetterInspectorWindow
    {
        // Token: 0x06005094 RID: 20628 RVA: 0x0018181B File Offset: 0x0017FA1B
        public PreviewWindow()
        {
        }

        // Token: 0x06005095 RID: 20629 RVA: 0x00181823 File Offset: 0x0017FA23
        public void SetParentInspector(BetterInspectorWindow inspector)
        {
            this.m_ParentInspectorWindow = inspector;
            this.CreateTracker();
        }

        // Token: 0x06005096 RID: 20630 RVA: 0x00181833 File Offset: 0x0017FA33
        protected override void OnEnable()
        {
            base.OnEnable();
            base.titleContent = new GUIContent("Preview");
            base.minSize = new Vector2(260f, 220f);
        }

        // Token: 0x06005097 RID: 20631 RVA: 0x00181861 File Offset: 0x0017FA61
        protected override void OnDisable()
        {
            base.OnDisable();
            this.m_ParentInspectorWindow.Repaint();
        }

        // Token: 0x06005098 RID: 20632 RVA: 0x00181875 File Offset: 0x0017FA75
        protected override void CreateTracker()
        {
            if (this.m_ParentInspectorWindow != null)
            {
                this.m_Tracker = this.m_ParentInspectorWindow.tracker;
            }
        }

        // Token: 0x06005099 RID: 20633 RVA: 0x0018189C File Offset: 0x0017FA9C
        public override Editor GetLastInteractedEditor()
        {
            return this.m_ParentInspectorWindow.GetLastInteractedEditor();
        }

        // Token: 0x0600509A RID: 20634 RVA: 0x001818BC File Offset: 0x0017FABC
        protected override void OnGUI()
        {
            if (!this.m_ParentInspectorWindow)
            {
                base.Close();
                GUIUtility.ExitGUI();
            }
            //Editor.m_AllowMultiObjectAccess = true;
            this.CreatePreviewables();
            base.AssignAssetEditor(base.tracker.activeEditors);
            IPreviewable[] editorsWithPreviews = base.GetEditorsWithPreviews(base.tracker.activeEditors);
            IPreviewable editorThatControlsPreview = base.GetEditorThatControlsPreview(editorsWithPreviews);
            bool flag = editorThatControlsPreview != null && editorThatControlsPreview.HasPreviewGUI();
            Rect rect = EditorGUILayout.BeginHorizontal(BetterInspectorWindow.styles.preToolbar, new GUILayoutOption[]
            {
                GUILayout.Height(17f)
            });
            GUILayout.FlexibleSpace();
            Rect lastRect = GUILayoutUtility.GetLastRect();
            string text = string.Empty;
            if (editorThatControlsPreview != null)
            {
                text = editorThatControlsPreview.GetPreviewTitle().text;
            }
            GUI.Label(lastRect, text, BetterInspectorWindow.styles.preToolbar2);
            if (flag)
            {
                editorThatControlsPreview.OnPreviewSettings();
            }
            EditorGUILayout.EndHorizontal();
            Event current = Event.current;
            if (current.type == EventType.MouseUp && current.button == 1 && rect.Contains(current.mousePosition))
            {
                base.Close();
                current.Use();
            }
            else
            {
                Rect rect2 = GUILayoutUtility.GetRect(0f, 10240f, 64f, 10240f);
                if (Event.current.type == EventType.Repaint)
                {
                    BetterInspectorWindow.styles.preBackground.Draw(rect2, false, false, false, false);
                }
                if (editorThatControlsPreview != null && editorThatControlsPreview.HasPreviewGUI())
                {
                    editorThatControlsPreview.DrawPreview(rect2);
                }
            }
        }

        // Token: 0x0600509B RID: 20635 RVA: 0x00003884 File Offset: 0x00001A84
        public override void AddItemsToMenu(GenericMenu menu)
        {
        }

        // Token: 0x0600509C RID: 20636 RVA: 0x00003884 File Offset: 0x00001A84
        protected override void ShowButton(Rect r)
        {
        }

        // Token: 0x04002CD6 RID: 11478
        [SerializeField]
        private BetterInspectorWindow m_ParentInspectorWindow;
    }
}
*/