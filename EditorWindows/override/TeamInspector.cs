﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Drawing;
using System.Linq;

#if false //Crashed Version
namespace MasiveStudio.TeamProjectEditor.overrideEditor
{
    internal class TeamInspector : EditorWindow // CRASHED
    {
        GameObject selectedObj;
        Transform selectedObjParent;
        //Editor[] custominspector;
        //	var TeamObjects : List.<TeamView> = new List.<TeamView>();
//      int teamobjcounter = 0;
        Vector2 scroll = Vector2.zero;

        bool teamflag = false;

        Component[] comps;

        bool start = false;
        bool changed = true;

        public static EditorWindow window;

        public static Texture2D tex;
        public static string text;

        Dictionary<string, bool> winshows = new Dictionary<string, bool>();

        [MenuItem("Team/TeamInspector")]
        public static void ShowWindow()
        {
            window = EditorWindow.GetWindow(typeof(TeamInspector));
            window.titleContent.text = "TeamInspector";
            tex = Converter.ToTexture(TeamProjectEditor.Properties.Resources.MassiveStudio);
            window.autoRepaintOnSceneChange = true;
        }

        void OnEnable()
        {
            if (tex == null)
            {
                tex = Converter.ToTexture(TeamProjectEditor.Properties.Resources.MassiveStudio);
            }
            if(window!=null)
                window.titleContent = new GUIContent ("TeamInspector", tex);
        }

        public static byte[] ObjectToByteArray(object obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        public static new void Repaint()
        {
            if (window != null)//if Window is open
            window.Repaint();
        }

        void OnGUI()
        {
            if (window == null)
                ShowWindow();
            if (selectedObj != null)
            {
                GUI.BeginGroup(new Rect(0, 0, window.position.width, window.position.height));
                DrawGameObjectHeader();

                DrawTeamHeader();

                DrawScriptProberties();

                GUI.EndGroup();

               /* GUILayout.Space(10);

                scroll = EditorGUILayout.BeginScrollView(scroll);//, Rect(0,0,40,40)); 
                if (selectedObj != null && comps != null && comps.Length > 0) // && changed)
                {
                    for (var a = 0; a < comps.Length; a++)
                    {
                        System.Type type = comps[a].GetType();
                        if (type != null)
                            if (type != typeof(TeamView) && type != typeof(TeamObject))
                            {
                                var targetComp = comps[a];
                                Editor editor = Editor.CreateEditor(targetComp);
                                EditorGUILayout.InspectorTitlebar(true, editor.target);


                                GUIStyle style = (!editor.UseDefaultMargins()) ? GUIStyle.none : EditorStyles.inspectorDefaultMargins;
                                EditorGUILayout.BeginVertical(style, new GUILayoutOption[0]);
                                if (targetComp.GetType().ToString().StartsWith("Unity"))
                                    editor.OnInspectorGUI();
                                else
                                    editor.DrawDefaultInspector();
                                EditorGUILayout.EndVertical();
                            }
                    }
                }
                GUI.EndScrollView();
                GUI.EndGroup();*/
            }
            else
            {
                float w = (window.position.width > window.position.height ? window.position.height : window.position.width);
                GUI.DrawTexture(new Rect(0, 0, w, w), tex);
                GUI.color = UnityEngine.Color.black;
                GUILayout.Label("No Object Selected");
                GUI.color = UnityEngine.Color.white;
            }
        }

        void DrawGameObjectHeader()
        {
            //	this.title = "TeamInspector -  RealtimeUpdater";
            //GAMEOBJECT
            GUI.changed = false;
            GUILayout.BeginHorizontal();
            selectedObj.SetActive(EditorGUILayout.Toggle(selectedObj.activeSelf, GUILayout.MaxWidth(20)));
            GUILayout.Label("Name");
            selectedObj.name = EditorGUILayout.TextField(selectedObj.name);
            selectedObj.isStatic = EditorGUILayout.Toggle(selectedObj.isStatic, GUILayout.MaxWidth(20));
            GUILayout.Label("Static");
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Tag", GUILayout.MaxWidth(25));
            selectedObj.tag = EditorGUILayout.TagField(selectedObj.tag);
            GUILayout.Label("Layer", GUILayout.MaxWidth(40));
            selectedObj.layer = EditorGUILayout.LayerField(selectedObj.layer);
            GUILayout.EndHorizontal();

            if (GUI.changed && teamflag)
            {
                Debug.Log("Changed GameObject Properties");
                if (selectedObj.GetComponent(typeof(TeamView)) != null)
                {
                    TeamView view = (TeamView)selectedObj.GetComponent(typeof(TeamView));
                    view.gameobjectupdate = true;
                }
                else if (selectedObj.GetComponent(typeof(TeamObject)) != null)
                {
                    TeamObject obj = (TeamObject)selectedObj.GetComponent(typeof(TeamObject));
                    obj.gameobjectupdate = true;
                }

                TeamSceneManager.TeamObject_isDirty = true;

            }
            GUI.changed = false;
        }

        void DrawTeamHeader()
        {
            if (TeamServerConnection.connected)
                if (GUILayout.Button(teamflag ? "Unmark as TeamObject" : "Mark as TeamObject"))
                {
                    if (!teamflag)
                    {
                        if (selectedObj.transform.root.GetComponent(typeof(TeamView)) == null)
                            ((TeamView)selectedObj.transform.root.gameObject.AddComponent(typeof(TeamView))).Start();
                        teamflag = true;
                        TeamSceneManager.Register((TeamView)selectedObj.transform.root.GetComponent(typeof(TeamView)));
                        //container.TeamObjects.Add( selectedObj.transform.root.GetComponent(TeamView));
                        //selectedObj.transform.root.GetComponent(TeamView).Update();
                    }
                    else
                    {
                        if (selectedObj.transform.root.GetComponent(typeof(TeamView)) != null)
                        {
                            ((TeamView)selectedObj.transform.root.GetComponent(typeof(TeamView))).destroyflag = true;
                            TeamSceneManager.Unregister((TeamView)selectedObj.transform.root.GetComponent(typeof(TeamView)));
                        }
                        else
                        {
                            Debug.Log("NO UNREGISTER");
                        }
                        teamflag = false;
                    }

                }
            if (teamflag)
            {
                if (GUILayout.Button("Upload/Update Object to Server"))
                {
                    if (selectedObj.GetComponent(typeof(TeamView)) != null)
                    {
                        TeamView view = (TeamView)selectedObj.GetComponent(typeof(TeamView));
                        view.update = true;
                    }
                    else if (selectedObj.GetComponent(typeof(TeamObject)) != null)
                    {
                        TeamObject obj = (TeamObject)selectedObj.GetComponent(typeof(TeamObject));
                        obj.update = true;
                    }
                }
            }
        }

        protected ActiveEditorTracker m_Tracker;
        private static readonly List<TeamInspector> m_AllInspectors = new List<TeamInspector>();

        protected virtual void CreateTracker()
        {
            if (this.m_Tracker != null)
            {
                this.m_Tracker.inspectorMode = InspectorMode.Normal;// this.m_InspectorMode;
                return;
            }
            ActiveEditorTracker sharedTracker = ActiveEditorTracker.sharedTracker;
            bool flag = TeamInspector.m_AllInspectors.Any((TeamInspector i) => i.m_Tracker != null && i.m_Tracker.Equals(sharedTracker));
            this.m_Tracker = ((!flag) ? ActiveEditorTracker.sharedTracker : new ActiveEditorTracker());
            this.m_Tracker.inspectorMode = InspectorMode.Normal;
            this.m_Tracker.RebuildIfNecessary();
        }


        void DrawScriptProberties()
        {
            scroll = EditorGUILayout.BeginScrollView(scroll);//, Rect(0,0,40,40)); 
            if (selectedObj != null && comps != null && comps.Length > 0 && changed)
            {
                for (var a = 0; a < comps.Length; a++)
                {
                    if (comps[a] == null)
                        break;

                    if (comps[a] == null)
                        Debug.LogError("COMP NULL");

                    System.Type type = comps[a].GetType();

                    if (type == null)
                        Debug.LogError("NULL_TYPE");

                    if (type != typeof(TeamView) && type != typeof(TeamObject) && type != System.Type.Missing)// && type!= typeof(Transform))
                    {
                        var targetComp = comps[a];//selectedObj.GetComponent.<type>();

                        if (targetComp != null)
                        {
                            Editor editor = Editor.CreateEditor(targetComp);


                            //DrawEditor(editor);

                            bool showw = true;

                            if (winshows == null)
                                winshows = new Dictionary<string, bool>();

                            if (!winshows.ContainsKey(type.ToString()))
                            {
                                winshows.Add(type.ToString(), true);
                            }
                            else
                            {
                                showw = winshows[type.ToString()];
                            }
                            
                            showw = EditorGUILayout.InspectorTitlebar(showw, editor.target);
                            winshows[type.ToString()] = showw;
                            
                            //editor.OnInspectorGUI();
                            //editor.DrawHeader();
                            if (editor != null)
                            { 
                                if (showw)
                                {

                                    editor.DrawDefaultInspector();

                                    /* editor.UseDefaultMargins();
                                     if (targetComp.GetType().ToString().StartsWith("Unity"))
                                         editor.OnInspectorGUI();
                                     else
                                         editor.DrawDefaultInspector();*/

                                    /*GUIStyle style = (!editor.UseDefaultMargins()) ? GUIStyle.none : EditorStyles.inspectorDefaultMargins;
                                    EditorGUILayout.BeginVertical(style, new GUILayoutOption[0]);
                                    if (targetComp.GetType().ToString().StartsWith("Unity"))
                                        editor.OnInspectorGUI();
                                    else
                                        editor.DrawDefaultInspector();
                                    EditorGUILayout.EndVertical();*/

                                    if (teamflag && GUI.changed)
                                    {
                                        Debug.Log("CHNAGES");
                                        ScriptChanges(type, a);
                                    }
                                    GUI.changed = false;
                                }
                            }

                        }
                        else if (type == null)
                            Debug.LogError("TYPE NULL");
                    }
                }
            }
            else
            {
                Debug.LogError("NULL");
            }
            EditorGUILayout.EndScrollView();
        }

        void ScriptChanges(System.Type type, int a)
        {
            TeamSceneManager.TeamObject_isDirty = true;

            //							Debug.Log("EDIT: "+ type.ToString()); 
            changed = true;
            if (selectedObj.GetComponent(typeof(TeamView)) != null)
            {

                TeamView view = (TeamView)selectedObj.GetComponent(typeof(TeamView));
                if (view.updateComponent.IndexOf(comps[a]) == -1 && type != typeof(Transform))
                {
                    view.updateComponent.Add(comps[a]);
                    view.update = true;
                }
                else if (type == typeof(Transform))
                {
                    Debug.Log("TRANSFORM CHANGES");
                    view.Update();
                    //view.transformupdate = true;
                }

                if (type == typeof(BoxCollider) || type == typeof(MeshCollider) || type == typeof(CapsuleCollider) || type == typeof(SphereCollider))
                {//Update Bounds
                    view.UpdateBounds();
                }
            }
            else if (selectedObj.GetComponent(typeof(TeamObject)) != null)
            {
                TeamObject obj = (TeamObject)selectedObj.GetComponent(typeof(TeamObject));
                if (type != typeof(Transform))
                {
                    if (obj.updateComponent.IndexOf(comps[a]) == -1) //if any component has changed
                    {
                        obj.updateComponent.Add(comps[a]);
                        obj.update = true;
                    }
                }
                else
                { //if transform changed
                    obj.transformupdate = true;
                }
            }
            else
                Debug.LogError("NOT TEAM OBJECT");

        }

        void Update()
        {
            if (!start)
            {
                try
                {
                    //container =TeamSceneManager;
                    start = true;
                }
                catch (System.Exception e)
                {
                    Debug.LogError("ERROR TEAMINSPECTOR: " + e.Message);
                }
            }


            if (selectedObj != null && ( selectedObj.transform.hasChanged || selectedObj.transform.parent != selectedObjParent))
            {
                selectedObjParent = selectedObj.transform.parent;
                if(teamflag)
                    if(selectedObj.transform.root.GetComponent<TeamView>()!=null)
                        selectedObj.transform.root.GetComponent<TeamView>().Update();

                if (selectedObj.GetComponent(typeof(TeamView)) != null)
                {
                    ((TeamView)selectedObj.GetComponent(typeof(TeamView))).SetIDs();// transformupdate = true;
                }
                else if (selectedObj.GetComponent(typeof(TeamObject)) != null)
                {
                    ((TeamObject)selectedObj.GetComponent(typeof(TeamObject))).Update();// transformupdate = true;
                }

                TeamSceneManager.TeamObject_isDirty = true;

                selectedObj.transform.hasChanged = false;
            }

            //if(del==null)
            //{		
            //	Debug.Log("ADD DELEGATE");

            //SceneView.onSceneGUIDelegate.BeginInvoke(OnSceneGUI);

            //}
        }

        void OnSelectionChange()
        {
            OnInspectorUpdate();
            Repaint();
        }

        void OnInspectorUpdate()
        {
            if (Selection.gameObjects.Length > 0)
            {
                if (selectedObj != Selection.activeGameObject)
                {
                    selectedObj = Selection.activeGameObject;
                    selectedObjParent = selectedObj.transform.parent;
                    comps = selectedObj.GetComponents(typeof(Component));
                    
                    //custominspector = new Editor[comps.Length];
                    teamflag = selectedObj.transform.root.GetComponent(typeof(TeamView)) != null ? true : selectedObj.transform.root.GetComponent(typeof(TeamObject)) ? true : false;
                }
            }
            else
                selectedObj = null;
            /*if(del==null)
            del = new Delegater();
            */
            if( selectedObj!=null){
                SceneView.onSceneGUIDelegate -= OnSceneGUI;
                SceneView.onSceneGUIDelegate += OnSceneGUI;
            }

        }

        void OnDestroy()
        {
            SceneView.onSceneGUIDelegate -= OnSceneGUI;
        }

       /* public void Callback(object obj)
        {
            Debug.Log("Selected: " + obj);
        }*/

        void OnSceneGUI(SceneView sceneView)
        {
            //Handles.DrawCube(0, new Vector3(0, 0, 0), Quaternion.identity, 10f);

            // Handles.BeginGUI();
            if (selectedObj != null)
            {

                /* if (Event.current.type == EventType.layout)
                     HandleUtility.AddDefaultControl(GUIUtility.GetControlID(GetHashCode(), FocusType.Native));

                 if (Tools.current == Tool.Move)
                     selectedObj.transform.position = Handles.PositionHandle(selectedObj.transform.position, Quaternion.Euler(0, 0, 0));
                 else if (Tools.current == Tool.Rotate)
                     selectedObj.transform.rotation = Handles.RotationHandle(selectedObj.transform.rotation, selectedObj.transform.position);
                 else if (Tools.current == Tool.Scale)
                     selectedObj.transform.localScale = Handles.ScaleHandle(selectedObj.transform.localScale, selectedObj.transform.position, Quaternion.Euler(0, 0, 0), HandleUtility.GetHandleSize(selectedObj.transform.position));
                 // else if (Tools.current == Tool.Rect)
                 //     selectedObj.transform.localScale = Handles.RectangleCap(selectedObj.transform.localScale, selectedObj.transform.position, Quaternion.Euler(0, 0, 0), HandleUtility.GetHandleSize(selectedObj.transform.position));
                 */

                if (selectedObj.transform.hasChanged || selectedObj.transform.parent != selectedObjParent)
                {
                    if (teamflag)
                    {
                        selectedObjParent = selectedObj.transform.parent;
                        selectedObj.transform.root.GetComponent<TeamView>().Update();
                        Debug.Log("UPDATE");
                        TeamSceneManager.TeamObject_isDirty = true;
                    }
                }
            }

           // Handles.EndGUI();
        }


        private void DrawEditor(Editor editor)
        {
            if (editor == null)
            {
                return;
            }
            UnityEngine.Object target = editor.target;
            GUIUtility.GetControlID(target.GetInstanceID(), FocusType.Passive);
            Rect rect = default(Rect);
            using (new EditorGUI.DisabledScope(false))
            {
                EditorGUIUtility.hierarchyMode = true;
                EditorGUIUtility.wideMode = (base.position.width > 330f);
                float num;
                {
                        GUIStyle style = (!editor.UseDefaultMargins()) ? GUIStyle.none : EditorStyles.inspectorDefaultMargins;
                        rect = EditorGUILayout.BeginVertical(style, new GUILayoutOption[0]);
                        GUI.changed = false;
                        try
                        {
                            editor.OnInspectorGUI();
                        }
                        catch (System.Exception exception)
                        {
                            Debug.LogException(exception);
                        }
                        EditorGUILayout.EndVertical();

                    if (Event.current.type == EventType.Used)
                    {
                        return;
                    }
                }
            }
        }
    }
}
#endif