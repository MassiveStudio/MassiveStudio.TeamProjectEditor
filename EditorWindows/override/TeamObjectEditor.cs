﻿using UnityEngine;
using UnityEditor;

namespace MassiveStudio.TeamProjectEditor
{
    [CustomEditor(typeof(TeamObject))]
    public class TeamObjectEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            //EMPTY
            //EditorGUILayout.InspectorTitlebar(false, (Object)null, false);
            //base.OnInspectorGUI();
        }
    }
}