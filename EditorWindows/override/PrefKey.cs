﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor.EditorWindows
{
    internal class PrefKey : IPrefType
    {
        private bool m_Loaded;

        private string m_name;

        private Event m_event;

        private string m_Shortcut;

        private string m_DefaultShortcut;

        public string Name
        {
            get
            {
                this.Load();
                return this.m_name;
            }
        }

        public Event KeyboardEvent
        {
            get
            {
                this.Load();
                return this.m_event;
            }
            set
            {
                this.Load();
                this.m_event = value;
            }
        }

        public bool activated
        {
            get
            {
                this.Load();
                return Event.current.Equals(this);// && !GUIUtility.textFieldInput;
            }
        }

        public PrefKey()
        {
            this.m_Loaded = true;
        }

        public PrefKey(string name, string shortcut)
        {
            this.m_name = name;
            this.m_Shortcut = shortcut;
            this.m_DefaultShortcut = shortcut;
            Settings.Add(this);
            this.m_Loaded = false;
        }

        public void Load()
        {
            if (!this.m_Loaded)
            {
                this.m_Loaded = true;
                this.m_event = Event.KeyboardEvent(this.m_Shortcut);
                PrefKey prefKey = Settings.Get<PrefKey>(this.m_name, this);
                this.m_name = prefKey.Name;
                this.m_event = prefKey.KeyboardEvent;
            }
        }

        public static implicit operator Event(PrefKey pkey)
        {
            pkey.Load();
            return pkey.m_event;
        }

        public string ToUniqueString()
        {
            this.Load();
            return string.Concat(new object[]
            {
                this.m_name,
                ";",
                (!this.m_event.alt) ? "" : "&",
                (!this.m_event.command) ? "" : "%",
                (!this.m_event.shift) ? "" : "#",
                (!this.m_event.control) ? "" : "^",
                this.m_event.keyCode
            });
        }

        public void FromUniqueString(string s)
        {
            this.Load();
            int num = s.IndexOf(";");
            if (num < 0)
            {
                Debug.LogError("Malformed string in Keyboard preferences");
            }
            else
            {
                this.m_name = s.Substring(0, num);
                this.m_event = Event.KeyboardEvent(s.Substring(num + 1));
            }
        }

        internal void ResetToDefault()
        {
            this.Load();
            this.m_event = Event.KeyboardEvent(this.m_DefaultShortcut);
        }
    }

    internal interface IPrefType
    {
        string ToUniqueString();

        void FromUniqueString(string sstr);

        void Load();
    }

    internal class Settings
    {
        private static List<IPrefType> m_AddedPrefs = new List<IPrefType>();

        private static SortedList<string, object> m_Prefs = new SortedList<string, object>();

        internal static void Add(IPrefType value)
        {
            Settings.m_AddedPrefs.Add(value);
        }

        internal static T Get<T>(string name, T defaultValue) where T : IPrefType, new()
        {
            Settings.Load();
            if (defaultValue == null)
            {
                throw new ArgumentException("default can not be null", "defaultValue");
            }
            T result;
            if (Settings.m_Prefs.ContainsKey(name))
            {
                result = (T)((object)Settings.m_Prefs[name]);
            }
            else
            {
                string @string = EditorPrefs.GetString(name, "");
                if (@string == "")
                {
                    Settings.Set<T>(name, defaultValue);
                    result = defaultValue;
                }
                else
                {
                    defaultValue.FromUniqueString(@string);
                    Settings.Set<T>(name, defaultValue);
                    result = defaultValue;
                }
            }
            return result;
        }

        internal static void Set<T>(string name, T value) where T : IPrefType
        {
            Settings.Load();
            EditorPrefs.SetString(name, value.ToUniqueString());
            Settings.m_Prefs[name] = value;
        }

       /* internal static IEnumerable<KeyValuePair<string, T>> Prefs<T>() where T : IPrefType
        {
            Settings.< Prefs > c__Iterator0 < T > < Prefs > c__Iterator = new Settings.< Prefs > c__Iterator0<T>();
            Settings.< Prefs > c__Iterator0 < T > expr_07 = < Prefs > c__Iterator;
            expr_07.$PC = -2;
            return expr_07;
        }*/

        private static void Load()
        {
            if (Settings.m_AddedPrefs.Any<IPrefType>())
            {
                List<IPrefType> list = new List<IPrefType>(Settings.m_AddedPrefs);
                Settings.m_AddedPrefs.Clear();
                foreach (IPrefType current in list)
                {
                    current.Load();
                }
            }
        }
    }
}
