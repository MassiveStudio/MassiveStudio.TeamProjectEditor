﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

namespace MassiveStudio.TeamProjectEditor.overrideEditor
{
    [InitializeOnLoad]
    public sealed class HierachyWindow
    {
        public static Texture2D hideIcon;
        public static Texture2D showIcon;

        public static Texture2D Object;
        public static Texture2D ObjectScript;

        public static Texture2D lockIcon;
        public static Texture2D unlockIcon;

        public static Texture2D ChildLineToParent;

        public static GUIStyle style;
        public static GUIStyle textstyle;

        public static Texture2D CameraView;

        public static bool texturesloaded = false;


        public static Dictionary<int, Color> DeepColors = new Dictionary<int, Color>();

        public static List<Color> colors = new List<Color>()
        {
            new Color32(227, 38, 54, 255),
            new Color32(0, 204, 255, 255),
            new Color32(255, 191, 0, 255),
            new Color32(0, 255, 255, 255),
            new Color32(253, 238, 0, 255),
            new Color32(255, 32, 82, 255),
            new Color32(0, 127, 255, 255),
            new Color32(102, 255, 0, 255),
            new Color32(138, 43, 226,255),
            new Color32(222, 93, 131,255),
            new Color32(255, 0, 127, 255),
            new Color32(0, 107, 60, 255),
            new Color32(0, 201, 153, 255),
            new Color32(229, 43, 80, 255),
            new Color32(0, 191, 255, 255),
            new Color32(255, 0, 56, 255),
            new Color32(127, 255, 0, 255),
            new Color32(255, 167, 0, 255),
            new Color32(0, 46, 99, 255),
            new Color32(0, 255, 255, 255),

            new Color32(255, 52, 0, 255),
            new Color32(255, 0, 76, 255),
            new Color32(255, 0, 204, 255),
            new Color32(255, 179,0, 255),

            new Color32(0, 255, 52, 255),
            new Color32(76, 255, 0, 255),
            new Color32(204, 255, 0, 255),
            new Color32(0, 255, 179, 255),

            new Color32(52, 0, 255, 255),
            new Color32(0, 76, 255, 255),
            new Color32(0, 204, 255, 255),
            new Color32(179, 0, 255, 255),
        };

        public static Color GetColor(int deep)
        {
            return colors[deep];

            Color col;
            if (!DeepColors.TryGetValue(deep, out col))
            {
                col = new Color(UnityEngine.Random.Range(0.01f, 1.0f), UnityEngine.Random.Range(0.01f, 1.0f), UnityEngine.Random.Range(0.01f, 1.0f), 1f);
                DeepColors.Add(deep, col);
            }
            return col;
        }

        static HierachyWindow()
        {
            EditorApplication.hierarchyWindowItemOnGUI += HierachyGUI;
            LoadTextures();
        }

        public static void LoadTextures()//Load Internal Textures
        {
            if (showIcon == null)
                showIcon = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.Show);
            if (hideIcon == null)
                hideIcon = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.Hide);
            if (CameraView == null)
                CameraView = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.ToCameraView);
            if (Object == null)
                Object = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.Object);
            if (ObjectScript == null)
                ObjectScript = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.Object_Script);
            if (lockIcon == null)
                lockIcon = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.Lock_Locked);
            if (unlockIcon == null)
                unlockIcon = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.Lock_Unlocked);
            if (ChildLineToParent == null)
                ChildLineToParent = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.ChildToParent_Line);

            texturesloaded = true;
        }

        public static void HierachyGUI(int instanceID, Rect rec)
        {
            if (style == null)
            {
                style = new GUIStyle();

                textstyle = new GUIStyle();
                textstyle.alignment = TextAnchor.MiddleRight;
            }

            GameObject o = (GameObject)EditorUtility.InstanceIDToObject(instanceID);
            if (o != null)
            {
                if (lockIcon == null)
                    LoadTextures();

                if (o.GetComponent<Camera>())
                {
                    if (GUI.Button(new Rect(rec.x + rec.width - 60, rec.y, 20, rec.height), CameraView, style))
                    {
                        EditorSceneUtility.ToCameraView(o.transform);
                    }
                }
                //Protect Object
                bool flags = (o && (o.hideFlags & HideFlags.NotEditable) == HideFlags.NotEditable);
                if (GUI.Button(new Rect(rec.x + rec.width - 20, rec.y, 20, rec.height), flags ? lockIcon : unlockIcon, style))
                {
                    if (!flags)
                    {
                        o.hideFlags = o.hideFlags | HideFlags.NotEditable;
                    }
                    else
                    {
                        o.hideFlags -= HideFlags.NotEditable;
                    }
                    EditorApplication.RepaintHierarchyWindow();
                    //TeamInspector.Repaint();
                }

                Transform cs = o.transform;
                Transform root = o.transform.root;
                int c = 0;
                while (root != cs)
                {
                    GUI.color = GetColor(c);
                    GUI.Box(new Rect(rec.x - (20 + c * 14), rec.y, 20, rec.height), ChildLineToParent, style);

                    //if (o.GetComponents<Behaviour>().Count() > 1)
                    //    GUI.Button(new Rect(rec.x - 20, rec.y, 20, rec.height), ObjectScript, style);
                    //else
                    //    GUI.Button(new Rect(rec.x - 20, rec.y, 20, rec.height), Object, style);
                    cs = cs.parent;
                    c++;
                }
                GUI.color = Color.white;

                if (GUI.Button(new Rect(rec.x + rec.width - 40, rec.y, 20, rec.height), o.activeSelf ? showIcon : hideIcon, style))
                    o.SetActive(!o.activeSelf);


                if (o.layer != 0)
                {
                    GUI.color = GetColor(o.layer);
                    GUI.Box(new Rect(rec.x + rec.width - 80, rec.y, 12, rec.height), "");
                    GUI.color = Color.white;
                }

                if (o.tag != "Untagged")
                    GUI.Label(new Rect(rec.x + rec.width - 194, rec.y, 110, rec.height), o.tag, textstyle);
            }
        }
    }

    public static class EditorSceneUtility
    {
        public static void ToCameraView(Transform t)
        {
            Camera a = t.GetComponent<Camera>();
            Selection.objects = new UnityEngine.Object[] { SceneView.lastActiveSceneView.camera };
            SceneView.lastActiveSceneView.pivot = t.position;
            SceneView.lastActiveSceneView.camera.transform.position = t.position;
            SceneView.lastActiveSceneView.rotation = t.rotation;

            SceneView.lastActiveSceneView.size = Vector3.Magnitude(t.position);

            SceneView.lastActiveSceneView.Repaint();

        }
    }
}