﻿using UnityEngine;
using UnityEditor;

namespace MassiveStudio.TeamProjectEditor
{
    [CustomEditor(typeof(TeamView))]
    public class TeamViewEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            EditorGUILayout.TextArea("TeamView");
            //base.OnInspectorGUI();
        }
    }
}