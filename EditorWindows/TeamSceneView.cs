﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace MassiveStudio.TeamProjectEditor
{
    public class TeamSceneView : EditorWindow
    {
        public static TeamSceneView window;

        [MenuItem("Team/TeamScenes")]
        static void OpenTeamScene()
        {
            window = EditorWindow.GetWindow<TeamSceneView>();
            window.titleContent = new GUIContent("TeamScenes");
        }

        public void Update()
        {

        }

        public void OnGUI()
        {
            if (!TeamServerConnection.connected)
            {
                GUILayout.Label("You need to be connected to a server to use this functions");
            }
            else
            {
                if (GUILayout.Button("UPDATE"))
                    TeamSceneManager.GetSceneList();

                if (TeamSceneManager.scenes==null || TeamSceneManager.scenes.sceneNames.Count==0)
                    GUILayout.Label("Scene List not loaded / still in downloading progress");

                for (int c = 0; c < TeamSceneManager.scenes.sceneNames.Count; c++)
                {
                    if (TeamServerConnection.currentServer.currentScene == TeamSceneManager.scenes.sceneNames[c].sceneID)
                        GUI.color = Color.green;
                    else
                        GUI.color = Color.white;

                    if (GUILayout.Button(TeamSceneManager.scenes.sceneNames[c].sceneName+" : " + TeamSceneManager.scenes.sceneNames[c].sceneID)){
                        if (TeamServerConnection.currentServer.currentScene != TeamSceneManager.scenes.sceneNames[c].sceneID)
                            TeamSceneManager.ChangeScene(TeamSceneManager.scenes.sceneNames[c].sceneID);
                    } 
                }
                GUI.color = Color.white;
                if (GUILayout.Button("+"))
                {
                    TeamSceneManager.CreateNewTeamScene("Test");
                }
            }
        }

        void OnDestroy()
        {
            
        }

    }
    /*
    public enum TeamSceneViewMode
    {
        TeamScene=0,
        MeshEditor=1
    };*/
}