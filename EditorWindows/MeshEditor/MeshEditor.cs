﻿using UnityEngine;
using UnityEditor;

namespace MassiveStudio.TeamProjectEditor.MeshEditor
{

    public class MeshEditor : ScriptableObject
    {
        public static EditorWindow editor;

        public MeshSelectMode selmode;
        public Mesh mesh;

        [MenuItem("Team/MeshEditor")]
        static void OpenTeamScene()
        {
            editor = EditorWindow.GetWindow(typeof(MeshEditor));
            editor.titleContent = new GUIContent("MeshEditor");
        }


        void OnInspectorUpdate()
        {
            if (Selection.gameObjects.Length > 0)
            {
                SceneView.onSceneGUIDelegate -= OnSceneGUI;
                SceneView.onSceneGUIDelegate += OnSceneGUI;

                if (Selection.gameObjects[0].GetComponent<MeshFilter>() != null)
                {
                    mesh = ((MeshFilter)Selection.gameObjects[0].GetComponent<MeshFilter>()).mesh;
                    LoadMeshData();
                }
            }
        }

        public void LoadMeshData()
        {

        }

        void OnDestroy()
        {
            SceneView.onSceneGUIDelegate -= OnSceneGUI;
        }

        public void OnSceneGUI(SceneView view)
        {

        }
    }

    public enum MeshSelectMode
    {
        Face = 0,
        Vertices = 1,
        Edges = 2
    };
}