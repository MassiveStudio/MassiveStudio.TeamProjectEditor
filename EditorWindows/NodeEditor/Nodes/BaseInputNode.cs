﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace MassiveStudio.NodeEditor
{
    public abstract class BaseInputNode : BaseNode
    {
        protected object nodeResult = null;
        //protected object nodeType = null;

        protected bool show = true;
        protected bool bypass = false;


        public BaseInputNode() : base()
        {
            CalculateInOutputBox();
        }

        public virtual object getResult()
        {
            return nodeResult;
        }

        public override void DrawWindow()
        {
            if (Inputs != null && Inputs.Length > 0)
            {
                GUI.backgroundColor = this.show ? Color.blue : Color.white;
                if (GUI.Button(new Rect((windowRect.width - 25) * zoom, 15, 25 * zoom, (windowRect.height * zoom) - 15), "S" + (this.show ? "1" : "0")))
                {
                    this.show = !this.show;
                }

                GUI.backgroundColor = bypass ? Color.yellow : Color.white;
                if (GUI.Button(new Rect(0, 15, 25 * zoom, (windowRect.height * zoom) - 15), "B" + (bypass ? "1" : "0")))
                {
                    bypass = !bypass;
                }
                GUI.backgroundColor = Color.white;
            }
            base.DrawWindow();
        }

        public override NodeData GetField(int field, FieldType type)
        {
            switch (type)
            {
                case FieldType.Output:
                    return Outputs[field];
                case FieldType.Input:
                    return Inputs[field];
            }
            return null;
        }

        public void CalculateInOutputBox()
        {
            Debug.Log("C O: " + Outputs.Length);
            if (Outputs != null)
                if (Outputs.Length > 0)
                {
                    Rect outputRect = new Rect();
                    outputRect.x = windowRect.width;
                    float hy = windowRect.height / (Outputs.Length + 1);
                    outputRect.y = hy - (25 / 2f);//Height/2->Middle
                    outputRect.width = 25;
                    outputRect.height = 25;
                    for (int c = 0; c < Outputs.Length; c++)
                    {
                        Outputs[c].rect = outputRect;
                        Debug.Log("OUTPUT RECT: " + outputRect.ToString());
                        outputRect.y += hy;
                    }
                }
            if (Inputs != null)
                if (Inputs.Length > 0)
                {
                    Rect inputRect = new Rect();
                    inputRect.x = -25;
                    float hy = windowRect.height / (Inputs.Length + 1);
                    inputRect.y = hy - (25 / 2f);//Height/2->Middle;
                    inputRect.width = 25;
                    inputRect.height = 25;

                    for (int c = 0; c < Inputs.Length; c++)
                    {
                        //Debug.Log("INPUT RECT: " + inputRect.ToString());
                        Inputs[c].rect = inputRect;
                        inputRect.y += hy;
                    }
                }
        }

        public override void DrawCurves()
        {
            if (Inputs != null)
                for (int i = 0; i < Inputs.Length; i++)
                    if (Inputs[i].node != null)
                        NodeEditor.DrawNodeCurve(new Rect((windowRect.position + Inputs[i].rect.position) * zoom, Inputs[i].rect.size * zoom), new Rect((Inputs[i].node.windowRect.position + Inputs[i].rect2.position) * zoom, Inputs[i].rect2.size * zoom));
        }
    }

    public abstract class FlowNode : BaseNode
    {
        public NodeData[] nodes;

        public FlowNode() : base(){}

        public override void DrawWindow()
        {

            base.DrawWindow();
        }

        public override NodeData GetField(int field, FieldType type)
        {
            switch (type)
            {
                case FieldType.Output:
                    return Outputs[field];
                case FieldType.Input:
                    return Inputs[field];
            }
            return null;
        }

        public void CalculateInOutputBox()
        {
            //Debug.Log("CalculateInOutputBox "+ Outputs.Length);
            if (Outputs != null)
                if (Outputs.Length > 0)
                {
                    Rect outputRect = new Rect();
                    outputRect.x = windowRect.width;
                    float hy = windowRect.height / (Outputs.Length + 1);
                    outputRect.y = hy - (25 / 2f);//Height/2->Middle
                    outputRect.width = 25;
                    outputRect.height = 25;
                    for (int c = 0; c < Outputs.Length; c++)
                    {
                        Outputs[c].rect = outputRect;
                        //Debug.Log("OUTPUT RECT: " + outputRect.ToString());
                        outputRect.y += hy;
                    }
                }
            if (Inputs != null)
                if (Inputs.Length > 0)
                {
                    Rect inputRect = new Rect();
                    inputRect.x = -25;
                    float hy = windowRect.height / (Inputs.Length + 1);
                    inputRect.y = hy - (25 / 2f);//Height/2->Middle;
                    inputRect.width = 25;
                    inputRect.height = 25;

                    for (int c = 0; c < Inputs.Length; c++)
                    {
                        //Debug.Log("INPUT RECT: " + inputRect.ToString());
                        Inputs[c].rect = inputRect;
                        inputRect.y += hy;
                    }
                }
        }

        public override void DrawCurves()
        {
            if (Inputs != null)
                for (int i = 0; i < Inputs.Length; i++)
                    if (Inputs[i].node != null)
                        NodeEditor.DrawNodeCurve(new Rect((windowRect.position + Inputs[i].rect.position) * zoom, Inputs[i].rect.size * zoom), new Rect((Inputs[i].node.windowRect.position + Inputs[i].rect2.position) * zoom, Inputs[i].rect2.size * zoom));
        }
    }
    //MeshEditor
    public abstract class ObjectNode : BaseNode
    {
        protected bool show = true;
        protected bool bypass = false;

        public ObjectNode() : base()
        {
            CalculateInOutputBox();
        }

        public override void DrawWindow()
        {
            if (Inputs != null && Inputs.Length > 0)
            {
                GUI.backgroundColor = this.show ? Color.blue : Color.white;
                if (GUI.Button(new Rect((windowRect.width - 25) * zoom, 15, 25 * zoom, (windowRect.height * zoom) - 15), "S" + (this.show ? "1" : "0")))
                {
                    this.show = !this.show;
                }

                GUI.backgroundColor = bypass ? Color.yellow : Color.white;
                if (GUI.Button(new Rect(0, 15, 25 * zoom, (windowRect.height * zoom) - 15), "B" + (bypass ? "1" : "0")))
                {
                    bypass = !bypass;
                }
                GUI.backgroundColor = Color.white;
            }
            base.DrawWindow();
        }

        public override NodeData GetField(int field, FieldType type)
        {
            switch (type)
            {
                case FieldType.Output:
                    return Outputs[field];
                case FieldType.Input:
                    return Inputs[field];
            }
            return null;
        }

        public void CalculateInOutputBox()
        {
            if (Outputs != null)
                if (Outputs.Length > 0)
                {
                    Rect outputRect = new Rect();
                    outputRect.x = windowRect.width;
                    float hy = windowRect.height / (Outputs.Length + 1);
                    outputRect.y = hy - (25 / 2f);//Height/2->Middle
                    outputRect.width = 25;
                    outputRect.height = 25;
                    for (int c = 0; c < Outputs.Length; c++)
                    {
                        Outputs[c].rect = outputRect;
                        //Debug.Log("OUTPUT RECT: " + outputRect.ToString());
                        outputRect.y += hy;
                    }
                }
            if (Inputs != null)
                if (Inputs.Length > 0)
                {
                    Rect inputRect = new Rect();
                    inputRect.x = -25;
                    float hy = windowRect.height / (Inputs.Length + 1);
                    inputRect.y = hy - (25 / 2f);//Height/2->Middle;
                    inputRect.width = 25;
                    inputRect.height = 25;

                    for (int c = 0; c < Inputs.Length; c++)
                    {
                        //Debug.Log("INPUT RECT: " + inputRect.ToString());
                        Inputs[c].rect = inputRect;
                        inputRect.y += hy;
                    }
                }
        }

        public override void DrawCurves()
        {
            if (Inputs != null)
                for (int i = 0; i < Inputs.Length; i++)
                    if (Inputs[i].node != null)
                        NodeEditor.DrawNodeCurve(new Rect((windowRect.position + Inputs[i].rect.position) * zoom, Inputs[i].rect.size * zoom), new Rect((Inputs[i].node.windowRect.position + Inputs[i].rect2.position) * zoom, Inputs[i].rect2.size * zoom));
        }
    }
}