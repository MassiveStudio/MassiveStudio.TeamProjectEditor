﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


namespace MassiveStudio.NodeEditor
{
    public class NodeData
    {
        public Rect rect;
        public Rect rect2;//Relative to WinRect

        public bool hasfield;
        public BaseNode node;
        public BaseNode self;
        public object val;
        public object valType;
        public byte type;//1 - Input  0- Output

        public NodeData result;

        public object needType;//Avoid Object Type Error

        public NodeData() { }
        public NodeData(bool hf, BaseNode s, object v)
        {
            this.hasfield = hf;
            this.self = s;
            this.node = null;
            this.val = v;
        }
        public NodeData(bool hf, BaseNode s, object v, byte t)
        {
            this.hasfield = hf;
            this.self = s;
            this.node = null;
            this.val = v;
            this.type = t;
        }
        public NodeData(Rect rec, bool hf, BaseNode n, BaseNode s)
        {
            this.rect = rec;
            this.hasfield = hf;
            this.node = n;
            this.self = s;
        }
        public NodeData(Rect rec, bool hf, BaseNode n, BaseNode s, object v)
        {
            this.rect = rec;
            this.hasfield = hf;
            this.node = n;
            this.self = s;
            this.val = v;
        }
        public NodeData(Rect rec, Rect rec2, bool hf, BaseNode n, BaseNode s, object v)
        {
            this.rect = rec;
            this.rect2 = rec2;
            this.hasfield = hf;
            this.node = n;
            this.self = s;
            this.val = v;
        }
    }

}
