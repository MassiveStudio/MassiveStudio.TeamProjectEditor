﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEditor;
using System;

namespace MassiveStudio.NodeEditor
{
    public abstract class BaseNode : ScriptableObject
    {

        public static byte INPUT { get { return 1; } }
        public static byte OUTPUT { get { return 0; } }


        public Rect windowRect;
        public bool hasInputs = false;
        public string windowTitle = "";
        public int NodeType = 0;
        public float zoom;

        public NodeData[] Inputs;
        public NodeData[] Outputs;


        public BaseNode() { }

        public void UpdateOutputNodes()
        {
            for (int i = 0; i < Outputs.Length; i++)
                if (Outputs[i].node != null)
                    Outputs[i].node.Updater();
        }

        public virtual void DrawWindow() { }

        public abstract void DrawCurves();

        public virtual void SetInput(NodeData input, Vector2 clickPos) { }

        public virtual void SetInput(NodeData input, int sockel) { }

        public virtual void NodeDeleted(BaseNode node) { }

        public virtual BaseInputNode ClickedOnInput(Vector2 pos)
        {
            return null;
        }

        public virtual void Start() { }

        public virtual void SetNode(BaseInputNode data, int sockel) { }

        public virtual object[] SaveNode()
        {
            return null;
        }

        public virtual void LoadNode(NodeValues data) { }

        public virtual bool HasField(Vector2 pos)
        {
            return false;
        }

        public virtual NodeData GetField(Vector2 pos)
        {
            return null;
        }

        public virtual NodeData GetField(int field, FieldType type)
        {
            return null;
        }

        public virtual void Updater() { }

        public virtual void Tick(float deltaTime) { }

        #region Delegates

        #endregion
    }

    public enum FieldType : sbyte
    {
        Input = 0,
        Output = 1
    }
}