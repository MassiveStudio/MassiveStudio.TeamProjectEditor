﻿using System.Collections.Generic;
using System.Collections;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace MassiveStudio.NodeEditor
{
    public static class ZoomGUI
    {
        static int startx;
        static int startx2;
        static int starty;

        static int width_1;
        static int width_2;
        static int height;

        static float cache;
        static float Textpadding;

        static int linewidth;

        public static void BeginZoomableGroup(int padding_x, int padding_y, int width, float zoom, float padding = 2)
        {
            linewidth = (int)((width - 2 * padding_x) * zoom);
            startx = (int)(padding_x * zoom);
            starty = (int)(padding_y * zoom);
            height = (int)(17 * zoom);

            width_1 = (int)((width * zoom) / 2 - startx);
            startx2 = (int)(startx + width_1);

            width_2 = (int)((width * zoom - (width_1 + 2 * startx)));

            Textpadding = padding;
            cache = starty;
        }

        public static void GUIZoomLabel(string name)
        {
            GUI.Label(new Rect(startx, cache, width_1+ width_2, height), name);
            cache += (height + Textpadding);
        }

        public static float GUIZoomField(string name, float value)
        {
            GUI.Label(new Rect(startx, cache, width_1, height), name);
            var a = EditorGUI.FloatField(new Rect(startx2, cache, width_2, height), value);
            cache += (height + Textpadding);
            return a;
        }

        public static int GUIZoomField(string name, int value)
        {
            GUI.Label(new Rect(startx, cache, width_1, height), name);
            var a = EditorGUI.IntField(new Rect(startx2, cache, width_2, height), value);
            cache += (height + Textpadding);
            return a;
        }

        public static string GUIZoomField(string name, string value)
        {
            GUI.Label(new Rect(startx, cache, width_1, height), name);
            var a = EditorGUI.TextField(new Rect(startx2, cache, width_2, height), value);
            cache += (height + Textpadding);
            return a;
        }

        public static System.Enum GUIZoomField(System.Enum sel)
        {
            System.Type type = sel.GetType();
            System.Enum[] array = System.Enum.GetValues(type).Cast<System.Enum>().ToArray<System.Enum>();
            string[] name = System.Enum.GetNames(type);

            int wx = linewidth / array.Length;
            int selx = System.Array.IndexOf<System.Enum>(array, sel);

            for (int x = 0; x < array.Length; x++)
            {
                if (selx == x)
                    GUI.backgroundColor = new Color32(100, 100, 100, 255);
                else
                    GUI.backgroundColor = Color.white;
                if (GUI.Button(new Rect(startx + wx * x, cache, wx, height), name[x]))
                    selx = x;
            }

            System.Enum result;
            if (selx < 0 || selx >= name.Length)
                result = sel;
            else
                result = array[selx];

            cache += (height + Textpadding);

            return result;
        }

        public static void GUIZoomField(Texture2D tex)
        {
            GUI.DrawTexture(new Rect(startx, cache, width_1 + width_2, width_1 + width_2), tex);
            cache += ((width_1 + width_2) + Textpadding);
        }

        public static float GUIZoomSlider(string name, float value, float min, float max)
        {
            GUI.Label(new Rect(startx, cache, width_1, height), name);//Label of the Slider
            var a = GUI.HorizontalSlider(new Rect(startx2, cache, width_2, height), value, min, max);
            cache += (height + Textpadding);
            return a;
        }

        public static void EndZoomableGroup()
        {
            cache = 0;
        }
    }
}
