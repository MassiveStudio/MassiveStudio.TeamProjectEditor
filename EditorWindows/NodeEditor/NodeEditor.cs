﻿using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Collections;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using System.IO;
using System;

using MassiveStudio.TeamProjectEditor.Utility;

namespace MassiveStudio.NodeEditor
{
    public abstract class NodeEditor : EditorWindow
    {
        private List<BaseNode> windows = new List<BaseNode>();
        private Vector2 mousePos;

        //Selection
        private BaseNode selectedNode;

        //Connection
        private bool makeTransitionMode = false;
        private int TransitionModeType = -1;
        private NodeData Node1;
        private NodeData Node2;
        private Vector2 firstClick;
        private Vector2 SelectedConnection;

        private bool selected = false;

        private Rect selection = new Rect(100f, 100f, 0f, 0f);
        private bool showselectionMode = false;
        private bool drawselection = false;
        private int selectioncount = 0;

        private bool isTranslateSelectedWindowMode = false;
        private Vector2 WindowTranslate;
        private List<int> winlist = new List<int>();

        private float zoom = 1f;

        private bool isScrollingView = false;
        private Vector2 scrollStartMousePos;

        private Vector2 scrollPosition;

        public Vector4 NodeArea = new Vector4(0, 0, 500, 500);
        private bool NodeChanged = false;

        public static NodeEditor editor
        {
            get
            {
                if (_editor == null)
                    _editor = GetWindow<NodeEditor>();
                return _editor;
            }
            set
            {
                _editor = value;
            }
        }
        public static NodeEditor _editor;

        [SerializeField]
        static MethodInfo HandleUtilityApplyWireMaterial
        {
            get
            {
                if (_HandleUtilityApplyWireMaterial == null)
                    _HandleUtilityApplyWireMaterial = typeof(UnityEditor.HandleUtility).GetMethod("ApplyWireMaterial", BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance, null, CallingConventions.Any, new Type[] { }, null);
                return _HandleUtilityApplyWireMaterial;
            }
        }
        protected static MethodInfo _HandleUtilityApplyWireMaterial;

        Event e;
        Vector2 MousePositionInScrollView;

        Rect selrect = new Rect();
        public bool drawcontex = false;

        private static readonly Color kGridMinorColorDark = new Color(0f, 0f, 0f, 0.18f);
        private static readonly Color nodeflowColor = new Color32(0, 230, 64, 255);

        GUIContexMenu contexmenu;

        GenericMenu menu
        {
            get
            {
                if (_menu == null)
                    InitIndexMenu();
                return _menu;
            }
        }
        GenericMenu _menu;

        //In order to be accessible the window from the menue we add a menu item
        /*[MenuItem("Window/Node Editor")]
        static void ShowEditor()
        {
            editor = EditorWindow.GetWindow<NodeEditor>();
            //editor.stopWatch.Start ();
        }*/
        void Update()
        {
            Repaint();
            /*if (counter >= 10)
            {
                counter = 0;
                UpdaterCall();
            }*/
        }
        void UpdaterCall()
        {
            try
            {
                foreach (BaseNode b in windows)
                {
                    if (b != null)
                        b.Updater();
                }
            }
            catch (System.Exception e)
            {
                Debug.LogError("ERROR: " + e.Message + "\n" + e.StackTrace);
                windows.Clear();
            }
        }
        void Zoom()
        {
            if (e.type == EventType.ScrollWheel)
            {
                float tz = zoom;

                zoom -= e.delta.y / 10f;

                if (zoom < 0f)
                    zoom = 0.01f;

                if (zoom > 2.5f)
                    zoom = 2.5f;

                float centerX = NodeArea.x + (mousePos.x - scrollPosition.x);// scrollPosition.x + mousePos.x;
                float centerY = NodeArea.x + (mousePos.y - scrollPosition.y);//scrollPosition.y + 
                float zoomFactor = zoom / tz;
                centerX *= zoomFactor;
                centerY *= zoomFactor;
                Vector2 p = new Vector2(centerX - mousePos.x, centerY - mousePos.y);
                NodeArea.x = p.x;
                NodeArea.y = p.y;
                //Recalculate Mouse Movement from zoom and restore Position

                float val = zoom - tz;
                if (val != 0f)
                {
                    Vector2 zoommovement = -MousePositionInScrollView * tz * val;
                    // Debug.Log("ZoomOffset: " + val + " MM: " + zoommovement.ToString());
                    NodeArea.x -= zoommovement.x;
                    NodeArea.y -= zoommovement.y;
                }
            }
        }
        public virtual void DrawToolbar(ref Rect rect)
        {
            GUILayout.BeginArea(rect);
            GUILayout.BeginHorizontal(EditorStyles.toolbar, new GUILayoutOption[0]);
            //GUILayout.Button("TEST", EditorStyles.toolbarButton, new GUILayoutOption[0]);

            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Clear", EditorStyles.toolbarButton, new GUILayoutOption[0]))
                windows.Clear();
            if (GUILayout.Button("Save", EditorStyles.toolbarButton, new GUILayoutOption[0]))
                SaveNodes();
            if (GUILayout.Button("Load", EditorStyles.toolbarButton, new GUILayoutOption[0]))
                LoadNodes();
            if (GUILayout.Button("Middle", EditorStyles.toolbarButton, new GUILayoutOption[0]))
            {
                NodeArea.x = 0f;
                NodeArea.y = 0;
                NodeArea.z = Screen.width;
                NodeArea.w = Screen.height;
                scrollPosition = new Vector2(0f, 0f);
                zoom = 1f;
            }
            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }
        void RecalculateSelectionBox()
        {
            if (selection.x < selection.width)
            {
                selrect.x = selection.x;
                selrect.width = selection.width - selection.x;
            }
            else
            {
                selrect.x = selection.width;
                selrect.width = selection.x - selection.width;
            }

            if (selection.y < selection.height)
            {
                selrect.y = selection.y;
                selrect.height = selection.height - selection.y;
            }
            else
            {
                selrect.y = selection.height;
                selrect.height = selection.y - selection.height;
            }
        }
        void DeleteSelectedNodes()
        {
            for (int i = 0; i < windows.Count; i++)
            {
                if ((showselectionMode || drawselection) && (selrect.Contains(windows[i].windowRect.max) || selrect.Contains(windows[i].windowRect.min)))
                {
                    BaseNode selNode = windows[i];
                    windows.RemoveAt(i);

                    foreach (BaseNode n in windows)
                        n.NodeDeleted(selNode);
                }
            }
        }
        void DrawGridLines(float gridSize, Color gridColor)
        {
            Vector4 Area = new Vector4(0, 0, position.width, position.height);
            // Area.x -= scrollPosition.x;
            // Area.y -= scrollPosition.y;

            // Area.x += scrollPosition.x;
            // Area.y += scrollPosition.y;

            Vector2 gridmover = new Vector2(NodeArea.x % gridSize, NodeArea.y % gridSize);

            GL.Color(gridColor);
            for (float num = (Area.x - Area.x % gridSize); num < Area.z; num += gridSize)
            {
                this.DrawLine(new Vector2(num - gridmover.x, Area.y - gridmover.y), new Vector2(num - gridmover.x, Area.w - gridmover.y));
            }
            GL.Color(gridColor);
            for (float num2 = (Area.y - Area.y % gridSize); num2 < Area.w; num2 += gridSize)
            {
                this.DrawLine(new Vector2(Area.x - gridmover.x, num2 - gridmover.y), new Vector2(Area.z - gridmover.x, num2 - gridmover.y));
            }
        }
        void DrawLine(Vector2 p1, Vector2 p2)
        {
            GL.Vertex(p1);
            GL.Vertex(p2);
        }
        void DrawGrid(ref Rect rect)
        {
            GUI.BeginGroup(rect);
            //Draw Grid
            if (Event.current.type == EventType.Repaint)
            {
                // HandleUtility.ApplyWireMaterial();
                HandleUtilityApplyWireMaterial.Invoke(null, null);
                GL.PushMatrix();
                GL.Begin(1);
                this.DrawGridLines(12f * zoom, kGridMinorColorDark);
                this.DrawGridLines(120f * zoom, kGridMinorColorDark);
                GL.End();
                GL.PopMatrix();
            }
            GUI.EndGroup();
        }
        void DrawWindows()
        {
            int fontSize = GUI.skin.font.fontSize;
            GUI.skin.textField.fontSize = Mathf.RoundToInt(fontSize * zoom);
            GUI.skin.label.fontSize = Mathf.RoundToInt(fontSize * zoom);

            BeginWindows();
            for (int i = 0; i < windows.Count; i++)
            {
                if (windows[i] == null)
                    break;

                Rect winRect = new Rect(windows[i].windowRect.x * zoom, windows[i].windowRect.y * zoom, windows[i].windowRect.width * zoom, windows[i].windowRect.height * zoom);
                if ((showselectionMode || drawselection) && (selrect.Contains(windows[i].windowRect.max * zoom) || selrect.Contains(windows[i].windowRect.min * zoom) || selrect.Contains(windows[i].windowRect.center * zoom)))
                {
                    GUI.color = new Color(1f, 1f, 1f);
                    selectioncount++;
                    //BUG: GUI COLOR ON WINDOW NOT CANGE				
                    GUI.Box(new Rect(winRect.x - 10f, winRect.y - 10f, winRect.width + 20f, winRect.height + 20f), "");
                }
                else
                    GUI.color = new Color(0.8f, 0.8f, 0.8f);

                windows[i].zoom = zoom;

                winRect = GUI.Window(i, winRect, DrawNodeWindow, windows[i].windowTitle);//windows [i].windowRect

                if (drawselection)
                    if (winRect.Contains(MousePositionInScrollView) && e.type == EventType.MouseDown)
                    {
                        Debug.Log("DISABLE");
                        drawselection = false;
                    }

                #region  DRAW OUTPUT
                Rect outputRect;
                if (windows[i].Outputs != null)
                {/*
                outputRect = winRect;
                outputRect.x += outputRect.width;
                float hy = outputRect.height / (windows[i].Outputs.Length + 1);
                outputRect.y = winRect.y + hy;
                outputRect.width = 25 * zoom;
                outputRect.height = outputRect.width;*/
                    if (TransitionModeType == 0)
                        GUI.enabled = false;
                    for (int c = 0; c < windows[i].Outputs.Length; c++)
                    {
                        outputRect = windows[i].Outputs[c].rect;
                        //Debug.Log("rec: " + outputRect.ToString());
                        outputRect.position += windows[i].windowRect.position;
                        outputRect.position *= zoom;
                        outputRect.size *= zoom;
                        //windows[i].Outputs[c].rect = new Rect(windows[i].windowRect.width, hy* c, 25, 25);

                        if (GUI.Button(outputRect, ""))
                        {
                            if (Node1 == null)
                            {
                                Node1 = windows[i].GetField(c, FieldType.Output);
                                SelectedConnection = windows[i].Outputs[c].rect.center;// outputRect.position - winRect.position;

                                selectedNode = windows[i];
                                makeTransitionMode = true;
                                TransitionModeType = 0;
                                selected = true;
                            }
                            else
                            {
                                Node2 = windows[i].GetField(c, FieldType.Output);

                                Node2.node = Node1.self;
                                //Node2.result = Node1;
                                Node1.node = Node2.self;
                                Node1.rect2 = new Rect(windows[i].Outputs[c].rect.center, Node2.rect.size);//Node2.rect.position - Node2.self.windowRect.position
                                                                                                           //SelectedConnection

                                Node1 = null;
                                Node2 = null;
                                makeTransitionMode = false;
                                TransitionModeType = -1;
                            }
                        }
                        //outputRect.y += hy;
                    }
                    GUI.enabled = true;
                }
                #endregion
                #region DRAW INPUTS
                Rect inputRect;
                if (windows[i].Inputs != null)
                {
                    /*
                    inputRect = winRect;
                    inputRect.x -= 25 * zoom;
                    float hy = inputRect.height / (windows[i].Inputs.Length + 1);
                    inputRect.y = winRect.y + hy;
                    inputRect.width = 25 * zoom;
                    inputRect.height = inputRect.width;
                    */
                    if (TransitionModeType == 1)
                        GUI.enabled = false;
                    for (int c = 0; c < windows[i].Inputs.Length; c++)
                    {
                        inputRect = windows[i].Inputs[c].rect;
                        inputRect.position += windows[i].windowRect.position;
                        inputRect.position *= zoom;
                        inputRect.size *= zoom;
                        //windows[i].Inputs[c].rect = inputRect;

                        if (GUI.Button(inputRect, ""))
                        {
                            if (Node1 == null)
                            {
                                TransitionModeType = 1;
                                Node1 = windows[i].GetField(c, FieldType.Input);
                                SelectedConnection = windows[i].Inputs[c].rect.center;// inputRect.center - winRect.position;

                                selectedNode = windows[i];
                                makeTransitionMode = true;
                                selected = true;
                                //GUI.UnfocusWindow();
                            }
                            else
                            {
                                Node2 = windows[i].GetField(c, FieldType.Input);

                                Node2.node = Node1.self;
                                Node1.node = Node2.self;

                                Node2.result = Node1;
                                Node2.rect2 = new Rect(SelectedConnection, Node1.rect.size);//Node1.rect.position - Node1.self.windowRect.position

                                Node1 = null;
                                Node2 = null;
                                makeTransitionMode = false;
                                TransitionModeType = -1;
                            }
                        }
                        //inputRect.y += hy;
                    }
                    GUI.enabled = true;
                }
                #endregion

                winRect.x /= zoom;
                winRect.y /= zoom;
                winRect.width /= zoom;
                winRect.height /= zoom;

                windows[i].windowRect = winRect;

            }
            EndWindows();
            //Reset Values

            GUI.skin.textField.fontSize = fontSize;
            GUI.skin.label.fontSize = fontSize;
            GUI.color = Color.white;
        }
        void DoSelection()
        {
            if (!makeTransitionMode && e.button == 0)
            {
                if (e.type == EventType.MouseDown && e.mousePosition.x < position.width - 15 && e.mousePosition.y < position.height - 15)
                {
                    selection = new Rect(MousePositionInScrollView.x, MousePositionInScrollView.y, 0, 0);

                    drawselection = true;
                    //CHECK WINDOW HIT
                    for (int i = 0; i < windows.Count; i++)
                    {
                        if (windows[i].windowRect.Contains(MousePositionInScrollView))
                        {
                            drawselection = false;
                            break;
                        }
                    }
                }

                if (drawselection)
                {
                    selection.width = MousePositionInScrollView.x;
                    selection.height = MousePositionInScrollView.y;
                }
            }

            if (drawselection && e.button == 0 && e.type == EventType.MouseUp)
            {
                drawselection = false;
                if (isTranslateSelectedWindowMode)
                {
                    isTranslateSelectedWindowMode = false;
                    showselectionMode = false;
                }
                else
                {
                    if (selectioncount > 0)
                        showselectionMode = true;
                }
            }

            if (showselectionMode)
            {
                if (e.button == 0 && e.type == EventType.MouseUp)
                {
                    if (isTranslateSelectedWindowMode)
                    {
                        isTranslateSelectedWindowMode = false;
                        showselectionMode = false;
                    }
                }
                else if (e.button == 0 && e.type == EventType.MouseDown)
                {
                    bool hit = false;
                    winlist.Clear();
                    //Check if Hit Window for Translate
                    for (int i = 0; i < windows.Count; i++)
                    {
                        if (selrect.Contains(windows[i].windowRect.max) || selrect.Contains(windows[i].windowRect.min))
                            winlist.Add(i);
                        if (windows[i].windowRect.Contains(MousePositionInScrollView))
                            hit = true;
                    }
                    if (winlist.Count == 0 || !hit)
                    {
                        showselectionMode = false;
                        selection = new Rect(0f, 0f, 0f, 0f);
                    }
                    else
                    {
                        isTranslateSelectedWindowMode = true;
                        WindowTranslate = e.mousePosition;
                    }
                }
            }

            if (isTranslateSelectedWindowMode)
            {
                Vector2 trans = e.mousePosition - WindowTranslate;

                foreach (int a in winlist)
                {
                    windows[a].windowRect.x += trans.x / 1;
                    windows[a].windowRect.y += trans.y / 1;
                }
                WindowTranslate = e.mousePosition;
            }
        }
        void DoConnection()
        {
            if (!makeTransitionMode && e.button == 1)
            {
                if (e.type == EventType.MouseDown)
                {
                    DrawIndexMenu();
                    Node1 = null;
                    for (int i = 0; i < windows.Count; i++)
                    {
                        if (windows[i].windowRect.Contains(MousePositionInScrollView))
                        {
                            Debug.Log("HIT WINDOW");
                            Node1 = windows[i].GetField(MousePositionInScrollView);
                            firstClick = MousePositionInScrollView;
                            makeTransitionMode = true;
                        }
                    }

                    if (Node1 != null)
                    {
                        Debug.Log("ON BOX");
                        if (Node1.hasfield)
                        {
                            DrawEditMenu();
                            selected = true;
                            selectedNode = Node1.node;
                            GUI.UnfocusWindow();
                        }
                        else
                            DrawIndexMenu();
                    }
                    else
                    {
                        DrawIndexMenu();
                    }
                    e.Use();
                }
            }
            else if (makeTransitionMode)
            {
                drawselection = false;
                if (e.button == 0 && e.type == EventType.MouseDown)
                {
                    Node2 = null;
                    for (int i = 0; i < windows.Count; i++)
                    {
                        if (windows[i].windowRect.Contains(MousePositionInScrollView))
                        {
                            Node2 = windows[i].GetField(MousePositionInScrollView);
                        }
                    }

                    if (Node2 != null)
                    {
                        if (Node2.hasfield && (Node1.type != Node2.type))
                        {
                            //Node2.node = Node1.self;
                            //Node1.node = Node2.self;
                            Node2.self.SetInput(Node1, MousePositionInScrollView);
                            Node1.self.SetInput(Node2, firstClick);//new Vector2(Node1.rect.x, Node1.rect.y));
                            Debug.Log("SET CURVE BETWEEN " + Node1.self + " : " + Node2.self);
                            makeTransitionMode = false;
                            selected = false;
                        }
                        /*else{
                            makeTransitionMode=false;
                            selected=false;
                        }*/
                    }
                    else
                    {
                        makeTransitionMode = false;
                        //Node1 = null;
                        TransitionModeType = -1;
                    }
                }

                if (selected)
                {
                    Rect mouseRect = new Rect(e.mousePosition.x, e.mousePosition.y, 10, 10);
                    //Rect winRect = new Rect(Node1.self.windowRect.x + Node1.rect.x - scrollPosition.x - NodeArea.x, Node1.self.windowRect.y + Node1.rect.y - scrollPosition.y - NodeArea.y, Node1.rect.width, Node1.rect.height);
                    Rect winRect = new Rect((Node1.self.windowRect.x + SelectedConnection.x) * zoom - scrollPosition.x - NodeArea.x, (Node1.self.windowRect.y + SelectedConnection.y) * zoom - scrollPosition.y - NodeArea.y, 25 * zoom, 25 * zoom);
                    //GUI.backgroundColor = Color.blue;
                    //GUI.Box(winRect, "");
                    //GUI.backgroundColor = Color.white;
                    DrawNodeCurve(winRect, mouseRect);
                }
            }
        }
        void OnGUI()
        {
            e = Event.current;
            mousePos = e.mousePosition;

            if (contexmenu == null)
            {
                contexmenu = new GUIContexMenu();
                contexmenu.Init();
            }

            if (e.type == EventType.MouseDown && e.button == 1)
            {
                contexmenu.ShowContexMenu();
            }

            //Set Dark Style
            if (Event.current.type == EventType.Repaint)
            {
                GUIStyle graphBackground = "flow background";
                graphBackground.Draw(new Rect(0, 0, position.width, position.height), false, false, false, false);
            }
            Rect toolbarRect = new Rect(0, 0, position.width, 25);
            DrawToolbar(ref toolbarRect);
            Zoom();
            //RecalculateWindows ();

            MousePositionInScrollView = new Vector2((mousePos.x + NodeArea.x + scrollPosition.x), (mousePos.y + NodeArea.y + scrollPosition.y));

            NodeArea.z = Screen.width + (Screen.width - (Screen.width * zoom));
            NodeArea.w = Screen.height + (Screen.height - (Screen.height * zoom));

            //SELECTION BOX
            RecalculateSelectionBox();


            float height = toolbarRect.height + toolbarRect.y;
            Rect gridRect = new Rect(0, height, position.width, position.height - height);
            DrawGrid(ref gridRect);

            DoSelection();

            DoConnection();

            //DELETE SELECTED NODES
            if (e.keyCode == KeyCode.Delete && selectioncount > 0)
                DeleteSelectedNodes();

            scrollPosition = GUI.BeginScrollView(new Rect(0, 25, position.width, position.height - 25), scrollPosition, new Rect(NodeArea.x, NodeArea.y, NodeArea.z, NodeArea.w), true, true);// new Rect(10, 10, 500, 500), true, true);

            if (drawselection)
                GUI.Box(selrect, "");

            DrawConnections();

            selectioncount = 0;

            DrawWindows();

            mousePos = e.mousePosition - scrollPosition;

            GUI.EndScrollView();

            //DrawDebug();

            RecalculateView();

            /*
            if (contexmenu.open)
            {
                BeginWindows();
                contexmenu.OnMenu();
                EndWindows();
            }*/
        }
        void OnDisable()
        {
            _menu = null;
        }

        void RecalculateView()
        {
            if (e.button == 2)
            {
                if (e.type == EventType.MouseDown)
                {
                    scrollStartMousePos = e.mousePosition;
                    isScrollingView = true;
                }
                else if (e.type == EventType.MouseUp)
                {
                    isScrollingView = false;
                }
            }

            if (isScrollingView)
            {
                Vector2 mouseDiff = e.mousePosition - scrollStartMousePos;

                scrollStartMousePos = e.mousePosition;

                if (scrollPosition.x < 0)
                {
                    NodeArea.x += scrollPosition.x;
                    scrollPosition.x = 0f;
                }
                else
                {
                    NodeArea.x -= mouseDiff.x;
                }

                if (scrollPosition.y < 0)
                {
                    NodeArea.y += scrollPosition.y;
                    scrollPosition.y = 0f;
                }
                else
                {
                    NodeArea.y -= mouseDiff.y;
                }
            }
        }

        void DrawConnections()
        {
            if (e.type == EventType.Repaint)
            {
                foreach (BaseNode n in windows)
                    if (n != null)
                        n.DrawCurves();
            }
        }

        void DrawDebug()
        {

            GUI.enabled = false;
            NodeArea = EditorGUI.Vector4Field(new Rect(0, 0, 500, 50), "POS:", NodeArea);
            GUI.enabled = true;
            //MATRIXPOS = EditorGUI.Vector3Field (new Rect (0, 200, 500, 50), "Matrix", MATRIXPOS);
            zoom = EditorGUI.FloatField(new Rect(0, 250, 500, 50), "Zoom", zoom);

            GUI.Label(new Rect(0, 500, 200, 50), "Pos: " + position.width + " : " + position.height);
            GUI.Label(new Rect(0, 550, 200, 50), "Window: " + NodeArea.x + " : " + NodeArea.y + " : " + NodeArea.w + " : " + NodeArea.z);
            GUI.Label(new Rect(0, 600, 200, 50), "Scroll: " + scrollPosition.ToString());
            GUI.Label(new Rect(0, 650, 200, 50), "Index " + selectioncount);
            GUI.Label(new Rect(0, 700, 200, 50), "Zoom " + zoom + "\t Screen: " + Screen.width + ":" + Screen.height);

            GUI.Label(new Rect(0, 50, 200, 50), "Draw Selection: " + drawselection);
            GUI.Label(new Rect(0, 75, 200, 50), "Store Selection: " + showselectionMode);
            GUI.Label(new Rect(0, 100, 200, 50), "Selection Count: " + winlist.Count);
            GUI.Label(new Rect(0, 125, 200, 50), "makeTransitionMode: " + makeTransitionMode);
        }

        void DrawIndexMenu()
        {
            menu.ShowAsContext();
        }

        void InitIndexMenu()
        {
            Debug.Log("DRAW GENERIC MENU");
            _menu = new GenericMenu();
            IndexMenu();
        }

        public abstract void IndexMenu();

        public void AddCallback(string name, string callback)
        {
            Debug.Log("ADD CALLBACK " + name);
            _menu.AddItem(new GUIContent(name), false, ContextCallback, callback);
        }

        void DrawEditMenu()
        {
            GenericMenu menu = new GenericMenu();
            menu.AddItem(new GUIContent("Make Transition"), false, ContextCallback, "makeTransition");
            menu.AddSeparator("");
            menu.AddItem(new GUIContent("Delete Node"), false, ContextCallback, "deleteNode");
            //we use it so that it will show
            menu.ShowAsContext();
        }
        //function that draws the windows
        void DrawNodeWindow(int id)
        {
            windows[id].DrawWindow();
            if (!isTranslateSelectedWindowMode && e.button != 2)
                GUI.DragWindow();
        }
        //Is called when a selection from the context menu is made
        void ContextCallback(object obj)
        {
            mousePos += scrollPosition;
            string clb = obj.ToString();

            ContextCallback(clb, mousePos);
            //Generatoren
            /*if (clb.Equals("RidgedNoiseNode"))
            {
                RidgedNoiseNode ridgedNoiseNode = new RidgedNoiseNode();
                ridgedNoiseNode.windowRect = new Rect(mousePos.x, mousePos.y, 200, 300);
                ridgedNoiseNode.NodeType = 0;
                windows.Add(ridgedNoiseNode);
            }*/
            NodeChanged = true;
        }

        public void AddNode(BaseNode node)
        {
            windows.Add(node);
            node.Start();
        }

        public virtual void ContextCallback(string callback, Vector2 mousePos){}

        //draw the node curve from the middle of the start Rect to the middle of the end rect 
        public static void DrawNodeCurve(Rect start, Rect end)
        {
            Vector3 startPos = new Vector3(start.x + start.width / 2, start.y + start.height / 2, 0);
            Vector3 endPos = new Vector3(end.x + end.width / 2, end.y + end.height / 2, 0);
            Vector3 startTan = startPos + Vector3.right * 50;
            Vector3 endTan = endPos + Vector3.left * 50;
            Color shadowCol = new Color(0, 0, 0, 0.06f);

            for (int i = 0; i < 3; i++)// Draw a shadow
                Handles.DrawBezier(startPos, endPos, startTan, endTan, shadowCol, null, (i + 1) * 5);

            Handles.DrawBezier(startPos, endPos, startTan, endTan, Color.black, null, 1);
        }

        public static void DrawNodeFlow(Rect start, Rect end)
        {
            Vector3 startPos = new Vector3(start.x + start.width / 2, start.y + start.height / 2, 0);
            Vector3 endPos = new Vector3(end.x + end.width / 2, end.y, 0);
            Handles.color = nodeflowColor;
            Handles.DrawLine(startPos, endPos);
        }

        public BaseNode AddNode(int id)
        {
            int ca = windows.Count;

            switch (id)
            {
                case 0:
                    ContextCallback("RidgedNoiseNode");
                    break;
                case 1:
                    ContextCallback("PinkNoiseNode");
                    break;
                case 2:
                    ContextCallback("GradientNoiseNode");
                    break;
                case 3:
                    ContextCallback("VoronoiValleysNode");
                    break;
                case 4:
                    ContextCallback("");
                    break;
                case 5:
                    ContextCallback("");
                    break;
                case 6:
                    ContextCallback("");
                    break;
                case 10:
                    ContextCallback("BiasNode");
                    break;
                case 11:
                    ContextCallback("GainNode");
                    break;
                case 12:
                    ContextCallback("TurbulenceNode");
                    break;
                case 14:
                    ContextCallback("ScaleNode");
                    break;
                case 20:
                    ContextCallback("CurveNode");
                    break;
                case 21:
                    ContextCallback("MathfNode");
                    break;
                case 25:
                    ContextCallback("GenerateToTextureNode");
                    break;
                case 27:
                    ContextCallback("BillowNoiseNode");
                    break;
            }

            if (windows.Count > ca)
            {
                return windows[ca];
            }
            else
            {
                return null;
            }
        }

        #region Save'nLoad
        public void SaveNodes()
        {
            List<NodeWindowData> nodes = new List<NodeWindowData>();

            int c = 0;
            foreach (BaseNode a in windows)
            {
                NodeWindowData ndata = new NodeWindowData();
                ndata.type = a.NodeType;
                ndata.id = c;

                ndata.posx = a.windowRect.x;
                ndata.posy = a.windowRect.y;
                ndata.width = a.windowRect.width;
                ndata.height = a.windowRect.height;

                ndata.values = new NodeValues();
                ndata.values.data = a.SaveNode();

                for (int sockel = 0; sockel < a.Inputs.Length; sockel++)
                    if (a.Inputs[sockel].node != null)
                        ndata.con.Add(new NodeIndentify(GetWindowID(a.Inputs[sockel].node), sockel));

                nodes.Add(ndata);
                c++;
            }

            FileStream fs = new FileStream(Application.dataPath + "/Generator" + ".dat", FileMode.Create);
            BinaryFormatter formatter = new BinaryFormatter();

            try
            {
                formatter.Serialize(fs, nodes);
            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to serialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }

            Debug.Log("Saved");

        }

        public void LoadNodes()
        {
            List<NodeWindowData> nodes;
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fs = File.Open(Application.dataPath + "/Generator" + ".dat", FileMode.Open);

            try
            {
                nodes = (List<NodeWindowData>)formatter.Deserialize(fs);
            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to deserialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }
            //ADD NODES
            foreach (NodeWindowData a in nodes)
            {
                BaseNode b = AddNode(a.type);
                Debug.Log("NN: " + a.type.ToString());

                if (b != null)
                {
                    b.windowRect = new Rect(a.posx, a.posy, a.width, a.height);

                    b.LoadNode(a.values);
                    b.Start();//Intialize NodeData
                }
            }

            //CONNECT TOGETHER    -  ERROR
            for (int b = 0; b < nodes.Count; b++)
            {
                if (nodes[b].con.Count > 0)
                {
                    for (int c = 0; c < nodes[b].con.Count; c++)
                    {
                        NodeIndentify nv = nodes[b].con[c];
                        Debug.Log(windows[b] + " CON " + windows[nv.id]);

                        //					windows[b].SetNode((BaseInputNode)windows[nv.id], nv.sockel);

                        /*NodeData[] node = windows[nv.id].GetNodes ();
                        for (int s = 0; s< node.Length; s++) {
                            if (node [s].type == 0) {
                                windows[nv.id].SetNode((BaseInputNode) windows[b], s);
                                break;
                            }
                        }*/

                    }
                }
            }


            Debug.Log("Loaded " + nodes.Count);
        }

        public int GetWindowID(BaseNode a)
        {
            int index = windows.IndexOf(a);

            if (index == -1)
                Debug.LogError("WINDOW NOT EXISTS");

            return index;

        }
        #endregion
    }

    [System.Serializable]
    public class NodeWindowData
    {
        public int id;
        public int type;

        public NodeValues values;//object[] data;

        public float posx;
        public float posy;
        public float width;
        public float height;

        public List<NodeIndentify> con = new List<NodeIndentify>();
    }

    [System.Serializable]
    public struct NodeIndentify
    {
        public int id;
        public int sockel;
        public NodeIndentify(int i, int s)
        {
            this.id = i;
            this.sockel = s;
        }
    }

    [System.Serializable]
    public class NodeValues
    {
        public object[] data;

        public NodeValues()
        {
        }
    }
}