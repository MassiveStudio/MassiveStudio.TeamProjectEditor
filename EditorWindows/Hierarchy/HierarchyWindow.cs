﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEditor.IMGUI.Controls;
using UnityEditor.SceneManagement;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEditor;

namespace MassiveStudio.TeamProjectEditor.EditorWindows.Hierarchy
{
    /*

    namespace UnityEditor
    {
        // Token: 0x020004C3 RID: 1219
        //[EditorWindowTitle(title = "Hierarchy", useTypeNameAsIconName = true)]
        internal class SceneHierarchyWindow : SearchableEditorWindow, IHasCustomMenu
        {
            // Token: 0x06002C4D RID: 11341 RVA: 0x000A8B52 File Offset: 0x000A6D52
            public SceneHierarchyWindow()
            {
            }

            // Token: 0x17000815 RID: 2069
            // (get) Token: 0x06002C4E RID: 11342 RVA: 0x000A8B90 File Offset: 0x000A6D90
            public static SceneHierarchyWindow lastInteractedHierarchyWindow
            {
                get
                {
                    return SceneHierarchyWindow.s_LastInteractedHierarchy;
                }
            }

            // Token: 0x06002C4F RID: 11343 RVA: 0x000A8BAC File Offset: 0x000A6DAC
            public static List<SceneHierarchyWindow> GetAllSceneHierarchyWindows()
            {
                return SceneHierarchyWindow.s_SceneHierarchyWindow;
            }

            // Token: 0x17000816 RID: 2070
            // (get) Token: 0x06002C50 RID: 11344 RVA: 0x000A8BC8 File Offset: 0x000A6DC8
            // (set) Token: 0x06002C51 RID: 11345 RVA: 0x000A8BE8 File Offset: 0x000A6DE8
            internal bool isLocked
            {
                get
                {
                    return this.m_LockTracker.isLocked;
                }
                set
                {
                    this.m_LockTracker.isLocked = value;
                }
            }

            // Token: 0x17000817 RID: 2071
            // (get) Token: 0x06002C52 RID: 11346 RVA: 0x000A8BF8 File Offset: 0x000A6DF8
            // (set) Token: 0x06002C53 RID: 11347 RVA: 0x000A8C17 File Offset: 0x000A6E17
            internal static bool debug
            {
                get
                {
                    return SceneHierarchyWindow.lastInteractedHierarchyWindow.m_Debug;
                }
                set
                {
                    SceneHierarchyWindow.lastInteractedHierarchyWindow.m_Debug = value;
                }
            }

            // Token: 0x17000818 RID: 2072
            // (get) Token: 0x06002C54 RID: 11348 RVA: 0x000A8C28 File Offset: 0x000A6E28
            // (set) Token: 0x06002C55 RID: 11349 RVA: 0x000A8C48 File Offset: 0x000A6E48
            public static bool s_Debug
            {
                get
                {
                    return SessionState.GetBool("HierarchyWindowDebug", false);
                }
                set
                {
                    SessionState.SetBool("HierarchyWindowDebug", value);
                }
            }

            // Token: 0x17000819 RID: 2073
            // (get) Token: 0x06002C56 RID: 11350 RVA: 0x000A8C58 File Offset: 0x000A6E58
            // (set) Token: 0x06002C57 RID: 11351 RVA: 0x000A8C73 File Offset: 0x000A6E73
            private bool treeViewReloadNeeded
            {
                get
                {
                    return this.m_TreeViewReloadNeeded;
                }
                set
                {
                    this.m_TreeViewReloadNeeded = value;
                    if (value)
                    {
                        base.Repaint();
                        if (SceneHierarchyWindow.s_Debug)
                        {
                            Debug.Log("Reload treeview on next event");
                        }
                    }
                }
            }

            // Token: 0x1700081A RID: 2074
            // (get) Token: 0x06002C58 RID: 11352 RVA: 0x000A8CA0 File Offset: 0x000A6EA0
            // (set) Token: 0x06002C59 RID: 11353 RVA: 0x000A8CBB File Offset: 0x000A6EBB
            private bool selectionSyncNeeded
            {
                get
                {
                    return this.m_SelectionSyncNeeded;
                }
                set
                {
                    this.m_SelectionSyncNeeded = value;
                    if (value)
                    {
                        base.Repaint();
                        if (SceneHierarchyWindow.s_Debug)
                        {
                            Debug.Log("Selection sync and frameing on next event");
                        }
                    }
                }
            }

            // Token: 0x1700081B RID: 2075
            // (get) Token: 0x06002C5A RID: 11354 RVA: 0x000A8CE8 File Offset: 0x000A6EE8
            // (set) Token: 0x06002C5B RID: 11355 RVA: 0x000A8D04 File Offset: 0x000A6F04
            private string currentSortingName
            {
                get
                {
                    return this.m_CurrentSortingName;
                }
                set
                {
                    this.m_CurrentSortingName = value;
                    if (!this.m_SortingObjects.ContainsKey(this.m_CurrentSortingName))
                    {
                        this.m_CurrentSortingName = this.GetNameForType(typeof(TransformSorting));
                    }
                    GameObjectTreeViewDataSource gameObjectTreeViewDataSource = (GameObjectTreeViewDataSource)this.treeView.data;
                    gameObjectTreeViewDataSource.sortingState = this.m_SortingObjects[this.m_CurrentSortingName];
                }
            }

            // Token: 0x1700081C RID: 2076
            // (get) Token: 0x06002C5C RID: 11356 RVA: 0x000A8D70 File Offset: 0x000A6F70
            private bool hasSortMethods
            {
                get
                {
                    return this.m_SortingObjects.Count > 1;
                }
            }

            // Token: 0x1700081D RID: 2077
            // (get) Token: 0x06002C5D RID: 11357 RVA: 0x000A8D94 File Offset: 0x000A6F94
            private Rect treeViewRect
            {
                get
                {
                    return new Rect(0f, 17f, base.position.width, base.position.height - 17f);
                }
            }

            // Token: 0x1700081E RID: 2078
            // (get) Token: 0x06002C5E RID: 11358 RVA: 0x000A8DDC File Offset: 0x000A6FDC
            private TreeViewController treeView
            {
                get
                {
                    if (this.m_TreeView == null)
                    {
                        this.Init();
                    }
                    return this.m_TreeView;
                }
            }

            // Token: 0x06002C5F RID: 11359 RVA: 0x000A8E08 File Offset: 0x000A7008
            private void Init()
            {
                if (this.m_TreeViewState == null)
                {
                    this.m_TreeViewState = new TreeViewState();
                }
                this.m_TreeView = new TreeViewController(this, this.m_TreeViewState);
                TreeViewController treeView = this.m_TreeView;
                treeView.itemDoubleClickedCallback = (Action<int>)Delegate.Combine(treeView.itemDoubleClickedCallback, new Action<int>(this.TreeViewItemDoubleClicked));
                TreeViewController treeView2 = this.m_TreeView;
                treeView2.selectionChangedCallback = (Action<int[]>)Delegate.Combine(treeView2.selectionChangedCallback, new Action<int[]>(this.TreeViewSelectionChanged));
                TreeViewController treeView3 = this.m_TreeView;
                treeView3.onGUIRowCallback = (Action<int, Rect>)Delegate.Combine(treeView3.onGUIRowCallback, new Action<int, Rect>(this.OnGUIAssetCallback));
                TreeViewController treeView4 = this.m_TreeView;
                treeView4.dragEndedCallback = (Action<int[], bool>)Delegate.Combine(treeView4.dragEndedCallback, new Action<int[], bool>(this.OnDragEndedCallback));
                TreeViewController treeView5 = this.m_TreeView;
                treeView5.contextClickItemCallback = (Action<int>)Delegate.Combine(treeView5.contextClickItemCallback, new Action<int>(this.ItemContextClick));
                TreeViewController treeView6 = this.m_TreeView;
                treeView6.contextClickOutsideItemsCallback = (Action)Delegate.Combine(treeView6.contextClickOutsideItemsCallback, new Action(this.ContextClickOutsideItems));
                this.m_TreeView.deselectOnUnhandledMouseDown = true;
                bool showRoot = false;
                bool rootItemIsCollapsable = false;
                GameObjectTreeViewDataSource gameObjectTreeViewDataSource = new GameObjectTreeViewDataSource(this.m_TreeView, this.m_CurrenRootInstanceID, showRoot, rootItemIsCollapsable);
                GameObjectsTreeViewDragging dragging = new GameObjectsTreeViewDragging(this.m_TreeView);
                GameObjectTreeViewGUI gui = new GameObjectTreeViewGUI(this.m_TreeView, false);
                this.m_TreeView.Init(this.treeViewRect, gameObjectTreeViewDataSource, gui, dragging);
                gameObjectTreeViewDataSource.searchMode = (int)base.searchMode;
                gameObjectTreeViewDataSource.searchString = this.m_SearchFilter;
                this.m_AllowAlphaNumericalSort = (EditorPrefs.GetBool("AllowAlphaNumericHierarchy", false) || !InternalEditorUtility.isHumanControllingUs);
                this.SetUpSortMethodLists();
                this.m_TreeView.ReloadData();
            }

            // Token: 0x06002C60 RID: 11360 RVA: 0x000A8FC5 File Offset: 0x000A71C5
            internal void SetupForTesting()
            {
                this.m_AllowAlphaNumericalSort = true;
                this.SetUpSortMethodLists();
            }

            // Token: 0x06002C61 RID: 11361 RVA: 0x000A8FD5 File Offset: 0x000A71D5
            public void SetCurrentRootInstanceID(int instanceID)
            {
                this.m_CurrenRootInstanceID = instanceID;
                this.Init();
                GUIUtility.ExitGUI();
            }

            // Token: 0x06002C62 RID: 11362 RVA: 0x000A8FEC File Offset: 0x000A71EC
            public string[] GetCurrentVisibleObjects()
            {
                IList<TreeViewItem> rows = this.m_TreeView.data.GetRows();
                string[] array = new string[rows.Count];
                for (int i = 0; i < rows.Count; i++)
                {
                    array[i] = rows[i].displayName;
                }
                return array;
            }

            // Token: 0x06002C63 RID: 11363 RVA: 0x000A9045 File Offset: 0x000A7245
            internal void SelectPrevious()
            {
                this.m_TreeView.OffsetSelection(-1);
            }

            // Token: 0x06002C64 RID: 11364 RVA: 0x000A9054 File Offset: 0x000A7254
            internal void SelectNext()
            {
                this.m_TreeView.OffsetSelection(1);
            }

            // Token: 0x06002C65 RID: 11365 RVA: 0x000A9064 File Offset: 0x000A7264
            private void OnProjectWasLoaded()
            {
                this.m_TreeViewState.expandedIDs.Clear();
                if (SceneManager.sceneCount == 1)
                {
                    this.treeView.data.SetExpanded(SceneManager.GetSceneAt(0).handle, true);
                }
                this.SetScenesExpanded(this.m_ExpandedScenes);
            }

            // Token: 0x06002C66 RID: 11366 RVA: 0x000A90BC File Offset: 0x000A72BC
            private IEnumerable<string> GetExpandedSceneNames()
            {
                List<string> list = new List<string>();
                for (int i = 0; i < SceneManager.sceneCount; i++)
                {
                    Scene sceneAt = SceneManager.GetSceneAt(i);
                    if (this.treeView.data.IsExpanded(sceneAt.handle))
                    {
                        list.Add(sceneAt.name);
                    }
                }
                return list;
            }

            // Token: 0x06002C67 RID: 11367 RVA: 0x000A9124 File Offset: 0x000A7324
            private void SetScenesExpanded(List<string> sceneNames)
            {
                List<int> list = new List<int>();
                foreach (string name in sceneNames)
                {
                    Scene sceneByName = SceneManager.GetSceneByName(name);
                    if (sceneByName.IsValid())
                    {
                        list.Add(sceneByName.handle);
                    }
                }
                if (list.Count > 0)
                {
                    this.treeView.data.SetExpandedIDs(list.ToArray());
                }
            }

            // Token: 0x06002C68 RID: 11368 RVA: 0x000A91C4 File Offset: 0x000A73C4
            private void OnSceneCreated(Scene scene, NewSceneSetup setup, NewSceneMode mode)
            {
                this.ExpandTreeViewItem(scene.handle, true);
            }

            // Token: 0x06002C69 RID: 11369 RVA: 0x000A91C4 File Offset: 0x000A73C4
            private void OnSceneOpened(Scene scene, OpenSceneMode mode)
            {
                this.ExpandTreeViewItem(scene.handle, true);
            }

            // Token: 0x06002C6A RID: 11370 RVA: 0x000A91D8 File Offset: 0x000A73D8
            private void ExpandTreeViewItem(int id, bool expand)
            {
                TreeViewDataSource treeViewDataSource = this.treeView.data as TreeViewDataSource;
                if (treeViewDataSource != null)
                {
                    treeViewDataSource.SetExpanded(id, expand);
                }
            }

            // Token: 0x06002C6B RID: 11371 RVA: 0x000A9206 File Offset: 0x000A7406
            private void Awake()
            {
                this.m_HierarchyType = HierarchyType.GameObjects;
                if (this.m_TreeViewState != null)
                {
                    this.m_TreeViewState.OnAwake();
                }
            }

            // Token: 0x06002C6C RID: 11372 RVA: 0x000A9228 File Offset: 0x000A7428
            private void OnBecameVisible()
            {
                if (SceneManager.sceneCount > 0)
                {
                    this.treeViewReloadNeeded = true;
                }
            }

            // Token: 0x06002C6D RID: 11373 RVA: 0x000A9240 File Offset: 0x000A7440
            public override void OnEnable()
            {
                base.OnEnable();
                base.titleContent = base.GetLocalizedTitleContent();
                SceneHierarchyWindow.s_SceneHierarchyWindow.Add(this);
                EditorApplication.projectChanged += this.ReloadData;
                EditorApplication.editorApplicationQuit = (UnityAction)Delegate.Combine(EditorApplication.editorApplicationQuit, new UnityAction(this.OnQuit));
                EditorApplication.searchChanged = (EditorApplication.CallbackFunction)Delegate.Combine(EditorApplication.searchChanged, new EditorApplication.CallbackFunction(this.SearchChanged));
                EditorApplication.projectWasLoaded = (UnityAction)Delegate.Combine(EditorApplication.projectWasLoaded, new UnityAction(this.OnProjectWasLoaded));
                EditorSceneManager.newSceneCreated += this.OnSceneCreated;
                EditorSceneManager.sceneOpened += this.OnSceneOpened;
                SceneHierarchyWindow.s_LastInteractedHierarchy = this;
            }

            // Token: 0x06002C6E RID: 11374 RVA: 0x000A9304 File Offset: 0x000A7504
            public override void OnDisable()
            {
                EditorApplication.projectChanged -= this.ReloadData;
                EditorApplication.editorApplicationQuit = (UnityAction)Delegate.Remove(EditorApplication.editorApplicationQuit, new UnityAction(this.OnQuit));
                EditorApplication.searchChanged = (EditorApplication.CallbackFunction)Delegate.Remove(EditorApplication.searchChanged, new EditorApplication.CallbackFunction(this.SearchChanged));
                EditorApplication.projectWasLoaded = (UnityAction)Delegate.Remove(EditorApplication.projectWasLoaded, new UnityAction(this.OnProjectWasLoaded));
                EditorSceneManager.newSceneCreated -= this.OnSceneCreated;
                EditorSceneManager.sceneOpened -= this.OnSceneOpened;
                SceneHierarchyWindow.s_SceneHierarchyWindow.Remove(this);
            }

            // Token: 0x06002C6F RID: 11375 RVA: 0x000A93B1 File Offset: 0x000A75B1
            private void OnQuit()
            {
                this.m_ExpandedScenes = this.GetExpandedSceneNames().ToList<string>();
            }

            // Token: 0x06002C70 RID: 11376 RVA: 0x000A93C8 File Offset: 0x000A75C8
            public void OnDestroy()
            {
                if (SceneHierarchyWindow.s_LastInteractedHierarchy == this)
                {
                    SceneHierarchyWindow.s_LastInteractedHierarchy = null;
                    foreach (SceneHierarchyWindow x in SceneHierarchyWindow.s_SceneHierarchyWindow)
                    {
                        if (x != this)
                        {
                            SceneHierarchyWindow.s_LastInteractedHierarchy = x;
                        }
                    }
                }
            }

            // Token: 0x06002C71 RID: 11377 RVA: 0x000A9448 File Offset: 0x000A7648
            private void SetAsLastInteractedHierarchy()
            {
                SceneHierarchyWindow.s_LastInteractedHierarchy = this;
            }

            // Token: 0x06002C72 RID: 11378 RVA: 0x000A9454 File Offset: 0x000A7654
            private void SyncIfNeeded()
            {
                if (this.treeViewReloadNeeded)
                {
                    this.treeViewReloadNeeded = false;
                    this.ReloadData();
                }
                if (this.selectionSyncNeeded)
                {
                    this.selectionSyncNeeded = false;
                    bool flag = EditorApplication.timeSinceStartup - this.m_LastUserInteractionTime < 0.2;
                    bool flag2 = !this.m_LockTracker.isLocked || this.m_FrameOnSelectionSync || flag;
                    bool animatedFraming = flag && flag2;
                    this.m_FrameOnSelectionSync = false;
                    this.treeView.SetSelection(Selection.instanceIDs, flag2, animatedFraming);
                }
            }

            // Token: 0x06002C73 RID: 11379 RVA: 0x000A94EC File Offset: 0x000A76EC
            private void DetectUserInteraction()
            {
                Event current = Event.current;
                if (current.type != EventType.Layout && current.type != EventType.Repaint)
                {
                    this.m_LastUserInteractionTime = EditorApplication.timeSinceStartup;
                }
            }

            // Token: 0x06002C74 RID: 11380 RVA: 0x000A9528 File Offset: 0x000A7728
            private void OnGUI()
            {
                if (SceneHierarchyWindow.s_Styles == null)
                {
                    SceneHierarchyWindow.s_Styles = new SceneHierarchyWindow.Styles();
                }
                this.DetectUserInteraction();
                this.SyncIfNeeded();
                this.m_TreeViewKeyboardControlID = GUIUtility.GetControlID(FocusType.Keyboard);
                this.OnEvent();
                Rect rect = new Rect(0f, 0f, base.position.width, base.position.height);
                Event current = Event.current;
                if (current.type == EventType.MouseDown && rect.Contains(current.mousePosition))
                {
                    this.treeView.EndPing();
                    this.SetAsLastInteractedHierarchy();
                }
                this.DoToolbar();
                float searchPathHeight = this.DoSearchResultPathGUI();
                this.DoTreeView(searchPathHeight);
                this.ExecuteCommands();
            }

            // Token: 0x06002C75 RID: 11381 RVA: 0x000A95E7 File Offset: 0x000A77E7
            private void OnLostFocus()
            {
                this.treeView.EndNameEditing(true);
            }

            // Token: 0x06002C76 RID: 11382 RVA: 0x000A95F8 File Offset: 0x000A77F8
            public static bool IsSceneHeaderInHierarchyWindow(Scene scene)
            {
                return scene.IsValid();
            }

            // Token: 0x06002C77 RID: 11383 RVA: 0x000A9614 File Offset: 0x000A7814
            private void TreeViewItemDoubleClicked(int instanceID)
            {
                Scene sceneByHandle = EditorSceneManager.GetSceneByHandle(instanceID);
                if (SceneHierarchyWindow.IsSceneHeaderInHierarchyWindow(sceneByHandle))
                {
                    if (sceneByHandle.isLoaded)
                    {
                        SceneManager.SetActiveScene(sceneByHandle);
                    }
                }
                else
                {
                    SceneView.FrameLastActiveSceneView();
                }
            }

            // Token: 0x06002C78 RID: 11384 RVA: 0x000A9654 File Offset: 0x000A7854
            public void SetExpandedRecursive(int id, bool expand)
            {
                TreeViewItem treeViewItem = this.treeView.data.FindItem(id);
                if (treeViewItem == null)
                {
                    this.ReloadData();
                    treeViewItem = this.treeView.data.FindItem(id);
                }
                if (treeViewItem != null)
                {
                    this.treeView.data.SetExpandedWithChildren(treeViewItem, expand);
                }
            }

            // Token: 0x06002C79 RID: 11385 RVA: 0x000A96AC File Offset: 0x000A78AC
            private void OnGUIAssetCallback(int instanceID, Rect rect)
            {
                if (EditorApplication.hierarchyWindowItemOnGUI != null)
                {
                    EditorApplication.hierarchyWindowItemOnGUI(instanceID, rect);
                }
            }

            // Token: 0x06002C7A RID: 11386 RVA: 0x000A96C7 File Offset: 0x000A78C7
            private void OnDragEndedCallback(int[] draggedInstanceIds, bool draggedItemsFromOwnTreeView)
            {
                if (draggedInstanceIds != null && draggedItemsFromOwnTreeView)
                {
                    this.ReloadData();
                    this.treeView.SetSelection(draggedInstanceIds, true);
                    this.treeView.NotifyListenersThatSelectionChanged();
                    base.Repaint();
                    GUIUtility.ExitGUI();
                }
            }

            // Token: 0x06002C7B RID: 11387 RVA: 0x000A9701 File Offset: 0x000A7901
            public void ReloadData()
            {
                if (this.m_TreeView == null)
                {
                    this.Init();
                }
                else
                {
                    this.m_TreeView.ReloadData();
                }
            }

            // Token: 0x06002C7C RID: 11388 RVA: 0x000A9728 File Offset: 0x000A7928
            public void SearchChanged()
            {
                GameObjectTreeViewDataSource gameObjectTreeViewDataSource = (GameObjectTreeViewDataSource)this.treeView.data;
                if (gameObjectTreeViewDataSource.searchMode != (int)base.searchMode || !(gameObjectTreeViewDataSource.searchString == this.m_SearchFilter))
                {
                    gameObjectTreeViewDataSource.searchMode = (int)base.searchMode;
                    gameObjectTreeViewDataSource.searchString = this.m_SearchFilter;
                    if (this.m_SearchFilter == "")
                    {
                        this.treeView.Frame(Selection.activeInstanceID, true, false);
                    }
                    this.ReloadData();
                }
            }

            // Token: 0x06002C7D RID: 11389 RVA: 0x000A97BA File Offset: 0x000A79BA
            private void TreeViewSelectionChanged(int[] ids)
            {
                Selection.instanceIDs = ids;
                this.m_DidSelectSearchResult = !string.IsNullOrEmpty(this.m_SearchFilter);
            }

            // Token: 0x06002C7E RID: 11390 RVA: 0x000A97D8 File Offset: 0x000A79D8
            private bool IsTreeViewSelectionInSyncWithBackend()
            {
                return this.m_TreeView != null && this.m_TreeView.state.selectedIDs.SequenceEqual(Selection.instanceIDs);
            }

            // Token: 0x06002C7F RID: 11391 RVA: 0x000A9819 File Offset: 0x000A7A19
            private void OnSelectionChange()
            {
                if (!this.IsTreeViewSelectionInSyncWithBackend())
                {
                    this.selectionSyncNeeded = true;
                }
                else if (SceneHierarchyWindow.s_Debug)
                {
                    Debug.Log("OnSelectionChange: Selection is already in sync so no framing will happen");
                }
            }

            // Token: 0x06002C80 RID: 11392 RVA: 0x000A984B File Offset: 0x000A7A4B
            private void OnHierarchyChange()
            {
                if (this.m_TreeView != null)
                {
                    this.m_TreeView.EndNameEditing(false);
                }
                this.treeViewReloadNeeded = true;
            }

            // Token: 0x06002C81 RID: 11393 RVA: 0x000A986C File Offset: 0x000A7A6C
            private float DoSearchResultPathGUI()
            {
                float result;
                if (!base.hasSearchFilter)
                {
                    result = 0f;
                }
                else
                {
                    GUILayout.FlexibleSpace();
                    Rect rect = EditorGUILayout.BeginVertical(EditorStyles.inspectorBig, new GUILayoutOption[0]);
                    GUILayout.Label("Path:", new GUILayoutOption[0]);
                    if (this.m_TreeView.HasSelection())
                    {
                        int instanceID = this.m_TreeView.GetSelection()[0];
                        IHierarchyProperty hierarchyProperty = new HierarchyProperty(HierarchyType.GameObjects);
                        hierarchyProperty.Find(instanceID, null);
                        if (hierarchyProperty.isValid)
                        {
                            do
                            {
                                EditorGUILayout.BeginHorizontal(new GUILayoutOption[0]);
                                GUILayout.Label(hierarchyProperty.icon, new GUILayoutOption[0]);
                                GUILayout.Label(hierarchyProperty.name, new GUILayoutOption[0]);
                                GUILayout.FlexibleSpace();
                                EditorGUILayout.EndHorizontal();
                            }
                            while (hierarchyProperty.Parent());
                        }
                    }
                    EditorGUILayout.EndVertical();
                    GUILayout.Space(0f);
                    result = rect.height;
                }
                return result;
            }

            // Token: 0x06002C82 RID: 11394 RVA: 0x000A9955 File Offset: 0x000A7B55
            private void OnEvent()
            {
                this.treeView.OnEvent();
            }

            // Token: 0x06002C83 RID: 11395 RVA: 0x000A9964 File Offset: 0x000A7B64
            private void DoTreeView(float searchPathHeight)
            {
                Rect treeViewRect = this.treeViewRect;
                treeViewRect.height -= searchPathHeight;
                this.treeView.OnGUI(treeViewRect, this.m_TreeViewKeyboardControlID);
            }

            // Token: 0x06002C84 RID: 11396 RVA: 0x000A999C File Offset: 0x000A7B9C
            private void DoToolbar()
            {
                GUILayout.BeginHorizontal(EditorStyles.toolbar, new GUILayoutOption[0]);
                this.CreateGameObjectPopup();
                GUILayout.Space(6f);
                if (SceneHierarchyWindow.s_Debug)
                {
                    int num;
                    int num2;
                    this.m_TreeView.gui.GetFirstAndLastRowVisible(out num, out num2);
                    GUILayout.Label(string.Format("{0} ({1}, {2})", this.m_TreeView.data.rowCount, num, num2), EditorStyles.miniLabel, new GUILayoutOption[0]);
                    GUILayout.Space(6f);
                }
                GUILayout.FlexibleSpace();
                Event current = Event.current;
                if (base.hasSearchFilterFocus && current.type == EventType.KeyDown && (current.keyCode == KeyCode.DownArrow || current.keyCode == KeyCode.UpArrow))
                {
                    GUIUtility.keyboardControl = this.m_TreeViewKeyboardControlID;
                    if (this.treeView.IsLastClickedPartOfRows())
                    {
                        this.treeView.Frame(this.treeView.state.lastClickedID, true, false);
                        this.m_DidSelectSearchResult = !string.IsNullOrEmpty(this.m_SearchFilter);
                    }
                    else
                    {
                        this.treeView.OffsetSelection(1);
                    }
                    current.Use();
                }
                base.SearchFieldGUI();
                GUILayout.Space(6f);
                if (this.hasSortMethods)
                {
                    if (Application.isPlaying && ((GameObjectTreeViewDataSource)this.treeView.data).isFetchAIssue)
                    {
                        GUILayout.Toggle(false, SceneHierarchyWindow.s_Styles.fetchWarning, SceneHierarchyWindow.s_Styles.MiniButton, new GUILayoutOption[0]);
                    }
                    this.SortMethodsDropDown();
                }
                GUILayout.EndHorizontal();
            }

            // Token: 0x06002C85 RID: 11397 RVA: 0x000A9B48 File Offset: 0x000A7D48
            internal override void SetSearchFilter(string searchFilter, SearchableEditorWindow.SearchMode searchMode, bool setAll)
            {
                base.SetSearchFilter(searchFilter, searchMode, setAll);
                if (this.m_DidSelectSearchResult && string.IsNullOrEmpty(searchFilter))
                {
                    this.m_DidSelectSearchResult = false;
                    this.FrameObjectPrivate(Selection.activeInstanceID, true, false, false);
                    if (GUIUtility.keyboardControl == 0)
                    {
                        GUIUtility.keyboardControl = this.m_TreeViewKeyboardControlID;
                    }
                }
            }

            // Token: 0x06002C86 RID: 11398 RVA: 0x000A9BA4 File Offset: 0x000A7DA4
            private void AddCreateGameObjectItemsToMenu(GenericMenu menu, UnityEngine.Object[] context, bool includeCreateEmptyChild, bool includeGameObjectInPath, int targetSceneHandle)
            {
                string[] submenus = Unsupported.GetSubmenus("GameObject");
                foreach (string text in submenus)
                {
                    UnityEngine.Object[] temporaryContext = context;
                    if (includeCreateEmptyChild || !(text.ToLower() == "GameObject/Create Empty Child".ToLower()))
                    {
                        if (text.EndsWith("..."))
                        {
                            temporaryContext = null;
                        }
                        if (text.ToLower() == "GameObject/Center On Children".ToLower())
                        {
                            break;
                        }
                        string replacementMenuString = text;
                        if (!includeGameObjectInPath)
                        {
                            replacementMenuString = text.Substring(11);
                        }
                        MenuUtils.ExtractMenuItemWithPath(text, menu, replacementMenuString, temporaryContext, targetSceneHandle, new Action<string, UnityEngine.Object[], int>(this.BeforeCreateGameObjectMenuItemWasExecuted), new Action<string, UnityEngine.Object[], int>(this.AfterCreateGameObjectMenuItemWasExecuted));
                    }
                }
            }

            // Token: 0x06002C87 RID: 11399 RVA: 0x000A9C70 File Offset: 0x000A7E70
            private void BeforeCreateGameObjectMenuItemWasExecuted(string menuPath, UnityEngine.Object[] contextObjects, int userData)
            {
                EditorSceneManager.SetTargetSceneForNewGameObjects(userData);
            }

            // Token: 0x06002C88 RID: 11400 RVA: 0x000A9C86 File Offset: 0x000A7E86
            private void AfterCreateGameObjectMenuItemWasExecuted(string menuPath, UnityEngine.Object[] contextObjects, int userData)
            {
                EditorSceneManager.SetTargetSceneForNewGameObjects(0);
                if (this.m_LockTracker.isLocked)
                {
                    this.m_FrameOnSelectionSync = true;
                }
            }

            // Token: 0x06002C89 RID: 11401 RVA: 0x000A9CA8 File Offset: 0x000A7EA8
            private void CreateGameObjectPopup()
            {
                Rect rect = GUILayoutUtility.GetRect(SceneHierarchyWindow.s_Styles.createContent, EditorStyles.toolbarDropDown, null);
                if (Event.current.type == EventType.Repaint)
                {
                    EditorStyles.toolbarDropDown.Draw(rect, SceneHierarchyWindow.s_Styles.createContent, false, false, false, false);
                }
                if (Event.current.type == EventType.MouseDown && rect.Contains(Event.current.mousePosition))
                {
                    GUIUtility.hotControl = 0;
                    GenericMenu genericMenu = new GenericMenu();
                    this.AddCreateGameObjectItemsToMenu(genericMenu, null, true, false, 0);
                    genericMenu.DropDown(rect);
                    Event.current.Use();
                }
            }

            // Token: 0x06002C8A RID: 11402 RVA: 0x000A9D44 File Offset: 0x000A7F44
            private void SortMethodsDropDown()
            {
                if (this.hasSortMethods)
                {
                    GUIContent guicontent = this.m_SortingObjects[this.currentSortingName].content;
                    if (guicontent == null)
                    {
                        guicontent = SceneHierarchyWindow.s_Styles.defaultSortingContent;
                        guicontent.tooltip = this.currentSortingName;
                    }
                    Rect rect = GUILayoutUtility.GetRect(guicontent, EditorStyles.toolbarButton);
                    if (EditorGUI.DropdownButton(rect, guicontent, FocusType.Passive, EditorStyles.toolbarButton))
                    {
                        List<SceneHierarchySortingWindow.InputData> list = new List<SceneHierarchySortingWindow.InputData>();
                        foreach (KeyValuePair<string, HierarchySorting> keyValuePair in this.m_SortingObjects)
                        {
                            list.Add(new SceneHierarchySortingWindow.InputData
                            {
                                m_TypeName = keyValuePair.Key,
                                m_Name = ObjectNames.NicifyVariableName(keyValuePair.Key),
                                m_Selected = (keyValuePair.Key == this.m_CurrentSortingName)
                            });
                        }
                        if (SceneHierarchySortingWindow.ShowAtPosition(new Vector2(rect.x, rect.y + rect.height), list, new SceneHierarchySortingWindow.OnSelectCallback(this.SortFunctionCallback)))
                        {
                            GUIUtility.ExitGUI();
                        }
                    }
                }
            }

            // Token: 0x06002C8B RID: 11403 RVA: 0x000A9E8C File Offset: 0x000A808C
            private void SetUpSortMethodLists()
            {
                this.m_SortingObjects = new Dictionary<string, HierarchySorting>();
                TransformSorting transformSorting = new TransformSorting();
                this.m_SortingObjects.Add(this.GetNameForType(transformSorting.GetType()), transformSorting);
                if (this.m_AllowAlphaNumericalSort || !InternalEditorUtility.isHumanControllingUs)
                {
                    AlphabeticalSorting alphabeticalSorting = new AlphabeticalSorting();
                    this.m_SortingObjects.Add(this.GetNameForType(alphabeticalSorting.GetType()), alphabeticalSorting);
                }
                this.currentSortingName = this.m_CurrentSortingName;
            }

            // Token: 0x06002C8C RID: 11404 RVA: 0x000A9F04 File Offset: 0x000A8104
            private string GetNameForType(Type type)
            {
                return type.Name;
            }

            // Token: 0x06002C8D RID: 11405 RVA: 0x000A9F1F File Offset: 0x000A811F
            private void SortFunctionCallback(SceneHierarchySortingWindow.InputData data)
            {
                this.SetSortFunction(data.m_TypeName);
            }

            // Token: 0x06002C8E RID: 11406 RVA: 0x000A9F2E File Offset: 0x000A812E
            public void SetSortFunction(Type sortType)
            {
                this.SetSortFunction(this.GetNameForType(sortType));
            }

            // Token: 0x06002C8F RID: 11407 RVA: 0x000A9F40 File Offset: 0x000A8140
            private void SetSortFunction(string sortTypeName)
            {
                if (!this.m_SortingObjects.ContainsKey(sortTypeName))
                {
                    Debug.LogError("Invalid search type name: " + sortTypeName);
                }
                else
                {
                    this.currentSortingName = sortTypeName;
                    if (this.treeView.GetSelection().Any<int>())
                    {
                        this.treeView.Frame(this.treeView.GetSelection().First<int>(), true, false);
                    }
                    this.treeView.ReloadData();
                }
            }

            // Token: 0x06002C90 RID: 11408 RVA: 0x000A9FB9 File Offset: 0x000A81B9
            public void DirtySortingMethods()
            {
                this.m_AllowAlphaNumericalSort = EditorPrefs.GetBool("AllowAlphaNumericHierarchy", false);
                this.SetUpSortMethodLists();
                this.treeView.SetSelection(this.treeView.GetSelection(), true);
                this.treeView.ReloadData();
            }

            // Token: 0x06002C91 RID: 11409 RVA: 0x000A9FF8 File Offset: 0x000A81F8
            private void ExecuteCommands()
            {
                Event current = Event.current;
                if (current.type == EventType.ExecuteCommand || current.type == EventType.ValidateCommand)
                {
                    bool flag = current.type == EventType.ExecuteCommand;
                    if (current.commandName == "Delete" || current.commandName == "SoftDelete")
                    {
                        if (flag)
                        {
                            this.DeleteGO();
                        }
                        current.Use();
                        GUIUtility.ExitGUI();
                    }
                    else if (current.commandName == "Duplicate")
                    {
                        if (flag)
                        {
                            this.DuplicateGO();
                        }
                        current.Use();
                        GUIUtility.ExitGUI();
                    }
                    else if (current.commandName == "Copy")
                    {
                        if (flag)
                        {
                            this.CopyGO();
                        }
                        current.Use();
                        GUIUtility.ExitGUI();
                    }
                    else if (current.commandName == "Paste")
                    {
                        if (flag)
                        {
                            this.PasteGO();
                        }
                        current.Use();
                        GUIUtility.ExitGUI();
                    }
                    else if (current.commandName == "SelectAll")
                    {
                        if (flag)
                        {
                            this.SelectAll();
                        }
                        current.Use();
                        GUIUtility.ExitGUI();
                    }
                    else if (current.commandName == "FrameSelected")
                    {
                        if (current.type == EventType.ExecuteCommand)
                        {
                            this.FrameObjectPrivate(Selection.activeInstanceID, true, true, true);
                        }
                        current.Use();
                        GUIUtility.ExitGUI();
                    }
                    else if (current.commandName == "Find")
                    {
                        if (current.type == EventType.ExecuteCommand)
                        {
                            base.FocusSearchField();
                        }
                        current.Use();
                    }
                }
            }

            // Token: 0x06002C92 RID: 11410 RVA: 0x000AA1BC File Offset: 0x000A83BC
            private void CreateGameObjectContextClick(GenericMenu menu, int contextClickedItemID)
            {
                menu.AddItem(EditorGUIUtility.TrTextContent("Copy", null, null), false, new GenericMenu.MenuFunction(this.CopyGO));
                menu.AddItem(EditorGUIUtility.TrTextContent("Paste", null, null), false, new GenericMenu.MenuFunction(this.PasteGO));
                menu.AddSeparator("");
                if (!base.hasSearchFilter && this.m_TreeViewState.selectedIDs.Count == 1)
                {
                    menu.AddItem(EditorGUIUtility.TrTextContent("Rename", null, null), false, new GenericMenu.MenuFunction(this.RenameGO));
                }
                else
                {
                    menu.AddDisabledItem(EditorGUIUtility.TrTextContent("Rename", null, null));
                }
                menu.AddItem(EditorGUIUtility.TrTextContent("Duplicate", null, null), false, new GenericMenu.MenuFunction(this.DuplicateGO));
                menu.AddItem(EditorGUIUtility.TrTextContent("Delete", null, null), false, new GenericMenu.MenuFunction(this.DeleteGO));
                menu.AddSeparator("");
                bool flag = false;
                if (this.m_TreeViewState.selectedIDs.Count == 1)
                {
                    GameObjectTreeViewItem gameObjectTreeViewItem = this.treeView.FindItem(this.m_TreeViewState.selectedIDs[0]) as GameObjectTreeViewItem;
                    if (gameObjectTreeViewItem != null)
                    {
                        SceneHierarchyWindow.< CreateGameObjectContextClick > c__AnonStorey0 < CreateGameObjectContextClick > c__AnonStorey = new SceneHierarchyWindow.< CreateGameObjectContextClick > c__AnonStorey0();

                    < CreateGameObjectContextClick > c__AnonStorey.prefab = PrefabUtility.GetCorrespondingObjectFromSource(gameObjectTreeViewItem.objectPPTR);
                        if (< CreateGameObjectContextClick > c__AnonStorey.prefab != null)
                        {
                            menu.AddItem(EditorGUIUtility.TrTextContent("Select Prefab", null, null), false, new GenericMenu.MenuFunction(< CreateGameObjectContextClick > c__AnonStorey.m__0));
                            flag = true;
                        }
                    }
                }
                if (!flag)
                {
                    menu.AddDisabledItem(EditorGUIUtility.TrTextContent("Select Prefab", null, null));
                }
                menu.AddSeparator("");
                this.AddCreateGameObjectItemsToMenu(menu, Selection.transforms.Select(new Func<Transform, GameObject>(SceneHierarchyWindow.< CreateGameObjectContextClick > m__0)).ToArray<GameObject>(), false, false, 0);
                menu.ShowAsContext();
            }

            // Token: 0x06002C93 RID: 11411 RVA: 0x000AA3A4 File Offset: 0x000A85A4
            private void CreateMultiSceneHeaderContextClick(GenericMenu menu, int contextClickedItemID)
            {
                Scene sceneByHandle = EditorSceneManager.GetSceneByHandle(contextClickedItemID);
                if (!SceneHierarchyWindow.IsSceneHeaderInHierarchyWindow(sceneByHandle))
                {
                    Debug.LogError("Context clicked item is not a scene");
                }
                else
                {
                    bool flag = SceneManager.sceneCount > 1;
                    if (sceneByHandle.isLoaded)
                    {
                        GUIContent content = EditorGUIUtility.TrTextContent("Set Active Scene", null, null);
                        if (flag && SceneManager.GetActiveScene() != sceneByHandle)
                        {
                            menu.AddItem(content, false, new GenericMenu.MenuFunction2(this.SetSceneActive), contextClickedItemID);
                        }
                        else
                        {
                            menu.AddDisabledItem(content);
                        }
                        menu.AddSeparator("");
                    }
                    if (sceneByHandle.isLoaded)
                    {
                        if (!EditorApplication.isPlaying)
                        {
                            menu.AddItem(EditorGUIUtility.TrTextContent("Save Scene", null, null), false, new GenericMenu.MenuFunction2(this.SaveSelectedScenes), contextClickedItemID);
                            menu.AddItem(EditorGUIUtility.TrTextContent("Save Scene As", null, null), false, new GenericMenu.MenuFunction2(this.SaveSceneAs), contextClickedItemID);
                            if (flag)
                            {
                                menu.AddItem(EditorGUIUtility.TrTextContent("Save All", null, null), false, new GenericMenu.MenuFunction2(this.SaveAllScenes), contextClickedItemID);
                            }
                            else
                            {
                                menu.AddDisabledItem(EditorGUIUtility.TrTextContent("Save All", null, null));
                            }
                        }
                        else
                        {
                            menu.AddDisabledItem(EditorGUIUtility.TrTextContent("Save Scene", null, null));
                            menu.AddDisabledItem(EditorGUIUtility.TrTextContent("Save Scene As", null, null));
                            menu.AddDisabledItem(EditorGUIUtility.TrTextContent("Save All", null, null));
                        }
                        menu.AddSeparator("");
                    }
                    bool flag2 = EditorSceneManager.loadedSceneCount != this.GetNumLoadedScenesInSelection();
                    if (sceneByHandle.isLoaded)
                    {
                        GUIContent content2 = EditorGUIUtility.TrTextContent("Unload Scene", null, null);
                        bool flag3 = flag2 && !EditorApplication.isPlaying && !string.IsNullOrEmpty(sceneByHandle.path);
                        if (flag3)
                        {
                            menu.AddItem(content2, false, new GenericMenu.MenuFunction2(this.UnloadSelectedScenes), contextClickedItemID);
                        }
                        else
                        {
                            menu.AddDisabledItem(content2);
                        }
                    }
                    else
                    {
                        GUIContent content3 = EditorGUIUtility.TrTextContent("Load Scene", null, null);
                        bool flag4 = !EditorApplication.isPlaying;
                        if (flag4)
                        {
                            menu.AddItem(content3, false, new GenericMenu.MenuFunction2(this.LoadSelectedScenes), contextClickedItemID);
                        }
                        else
                        {
                            menu.AddDisabledItem(content3);
                        }
                    }
                    GUIContent content4 = EditorGUIUtility.TrTextContent("Remove Scene", null, null);
                    bool flag5 = this.GetSelectedScenes().Count == SceneManager.sceneCount;
                    bool flag6 = flag2 && !flag5 && !EditorApplication.isPlaying;
                    if (flag6)
                    {
                        menu.AddItem(content4, false, new GenericMenu.MenuFunction2(this.RemoveSelectedScenes), contextClickedItemID);
                    }
                    else
                    {
                        menu.AddDisabledItem(content4);
                    }
                    if (sceneByHandle.isLoaded)
                    {
                        GUIContent content5 = EditorGUIUtility.TrTextContent("Discard changes", null, null);
                        List<int> selectedScenes = this.GetSelectedScenes();
                        Scene[] modifiedScenes = this.GetModifiedScenes(selectedScenes);
                        bool flag7 = !EditorApplication.isPlaying && modifiedScenes.Length > 0;
                        if (flag7)
                        {
                            menu.AddItem(content5, false, new GenericMenu.MenuFunction2(this.DiscardChangesInSelectedScenes), contextClickedItemID);
                        }
                        else
                        {
                            menu.AddDisabledItem(content5);
                        }
                    }
                    menu.AddSeparator("");
                    GUIContent content6 = EditorGUIUtility.TrTextContent("Select Scene Asset", null, null);
                    if (!string.IsNullOrEmpty(sceneByHandle.path))
                    {
                        menu.AddItem(content6, false, new GenericMenu.MenuFunction2(this.SelectSceneAsset), contextClickedItemID);
                    }
                    else
                    {
                        menu.AddDisabledItem(content6);
                    }
                    GUIContent content7 = EditorGUIUtility.TrTextContent("Add New Scene", null, null);
                    if (!EditorApplication.isPlaying)
                    {
                        menu.AddItem(content7, false, new GenericMenu.MenuFunction2(this.AddNewScene), contextClickedItemID);
                    }
                    else
                    {
                        menu.AddDisabledItem(content7);
                    }
                    if (sceneByHandle.isLoaded)
                    {
                        menu.AddSeparator("");
                        this.AddCreateGameObjectItemsToMenu(menu, Selection.transforms.Select(new Func<Transform, GameObject>(SceneHierarchyWindow.< CreateMultiSceneHeaderContextClick > m__1)).ToArray<GameObject>(), false, true, sceneByHandle.handle);
                    }
                }
            }

            // Token: 0x06002C94 RID: 11412 RVA: 0x000AA7C0 File Offset: 0x000A89C0
            private int GetNumLoadedScenesInSelection()
            {
                int num = 0;
                foreach (int handle in this.GetSelectedScenes())
                {
                    if (EditorSceneManager.GetSceneByHandle(handle).isLoaded)
                    {
                        num++;
                    }
                }
                return num;
            }

            // Token: 0x06002C95 RID: 11413 RVA: 0x000AA83C File Offset: 0x000A8A3C
            private List<int> GetSelectedScenes()
            {
                List<int> list = new List<int>();
                int[] selection = this.m_TreeView.GetSelection();
                foreach (int num in selection)
                {
                    if (SceneHierarchyWindow.IsSceneHeaderInHierarchyWindow(EditorSceneManager.GetSceneByHandle(num)))
                    {
                        list.Add(num);
                    }
                }
                return list;
            }

            // Token: 0x06002C96 RID: 11414 RVA: 0x000AA8A4 File Offset: 0x000A8AA4
            private void ContextClickOutsideItems()
            {
                Event current = Event.current;
                current.Use();
                GenericMenu genericMenu = new GenericMenu();
                this.CreateGameObjectContextClick(genericMenu, 0);
                genericMenu.ShowAsContext();
            }

            // Token: 0x06002C97 RID: 11415 RVA: 0x000AA8D4 File Offset: 0x000A8AD4
            private void ItemContextClick(int contextClickedItemID)
            {
                Event current = Event.current;
                current.Use();
                GenericMenu genericMenu = new GenericMenu();
                bool flag = SceneHierarchyWindow.IsSceneHeaderInHierarchyWindow(EditorSceneManager.GetSceneByHandle(contextClickedItemID));
                if (flag)
                {
                    this.CreateMultiSceneHeaderContextClick(genericMenu, contextClickedItemID);
                }
                else
                {
                    this.CreateGameObjectContextClick(genericMenu, contextClickedItemID);
                }
                genericMenu.ShowAsContext();
            }

            // Token: 0x06002C98 RID: 11416 RVA: 0x000AA921 File Offset: 0x000A8B21
            private void CopyGO()
            {
                Unsupported.CopyGameObjectsToPasteboard();
            }

            // Token: 0x06002C99 RID: 11417 RVA: 0x000AA929 File Offset: 0x000A8B29
            private void PasteGO()
            {
                Unsupported.PasteGameObjectsFromPasteboard();
            }

            // Token: 0x06002C9A RID: 11418 RVA: 0x000AA931 File Offset: 0x000A8B31
            private void DuplicateGO()
            {
                Unsupported.DuplicateGameObjectsUsingPasteboard();
            }

            // Token: 0x06002C9B RID: 11419 RVA: 0x000AA939 File Offset: 0x000A8B39
            private void RenameGO()
            {
                this.treeView.BeginNameEditing(0f);
            }

            // Token: 0x06002C9C RID: 11420 RVA: 0x000AA94D File Offset: 0x000A8B4D
            private void DeleteGO()
            {
                Unsupported.DeleteGameObjectSelection();
            }

            // Token: 0x06002C9D RID: 11421 RVA: 0x000AA958 File Offset: 0x000A8B58
            private void SetSceneActive(object userData)
            {
                int handle = (int)userData;
                SceneManager.SetActiveScene(EditorSceneManager.GetSceneByHandle(handle));
            }

            // Token: 0x06002C9E RID: 11422 RVA: 0x000AA97C File Offset: 0x000A8B7C
            private void LoadSelectedScenes(object userdata)
            {
                List<int> selectedScenes = this.GetSelectedScenes();
                foreach (int handle in selectedScenes)
                {
                    Scene sceneByHandle = EditorSceneManager.GetSceneByHandle(handle);
                    if (!sceneByHandle.isLoaded)
                    {
                        EditorSceneManager.OpenScene(sceneByHandle.path, OpenSceneMode.Additive);
                    }
                }
                EditorApplication.RequestRepaintAllViews();
            }

            // Token: 0x06002C9F RID: 11423 RVA: 0x000AAA00 File Offset: 0x000A8C00
            private void SaveSceneAs(object userdata)
            {
                int handle = (int)userdata;
                Scene sceneByHandle = EditorSceneManager.GetSceneByHandle(handle);
                if (sceneByHandle.isLoaded)
                {
                    EditorSceneManager.SaveSceneAs(sceneByHandle);
                }
            }

            // Token: 0x06002CA0 RID: 11424 RVA: 0x000AAA31 File Offset: 0x000A8C31
            private void SaveAllScenes(object userdata)
            {
                EditorSceneManager.SaveOpenScenes();
            }

            // Token: 0x06002CA1 RID: 11425 RVA: 0x000AAA3C File Offset: 0x000A8C3C
            private void SaveSelectedScenes(object userdata)
            {
                List<int> selectedScenes = this.GetSelectedScenes();
                foreach (int handle in selectedScenes)
                {
                    Scene sceneByHandle = EditorSceneManager.GetSceneByHandle(handle);
                    if (sceneByHandle.isLoaded)
                    {
                        EditorSceneManager.SaveScene(sceneByHandle);
                    }
                }
            }

            // Token: 0x06002CA2 RID: 11426 RVA: 0x000AAAB4 File Offset: 0x000A8CB4
            private void UnloadSelectedScenes(object userdata)
            {
                this.CloseSelectedScenes(false);
            }

            // Token: 0x06002CA3 RID: 11427 RVA: 0x000AAABE File Offset: 0x000A8CBE
            private void RemoveSelectedScenes(object userData)
            {
                this.CloseSelectedScenes(true);
            }

            // Token: 0x06002CA4 RID: 11428 RVA: 0x000AAAC8 File Offset: 0x000A8CC8
            private bool UserAllowedDiscardingChanges(Scene[] modifiedScenes)
            {
                string title = "Discard Changes";
                string text = "Are you sure you want to discard the changes in the following scenes:\n\n   {0}\n\nYour changes will be lost.";
                string arg = string.Join("\n   ", modifiedScenes.Select(new Func<Scene, string>(SceneHierarchyWindow.< UserAllowedDiscardingChanges > m__2)).ToArray<string>());
                text = string.Format(text, arg);
                return EditorUtility.DisplayDialog(title, text, "OK", "Cancel");
            }

            // Token: 0x06002CA5 RID: 11429 RVA: 0x000AAB38 File Offset: 0x000A8D38
            private void DiscardChangesInSelectedScenes(object userData)
            {
                IEnumerable<string> expandedSceneNames = this.GetExpandedSceneNames();
                List<int> selectedScenes = this.GetSelectedScenes();
                Scene[] modifiedScenes = this.GetModifiedScenes(selectedScenes);
                Scene[] array = modifiedScenes.Where(new Func<Scene, bool>(SceneHierarchyWindow.< DiscardChangesInSelectedScenes > m__3)).ToArray<Scene>();
                if (this.UserAllowedDiscardingChanges(array))
                {
                    if (array.Length != modifiedScenes.Length)
                    {
                        Debug.LogWarning("Discarding changes in a scene that have not yet been saved is not supported. Save the scene first or create a new scene.");
                    }
                    foreach (Scene scene in array)
                    {
                        EditorSceneManager.ReloadScene(scene);
                    }
                    if (SceneManager.sceneCount == 1)
                    {
                        this.SetScenesExpanded(expandedSceneNames.ToList<string>());
                    }
                    EditorApplication.RequestRepaintAllViews();
                }
            }

            // Token: 0x06002CA6 RID: 11430 RVA: 0x000AAC00 File Offset: 0x000A8E00
            private Scene[] GetModifiedScenes(List<int> handles)
            {
                return handles.Select(new Func<int, Scene>(SceneHierarchyWindow.< GetModifiedScenes > m__4)).Where(new Func<Scene, bool>(SceneHierarchyWindow.< GetModifiedScenes > m__5)).ToArray<Scene>();
            }

            // Token: 0x06002CA7 RID: 11431 RVA: 0x000AAC60 File Offset: 0x000A8E60
            private void CloseSelectedScenes(bool removeScenes)
            {
                List<int> selectedScenes = this.GetSelectedScenes();
                Scene[] modifiedScenes = this.GetModifiedScenes(selectedScenes);
                bool flag = !EditorSceneManager.SaveModifiedScenesIfUserWantsTo(modifiedScenes);
                if (!flag)
                {
                    foreach (int handle in selectedScenes)
                    {
                        EditorSceneManager.CloseScene(EditorSceneManager.GetSceneByHandle(handle), removeScenes);
                    }
                    EditorApplication.RequestRepaintAllViews();
                }
            }

            // Token: 0x06002CA8 RID: 11432 RVA: 0x000AACEC File Offset: 0x000A8EEC
            private void AddNewScene(object userData)
            {
                Scene sceneByPath = SceneManager.GetSceneByPath("");
                if (sceneByPath.IsValid())
                {
                    string text = EditorGUIUtility.TrTextContent("Save Untitled Scene", null, null).text;
                    string text2 = EditorGUIUtility.TrTextContent("Existing Untitled scene needs to be saved before creating a new scene. Only one untitled scene is supported at a time.", null, null).text;
                    if (!EditorUtility.DisplayDialog(text, text2, "Save", "Cancel"))
                    {
                        return;
                    }
                    if (!EditorSceneManager.SaveScene(sceneByPath))
                    {
                        return;
                    }
                }
                Scene src = EditorSceneManager.NewScene(NewSceneSetup.DefaultGameObjects, NewSceneMode.Additive);
                int handle = (int)userData;
                Scene sceneByHandle = EditorSceneManager.GetSceneByHandle(handle);
                if (sceneByHandle.IsValid())
                {
                    EditorSceneManager.MoveSceneAfter(src, sceneByHandle);
                }
            }

            // Token: 0x06002CA9 RID: 11433 RVA: 0x000AAD98 File Offset: 0x000A8F98
            private void SelectSceneAsset(object userData)
            {
                int handle = (int)userData;
                UnityEngine.Object @object = AssetDatabase.LoadMainAssetAtPath(EditorSceneManager.GetSceneByHandle(handle).path);
                Selection.activeObject = @object;
                EditorGUIUtility.PingObject(@object);
            }

            // Token: 0x06002CAA RID: 11434 RVA: 0x000AADD0 File Offset: 0x000A8FD0
            private void SelectAll()
            {
                int[] rowIDs = this.treeView.GetRowIDs();
                this.treeView.SetSelection(rowIDs, false);
                this.TreeViewSelectionChanged(rowIDs);
            }

            // Token: 0x06002CAB RID: 11435 RVA: 0x000AADFE File Offset: 0x000A8FFE
            private static void ToggleDebugMode()
            {
                SceneHierarchyWindow.s_Debug = !SceneHierarchyWindow.s_Debug;
            }

            // Token: 0x06002CAC RID: 11436 RVA: 0x000AAE10 File Offset: 0x000A9010
            public virtual void AddItemsToMenu(GenericMenu menu)
            {
                this.m_LockTracker.AddItemsToMenu(menu, false);
                if (Unsupported.IsDeveloperMode())
                {
                    GUIContent content = EditorGUIUtility.TrTextContent("DEVELOPER/Toggle DebugMode", null, null);
                    bool on = false;
                    if (SceneHierarchyWindow.func_6 == null)
				{
                        SceneHierarchyWindow.func_6 = new GenericMenu.MenuFunction(SceneHierarchyWindow.ToggleDebugMode);
                    }
                    menu.AddItem(content, on, SceneHierarchyWindow.func_6);
                }
            }

            // Token: 0x06002CAD RID: 11437 RVA: 0x000AAE67 File Offset: 0x000A9067
            public void FrameObject(int instanceID, bool ping)
            {
                this.FrameObjectPrivate(instanceID, true, ping, true);
            }

            // Token: 0x06002CAE RID: 11438 RVA: 0x000AAE74 File Offset: 0x000A9074
            private void FrameObjectPrivate(int instanceID, bool frame, bool ping, bool animatedFraming)
            {
                if (instanceID != 0)
                {
                    if (this.m_LastFramedID != instanceID)
                    {
                        this.treeView.EndPing();
                    }
                    this.SetSearchFilter("", SearchableEditorWindow.SearchMode.All, true);
                    this.m_LastFramedID = instanceID;
                    this.treeView.Frame(instanceID, frame, ping, animatedFraming);
                    this.FrameObjectPrivate(InternalEditorUtility.GetGameObjectInstanceIDFromComponent(instanceID), frame, ping, animatedFraming);
                }
            }

            // Token: 0x06002CAF RID: 11439 RVA: 0x000AAED8 File Offset: 0x000A90D8
            protected virtual void ShowButton(Rect r)
            {
                if (SceneHierarchyWindow.s_Styles == null)
                {
                    SceneHierarchyWindow.s_Styles = new SceneHierarchyWindow.Styles();
                }
                this.m_LockTracker.ShowButton(r, SceneHierarchyWindow.s_Styles.lockButton, false);
            }

            // Token: 0x06002CB0 RID: 11440 RVA: 0x000AAF06 File Offset: 0x000A9106
            static SceneHierarchyWindow()
            {
                // Note: this type is marked as 'beforefieldinit'.
            }

            // Token: 0x06002CB1 RID: 11441 RVA: 0x000AAF14 File Offset: 0x000A9114
            [CompilerGenerated]
            private static GameObject CreateGameObjectContextClick(Transform t)
            {
                return t.gameObject;
            }

            // Token: 0x06002CB2 RID: 11442 RVA: 0x000AAF30 File Offset: 0x000A9130
            [CompilerGenerated]
            private static GameObject CreateMultiSceneHeaderContextClick1(Transform t)
            {
                return t.gameObject;
            }

            // Token: 0x06002CB3 RID: 11443 RVA: 0x000AAF4C File Offset: 0x000A914C
            [CompilerGenerated]
            private static string UserAllowedDiscardingChanges2(Scene scene)
            {
                return scene.name;
            }

            // Token: 0x06002CB4 RID: 11444 RVA: 0x000AAF68 File Offset: 0x000A9168
            [CompilerGenerated]
            private static bool DiscardChangesInSelectedScenes3(Scene scene)
            {
                return !string.IsNullOrEmpty(scene.path);
            }

            // Token: 0x06002CB5 RID: 11445 RVA: 0x000AAF8C File Offset: 0x000A918C
            [CompilerGenerated]
            private static Scene GetModifiedScenes4(int handle)
            {
                return EditorSceneManager.GetSceneByHandle(handle);
            }

            // Token: 0x06002CB6 RID: 11446 RVA: 0x000AAFA8 File Offset: 0x000A91A8
            [CompilerGenerated]
            private static bool GetModifiedScenes5(Scene scene)
            {
                return scene.isDirty;
            }

            // Token: 0x0400154B RID: 5451
            private static SceneHierarchyWindow s_LastInteractedHierarchy;

            // Token: 0x0400154C RID: 5452
            private static SceneHierarchyWindow.Styles s_Styles;

            // Token: 0x0400154D RID: 5453
            private static List<SceneHierarchyWindow> s_SceneHierarchyWindow = new List<SceneHierarchyWindow>();

            // Token: 0x0400154E RID: 5454
            private const int kInvalidSceneHandle = 0;

            // Token: 0x0400154F RID: 5455
            private TreeViewController m_TreeView;

            // Token: 0x04001550 RID: 5456
            [SerializeField]
            private TreeViewState m_TreeViewState;

            // Token: 0x04001551 RID: 5457
            [SerializeField]
            private List<string> m_ExpandedScenes = new List<string>();

            // Token: 0x04001552 RID: 5458
            private int m_TreeViewKeyboardControlID;

            // Token: 0x04001553 RID: 5459
            [SerializeField]
            private int m_CurrenRootInstanceID = 0;

            // Token: 0x04001554 RID: 5460
            [SerializeField]
            private EditorGUIUtility.EditorLockTracker m_LockTracker = new EditorGUIUtility.EditorLockTracker();

            // Token: 0x04001555 RID: 5461
            [SerializeField]
            private string m_CurrentSortingName = "";

            // Token: 0x04001556 RID: 5462
            [NonSerialized]
            private int m_LastFramedID = -1;

            // Token: 0x04001557 RID: 5463
            [NonSerialized]
            private bool m_TreeViewReloadNeeded;

            // Token: 0x04001558 RID: 5464
            [NonSerialized]
            private bool m_SelectionSyncNeeded;

            // Token: 0x04001559 RID: 5465
            [NonSerialized]
            private bool m_FrameOnSelectionSync;

            // Token: 0x0400155A RID: 5466
            [NonSerialized]
            private bool m_DidSelectSearchResult;

            // Token: 0x0400155B RID: 5467
            private Dictionary<string, HierarchySorting> m_SortingObjects = null;

            // Token: 0x0400155C RID: 5468
            private bool m_AllowAlphaNumericalSort;

            // Token: 0x0400155D RID: 5469
            [NonSerialized]
            private double m_LastUserInteractionTime;

            // Token: 0x0400155E RID: 5470
            private bool m_Debug;

            // Token: 0x0400155F RID: 5471
            [CompilerGenerated]
            private static Func<Transform, GameObject> func_0;

		// Token: 0x04001560 RID: 5472

            private static Func<Transform, GameObject> func_1;

		// Token: 0x04001561 RID: 5473

            private static Func<Scene, string> func_2;

		// Token: 0x04001562 RID: 5474

            private static Func<Scene, bool> func_3;

		// Token: 0x04001563 RID: 5475

            private static Func<int, Scene> func_4;

		// Token: 0x04001564 RID: 5476

            private static Func<Scene, bool> func_5;

		// Token: 0x04001565 RID: 5477

            private static GenericMenu.MenuFunction func_6;

		// Token: 0x020004C4 RID: 1220
		private class Styles
            {
                // Token: 0x06002CB7 RID: 11447 RVA: 0x000AAFC4 File Offset: 0x000A91C4
                public Styles()
                {
                    this.MiniButton = "ToolbarButton";
                }

                // Token: 0x04001566 RID: 5478
                private const string kCustomSorting = "CustomSorting";

                // Token: 0x04001567 RID: 5479
                private const string kWarningSymbol = "console.warnicon.sml";

                // Token: 0x04001568 RID: 5480
                private const string kWarningMessage = "The current sorting method is taking a lot of time. Consider using 'Transform Sort' in playmode for better performance.";

                // Token: 0x04001569 RID: 5481
                public GUIContent defaultSortingContent = new GUIContent(EditorGUIUtility.FindTexture("CustomSorting"));

                // Token: 0x0400156A RID: 5482
                public GUIContent createContent = EditorGUIUtility.TrTextContent("Create", null, null);

                // Token: 0x0400156B RID: 5483
                public GUIContent fetchWarning = new GUIContent("", EditorGUIUtility.FindTexture("console.warnicon.sml"), "The current sorting method is taking a lot of time. Consider using 'Transform Sort' in playmode for better performance.");

                // Token: 0x0400156C RID: 5484
                public GUIStyle MiniButton;

                // Token: 0x0400156D RID: 5485
                public GUIStyle lockButton = "IN LockButton";
            }

            // Token: 0x020004C5 RID: 1221
            [CompilerGenerated]
            private sealed class CreateGameObjectContextClick
            {
                // Token: 0x06002CB8 RID: 11448 RVA: 0x00002D4C File Offset: 0x00000F4C
                public CreateGameObjectContextClick()
                {
                }

                // Token: 0x06002CB9 RID: 11449 RVA: 0x000AB03E File Offset: 0x000A923E
                internal void m__0()
                {
                    Selection.activeObject = this.prefab;
                    EditorGUIUtility.PingObject(this.prefab.GetInstanceID());
                }

                // Token: 0x0400156E RID: 5486
                internal UnityEngine.Object prefab;
            }
        }
    }



    internal class TreeViewController
    {
        // Token: 0x06004CCB RID: 19659 RVA: 0x0015297C File Offset: 0x00150B7C
        public TreeViewController(EditorWindow editorWindow, TreeViewState treeViewState)
        {
            this.m_GUIView = ((!editorWindow) ? GUIView.current : editorWindow.m_Parent);
            this.state = treeViewState;
        }

        // Token: 0x17000E18 RID: 3608
        // (get) Token: 0x06004CCC RID: 19660 RVA: 0x001529F4 File Offset: 0x00150BF4
        // (set) Token: 0x06004CCD RID: 19661 RVA: 0x00152A0E File Offset: 0x00150C0E
        public Action<int[]> selectionChangedCallback { get; set; }

        // Token: 0x17000E19 RID: 3609
        // (get) Token: 0x06004CCE RID: 19662 RVA: 0x00152A18 File Offset: 0x00150C18
        // (set) Token: 0x06004CCF RID: 19663 RVA: 0x00152A32 File Offset: 0x00150C32
        public Action<int> itemSingleClickedCallback { get; set; }

        // Token: 0x17000E1A RID: 3610
        // (get) Token: 0x06004CD0 RID: 19664 RVA: 0x00152A3C File Offset: 0x00150C3C
        // (set) Token: 0x06004CD1 RID: 19665 RVA: 0x00152A56 File Offset: 0x00150C56
        public Action<int> itemDoubleClickedCallback { get; set; }
        // Token: 0x17000E1B RID: 3611
        // (get) Token: 0x06004CD2 RID: 19666 RVA: 0x00152A60 File Offset: 0x00150C60
        // (set) Token: 0x06004CD3 RID: 19667 RVA: 0x00152A7A File Offset: 0x00150C7A
        public Action<int[], bool> dragEndedCallback { get; set; }

        // Token: 0x17000E1C RID: 3612
        // (get) Token: 0x06004CD4 RID: 19668 RVA: 0x00152A84 File Offset: 0x00150C84
        // (set) Token: 0x06004CD5 RID: 19669 RVA: 0x00152A9E File Offset: 0x00150C9E
        public Action<int> contextClickItemCallback { get; set; }

        // Token: 0x17000E1D RID: 3613
        // (get) Token: 0x06004CD6 RID: 19670 RVA: 0x00152AA8 File Offset: 0x00150CA8
        // (set) Token: 0x06004CD7 RID: 19671 RVA: 0x00152AC2 File Offset: 0x00150CC2
        public Action contextClickOutsideItemsCallback { get; set; }

        // Token: 0x17000E1E RID: 3614
        // (get) Token: 0x06004CD8 RID: 19672 RVA: 0x00152ACC File Offset: 0x00150CCC
        // (set) Token: 0x06004CD9 RID: 19673 RVA: 0x00152AE6 File Offset: 0x00150CE6
        public Action keyboardInputCallback { get; set; }

        // Token: 0x17000E1F RID: 3615
        // (get) Token: 0x06004CDA RID: 19674 RVA: 0x00152AF0 File Offset: 0x00150CF0
        // (set) Token: 0x06004CDB RID: 19675 RVA: 0x00152B0A File Offset: 0x00150D0A
        public Action expandedStateChanged { get; set; }
        // Token: 0x17000E20 RID: 3616
        // (get) Token: 0x06004CDC RID: 19676 RVA: 0x00152B14 File Offset: 0x00150D14
        // (set) Token: 0x06004CDD RID: 19677 RVA: 0x00152B2E File Offset: 0x00150D2E
        public Action<string> searchChanged { get; set; }

        // Token: 0x17000E21 RID: 3617
        // (get) Token: 0x06004CDE RID: 19678 RVA: 0x00152B38 File Offset: 0x00150D38
        // (set) Token: 0x06004CDF RID: 19679 RVA: 0x00152B52 File Offset: 0x00150D52
        public Action<Vector2> scrollChanged { get; set; }

        // Token: 0x17000E22 RID: 3618
        // (get) Token: 0x06004CE0 RID: 19680 RVA: 0x00152B5C File Offset: 0x00150D5C
        // (set) Token: 0x06004CE1 RID: 19681 RVA: 0x00152B76 File Offset: 0x00150D76
        public Action<int, Rect> onGUIRowCallback { get; set; }

        // Token: 0x17000E23 RID: 3619
        // (get) Token: 0x06004CE2 RID: 19682 RVA: 0x00152B80 File Offset: 0x00150D80
        // (set) Token: 0x06004CE3 RID: 19683 RVA: 0x00152B9A File Offset: 0x00150D9A
        public ITreeViewDataSource data{get; set;}

        // Token: 0x17000E24 RID: 3620
        // (get) Token: 0x06004CE4 RID: 19684 RVA: 0x00152BA4 File Offset: 0x00150DA4
        // (set) Token: 0x06004CE5 RID: 19685 RVA: 0x00152BBE File Offset: 0x00150DBE
        public ITreeViewDragging dragging { get; set; }

        // Token: 0x17000E25 RID: 3621
        // (get) Token: 0x06004CE6 RID: 19686 RVA: 0x00152BC8 File Offset: 0x00150DC8
        // (set) Token: 0x06004CE7 RID: 19687 RVA: 0x00152BE2 File Offset: 0x00150DE2
        public ITreeViewGUI gui { get; set; }

        // Token: 0x17000E26 RID: 3622
        // (get) Token: 0x06004CE8 RID: 19688 RVA: 0x00152BEC File Offset: 0x00150DEC
        // (set) Token: 0x06004CE9 RID: 19689 RVA: 0x00152C06 File Offset: 0x00150E06
        public TreeViewState state { get; set; }

        // Token: 0x17000E27 RID: 3623
        // (get) Token: 0x06004CEA RID: 19690 RVA: 0x00152C10 File Offset: 0x00150E10
        // (set) Token: 0x06004CEB RID: 19691 RVA: 0x00152C2A File Offset: 0x00150E2A
        public GUIStyle horizontalScrollbarStyle { get; set; }

        // Token: 0x17000E28 RID: 3624
        // (get) Token: 0x06004CEC RID: 19692 RVA: 0x00152C34 File Offset: 0x00150E34
        // (set) Token: 0x06004CED RID: 19693 RVA: 0x00152C4E File Offset: 0x00150E4E
        public GUIStyle verticalScrollbarStyle { get; set; }

        // Token: 0x17000E29 RID: 3625
        // (get) Token: 0x06004CEE RID: 19694 RVA: 0x00152C58 File Offset: 0x00150E58
        public TreeViewItemExpansionAnimator expansionAnimator
        {
            get
            {
                return this.m_ExpansionAnimator;
            }
        }

        // Token: 0x17000E2A RID: 3626
        // (get) Token: 0x06004CEF RID: 19695 RVA: 0x00152C74 File Offset: 0x00150E74
        // (set) Token: 0x06004CF0 RID: 19696 RVA: 0x00152C8E File Offset: 0x00150E8E
        public bool deselectOnUnhandledMouseDown { get; set; }
        // Token: 0x17000E2B RID: 3627
        // (get) Token: 0x06004CF1 RID: 19697 RVA: 0x00152C98 File Offset: 0x00150E98
        // (set) Token: 0x06004CF2 RID: 19698 RVA: 0x00152CB3 File Offset: 0x00150EB3
        public bool useExpansionAnimation
        {
            get
            {
                return this.m_UseExpansionAnimation;
            }
            set
            {
                this.m_UseExpansionAnimation = value;
            }
        }

        // Token: 0x06004CF3 RID: 19699 RVA: 0x00152CC0 File Offset: 0x00150EC0
        public void Init(Rect rect, ITreeViewDataSource data, ITreeViewGUI gui, ITreeViewDragging dragging)
        {
            this.data = data;
            this.gui = gui;
            this.dragging = dragging;
            this.m_TotalRect = rect;
            this.m_VisibleRect = rect;
            data.OnInitialize();
            gui.OnInitialize();
            if (dragging != null)
            {
                dragging.OnInitialize();
            }
            this.expandedStateChanged = (Action)Delegate.Combine(this.expandedStateChanged, new Action(this.ExpandedStateHasChanged));
            this.m_FramingAnimFloat = new AnimFloat(this.state.scrollPos.y, new UnityAction(this.AnimatedScrollChanged));
        }

        // Token: 0x06004CF4 RID: 19700 RVA: 0x00152D57 File Offset: 0x00150F57
        private void ExpandedStateHasChanged()
        {
            this.m_StopIteratingItems = true;
        }

        // Token: 0x17000E2C RID: 3628
        // (get) Token: 0x06004CF5 RID: 19701 RVA: 0x00152D64 File Offset: 0x00150F64
        public bool isSearching
        {
            get
            {
                return !string.IsNullOrEmpty(this.state.searchString);
            }
        }

        // Token: 0x17000E2D RID: 3629
        // (get) Token: 0x06004CF6 RID: 19702 RVA: 0x00152D8C File Offset: 0x00150F8C
        public bool isDragging
        {
            get
            {
                return this.m_DragSelection != null && this.m_DragSelection.Count > 0;
            }
        }

        // Token: 0x17000E2E RID: 3630
        // (get) Token: 0x06004CF7 RID: 19703 RVA: 0x00152DC0 File Offset: 0x00150FC0
        public bool showingVerticalScrollBar
        {
            get
            {
                return this.m_ContentRect.height > this.m_VisibleRect.height;
            }
        }

        // Token: 0x17000E2F RID: 3631
        // (get) Token: 0x06004CF8 RID: 19704 RVA: 0x00152DF0 File Offset: 0x00150FF0
        public bool showingHorizontalScrollBar
        {
            get
            {
                return this.m_ContentRect.width > this.m_VisibleRect.width;
            }
        }

        // Token: 0x17000E30 RID: 3632
        // (get) Token: 0x06004CF9 RID: 19705 RVA: 0x00152E20 File Offset: 0x00151020
        // (set) Token: 0x06004CFA RID: 19706 RVA: 0x00152E40 File Offset: 0x00151040
        public string searchString
        {
            get
            {
                return this.state.searchString;
            }
            set
            {
                if (!object.ReferenceEquals(this.state.searchString, value))
                {
                    if (!(this.state.searchString == value))
                    {
                        this.state.searchString = value;
                        this.data.OnSearchChanged();
                        if (this.searchChanged != null)
                        {
                            this.searchChanged(this.state.searchString);
                        }
                    }
                }
            }
        }

        // Token: 0x17000E31 RID: 3633
        // (get) Token: 0x06004CFB RID: 19707 RVA: 0x00152EBC File Offset: 0x001510BC
        // (set) Token: 0x06004CFC RID: 19708 RVA: 0x00152ED7 File Offset: 0x001510D7
        public bool useScrollView
        {
            get
            {
                return this.m_UseScrollView;
            }
            set
            {
                this.m_UseScrollView = value;
            }
        }

        // Token: 0x17000E32 RID: 3634
        // (get) Token: 0x06004CFD RID: 19709 RVA: 0x00152EE4 File Offset: 0x001510E4
        public Rect visibleRect
        {
            get
            {
                return this.m_VisibleRect;
            }
        }

        // Token: 0x06004CFE RID: 19710 RVA: 0x00152F00 File Offset: 0x00151100
        public bool IsSelected(int id)
        {
            return this.state.selectedIDs.Contains(id);
        }

        // Token: 0x06004CFF RID: 19711 RVA: 0x00152F28 File Offset: 0x00151128
        public bool HasSelection()
        {
            return this.state.selectedIDs.Count<int>() > 0;
        }

        // Token: 0x06004D00 RID: 19712 RVA: 0x00152F50 File Offset: 0x00151150
        public int[] GetSelection()
        {
            return this.state.selectedIDs.ToArray();
        }

        // Token: 0x06004D01 RID: 19713 RVA: 0x00152F78 File Offset: 0x00151178
        public int[] GetRowIDs()
        {
            return this.data.GetRows().Select(new Func<TreeViewItem, int>(TreeViewController.< GetRowIDs > m__0)).ToArray<int>();
        }

        // Token: 0x06004D02 RID: 19714 RVA: 0x00152FBF File Offset: 0x001511BF
        public void SetSelection(int[] selectedIDs, bool revealSelectionAndFrameLastSelected)
        {
            this.SetSelection(selectedIDs, revealSelectionAndFrameLastSelected, false);
        }

        // Token: 0x06004D03 RID: 19715 RVA: 0x00152FCC File Offset: 0x001511CC
        public void SetSelection(int[] selectedIDs, bool revealSelectionAndFrameLastSelected, bool animatedFraming)
        {
            if (selectedIDs.Length > 0)
            {
                if (revealSelectionAndFrameLastSelected)
                {
                    foreach (int id in selectedIDs)
                    {
                        this.data.RevealItem(id);
                    }
                }
                this.state.selectedIDs = new List<int>(selectedIDs);
                bool flag = this.state.selectedIDs.IndexOf(this.state.lastClickedID) >= 0;
                if (!flag)
                {
                    int num = selectedIDs.Last<int>();
                    if (this.data.GetRow(num) != -1)
                    {
                        this.state.lastClickedID = num;
                        flag = true;
                    }
                    else
                    {
                        this.state.lastClickedID = 0;
                    }
                }
                if (revealSelectionAndFrameLastSelected && flag)
                {
                    this.Frame(this.state.lastClickedID, true, false, animatedFraming);
                }
            }
            else
            {
                this.state.selectedIDs.Clear();
                this.state.lastClickedID = 0;
            }
        }

        // Token: 0x06004D04 RID: 19716 RVA: 0x001530D0 File Offset: 0x001512D0
        public TreeViewItem FindItem(int id)
        {
            return this.data.FindItem(id);
        }

        // Token: 0x06004D05 RID: 19717 RVA: 0x00152ED7 File Offset: 0x001510D7
        [Obsolete("SetUseScrollView has been deprecated. Use property useScrollView instead.")]
        public void SetUseScrollView(bool useScrollView)
        {
            this.m_UseScrollView = useScrollView;
        }

        // Token: 0x06004D06 RID: 19718 RVA: 0x001530F1 File Offset: 0x001512F1
        public void SetConsumeKeyDownEvents(bool consume)
        {
            this.m_ConsumeKeyDownEvents = consume;
        }

        // Token: 0x06004D07 RID: 19719 RVA: 0x001530FB File Offset: 0x001512FB
        public void Repaint()
        {
            if (this.m_GUIView != null)
            {
                this.m_GUIView.Repaint();
            }
        }

        // Token: 0x06004D08 RID: 19720 RVA: 0x0015311A File Offset: 0x0015131A
        public void ReloadData()
        {
            this.data.ReloadData();
            this.Repaint();
            this.m_StopIteratingItems = true;
        }

        // Token: 0x06004D09 RID: 19721 RVA: 0x00153138 File Offset: 0x00151338
        public bool HasFocus()
        {
            bool flag = (!(this.m_GUIView != null)) ? EditorGUIUtility.HasCurrentWindowKeyFocus() : this.m_GUIView.hasFocus;
            return flag && GUIUtility.keyboardControl == this.m_KeyboardControlID;
        }

        // Token: 0x06004D0A RID: 19722 RVA: 0x0015318C File Offset: 0x0015138C
        internal static int GetItemControlID(TreeViewItem item)
        {
            return ((item == null) ? 0 : item.id) + 10000000;
        }

        // Token: 0x06004D0B RID: 19723 RVA: 0x001531BC File Offset: 0x001513BC
        public void HandleUnusedMouseEventsForItem(Rect rect, TreeViewItem item, int row)
        {
            int itemControlID = TreeViewController.GetItemControlID(item);
            Event current = Event.current;
            EventType typeForControl = current.GetTypeForControl(itemControlID);
            switch (typeForControl)
            {
                case EventType.MouseDown:
                    if (rect.Contains(Event.current.mousePosition))
                    {
                        if (Event.current.button == 0)
                        {
                            GUIUtility.keyboardControl = this.m_KeyboardControlID;
                            this.Repaint();
                            if (Event.current.clickCount == 2)
                            {
                                if (this.itemDoubleClickedCallback != null)
                                {
                                    this.itemDoubleClickedCallback(item.id);
                                }
                            }
                            else
                            {
                                List<int> newSelection = this.GetNewSelection(item, true, false);
                                bool flag = this.dragging != null && newSelection.Count != 0 && this.dragging.CanStartDrag(item, newSelection, Event.current.mousePosition);
                                if (flag)
                                {
                                    this.m_DragSelection = newSelection;
                                    DragAndDropDelay dragAndDropDelay = (DragAndDropDelay)GUIUtility.GetStateObject(typeof(DragAndDropDelay), itemControlID);
                                    dragAndDropDelay.mouseDownPosition = Event.current.mousePosition;
                                }
                                else
                                {
                                    this.m_DragSelection.Clear();
                                    if (this.m_AllowRenameOnMouseUp)
                                    {
                                        this.m_AllowRenameOnMouseUp = (this.state.selectedIDs.Count == 1 && this.state.selectedIDs[0] == item.id);
                                    }
                                    this.SelectionClick(item, false);
                                    if (this.itemSingleClickedCallback != null)
                                    {
                                        this.itemSingleClickedCallback(item.id);
                                    }
                                }
                                GUIUtility.hotControl = itemControlID;
                            }
                            current.Use();
                        }
                        else if (Event.current.button == 1)
                        {
                            bool keepMultiSelection = true;
                            this.SelectionClick(item, keepMultiSelection);
                        }
                    }
                    break;
                case EventType.MouseUp:
                    if (GUIUtility.hotControl == itemControlID)
                    {
                        bool flag2 = this.m_DragSelection.Count > 0;
                        GUIUtility.hotControl = 0;
                        this.m_DragSelection.Clear();
                        current.Use();
                        if (rect.Contains(current.mousePosition))
                        {
                            Rect renameRect = this.gui.GetRenameRect(rect, row, item);
                            List<int> selectedIDs = this.state.selectedIDs;
                            if (this.m_AllowRenameOnMouseUp && selectedIDs != null && selectedIDs.Count == 1 && selectedIDs[0] == item.id && renameRect.Contains(current.mousePosition) && !EditorGUIUtility.HasHolddownKeyModifiers(current))
                            {
                                this.BeginNameEditing(0.5f);
                            }
                            else if (flag2)
                            {
                                this.SelectionClick(item, false);
                                if (this.itemSingleClickedCallback != null)
                                {
                                    this.itemSingleClickedCallback(item.id);
                                }
                            }
                        }
                    }
                    break;
                default:
                    if (typeForControl != EventType.DragUpdated && typeForControl != EventType.DragPerform)
                    {
                        if (typeForControl == EventType.ContextClick)
                        {
                            if (rect.Contains(current.mousePosition))
                            {
                                if (this.contextClickItemCallback != null)
                                {
                                    this.contextClickItemCallback(item.id);
                                }
                            }
                        }
                    }
                    else if (this.dragging != null && this.dragging.DragElement(item, rect, row))
                    {
                        GUIUtility.hotControl = 0;
                    }
                    break;
                case EventType.MouseDrag:
                    if (GUIUtility.hotControl == itemControlID && this.dragging != null && this.m_DragSelection.Count > 0)
                    {
                        DragAndDropDelay dragAndDropDelay2 = (DragAndDropDelay)GUIUtility.GetStateObject(typeof(DragAndDropDelay), itemControlID);
                        if (dragAndDropDelay2.CanStartDrag() && this.dragging.CanStartDrag(item, this.m_DragSelection, dragAndDropDelay2.mouseDownPosition))
                        {
                            this.dragging.StartDrag(item, this.m_DragSelection);
                            GUIUtility.hotControl = 0;
                        }
                        current.Use();
                    }
                    break;
            }
        }

        // Token: 0x06004D0C RID: 19724 RVA: 0x0015358A File Offset: 0x0015178A
        public void GrabKeyboardFocus()
        {
            this.m_GrabKeyboardFocus = true;
        }

        // Token: 0x06004D0D RID: 19725 RVA: 0x00153594 File Offset: 0x00151794
        public void NotifyListenersThatSelectionChanged()
        {
            if (this.selectionChangedCallback != null)
            {
                this.selectionChangedCallback(this.state.selectedIDs.ToArray());
            }
        }

        // Token: 0x06004D0E RID: 19726 RVA: 0x001535BD File Offset: 0x001517BD
        public void NotifyListenersThatDragEnded(int[] draggedIDs, bool draggedItemsFromOwnTreeView)
        {
            if (this.dragEndedCallback != null)
            {
                this.dragEndedCallback(draggedIDs, draggedItemsFromOwnTreeView);
            }
        }

        // Token: 0x06004D0F RID: 19727 RVA: 0x001535D8 File Offset: 0x001517D8
        public Vector2 GetContentSize()
        {
            return this.gui.GetTotalSize();
        }

        // Token: 0x06004D10 RID: 19728 RVA: 0x001535F8 File Offset: 0x001517F8
        public Rect GetTotalRect()
        {
            return this.m_TotalRect;
        }

        // Token: 0x06004D11 RID: 19729 RVA: 0x00153613 File Offset: 0x00151813
        public void SetTotalRect(Rect rect)
        {
            this.m_TotalRect = rect;
        }

        // Token: 0x06004D12 RID: 19730 RVA: 0x00153620 File Offset: 0x00151820
        public bool IsItemDragSelectedOrSelected(TreeViewItem item)
        {
            return (this.m_DragSelection.Count <= 0) ? this.state.selectedIDs.Contains(item.id) : this.m_DragSelection.Contains(item.id);
        }

        // Token: 0x17000E33 RID: 3635
        // (get) Token: 0x06004D13 RID: 19731 RVA: 0x00153674 File Offset: 0x00151874
        public bool animatingExpansion
        {
            get
            {
                return this.m_UseExpansionAnimation && this.m_ExpansionAnimator.isAnimating;
            }
        }

        // Token: 0x06004D14 RID: 19732 RVA: 0x001536A4 File Offset: 0x001518A4
        private void DoItemGUI(TreeViewItem item, int row, float rowWidth, bool hasFocus)
        {
            if (row < 0 || row >= this.data.rowCount)
            {
                Debug.LogError(string.Concat(new object[]
                {
                    "Invalid. Org row: ",
                    row,
                    " Num rows ",
                    this.data.rowCount
                }));
            }
            else
            {
                bool selected = this.IsItemDragSelectedOrSelected(item);
                Rect rect = this.gui.GetRowRect(row, rowWidth);
                if (this.animatingExpansion)
                {
                    rect = this.m_ExpansionAnimator.OnBeginRowGUI(row, rect);
                }
                if (this.animatingExpansion)
                {
                    this.m_ExpansionAnimator.OnRowGUI(row);
                }
                this.gui.OnRowGUI(rect, item, row, selected, hasFocus);
                if (this.onGUIRowCallback != null)
                {
                    float contentIndent = this.gui.GetContentIndent(item);
                    Rect arg = new Rect(rect.x + contentIndent, rect.y, rect.width - contentIndent, rect.height);
                    this.onGUIRowCallback(item.id, arg);
                }
                if (this.animatingExpansion)
                {
                    this.m_ExpansionAnimator.OnEndRowGUI(row);
                }
                this.HandleUnusedMouseEventsForItem(rect, item, row);
            }
        }

        // Token: 0x06004D15 RID: 19733 RVA: 0x001537D8 File Offset: 0x001519D8
        public void OnGUI(Rect rect, int keyboardControlID)
        {
            this.m_KeyboardControlID = keyboardControlID;
            Event current = Event.current;
            if (current.type == EventType.Repaint)
            {
                this.m_TotalRect = rect;
            }
            this.m_GUIView = GUIView.current;
            if (this.m_GUIView != null && !this.m_GUIView.hasFocus && this.state.renameOverlay.IsRenaming())
            {
                this.EndNameEditing(true);
            }
            if (this.m_GrabKeyboardFocus || (current.type == EventType.MouseDown && this.m_TotalRect.Contains(current.mousePosition)))
            {
                this.m_GrabKeyboardFocus = false;
                GUIUtility.keyboardControl = this.m_KeyboardControlID;
                this.m_AllowRenameOnMouseUp = true;
                this.Repaint();
            }
            bool flag = this.HasFocus();
            if (flag != this.m_HadFocusLastEvent && current.type != EventType.Layout)
            {
                this.m_HadFocusLastEvent = flag;
                if (flag)
                {
                    if (current.type == EventType.MouseDown)
                    {
                        this.m_AllowRenameOnMouseUp = false;
                    }
                }
            }
            if (this.animatingExpansion)
            {
                this.m_ExpansionAnimator.OnBeforeAllRowsGUI();
            }
            this.data.InitIfNeeded();
            Vector2 totalSize = this.gui.GetTotalSize();
            this.m_ContentRect = new Rect(0f, 0f, totalSize.x, totalSize.y);
            if (this.m_UseScrollView)
            {
                this.state.scrollPos = GUI.BeginScrollView(this.m_TotalRect, this.state.scrollPos, this.m_ContentRect, (this.horizontalScrollbarStyle == null) ? GUI.skin.horizontalScrollbar : this.horizontalScrollbarStyle, (this.verticalScrollbarStyle == null) ? GUI.skin.verticalScrollbar : this.verticalScrollbarStyle);
            }
            else
            {
                GUI.BeginClip(this.m_TotalRect);
            }
            if (current.type == EventType.Repaint)
            {
                if (this.m_UseScrollView)
                {
                    this.m_VisibleRect = GUI.GetTopScrollView().visibleRect;
                }
                else
                {
                    ScrollViewState topScrollView = GUI.GetTopScrollView();
                    if (topScrollView != null)
                    {
                        this.state.scrollPos = Vector2.Max(Vector2.zero, topScrollView.scrollPosition - this.m_TotalRect.min - topScrollView.position.min);
                        this.m_VisibleRect = topScrollView.visibleRect;
                        this.m_VisibleRect.size = Vector2.Max(Vector2.zero, Vector2.Min(this.m_VisibleRect.size, this.m_TotalRect.size - this.state.scrollPos));
                    }
                    else
                    {
                        this.m_VisibleRect = this.m_TotalRect;
                    }
                }
            }
            this.gui.BeginRowGUI();
            int num;
            int num2;
            this.gui.GetFirstAndLastRowVisible(out num, out num2);
            if (num2 >= 0)
            {
                int numVisibleRows = num2 - num + 1;
                float rowWidth = Mathf.Max(GUIClip.visibleRect.width, this.m_ContentRect.width);
                this.IterateVisibleItems(num, numVisibleRows, rowWidth, flag);
            }
            if (this.animatingExpansion)
            {
                this.m_ExpansionAnimator.OnAfterAllRowsGUI();
            }
            this.gui.EndRowGUI();
            if (this.m_UseScrollView)
            {
                GUI.EndScrollView(this.showingVerticalScrollBar);
            }
            else
            {
                GUI.EndClip();
            }
            this.HandleUnusedEvents();
            this.KeyboardGUI();
            GUIUtility.GetControlID(33243602, FocusType.Passive);
        }

        // Token: 0x06004D16 RID: 19734 RVA: 0x00153B40 File Offset: 0x00151D40
        private void IterateVisibleItems(int firstRow, int numVisibleRows, float rowWidth, bool hasFocus)
        {
            this.m_StopIteratingItems = false;
            int num = 0;
            int i = 0;
            while (i < numVisibleRows)
            {
                int num2 = firstRow + i;
                if (this.animatingExpansion)
                {
                    int endRow = this.m_ExpansionAnimator.endRow;
                    if (this.m_ExpansionAnimator.CullRow(num2, this.gui))
                    {
                        num++;
                        num2 = endRow + num;
                    }
                    else
                    {
                        num2 += num;
                    }
                    if (num2 < this.data.rowCount)
                    {
                        goto IL_BA;
                    }
                }
                else
                {
                    float num3 = this.gui.GetRowRect(num2, rowWidth).y - this.state.scrollPos.y;
                    if (num3 <= this.m_TotalRect.height)
                    {
                        goto IL_BA;
                    }
                }
                IL_E1:
                i++;
                continue;
                IL_BA:
                this.DoItemGUI(this.data.GetItem(num2), num2, rowWidth, hasFocus);
                if (this.m_StopIteratingItems)
                {
                    break;
                }
                goto IL_E1;
            }
        }

        // Token: 0x06004D17 RID: 19735 RVA: 0x00153C3C File Offset: 0x00151E3C
        private List<int> GetVisibleSelectedIds()
        {
            int num;
            int num2;
            this.gui.GetFirstAndLastRowVisible(out num, out num2);
            List<int> result;
            if (num2 < 0)
            {
                result = new List<int>();
            }
            else
            {
                List<int> list = new List<int>(num2 - num);
                for (int i = num; i < num2; i++)
                {
                    TreeViewItem item = this.data.GetItem(i);
                    list.Add(item.id);
                }
                List<int> list2 = list.Where(new Func<int, bool>(this.< GetVisibleSelectedIds > m__1)).ToList<int>();
                result = list2;
            }
            return result;
        }

        // Token: 0x06004D18 RID: 19736 RVA: 0x00153CCA File Offset: 0x00151ECA
        private void ExpansionAnimationEnded(TreeViewAnimationInput setup)
        {
            if (!setup.expanding)
            {
                this.ChangeExpandedState(setup.item, false, setup.includeChildren);
            }
        }

        // Token: 0x06004D19 RID: 19737 RVA: 0x00153CF0 File Offset: 0x00151EF0
        private float GetAnimationDuration(float height)
        {
            return (height <= 60f) ? (height * 0.07f / 60f) : 0.07f;
        }

        // Token: 0x06004D1A RID: 19738 RVA: 0x00153D28 File Offset: 0x00151F28
        public void UserInputChangedExpandedState(TreeViewItem item, int row, bool expand)
        {
            bool alt = Event.current.alt;
            if (this.useExpansionAnimation)
            {
                if (expand)
                {
                    this.ChangeExpandedState(item, true, alt);
                }
                int num = row + 1;
                int lastChildRowUnder = this.GetLastChildRowUnder(row);
                float width = GUIClip.visibleRect.width;
                Rect rectForRows = this.GetRectForRows(num, lastChildRowUnder, width);
                float animationDuration = this.GetAnimationDuration(rectForRows.height);
                TreeViewAnimationInput setup = new TreeViewAnimationInput
                {
                    animationDuration = (double)animationDuration,
                    startRow = num,
                    endRow = lastChildRowUnder,
                    startRowRect = this.gui.GetRowRect(num, width),
                    rowsRect = rectForRows,
                    expanding = expand,
                    includeChildren = alt,
                    animationEnded = new Action<TreeViewAnimationInput>(this.ExpansionAnimationEnded),
                    item = item,
                    treeView = this
                };
                this.expansionAnimator.BeginAnimating(setup);
            }
            else
            {
                this.ChangeExpandedState(item, expand, alt);
            }
        }

        // Token: 0x06004D1B RID: 19739 RVA: 0x00153E24 File Offset: 0x00152024
        private void ChangeExpandedState(TreeViewItem item, bool expand, bool includeChildren)
        {
            if (includeChildren)
            {
                this.data.SetExpandedWithChildren(item, expand);
            }
            else
            {
                this.data.SetExpanded(item, expand);
            }
        }

        // Token: 0x06004D1C RID: 19740 RVA: 0x00153E4C File Offset: 0x0015204C
        private int GetLastChildRowUnder(int row)
        {
            IList<TreeViewItem> rows = this.data.GetRows();
            int depth = rows[row].depth;
            for (int i = row + 1; i < rows.Count; i++)
            {
                if (rows[i].depth <= depth)
                {
                    return i - 1;
                }
            }
            return rows.Count - 1;
        }

        // Token: 0x06004D1D RID: 19741 RVA: 0x00153EB8 File Offset: 0x001520B8
        protected virtual Rect GetRectForRows(int startRow, int endRow, float rowWidth)
        {
            Rect rowRect = this.gui.GetRowRect(startRow, rowWidth);
            Rect rowRect2 = this.gui.GetRowRect(endRow, rowWidth);
            return new Rect(rowRect.x, rowRect.y, rowWidth, rowRect2.yMax - rowRect.yMin);
        }

        // Token: 0x06004D1E RID: 19742 RVA: 0x00153F0C File Offset: 0x0015210C
        private void HandleUnusedEvents()
        {
            EventType type = Event.current.type;
            if (type != EventType.DragUpdated)
            {
                if (type != EventType.DragPerform)
                {
                    if (type != EventType.DragExited)
                    {
                        if (type != EventType.ContextClick)
                        {
                            if (type == EventType.MouseDown)
                            {
                                if (this.deselectOnUnhandledMouseDown && Event.current.button == 0 && this.m_TotalRect.Contains(Event.current.mousePosition) && this.state.selectedIDs.Count > 0)
                                {
                                    this.SetSelection(new int[0], false);
                                    this.NotifyListenersThatSelectionChanged();
                                }
                            }
                        }
                        else if (this.m_TotalRect.Contains(Event.current.mousePosition))
                        {
                            if (this.contextClickOutsideItemsCallback != null)
                            {
                                this.contextClickOutsideItemsCallback();
                            }
                        }
                    }
                    else if (this.dragging != null)
                    {
                        this.m_DragSelection.Clear();
                        this.dragging.DragCleanup(true);
                        this.Repaint();
                    }
                }
                else if (this.dragging != null && this.m_TotalRect.Contains(Event.current.mousePosition))
                {
                    this.m_DragSelection.Clear();
                    this.dragging.DragElement(null, default(Rect), -1);
                    this.Repaint();
                    Event.current.Use();
                }
            }
            else if (this.dragging != null && this.m_TotalRect.Contains(Event.current.mousePosition))
            {
                this.dragging.DragElement(null, default(Rect), -1);
                this.Repaint();
                Event.current.Use();
            }
        }

        // Token: 0x06004D1F RID: 19743 RVA: 0x001540CB File Offset: 0x001522CB
        public void OnEvent()
        {
            this.state.renameOverlay.OnEvent();
        }

        // Token: 0x06004D20 RID: 19744 RVA: 0x001540E0 File Offset: 0x001522E0
        public bool BeginNameEditing(float delay)
        {
            bool result;
            if (this.state.selectedIDs.Count == 0)
            {
                result = false;
            }
            else
            {
                IList<TreeViewItem> rows = this.data.GetRows();
                TreeViewItem treeViewItem = null;
                using (List<int>.Enumerator enumerator = this.state.selectedIDs.GetEnumerator())
                {
                    while (enumerator.MoveNext())
                    {
                        TreeViewController.< BeginNameEditing > c__AnonStorey0 < BeginNameEditing > c__AnonStorey = new TreeViewController.< BeginNameEditing > c__AnonStorey0();

                        < BeginNameEditing > c__AnonStorey.id = enumerator.Current;
                        TreeViewItem treeViewItem2 = rows.FirstOrDefault(new Func<TreeViewItem, bool>(< BeginNameEditing > c__AnonStorey.m__0));
                        if (treeViewItem == null)
                        {
                            treeViewItem = treeViewItem2;
                        }
                        else if (treeViewItem2 != null)
                        {
                            return false;
                        }
                    }
                }
                result = (treeViewItem != null && this.data.IsRenamingItemAllowed(treeViewItem) && this.gui.BeginRename(treeViewItem, delay));
            }
            return result;
        }

        // Token: 0x06004D21 RID: 19745 RVA: 0x001541D8 File Offset: 0x001523D8
        public void EndNameEditing(bool acceptChanges)
        {
            if (this.state.renameOverlay.IsRenaming())
            {
                this.state.renameOverlay.EndRename(acceptChanges);
                this.gui.EndRename();
            }
        }

        // Token: 0x06004D22 RID: 19746 RVA: 0x00154210 File Offset: 0x00152410
        private TreeViewItem GetItemAndRowIndex(int id, out int row)
        {
            row = this.data.GetRow(id);
            TreeViewItem result;
            if (row == -1)
            {
                result = null;
            }
            else
            {
                result = this.data.GetItem(row);
            }
            return result;
        }

        // Token: 0x06004D23 RID: 19747 RVA: 0x00154250 File Offset: 0x00152450
        private void HandleFastCollapse(TreeViewItem item, int row)
        {
            if (item.depth == 0)
            {
                for (int i = row - 1; i >= 0; i--)
                {
                    if (this.data.GetItem(i).hasChildren)
                    {
                        this.OffsetSelection(i - row);
                        break;
                    }
                }
            }
            else if (item.depth > 0)
            {
                for (int j = row - 1; j >= 0; j--)
                {
                    if (this.data.GetItem(j).depth < item.depth)
                    {
                        this.OffsetSelection(j - row);
                        break;
                    }
                }
            }
        }

        // Token: 0x06004D24 RID: 19748 RVA: 0x001542FC File Offset: 0x001524FC
        private void HandleFastExpand(TreeViewItem item, int row)
        {
            int rowCount = this.data.rowCount;
            for (int i = row + 1; i < rowCount; i++)
            {
                if (this.data.GetItem(i).hasChildren)
                {
                    this.OffsetSelection(i - row);
                    break;
                }
            }
        }

        // Token: 0x06004D25 RID: 19749 RVA: 0x00154351 File Offset: 0x00152551
        private void ChangeFolding(int[] ids, bool expand)
        {
            if (ids.Length == 1)
            {
                this.ChangeFoldingForSingleItem(ids[0], expand);
            }
            else if (ids.Length > 1)
            {
                this.ChangeFoldingForMultipleItems(ids, expand);
            }
        }

        // Token: 0x06004D26 RID: 19750 RVA: 0x00154380 File Offset: 0x00152580
        private void ChangeFoldingForSingleItem(int id, bool expand)
        {
            int row;
            TreeViewItem itemAndRowIndex = this.GetItemAndRowIndex(id, out row);
            if (itemAndRowIndex != null)
            {
                if (this.data.IsExpandable(itemAndRowIndex) && this.data.IsExpanded(itemAndRowIndex) != expand)
                {
                    this.UserInputChangedExpandedState(itemAndRowIndex, row, expand);
                }
                else
                {
                    this.expansionAnimator.SkipAnimating();
                    if (expand)
                    {
                        this.HandleFastExpand(itemAndRowIndex, row);
                    }
                    else
                    {
                        this.HandleFastCollapse(itemAndRowIndex, row);
                    }
                }
            }
        }

        // Token: 0x06004D27 RID: 19751 RVA: 0x001543FC File Offset: 0x001525FC
        private void ChangeFoldingForMultipleItems(int[] ids, bool expand)
        {
            HashSet<int> hashSet = new HashSet<int>();
            foreach (int num in ids)
            {
                int num2;
                TreeViewItem itemAndRowIndex = this.GetItemAndRowIndex(num, out num2);
                if (itemAndRowIndex != null)
                {
                    if (this.data.IsExpandable(itemAndRowIndex) && this.data.IsExpanded(itemAndRowIndex) != expand)
                    {
                        hashSet.Add(num);
                    }
                }
            }
            if (Event.current.alt)
            {
                foreach (int id in hashSet)
                {
                    this.data.SetExpandedWithChildren(id, expand);
                }
            }
            else
            {
                HashSet<int> hashSet2 = new HashSet<int>(this.data.GetExpandedIDs());
                if (expand)
                {
                    hashSet2.UnionWith(hashSet);
                }
                else
                {
                    hashSet2.ExceptWith(hashSet);
                }
                this.data.SetExpandedIDs(hashSet2.ToArray<int>());
            }
        }

        // Token: 0x06004D28 RID: 19752 RVA: 0x0015451C File Offset: 0x0015271C
        private void KeyboardGUI()
        {
            if (this.m_KeyboardControlID == GUIUtility.keyboardControl && GUI.enabled)
            {
                if (this.keyboardInputCallback != null)
                {
                    this.keyboardInputCallback();
                }
                if (this.m_ConsumeKeyDownEvents)
                {
                    if (Event.current.type == EventType.KeyDown)
                    {
                        KeyCode keyCode = Event.current.keyCode;
                        switch (keyCode)
                        {
                            case KeyCode.KeypadEnter:
                                break;
                            default:
                                if (keyCode != KeyCode.Return)
                                {
                                    if (Event.current.keyCode <= KeyCode.A || Event.current.keyCode < KeyCode.Z)
                                    {
                                    }
                                    goto IL_279;
                                }
                                break;
                            case KeyCode.UpArrow:
                                Event.current.Use();
                                this.OffsetSelection(-1);
                                goto IL_279;
                            case KeyCode.DownArrow:
                                Event.current.Use();
                                this.OffsetSelection(1);
                                goto IL_279;
                            case KeyCode.RightArrow:
                                this.ChangeFolding(this.state.selectedIDs.ToArray(), true);
                                Event.current.Use();
                                goto IL_279;
                            case KeyCode.LeftArrow:
                                this.ChangeFolding(this.state.selectedIDs.ToArray(), false);
                                Event.current.Use();
                                goto IL_279;
                            case KeyCode.Home:
                                Event.current.Use();
                                this.OffsetSelection(-1000000);
                                goto IL_279;
                            case KeyCode.End:
                                Event.current.Use();
                                this.OffsetSelection(1000000);
                                goto IL_279;
                            case KeyCode.PageUp:
                                {
                                    Event.current.Use();
                                    TreeViewItem treeViewItem = this.data.FindItem(this.state.lastClickedID);
                                    if (treeViewItem != null)
                                    {
                                        int numRowsOnPageUpDown = this.gui.GetNumRowsOnPageUpDown(treeViewItem, true, this.m_TotalRect.height);
                                        this.OffsetSelection(-numRowsOnPageUpDown);
                                    }
                                    goto IL_279;
                                }
                            case KeyCode.PageDown:
                                {
                                    Event.current.Use();
                                    TreeViewItem treeViewItem2 = this.data.FindItem(this.state.lastClickedID);
                                    if (treeViewItem2 != null)
                                    {
                                        int numRowsOnPageUpDown2 = this.gui.GetNumRowsOnPageUpDown(treeViewItem2, true, this.m_TotalRect.height);
                                        this.OffsetSelection(numRowsOnPageUpDown2);
                                    }
                                    goto IL_279;
                                }
                            case KeyCode.F2:
                                if (Application.platform != RuntimePlatform.OSXEditor && this.BeginNameEditing(0f))
                                {
                                    Event.current.Use();
                                }
                                goto IL_279;
                        }
                        if (Application.platform == RuntimePlatform.OSXEditor && this.BeginNameEditing(0f))
                        {
                            Event.current.Use();
                        }
                        IL_279:;
                    }
                }
            }
        }

        // Token: 0x06004D29 RID: 19753 RVA: 0x001547A4 File Offset: 0x001529A4
        internal static int GetIndexOfID(IList<TreeViewItem> items, int id)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].id == id)
                {
                    return i;
                }
            }
            return -1;
        }

        // Token: 0x06004D2A RID: 19754 RVA: 0x001547EC File Offset: 0x001529EC
        public bool IsLastClickedPartOfRows()
        {
            IList<TreeViewItem> rows = this.data.GetRows();
            return rows.Count != 0 && TreeViewController.GetIndexOfID(rows, this.state.lastClickedID) >= 0;
        }

        // Token: 0x06004D2B RID: 19755 RVA: 0x00154838 File Offset: 0x00152A38
        public void OffsetSelection(int offset)
        {
            this.expansionAnimator.SkipAnimating();
            IList<TreeViewItem> rows = this.data.GetRows();
            if (rows.Count != 0)
            {
                Event.current.Use();
                int indexOfID = TreeViewController.GetIndexOfID(rows, this.state.lastClickedID);
                int num = Mathf.Clamp(indexOfID + offset, 0, rows.Count - 1);
                this.EnsureRowIsVisible(num, true);
                this.SelectionByKey(rows[num]);
            }
        }

        // Token: 0x06004D2C RID: 19756 RVA: 0x001548B0 File Offset: 0x00152AB0
        private bool GetFirstAndLastSelected(List<TreeViewItem> items, out int firstIndex, out int lastIndex)
        {
            firstIndex = -1;
            lastIndex = -1;
            for (int i = 0; i < items.Count; i++)
            {
                if (this.state.selectedIDs.Contains(items[i].id))
                {
                    if (firstIndex == -1)
                    {
                        firstIndex = i;
                    }
                    lastIndex = i;
                }
            }
            return firstIndex != -1 && lastIndex != -1;
        }

        // Token: 0x06004D2D RID: 19757 RVA: 0x00154928 File Offset: 0x00152B28
        private List<int> GetNewSelection(TreeViewItem clickedItem, bool keepMultiSelection, bool useShiftAsActionKey)
        {
            IList<TreeViewItem> rows = this.data.GetRows();
            List<int> list = new List<int>(rows.Count);
            for (int i = 0; i < rows.Count; i++)
            {
                list.Add(rows[i].id);
            }
            List<int> selectedIDs = this.state.selectedIDs;
            int lastClickedID = this.state.lastClickedID;
            bool allowMultiSelection = this.data.CanBeMultiSelected(clickedItem);
            return InternalEditorUtility.GetNewSelection(clickedItem.id, list, selectedIDs, lastClickedID, keepMultiSelection, useShiftAsActionKey, allowMultiSelection);
        }

        // Token: 0x06004D2E RID: 19758 RVA: 0x001549BC File Offset: 0x00152BBC
        private void SelectionByKey(TreeViewItem itemSelected)
        {
            List<int> newSelection = this.GetNewSelection(itemSelected, false, true);
            this.NewSelectionFromUserInteraction(newSelection, itemSelected.id);
        }

        // Token: 0x06004D2F RID: 19759 RVA: 0x001549E4 File Offset: 0x00152BE4
        public void SelectionClick(TreeViewItem itemClicked, bool keepMultiSelection)
        {
            List<int> newSelection = this.GetNewSelection(itemClicked, keepMultiSelection, false);
            this.NewSelectionFromUserInteraction(newSelection, (itemClicked == null) ? 0 : itemClicked.id);
        }

        // Token: 0x06004D30 RID: 19760 RVA: 0x00154A18 File Offset: 0x00152C18
        private void NewSelectionFromUserInteraction(List<int> newSelection, int itemID)
        {
            this.state.lastClickedID = itemID;
            bool flag = !this.state.selectedIDs.SequenceEqual(newSelection);
            if (flag)
            {
                this.state.selectedIDs = newSelection;
                this.NotifyListenersThatSelectionChanged();
            }
        }

        // Token: 0x06004D31 RID: 19761 RVA: 0x00154A61 File Offset: 0x00152C61
        public void RemoveSelection()
        {
            if (this.state.selectedIDs.Count > 0)
            {
                this.state.selectedIDs.Clear();
                this.NotifyListenersThatSelectionChanged();
            }
        }

        // Token: 0x06004D32 RID: 19762 RVA: 0x00154A94 File Offset: 0x00152C94
        private float GetTopPixelOfRow(int row)
        {
            return this.gui.GetRowRect(row, 1f).y;
        }

        // Token: 0x06004D33 RID: 19763 RVA: 0x00154AC4 File Offset: 0x00152CC4
        private void EnsureRowIsVisible(int row, bool animated)
        {
            if (this.m_UseScrollView)
            {
                if (row >= 0)
                {
                    float num = (this.m_VisibleRect.height <= 0f) ? this.m_TotalRect.height : this.m_VisibleRect.height;
                    Rect rectForFraming = this.gui.GetRectForFraming(row);
                    float y = rectForFraming.y;
                    float num2 = rectForFraming.yMax - num;
                    if (this.state.scrollPos.y < num2)
                    {
                        this.ChangeScrollValue(num2, animated);
                    }
                    else if (this.state.scrollPos.y > y)
                    {
                        this.ChangeScrollValue(y, animated);
                    }
                }
            }
        }

        // Token: 0x06004D34 RID: 19764 RVA: 0x00154B7C File Offset: 0x00152D7C
        private void AnimatedScrollChanged()
        {
            this.Repaint();
            this.state.scrollPos.y = this.m_FramingAnimFloat.value;
        }

        // Token: 0x06004D35 RID: 19765 RVA: 0x00154BA0 File Offset: 0x00152DA0
        private void ChangeScrollValue(float targetScrollPos, bool animated)
        {
            if (this.m_UseExpansionAnimation && animated)
            {
                this.m_FramingAnimFloat.value = this.state.scrollPos.y;
                this.m_FramingAnimFloat.target = targetScrollPos;
                this.m_FramingAnimFloat.speed = 3f;
            }
            else
            {
                this.state.scrollPos.y = targetScrollPos;
            }
        }

        // Token: 0x06004D36 RID: 19766 RVA: 0x00154C10 File Offset: 0x00152E10
        public void Frame(int id, bool frame, bool ping)
        {
            this.Frame(id, frame, ping, false);
        }

        // Token: 0x06004D37 RID: 19767 RVA: 0x00154C20 File Offset: 0x00152E20
        public void Frame(int id, bool frame, bool ping, bool animated)
        {
            float num = -1f;
            if (frame)
            {
                this.data.RevealItem(id);
                int row = this.data.GetRow(id);
                if (row >= 0)
                {
                    num = this.GetTopPixelOfRow(row);
                    this.EnsureRowIsVisible(row, animated);
                }
            }
            if (ping)
            {
                int row2 = this.data.GetRow(id);
                if (num == -1f)
                {
                    if (row2 >= 0)
                    {
                        num = this.GetTopPixelOfRow(row2);
                    }
                }
                if (num >= 0f && row2 >= 0 && row2 < this.data.rowCount)
                {
                    TreeViewItem item = this.data.GetItem(row2);
                    float num2 = (this.GetContentSize().y <= this.m_TotalRect.height) ? 0f : -16f;
                    this.gui.BeginPingItem(item, num, this.m_TotalRect.width + num2);
                }
            }
        }

        // Token: 0x06004D38 RID: 19768 RVA: 0x00154D1D File Offset: 0x00152F1D
        public void EndPing()
        {
            this.gui.EndPingItem();
        }

        // Token: 0x06004D39 RID: 19769 RVA: 0x00154D2C File Offset: 0x00152F2C
        public List<int> SortIDsInVisiblityOrder(IList<int> ids)
        {
            List<int> result;
            if (ids.Count <= 1)
            {
                result = ids.ToList<int>();
            }
            else
            {
                IList<TreeViewItem> rows = this.data.GetRows();
                List<int> list = new List<int>();
                for (int i = 0; i < rows.Count; i++)
                {
                    int id = rows[i].id;
                    for (int j = 0; j < ids.Count; j++)
                    {
                        if (ids[j] == id)
                        {
                            list.Add(id);
                            break;
                        }
                    }
                }
                if (ids.Count != list.Count)
                {
                    list.AddRange(ids.Except(list));
                    if (ids.Count != list.Count)
                    {
                        Debug.LogError(string.Concat(new object[]
                        {
                            "SortIDsInVisiblityOrder failed: ",
                            ids.Count,
                            " != ",
                            list.Count
                        }));
                    }
                }
                result = list;
            }
            return result;
        }

        // Token: 0x06004D3A RID: 19770 RVA: 0x00154E3C File Offset: 0x0015303C
        [CompilerGenerated]
        private static int GetRowIDs(TreeViewItem item)
        {
            return item.id;
        }

        // Token: 0x06004D3B RID: 19771 RVA: 0x00154E58 File Offset: 0x00153058
        [CompilerGenerated]
        private bool GetVisibleSelectedIds1(int id)
        {
            return this.state.selectedIDs.Contains(id);
        }

        // Token: 0x04002821 RID: 10273
        [CompilerGenerated]
        private Action<int[]> _selectionChangedCallback;

        // Token: 0x04002822 RID: 10274
        [CompilerGenerated]
        private Action<int> _itemSingleClickedCallback;

        // Token: 0x04002823 RID: 10275
        [CompilerGenerated]
        private Action<int> _itemDoubleClickedCallback;

        // Token: 0x04002824 RID: 10276
        [CompilerGenerated]
        private Action<int[], bool> _dragEndedCallback;

        // Token: 0x04002825 RID: 10277
        [CompilerGenerated]
        private Action<int> _contextClickItemCallback;

        // Token: 0x04002826 RID: 10278
        [CompilerGenerated]
        private Action _contextClickOutsideItemsCallback;

        // Token: 0x04002827 RID: 10279
        [CompilerGenerated]
        private Action _keyboardInputCallback;

        // Token: 0x04002828 RID: 10280
        [CompilerGenerated]
        private Action _expandedStateChanged;

        // Token: 0x04002829 RID: 10281
        [CompilerGenerated]
        private Action<string> _searchChanged;

        // Token: 0x0400282A RID: 10282
        [CompilerGenerated]
        private Action<Vector2> _scrollChanged;

        // Token: 0x0400282B RID: 10283
        [CompilerGenerated]
        private Action<int, Rect> _onGUIRowCallback;

        // Token: 0x0400282C RID: 10284
        private GUIView m_GUIView;

        // Token: 0x0400282D RID: 10285
        [CompilerGenerated]
        private ITreeViewDataSource _data;

        // Token: 0x0400282E RID: 10286
        [CompilerGenerated]
        private ITreeViewDragging _dragging;

        // Token: 0x0400282F RID: 10287
        [CompilerGenerated]
        private ITreeViewGUI _gui;

        // Token: 0x04002830 RID: 10288
        [CompilerGenerated]
        private TreeViewState _state;

        // Token: 0x04002831 RID: 10289
        [CompilerGenerated]
        private GUIStyle _horizontalScrollbarStyle;

        // Token: 0x04002832 RID: 10290
        [CompilerGenerated]
        private GUIStyle _verticalScrollbarStyle;

        // Token: 0x04002833 RID: 10291
        private readonly TreeViewItemExpansionAnimator m_ExpansionAnimator = new TreeViewItemExpansionAnimator();

        // Token: 0x04002834 RID: 10292
        private AnimFloat m_FramingAnimFloat;

        // Token: 0x04002835 RID: 10293
        private bool m_StopIteratingItems;

        // Token: 0x04002836 RID: 10294
        [CompilerGenerated]
        private bool _deselectOnUnhandledMouseDown;

        // Token: 0x04002837 RID: 10295
        private List<int> m_DragSelection = new List<int>();

        // Token: 0x04002838 RID: 10296
        private bool m_UseScrollView = true;

        // Token: 0x04002839 RID: 10297
        private bool m_ConsumeKeyDownEvents = true;

        // Token: 0x0400283A RID: 10298
        private bool m_AllowRenameOnMouseUp = true;

        // Token: 0x0400283B RID: 10299
        internal const string kExpansionAnimationPrefKey = "TreeViewExpansionAnimation";

        // Token: 0x0400283C RID: 10300
        private bool m_UseExpansionAnimation = EditorPrefs.GetBool("TreeViewExpansionAnimation", true);

        // Token: 0x0400283D RID: 10301
        private bool m_GrabKeyboardFocus;

        // Token: 0x0400283E RID: 10302
        private Rect m_TotalRect;

        // Token: 0x0400283F RID: 10303
        private Rect m_VisibleRect;

        // Token: 0x04002840 RID: 10304
        private Rect m_ContentRect;

        // Token: 0x04002841 RID: 10305
        private bool m_HadFocusLastEvent;

        // Token: 0x04002842 RID: 10306
        private int m_KeyboardControlID;

        // Token: 0x04002843 RID: 10307
        private const float kSpaceForScrollBar = 16f;

        // Token: 0x04002844 RID: 10308
        [CompilerGenerated]
        private static Func<TreeViewItem, int> f__am$cache0;

		// Token: 0x020008E5 RID: 2277
		[CompilerGenerated]
        private sealed class BeginNameEditing
        {
            // Token: 0x06004D3C RID: 19772 RVA: 0x00002D4C File Offset: 0x00000F4C
            public BeginNameEditing()
            {
            }

            // Token: 0x06004D3D RID: 19773 RVA: 0x00154E80 File Offset: 0x00153080
            internal bool m__0(TreeViewItem i)
            {
                return i.id == this.id;
            }

            // Token: 0x04002845 RID: 10309
            internal int id;
        }
    }

    internal interface ITreeViewDataSource
    {
        // Token: 0x06004C37 RID: 19511
        void OnInitialize();

        // Token: 0x17000DFF RID: 3583
        // (get) Token: 0x06004C38 RID: 19512
        TreeViewItem root { get; }

        // Token: 0x17000E00 RID: 3584
        // (get) Token: 0x06004C39 RID: 19513
        int rowCount { get; }

        // Token: 0x06004C3A RID: 19514
        void ReloadData();

        // Token: 0x06004C3B RID: 19515
        void InitIfNeeded();

        // Token: 0x06004C3C RID: 19516
        TreeViewItem FindItem(int id);

        // Token: 0x06004C3D RID: 19517
        int GetRow(int id);

        // Token: 0x06004C3E RID: 19518
        TreeViewItem GetItem(int row);

        // Token: 0x06004C3F RID: 19519
        IList<TreeViewItem> GetRows();

        // Token: 0x06004C40 RID: 19520
        bool IsRevealed(int id);

        // Token: 0x06004C41 RID: 19521
        void RevealItem(int id);

        // Token: 0x06004C42 RID: 19522
        void SetExpandedWithChildren(TreeViewItem item, bool expand);

        // Token: 0x06004C43 RID: 19523
        void SetExpanded(TreeViewItem item, bool expand);

        // Token: 0x06004C44 RID: 19524
        bool IsExpanded(TreeViewItem item);

        // Token: 0x06004C45 RID: 19525
        bool IsExpandable(TreeViewItem item);

        // Token: 0x06004C46 RID: 19526
        void SetExpandedWithChildren(int id, bool expand);

        // Token: 0x06004C47 RID: 19527
        int[] GetExpandedIDs();

        // Token: 0x06004C48 RID: 19528
        void SetExpandedIDs(int[] ids);

        // Token: 0x06004C49 RID: 19529
        bool SetExpanded(int id, bool expand);

        // Token: 0x06004C4A RID: 19530
        bool IsExpanded(int id);

        // Token: 0x06004C4B RID: 19531
        bool CanBeMultiSelected(TreeViewItem item);

        // Token: 0x06004C4C RID: 19532
        bool CanBeParent(TreeViewItem item);

        // Token: 0x06004C4D RID: 19533
        bool IsRenamingItemAllowed(TreeViewItem item);

        // Token: 0x06004C4E RID: 19534
        void InsertFakeItem(int id, int parentID, string name, Texture2D icon);

        // Token: 0x06004C4F RID: 19535
        void RemoveFakeItem();

        // Token: 0x06004C50 RID: 19536
        bool HasFakeItem();

        // Token: 0x06004C51 RID: 19537
        void OnSearchChanged();
    }

    internal interface ITreeViewGUI
    {
        // Token: 0x06004C5B RID: 19547
        void OnInitialize();

        // Token: 0x06004C5C RID: 19548
        Vector2 GetTotalSize();

        // Token: 0x06004C5D RID: 19549
        void GetFirstAndLastRowVisible(out int firstRowVisible, out int lastRowVisible);

        // Token: 0x06004C5E RID: 19550
        Rect GetRowRect(int row, float rowWidth);

        // Token: 0x06004C5F RID: 19551
        Rect GetRectForFraming(int row);

        // Token: 0x06004C60 RID: 19552
        int GetNumRowsOnPageUpDown(TreeViewItem fromItem, bool pageUp, float heightOfTreeView);

        // Token: 0x06004C61 RID: 19553
        void OnRowGUI(Rect rowRect, TreeViewItem item, int row, bool selected, bool focused);

        // Token: 0x06004C62 RID: 19554
        void BeginRowGUI();

        // Token: 0x06004C63 RID: 19555
        void EndRowGUI();

        // Token: 0x06004C64 RID: 19556
        void BeginPingItem(TreeViewItem item, float topPixelOfRow, float availableWidth);

        // Token: 0x06004C65 RID: 19557
        void EndPingItem();

        // Token: 0x06004C66 RID: 19558
        bool BeginRename(TreeViewItem item, float delay);

        // Token: 0x06004C67 RID: 19559
        void EndRename();

        // Token: 0x06004C68 RID: 19560
        Rect GetRenameRect(Rect rowRect, int row, TreeViewItem item);

        // Token: 0x06004C69 RID: 19561
        float GetContentIndent(TreeViewItem item);

        // Token: 0x17000E02 RID: 3586
        // (get) Token: 0x06004C6A RID: 19562
        float halfDropBetweenHeight { get; }

        // Token: 0x17000E03 RID: 3587
        // (get) Token: 0x06004C6B RID: 19563
        float topRowMargin { get; }

        // Token: 0x17000E04 RID: 3588
        // (get) Token: 0x06004C6C RID: 19564
        float bottomRowMargin { get; }
    }*/
}
