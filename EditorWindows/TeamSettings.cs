﻿using UnityEngine;
using UnityEditor;
using System.IO;

namespace MassiveStudio.TeamProjectEditor
{
    //[InitializeOnLoad]
    public class TeamSettings : EditorWindow
    {
        public TeamProject project;
        public static string tempPath = "";

        [SerializeField]
        public static TeamSettings editor;

        public static GUISkin headline;
        public static GUISkin headline2;

        public static GUIStyle headlinestyle;

        [SerializeField]
        public int menu_sel = 0;
        public static string[] menu_names = new string[] { "Main", "Account", "Permission", "Settings","Scenes", "Status" };

        public static bool isDebug = true;

        [SerializeField]
        private static TeamSettings secondwindow;

        [SerializeField]
        public bool ismain = true;

        private static bool isinit = false;

        static TeamSettings()
        {
            //InitSettings();
            //LoadProjectData();
        }

        [MenuItem("Team/Project Settings")]
        static void InitSettings()//Loads the editor window
        {
            InitStyle();

            editor = EditorWindow.GetWindow<TeamSettings>();
            //editor.position = new Rect(100, 100, 200, 400);
            editor.titleContent = new GUIContent("TeamProject");
            editor.minSize = new Vector2(200, 400);
            editor.maxSize = editor.minSize;
            editor.Show();
            editor.ismain = true;

            editor.LoadProjectData();


            TeamProjectEditor.SystemUtility.EditorEventSystem.OnDeserilization -= editor.SaveProjectDataChanges;
            TeamProjectEditor.SystemUtility.EditorEventSystem.OnDeserilization += editor.SaveProjectDataChanges;

            //MOVE TO NEW SCRIPT TEAMSETTINGS_SUBWINDOW
            //secondwindow = ScriptableObject.CreateInstance<TeamSettings>();
            //secondwindow.position = new Rect(300, 100, 600, 400);
            //secondwindow.ShowPopup();
            //secondwindow.ismain = false;


             /* headline = (GUISkin)GUISkin.FindObjectOfType(typeof(GUISkin));
             headline.label.fontSize = 20;
             headline.textField.fontSize = 20;
             headline.box.fontSize = 20;
             headline.button.fontSize = 20;
             headline.label.alignment = (TextAnchor)3;

             headline2 = (GUISkin)GUISkin.FindObjectOfType(typeof(GUISkin));
             headline2.label.fontSize = 30;
             headline2.label.alignment = (TextAnchor)4;*/
            tempPath = (Path.GetFullPath(Path.Combine(Application.dataPath, @"../")) + "/Library/TeamManager.asset");

        }

        public static void InitStyle()
        {
            headlinestyle = new GUIStyle("label");
            headlinestyle.alignment = TextAnchor.MiddleCenter;
            headlinestyle.fontSize = 18;
            isinit = true;
        }

        public void OnDestroy()
        {
            TeamProjectEditor.SystemUtility.EditorEventSystem.OnDeserilization -= editor.SaveProjectDataChanges;
        }

        void LoadProjectData()
        {
            if(tempPath== string.Empty)
                tempPath = (Path.GetFullPath(Path.Combine(Application.dataPath, @"../")) + "/Library/TeamManager.asset");

            if (File.Exists(tempPath))
            {
                FileStream fs = new FileStream(tempPath, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                if (fs.Length > 0)
                {
                    Header h = new Header(br.ReadBytes((int)fs.Length));
                    project = Serializer.ToType<TeamProject>(h.data);
                }

                br.Close();
                fs.Close();
            }
            else
            {
                File.Create(tempPath);
                project = null;
                menu_sel = 2;//Create new Account
            }
        }

        public void SaveProjectDataChanges()
        {
            if(tempPath== string.Empty)
                tempPath = (Path.GetFullPath(Path.Combine(Application.dataPath, @"../")) + "/Library/TeamManager.asset");

            if (project == null)
                return;

            byte[] bytes = Serializer.TypeToByte<TeamProject>(project);
            FileStream fs = new FileStream(tempPath, FileMode.OpenOrCreate);
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(bytes);
            bw.Close();
            fs.Close();
        }

        TeamProject NewProject = null;

        void OnGUI()
        {
            if (!isinit)
                InitStyle();

            GUI.Label(new Rect(0, 0, 100, 50), "M:" + ismain + " W:" + (editor == null ? "NULL" : "EXISTS") + " M:" + menu_sel);

            if (ismain && editor == null)
            {
                InitSettings();
                return;
            }

            if (ismain)
            {
                if (secondwindow != null)
                {
                    Rect r = new Rect(this.position.x + this.position.width, this.position.y, 600, 400);
                    if (secondwindow.position != r)
                    {
                        secondwindow.position = r;
                        secondwindow.Repaint();
                    }
                }

                //0->Connect / Edit / Create /Info Button
                //1->Edit Project Info
                //2->Create Project / Connect

                if (menu_sel == 0)
                    Main_OverviewScreen();
                else if (menu_sel == 1)
                    Main_ChangeProjectInfo();
                else if( menu_sel == 2)
                    Main_CreateNewProjectScreen();
            }
            else
            {

                if (project == null)
                {
                    menu_sel = 0;
                    LoadProjectData();
                }

                //if (GUILayout.Button("Agree!")) this.Close();

                if (GUI.Button(new Rect(0, 0, position.width, 15), "<-----------"))//BACK TO MENU
                    this.Close();

                menu_sel = GUI.Toolbar(new Rect(0, 15, position.width, 25), menu_sel, menu_names);

                if (menu_sel == 1)
                    Subwindow_AccountView();
                else if (menu_sel == 2)
                    Subwindow_PermissionView();
                else if (menu_sel == 3)
                    Subwindow_SettingsView();
                else if (menu_sel == 4)
                    Subwindow_SceneView();
                else
                    Subwindow_StatusView();
            }
            //if(editor==null)
            //   InitSettings();
            
        }

        public void Main_OverviewScreen()
        {
            if (project == null)
            {
                LoadProjectData();
                //Check if Project Data Found
                if (project == null)
                    menu_sel = 2;
            }
            else
            {
                GUI.enabled = !TeamServerConnection.connected;
                if (GUI.Button(new Rect(50, 50, 100, 25), "Create New"))
                {
                    project = null;
                    menu_sel = 2;
                }

                if (GUI.Button(new Rect(50, 100, 100, 25), "Edit"))
                    menu_sel = 1;

                GUI.enabled = true;

                if (GUI.Button(new Rect(50, 150, 100, 25), "Info"))
                {
                    if (secondwindow == null)
                    {
                        //Initialize SecondWindow
                        secondwindow = ScriptableObject.CreateInstance<TeamSettings>();
                        secondwindow.position = new Rect(300, 100, 600, 400);
                        secondwindow.ShowPopup();
                        secondwindow.ismain = false;
                    }
                    secondwindow.ShowPopup();
                }

                if (GUI.Button(new Rect(50, 200, 100, 25), !TeamServerConnection.connected ? "Connect" : "Disconnect"))
                {

                    if (!TeamServerConnection.connected)
                    {
                        Debug.Log("SERVER USERNAME: " + project.user.Username + ":" + project.user.Password);
                        Utility.Dispatcher.ToBackground(() =>
                        {
                            TeamServerConnection.Connect(project);
                        });
                    }
                    else
                    {
                        TeamServerConnection.Disconnect();
                        TeamSceneManager.DeaktivateBackgroundTeamSceneLoader();
                    }
                }
            }
        }

        public void Main_ChangeProjectInfo()
        {
            if (project == null)
            {
                menu_sel = 0;
                LoadProjectData();
            }

            GUI.Label(new Rect(0, 25, Screen.width, 50), "Team Project Information");

            GUI.Label(new Rect(0, 70, 100, 25), "Project Name:");
            project.ProjectName = GUI.TextField(new Rect(100, 70, 200, 25), project.ProjectName);

            GUI.Label(new Rect(0, 95, 100, 25), "Server IP:");
            project.IP = GUI.TextField(new Rect(100, 95, 200, 25), project.IP);

            GUI.Label(new Rect(0, 120, 100, 25), "Server Port:");
            project.PORT = EditorGUI.IntField(new Rect(100, 120, 200, 25), project.PORT);

            GUI.Label(new Rect(0, 145, 100, 25), "Server TPort:");
            project.Transport_Port = EditorGUI.IntField(new Rect(100, 145, 200, 25), project.Transport_Port);
            /* try
             {
                 GUI.Label(new Rect(0, 200, 100, 25), "Ping: " + (TeamServerConnection.ping != null ? TeamServerConnection.ping.time : -0f));
             }
             catch (System.Exception e)
             {
                 Debug.LogError(e.Message);
             }*/

            if (GUI.changed)
                SaveProjectDataChanges();

            if (GUI.Button(new Rect(position.width - 100, position.height - 25, 100, 25), "<-"))
                menu_sel = 0;
        }

        public void Main_CreateNewProjectScreen()
        {
            if (headlinestyle == null)
                InitStyle();

            if (project == null)
            {
                if (NewProject == null)
                {
                    NewProject = new TeamProject();

                    if (project != null && project.user == null)
                        project.user = new UserAccount();

                    if (NewProject.user == null)
                        NewProject.user = new UserAccount();
                }

                GUI.Label(new Rect(0, 0, 200, 50), "New Team Project", headlinestyle);

                GUI.Box(new Rect(0, 0, 170, 15), "");//,errorstyle); //For Error

                if (GUI.Button(new Rect(170, 0, 30, 15), "<-"))//BACK TO MENU
                    this.menu_sel = 0;

                //GUI.Label(new Rect(0, 50, 100, 50), "Project Name:");
                NewProject.ProjectName = GUI.TextField(new Rect(50, 50, 100, 25), NewProject.ProjectName);
                if (NewProject.ProjectName.Length == 0)
                    GUI.Label(new Rect(50, 50, 100, 25), "Project Name");

                //GUI.Label(new Rect(0, 100, 100, 50), "Server IP:");
                NewProject.IP = GUI.TextField(new Rect(50, 100, 100, 25), NewProject.IP);
                if (NewProject.IP.Length == 0)
                    GUI.Label(new Rect(50, 100, 100, 25), "Server IP:");

                NewProject.PORT = EditorGUI.IntField(new Rect(50, 150, 100, 25), NewProject.PORT);
                if (NewProject.PORT == 0)
                    GUI.Label(new Rect(0, 150, 100, 50), "Server Port:");

                //GUI.Label(new Rect(0, 200, 100, 50), "Transport Port:");
                NewProject.Transport_Port = EditorGUI.IntField(new Rect(50, 200, 100, 25), NewProject.Transport_Port);
                if (NewProject.Transport_Port == 0)
                    GUI.Label(new Rect(50, 200, 100, 25), "Transport Port");

                //GUI.Label(new Rect(0, 250, 100, 50), "Username:");
                NewProject.user.Username = GUI.TextField(new Rect(50, 250, 100, 25), NewProject.user.Username);
                if (NewProject.user.Username.Length == 0)
                    GUI.Label(new Rect(50, 250, 100, 25), "Username");

                //GUI.Label(new Rect(0, 300, 100, 50), "Password:");
                NewProject.user.Password = EditorGUI.PasswordField(new Rect(50, 300, 100, 25), NewProject.user.Password);
                if (NewProject.user.Password.Length == 0)
                    GUI.Label(new Rect(50, 300, 100, 25), "Password");

                if (GUI.Button(new Rect(25, 350, 150, 50), "Create'n & Connect" + "\n" + "TeamProject"))
                {
                    byte[] bytes = Serializer.TypeToByte<TeamProject>(NewProject);
                    FileStream fs = new FileStream(tempPath, FileMode.OpenOrCreate);
                    BinaryWriter bw = new BinaryWriter(fs);
                    bw.Write(bytes);
                    bw.Close();
                    fs.Close();

                    NewProject = null;
                    //LoadProjectData();-> MENU_0 do it
                    menu_sel = 0;
                }
            }
        }

        public void Subwindow_AccountView()
        {
            GUI.changed = false;
            GUI.enabled = !TeamServerConnection.connected;
            GUI.Label(new Rect(0, 25, Screen.width, 50), "Account Information");

            GUI.Label(new Rect(0, 50, 100, 25), "Username:");
            project.user.Username = GUI.TextField(new Rect(100, 50, 200, 25), project.user.Username);

            GUI.Label(new Rect(0, 75, 100, 25), "Password:");
            project.user.Password = EditorGUI.PasswordField(new Rect(100, 75, 200, 25), project.user.Password);

            if (GUI.changed)
            {
                editor.SaveProjectDataChanges();
            }

            GUI.enabled = true;

        }

        public void Subwindow_PermissionView()
        {
            GUI.Label(new Rect(0, 50, Screen.width, 50), "Project Permission Information");
            GUI.Label(new Rect(200, 50, 150, 50), "Read");
            GUI.Label(new Rect(250, 50, 150, 50), "Write");
        }

        public void Subwindow_SceneView()
        {

        }

        public void Subwindow_SettingsView()
        {
            GUI.enabled = TeamServerConnection.connected;
            GUI.Label(new Rect(0, 50, 200, 25), "Background TeamAssetLoader");
            if (GUI.Button(new Rect(200, 50, 100, 25), TeamSceneManager.state ? "Disable" : "Enable"))
            {
                if (TeamSceneManager.state)
                    TeamSceneManager.DeaktivateBackgroundTeamSceneLoader();
                else
                    TeamSceneManager.AktivateBackgroundTeamAssetImporter();
            }

            GUI.Label(new Rect(0, 100, 200, 25), "Background TeamAssetUploader");
            if (GUI.Button(new Rect(200, 100, 100, 25), TeamSceneManager.updater_state ? "Disable" : "Enable"))
            {
                if (TeamSceneManager.updater_state)
                    TeamSceneManager.DeactivateSceneAssetUploader();
                else
                    TeamSceneManager.ActivateSceneAssetUploader();
            }
            GUI.enabled = true;//reset GUI state to selectable


            if (GUI.Button(new Rect(0, 150, 200, 25), "Debug: " + isDebug))
                isDebug = !isDebug;
        }

        public void Subwindow_StatusView()
        {
            GUI.enabled = TeamServerConnection.connected;

            //if (TeamServerConnection.connected)
            //{
            if (TeamSceneManager.backgroundloaderLED)
                GUI.Box(new Rect(position.width - 25, 25, 25, 25), "");

            if (GUI.Button(new Rect(position.width - 100, 25, 100, 50), "Clear"))
                TeamSceneManager.TeamObjects.Clear();
            //}

            GUI.Label(new Rect(0, 25, 200, 25), "Sync TeamViews: " + TeamSceneManager.TeamObjects.Count);
            GUI.BeginGroup(new Rect(0, 50, 500, position.height - 100));
            TeamSceneManager.TeamObjects.ForEach(t =>
            {
                if (t != null)
                    GUILayout.Label(t.name + " " + t.gameObject.name);
            });
            GUI.EndGroup();
            GUI.enabled = true;

            GUI.Label(new Rect(0, position.height - 50, 500, 50), "Datapackets to compute: " + TeamSceneManager.incomingData.Count);
        }
    }
}