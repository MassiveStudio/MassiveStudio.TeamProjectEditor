﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

using System.Reflection;

using TeamProjectEditor.Manager;
using TeamProjectEditor.SystemUtility;
namespace TeamProjectEditor.EditorTools.Scene_
{
    [InitializeOnLoad]
    public class FastMenu
    {
        static Camera editorCamera;

        static Canvas editorCanvas;
        static bool set = false;

        public static FastSecectionMenuData data;

        public static Sprite MainCircle;
        public static Sprite FirstCircle;

        public static Sprite _2D;
        public static Sprite _3D;

        public static bool toggle;

        static FastMenu()
        {
            InitStyle();
            SceneView.onSceneGUIDelegate += OnScene;
            EditorEventSystem.OnDeserilization += OnDeserilization;
            //EditorApplication.update += Update;
            if (editorCanvas == null)
                set = false;
        }

        public static void OnDeserilization()
        {
            EditorEventSystem.OnDeserilization -= OnDeserilization;
            SceneView.onSceneGUIDelegate -= OnScene;
            GameObject.DestroyImmediate(editorCanvas);
            set = false;
        }

        public static void InitStyle()
        {
            if (MainCircle == null)
                MainCircle = RessourceManager.ToStprite(RessourceManager.MainCircle);

            if (FirstCircle == null)
                FirstCircle = RessourceManager.ToStprite(RessourceManager.MainCircle);

            if (_2D == null)
                _2D = RessourceManager.ToStprite(RessourceManager.FastSelect_2D);

            if (_3D == null)
                _3D = RessourceManager.ToStprite(RessourceManager.FastSelect_3D);

        }

        public static void GenerateEditorCanvas()
        {
            if (MainCircle == null)
                InitStyle();

            set = true;

            GameObject a = new GameObject("FastMenu");
            editorCanvas = a.AddComponent<Canvas>();
            editorCanvas.renderMode = RenderMode.ScreenSpaceCamera;
            editorCanvas.planeDistance = 1000f;

            GameObject first;
            GameObject second;
            GameObject third;

            GameObject temp;

            //Add Empty Objects to Canvas (sorting Layer)
            first = new GameObject("First CIRCLE");
            second = new GameObject("Second CIRCLE");
            third = new GameObject("Third CIRCLE");

            third.transform.parent = a.transform;
            second.transform.parent = a.transform;
            first.transform.parent = a.transform;

            third.transform.localPosition = Vector3.zero;
            second.transform.localPosition = Vector3.zero;
            first.transform.localPosition = Vector3.zero;


            //AddMain Circle
            GameObject main = new GameObject("MAIN CIRCLE");
            main.transform.parent = a.transform;
            main.transform.localPosition = new Vector2(0, 0);
            Image b = main.AddComponent<Image>();
            b.sprite = MainCircle;

            //First Circles
            for (int i = 0; i < 4; i++)
            {
                temp = new GameObject("First CIRCLE " + i);
                temp.transform.parent = first.transform;
                temp.transform.localPosition = new Vector2(0, 0);
                temp.transform.localScale = new Vector3(2, 2, 2);
                b = temp.AddComponent<Image>();
                b.sprite = FirstCircle;
                b.type = Image.Type.Filled;
                b.fillMethod = Image.FillMethod.Radial360;
                b.fillAmount = 1f / 4;
                b.color = Color.green;
                b.rectTransform.localRotation = Quaternion.Euler(new Vector3(0, 0, 360 / 4 * i));
            }


            //First Circles
            for (int i = 0; i < 10; i++)
            {
                temp = new GameObject("Second CIRCLE " + i);
                temp.transform.parent = second.transform;
                temp.transform.localPosition = new Vector2(0, 0);
                temp.transform.localScale = new Vector3(3, 3, 3);
                b = temp.AddComponent<Image>();
                b.sprite = FirstCircle;
                b.type = Image.Type.Filled;
                b.fillMethod = Image.FillMethod.Radial360;
                b.fillAmount = 1f / 10f;
                b.color = Color.red;
                b.rectTransform.localRotation = Quaternion.Euler(new Vector3(0, 0, 360 / 10f * i));
            }

            editorCanvas.worldCamera = editorCamera;
        }

        static Event e;
        static bool isset = false;
        static void OnScene(SceneView sceneView)
        {
            e = Event.current;

            if (SceneView.currentDrawingSceneView != null)
                editorCamera = SceneView.currentDrawingSceneView.camera;
            else
            {
                editorCamera = null;
                set = false;
            }//else
            //    editorCamera = SceneView.lastActiveSceneView.camera;

            if ((editorCamera != null && !set) || editorCanvas==null)
            {
                data = GenerateFastMenu(FastSelectionMenu.GameSceneMenu);
                //GenerateEditorCanvas();
            }   

            if (e.keyCode == KeyCode.LeftControl && e.type == EventType.KeyDown)
            {
                toggle = true;
                //e.Use();
            }
            else if (e.keyCode == KeyCode.LeftControl && e.type == EventType.KeyUp)
            {
                toggle = false;
                //e.Use();
            }

            if (editorCanvas != null)
            {
                if (toggle)
                {
                    if (!isset)
                        editorCanvas.gameObject.SetActive(true);
                    isset = true;
                    editorCanvas.gameObject.name = editorCanvas.gameObject.name;//Update it
                    Selection();

                    sceneView.Repaint();
                }
                else
                {
                    if (isset)
                        editorCanvas.gameObject.SetActive(false);
                    isset = false;
                }
            }
        }

        public static void Selection()
        {
            if (data != null)
            {
                Vector2 a = (new Vector2(e.mousePosition.x, Screen.height - e.mousePosition.y) - new Vector2(Screen.width / 2f, Screen.height / 2f));
                int circle_row = Mathf.FloorToInt(a.magnitude / 50f);
                if (circle_row < data.circles.Count)
                {

                    Vector2 b = (new Vector2(Screen.width / 2f, Screen.height / 2f - circle_row * 50f) - new Vector2(Screen.width / 2f, Screen.height / 2f));
                    double angle = AngleBetween(a, b);

                    if (angle < 0)
                        angle = 360f + angle;

                    //SKALARPRODUKT Mathf.Acos(a.x * b.x + a.y * b.y) / (Mathf.Sqrt(Mathf.Pow(a.x, 2) + Mathf.Pow(a.y, 2)) * Mathf.Sqrt(Mathf.Pow(b.x, 2) + Mathf.Pow(b.y, 2)));
                    //Debug.Log("ROW: " + circle_row+ ":" + a.magnitude + "  angle: " + angle);
                    //GUI.Box(new Rect(e.mousePosition, new Vector2(100, 100)), angle.ToString());

                    //Clear Last Ring
                    FastSecectionMenuCircleData ring;
                    bool changedrow = false;
                    if (data.currentSelectedCircle != circle_row)
                    {
                        changedrow = true;
                        ring = data.circles[data.currentSelectedCircle];
                        for (int i = 0; i < ring.buttons.Count; i++)
                            ring.buttons[i].obj.color = Color.white;
                        data.currentSelectedCircle = circle_row;
                    }

                    int sel = -1;

                    ring = data.circles[circle_row];
                    for (int i = 0; i < ring.buttons.Count; i++)
                        if (angle > ring.buttons[i].angle_start && angle < ring.buttons[i].angle_end || ring.buttons.Count == 1)
                        {
                            ring.buttons[i].obj.color = Color.yellow;
                            sel = i;
                        }
                        else
                            ring.buttons[i].obj.color = Color.white;

                    if ( sel != -1)//e.type == EventType.MouseDown && e.button == 0 &&
                    {
                        //Debug.Log("CLICKED");
                        if (data.currentSelectedButton != sel || changedrow)
                        {
                            data.currentSelectedButton = sel;
                            if (ring.buttons[sel].button != null && ring.buttons[sel].button.childs != null && ring.buttons[sel].button.childs.buttons != null && ring.buttons[sel].button.childs.buttons.Count > 0)
                            {
                                Debug.Log("Update Generate");
                                GenerateUpdateCircle(data, ring.buttons[sel].button.childs, MainCircle);
                            }
                        }
                        else if (e.type == EventType.MouseDown && e.button == 0 && ring.buttons[sel].button.action!= null)
                        {
                            Debug.Log("Try Invoke Action");
                            ring.buttons[sel].button.action();
                            toggle = false;
                            e.Use();

                        }

                        /*else
                        {
                            if (ring.buttons[sel].button != null && ring.buttons[sel].button.childs != null && ring.buttons[sel].button.childs.buttons != null && ring.buttons[sel].button.childs.buttons.Count > 0)
                            { }
                            else if (e.type == EventType.MouseDown && e.button == 0)
                            {
                                Debug.Log("Try Invoke Action");
                                ring.buttons[sel].button.action();
                                toggle = false;
                            }
                            e.Use();
                        }*/
                    }
                }
                //else
                //   Debug.Log("W! " + circle_row +"     " + a.ToString()+"    " +a.magnitude);
            }
        }

        public static double AngleBetween(Vector2 vector1, Vector2 vector2)
        {
            double sin = vector1.x * vector2.y - vector2.x * vector1.y;
            double cos = vector1.x * vector2.x + vector1.y * vector2.y;

            return Math.Atan2(sin, cos) * (180 / Math.PI);
        }

        void OnDestroy()
        {
            SceneView.onSceneGUIDelegate -= OnScene;
            GameObject.DestroyImmediate(editorCanvas);
        }

        public static void InstantiateObject(ObjectTye type)
        {
            switch (type)
            {
                case ObjectTye.Cube:
                    GameObject.CreatePrimitive(PrimitiveType.Cube);
                    break;
                case ObjectTye.Sphere:
                    GameObject.CreatePrimitive(PrimitiveType.Sphere);
                    break;
                case ObjectTye.Capsule:
                    GameObject.CreatePrimitive(PrimitiveType.Capsule);
                    break;
                case ObjectTye.Cylinder:
                    GameObject.CreatePrimitive(PrimitiveType.Cylinder);
                    break;
                case ObjectTye.Plane:
                    GameObject.CreatePrimitive(PrimitiveType.Plane);
                    break;
                case ObjectTye.Quad:
                    GameObject.CreatePrimitive(PrimitiveType.Quad);
                    break;
                case ObjectTye.Terrain:
                    TerrainData data = new TerrainData();
                    Terrain t = new GameObject("Terrain").AddComponent<Terrain>();
                    t.terrainData = data;
                    t.gameObject.AddComponent<TerrainCollider>().terrainData = data;
                    break;
                case ObjectTye.Canvas:
                    break;
                case ObjectTye.Image:
                    break;
                case ObjectTye.Button:
                    break;
                case ObjectTye.Toggle:
                    break;
                case ObjectTye.Panel:
                    break;
                case ObjectTye.Directional:
                    new GameObject("DirectionalLight").AddComponent<Light>().type = LightType.Directional;
                    break;
                case ObjectTye.Point:
                    new GameObject("PointLight").AddComponent<Light>().type = LightType.Point;
                    break;
                case ObjectTye.Spot:
                    new GameObject("SpotLight").AddComponent<Light>().type = LightType.Spot;
                    break;
                case ObjectTye.Area:
                    new GameObject("AreaLight").AddComponent<Light>().type = LightType.Area;
                    break;
                default:
                    break;
            }
        }

        public static FastSecectionMenuData GenerateFastMenu(FastSelectionMenu menu)
        {
            if (PlayerPrefs.HasKey("FastMenu"))
            {
                int id = PlayerPrefs.GetInt("FastMenu");
                GameObject o = EditorUtility.InstanceIDToObject(id) as GameObject;
                if (o != null)
                    if (o.name == "FastMenu")
                        GameObject.DestroyImmediate(o);
            }

            //Generate Basics
            if (MainCircle == null)
                InitStyle();
            set = true;
            GameObject a = new GameObject("FastMenu");
            PlayerPrefs.SetInt("FastMenu", ReflectDllMethods.GetInstanceID(a));
            a.hideFlags = HideFlags.DontSave | HideFlags.NotEditable;
            //a.hideFlags =  HideFlags.DontSave;//HideFlags.HideInHierarchy | HideFlags.HideInInspector |
            editorCanvas = a.AddComponent<Canvas>();
            
            editorCanvas.renderMode = RenderMode.ScreenSpaceCamera;
            editorCanvas.planeDistance = 1;

            FastSecectionMenuData data = new FastSecectionMenuData(editorCanvas);

            //GameObject row1 = new GameObject("1");
            //GameObject row2 = new GameObject("2");

            //data.objs.Add(row1);
            //data.objs.Add(row2);

            //Reverse Order -> Overdraw
            //row2.transform.parent = a.transform;
            //row1.transform.parent = a.transform;

            //row1.transform.localPosition = Vector3.zero;
            //row1.transform.localScale = Vector3.one;
            GenerateUpdateCircle(data, menu.circles[0], MainCircle);

            //row2.transform.localPosition = Vector3.zero;
            //row2.transform.localScale = Vector3.one;
            GenerateUpdateCircle(data, menu.circles[1], MainCircle);

            editorCanvas.worldCamera = editorCamera;
            return data;
        }

        public static void GenerateUpdateCircle(FastSecectionMenuData Menudata, FastSelectionCircle circle, Sprite background, GameObject[] objects = null)
        {
            //Debug.Log("GERATE RING" + circle.row);
            GameObject parent;
            FastSecectionMenuCircleData data = new FastSecectionMenuCircleData();
            GameObject temp;
            GameObject child;
            Image b;
            Image b2;
            float angle;
            float angle_self;
            float space = 0f;
            Vector2 size;
            float textcenter;
            // Debug.Log("Circle " + circle.row + " Items: " + circle.buttons.Count);

            angle_self = 360f / circle.buttons.Count;
            size = new Vector2(120, 120) * circle.row;
            textcenter = (size.x / 2f + size.x / 4f) / 1.9f;
            space = (360f / circle.buttons.Count) * 0.05f / 2f;


            if (Menudata.objs.Count < circle.row)
            {
                //Debug.Log("Create New TOP OBJECT");
                parent = new GameObject(circle.row.ToString());
                parent.transform.parent = Menudata.canvas.transform;
                parent.hideFlags = HideFlags.HideInHierarchy | HideFlags.HideInInspector | HideFlags.HideAndDontSave;
                //New GameObjects added to first pos -> cause of Overdraw&&Controll
                parent.transform.SetSiblingIndex(0);
                parent.transform.localPosition = Vector3.zero;
                parent.transform.localScale = Vector3.one;
                parent.transform.localRotation = Quaternion.identity;
                Menudata.objs.Add(parent);
            }
            else
            {
                //Debug.Log("Load existing TOP OBJECT");
                parent = Menudata.objs[(circle.row - 1)];

                int m = parent.transform.childCount;
                for (int i = 0; i < m; i++)
                {
                    //Debug.Log("CHILD " + i + "  " + parent.transform.GetChild(0).name);
                    GameObject.DestroyImmediate(parent.transform.GetChild(0).gameObject);
                }
                /*
                while(parent.transform.childCount>0)
                {
                    //Menudata.objs.Remove(tt.gameObject);
                    GameObject.DestroyImmediate(parent.transform.GetChild(0));
                }*/
                Menudata.circles.RemoveAt(circle.row - 1);

            }

            if (Menudata.circles.Count > circle.row)//Clear Data if Neeed
            {
                //Debug.Log("Clear Child");
                //Remove CurrentCircleData with Obj
               /* while (Menudata.objs[(circle.row - 1)].transform.childCount > 0)
                {

                    GameObject.DestroyImmediate(Menudata.objs[(circle.row - 1)].transform.GetChild(0));
                }*/
            }


            if(parent.GetComponent<RectTransform>()==null)
                parent.AddComponent<RectTransform>().sizeDelta = size;
            else
                parent.GetComponent<RectTransform>().sizeDelta = size;

            if (parent.GetComponent<Image>() == null)
                parent.AddComponent<Image>().sprite = background;
            else
                parent.GetComponent<Image>().sprite = background;
            parent.GetComponent<Image>().color = Color.grey;


            for (int i = 0; i < circle.buttons.Count; i++)
            {
                angle = angle_self * i;
                if (objects != null && objects.Length > i)
                {
                    temp = objects[i];
                    b = temp.GetComponent<Image>();
                    objects[i].SetActive(true);
                    child = temp.transform.GetChild(0).gameObject;
                }
                else
                {
                    temp = new GameObject(circle.row + " CIRCLE " + i + "  " + circle.buttons[i].info);
                    temp.hideFlags = HideFlags.DontSaveInEditor | HideFlags.NotEditable | HideFlags.HideInHierarchy | HideFlags.HideInInspector;
                    temp.transform.parent = parent.transform;
                    temp.transform.localPosition = new Vector2(0, 0);
                    temp.transform.localRotation = Quaternion.identity;
                    temp.transform.transform.localScale = Vector3.one;// new Vector3(circle.row, circle.row, circle.row);
                    b = temp.AddComponent<Image>();
                    b.rectTransform.sizeDelta = size;

                    child = new GameObject("image " + (180f + (angle_self / 2f)));
                    child.hideFlags = HideFlags.DontSaveInEditor | HideFlags.NotEditable | HideFlags.HideInHierarchy | HideFlags.HideInInspector;
                }

                if (circle.row > 1)
                {
                    child.transform.parent = temp.transform;
                    child.transform.localPosition = new Vector3(0, -textcenter, 0);// new Vector3(Mathf.Cos((180f + angle_self / 2f) * Mathf.Deg2Rad) * textcenter, Mathf.Sin((180f + angle_self / 2f) * Mathf.Deg2Rad) * textcenter, 0);
                    child.transform.localPosition = Quaternion.Euler(0, 0, -angle_self / 2f) * child.transform.localPosition;

                    Vector2 dir = child.transform.localPosition;
                    dir = dir / dir.magnitude * textcenter;
                    child.transform.localPosition = dir;
                    child.transform.localScale = Vector3.one;
                    //child.transform.localRotation = Quaternion.identity;
                    //child.transform.localRotation = Quaternion.Euler(0, 0, angle_self / 2f);
                }
                else
                {
                    child.transform.parent = temp.transform;
                    child.transform.localPosition = Vector3.zero;
                    child.transform.localScale = Vector3.one;
                    //child.transform.localRotation = Quaternion.identity;
                    //child.transform.localRotation = Quaternion.Euler(0, 0, angle_self / 2f);

                }

                b.rectTransform.localRotation = Quaternion.Euler(new Vector3(0, 0, 360 - (angle - space)));

                b.sprite = MainCircle;
                if (circle.row > 1)
                {
                    b.type = Image.Type.Filled;
                    b.fillMethod = Image.FillMethod.Radial360;
                    b.fillAmount = (1f / circle.buttons.Count) - (1f / 360f * space);
                }
                b2 = child.AddComponent<Image>();
                b2.sprite = circle.buttons[i].tex;
                b2.rectTransform.sizeDelta = new Vector2(45, 45);


                if (circle.row>1)
                    child.transform.localEulerAngles = new Vector3(0,0, angle + angle_self / 2f  -45f);

                data.buttons.Add(new FastSecectionMenuButtonData(b, angle, (angle + angle_self), circle.buttons[i]));
            }

            //Debug.Log("Circle GENERATED");

            if(objects!=null)
                for (int i = circle.buttons.Count; i < objects.Length; i++)
                    objects[i].SetActive(false);

            //Debug.Log("Circle DISABLED");

            if (Menudata.circles.Count >= circle.row)//Update
            {
                Menudata.objs[circle.row].SetActive(true);
                for (int i=(circle.row); i< Menudata.objs.Count; i++)
                    Menudata.objs[i].SetActive(false);

                Menudata.circles.RemoveAt(circle.row - 1);
                Menudata.circles.Add(data);
            }
            else
                Menudata.circles.Add(data);
        }
    }

    public enum ObjectTye
    {
        //3D
        Cube,
        Sphere,
        Capsule,
        Cylinder,
        Plane,
        Quad,
        Terrain,
        //2D
        Canvas,
        Image,
        Button,
        Toggle,
        Panel,
        //Light
        Directional,
        Spot,
        Area,
        Point
    }

    [System.Serializable]
    public class FastSelectionMenu
    {
        public List<FastSelectionCircle> circles;

        public FastSelectionMenu()
        {
            circles = new List<FastSelectionCircle>();
        }

        private static FastSelectionMenu _GameSceneMenu;

        public static FastSelectionMenu GameSceneMenu
        {
            get
            {
                if (_GameSceneMenu != null && _GameSceneMenu.circles[0].buttons[0].tex!=null)
                    return _GameSceneMenu;

                FastSelectionMenu menu = new FastSelectionMenu();

                #region Third Circle Lights - Objects
                FastSelectionCircle circle_Light = new FastSelectionCircle(3);
                circle_Light.buttons.Add(new FastSelectionButton(RessourceManager.DirectionalLight, new Action(() => { FastMenu.InstantiateObject(ObjectTye.Directional); }), "Directional"));
                circle_Light.buttons.Add(new FastSelectionButton(RessourceManager.AreaLight, new Action(() => { FastMenu.InstantiateObject(ObjectTye.Area); }), "Area"));
                circle_Light.buttons.Add(new FastSelectionButton(RessourceManager.SpotLight, new Action(() => { FastMenu.InstantiateObject(ObjectTye.Spot); }), "Spot"));
                circle_Light.buttons.Add(new FastSelectionButton(RessourceManager.PointLight, new Action(() => { FastMenu.InstantiateObject(ObjectTye.Point); }), "Point"));
                #endregion

                #region Third Circle 3D - Objects
                FastSelectionCircle circle_3D = new FastSelectionCircle(3);
                circle_3D.buttons.Add(new FastSelectionButton(RessourceManager.Cube, new Action(() => { FastMenu.InstantiateObject(ObjectTye.Cube); }), "Cube"));
                circle_3D.buttons.Add(new FastSelectionButton(RessourceManager.Sphere, new Action(() => { FastMenu.InstantiateObject(ObjectTye.Sphere); }), "Sphere"));
                circle_3D.buttons.Add(new FastSelectionButton(RessourceManager.Cylinder, new Action(() => { FastMenu.InstantiateObject(ObjectTye.Cylinder); }), "Cylinder"));
                circle_3D.buttons.Add(new FastSelectionButton(RessourceManager.Capsule, new Action(() => { FastMenu.InstantiateObject(ObjectTye.Capsule); }), "Capsule"));
                circle_3D.buttons.Add(new FastSelectionButton(RessourceManager.Quad, new Action(() => { FastMenu.InstantiateObject(ObjectTye.Quad); }), "Quad"));
                circle_3D.buttons.Add(new FastSelectionButton(RessourceManager.MainCircle, new Action(() => { FastMenu.InstantiateObject(ObjectTye.Plane); }), "Plane"));
                circle_3D.buttons.Add(new FastSelectionButton(RessourceManager.Terrain, new Action(() => { FastMenu.InstantiateObject(ObjectTye.Terrain); }), "Terrain"));
                #endregion

                #region Third Circle 2D - Objects
                FastSelectionCircle circle_2D = new FastSelectionCircle(3);
                circle_2D.buttons.Add(new FastSelectionButton(RessourceManager.UI_Canvas, new Action(() => { FastMenu.InstantiateObject(ObjectTye.Canvas); }), "Canvas"));
                circle_2D.buttons.Add(new FastSelectionButton(RessourceManager.UI_Image, new Action(() => { FastMenu.InstantiateObject(ObjectTye.Image); }), "Image"));
                circle_2D.buttons.Add(new FastSelectionButton(RessourceManager.UI_Button, new Action(() => { FastMenu.InstantiateObject(ObjectTye.Button); }), "Button"));
                circle_2D.buttons.Add(new FastSelectionButton(RessourceManager.UI_Toggle, new Action(() => { FastMenu.InstantiateObject(ObjectTye.Toggle); }), "Toggle"));
                circle_2D.buttons.Add(new FastSelectionButton(RessourceManager.UI_Panel, new Action(() => { FastMenu.InstantiateObject(ObjectTye.Panel); }), "Panel"));
                #endregion

                #region Second Circle 2D 3D Light
                FastSelectionCircle circle_R2 = new FastSelectionCircle(2);
                circle_R2.buttons.Add(new FastSelectionButton(RessourceManager.FastSelect_3D, null, "3D", circle_3D));
                circle_R2.buttons.Add(new FastSelectionButton(RessourceManager.FastSelect_2D, null, "2D", circle_2D));
                circle_R2.buttons.Add(new FastSelectionButton(RessourceManager.FastSelect_Light, null, "Light", circle_Light));
                //circle_R2.buttons.Add(new FastSelectionButton(RessourceManager.MainCircle, null, "Light", circle_2D));
                #endregion

                #region First Circle Back
                FastSelectionCircle circle = new FastSelectionCircle(1);
                circle.buttons.Add(new FastSelectionButton(RessourceManager.Back, new Action(() => { Debug.Log(""); }), "back"));
                #endregion

                menu.circles.Add(circle);
                menu.circles.Add(circle_R2);
                menu.circles.Add(circle_3D);
                menu.circles.Add(circle_2D);

                //Cache
                _GameSceneMenu = menu;
                return menu;
            }
        }
    }

    [System.Serializable]
    public class FastSecectionMenuData
    {
        public List<FastSecectionMenuCircleData> circles;
        public int currentSelectedCircle = 0;
        public int currentSelectedButton = 0;
        public List<GameObject> objs;
        public Canvas canvas;

        public FastSecectionMenuData(Canvas canvas)
        {
            circles = new List<FastSecectionMenuCircleData>();
            objs = new List<GameObject>();
            this.canvas = canvas;
        }
    }

    [System.Serializable]
    public class FastSecectionMenuCircleData
    {
        public List<FastSecectionMenuButtonData> buttons;
        public FastSecectionMenuCircleData()
        {
            buttons = new List<FastSecectionMenuButtonData>();
        }
    }

    [System.Serializable]
    public class FastSecectionMenuButtonData
    {
        public Image obj;
        public float angle_start;
        public float angle_end;
        public FastSelectionButton button;

        public FastSecectionMenuButtonData(Image obj, float angle_start, float angle_end, FastSelectionButton button)
        {
            this.obj = obj;
            this.angle_end = angle_end;
            this.angle_start = angle_start;
            this.button = button;
        }
    }

    [System.Serializable]
    public class FastSelectionCircle
    {
        public List<FastSelectionButton> buttons;
        public int row;

        public FastSelectionCircle(int id)
        {
            buttons = new List<FastSelectionButton>();
            row = id;
        }
    }

    [System.Serializable]
    public class FastSelectionButton
    {
        public Sprite tex;
        public System.Action action;
        public string info;
        public FastSelectionCircle childs;

        public FastSelectionButton(Sprite sprite, Action t, string title, FastSelectionCircle childs = null)
        {
            this.tex = sprite;
            this.action = t;
            this.info = title;
            this.childs =childs;
        }

        public FastSelectionButton(Texture2D sprite, Action t, string title, FastSelectionCircle childs = null)
        {
            this.tex = Sprite.Create(sprite, new Rect(0,0,sprite.width,sprite.height), new Vector2(0.5f, 0.5f));
            this.action = t;
            this.info = title;
            this.childs = childs;
        }
    }
}