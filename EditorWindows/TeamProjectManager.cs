﻿using UnityEngine;
using System.Collections;

namespace MassiveStudio.TeamProjectEditor
{
    internal static class TeamProjectManager
    {
        public static TeamProject currentProject = null;

        public static void OpenTeamProject(TeamProject p)
        {
            currentProject = p;
        }
    }
}