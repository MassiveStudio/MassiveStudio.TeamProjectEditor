﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor.EditorWindows.Design.SkinEditor.GUISkinStyles
{
    [Serializable]
    internal class GUISkinEditorStyle_Add : GUISkinEditorStyle
    {
        public override void Init()
        {
            this.type = GUISkinStyleType.Box;
            this.style = skin.box;
            this.guistylename = "+";
        }

        public override void OnGUI()
        {
            GUI.BeginGroup(new Rect(0, 0, 200, 200));
            GUI.Box(new Rect(25,25,150,50), "+", style);
            //GUI.Label(new Rect(0, 150, 200, 50), type.ToString(), TypeNameStyle);
            GUI.EndGroup();
        }

        public override void OnPreview(GUIStyle _style, int interactible = 0) { }
    }
}
