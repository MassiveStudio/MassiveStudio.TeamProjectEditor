﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor.EditorWindows.Design.SkinEditor.GUISkinStyles
{
    [Serializable]
    class GUISkinEditorStyle_TextField : GUISkinEditorStyle
    {
        public override void Init()
        {
            this.type = GUISkinStyleType.TextField;
            this.style = skin.textField;
            this.guistylename = "textField";
        }

        public override void OnGUI()
        {
            GUI.BeginGroup(new Rect(0, 0, 200, 200));
            GUI.TextField(new Rect(25, 25, 150, 50), "Text", style);
            GUI.Label(new Rect(0, 150, 200, 50), type.ToString(), TypeNameStyle);
            GUI.EndGroup();
        }

        public override void OnPreview(GUIStyle _style, int interactible = 0)
        {
            GUI.TextField(new Rect(0, 0, 150, 50), "Text", _style);
        }
    }
}