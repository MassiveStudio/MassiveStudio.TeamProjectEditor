﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor.EditorWindows.Design.SkinEditor.GUISkinStyles
{
    [Serializable]
    internal class GUISkinEditorStyle_Box : GUISkinEditorStyle
    {
        public override void Init()
        {
            this.type = GUISkinStyleType.Box;
            this.style = skin.box;
            this.guistylename = "box";

            /*
            var field = typeof(GUI).GetField("s_BoxHash", BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly | BindingFlags.NonPublic);
            var value = (int)field.GetValue(null);
            Debug.Log("VAL- " + value);*/
        }

        public override void OnGUI()
        {
            GUI.BeginGroup(new Rect(0, 0, 200, 200));
            GUI.Box(new Rect(25,25,150,50), "Text", style);
            GUI.Label(new Rect(0, 150, 200, 50), type.ToString(), TypeNameStyle);
            GUI.EndGroup();
        }

        public override void OnPreview(GUIStyle _style, int interactible = 0)
        {
            //if(state.background != null)
            // GUI.Box(new Rect(0, 0, 200, 50), state.background, _style);
            //else
            GUI.Box(new Rect(0, 0, 150, 50), "Text", _style);
            /*GUI.color = state.textColor;
            GUI.Label(new Rect(0, 0, 200, 50), "Text", _style);
            GUI.color = Color.white;*/
        }
        /*
        public static void Box(Rect position, GUIContent content, GUIStyle style)
        {
            int s_BoxHash = 66987;
            int controlID = GUIUtility.GetControlID(s_BoxHash, FocusType.Passive);
            if (Event.current.type == EventType.Repaint)
            {
                style.Draw(position, content, controlID);
            }
        }*/
    }
}
