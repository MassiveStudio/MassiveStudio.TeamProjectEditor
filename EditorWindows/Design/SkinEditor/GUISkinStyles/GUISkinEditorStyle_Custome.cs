﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor.EditorWindows.Design.SkinEditor.GUISkinStyles
{
    [Serializable]
    class GUISkinEditorStyle_Custome : GUISkinEditorStyle
    {
        public int customeStyleIndex=0;

        public override void Init()
        {
            this.type = GUISkinStyleType.CustomeStyle;
            this.iscustome = true;
            if (skin == null)
                throw new System.NullReferenceException("SKIN NULL");
            this.style = skin.customStyles[customeStyleIndex];
        }

        public override void OnGUI()
        {
            GUI.BeginGroup(new Rect(0, 0, 200, 200));
            GUI.Button(new Rect(25, 25, 150, 50), "Text", style);
            GUI.Label(new Rect(0, 150, 200, 50), this.style.name, TypeNameStyle);
            GUI.EndGroup();
        }

        public override void OnPreview(GUIStyle _style, int interactible = 0)
        {
            GUI.Button(new Rect(0, 0, 150, 50), "Text", style);
        }
    }
}