﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor.EditorWindows.Design.SkinEditor.GUISkinStyles
{
    [Serializable]

    class GUISkinEditorStyle_Label : GUISkinEditorStyle
    {
        public override void Init()
        {
            this.type = GUISkinStyleType.Label;
            this.style = skin.label;
            this.guistylename = "label";
        }

        public override void OnGUI()
        {
            GUI.BeginGroup(new Rect(0, 0, 200, 200));
            GUI.Label(new Rect(25, 25, 150, 50), "Text", style);
            GUI.Label(new Rect(0, 150, 200, 50), type.ToString(), TypeNameStyle);
            GUI.EndGroup();
        }

        public override void OnPreview(GUIStyle _style, int interactible = 0)
        {
            GUI.color = new Color(1, 1, 1, 0.2f);
            GUI.Box(new Rect(0, 0, 150, 50), "");
            GUI.color = Color.white;
            GUI.Label(new Rect(0, 0, 150, 50), "Text", _style);
        }
    }
}

