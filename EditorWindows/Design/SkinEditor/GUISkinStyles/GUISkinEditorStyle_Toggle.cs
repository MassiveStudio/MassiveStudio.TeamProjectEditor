﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


namespace MassiveStudio.TeamProjectEditor.EditorWindows.Design.SkinEditor.GUISkinStyles
{
    [Serializable]
    class GUISkinEditorStyle_Toggle : GUISkinEditorStyle
    {
        [SerializeField]
        GUIStyle TogglePreviewStyle;
        [SerializeField]
        GUIStyle ToggleLabelPreviewStyle;

        [SerializeField]
        private bool temp;
        public override void Init()
        {
            this.type = GUISkinStyleType.Toggle;
            this.style = skin.toggle;
            this.guistylename = "Toggle";

            TogglePreviewStyle = new GUIStyle("Toggle");
            ToggleLabelPreviewStyle = new GUIStyle("label");

            //TogglePreviewStyle.border = new RectOffset(0, 0, 0, 0);
            //TogglePreviewStyle.overflow = new RectOffset(0, 0, 0, 0);
            //TogglePreviewStyle.imagePosition = ImagePosition.ImageOnly;
            //TogglePreviewStyle.padding = new RectOffset(50, 0, 50, 0);

            ToggleLabelPreviewStyle.alignment = TextAnchor.MiddleLeft;
        }

        public override void OnGUI()
        {
            /*GUI.BeginGroup(new Rect(0, 0, 200, 200));
            GUI.Toggle(new Rect(25, 25, 50, 50), true, "", TogglePreviewStyle);
            GUI.Label(new Rect(75, 25, 100, 50), "Text", ToggleLabelPreviewStyle);
            GUI.Label(new Rect(0, 150, 200, 50), type.ToString(), TypeNameStyle);
            GUI.EndGroup();*/

            GUI.BeginGroup(new Rect(0, 0, 200, 200));
            temp = GUI.Toggle(new Rect(25, 25, 150, 50), true, "Text", style);
            GUI.Label(new Rect(0, 150, 200, 50), type.ToString(), TypeNameStyle);
            GUI.EndGroup();
        }

        public override void OnPreview(GUIStyle _style, int interactible = 0)
        {
            if(interactible==1)
                temp = GUI.Toggle(new Rect(0, 0, 150, 50), temp, "Text", _style);
            else if(interactible==0)
                GUI.Toggle(new Rect(0, 0, 150, 50), true, "Text", _style);
            else
                GUI.Toggle(new Rect(0, 0, 150, 50), false, "Text", _style);

        }
    }
}

