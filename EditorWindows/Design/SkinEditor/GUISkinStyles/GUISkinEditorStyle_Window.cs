﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor.EditorWindows.Design.SkinEditor.GUISkinStyles
{
    [Serializable]
    class GUISkinEditorStyle_Window : GUISkinEditorStyle
    {
        public override void Init()
        {
            this.type = GUISkinStyleType.Window;
            this.style = skin.window;
            this.guistylename = "window";
            this.iswindow = true;
        }

        public override void OnGUI()
        {
            GUI.BeginGroup(new Rect(0, 0, 200, 200));
            GUI.Window(0, new Rect(25, 25, 150, 125), WindowGUI, "Title");
            GUI.Label(new Rect(0, 150, 200, 50), type.ToString(), TypeNameStyle);
            GUI.EndGroup();
        }

        public void WindowGUI(int id) {
            //GUI.Box(new Rect(0, 0, 50, 50), " Test");
        }

        public override void OnPreview(GUIStyle _style, int interactible = 0)
        {
            GUI.Window(0, new Rect(0, 0, 150, 50),WindowGUI, "Title", _style);
        }
    }
}