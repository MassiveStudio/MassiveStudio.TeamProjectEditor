﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

using MassiveStudio.TeamProjectEditor.EditorWindows.Design.SkinEditor;

namespace MassiveStudio.TeamProjectEditor.EditorWindows.Design.SkinEditor.GUISkinStyles
{
    [Serializable]
    internal abstract class GUISkinEditorStyle : ScriptableObject
    {
        public GUISkin skin;
        public GUIStyle style;
        public bool basestyle = false;
        public bool iswindow = false;
        public bool iscustome = false;
        public GUIStyle TypeNameStyle;

        public GUISkinStyleType type;

        public string guistylename;

        public void InitStyle()
        {
            //Debug.Log("INIT STYLE");
            TypeNameStyle = new GUIStyle();
            TypeNameStyle.alignment = TextAnchor.MiddleCenter;
            TypeNameStyle.fontSize = 24;
        }

        public abstract void OnGUI();

        public abstract void Init();

        public abstract void OnPreview(GUIStyle _style, int interactible = 0);

    }
}