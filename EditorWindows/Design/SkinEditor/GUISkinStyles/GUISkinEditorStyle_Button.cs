﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor.EditorWindows.Design.SkinEditor.GUISkinStyles
{
    [Serializable]
    class GUISkinEditorStyle_Button : GUISkinEditorStyle
    {
        public override void Init()
        {
            this.type = GUISkinStyleType.Button;
            this.style = skin.button;
            this.guistylename = "Button";
        }

        public override void OnGUI()
        {
            GUI.BeginGroup(new Rect(0, 0, 200, 200));
            GUI.Button(new Rect(25, 25, 150, 50), "Text", style);
            GUI.Label(new Rect(0, 150, 200, 50), type.ToString(), TypeNameStyle);
            GUI.EndGroup();
        }
        public override void OnPreview(GUIStyle _style, int interactible = 0)
        {
            GUI.Button(new Rect(0, 0, 150, 50), "Text", _style);
        }
    }
}
