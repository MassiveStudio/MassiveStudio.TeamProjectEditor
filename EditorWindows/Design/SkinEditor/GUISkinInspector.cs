﻿using UnityEngine;
using UnityEditor;

namespace MassiveStudio.TeamProjectEditor.EditorWindows.Design.SkinEditor
{
    [CustomEditor(typeof(GUISkin))]
    public class GUISkinInspector : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {

        }

    }
}