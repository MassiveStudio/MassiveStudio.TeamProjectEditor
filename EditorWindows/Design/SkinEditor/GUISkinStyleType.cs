﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MassiveStudio.TeamProjectEditor.EditorWindows.Design.SkinEditor
{
    public enum GUISkinStyleType
    {
        Box = 0,
        Button = 1,
        Toggle = 3,
        Label = 4,
        TextField = 5,
        TextArea = 6,
        Window = 7,
        HorizontalSlider = 8,
        HorizontalSliderThumb = 9,
        VerticalSlider = 10,
        VerticalSliderThumb = 11,
        HorizontalScrollbar = 12,
        HorizontalScrollbarThumb = 13,
        HorizontalScrollbarLeftButton = 14,
        HorizontalScrollbarRightButton = 15,
        VerticalScrollbar = 16,
        VerticalScrollbarThumb = 17,
        VerticalScrollbarUpButton = 18,
        VerticalScrollbarDownButton = 19,
        ScrollView = 20,
        CustomeStyle = 21
    };
}
