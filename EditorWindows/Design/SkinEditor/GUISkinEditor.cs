﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using Microsoft.Win32;
using System;
using System.Text;
using System.IO;
using System.Linq;

using MassiveStudio.TeamProjectEditor.EditorWindows.Design.SkinEditor.GUISkinStyles;
using MassiveStudio.TeamProjectEditor.Utility;

namespace MassiveStudio.TeamProjectEditor.EditorWindows.Design.SkinEditor
{
    internal class GUISkinEditor : EditorWindow
    {
        public static GUISkinEditor window;
        public static Font[] fonts;
        public static List<string> fontnames = new List<string>();
        [SerializeField]
        public static List<GUISkinEditorStyle> styles = new List<GUISkinEditorStyle>();

        public int selectedmainfont = 0;
        [SerializeField]
        public GUISkinEditorStyle selectestylemenu = null;
        public GUISkin selectedSkin = null;
        public float selectedStyleScroll = 0f;


        [SerializeField]
        public int menu = 0;
        private Event e;

        [SerializeField]
        private static GUIStyle TitleStyle;


        private static Texture2D GuiArrow;
        private static Texture2D GuiArrowRight;

        [SerializeField]
        private GUIStyle temp_activeStyle;
        [SerializeField]
        private GUIStyle temp_hoverstyle;
        [SerializeField]
        private GUIStyle temp_normalStyle;
        [SerializeField]
        private GUIStyle temp_focusedStyle;
        [SerializeField]
        private GUIStyle temp_onactiveStyle;
        [SerializeField]
        private GUIStyle temp_onhoverstyle;
        [SerializeField]
        private GUIStyle temp_onnormalStyle;
        [SerializeField]
        private GUIStyle temp_onfocusedStyle;

        [MenuItem("MassiveStudio/GUISkinEditor")]
        static void Init()
        {
            window = (GUISkinEditor)GetWindow<GUISkinEditor>();
            window.titleContent = new GUIContent("GUISkinEditor");
            window.minSize = new Vector2(300, 480);


            TitleStyle = new GUIStyle();
            TitleStyle.alignment = TextAnchor.MiddleCenter;
            TitleStyle.fontSize = 20;

            if(Selection.activeObject!=null)
                if(Selection.activeObject.GetType() == typeof(GUISkin))
                {
                    window.OnSelectionChange();
                }

            fontnames.Clear();

            fonts = FindObjectsOfType<Font>();
            foreach (Font a in fonts)
                fontnames.Add(a.name);
            // fontnames.AddRange(Font.GetOSInstalledFontNames());

            GuiArrow = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.GUI_Arrow);
            GuiArrowRight = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.GUI_Arrow_Right);

            ReloadStyleSheet();

            window.menu = 0;
        }

        public static void ReloadStyleSheet()
        {
            if (styles == null)
                styles = new List<GUISkinEditorStyle>();
            else
                styles.Clear();

            GUISkinEditorStyle st;
            st = CreateInstance<GUISkinEditorStyle_Box>();
            styles.Add(st);

            st = CreateInstance<GUISkinEditorStyle_Button>();
            styles.Add(st);

            st = CreateInstance<GUISkinEditorStyle_Toggle>();
            styles.Add(st);

            st = CreateInstance<GUISkinEditorStyle_Label>();
            styles.Add(st);

            st = CreateInstance<GUISkinEditorStyle_TextField>();
            styles.Add(st);

            st = CreateInstance<GUISkinEditorStyle_TextArea>();
            styles.Add(st);

            st = CreateInstance<GUISkinEditorStyle_Window>();
            styles.Add(st);

            GUISkinEditorStyle_Custome st1;
            if (window.selectedSkin!=null)
                for(int i=0; i< window.selectedSkin.customStyles.Length; i++)
                {
                    st1 = CreateInstance<GUISkinEditorStyle_Custome>();
                    st1.InitStyle();
                    st1.skin = window.selectedSkin;
                    st1.customeStyleIndex = i;
                    styles.Add(st1 as GUISkinEditorStyle);
                }

            st = CreateInstance<GUISkinEditorStyle_Add>();
            styles.Add(st);


            if (window.selectedSkin== null)
                foreach (GUISkinEditorStyle a in styles)
                a.InitStyle();
            else
            {
                foreach (GUISkinEditorStyle a in styles)
                {
                    a.InitStyle();
                    a.skin = window.selectedSkin;
                    a.Init();
                }
            }
        }

        public void Update()
        {

            if (EditorWindow.focusedWindow == window)
                Repaint();
        }

        /*
        public void InitStyleElements(System.Type type)
        {
            GUISkinEditorStyle st = (GUISkinEditorStyle)CreateInstance(type);
            styles.Add(st);
            st.InitStyle();
        }
        */

        public void OnSelectionChange()
        {
            if (Selection.objects.Length > 0)
                if (Selection.objects[0].GetType() == typeof(UnityEngine.GUISkin))
                {
                    Debug.Log("SKIN  SELECTED");
                    selectedSkin = (GUISkin)Selection.objects[0];
                    menu = 0;

                    if(window==null)
                        Init();

                    ReloadStyleSheet();
                }
                else
                    selectedSkin = null;

            Repaint();
        }

        public void OnGUI()
        {
            e = Event.current;
            GUI.BeginGroup(new Rect(0, 0, position.width, 25f));
            DrawHeader();
            GUI.EndGroup();

            if (selectedSkin != null)
            {
                if (menu == 0)
                    MainStyleView();
                else if (menu == 1)
                {
                    GUI.BeginGroup(new Rect(25f, 25f, position.width - 50, position.height - 50f));
                    StyleTypeView();
                    GUI.EndGroup();
                }

            }
            else
                GUI.Box(new Rect(0, 0, position.width - 50, position.height - 50), "NO GUISKIN SELCTED");

            GUI.Window(0, new Rect(200, 300, 300, 200), GUIWindow, "Title");

        }

        public void GUIWindow(int id)
        {

        }

        public void MainStyleView()
        {
            if (GuiArrowRight == null || GuiArrow == null)
                Init();

            Rect r;
            int x = 0;
            int y = 0;

            GUI.Button(new Rect(0, window.position.height / 2 - 50, 50, 100), GuiArrow);
            GUI.Button(new Rect(window.position.width - 50, window.position.height / 2 - 50, 50, 100), GuiArrowRight);

            GUI.BeginGroup(new Rect(50f, 25f, position.width - 100, position.height - 25));
            GUISkinEditorStyle s;

            for (int i = 0; i < styles.Count; i++)
            {
                s = styles[i];
                //s.skin = selectedSkin;
                //s.Init();
                if (x + 200 > window.position.width - 50f)
                {
                    x = 0;
                    y += 200;
                }

                if (y + 200 > window.position.height - 75f)
                {
                    y = 0;
                    break;
                }
                else
                {
                    r = new Rect(x, y, 200, 200);
                    if (r.Contains(e.mousePosition))
                        if (GUI.Button(r, ""))
                        {
                            if (s.GetType() == typeof(GUISkinEditorStyle_Add))
                            {
                                GUISkinEditorStyle st = CreateInstance<GUISkinEditorStyle_Custome>();
                                st.InitStyle();
                                st.skin = selectedSkin;
                                styles.Insert(styles.Count - 1, st);

                                //Resize Data
                                GUIStyle[] data = selectedSkin.customStyles.ToArray();
                                selectedSkin.customStyles = new GUIStyle[ selectedSkin.customStyles.Length+1];
                                data.CopyTo(selectedSkin.customStyles, 0);
                                selectedSkin.customStyles[selectedSkin.customStyles.Length - 1] = new GUIStyle();
                                st.Init();
                                //Debug.Log("ADDED CUSTOME");
                            }
                            else
                            {
                                selectestylemenu = s;
                                selectedStyleScroll = 0;
                                InitStylePreview();
                                menu = 1;
                            }
                        }

                    GUI.BeginGroup(r);

                    GUI.color = new Color(1, 1, 1, 2);
                    GUI.enabled = false;

                    if (s.iswindow)
                    {
                        BeginWindows();
                        s.OnGUI();
                        EndWindows();
                    }
                    else
                    {
                        s.OnGUI();
                    }


                    GUI.enabled = true;
                    GUI.color = Color.white;
                    GUI.EndGroup();
                    x += 200;
                }
            }
            GUI.EndGroup();
        }

        public void InitStylePreview()
        {
            if (temp_onfocusedStyle == null)
            {
                temp_normalStyle = new GUIStyle(selectestylemenu.guistylename);
                temp_hoverstyle = new GUIStyle(selectestylemenu.guistylename);
                temp_activeStyle = new GUIStyle(selectestylemenu.guistylename);
                temp_focusedStyle = new GUIStyle(selectestylemenu.guistylename);
                temp_onnormalStyle = new GUIStyle(selectestylemenu.guistylename);
                temp_onhoverstyle = new GUIStyle(selectestylemenu.guistylename);
                temp_onactiveStyle = new GUIStyle(selectestylemenu.guistylename);
                temp_onfocusedStyle = new GUIStyle(selectestylemenu.guistylename);
            }
                temp_normalStyle = UpdateStyle(temp_normalStyle, selectestylemenu.style.normal);
                temp_hoverstyle = UpdateStyle(temp_hoverstyle, selectestylemenu.style.hover);
                temp_activeStyle = UpdateStyle(temp_activeStyle, selectestylemenu.style.active);
                temp_focusedStyle = UpdateStyle(temp_focusedStyle, selectestylemenu.style.focused);
                temp_onnormalStyle = UpdateStyle(temp_onnormalStyle, selectestylemenu.style.onNormal);
                temp_onhoverstyle = UpdateStyle(temp_onhoverstyle, selectestylemenu.style.onHover);
                temp_onactiveStyle = UpdateStyle(temp_onactiveStyle, selectestylemenu.style.onActive);
                temp_onfocusedStyle = UpdateStyle(temp_onfocusedStyle, selectestylemenu.style.onFocused);
        }

        public GUIStyle UpdateStyle(GUIStyle s, GUIStyleState st)
        {
            if (st.background != null)
                s.normal.background = st.background;
            else
                s.normal.background = selectestylemenu.style.normal.background;

            s.padding = selectestylemenu.style.padding;
            s.overflow = selectestylemenu.style.overflow;
            s.alignment = selectestylemenu.style.alignment;
            s.border = selectestylemenu.style.border;
            s.clipping = selectestylemenu.style.clipping;
            s.contentOffset = selectestylemenu.style.contentOffset;
            s.fixedHeight = selectestylemenu.style.fixedHeight;
            s.fixedWidth = selectestylemenu.style.fixedWidth;

            s.normal.textColor = st.textColor;
            return s;
        }

        public void StyleTypeView()
        {
            if (selectestylemenu == null)
                menu = 0;

            if(TitleStyle == null)
            {
                TitleStyle = new GUIStyle();
                TitleStyle.alignment = TextAnchor.MiddleCenter;
                TitleStyle.fontSize = 16;
            }

            if(selectestylemenu.iscustome)
                selectestylemenu.style.name = GUI.TextField(new Rect(position.width / 2 - 50, 0, 100, 50), selectestylemenu.style.name, TitleStyle);
            else
                GUI.Box(new Rect(position.width / 2 - 50, 0, 100, 50), selectestylemenu.type.ToString(), TitleStyle);

            if (GUI.Button(new Rect(0, 0, 50, 25), "Back"))//future: Texture Replacement
                menu=0;
            //Init and Updates TempStyles for hover/active/focus/... state
            InitStylePreview();

            GUI.BeginGroup(new Rect(0, 100, position.width - 50, 300));

            GUI.Box(new Rect(0, 0, 125, 300), "");
            GUI.Label(new Rect(0, 75, 125, 25), "State", TitleStyle);
            GUI.Label(new Rect(0, 150, 125, 25), "Background", TitleStyle);
            GUI.Label(new Rect(0, 225, 125, 25), "TextColor", TitleStyle);

            GUI.Box(new Rect(125, 0, position.width - 50, 300), "");
                GUI.BeginGroup(new Rect(125, 0, position.width - 175, 300));
                Rect r = new Rect(-selectedStyleScroll, 0, 300, 500);

                r.x += 25;
                selectestylemenu.style.normal = DrawStylePreview(r, selectestylemenu, selectestylemenu.style.normal, "Normal", temp_normalStyle);

                r.x += 200;
                selectestylemenu.style.hover = DrawStylePreview(r, selectestylemenu, selectestylemenu.style.hover, "Hover", temp_hoverstyle);

                r.x += 200;
                selectestylemenu.style.active = DrawStylePreview(r, selectestylemenu, selectestylemenu.style.active, "Active", temp_activeStyle);

                r.x += 200;
                selectestylemenu.style.focused = DrawStylePreview(r, selectestylemenu, selectestylemenu.style.focused, "Focused", temp_focusedStyle);

                r.x += 200;
                selectestylemenu.style.onNormal = DrawStylePreview(r, selectestylemenu, selectestylemenu.style.onNormal, "OnNormal", temp_onnormalStyle, true);

                r.x += 200;
                selectestylemenu.style.onHover = DrawStylePreview(r, selectestylemenu, selectestylemenu.style.onHover, "OnHover", temp_onhoverstyle, true);

                r.x += 200;
                selectestylemenu.style.onActive = DrawStylePreview(r, selectestylemenu, selectestylemenu.style.onActive, "OnActive", temp_onactiveStyle, true);

                r.x += 200;
                selectestylemenu.style.onFocused = DrawStylePreview(r, selectestylemenu, selectestylemenu.style.onFocused, "OnFocused", temp_onfocusedStyle, true);
                GUI.EndGroup();
            GUI.EndGroup();

            selectedStyleScroll = GUI.HorizontalScrollbar(new Rect(125, 400, position.width - (50+125), 25), selectedStyleScroll, position.width - (50f+125f), 0f, 1600f);
            GUI.Label(new Rect(25, 425, 200, 25), "Overflow");
            selectestylemenu.style.overflow = UtilityGUI.RectOffsetField(new Rect(25, 450, 200, 25), selectestylemenu.style.overflow);

            GUI.Label(new Rect(250, 425, 200, 25), "Border");
            selectestylemenu.style.border = UtilityGUI.RectOffsetField(new Rect(250, 450, 200, 25), selectestylemenu.style.border);

            GUI.Label(new Rect(25, 500, 200, 25), "Padding");
            selectestylemenu.style.padding = UtilityGUI.RectOffsetField(new Rect(25, 525, 200, 25), selectestylemenu.style.padding);

            GUI.Label(new Rect(250, 500, 200, 25), "Margin");
            selectestylemenu.style.margin = UtilityGUI.RectOffsetField(new Rect(250, 525, 200, 25), selectestylemenu.style.margin);

            GUI.Label(new Rect(780, 450, 200, 25), "richText");
            selectestylemenu.style.richText = GUI.Toggle(new Rect(750, 450, 30, 25), selectestylemenu.style.richText, "");

            GUI.Label(new Rect(780, 475, 200, 25), "wordWrap");
            selectestylemenu.style.wordWrap = GUI.Toggle(new Rect(750, 475, 30, 25), selectestylemenu.style.wordWrap, "");

            GUI.Label(new Rect(780, 525, 200, 25), "stretchHeight");
            selectestylemenu.style.stretchHeight = GUI.Toggle(new Rect(750, 525, 30, 25), selectestylemenu.style.stretchHeight, "");

            GUI.Label(new Rect(780, 550, 200, 25), "stretchWidth");
            selectestylemenu.style.stretchWidth = GUI.Toggle(new Rect(750, 550, 30, 25), selectestylemenu.style.stretchWidth, "");

            GUI.Label(new Rect(25, 575, 200, 25), "ImagePosition");
            GUILayout.BeginArea(new Rect(25, 600, 425, 25));
            selectestylemenu.style.imagePosition = (ImagePosition)GUILayout.SelectionGrid((int)selectestylemenu.style.imagePosition, new string[] { "ImageLeft", "ImageAbove", "ImageOnly", "TextOnly" }, 4);
            //selectestylemenu.style.imagePosition = (ImagePosition)GUI.SelectionGrid(new Rect(125, 575, 325, 25), (int)selectestylemenu.style.imagePosition, new string[] {"ImageLeft","ImageAbove", "ImageOnly", "TextOnly"}, 4);
            GUILayout.EndArea();

            GUI.Label(new Rect(25, 625, 200, 25), "Font Style");
            GUILayout.BeginArea(new Rect(25, 650, 425, 25));
            selectestylemenu.style.fontStyle = (FontStyle)GUILayout.SelectionGrid((int)selectestylemenu.style.fontStyle, new string[] { "Normal", "Bold", "Italic", "BoldAndItalic" }, 4);
            GUILayout.EndArea();

            GUI.Label(new Rect(500, 625, 200, 25), "Font Size");
            selectestylemenu.style.fontSize = EditorGUI.IntField(new Rect(500, 650, 100, 25), selectestylemenu.style.fontSize);

            GUI.Label(new Rect(625, 625, 200, 25), "Font");
            selectestylemenu.style.font = UtilityGUI.FontField(new Rect(625, 650, 100, 25), selectestylemenu.style.font);

            GUI.BeginGroup(new Rect(525, 475, 200, 200));
                if (selectestylemenu.iswindow)
                {
                    BeginWindows();
                    selectestylemenu.OnPreview(selectestylemenu.style, 1);//Rect(0,0,200,50)
                    EndWindows();
                }
                else
                {
                    selectestylemenu.OnPreview(selectestylemenu.style, 1);//Rect(0,0,200,50)
                }
                GUI.EndGroup();
        }

        public GUIStyleState DrawStylePreview(Rect rect,GUISkinEditorStyle style, GUIStyleState state, string statename, GUIStyle tempstyle, bool Showstate=false)
        {
            if (tempstyle == null)
                InitStylePreview();

            GUI.BeginGroup(rect);

            if(style.iswindow)
            {
                BeginWindows();
                style.OnPreview(tempstyle, Showstate?0:2);//Rect(0,0,200,50)
                EndWindows();
            }
            else
                style.OnPreview(tempstyle, Showstate ? 0 : 2);//Rect(0,0,200,50)

            GUI.Label(new Rect(0, 75, 150, 50), statename, TitleStyle);
            state.background = (Texture2D)EditorGUI.ObjectField(new Rect(25, 150, 100, 30), state.background, typeof(Texture2D), false);
            state.textColor = (Color)EditorGUI.ColorField(new Rect(25, 225, 100, 30), GUIContent.none, state.textColor);
            GUI.EndGroup();
            return state;
        }

        public void DrawHeader()
        {
            if (selectedSkin != null)
            {
                GUI.Label(new Rect(0, 0, 50, 25), "Name: ");
                selectedSkin.name = GUI.TextField(new Rect(50, 0, 100, 20), selectedSkin.name);

                GUI.changed = false;
                if (fontnames.Count > 0)
                {
                    selectedmainfont = EditorGUI.Popup(new Rect(150, 0, 100, 20), selectedmainfont, fontnames.ToArray());
                    if (GUI.changed)
                    {
                        selectedSkin.font = fonts[selectedmainfont];
                        //selectedSkin.font = ImportFont(fontnames[selectedmainfont], FontType.Normal);
                    }
                }
                else
                {
                    GUI.Box(new Rect(150, 0, 100, 20), "NO FONTS FOUND");
                }
            }
            else
                if(Selection.objects.Length>0)
                    GUILayout.Box(Selection.objects[0].GetType().ToString());
        }

        public Font ImportFont(string name, FontType t)
        {
            string s = GetSystemFontFileName(name, t);
            if (s.Length == 0)
                return null;

            string p = "C:/Windows/Fonts/" + s;

            if (!Directory.Exists(Application.dataPath + "/Fonts/"))
                Directory.CreateDirectory(Application.dataPath + "/Fonts/");

            File.Copy(p, Application.dataPath + "/Fonts/" + name + Path.GetExtension(p));
            AssetDatabase.Refresh();
            AssetDatabase.ImportAsset("Asset/Fonts/" + name + Path.GetExtension(p), ImportAssetOptions.Default);
            Font f = (Font)AssetDatabase.LoadAssetAtPath("Asset/Fonts/" + name + Path.GetExtension(p), typeof(Font)) as Font;
            Debug.Log(f == null ? "NULL" : "EXISTS");
            return f;
        }

        public static string GetSystemFontFileName(string name, FontType type)
        {
            RegistryKey fonts = null;
            try
            {
                fonts = Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\Windows NT\CurrentVersion\Fonts", false);
                if (fonts == null)
                {
                    fonts = Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Fonts", false);
                    if (fonts == null)
                    {
                        throw new Exception("Can't find font registry database.");
                    }
                }
                string[] names = fonts.GetValueNames();
                StringBuilder nameb = new StringBuilder(name);
                if (type == FontType.Bold)
                    nameb.Append(" Bold");
                if (type == FontType.Italic)
                    nameb.Append(" Italic");
                nameb.Append(" (TrueType)");
                string fullname = nameb.ToString();
                string basename = name + " (TrueType)";
                object file = fonts.GetValue(fullname);
                if (file == null && fullname != basename)
                {
                    file = fonts.GetValue(basename);
                }
                if (file != null) return file.ToString();
                return null;
            }
            finally
            {
                if (fonts != null)
                {
                    fonts.Close();
                }
            }
        }

        public enum FontType
        {
            Normal = 0,
            Bold = 1,
            Italic = 2
        };
    }
}