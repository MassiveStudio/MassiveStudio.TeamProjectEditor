﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;


using MassiveStudio.TeamProjectEditor.EditorWindows.GUIDesigner.Elements;

namespace MassiveStudio.TeamProjectEditor.EditorWindows.GUIDesigner.Tools
{
    [System.Serializable]
    internal class GUIDesigner_ToolBox : GUIDesignerTool
    {
        public static List<GUIDesignerElement> elements = new List<GUIDesignerElement>();

        public override void Init()
        {
            this.name = "ToolBox";
            Debug.Log("Tool " + this.name + " Init");
            elements.Clear();
            GUIDesignerElement l;

            l = ScriptableObject.CreateInstance<GUIDesigner_Label>();
            elements.Add(l);
            l = ScriptableObject.CreateInstance<GUIDesigner_Box>();
            elements.Add(l);

            Debug.Log("Elements: " + elements.Count);

            foreach (GUIDesignerElement e in elements)
                e.Init();

        }

        private Event e;
        public override void OnGUI()
        {
            e = Event.current;

            GUI.BeginGroup(rect);
            GUI.Box(new Rect(0, 0, rect.width, 25), name);
            int y = 25;
            Rect r;
            foreach (GUIDesignerElement element in elements)
            {
                r = new Rect(0, y, rect.width, 25);
                GUI.Box(r, element.element_name);
                if (r.Contains(e.mousePosition - new Vector2(rect.x, rect.y)))
                {
                    if (e.button == 0 && e.type == EventType.MouseDown)
                    {
                        Debug.Log("type: " +element.element_name +"  "+ element.GetType());
                        GUIDesigner.Drag = CreateInstance<GUIDesignerElement>(element);
                        
                    }
                }
                y +=25;
            }

            GUI.EndGroup();
        }

        public static object CreateInstance<T>(T type) where T : GUIDesignerElement
        {
            //Debug.Log("Create Instance");
            var instance = ScriptableObject.CreateInstance(type.GetType());
            ((GUIDesignerElement)instance).Init();
            //Debug.Log(typeof(T).ToString());
            //Debug.Log(type.GetType().ToString());
            if (instance == null)
                Debug.LogError("NULL");
            return instance;
        }
    }


}
