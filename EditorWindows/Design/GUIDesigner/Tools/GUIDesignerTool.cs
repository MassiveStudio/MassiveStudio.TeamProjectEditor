﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor.EditorWindows.GUIDesigner.Tools
{
    [System.Serializable]
    internal abstract class GUIDesignerTool : ScriptableObject
    { 
        public UnityEditor.MonoScript targetscript;
        public string ToolName = "";
        public Rect rect;

        public abstract void OnGUI();

        public abstract void Init();
    }
}
