﻿using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;
using System.Collections.Generic;

using MassiveStudio.TeamProjectEditor.Properties;

using MassiveStudio.TeamProjectEditor.EditorWindows.GUIDesigner.Tools;
using MassiveStudio.TeamProjectEditor.EditorWindows.GUIDesigner.Elements;
namespace MassiveStudio.TeamProjectEditor.EditorWindows.GUIDesigner
{
    internal class GUIDesigner : EditorWindow
    {
        public static GUIDesigner window;

        public GUIDesignerType type;

        public static List<GUIDesignerTool> tools = new List<GUIDesignerTool>();
        public int selectedTool = -1;

        public bool showToolBox = false;

        public object SelectedObject;
        public UnityEditor.MonoScript script;

        public Dictionary<string, Type> Scriptfields = new Dictionary<string, Type>();

        public static GUIStyle Toolstyle = new GUIStyle();

        public Rect UserArea = new Rect();

        public static object Drag = null;
        //public List<GUIDesignerElement> guis = new List<GUIDesignerElement>();

        public GUIDesigner_Document doc;

        public bool editmode = true;
        public GUIDesignerElement draggedElement;
        public Vector2 mousedown;

        public Event e;
        //ScaleHandle
        private static Texture2D ScaleKnop;
        public ScaleHandle selscalehandle = ScaleHandle.None;
        public Rect temp_scalerect;

        [MenuItem("MassiveStudio/GUIDesigner")]
        static void Init()
        {
            window = GetWindow<GUIDesigner>();
            window.titleContent = new GUIContent("GUIDesigner");

            InitStyle();
            InitTools();
            Selection.objects = new UnityEngine.Object[] { window };

            if (window.doc == null)
                window.doc = new GUIDesigner_Document();

        }

        public void OnEnable()
        {
            if (doc == null)
                doc = new GUIDesigner_Document();

            if (tools == null)
                InitTools();
        }

        public static void InitStyle()
        {
            Toolstyle.fontSize = 10;
            Toolstyle.alignment = TextAnchor.MiddleCenter;

            ScaleKnop = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.Knop);
            ScaleKnop.wrapMode = TextureWrapMode.Clamp;
        }

        public static void InitTools()
        {
            tools = new List<GUIDesignerTool>();
            GUIDesignerTool t;
            // Debug.Log("Type: " + GetType().BaseType);
            t = CreateInstance<GUIDesigner_ToolBox>();
            tools.Add(t);

            //Init Tools
            foreach (GUIDesignerTool tool in tools)
            {
                tool.Init();
            }
        }
    
        public void Update()
        {
            if (Selection.objects.Length > 0)
                SelectedObject = Selection.objects[0];

            if (EditorWindow.focusedWindow == window)
                Repaint();


        }

        public void HandleDragDrop()
        {
            GUI.Box(new Rect(e.mousePosition.x, e.mousePosition.y, 50, 50), Drag.GetType().ToString());

            if (e.type == EventType.MouseUp && e.button == 0)
            {
                Debug.Log("Drop " + Drag.GetType());
                doc.guis.Add(Drag as GUIDesignerElement);
                ((GUIDesignerElement)Drag).position = new Rect(e.mousePosition.x, e.mousePosition.y, 100, 25);
                Drag = null;
                
            }
        }

        public void OnSelectionChange()
        {
            Repaint();
            LoadGUI();
        }

        public void LoadGUI()
        {
            if (SelectedObject.GetType() == typeof(UnityEditor.MonoScript))
            {
                script = (UnityEditor.MonoScript)SelectedObject;
                LoadFields();
            }
        }

        public void LoadFields()
        {
            Scriptfields = new Dictionary<string, Type>();

            BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetField;
            Type t = script.GetType();
            FieldInfo[] fields = t.GetFields(flags);

            foreach (FieldInfo field in fields)
            {
                UnityEngine.Debug.Log("FIELD " + field.Name);
                Scriptfields.Add(field.Name, field.GetType());
            }
        }

        public void OnGUI()
        {
            e = Event.current;
            //if (SelectedObject != null)
            //    GUI.Box(new Rect(0, 0, 500, 25), SelectedObject.GetType().ToString());

            DrawMenu();

            //if (showToolBox)
            DrawToolsBox();

            DrawWorkScreen();

            if( selectedTool!=-1)
            {
                GUI.BeginGroup(new Rect(25, 25, 300, position.height - 25), "");
                GUI.Box(new Rect(0, 0, 300, position.height - 25), "");

                tools[selectedTool].rect = new Rect(0, 0, 300, position.height - 25);
                tools[selectedTool].OnGUI();
                if(GUI.Button(new Rect(275, 0, 25, 25), "X"))
                {
                    selectedTool = -1;
                }
                GUI.EndGroup();
            }

            if (Drag != null)
                HandleDragDrop();
        }

        public void DrawMenu()
        {
            GUI.BeginGroup(new Rect(0, 0, position.width, 25));
            GUI.Box(new Rect(0, 0, position.width, 25), "");
            GUILayout.BeginHorizontal();
            if(GUILayout.Button("Generate GUI"))
            {
                if(type == GUIDesignerType.FPDFDesigner)
                    GenerateToPDF("PDF");
            }

            if (script == null)
                GUILayout.Label("Script: " + "None");
            else
                GUILayout.Label("Script: " + script.name);

            editmode = GUILayout.Toggle(editmode, "Editmode");

            GUILayout.Label(" ");

            type = (GUIDesignerType) EditorGUI.EnumPopup(GUILayoutUtility.GetLastRect(), GUIContent.none, type);

            GUILayout.EndHorizontal();
            GUI.EndGroup();
        }

        public void DrawWorkScreen()
        {
            GUI.BeginGroup(new Rect(25, 25, position.width - 25, position.height - 25));
            GUI.Box(new Rect(0, 0, position.width - 25, position.height - 25), "");

            if(type == GUIDesignerType.GUIWindow)
            {

            }
            else if(type == GUIDesignerType.ProbertyDrawer)
            {

            }
            else
            {

            }

            foreach (GUIDesignerElement e in doc.guis)
            {
                if (e != null)
                {
                    e.OnDraw(editmode);
                    if (editmode)
                    {
                        RectHandler(e);

                        if (SelectedObject == (object)e)
                            ScaleHandler();
                    }
                }
                else
                {
                    doc.guis.Remove(e);
                    break;
                }
            }

            GUI.EndGroup();
        }

        public void RectHandler(GUIDesignerElement el)
        {
            if (draggedElement == null)
            {
                Vector2 pos = e.mousePosition;
                GUI.Box(new Rect(pos.x, pos.y, 50, 50), "");

                if (el.position.Contains(e.mousePosition))
                {
                    if (e.button == 0 && e.type == EventType.MouseDown)
                    {
                        draggedElement = el;
                        mousedown = e.mousePosition;
                        Selection.objects = new UnityEngine.Object[] { el };

                    }
                }
            }
            else if (draggedElement == el)
            {
                if (e.type == EventType.MouseUp && e.button == 0)
                    draggedElement = null;

                else if (e.type == EventType.MouseDrag && e.button == 0)
                {
                    el.position.x += e.mousePosition.x - mousedown.x;
                    el.position.y += e.mousePosition.y - mousedown.y;
                    mousedown = e.mousePosition;
                }
            }
        }

        public void ScaleHandler()
        {
            if (ScaleKnop == null)
                InitStyle();
            GUIDesignerElement el = ((GUIDesignerElement)SelectedObject);
            Rect r = el.position;
            Rect r2 = new Rect(r.x - 5, r.y - 5, 10, 10);
            GUI.Box(r2, ScaleKnop, GUIStyle.none);

            Rect r3 = new Rect(r.x + r.width - 5, r.y - 5, 10, 10);
            GUI.Box(r3, ScaleKnop, GUIStyle.none);

            Rect r4 = new Rect(r.x + r.width - 5, r.y + r.height - 5, 10, 10);
            GUI.Box( r4, ScaleKnop, GUIStyle.none);

            Rect r5 = new Rect(r.x - 5, r.y + r.height - 5, 10, 10);
            GUI.Box(r5, ScaleKnop, GUIStyle.none);

            if (e.button == 0 && e.type == EventType.MouseDown)
            {
                temp_scalerect = r;
                if (r2.Contains(e.mousePosition))
                    selscalehandle = ScaleHandle.TopLeft;
                else if (r3.Contains(e.mousePosition))
                    selscalehandle = ScaleHandle.TopRight;
                else if (r4.Contains(e.mousePosition))
                    selscalehandle = ScaleHandle.BotRight;
                else if (r5.Contains(e.mousePosition))
                    selscalehandle = ScaleHandle.BotLeft;
            }
            else if (e.button == 0 && e.type == EventType.MouseUp)
            {
                selscalehandle = ScaleHandle.None;
            }


            if (selscalehandle == ScaleHandle.TopLeft)
                el.position = new Rect(temp_scalerect.x + (e.mousePosition.x -temp_scalerect.x), temp_scalerect.y + (e.mousePosition.y - temp_scalerect.y), temp_scalerect.width - (e.mousePosition.x - temp_scalerect.x), temp_scalerect.height - (e.mousePosition.y - temp_scalerect.y));
            else if (selscalehandle == ScaleHandle.TopRight)
                el.position = new Rect(temp_scalerect.x, temp_scalerect.y + (e.mousePosition.y - temp_scalerect.y), temp_scalerect.width + (e.mousePosition.x - (temp_scalerect.position.x+ temp_scalerect.width)), temp_scalerect.height - (e.mousePosition.y - temp_scalerect.y));
            else if (selscalehandle == ScaleHandle.BotRight)
                el.position = new Rect(temp_scalerect.x, temp_scalerect.y, temp_scalerect.width + (e.mousePosition.x - (temp_scalerect.position.x + temp_scalerect.width)), temp_scalerect.height + (e.mousePosition.y - (temp_scalerect.height+ temp_scalerect.y)));
            else if (selscalehandle == ScaleHandle.BotLeft)
                el.position = new Rect(temp_scalerect.x + (e.mousePosition.x - temp_scalerect.x), temp_scalerect.y, temp_scalerect.width - (e.mousePosition.x - temp_scalerect.x), temp_scalerect.height + (e.mousePosition.y - (temp_scalerect.height + temp_scalerect.y)));
        }

        public void DrawToolsBox()
        {
            GUI.BeginGroup(new Rect(0, 25, 500, position.height - 25));
            GUI.Box(new Rect(0, 0, 25, position.height - 25), "");
            int y = 0;
            Rect r;
            Rect r2;
            int c = 0;

            if (tools != null)
                foreach (GUIDesignerTool tool in tools)
                {
                    if (tool != null)
                    {
                        r = new Rect(0, 50 + y * 50f, 50f, 25f);
                        r2 = new Rect(0, y * 50, 25f, 50f);

                        if (r2.Contains(e.mousePosition))
                        {
                            GUI.color = Color.blue;
                            if (e.button == 0 && e.clickCount >= 1 && e.type == EventType.MouseDown)
                                if (selectedTool != c)
                                    selectedTool = c;
                                else
                                    selectedTool = -1;
                        }

                        

                        EditorGUIUtility.RotateAroundPivot(-90f, new Vector2(0, 50+y*50f));
                        GUI.Box(r, "");

                       
                        EditorGUI.LabelField(r, tool.name, Toolstyle);
                        GUI.color = Color.white;

                        EditorGUIUtility.RotateAroundPivot(90f, new Vector2(0, 50 + y * 50f));


                        //if (GUI.Button(, new Rect(0, 0, 25, 50))
                    }
                    c++;
                }

            GUI.EndGroup();
        }

        public void GenerateToPDF(string FILENAME)
        {
            char UC = (char)0x20;
            string s = " <? php" + "\n" +
            "require('../fpdf.php');" + "\n" +
            "$pdf = new FPDF(); " + "\n" +
            "$pdf->AddPage();"+"\n";

            foreach(GUIDesignerElement a in doc.guis)
            {
                s += "$pdf->Text(" + a.position.x +", "+ a.position.y+", '" +  a.text +"');"+"\n";
            }

            s += "$pdf->Output("+ UC+ "D" + UC+ ", " +UC + FILENAME + UC+", true);";

            System.IO.File.WriteAllText(Application.dataPath + "/" + "PDF_output.txt", s);
        }
    }

    public static class Utility
    {
        public static void ZoomRect(ref Rect r, float f)
        {
            r.x = r.x * f;
            r.y = r.y * f;
            r.width = r.width * f;
            r.height = r.height * f;
        }
    }

    public enum ScaleHandle
    {
        None=0,
        TopLeft=1,
        TopRight=2,
        BotRight=3,
        BotLeft=4
    };

}