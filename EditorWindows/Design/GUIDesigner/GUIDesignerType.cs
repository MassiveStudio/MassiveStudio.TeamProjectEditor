﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MassiveStudio.TeamProjectEditor.EditorWindows.GUIDesigner
{
    [System.Serializable]
    public enum GUIDesignerType
    {
        GUI=0,
        GUIWindow=1,
        ProbertyDrawer=2,
        FPDFDesigner=3
    };
}
