﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor.EditorWindows.GUIDesigner.Elements
{
    [System.Serializable]
    internal abstract class GUIDesignerElement : ScriptableObject
    {
        public Rect position;
        public string element_name;

        public GUISkin skin;

        public string[] inputs;

        public string text
        {
            get
            {
                if (inputs == null)
                    return "";
                else
                    return inputs[0]; 
            }
        }

        public GUIDesignerElement(){}

        public virtual void Init()
        {
            Debug.Log("Init " + element_name+" Element");
        }

        public virtual string GetCode()
        {
            return "";
        }

        public abstract void OnPreview();

        public abstract void OnDraw(bool edit);
    }
}
