﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MassiveStudio.TeamProjectEditor.EditorWindows.GUIDesigner;
using UnityEditor;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor.EditorWindows.GUIDesigner.Elements
{
    internal class GUIDesigner_Box : GUIDesignerElement
    {
        public override void Init()
        {
            this.element_name = "GUI.Box";
            position.width = 100;
            position.height = 25;
            inputs = new string[0];
            base.Init();
        }

        public override string GetCode()
        {
            return "GUI.Box(new " + this.position.ToString() + "," + '"'+ '"' + ");";
        }

        public override void OnPreview()
        {

        }

        public override void OnDraw(bool edit)
        {
           // if (edit)
           //     GUI.Box(position, "");

            if (inputs != null)
                GUI.Box(position, "");
        }
    }
}