﻿using UnityEngine;
using System.Collections.Generic;


using MassiveStudio.TeamProjectEditor.EditorWindows.GUIDesigner.Tools;
using MassiveStudio.TeamProjectEditor.EditorWindows.GUIDesigner.Elements;
namespace MassiveStudio.TeamProjectEditor.EditorWindows.GUIDesigner
{
    internal class GUIDesigner_Document
    {
        public Vector2 document_size;
        public float resulution = 1000;

        public List<GUIDesignerElement> guis;

        DINFormat _format;
        public DINFormat format
        {
            get
            {
                return _format;
            }
            set
            {
                document_size = new Vector2(1, Mathf.Sqrt((int)value)) * resulution;
                _format = value;
            }
        }

        public GUIDesigner_Document()
        {
            guis = new List<GUIDesignerElement>();
            format = DINFormat.A4;
        }
    }

    public enum DINFormat : byte//1:Sqrt(2) ~ 1:1.4142
    {
        A4=2,
        A3,
        A2,
        A1,
        A5
    }
}
