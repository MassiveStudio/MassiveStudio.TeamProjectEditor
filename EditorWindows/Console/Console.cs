﻿using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using UnityEngine;
using UnityEditor;
using System.IO;
using System;

using MassiveStudio.TeamProjectEditor.Utility;

namespace MassiveStudio.TeamProjectEditor.Console
{
    public static class TeamConsole//Wrapperclass
    {
        public static void RegisterThread(Thread thread, string titel)
        {
            TeamProjectEditor.EditorWindows.Console.Console.RegisterThread(thread, titel);
        }

        public static void RegisterVariable(object _class, string variable_name)
        {
            TeamProjectEditor.EditorWindows.Console.Console.RegisterVariable(_class, variable_name);
        }
        /*public static void WriteLine(string message)
        {
            TeamProjectEditor.EditorWindows.Console.Console.Message(message, string.Empty, Color.white, System.Environment.StackTrace);
        }*/
        public static void Message(string message, string TAG="")
        {
            TeamProjectEditor.EditorWindows.Console.Console.Message(message, TAG, Color.white, System.Environment.StackTrace);
        }

        public static void Message(string message, Color col)
        {
            TeamProjectEditor.EditorWindows.Console.Console.Message(message, string.Empty, col, System.Environment.StackTrace);
        }
        /*public static void Message(string message, string TAG, Color col)
        {
            TeamProjectEditor.EditorWindows.Console.Console.Message(message, TAG, col, System.Environment.StackTrace);
        }*/
        public static void Message(string message, string TAG, Color col, bool save=false)
        {
            if (false || save) //Preferences.aktivate_LogSaver
            {
                using (FileStream stream = new FileStream(TeamSceneManager.DataPath + "/Log.txt", FileMode.OpenOrCreate))
                    using (StreamWriter writer = new StreamWriter(stream))
                        writer.WriteLine(message);
            }
            TeamProjectEditor.EditorWindows.Console.Console.Message(message, TAG, col, System.Environment.StackTrace);
        }

        public static void Message(string message, string TAG, Color col, bool dephformat, bool save = false)
        {
            if (false || save) //Preferences.aktivate_LogSaver
            {
                using (FileStream stream = new FileStream(TeamSceneManager.DataPath + "/Log.txt", FileMode.OpenOrCreate))
                using (StreamWriter writer = new StreamWriter(stream))
                    writer.WriteLine(message);
            }
            string dephstring = "";
            string stacktrace = System.Environment.StackTrace;
            if (dephformat)
            {
                int deph = stacktrace.Count(c => c == '\n');
                for (int i = 0; i < deph; i++)
                    dephstring += " ";
            }
            TeamProjectEditor.EditorWindows.Console.Console.Message(dephstring + message, TAG, col, stacktrace);
        }

        public static void Error(string message, string TAG)
        {
            TeamProjectEditor.EditorWindows.Console.Console.output_Messages.Add(new ConsoleMessage(message, TAG ,System.Environment.StackTrace.ToString(), ConsoleMessageType.System_Error));
        }
    }
}

namespace MassiveStudio.TeamProjectEditor.EditorWindows.Console
{
    [DisallowMultipleComponent]
    internal class Console : EditorWindow
    {
        [SerializeField]
        private static Console window = null;

        public static List<ConsoleMessage> output_Messages;// = new List<ConsoleMessage>();
        public static List<ConsoleMessage> sort_output = new List<ConsoleMessage>();
        public static bool output_Messages_dirty = true;
        public static List<ConsoleThreadDebug> output_ThreadDebug;// = new List<ConsoleThreadDebug>();
        public static List<ConsoleVariableDebug> output_VariableDebug;// = new List<ConsoleVariableDebug>();

        public string searchTag = "";
        private string temp_searchTag = "";

        private float scroll = 0f;
        private static int error = 0;
        private static int warn = 0;
        private static int log = 0;
        private static int inf = 0;

        private int select = -1;

        [SerializeField]
        private static GUIStyle style;
        [SerializeField]
        private static GUIStyle deleletestyle;
        [SerializeField]
        public static TableRow row_MessageView;     //DEBUG MESSAGES
        [SerializeField]
        public static TableRow row_ThreadView;      //THREADS
        [SerializeField]
        public static TableRow row_VariableView;    //VARIABLES

        public static bool autoScroll = true;
        private static bool calc = false;

        private static bool calc_ThreadView = false;

        [SerializeField]
        private static string[] menu_titels = new string[] { "Debug", "Thread", "Variable" };
        private static int menu = 0;

        [SerializeField]
        public ConsoleData tempData;

        public void Awake()
        {
            OnSerializing();
        }

        void OnSerializing()
        {
            Debug.Log("SERIALIZING");
            if(tempData==null)
                tempData = new ConsoleData();
            tempData.output_Messages = output_Messages;
            tempData.output_ThreadDebug = output_ThreadDebug;
            tempData.output_VariableDebug = output_VariableDebug;
        }

        void OnDeserializing()
        {
            Debug.Log("DERSERIALIZING");
            if (tempData != null)
            {
                output_Messages = tempData.output_Messages;
                output_ThreadDebug = tempData.output_ThreadDebug;
                output_VariableDebug = tempData.output_VariableDebug;
            }
            else
            {
                output_Messages = new List<ConsoleMessage>();
                output_ThreadDebug = new List<ConsoleThreadDebug>();
                output_VariableDebug = new List<ConsoleVariableDebug>();
            }
        }

        private static bool loaded = false;//if lost reinit all null fields

        [MenuItem("MassiveStudio/Console")]
        static void InitWindow()
        {
            window = GetWindow<Console>();
            window.titleContent = new GUIContent("Team Console");
            window.minSize = new Vector2(400, 200);
            Debug.developerConsoleVisible = true;

            Init();//Init Values

            TeamProjectEditor.SystemUtility.EditorEventSystem.OnDeserilization -= window.OnDeserializing;
            TeamProjectEditor.SystemUtility.EditorEventSystem.OnDeserilization += window.OnDeserializing;

            window.Awake();
            window.Show();
        }

        public static void InitStyles()
        {
            deleletestyle = new GUIStyle("button");
            style = GUI.skin.box;
            style.wordWrap = true;
            style.alignment = TextAnchor.MiddleLeft;
            style.fontSize = 12;
        }

        public static void Init()
        {
            menu = 0;

            if (output_Messages == null)
                output_Messages = new List<ConsoleMessage>();
            if (output_ThreadDebug == null)
                output_ThreadDebug = new List<ConsoleThreadDebug>();
            if (output_VariableDebug == null)
                output_VariableDebug = new List<ConsoleVariableDebug>();

            sort_output = output_Messages;
            if (sort_output == null)
                sort_output = new List<ConsoleMessage>();

            //Aif (showwindow)
            //{

            //if (window == null)
            //    InitWindow();

            Debug.Log("Init ConsoleWindow");

                row_MessageView = new TableRow(new Rect(0, 0, 500, 25f), new float[] { 85, 30, 10, 40 }, new bool[] { true, false, false, false });//width:500->window.position.width
                row_ThreadView = new TableRow(new Rect(0, 0, 500, 25f), new float[] { 10, 10, 5, 10 });
                row_VariableView = new TableRow(new Rect(0, 0, 500, 25f), new float[] { 10, 10, 10, 5 });
                row_MessageView = new TableRow(new Rect(0, 0, 500, 25f), new float[] { 85, 30, 10, 40 }, new bool[] { true, false, false, false });
                row_ThreadView = new TableRow(new Rect(0, 0, 500, 25f), new float[] { 10, 10, 5, 10 });
                row_VariableView = new TableRow(new Rect(0, 0, 500, 25f), new float[] { 10, 10, 10, 5 });

                loaded = true;
            //}
            //else
            //    Debug.Log("INIT ConsoleData");

            //EditorApplication.update -= OnUpdate;
            //EditorApplication.update += OnUpdate;

            //Application.logMessageReceived -= LogHandler;
            //Application.logMessageReceived += LogHandler;
            //Application.SetStackTraceLogType(LogType.Log, StackTraceLogType.ScriptOnly);

            menu_titels = new string[] { "Debug", "Thread", "Variable" };

            // GetLogEntry();
        }

        [InitializeOnLoadMethod]
        public static void UnityInit()
        {
            Application.logMessageReceived -= LogHandler;
            Application.logMessageReceived += LogHandler;
            Application.SetStackTraceLogType(LogType.Log, StackTraceLogType.ScriptOnly);
            Debug.Log("Unity started -  TeamConsole Init");

            if (window != null)
                window.OnDeserializing();

            //Only Init Data
            Init();
        }

        public static void LogHandler(string message, string stackTrace, LogType type)
        {
            if (false)//Preferences.aktivate_LogSaver)
            {
                using (StreamWriter writer = new StreamWriter(Application.dataPath + "/Log.txt", true))
                    writer.WriteLine(message + "\t" + stackTrace + " \t" + type.ToString());
            }

            if (!loaded)
                Init();

            if (output_Messages == null)
                window.OnDeserializing();

            output_Messages.Add(new ConsoleMessage(message, stackTrace, type));
            calc = true;

            //GetErrorCount();

            if(window!=null)
                window.Repaint();
        }

        public static void RegisterThread(Thread thread, string titel)
        {
            if (output_ThreadDebug == null)
                window.OnDeserializing();

            output_ThreadDebug.Add(new ConsoleThreadDebug(thread, titel));
        }

        internal protected static void Message(string message, string TAG, Color col, string StackTrace)
        {
            if (window!=null && output_Messages == null)
                window.OnDeserializing();

            output_Messages.Add(new ConsoleMessage(message, TAG, col, StackTrace));
            calc = true;
            output_Messages_dirty = true;
        }

        public static void RegisterVariable(object _class, string variable_name)
        {
            if (output_VariableDebug == null)
                window.OnDeserializing();

            if (_class == null)
                throw new System.NullReferenceException("Property '_class' couldn't be null");
            ConsoleVariableDebug cvd = new ConsoleVariableDebug(_class, variable_name);
            if (output_VariableDebug == null)
            {
                output_VariableDebug = new List<ConsoleVariableDebug>();
                Debug.Log("TEAM CONSOLE VARIABLE LIST NULL");
            }
            output_VariableDebug.Add(cvd);
        }

        private static Event e;

        public void OnGUI()
        {
            if (window == null)
            {
                window = GetWindow<Console>();
                if(window == null)
                    return;
            }

            if (style == null)
                InitStyles();

            if (!loaded)
                Init();

            try
            {
                e = Event.current;
                DrawHeader();
                if (menu == 0)
                    DrawMessageview();
                else if (menu == 1)
                    DrawThreadView();
                else if (menu == 2)
                    DrawVariableView();
            }
            catch (System.Exception e)
            {
                Debug.LogError("TEAM CONSOLE: " + e.Message + "\n" + e.StackTrace);

                if (menu_titels == null)
                {
                    Debug.LogError("TEAM CONSOLE RELOAD MENU ITEMS");
                    menu_titels = new string[] { "Debug", "Thread", "Variable" };
                }
                else
                    Init();
            }
        }

        public void DrawHeader()
        {
            if (deleletestyle == null)
                deleletestyle = new GUIStyle("button");

            GUI.Box(new Rect(0, 0, 100, 20), "");
            searchTag = GUI.TextField(new Rect(0, 0, 80, 20), searchTag, GUIStyle.none);
            if (searchTag.Length > 0)
                if (GUI.Button(new Rect(80, 0, 20, 20), "X", deleletestyle))
                {
                    searchTag = "";
                    sort_output = output_Messages;
                }

            if (GUI.Button(new Rect(100, 0, 50, 20), "Clear"))
            {
                output_Messages.Clear();
                sort_output.Clear();
                output_ThreadDebug.Clear();
                scroll = -1;
                //Repaint();
            }

            autoScroll = GUI.Toggle(new Rect(150, 0, 100, 20), autoScroll, "AutoScroll");

            GUI.Box(new Rect(250, 0, 150, 20), "Memory: " + System.GC.GetTotalMemory(true).ToString());

            GUI.changed = false;
            menu = GUI.Toolbar(new Rect(400, 0, 400, 20), menu, menu_titels);
            if (GUI.changed)
                scroll = 0;

            if ((temp_searchTag != searchTag || output_Messages_dirty) && menu ==0)
            {
                temp_searchTag = searchTag;
                output_Messages_dirty = false;

                if (searchTag != string.Empty)
                    FindWithTAG(searchTag);
                else
                    sort_output = output_Messages;
            }
            else if(searchTag =="")
            {
                temp_searchTag = "";
            }

            inf = output_Messages.Count;

            GUI.Box(new Rect(position.width - 40, 0, 40, 20), error > 999 ? "999+" : error.ToString());
            GUI.Box(new Rect(position.width - 80, 0, 40, 20), warn > 999 ? "999+" : warn.ToString());
            GUI.Box(new Rect(position.width - 120, 0, 40, 20), log > 999 ? "999+" : log.ToString());
            GUI.Box(new Rect(position.width - 160, 0, 40, 20), inf > 999 ? "999+" : inf.ToString());
        }

        //INTERNAL ERROR GETCOUNT
        public static void GetErrorCount()
        {
            object[] arguments = new object[3];
            arguments[0] = 0;//Errors
            arguments[1] = 0;//Warning
            arguments[2] = 0;//Info
            ReflectDllMethods.GetMethodInvoked<object>("UnityEditor", "LogEntries", "GetCountsByType", arguments);//internal function -> only on mainthread
            error = (int)arguments[0];
            warn = (int)arguments[1];
            log = (int)arguments[2];
        }

        private int max = 0;

        public static string[] MessageListTitelBar = new string[] { "Time", "Message", "Tag", "StackTrace" };

        public void DrawMessageview()
        {
            if (window == null)
                Init();
            else if (row_MessageView == null)
                row_MessageView = new TableRow(new Rect(0, 0, window.position.width, 25f), new float[] { 100, 30, 10, 40 }, new bool[] { true, false, false, false });

            if (style == null)
                style = new GUIStyle("box");

            if (sort_output == null)
                sort_output = output_Messages;

            GUI.Box(new Rect(0, 20, position.width, position.height - 20), "");
            //int max = sort_output.Count * 25;

            if (max < (position.height - 40))
                scroll = 0f;// GUI.VerticalScrollbar(new Rect(position.width - 15f, 40, 15f, position.height - 40), 0f, (position.height - 40), 0f, 0f);
            else
                scroll = GUI.VerticalScrollbar(new Rect(position.width - 15f, 40, 15f, position.height - 40), scroll, (position.height - 40), 0, max);

            if (e.isMouse || e.isKey || calc || e.type == EventType.Repaint)
            {
                UtilityGUI.DrawTableRow(new Rect(0, 20, position.width - 16, 20), ref row_MessageView, MessageListTitelBar, Color.white, style, true);

                if (row_MessageView._dirty)
                {
                    Debug.Log("Sort List " + output_Messages.Count + "st  ELEMENT: " + row_MessageView.sortsel[0] + " DIR: " + row_MessageView.sortsel[1]);
                    row_MessageView._dirty = false;
                    sort_output = SortMessagesList(output_Messages, row_MessageView.sortsel[0], row_MessageView.sortsel[1]);
                    sort_output.Reverse();
                    Repaint();
                }

                int c = 0;
                GUI.BeginGroup(new Rect(0, 40, position.width - 16, position.height - 40));
                int y = 0;

                int h = 20;

                //float w = 0;
                sort_output.ForEach(cm =>
                {
                    if (select == c)
                        h = GetMinConsoleContentHeigth(cm);
                    else
                        h = 20;
                    try
                    {
                        if (UtilityGUI.DrawTableRow(new Rect(0, y - scroll, position.width - 16, h), ref row_MessageView, new string[] { cm.timestamp.ToString("hh:mm:ss.fff"), cm.text, cm.TAG, cm.stackTrace }, cm.col, style))//, cm.type == ConsoleMessageType.Team?style:null))
                        {
                            if (select == c)
                                select = -1;
                            else
                                select = c;

                            h = GetMinConsoleContentHeigth(cm);
                            JumpToPos(y - scroll, h);
                        }
                    }
                    catch (System.Exception e)
                    {
                        Debug.LogError("Team Console: " + e.Message);
                    }
                    c++;
                    y += h;
                });

                max = y;


                if (calc)
                {
                    calc = false;
                    if (autoScroll)
                        scroll = max;
                    Repaint();
                }

                /*if (y > max)
                    y = -1;*/

                /*foreach (ConsoleMessage cm in sort_output)
                {
                    if (cm.type == ConsoleMessageType.Team)
                        UtilityGUI.DrawTableRow(new Rect(0, (c * 25) - scroll, position.width - 16, 25f), row, new string[] { cm.timestamp.ToString("hh:mm:ss.fff"), cm.text, cm.TAG, cm.stackTrace }, cm.col, style);
                    else
                        UtilityGUI.DrawTableRow(new Rect(0, (c * 25) - scroll, position.width - 16, 25f), row, new string[] { cm.timestamp.ToString("hh:mm:ss.fff"), cm.text, cm.TAG, cm.stackTrace }, cm.col);

                    //GUI.Box(new Rect(0, (c*25)- scroll, position.width-15, 25f),cm.timestamp.ToString() + " : "+ cm.text+ " : " + cm.stackTrace);
                    c++;
                }*/
                //GUI.skin.font.material.color = Color.white;

                GUI.color = Color.white;
                GUI.EndGroup();
            }
            if (e.type == EventType.ScrollWheel)
            {
                scroll += e.delta.y * 20;
                Repaint();
            }

            if (scroll < 0)
                scroll = 0f;
            else if (scroll > max)
                scroll = max;
        }

        public void DrawThreadView()
        {
            if (window == null)
                Init();
            else if (row_ThreadView == null)
                row_ThreadView = new TableRow(new Rect(0, 0, window.position.width, 25f), new float[] { 10, 10, 5, 10 });

            if (style == null)
                style = new GUIStyle("box");

            if (sort_output == null)
                sort_output = output_Messages;

            GUI.Box(new Rect(0, 20, position.width, position.height - 20), "");
            //int max = sort_output.Count * 25;


            if (max < (position.height - 40))
            {
                scroll = 0f;// GUI.VerticalScrollbar(new Rect(position.width - 15f, 40, 15f, position.height - 40), 0f, (position.height - 40), 0f, 0f);
            }
            else
            {
                scroll = GUI.VerticalScrollbar(new Rect(position.width - 15f, 40, 15f, position.height - 40), scroll, (position.height - 40), 0, max);
            }

            if (e.isMouse || e.isKey || calc_ThreadView || e.type == EventType.Repaint)
            {
                UtilityGUI.DrawTableRow(new Rect(0, 20, position.width - 16, 20), ref row_ThreadView, new string[] { "Thread Titel", "Thread State", "isAlive", "Priority" }, Color.white, style);

                int c = 0;
                GUI.BeginGroup(new Rect(0, 40, position.width - 16, position.height - 40));
                int y = 0;
                int h = 25;

                output_ThreadDebug.ForEach(cm =>
                {
                    cm.alive = cm.thread.IsAlive;
                    cm.lastState = cm.thread.ThreadState;

                    if (UtilityGUI.DrawTableRow(new Rect(0, y - scroll, position.width - 16, h), ref row_ThreadView, new string[] { cm.titel, cm.lastState.ToString(), cm.alive.ToString(), cm.thread.Priority.ToString() }, Color.white, style))//, cm.type == ConsoleMessageType.Team?style:null))
                    {
                        JumpToPos(y - scroll, h);
                    }
                    c++;
                    y += h;
                });

                max = y;


                if (calc_ThreadView)
                {
                    calc_ThreadView = false;
                    if (autoScroll)
                        scroll = max;
                    Repaint();
                }

                GUI.color = Color.white;
                GUI.EndGroup();
            }
            if (e.type == EventType.ScrollWheel)
            {
                scroll += e.delta.y * 20;
                Repaint();
            }

            if (scroll < 0)
                scroll = 0f;
            else if (scroll > max)
                scroll = max;

            if (GUI.Button(new Rect(position.width - 100, position.height - 50, 100, 50), "Kill All"))
                foreach (ConsoleThreadDebug a in output_ThreadDebug)
                    a.thread.Abort();
        }

        public void DrawVariableView()
        {
            if (window == null)
                Init();
            else if (row_VariableView == null)
                row_VariableView = new TableRow(new Rect(0, 0, window.position.width, 25f), new float[] { 10, 10, 10, 5 });

            if (style == null)
                style = new GUIStyle("box");

            if (sort_output == null)
                sort_output = output_Messages;

            GUI.Box(new Rect(0, 20, position.width, position.height - 20), "");
            //int max = sort_output.Count * 25;


            if (max < (position.height - 40))
            {
                scroll = 0f;// GUI.VerticalScrollbar(new Rect(position.width - 15f, 40, 15f, position.height - 40), 0f, (position.height - 40), 0f, 0f);
            }
            else
            {
                scroll = GUI.VerticalScrollbar(new Rect(position.width - 15f, 40, 15f, position.height - 40), scroll, (position.height - 40), 0, max);
            }

            if (e.isMouse || e.isKey || calc_ThreadView || e.type == EventType.Repaint)
            {
                UtilityGUI.DrawTableRow(new Rect(0, 20, position.width - 16, 20), ref row_VariableView, new string[] { "Class", "Variable Name", "Type", "Value" }, Color.white, style);

                int c = 0;
                GUI.BeginGroup(new Rect(0, 40, position.width - 16, position.height - 40));
                int y = 0;
                int h = 25;

                output_VariableDebug.ForEach(cm =>
                {
                    if (UtilityGUI.DrawTableRow(new Rect(0, y - scroll, position.width - 16, h), ref row_VariableView, new string[] { cm.obj.GetType().ToString(), cm._name.ToString(), cm._type.ToString(), cm.GetValue().ToString() }, Color.white, style)) // cm.lastvalue.ToString()
                        JumpToPos(y - scroll, h);

                    c++;
                    y += h;
                });
                max = y;

                if (calc_ThreadView)
                {
                    calc_ThreadView = false;
                    if (autoScroll)
                        scroll = max;
                    Repaint();
                }

                GUI.color = Color.white;
                GUI.EndGroup();
            }
            if (e.type == EventType.ScrollWheel)
            {
                scroll += e.delta.y * 20;
                Repaint();
            }

            if (scroll < 0)
                scroll = 0f;
            else if (scroll > max)
                scroll = max;
        }

        private GUIContent co = new GUIContent();

        public int GetMinConsoleContentHeigth(ConsoleMessage cm)
        {
            if (co == null)
                co = new GUIContent();

            float w = 0;
            if (cm.stackTrace.Length > cm.text.Length)
            {
                //co = new GUIContent(cm.stackTrace);
                co.text = cm.stackTrace;
                w = row_MessageView.rects[3].width;
            }
            else
            {
                //co = new GUIContent(cm.text);
                co.text = cm.text;
                w = row_MessageView.rects[1].width;
            }
            return (int)Mathf.Ceil(style.CalcHeight(co, w));//((position.width - 16))
        }

        public void JumpToPos(float y, int h)
        {
            if (y < 0f)
                scroll += y;
            else if ((y + h) > position.height - 40)
            {
                scroll += (y + h) - (position.height - 40);
            }
            Repaint();
        }

        public List<ConsoleMessage> SortMessagesList(List<ConsoleMessage> list, byte element, byte direction)
        {

            if (searchTag != string.Empty)
            {
                sort_output = new List<ConsoleMessage>();//Clear removes all collected messages from output 'cause of it's reference
                list.ForEach(c => { if (c.TAG == searchTag) sort_output.Add(c); });
            }

            int i = (int)element;
            switch (i)
            {
                case 0:
                    list = list.OrderBy(x => x.timestamp.Ticks).ToList();
                    break;
                case 1:
                    list = list.OrderBy(x => x.text).ToList();
                    break;
                case 2:
                    list = list.OrderBy(x => x.TAG).ToList();
                    break;
                case 3:
                    list = list.OrderBy(x => x.stackTrace).ToList();
                    break;
                default:
                    Debug.LogError("CASE NOT FOUND " + i);
                    break;
            }

            if (direction == (byte)1)
            {
                list.Reverse();
            }

            return list;
        }

        public void FindWithTAG(string t)
        {
            sort_output = new List<ConsoleMessage>();//Clear removes all collected messages from output 'cause of it's reference
            output_Messages.ForEach(c => { if (c.TAG == t) sort_output.Add(c); });

            /*foreach (ConsoleMessage c in output)
                if (c.TAG == t)
                    sort_output.Add(c);*/
        }

        /* public static void GetLogEntry()
         {
             System.Type type = ReflectDllMethods.GetClassType("UnityEditor", "UnityEditorInternal.LogEntry");

             object obj =
             System.Activator.CreateInstance(type, System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);

             object[] arguments = new object[] {0, obj};
             ReflectDllMethods.GetMethodInvoked("UnityEditor", "UnityEditorInternal.LogEntries", "GetEntryInternal", arguments);//internal function -> only on mainthread

             object f = ReflectDllMethods.GetField(type, obj, "condition");
             Debug.Log("Message: " + ((string)f));
         }*/

        public void OnDestroy()
        {
            //EditorApplication.update -= OnUpdate;
            Application.logMessageReceived -= LogHandler;
        }
    }
}