﻿using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using System;

namespace MassiveStudio.TeamProjectEditor//.Console
{
    [System.Serializable]
    internal class ConsoleMessage
    {
        public string text;
        public Color col;
        public string TAG;
        public string stackTrace;
        public System.DateTime timestamp;
        public ConsoleMessageType type = ConsoleMessageType.Sytem_Log;

        public ConsoleMessage(string message, string tag, Color Color, string stacktrace, ConsoleMessageType messagetype = ConsoleMessageType.Sytem_Log)
        {
            this.timestamp = System.DateTime.Now;
            this.text = message;
            this.col = Color;
            this.TAG = tag;
            this.stackTrace = stacktrace;
            this.type = messagetype;
        }

        public ConsoleMessage(string message, string tag, string stacktrace, ConsoleMessageType messagetype = ConsoleMessageType.Sytem_Log)
        {
            this.timestamp = System.DateTime.Now;
            this.text = message;
            this.TAG = tag;
            this.stackTrace = stacktrace;
            this.type = messagetype;

            col = GetMessageColor(type);
        }

        public ConsoleMessage(string te, string st, LogType ty)//From Unity LogHandler
        {
            this.timestamp = System.DateTime.Now;
            this.text = te;
            this.TAG = string.Empty;
            this.stackTrace = st;

            if (ty == LogType.Error || ty == LogType.Exception || ty == LogType.Assert)
            {
                type = ConsoleMessageType.System_Error;
            }
            else if (ty == LogType.Warning)
            {
                type = ConsoleMessageType.System_Warn;
            }
            else if (ty == LogType.Log)
            {
                type = ConsoleMessageType.Sytem_Log;
            }
            col = GetMessageColor(type);
        }

        public static Color GetMessageColor(ConsoleMessageType type)//SWTICH CASE
        {
            if (type == ConsoleMessageType.System_Error)
                return Color.red;
            else if (type == ConsoleMessageType.System_Warn)
                return Color.yellow;
            else if (type == ConsoleMessageType.Sytem_Log)
                return Color.white;
            else if(type == ConsoleMessageType.Team)
                return Color.blue;
            return Color.white;
        }
    }

    [System.Serializable]
    internal class ConsoleThreadDebug
    {
        public Thread thread;
        public ThreadState lastState;
        public string titel;
        public bool alive;

        public ConsoleThreadDebug(Thread t , string name)
        {
            this.thread = t;
            this.titel = name;
            lastState = t.ThreadState;
            alive = t.IsAlive;
        }
    }

    [System.Serializable]
    internal class ConsoleVariableDebug
    {
        public System.Type _type;
        public string _name;

        private FieldInfo info;

        public object obj;//Class
        public object lastvalue;

        public ConsoleVariableDebug(object o, string name)
        {
            this.obj = o;
            this._name = name;
            this.info = o.GetType().GetField(name);
            if (info == null)
                Debug.LogError("FIELD " + name + " coulnd't be found in " + o.GetType());
            this._type = info.FieldType;
        }

        public object GetValue()
        {
            return info.GetValue(obj);
        }
    }

    [System.Serializable]
    internal class ConsoleData
    {
        public List<ConsoleMessage> output_Messages;// = new List<ConsoleMessage>();
        public List<ConsoleThreadDebug> output_ThreadDebug;// = new List<ConsoleThreadDebug>();
        public List<ConsoleVariableDebug> output_VariableDebug;// = new List<ConsoleVariableDebug>();
    }
}