﻿using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using System.IO;
using System;

namespace MassiveStudio.Extensions
{
    public class PSDHandler : MonoBehaviour
    {
        public PSD_Reader reader;
    }

    public class PSDWindow : EditorWindow
    {
        public static PSD_Reader reader;
        public string path;

        [MenuItem("MassiveStudio/PSD")]
        public static void Init()
        {
            PSDWindow window = GetWindow<PSDWindow>();
            window.Show();
        }

        void OnGUI()
        {
            if (GUI.Button(new Rect(0, 0, 100, 50), "Open"))
            {
                path = EditorUtility.OpenFilePanelWithFilters("Load PSD File", Application.dataPath, new[] { "PSD", "psd" });
                try
                {
                    reader = new PSD_Reader(path);
                }
                catch (Exception e)
                {
                    Debug.LogError("Error: " + e.Message + "\n" + e.StackTrace);
                }
                finally
                {
                    GameObject o = new GameObject();
                    PSDHandler handler = o.AddComponent<PSDHandler>();
                    handler.reader = reader;

                    Selection.activeObject = handler;
                }
            }

            if(reader!= null)
            {

            }

        }
    }

    [Serializable]
    public class PSD_Reader
    {
        public string path;
        public PSD psd;

        public PSD_Reader(string path)
        {

            this.path = path;
            psd = new PSD();
            using (var fs = new FileStream(path, FileMode.Open))
            {
                using (BigEndianBinaryReader reader = new BigEndianBinaryReader(fs))
                {
                    ReadHeader(reader);
                    ReadColorModeData(reader);
                    ReadImageResource(reader);
                    ReadLayerMaskBlock(reader);
                }
            }
        }

        public void ReadHeader(BinaryReader reader)
        {
            Debug.Log("ReadHeader");
            psd.header.Signature = System.Text.Encoding.Default.GetString(reader.ReadBytes(4));
            Debug.Log(" Signature: " + psd.header.Signature);
            psd.header.version = reader.ReadInt16();
            Debug.Log("Version: " + psd.header.version);
            reader.BaseStream.Position += 6;

            psd.header.channels = reader.ReadInt16();
            Debug.Log("Channels: " + psd.header.channels);

            psd.header.height = reader.ReadInt32();
            psd.header.width = reader.ReadInt32();
            psd.header.depth = reader.ReadInt16();
            Debug.Log("Size: " + psd.header.width + "/" + psd.header.height + " : " + psd.header.depth);
            psd.header.mode = (PSDHeader.ColorMode)reader.ReadInt16();
            Debug.Log(" Mode: " + psd.header.mode);

        }

        public void ReadColorModeData(BinaryReader reader)
        {
            Debug.Log("ReadColorModeData");
            //if (psd.header.mode == PSDHeader.ColorMode.Indexed || psd.header.mode == PSDHeader.ColorMode.Duotone)
            {
                psd.colorModeData.length = reader.ReadInt32();
                Debug.Log("\tLength: " + psd.colorModeData.length);
                if (psd.colorModeData.length > 0)
                {
                    psd.colorModeData.colordata = reader.ReadBytes(psd.colorModeData.length);
                }
            }
        }

        public void ReadImageResource(BinaryReader reader)
        {
            Debug.Log("ReadImageResource");
            psd.imageResources.length = reader.ReadInt32();
            Debug.Log("\tLength: " + psd.imageResources.length);
            if (psd.imageResources.length > 0)
            {
                Debug.Log(" Signature: " + System.Text.Encoding.Default.GetString(reader.ReadBytes(4)));
                psd.imageResources.imageResource = reader.ReadBytes(psd.imageResources.length - 4);
            }
        }

        public void ReadLayerMaskBlock(BinaryReader reader)
        {
            Debug.Log("ReadLayerMaskBlock");
            psd.layerMask.length = reader.ReadInt32();
            Debug.Log("\tLength: " + psd.layerMask.length);
            if (psd.layerMask.length > 0)
            {
                psd.layerMask.layerInfo = new LayerInfo();
                ReadLayerInfo(reader);
            }
        }

        public void ReadLayerInfo(BinaryReader reader)
        {
            Debug.Log("ReadLayerInfo");
            LayerInfo layer = new LayerInfo();
            layer.length = reader.ReadInt32();
            layer.Layercount = reader.ReadInt16();
            layer.records = new LayerRecords[layer.Layercount];
            Debug.Log("\tLayers: " + layer.Layercount);

            for (int l = 0; l < layer.Layercount; l++)
            {
                Debug.Log("LayerRecords");

                LayerRecords record = new LayerRecords();
                record.top = reader.ReadInt32();
                record.left = reader.ReadInt32();
                record.bottom = reader.ReadInt32();
                record.right = reader.ReadInt32();

                record.channels = reader.ReadInt16();

                Debug.Log("\tChannels: " + record.channels);
                if (record.channels > 4)
                {
                    Debug.LogError("CHANNEL OUT OF BOUNDS");
                    throw new System.Exception("ERROR");
                }

                record.channelInformation = new ChannelInformation[record.channels];
                for (int c = 0; c < record.channels; c++)
                {
                    Debug.Log("ChannelInformation");

                    ChannelInformation channel = new ChannelInformation();
                    channel.channelID = reader.ReadInt16();
                    channel.length = reader.ReadInt32();
                    record.channelInformation[c] = channel;
                }

                record.blendMode = System.Text.Encoding.Default.GetString(reader.ReadBytes(4));
                record.blendModeKey = System.Text.Encoding.Default.GetString(reader.ReadBytes(4));
                record.Opacity = reader.ReadByte();
                record.clipping = reader.ReadByte() == 255;
                record.Flags = reader.ReadByte();

                reader.ReadByte();

                record.sizeofExtraFields = reader.ReadInt32();
                reader.ReadBytes(record.sizeofExtraFields);
                layer.records[l] = record;

                Debug.Log("Readed Layer: " + l + "\n" + "Flags: " + record.Flags +" : "+ record.ShiftedFlags + " LayerEnabled " + record.enabled);
            }

            psd.layerMask.layerInfo = layer;
        }
    }

    [Serializable]
    public class PSD
    {
        public PSDHeader header;
        public ColorModeData colorModeData;
        public ImageResources imageResources;
        public LayerMaskBlock layerMask;
        public ImageData imageData;

        public PSD()
        {
            header = new PSDHeader();
            colorModeData = new ColorModeData();
            imageResources = new ImageResources();
            layerMask = new LayerMaskBlock();
        }
    }

    [Serializable]
    public struct PSDHeader
    {
        public string Signature;//4bytes
        public short version;//2bytes
        //6 Bytes Reserved;
            int _fill1;
            short _fill2;
        public short channels;
        public int height;
        public int width;
        public short depth;
        public ColorMode mode;

        public enum ColorMode : short
        {
            Bitmap = 0,
            Grayscale = 1,
            Indexed = 2,
            RGB = 3,
            CMYK = 4,
            Multichannel = 7,
            Duotone = 8,
            Lab = 9
        }
    }

    [Serializable]
    public class ColorModeData
    {
        public int length;
        public byte[] colordata;
    }

    [Serializable]
    public class ImageResources
    {
        public int length;
        public byte[] imageResource;
        //public ImageResource imageResource;
    }

    [Serializable]
    public class ImageResource
    {
        string Signature;//4bytes
        short id;
        string name;
        byte[] resourcedata;
    }

    [Serializable]
    public class LayerMaskBlock
    {
        public int length;
        public LayerInfo layerInfo;
    }

    [Serializable]
    public class LayerInfo
    {
        public int length;
        public short Layercount;
        public LayerRecords[] records;

    }

    [Serializable]
    public class ImageData
    {
        public enum Compressionmethod :uint
        {
            Raw =0,
            RLE=1, //compressed the image data starts with the byte counts for all the scan lines(rows* channels), with each count stored as a two-byte value.The RLE compressed data follows, with each scan line compressed separately. The RLE compression is the same compression algorithm used by the Macintosh ROM routine PackBits , and the TIFF standard.
            ZIP=2, //without prediction
            ZIPw =3//with prediction.
        }

        Compressionmethod compressionMethod;

        public PixelData pixelData;

    }

    public class PixelData
    {

    }

    [Serializable]
    public class LayerRecords
    {
        public int top;
        public int left;
        public int bottom;
        public int right;

        public short channels;

        public ChannelInformation[] channelInformation;
        public string blendMode;//4 bytes; 8BIM
        public string blendModeKey;//4 bytes;
        public byte Opacity;
        public bool clipping;
        public byte Flags;
        private enum FlagValues
        {
            TransparencyProtected = 1,
            Invisible = 2,
            Obsolete = 4,
            Bit4Used = 8, //1 for Photoshop 5.0 and later, tells if bit 4 has useful information
            PixelDataIrrelevant = 16 //pixel data irrelevant to appearance of document
        }
        public string ShiftedFlags
        {
            get
            {
                string s = "";
                for (int i = 0; i < 8; i++)
                    s += (Flags & (1 << i));
                return s;
            }
        }

        public static Dictionary<string, string> BlendMode = new Dictionary<string, string>()
        {
            {"pass","pass through" },
            {"norm","normal" },
            {"diss","dissolve" },
            {"dark","darken" },
            {"mul ","multiply" },
            {"idiv","color burn" },
            {"lbrn","linear burn" },
            {"lite","lighten" },
            {"scrn","screen" },
            {"div ","color dodge" },
            {"lddg","linear dodge" },
            {"lgCl","lighter color" },
            {"over","overlay" },
            {"sLit","soft light" },
            {"hLit","hard light" },
            {"vLit","vivid light" },
            {"lLit","linear light" },
            {"pLit","pin light" },
            {"hMix","hard mix" },
            {"diff","difference" },
            {"smud","exclusion" },
            {"fsub","subtract" },
            {"fdiv","divide" },
            {"hue ","hue" },
            {"sat ","saturation" },
            {"colr","color" },
            {"lum ","luminosity" },
            {"dark","darken" },
        };

        /*Flags:
            bit 0 = transparency protected;
            bit 1 = visible;
            bit 2 = obsolete;
            bit 3 = 1 for Photoshop 5.0 and later, tells if bit 4 has useful information;
            bit 4 = pixel data irrelevant to appearance of document
        */
        public int sizeofExtraFields;

        Layermask layermask;
        LayerBlendingRanges layerBlendingRanges;
        string layername;

        public bool enabled
        {
            get
            {
                return  ((Flags & (1 << (int)FlagValues.Invisible)) !=0);
            }
            set
            {
                if (value)
                {
                    //left-shift 1, then bitwise OR
                    Flags = (byte)(Flags | (1 << (int)FlagValues.Invisible));
                }
                else
                {
                    //left-shift 1, then take complement, then bitwise AND
                    Flags = (byte)(Flags & ~(1 << (int)FlagValues.Invisible));
                }
            }
        }

    }

    [Serializable]
    public class Layermask
    {

    }

    [Serializable]
    public class LayerBlendingRanges {
    }

    [Serializable]
    public class ChannelInformation
    {
        public short channelID;
        public int length;
    }




    class BigEndianBinaryReader : BinaryReader
    {
        public BigEndianBinaryReader(System.IO.Stream stream) : base(stream) { }

        public override int ReadInt32()
        {
            var data = base.ReadBytes(4);
            Array.Reverse(data);
            return BitConverter.ToInt32(data, 0);
        }

        public override Int16 ReadInt16()
        {
            var data = base.ReadBytes(2);
            Array.Reverse(data);
            return BitConverter.ToInt16(data, 0);
        }

        public override Int64 ReadInt64()
        {
            var data = base.ReadBytes(8);
            Array.Reverse(data);
            return BitConverter.ToInt64(data, 0);
        }

        public override UInt32 ReadUInt32()
        {
            var data = base.ReadBytes(4);
            Array.Reverse(data);
            return BitConverter.ToUInt32(data, 0);
        }

    }
}
