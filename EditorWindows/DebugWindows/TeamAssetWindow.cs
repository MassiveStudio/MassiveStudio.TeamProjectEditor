﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor.EditorWindows.DebugWindows
{
    internal class TeamAssetWindow : EditorWindow
    {
        public static TeamAssetWindow window = null;
        public static int selectedType = 0;

        public readonly string[] titles = new string[6] { "Material", "Script", "Shader", "Texture", "SceneAssets", "TeamObjects" };

    [MenuItem("Team/AssetWindow")]
        public static void Init()
        {
            window = GetWindow<TeamAssetWindow>();
            window.titleContent = new GUIContent("AssetWindow");
            window.Show();
        }

        [MenuItem("Team/Switch")]
        public static void SwitchSkin()
        {
            UnityEditorInternal.InternalEditorUtility.SwitchSkinAndRepaintAllViews();
        }
        public void OnGUI()
        {
            if (TeamServerConnection.connected)
            {
                if (TeamAssetSystem.TeamAssetCore.TeamAssetList == null)
                {
                    GUI.Label(new Rect(0, 0, Screen.width, 25), "AssetList is Null");
                    return;
                }

                //#DEBUG
                GUI.Box(new Rect(position.width - 100, position.height - 25, 100, 25), "Count: " + TeamAssetSystem.TeamAssetCore.TeamAssetList.ShareAssetCount);
                if (GUI.Button(new Rect(position.width - 200, position.height - 25, 100, 25), "Clear"))
                {
                    TeamAssetSystem.TeamAssetCore.TeamAssetList.Clear();
                    TeamSceneManager.TeamObjects.Clear();
                }
                if (GUI.Button(new Rect(position.width - 300, position.height - 25, 100, 25), "Save"))
                    TeamAssetSystem.TeamAssetCore.Save();
                if (GUI.Button(new Rect(position.width - 400, position.height - 25, 100, 25), "Load"))
                    TeamAssetSystem.TeamAssetCore.Load(TeamServerConnection.currentServer.currentScene);
                if (GUI.Button(new Rect(position.width - 500, position.height - 25, 100, 25), "Get Pos"))
                    for (int i = 0; i < TeamAssetSystem.TeamAssetCore.SceneAssets.Count; i++)
                        Debug.Log("OBJ: " + i + " POS: " + TeamAssetSystem.TeamAssetCore.SceneAssets[0].models[0].tran.pos);
                //END DEBUG
                int y = 50;

                selectedType = GUI.SelectionGrid(new Rect(0, 0, position.width, 50), selectedType, titles, 6);
                if (selectedType < 4)
                    foreach (ShareAsset c in TeamAssetSystem.TeamAssetCore.TeamAssetList.GetList((ShareAssetType)selectedType))
                    {
                        if(c!=null)
                            GUI.Box(new Rect(0, y, position.width, 25), (c.HasObject()?"NULL":c.obj.name) + "|\t" + c.localpath + "  has ID: " + (c.uuid == null ? "FALSE" : "TRUE") + "|\t" + (c.uuid != null ? c.uuid.ToString() : "") + "|\tloaded: " + c.loaded);
                        y += 30;
                    }
                else if(selectedType==4)
                {
                    if (TeamAssetSystem.TeamAssetCore.SceneAssets != null)
                        foreach (ModelContainer c in TeamAssetSystem.TeamAssetCore.SceneAssets)
                        {
                            if (c != null)
                                GUI.Box(new Rect(0, y, position.width, 25), "ID: " + (string.IsNullOrEmpty(c.serverid) ? "EMPTY" : c.serverid) + (c.models != null ? " ModelCount: " + c.models.Count : ""));
                            else
                                GUI.Box(new Rect(0, y, position.width, 25), "RESERVED SLOT");
                            y += 30;
                        }
                }
                else
                {
                    if (TeamSceneManager.TeamObjects != null)
                        foreach (TeamView c in TeamSceneManager.TeamObjects)
                        {
                            if (c != null)
                            {
                                if (GUI.Button(new Rect(0, y, position.width, 25), "ID: " + (string.IsNullOrEmpty(c.id.ServerID) ? "EMPTY" : c.id.ServerID)))
                                    EditorGUIUtility.PingObject(c.gameObject);
                            }
                            else
                                GUI.Box(new Rect(0, y, position.width, 25), "RESERVED SLOT");
                            y += 30;
                        }
                }
                /*
                for (int i = 0; i < TeamAssetSystem.TeamAssetCore.assets.Count; i++)
                {
                    GUI.Box(new Rect(0, y, position.width, 25), TeamAssetSystem.TeamAssetCore.assets[i].obj.name+" "+ TeamAssetSystem.TeamAssetCore.assets[i].localpath + "  has ID: " + (TeamAssetSystem.TeamAssetCore.assets[i].uuid == null ? "FALSE" : "TRUE") + "  " + (TeamAssetSystem.TeamAssetCore.assets[i].uuid != null ? TeamAssetSystem.TeamAssetCore.assets[i].uuid.ToString() : ""));
                    y++;
                }*/
            }
            else
            {
                GUI.Label(new Rect(0, 0, position.width, 25), "Team session not Initialized -> First need to connect to a server");
            }
        }
    }
}