﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MassiveStudio.TeamProjectEditor
{
    [Obsolete("Use ClientRequest.MaterialRegister instead")]
    [System.Serializable]
    public class MaterialRegisterRequest
    {
        //public AssetUUID uuid; //Inside of MaterialData
        public MaterialData data;

        public MaterialRegisterRequest(MaterialData da)
        {
            //this.uuid = id;
            this.data = da;
        }
    }
}
