﻿using UnityEngine;
using System.Collections;
using System;

namespace MassiveStudio.TeamProjectEditor
{ 
    [Serializable]
    [Obsolete("Use ClientRequest instead")]
    public class TeamViewRegisterRequest : System.Object
    {
        public string objectID = "";

        public TeamViewRegisterRequest() { }
        public TeamViewRegisterRequest(string id)
        {
            this.objectID = id;
        }
    }
}