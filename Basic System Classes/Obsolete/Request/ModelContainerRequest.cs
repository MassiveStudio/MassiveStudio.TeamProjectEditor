﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MassiveStudio.TeamProjectEditor
{

    [System.Serializable]
    [Obsolete("Use ClientRequest instead")]
    public class ModelContainerRequest
    {
        public ObjectIdentify id;

        public ModelContainerRequest(ObjectIdentify i)
        {
            id = i;
        }
    }
}
