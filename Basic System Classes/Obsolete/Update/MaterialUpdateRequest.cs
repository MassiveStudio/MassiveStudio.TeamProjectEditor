﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor
{
    [Obsolete("Use ClientRequest instead")]
    [System.Serializable]
    public class MaterialUpdateRequest
    {
        public string ServerAssetID;
        public MaterialData data;
        public long synctime;
        public MaterialUpdateRequest(MaterialData MaterialData, string ServerId)
        {
            this.data = MaterialData;
            this.ServerAssetID = ServerId;
            this.synctime = TeamSceneManager.GetCurrentTime();
            Debug.Log("Material Update Request " + "[" + synctime.ToString() + "]");
        }
    }
}
