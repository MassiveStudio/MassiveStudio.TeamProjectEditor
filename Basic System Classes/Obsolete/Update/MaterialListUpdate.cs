﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MassiveStudio.TeamProjectEditor
{
    [Serializable]
    public class MaterialListUpdate : System.Object
    {
        public List<MaterialData> mat = new List<MaterialData>();
    }
}
