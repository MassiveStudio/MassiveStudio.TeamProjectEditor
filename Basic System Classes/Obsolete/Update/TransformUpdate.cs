﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MassiveStudio.TeamProjectEditor
{
    [Obsolete("Use DataClass DataType.TransformUpdate instead")]
    [Serializable]
    public class TransformUpdate : System.Object
    {
        public List<TransformData> datas = new List<TransformData>();
        //public ObjectIdentify id = new ObjectIdentify();

	    public TransformUpdate() { }
    }
}
