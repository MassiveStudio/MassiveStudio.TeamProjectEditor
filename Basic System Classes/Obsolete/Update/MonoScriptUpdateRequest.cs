﻿using UnityEngine;
using System.Collections;

namespace MassiveStudio.TeamProjectEditor
{
    [System.Obsolete("Use ClientRequest instead")]
    public class MonoScriptUpdateRequest : System.Object
    {
        public MonoScript script;
        public MonoEvent monoevent;

        public MonoScriptUpdateRequest() { }
    }
}