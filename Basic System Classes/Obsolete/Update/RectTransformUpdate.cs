﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MassiveStudio.TeamProjectEditor
{
    [Obsolete("Use ClientRequest instead")]
    [System.Serializable]
    public class RectTransformUpdate
    {
        public List<TransformData> datas = new List<TransformData>();

        public RectTransformUpdate()
        {

        }
    }
}
