﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MassiveStudio.TeamProjectEditor
{
    [Obsolete("Use DataClass Datatype.GameObjectUpdate instead")]
    [Serializable]
    public class GameObjectUpdate : System.Object
    {
        public GameObjectData data;
        //public ObjectIdentify id;
    
        public GameObjectUpdate(){ }
    }
}
