﻿using UnityEngine;
using System.Collections;
using System;

namespace MassiveStudio.TeamProjectEditor
{
    [Obsolete("Use DataClass DataType.ModelContainerUpdate instead")]
    [System.Serializable]
    public class ModelContainerUpdate : System.Object
    {
        public ModelContainer data;
        public ObjectIdentify id;//ServerID

        public ModelContainerUpdate() { }
    }
}
