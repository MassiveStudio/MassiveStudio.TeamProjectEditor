﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MassiveStudio.TeamProjectEditor
{
    [Obsolete( "Use DataClass DataType.ComponentUpdate instead")]
    [Serializable]
    public class ComponentUpdateRequest : System.Object
    {
        //public ObjectIdentify id;
        public List<ObjectComponent> com = new List<ObjectComponent>();

	    public ComponentUpdateRequest() { }
    }
}
