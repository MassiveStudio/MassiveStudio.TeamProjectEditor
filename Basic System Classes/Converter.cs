﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEditor;
//
using UnityEngine;
using MassiveStudio.TeamProjectEditor.TeamAssetSystem;
using MassiveStudio.TeamProjectEditor.Utility;
using MassiveStudio.TeamProjectEditor.Attributes;
namespace MassiveStudio.TeamProjectEditor
{
    static class Converter
    {
        [Obsolete("Will be moved to C++")]
        public static Texture2D ToTexture(System.Drawing.Bitmap bitmap)
        {
            Texture2D tex = new Texture2D(bitmap.Width, bitmap.Height, TextureFormat.ARGB4444, false);
            for (int x = 0; x < bitmap.Width; x++)
            {
                for (int y = 0; y < bitmap.Height; y++)
                {
                    System.Drawing.Color col = bitmap.GetPixel(x, y);
                    UnityEngine.Color c = new UnityEngine.Color(1f / 255f * col.R, 1f / 255f * col.G, 1f / 255f * col.B, 1f / 255f * col.A);
                    tex.SetPixel(x, bitmap.Height - y, c);
                }
            }
            tex.Apply();
            return tex;
        }

        [Obsolete("NOT IMPLEMENTED")]
        public static MonoScript ToMonoScript(string file)
        {
            MonoScript script = new MonoScript();
            script.scriptname = Path.GetFileNameWithoutExtension(file);
            script.data = File.ReadAllBytes(file);
            script.creator = TeamServerConnection.currentServer.user.Username;
            return script;
        }

        [Obsolete("NOT IMPLEMENTED")]
        public static void ToScript(MonoScript script)
        {
            File.WriteAllBytes(TeamSceneManager.DataPath + "TeamAssets/Scripts/" + script.scriptname + ".cs", script.data);
        }

        [OptimizedForBackroundMainDispatch]
        public static MaterialData ToMaterialData(Material mat)
        {
            if (mat != null)
            {
                MaterialData data = new MaterialData();
                Utility.Dispatcher.ToMainThread(() =>
                {
                    data.name = mat.name;
                    if (TeamProjectEditor.TeamSettings.isDebug)
                       Console.TeamConsole.Message("Create UUID " + mat.name, "Converter", Color.yellow, true, false);
                    //Debug.LogWarning("Create UUID " + mat.name);

                    data.mtUUID = TeamAssetSystem.TeamAssetCore.GetAssetUUID(mat);
                    if (TeamProjectEditor.TeamSettings.isDebug)
                        Console.TeamConsole.Message("Material UID: " + "[" + data.mtUUID.UUID + "]", "Converter", Color.yellow, true, false);
                    //Debug.LogWarning("Material UID: " + "[" + data.mtUUID.UUID + "]");

                    data.shader = TeamAssetSystem.TeamAssetCore.GetAssetUUID(mat.shader);
                    if (TeamProjectEditor.TeamSettings.isDebug)
                        Console.TeamConsole.Message("Shader UID: " + "[" + data.shader.UUID + "]", "Converter", Color.yellow, true, false);
                    //Debug.LogWarning("Shader UID: " + "[" + data.shader.UUID + "]");

                    if (TeamProjectEditor.TeamSettings.isDebug)
                        Console.TeamConsole.Message("Create Shader Data", "Converter", Color.yellow, true, false);
                    //Debug.LogWarning("Create Shader Data");
                    data.shaderdata = DataConverter.PackMaterial(mat);
                    if (TeamProjectEditor.TeamSettings.isDebug)
                        Console.TeamConsole.Message("Shader Data created", "Converter", Color.yellow, true, false);
                    //Debug.LogWarning("Shader Data created");
                });
                return data;
            }
            else
            {
                Debug.LogError("NULL MATERIAL");
                return null;
            }
        }

        [OptimizedForBackroundMainDispatch]
        public static AssetUUIDs ToMaterialDataList(Material[] mats)
        {
            AssetUUIDs uids = new AssetUUIDs();
            //List<MaterialData> datas = new List<MaterialData>();
            List<string> datas = new List<string>();

            AssetUUID temp = null;
            string apath;
            if (TeamProjectEditor.TeamSettings.isDebug)
                Console.TeamConsole.Message("MATERIAL LIST TO BAKE " + mats.Length, "Converter", Color.white, true, false);
            //Debug.Log("MATERIAL LIST TO BAKE " + mats.Length);

            Utility.Dispatcher.ToMainThread(() =>
            {
                foreach (Material mat in mats)
                {
                    if (mat != null)
                    {
                        apath = AssetDatabase.GetAssetPath(mat);
                        if (apath.StartsWith("Resources"))//mat.shader.name == "Standard")
                        {
                            datas.Add("-1");
                        }
                        else
                        {
                            Console.TeamConsole.Message("Material Name " + mat.name, "Converter", Color.cyan, true, false);

                            temp = TeamAssetSystem.TeamAssetCore.GetAssetUUID(mat);
                            Console.TeamConsole.Message("MaterialID " + temp.ToString(), "Converter", Color.yellow, true, false);
                            //Debug.LogWarning("MaterialID " + temp.ToString());

                            if (temp.HasTeamID())
                                datas.Add(temp.ServerID.ToString());
                            else
                                datas.Add(temp.UUID);
                        }
                    }
                    else
                        datas.Add(string.Empty);
                    /*
                    if (mat != null)
                        datas.Add(ToMaterialData(mat));
                    else
                        datas.Add(null);*/
                    //Debug.LogError("NULL MATERIAL IN LIST");
                }
            });

            if (TeamProjectEditor.TeamSettings.isDebug)
                Console.TeamConsole.Message("Materials Baked: " + datas.Count, "Converter", Color.white, true, false);
            //Debug.Log("Materials Baked: " + datas.Count);

            uids.uids = datas;

            return uids;
        }

        [OptimizedForBackroundMainDispatch]
        public static Material[] ToMaterialList(AssetUUIDs mats)
        {

            Material[] datas = null;
            Utility.Dispatcher.ToMainThread(() =>
            {
                datas = new Material[mats.uids.Count];
            });

            int c = 0;
            foreach (string ServerID in mats.uids)
            {
                if (TeamProjectEditor.TeamSettings.isDebug)
                    Console.TeamConsole.Message("Mat: " + c + " Count: " + datas.Length, "Converter", Color.white, true, false);
                //Debug.Log("Mat: " + c + " Count: " + datas.Length);
                if (ServerID == string.Empty)
                    datas[c] = null;
                else if (ServerID == "-1")
                    datas[c] = (Material)TeamAssetCore.CreateDefault(ShareAssetType.Material);
                else
                {
                    datas[c] = TeamAssetSystem.TeamAssetCore.GetAsset<Material>(ServerID, ShareAssetType.Material);
                    //datas[c] = (ToMaterial(mat));
                }
                c++;
            }
            return datas;
        }

        [OptimizedForBackroundMainDispatch]
        public static Material ToMaterial(MaterialData data)
        {
            if (data == null)
            {
                Debug.LogError("NULL MATERIALDATA!");
                return null;
            }

            //Shader shader = TeamAssetSystem.TeamAssetCore.GetAsset<Shader>(data.shader.ServerID, ShareAssetType.Shader);
            ShareAsset shader = TeamAssetCore.CheckExists(data.shader.ServerID, ShareAssetType.Shader, AssetSearchOption.ServerID);
            if (shader == null)
                shader = TeamAssetCore.CreateNewShareAsset(TeamAssetCore.CreateDefault(ShareAssetType.Shader), data.mtUUID, true);
            if (!shader.HasObject())
                shader.obj = TeamAssetCore.CreateDefault(ShareAssetType.Shader);

            if (TeamProjectEditor.TeamSettings.isDebug)
                Console.TeamConsole.Message("Shader Found", "Converter", Color.white, true, false);
            //Debug.LogWarning("Shader Found");

            //Material m = new Material(shader);
            //Debug.Log("MATERIAL " + (m == null ? "NULL" : "NOTNULL"));

            //if (TeamProjectEditor.TeamSettings.isDebug)
            //    Debug.Log("Register");

            ShareAsset mat = TeamAssetCore.CheckExists(data.mtUUID.ServerID, ShareAssetType.Material, AssetSearchOption.ServerID);

            if (mat == null)
                mat = TeamAssetCore.CreateNewShareAsset(TeamAssetCore.CreateDefault(ShareAssetType.Material), data.mtUUID, true);
            if (!mat.HasObject())
                mat.obj = TeamAssetCore.CreateDefault(ShareAssetType.Material);

            if (TeamProjectEditor.TeamSettings.isDebug)
                Console.TeamConsole.Message("Material Found", "Converter", Color.white, true, false);
            //Debug.LogWarning("Material Found");

            Material m = (Material)mat.obj;

            Utility.Dispatcher.ToMainThread(() =>
            {
                m.shader = (Shader)shader.obj;
                m.name = data.name;

                if (TeamProjectEditor.TeamSettings.isDebug)
                    Console.TeamConsole.Message("Unpack Done " + ((m == null) ? "NULL" : "NOTNULL"), "Converter", Color.white, true, false);
                //Debug.Log("Unpack Done " + ((m == null) ? "NULL" : "NOTNULL"));
            });
            //TeamAssetSystem.TeamAssetCore.GetAsset<Material>(data.mtUUID.ServerID, ShareAssetType.Material);
            //m = (Material)TeamAssetSystem.TeamAssetCore.UpdateAssetLocal(m, data.mtUUID);

            /*string[] s = data.shader.UUID.Split("_"[0]);
            if (m.shader.name != "Standard")
            {*/
            //            m = (Material)TeamAssetSystem.TeamAssetCore.RegisterAsset(m, data.mtUUID);
            if (TeamProjectEditor.TeamSettings.isDebug)
                Console.TeamConsole.Message("Unpack", "Converter", Color.white, true, false);
            //Debug.Log(" Unpack");
            m = DataConverter.UnpackMaterial(data.shaderdata, m);
            /* }
             else
                 Debug.LogError("Expected Shader not Found: " + s[s.Length-1] + "  " + m.shader.name);
             */

            mat.loaded = true;
            return m;
        }

        [OptimizedForBackroundMainDispatch]
        internal static ShaderData ToShaderData(Shader s)
        {
            ShaderData shader = new ShaderData();
            shader.uuid = TeamAssetSystem.TeamAssetCore.GetAssetUUID(s);

            string path = "";
            string name = "";
            Utility.Dispatcher.ToMainThread(() =>
            {
                path = AssetDatabase.GetAssetPath(s);
                name = s.name;
            });

            Debug.Log("PATH " + path);
            if (!path.StartsWith("Standard"))
            {
                if (File.Exists(path))
                {
                    shader.data = System.IO.File.ReadAllText(path);
                    Console.TeamConsole.Message("Shader Reader " + "\n" + shader.data, "Converter", new UnityEngine.Color(50, 250, 10));
                    Debug.Log("READED SHADER DATA: " + shader.data);
                }
                else
                    Debug.LogError("FILE " + path + " Not exists");

                shader.name = Path.GetFileName(path);
            }
            else
            {
                shader.name = name;
            }
            return shader;
        }

        [OptimizedForBackroundMainDispatch]
        internal static Shader ToShader(ShaderData data)
        {
            Shader s = null;

            if (data.uuid.UUID.StartsWith("Standard"))
            {
                Dispatcher.ToMainThread(() =>
                {
                    s = Shader.Find("Standard");
                });
                return s;
            }


            //if (!File.Exists(@"Assets/ServerLibraryFiles/" + data.uuid.UUID))
            File.WriteAllText("Assets/ServerLibraryFiles/" + data.uuid.UUID, data.data);
            Debug.LogError("Shader Written " + "\n" + data.data);
            Console.TeamConsole.Message("Shader Written " + "\n" + data.data, "Converter", new UnityEngine.Color32(50, 250, 10, 100));
            /*
            using (FileStream stream = new FileStream(@"Assets/ServerLibraryFiles/" + data.uuid.UUID, FileMode.OpenOrCreate))
            {
                stream.Write(data.data, 0, data.data.Length);
            }*/
            Dispatcher.ToMainThread(() =>
            {
                //Temporary Shader /not Reload of All Assets need -> Freeze Unity Editor
                s = UnityEditor.ShaderUtil.CreateShaderAsset(data.data);
                s = (Shader)TeamAssetSystem.TeamAssetCore.UpdateAssetLocal(s, data.uuid);
            });

            return s;
        }

        [OptimizedForBackroundMainDispatch]
        public static ObjectIdentify ToObjectInfo(Transform t)
        {
            ObjectIdentify info = null;
            Utility.Dispatcher.ToMainThread(() =>
            {
                if (t.GetComponent<TeamView>() != null)
                    info = t.GetComponent<TeamView>().id;
                else if (t.GetComponent<TeamObject>() != null)
                    info = t.GetComponent<TeamObject>().id;
            });
            return info;
        }

        [OptimizedForBackroundMainDispatch]
        internal static TextureData ToTextureData(Texture2D t)
        {
            ShareAsset asset = TeamAssetSystem.TeamAssetCore.GetAssetFromObject(t);
            TextureData data = new TextureData();
            if (File.Exists(asset.localpath))
            {
                using (FileStream file = new FileStream(asset.localpath, FileMode.Open))
                {
                    using (BinaryReader reader = new BinaryReader(file))
                    {
                        data.data = new byte[reader.BaseStream.Length];
                        reader.Read(data.data, 0, (int)reader.BaseStream.Length);
                    }
                }
            }
            data.uuid = asset.uuid;
            return data;
        }

        [OptimizedForBackroundMainDispatch]
        public static Texture2D ToTexture(TextureData data)
        {
            Texture2D t = null;

            using (FileStream stream = new FileStream(@"Assets/ServerLibraryFiles/" + data.uuid.UUID, FileMode.OpenOrCreate))
            {
                stream.Write(data.data, 0, data.data.Length);
            }
            Utility.Dispatcher.ToMainThread(() =>
            {
                t = new Texture2D(10, 10);//Replac with Loading Icon later
            });
            //Temporary Shader /not Reload of All Assets need -> Freeze Unity Editor
            t = (Texture2D)TeamAssetSystem.TeamAssetCore.UpdateAssetLocal(t, data.uuid);
            return t;
        }

        [OptimizedForBackroundMainDispatch]
        public static ModelContainer GameObjectToModel3D(Transform obj)
        {
            ModelContainer container = new ModelContainer();
            List<Transform> transforms = new List<Transform>();

            //ROOT OBJ
            //transforms.Add(obj);
            // Debug.Log("Childs: "+obj.GetComponentsInChildren(typeof(Transform)).Length);
            Utility.Dispatcher.ToMainThread(() =>
            {
                foreach (Transform child in obj.GetComponentsInChildren<Transform>())
                    transforms.Add(child);
            });

            Model3d modn;
            foreach (Transform a in transforms)
            {
                modn = ToModel3d(a);
                container.models.Add(modn);
            }

            Utility.Dispatcher.ToMainThread(() =>
            {
                container.serverid = obj.GetComponent<TeamView>().id.ServerID;
            });

            if (TeamProjectEditor.TeamSettings.isDebug)
                Debug.Log(container.models.Count);
            return container;
        }

        [OptimizedForBackroundMainDispatch]
        public static ModelContainer GameObjectToModel3D(TeamView obj)
        {
            ModelContainer container = new ModelContainer();
            List<Transform> transforms = new List<Transform>();


            Utility.Dispatcher.ToMainThread(() =>
            {
                //ROOT OBJ
                //transforms.Add(obj);
                // Debug.Log("Childs: "+obj.GetComponentsInChildren(typeof(Transform)).Length);
                foreach (Transform child in obj.GetComponentsInChildren<Transform>())
                    transforms.Add(child);
            });

            Model3d modn;
            foreach (Transform a in transforms)
            {
                modn = ToModel3d(a);
                container.models.Add(modn);
            }

            container.serverid = obj.id.ServerID;

            if (TeamProjectEditor.TeamSettings.isDebug)
                Debug.Log(container.models.Count);
            return container;
        }

        [OptimizedForBackroundMainDispatch]
        public static Model3d ToModel3d(Transform t)
        {
            Model3d model = new Model3d();
            GameObject @object = null;
            try
            {
                Dispatcher.ToMainThread(() =>
                {
                    if (t.GetComponent<TeamView>() == null && t.GetComponent<TeamObject>() == null)
                        t.root.GetComponent<TeamView>().SetIDs();

                    //EDITOR ATTRIBUTE
                    if (t.GetComponent<TeamObject>() != null)
                        model.id = t.GetComponent<TeamObject>().id;
                    else if (t.GetComponent<TeamView>() != null)
                        model.id = t.GetComponent<TeamView>().id;
                    else
                        Debug.LogError("NO TEAM  COMPONENT FOUND ON " + t.transform.name + " | " + t.gameObject.GetInstanceID());

                    @object = t.gameObject;
                });
            }
            catch (System.Exception e)
            {
                Debug.LogError("MODEL3d " + e.Message);
            }

            //TRANSFORM
            model.tran = ToTransformData(t);
            model.game = ToGameObjectData(@object);//t.gameObject

            //GEOMETRY DATA
            MeshFilter filter = null;
            bool isnull = false;
            Dispatcher.ToMainThread(() =>
            {
                filter = t.GetComponent<MeshFilter>();
                isnull = (filter == null);
            });

            if (!isnull)
            {
                model.geo = new MeshData();
                model.geo.ToMeshData(filter);
            }

            //COMPONENTS
            Component[] comps = null;

            Dispatcher.ToMainThread(() => {
                comps = t.GetComponents<Component>();
            });

            model.comp.AddRange(DataConverter.BakeComponents(comps, t));

            return model;
        }

        [OptimizedForBackroundMainDispatch]
        public static TeamView Model3DToGameObject(ModelContainer container)//GameObject
        {
            //var starttime:float = Time.realtimeSinceStartup;
            List<ObjectID> objs = new List<ObjectID>();
            Debug.Log(" MODEL COUNT: "+ container.models.Count);
            if (container.models != null && container.models.Count > 0)
            {
                foreach (Model3d a in container.models)
                {
                    Debug.Log("     Model gefunden "+ a.tran.pos); 
                    GameObject obj = null;
                    Transform otran = null;

                    Utility.Dispatcher.ToMainThread(() =>
                    {
                        Debug.Log("     Create GameObject on MainThread");
                        obj = new GameObject();
                        otran = obj.transform;
                    });

                    Debug.Log("  Set Transform / GameObject data");

                    ToTransform(a.tran, otran);//obj.transform

                    ToGameObject(a.game, obj);

                    //GEOMETRY DATA
                    if (a.geo != null)
                    {
                        Mesh mesh = a.geo.ToMesh();
                        Utility.Dispatcher.ToMainThread(() =>
                        {
                            obj.AddComponent<MeshFilter>().sharedMesh = mesh;
                            //obj.AddComponent<MeshRenderer>().material = new Material(Shader.Find("Standard")); ->UnBackeComponents will add if exists
                        });
                    }

                    if (TeamProjectEditor.TeamSettings.isDebug)
                        Debug.Log("     UNPACK COMPONENETS " + a.comp.Count +" Elements");
                    DataConverter.UnBakeComponents(obj, a.comp);
                    if (TeamProjectEditor.TeamSettings.isDebug)
                        Debug.Log("     ADD OBJECT TO LIST");
                    objs.Add(new ObjectID(obj, a.id));
                }
                //Debug.Log("LOAD TRANSFORM TREE");
                GameObject root = RebuildTransformTree(objs);
                //Debug.Log("CREATE TEAM VIEW");
                TeamView view = null;

                Utility.Dispatcher.ToMainThread(() =>
                {
                    view = root.AddComponent<TeamView>();
                    view.hideFlags = HideFlags.DontSaveInEditor | HideFlags.DontSaveInBuild;
                });

                if (TeamProjectEditor.TeamSettings.isDebug)
                    Debug.Log(" Set TEAMVIEW Data");
                if (TeamProjectEditor.TeamSettings.isDebug)
                    Debug.Log(" Modelcontainer server id: " + container.serverid);
                view.id.ServerID = container.serverid;
                view.id.ObjectID = string.Empty;
                view.isnew = false;

                Utility.Dispatcher.ToMainThread(() =>
                {
                    view.Update();
                });

                objs.Clear();
                /*if (TeamProjectEditor.TeamSettings.isDebug)
                    Debug.Log("RETURN");*/

                Debug.Log(" Register");
                TeamSceneManager.Register(view);
                //Debug.LogWarning("LOAD MODEL TAKES "+ (Time.realtimeSinceStartup-starttime)+"sec");
                return view;
            }
            return null;
        }

        [OptimizedForBackroundMainDispatch]
        public static GameObject RebuildTransformTree(List<ObjectID> list)//ROOT OBJ
        {
            GameObject root = null;
            string _name = "";
            foreach (ObjectID a in list)
            {
                if (a.id == null)
                {
                    a.id.ObjectID = "";
                    a.id.ServerID = "";
                }

                if (a.id.ObjectID != String.Empty)//NOT THE ROOT OBJ
                {
                    //				Debug.Log("["+a.id.ToString()+"]");
                    if (a.id.ObjectID.IndexOf(":") == -1)
                        _name = "";//a.id.Substring(0, a.id.LastIndexOf(":")-1); 
                    else
                        _name = a.id.ObjectID.Substring(0, a.id.ObjectID.LastIndexOf(":"));
                    //				Debug.Log("NAME: "+_name+" / "+ a.id);
                    foreach (ObjectID b in list)
                    {
                        if (b.id.ObjectID == _name)
                        {
                            Utility.Dispatcher.ToMainThread(() =>
                            {
                                a.obj.transform.parent = b.obj.transform;
                            });
                            break;
                        }
                    }
                }
                else
                    root = a.obj;
            }
            return root;
        }

        [OptimizedForBackroundMainDispatch]
        public static TransformData ToTransformData(Transform t)
        {
            // Debug.Log("TYPE of TRANSFORM: " + t.GetType()+"  "+ t.gameObject.name);
            TransformData data = new TransformData();
            Utility.Dispatcher.ToMainThread(() =>
            {
                data.pos = t.position;
                data.rot = t.rotation.eulerAngles;
                data.scal = t.localScale;

                if (t.GetComponent<TeamObject>() != null)
                    data.parent = t.GetComponent<TeamObject>().id.ObjectID;
                else if (t.GetComponent<TeamView>() != null)
                    data.parent = t.GetComponent<TeamView>().id.ObjectID;
                else
                {
                    //Add and Initialize()
                    t.root.GetComponent<TeamView>().SetIDs();
                    if (t.GetComponent<TeamObject>() != null)
                        data.parent = t.GetComponent<TeamObject>().id.ObjectID;
                    else if (t.GetComponent<TeamView>() != null)
                        data.parent = t.GetComponent<TeamView>().id.ObjectID;
                    else
                    {
                        Debug.LogError("COULDN ADD TEAM");
                    }
                }
            });
            return data;
        }

        [OptimizedForBackroundMainDispatch]
        public static void ToTransform(TransformData data, Transform obj)
        {
            Utility.Dispatcher.ToMainThread(() =>
            {
                data.ToTransform(obj);
            });
        }

        [OptimizedForBackroundMainDispatch]
        public static RectTransformData ToRectTransformData(RectTransform r)
        {
            RectTransformData data = new RectTransformData();

            Utility.Dispatcher.ToMainThread(() =>
            {
                data.pos = r.position;
                data.rot = r.eulerAngles;
                data.scal = r.localScale;

                if (r.GetComponent<TeamObject>() != null)
                    data.parent = r.GetComponent<TeamObject>().id.ObjectID;
                else
                    data.parent = r.GetComponent<TeamView>().id.ObjectID;

                data.AnMin = r.anchorMin;
                data.AnMax = r.anchorMax;
                data.AnPos = r.anchoredPosition;

                data.SizDel = r.sizeDelta;
                data.Piv = r.pivot;
            });

            return data;
        }

        [OptimizedForBackroundMainDispatch]
        public static void ToRectTransform(RectTransformData data, RectTransform obj)
        {
            Utility.Dispatcher.ToMainThread(() =>
            {
                obj.position = data.pos;
                obj.rotation = Quaternion.Euler(data.rot);
                obj.localScale = data.scal;

                obj.anchorMin = data.AnMin;
                obj.anchorMax = data.AnMax;
                obj.anchoredPosition = data.AnPos;

                obj.sizeDelta = data.SizDel;
                obj.pivot = data.Piv;

                //data.ToRectTransform(obj);
            });
        }

        [OptimizedForBackroundMainDispatch]
        public static GameObjectData ToGameObjectData(GameObject g)
        {
            GameObjectData data = new GameObjectData();
            Utility.Dispatcher.ToMainThread(() =>
            {
                data.name = g.name;
                data.state = g.activeSelf;
                data.tag = g.tag;
                data.isstatic = g.isStatic;
                data.layer = g.layer;

            });
            //Debug.LogWarning("DATA: "+ data.name);
            return data;
        }

        [OptimizedForBackroundMainDispatch]
        public static void ToGameObject(GameObjectData data, GameObject obj)
        {
            //Debug.LogWarning("SET GAME OBJECT NAME");
            Utility.Dispatcher.ToMainThread(() =>
            {
                obj.name = data.name;
                obj.SetActive(data.state);
                if (data.tag != "")
                    obj.tag = data.tag;
                obj.isStatic = data.isstatic;
                obj.layer = data.layer;
            });
            // Debug.Log("GAMEObject Data SETTED");
        }

        [OptimizedForBackroundMainDispatch]
        public static List<TransformData> ToTransformDataList(Transform tr)
        {
            List<TransformData> transforms = new List<TransformData>();
            //The root obj
            transforms.Add(ToTransformData(tr));

            Utility.Dispatcher.ToMainThread(() =>
            {
                //The childs
                foreach (Transform t in tr)
                {
                    if (t == tr)
                        Debug.LogError("CONVERTER TOTRANSFORMDATALIST CONTAINS ROOT OBJ TWICE");
                    transforms.Add(ToTransformData(t));
                }
            });

            return transforms;
        }

        [OptimizedForBackroundMainDispatch]
        public static List<TransformData> ToRectTransformDataList(Transform tr)
        {
            List<TransformData> transforms = new List<TransformData>();

            Utility.Dispatcher.ToMainThread(() =>
            {

                //The root obj
                if (tr.GetComponent<RectTransform>() == null)
                    transforms.Add(ToTransformData(tr));
                else
                    transforms.Add(ToRectTransformData(tr.GetComponent<RectTransform>()));
                //The childs
                foreach (Transform t in tr)
                {
                    if (t == tr)
                        Debug.LogError("CONVERTER TOTRANSFORMDATALIST CONTAINS ROOT OBJ TWICE");
                    if (t.GetComponent<RectTransform>() == null)
                        transforms.Add(ToTransformData(t));
                    else
                        transforms.Add(ToRectTransformData(tr.GetComponent<RectTransform>()));
                }
            });
            return transforms;
        }
    }
}