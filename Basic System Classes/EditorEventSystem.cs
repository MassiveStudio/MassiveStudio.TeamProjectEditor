﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using UnityEditorInternal;

namespace MassiveStudio.TeamProjectEditor.SystemUtility
{
    //[InitializeOnLoad]
    internal class EditorEventSystem
    {
        [System.Serializable]
        public delegate void CallbackFunction();

        public static CallbackFunction OnDeserilization;
        public static CallbackFunction OnSerilization;

        public static UnityPlayModeState state = UnityPlayModeState.Stopped;

        public enum UnityPlayModeState
        {
            Stopped=0,
            EnteringPlaymode=1,
            Playing=2,
            Paused=3,
        }

        /*
        public EditorEventSystem()
        {
            EditorApplication.playmodeStateChanged -= OnPlayModeChanged;
            EditorApplication.playmodeStateChanged += OnPlayModeChanged;
        }
        */

        [InitializeOnLoadMethod]
        public static void Init()
        {
            EditorApplication.playmodeStateChanged -= OnPlayModeChanged;
            EditorApplication.playmodeStateChanged += OnPlayModeChanged;
        }

        private static void OnPlayModeChanged()
        {
            if (EditorApplication.isCompiling || EditorApplication.isUpdating || (!EditorApplication.isPlaying && EditorApplication.isPlayingOrWillChangePlaymode))//
            {
                state = UnityPlayModeState.Stopped;
                //Debug.Log("EVENT SYSTEM ON DESERIALIZATION");
                OnDeserilization();
            }
            else if (EditorApplication.isPlaying && EditorApplication.isPlayingOrWillChangePlaymode)//Exit Playmodes
            {
                state = UnityPlayModeState.EnteringPlaymode;
            }
            else if (EditorApplication.isPlaying)
                state = UnityPlayModeState.Playing;
        }

        public void OnDestroy()
        {
            EditorApplication.playmodeStateChanged -= OnPlayModeChanged;
        }
    }
}