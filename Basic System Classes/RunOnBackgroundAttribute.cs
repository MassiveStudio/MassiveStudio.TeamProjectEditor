﻿using System;

namespace MassiveStudio.TeamProjectEditor.Attributes
{
    internal class RunOnBackgroundAttribute : Attribute
    {
    }

    internal class RunOnMainThreadAttribute : Attribute
    {
    }

    internal class NeedsToRunOnMainThreadAttribute : Attribute
    {
    }

    internal class OptimizedForBackroundMainDispatch : RunOnBackgroundAttribute
    {
    }
}