﻿using MassiveStudio.TeamProjectEditor.Utility;

namespace MassiveStudio.TeamProjectEditor
{
    [System.Serializable]
    public class UserAccount : ListType
    {
        public string Username;
        public string Password;
        public string UserID;
        public bool valid;

        public string[] ToData()
        {
            return new string[] { UserID, Username };
        }

        public UserAccount()
        {
            Username = "";
            Password = "";
            UserID = "";
            valid = false;
        }
    }
}