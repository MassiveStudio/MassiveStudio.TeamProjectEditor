﻿using System;
using System.Collections.Generic;

namespace MassiveStudio.TeamProjectEditor.Chat
{
    [Serializable]
    public class ChatMembers
    {
        public List<ChatMember> member;

        public ChatMembers() {
            member = new List<ChatMember>();
        }

        public ChatMembers(List<ChatMember> l)
        {
            member = new List<ChatMember>();
            member = l;
        }
    }

    [Serializable]
    public class ChatMember
    {
        public string name;//Later id, stuff for direct chat, permission system,...

        public ChatMember(string n)
        {
            this.name = n;
        }
    }

    [Serializable]
    public class ChatRoom
    {
        public string RoomName;
        public int RoomID;//Set by the server
        public RoomPermision permission;

        public bool CheckPassword(string password)
        {
            return permission.CheckPassword(password);
        }

        public ChatRoom()
        {
            permission = new RoomPermision();
            RoomID = 0;
        }
    }

    [Serializable]
    public class ChatRoomList
    {
        public List<ChatRoom> rooms = new List<ChatRoom>();

    }

    [Serializable]
    public class RoomPermision
    {
        public ChatRoomState roomstate;//Can others see the Chat room?

        protected string roompassword;
        public string password
        {
            private get
            {
                return roompassword;
            }
            set
            {
                roompassword = value;
            }
        }

        public bool CheckPassword(string pass)
        {
            return (pass == password);
        }
    }

    [Serializable]
    public enum ChatRoomState
    {
        Public =0,
        Private=1,
        PrivateHidden=2
    };
}
