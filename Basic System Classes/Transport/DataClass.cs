﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MassiveStudio.TeamProjectEditor
{
    [Serializable]
    public class DataClass//Base class of all Scene Data (COMPONENT / MESH / TRANSFORM / GAMEOBJECT)
    {
        public string FileIdentify;
        public ObjectIdentify info;
        public object data;
        public int scene;
        public long syncTime;

        public DataType type;

        public DataClass() { }

        public DataClass(ObjectIdentify i, object o)
        {
            this.info = i;
            this.data = o;
            this.scene = TeamServerConnection.currentServer.currentScene;
            syncTime = DateTime.Now.Ticks;
        }

        public DataClass(string i, object o)
        {
            this.FileIdentify = i;
            this.data = o;
            this.scene = TeamServerConnection.currentServer.currentScene;
            syncTime = DateTime.Now.Ticks;
        }

        public DataClass(object o)
        {
            this.data = o;
            this.scene = TeamServerConnection.currentServer.currentScene;
            syncTime = DateTime.Now.Ticks;
        }
    }

    public enum DataType : byte
    {
        ComponentUpdate,
        TransformUpdate,
        GameObjectUpdate,
        ModelContainerUpdate
        //MaterialUpdate,
        //TextureUpdate
    }
}