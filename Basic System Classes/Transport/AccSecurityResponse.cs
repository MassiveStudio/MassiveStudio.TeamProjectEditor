﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MassiveStudio.TeamProjectEditor
{
    [Serializable]
    public class AccSecurityResponse
    {
        public int valid ;
        public string userid;
        public bool transportflag;

        public AccSecurityResponse(int v, string i, bool t)
        {
            this.valid = v;
            this.userid = i;
            this.transportflag = t;
        }
    }
}