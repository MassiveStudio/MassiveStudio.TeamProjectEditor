﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MassiveStudio.TeamProjectEditor
{
    [Serializable]
    public class Header
    {
       public byte[] data;
        public Header(byte[] d)
        {
            data = new byte[d.Length];
            d.CopyTo(data, 0);
        }
    }
}
