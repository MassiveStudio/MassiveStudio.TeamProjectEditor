﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace TeamProjectEditor
{
    [System.Serializable]
    public class RigidbodyObject : System.Object
    {
        float mass;
        float drag;
        float angularDrag;
        bool useGravity;
        bool isKinematic;
        RigidbodyConstraints constraints;
        RigidbodyInterpolation interpolation;
        CollisionDetectionMode collisionDetectionMode;

        public void ToRigidbody(Rigidbody rig)
        {
            rig.mass = mass;
            rig.drag = drag;
            rig.angularDrag = angularDrag;
            rig.useGravity = useGravity;
            rig.isKinematic = isKinematic;
            rig.constraints = constraints;
            rig.interpolation = interpolation;
            rig.collisionDetectionMode = collisionDetectionMode;
        }

        public void ToRigidbodyObject(Rigidbody rig)
        {
            this.mass = rig.mass;
            this.drag = rig.drag;
            this.angularDrag = rig.angularDrag;
            this.useGravity = rig.useGravity;
            this.isKinematic = rig.isKinematic;
            this.constraints = rig.constraints;
            this.interpolation = rig.interpolation;
            this.collisionDetectionMode = rig.collisionDetectionMode;
        }
    }
}
