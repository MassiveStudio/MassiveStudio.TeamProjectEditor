﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor
{
    sealed public class PhysicMaterialSurrogate : ISerializationSurrogate
    {

        // Method called to serialize a Vector3 object
        public void GetObjectData(System.Object obj, SerializationInfo info, StreamingContext context)
        {

            PhysicMaterial tr = (PhysicMaterial)obj;
            //info.AddValue("bounce", tr.bounciness);
        }

        // Method called to deserialize a Vector3 object
        public System.Object SetObjectData(System.Object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            PhysicMaterial tr = (PhysicMaterial)obj;
            //tr.bounciness = (float)info.GetSingle("bounce");
            return obj;
        }
    }
}