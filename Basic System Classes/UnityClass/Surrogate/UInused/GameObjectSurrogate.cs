using System.Runtime.Serialization;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor
{
    sealed public class GameObjectSurrogate : ISerializationSurrogate
    {

        // Method called to serialize a Vector3 object
        public void GetObjectData(System.Object obj,
                                  SerializationInfo info, StreamingContext context)
        {

            GameObject go = (GameObject)obj;
            info.AddValue("name", go.name);
            info.AddValue("active", go.activeSelf);
            info.AddValue("layer", go.layer);
            info.AddValue("tag", go.tag);

        }

        // Method called to deserialize a Vector3 object
        public System.Object SetObjectData(System.Object obj,
                                           SerializationInfo info, StreamingContext context,
                                           ISurrogateSelector selector)
        {

            GameObject go = (GameObject)obj;
            go.name = (string)info.GetValue("name", typeof(string));
            go.SetActive((bool)info.GetValue("active", typeof(bool)));
            go.layer = (int)info.GetValue("layer", typeof(int));
            go.tag = (string)info.GetValue("tag", typeof(string));
            obj = go;
            return obj;
        }
    }
}