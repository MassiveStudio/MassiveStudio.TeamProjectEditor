﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;

namespace MassiveStudio.TeamProjectEditor
{
    public class MonobehaivurSurrogate : ISerializationSurrogate
    {

        // Method called to serialize a Vector3 object
        public void GetObjectData(System.Object obj,
                                  SerializationInfo info, StreamingContext context)
        {

            GameObject go = (GameObject)obj;
        }

        // Method called to deserialize a Vector3 object
        public System.Object SetObjectData(System.Object obj,
                                           SerializationInfo info, StreamingContext context,
                                           ISurrogateSelector selector)
        {

            GameObject go = (GameObject)obj;
            return obj;
        }
    }
}