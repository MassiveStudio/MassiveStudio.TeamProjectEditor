﻿using System.Runtime.Serialization;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor
{
    class SphereColliderSurrogate : ISerializationSurrogate
    {
        public void GetObjectData(System.Object obj,
                          SerializationInfo info, StreamingContext context)
        {
            SphereCollider cap = (SphereCollider)obj;
            info.AddValue("center", cap.center);
            info.AddValue("trigger", cap.isTrigger);
            info.AddValue("raduis", cap.radius);
        }

        // Method called to deserialize a Vector3 object
        public System.Object SetObjectData(System.Object obj,
                                           SerializationInfo info, StreamingContext context,
                                           ISurrogateSelector selector)
        {

            SphereCollider cap = (SphereCollider)obj;
            cap.center = (Vector3)info.GetValue("center", typeof(Vector3));
            cap.isTrigger = (bool)info.GetValue("trigger", typeof(bool));
            cap.radius = (float)info.GetValue("radius", typeof(float));
            obj =cap;
            return obj;
        }
    }
}
