﻿using System.Runtime.Serialization;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor
{
    class BoxColliderSurrogate: ISerializationSurrogate
    {
        public void GetObjectData(System.Object obj,
                          SerializationInfo info, StreamingContext context)
        {

            BoxCollider box = (BoxCollider)obj;
            info.AddValue("size", box.size);
            info.AddValue("tigger", box.isTrigger);
            info.AddValue("center", box.center);
        }

        // Method called to deserialize a Vector3 object
        public System.Object SetObjectData(System.Object obj,
                                           SerializationInfo info, StreamingContext context,
                                           ISurrogateSelector selector)
        {

            BoxCollider box = (BoxCollider)obj;
            box.size = (Vector3)info.GetValue("size", typeof(Vector3));
            box.center = (Vector3)info.GetValue("center", typeof(Vector3));
            box.isTrigger = (bool)info.GetValue("trigger", typeof(bool));
            obj = box;
            return obj;
        }
    }
}
