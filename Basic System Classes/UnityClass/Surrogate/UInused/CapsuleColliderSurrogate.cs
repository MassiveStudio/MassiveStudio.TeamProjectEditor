﻿using System.Runtime.Serialization;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor
{
    class CapsuleColliderSurrogate : ISerializationSurrogate
    {
        public void GetObjectData(System.Object obj,
                          SerializationInfo info, StreamingContext context)
        {
            CapsuleCollider cap = (CapsuleCollider)obj;
            info.AddValue("center", cap.center);
            info.AddValue("trigger", cap.isTrigger);
            info.AddValue("raduis", cap.radius);
            info.AddValue("height", cap.height);
            info.AddValue("dir", cap.direction);
        }

        // Method called to deserialize a Vector3 object
        public System.Object SetObjectData(System.Object obj,
                                           SerializationInfo info, StreamingContext context,
                                           ISurrogateSelector selector)
        {

            CapsuleCollider cap = (CapsuleCollider)obj;
            cap.center = (Vector3)info.GetValue("center", typeof(Vector3));
            cap.isTrigger = (bool)info.GetValue("trigger", typeof(bool));
            cap.radius = (float)info.GetValue("radius", typeof(float));
            cap.height = (float)info.GetValue("height", typeof(float));
            cap.direction = (int)info.GetValue("dir", typeof(int));
            obj =cap;
            return obj;
        }
    }
}
