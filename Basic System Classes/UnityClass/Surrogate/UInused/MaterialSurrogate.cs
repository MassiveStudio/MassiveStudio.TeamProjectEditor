using System.Runtime.Serialization;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor
{
    sealed public class MaterialSurrogate : ISerializationSurrogate
    {

        // Method called to serialize a Vector3 object
        public void GetObjectData(System.Object obj,
                                  SerializationInfo info, StreamingContext context)
        {

            Material v3 = (Material)obj;
            info.AddValue("shader", v3.shader.name);
        }

        // Method called to deserialize a Vector3 object
        public System.Object SetObjectData(System.Object obj,
                                           SerializationInfo info, StreamingContext context,
                                           ISurrogateSelector selector)
        {

            Material v3 = (Material)obj;
            v3.shader = (Shader)Shader.Find((string)info.GetValue("shader", typeof(string)));
            obj = v3;
            return obj;
        }
    }
}