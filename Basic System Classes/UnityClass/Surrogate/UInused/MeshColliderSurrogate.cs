﻿using System.Runtime.Serialization;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor
{
    class MeshColliderSurrogate : ISerializationSurrogate
    {
        public void GetObjectData(System.Object obj,
                          SerializationInfo info, StreamingContext context)
        {
            MeshCollider cap = (MeshCollider)obj;
            info.AddValue("convex", cap.convex);
            info.AddValue("trigger", cap.isTrigger);
        }

        // Method called to deserialize a Vector3 object
        public System.Object SetObjectData(System.Object obj,
                                           SerializationInfo info, StreamingContext context,
                                           ISurrogateSelector selector)
        {

            MeshCollider cap = (MeshCollider)obj;
            cap.convex = (bool)info.GetValue("convex", typeof(bool));
            cap.isTrigger = (bool)info.GetValue("trigger", typeof(bool));
            obj =cap;
            return obj;
        }
    }
}
