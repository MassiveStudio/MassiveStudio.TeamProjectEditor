﻿using System.Runtime.Serialization;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor
{
    class RectSurrogate : ISerializationSurrogate
    {
        public void GetObjectData(System.Object obj,
                          SerializationInfo info, StreamingContext context)
        {

            Rect box = (Rect)obj;
            info.AddValue("x", box.x);
            info.AddValue("y", box.y);
            info.AddValue("width", box.width);
            info.AddValue("height", box.height);
        }

        // Method called to deserialize a Vector3 object
        public System.Object SetObjectData(System.Object obj,
                                           SerializationInfo info, StreamingContext context,
                                           ISurrogateSelector selector)
        {

            Rect box = (Rect)obj;
            box.x = (float)info.GetValue("x", typeof(float));
            box.y = (float)info.GetValue("y", typeof(float));
            box.width = (float)info.GetValue("width", typeof(float));
            box.height = (float)info.GetValue("height", typeof(float));
            obj = box;
            return obj;
        }
    }
}
