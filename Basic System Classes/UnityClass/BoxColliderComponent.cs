﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace TeamProjectEditor
{
    [Serializable]
    public class BoxColliderComponent : System.Object
    {
        Vector3 center;
        Vector3 size;
        bool trigger;

        public void ToBoxColliderComponent(BoxCollider box)
        {
            this.center =box.center;
            this.size = box.size;
            this.trigger = box.isTrigger;
        }

        public void ToBoxCollider(BoxCollider box)
        {
            box.center = this.center;
            box.size = this.size;
            box.isTrigger = this.trigger;
        }
    }
}
