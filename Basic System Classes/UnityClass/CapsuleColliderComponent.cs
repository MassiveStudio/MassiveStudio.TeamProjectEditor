﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace TeamProjectEditor
{
    [Serializable]
    public class CapsuleColliderComponent
    {
        Vector3 center;
	    Vector3 size;
	    bool trigger;
	    int dir;
	
	    public void ToCapsuleColliderComponent(CapsuleCollider box)
        {
            this.center = box.center;
            this.size = new Vector2(box.radius, box.height);
            this.trigger = box.isTrigger;
            this.dir = box.direction;

        }

        public void ToCapsuleCollider(CapsuleCollider box)
        {
            box.center = this.center;
            box.radius = this.size.x;
            box.height = this.size.y;
            box.isTrigger = this.trigger;
            box.direction = this.dir;
        }
    }
}
