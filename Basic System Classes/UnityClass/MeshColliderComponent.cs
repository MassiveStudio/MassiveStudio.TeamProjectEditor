﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace TeamProjectEditor
{
    [Serializable]
    public class MeshColliderComponent
    {
        bool convex;
	    bool trigger;
	
	    public void ToMeshColliderComponent(MeshCollider box)
        {
            this.convex = box.convex;
            this.trigger = box.isTrigger;
        }

        public void ToMeshCollider(MeshCollider box)
        {
            box.convex = this.convex;
            box.isTrigger = this.trigger;
        }
    }
}
