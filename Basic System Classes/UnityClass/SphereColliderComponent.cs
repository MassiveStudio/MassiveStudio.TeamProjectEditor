﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace TeamProjectEditor
{
    [Serializable]
    public class SphereColliderComponent
    {
        Vector3 center;
	    float radius;
	    bool trigger;
	
	    public void ToSphereColliderComponent(SphereCollider sphere)
        {
            this.center = sphere.center;
            this.radius = sphere.radius;
            this.trigger = sphere.isTrigger;
        }

        public void ToSphereCollider(SphereCollider box)
        {
            box.center = this.center;
            box.radius = this.radius;
            box.isTrigger = this.trigger;
        }
    }
}
