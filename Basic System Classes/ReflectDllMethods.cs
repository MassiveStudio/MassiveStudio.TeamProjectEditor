﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

using FastMember;
using System.Reflection.Emit;

//using UnityHelper;
using System.Linq.Expressions;
using System.Linq;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using MassiveStudio.TeamProjectEditor.Tools;

namespace MassiveStudio.TeamProjectEditor
{
    public static class ReflectDllMethods
    {
       //public static UnityHelper.HandleHelper HANDLE_helper;

        public static object GetMethodInvoked(string dll, string classname, string methodname, object[] args)
        {
            Assembly a = null;
            Assembly[] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly ass in assemblies)
                if (ass.GetName().Name == dll)
                {
                    a = ass;
                    break;
                }
            if (a == null)
                return null;
            else
            {
                System.Type _type = a.GetType(classname);
                if (_type == null)
                    throw new System.ArgumentNullException("CLASS " + classname + " couldn't found in " + dll);
                //else
                //    System.Console.WriteLine("CLASS " + classname + " found in " + dll);

                MethodInfo minfo = _type.GetMethod(methodname, BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic);

                if (minfo == null)
                    throw new System.ArgumentNullException("Method " + methodname + " couldn't found");
                else
                    System.Console.WriteLine("METHOD " + methodname + " found");

                // ParameterInfo pinfo = minfo.ReturnParameter;
                try
                {
                    var ret = minfo.Invoke(null, args);
                    return (object)ret;

                }
                catch (System.Exception e)
                {
                    throw new System.Exception("ERROR: ", e);
                }
                //return null;
            }
        }

        public static T GetMethodInvoked<T>(string dll, string classname, string methodname, object[] args)
        {
            /*if (HANDLE_helper == null)
            {
                HANDLE_helper = new HandleHelper();
                HANDLE_helper.DynamicMethod();
            }

            if (methodname == "GetCameraRect")
            {
                //Debug.Log("RECT IN: " + ((Rect)args[0]).ToString());
                Rect _out = new Rect();

                try
                {
                    if(HandleHelper.getCameraRect == null)
                        HANDLE_helper.DynamicMethod();

                    _out = HandleHelper.getCameraRect((Rect)args[0]);
                }
                catch (Exception e)
                {
                    throw new System.Exception("ERROR", e);
                }
                return (T)(object)_out;
            }*/
            
            Assembly a = null;
            Assembly[] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly ass in assemblies)
                if (ass.GetName().Name == dll)
                {
                    // Debug.Log("DLL " + ass.GetName().Name + " Found");
                    a = ass;
                    break;
                }
            if (a == null)
            {
                return default(T);
            }
            else
            {
                System.Type _type = a.GetType(dll + "." + classname);
                //Debug.Log(_type != null ? "Type Found: " + _type.ToString(): "TYPE [" + classname + "] NOT FOUND");
                MethodInfo minfo = _type.GetMethod(methodname, BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic);
                //Debug.Log(minfo != null ? "Methode Found: " + minfo.ToString() : " METHOD [" + methodname+"] NOT FOUND");
                // ParameterInfo pinfo = minfo.ReturnParameter;
                var ret = minfo.Invoke(null, args);
                return (T)ret;
            }
        }

        public static void GetMethodInvokedVoid(string dll, string classname, string methodname, object[] args)
        {
            Assembly a = null;
            Assembly[] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly ass in assemblies)
                if (ass.GetName().Name == dll)
                {
                    // Debug.Log("DLL " + ass.GetName().Name + " Found");
                    a = ass;
                    break;
                }
            if (a == null)
                return;
            else
            {
                System.Type _type = a.GetType(dll + "." + classname);
                //Debug.Log(_type != null ? "Type Found: " + _type.ToString(): "TYPE [" + classname + "] NOT FOUND");
                MethodInfo minfo = _type.GetMethod(methodname, BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic);
                //Debug.Log(minfo != null ? "Method Found: " + minfo.ToString() : " METHOD [" + methodname+"] NOT FOUND");
                minfo.Invoke(null, args);
                return;
            }
        }

        public static void GetMethodInvokedVoid(MethodInfo method, object obj, object[] args)
        {
            method.Invoke(obj, args);
        }

        public static void GetMethodInvokedVoid(MethodInfo method, object[] args)
        {
            method.Invoke(null, args);
        }

        public static void GetMethodInvokedVoid(MethodInfo method,object obj)
        {
            method.Invoke(obj, null);
        }

        public static T GetMethodInvoked<T>(MethodInfo method, object[] args)
        {
            return (T)method.Invoke(null, args);
        }

        public static System.Type GetClassType(string dll, string classname)
        {
            Assembly a = null;
            Assembly[] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly ass in assemblies)
                if (ass.GetName().Name == dll)
                {
                    a = ass;
                    break;
                }
            if (a == null)
                return null;
            else
            {
                Type t = a.GetType(dll + "." + classname);

                if (t == null)
                    throw new System.NullReferenceException("TYPE [" + dll + "." + classname + "] not Found");
                //else
                //     Debug.Log("TYPE [" + dll + "." + classname + "] FOUND");
                return t;
            }
        }

        public static object GetField(System.Type t, object o, string field, bool debug = true)
        {
            FieldInfo f = t.GetField(field, BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);


            if (debug)
            {

                if (f == null)
                {
                    Debug.Log("FIELD " + field + "  " + (f == null ? "NOT FOUND" : "FOUND"));

                    foreach (FieldInfo fs in t.GetFields(BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance))
                    {
                        Debug.Log("Field: " + fs.Name + "  " + fs.FieldType.ToString());
                    }

                }

                //if (f != null)
                //    Debug.Log("IS STATIC: " + f.IsStatic);
            }

            /*if(!once)   //DEBUG FIELDS
            foreach(FieldInfo fs in t.GetFields(BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance))
            {
                Debug.Log("Field: " + fs.Name + "  " + fs.FieldType.ToString());
            }

            once = true;A

            if (f == null)
                Debug.LogError("FIELD NULL");
            else
                Debug.Log("FIELD FOUND");
            */



            if (f.IsStatic)
                return f.GetValue(null);
            else
                return f.GetValue(o);
        }

        public static object GetFieldFast(Type t, object o, string field)
        {
            var accessor = TypeAccessor.Create(t);
            return accessor[o, field];
        }

        public static void SetField(System.Type t, object o, string field, object value)
        {
            FieldInfo f = null;
            try
            {
                BindingFlags flags = BindingFlags.Public |
                            BindingFlags.NonPublic |
                            BindingFlags.Static |
                            BindingFlags.Instance |
                            BindingFlags.DeclaredOnly;

                f = t.GetField(field, flags);// BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);

                if (f.IsStatic)
                    f.SetValue(null, value);
                else
                    f.SetValue(o, value);
            }
            catch (Exception e)
            {
                Debug.Log("ERROR WITH FIELD " + field);
                Debug.Log("FIELD EXISTS: " + (f == null ? "NO" : "Yes"));
                throw e;
            }
        }

        public static void SetFieldFast(Type t, object o, string field, object value)
        {
            var accessor = TypeAccessor.Create(t);
            accessor[o, field] = value;
        }

        public static System.Type GetType(string name)
        {
            Type type = Type.GetType(name);
            if (type != null)
                return type;
             
            if (name.Contains("."))
            {
                string dll = name.Split('.')[0];
                return GetClassType(dll, name.Substring(dll.Length + 1));
            }
            else
            {
                Assembly[] asm = AppDomain.CurrentDomain.GetAssemblies();
                Type t;

                for (int i = 0; i < asm.Length; i++)
                {
                    t = asm[i].GetType(name);
                    if (t != null)
                        return t;
                }
                return null;
            }
        }

        public static int GetInstanceID(UnityEngine.Object obj)
        {
            return (int)ReflectDllMethods.GetField(typeof(UnityEngine.Object), obj, "m_InstanceID");
        }

        /*
        System.Type ListType = createList(gridPaintPaletteWindow).GetType();
        IList f  = CastHelper.Cast(windows, ListType);
        public static IList createList(Type myType)
        {
            Type genericListType = typeof(List<>).MakeGenericType(myType);
            return (IList)Activator.CreateInstance(genericListType);
        }

        public static object Cast(object obj, Type t)
        {
            try
            {
                var param = Expression.Parameter(obj.GetType());
                return Expression.Lambda(Expression.Convert(param, t), param)
                         .Compile().DynamicInvoke(obj);
            }
            catch (TargetInvocationException ex)
            {
                throw ex.InnerException;
            }
        }

        public class CastHelper
        {
            public static dynamic Cast(object src, Type t)
            {
                var castMethod = typeof(CastHelper).GetMethod("CastGeneric").MakeGenericMethod(t);
                return castMethod.Invoke(null, new[] { src });
            }
            public static T CastGeneric<T>(object src)
            {
                return (T)Convert.ChangeType(src, typeof(T));
            }
        }*/

    }

    public static class DynamicMethodBinder
    {

        [MenuItem("MassiveStudio/ReflectionTest")]
        public static void TestStopWatcher()
        {
            System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();

            watch.Start();
            MethodInfo minfo = typeof(Handles).GetMethod("GetCameraRect", BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic);
            for (int i = 0; i < 100000; i++)
                minfo.Invoke(null, new object[] {new Rect(500, 500, 10, 10)});
            Debug.Log("TIME DEL: " + watch.ElapsedMilliseconds + ":" + watch.ElapsedTicks);
            watch.Stop();

            watch.Reset();

            watch.Start();
            //for(int i=0;  i< 100000; i++)
            //    HandleHelper.getCameraRect(new Rect(500,500,10,10));
            Debug.Log("TIME DEL: " + watch.ElapsedMilliseconds+":"+ watch.ElapsedTicks);
            watch.Stop();

           // AppDomain.CurrentDomain.GetAssemblies().Single(x=> x.FullName.Contains("UnityHelper")).
        }

        public static AssemblyBuilder GetAssemblyBuilder(string assemblyName)
        {
            AssemblyName a_name = new AssemblyName(assemblyName);
            AppDomain currentDomain = AppDomain.CurrentDomain; // Thread.GetDomain();
            AssemblyBuilder builder = currentDomain.DefineDynamicAssembly(a_name, AssemblyBuilderAccess.Save);
            return builder;
        }

        public static ModuleBuilder GetModule(AssemblyBuilder asmBuilder, AssemblyName name)
        {
            ModuleBuilder builder = asmBuilder.DefineDynamicModule(name.Name,name.Name+".dll");
            return builder;
        }

        public static TypeBuilder GetType(ModuleBuilder modBuilder, string className)
        {
            TypeBuilder builder = modBuilder.DefineType(className, TypeAttributes.Public);
            return builder;
        }

        public static TypeBuilder GetType(ModuleBuilder modBuilder, string className, TypeAttributes att)
        {
            TypeBuilder builder = modBuilder.DefineType(className, att);
            return builder;
        }

        public static TypeBuilder GetType(ModuleBuilder modBuilder, string className, params string[] genericparameters)
        {
            TypeBuilder builder = modBuilder.DefineType(className, TypeAttributes.Public);
            GenericTypeParameterBuilder[] genBuilders = builder.DefineGenericParameters(genericparameters);

            foreach (GenericTypeParameterBuilder genBuilder in genBuilders) // We take each generic type T : class, new()
            {
                genBuilder.SetGenericParameterAttributes(GenericParameterAttributes.ReferenceTypeConstraint | GenericParameterAttributes.DefaultConstructorConstraint);
                //genBuilder.SetInterfaceConstraints(interfaces);
            }
            return builder;
        }

        public static MethodBuilder GetMethod(TypeBuilder typBuilder, string methodName)
        {
            MethodBuilder builder = typBuilder.DefineMethod(methodName,MethodAttributes.Public | MethodAttributes.HideBySig);
            return builder;
        }

        public static MethodBuilder GetMethod(TypeBuilder typBuilder, string methodName,Type returnType, params Type[] parameterTypes)
        {
            MethodBuilder builder = typBuilder.DefineMethod(methodName, MethodAttributes.Public | MethodAttributes.HideBySig,CallingConventions.HasThis, returnType, parameterTypes);
            return builder;
        }

        public static MethodBuilder GetMethod(TypeBuilder typBuilder, string methodName, Type returnType, string[] genericParameters, params Type[] parameterTypes)
        {
            MethodBuilder builder = typBuilder.DefineMethod(methodName,MethodAttributes.Public | MethodAttributes.HideBySig, CallingConventions.HasThis, returnType, parameterTypes);

            GenericTypeParameterBuilder[] genBuilders =builder.DefineGenericParameters(genericParameters);

            foreach (GenericTypeParameterBuilder genBuilder in genBuilders)// We take each generic type T : class, new()
            {
                genBuilder.SetGenericParameterAttributes(GenericParameterAttributes.ReferenceTypeConstraint |GenericParameterAttributes.DefaultConstructorConstraint);
                //genBuilder.SetInterfaceConstraints(interfaces);
            }
            return builder;
        }

        private static TypeBuilder GetTypeBuilder()
        {
            var typeSignature = "MyDynamicType";
            var an = new AssemblyName(typeSignature);
            AssemblyBuilder assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(an, AssemblyBuilderAccess.Run);
            ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule("MainModule");
            TypeBuilder tb = moduleBuilder.DefineType(typeSignature,
                    TypeAttributes.Public |
                    TypeAttributes.Class |
                    TypeAttributes.AutoClass |
                    TypeAttributes.AnsiClass |
                    TypeAttributes.BeforeFieldInit |
                    TypeAttributes.AutoLayout,
                    null);
            return tb;
        }

        [MenuItem("MassiveStudio/Compile UnityHelper")]
        public static void CreateMethod()
        {
#if UNITY_EDITOR
            Debug.Log("CREATE DYNAMIC METHOD");
#endif

            AppDomain currentDomain = AppDomain.CurrentDomain;
            AssemblyName name = new AssemblyName("UnityHelper");
            name.HashAlgorithm = System.Configuration.Assemblies.AssemblyHashAlgorithm.SHA1;
            name.VersionCompatibility = System.Configuration.Assemblies.AssemblyVersionCompatibility.SameProcess;
            name.Flags = AssemblyNameFlags.PublicKey;


            SignAssembly(name);

            AssemblyBuilder asmbuilder = GetAssemblyBuilder(name.Name);
            ModuleBuilder mbuilder = asmbuilder.DefineDynamicModule("UnityHelper", "UnityHelper.dll");
            //ModuleBuilder mbuilder_Editor = asmbuilder.DefineDynamicModule("UnityEditor");

#region OLD TEST
            /*
            MethodBuilder method2 = tbuilder.DefineMethod("DynamicMethod", MethodAttributes.Public, typeof(void), null);
            ILGenerator generator2 = method2.GetILGenerator();
            CreateDynamicDelegate(generator2, "GetCameraRect", typeof(Rect), new[] { typeof(Rect) });*/

            /*
            Type[] tparams = { typeof(Rect) };
            MethodBuilder method = tbuilder.DefineMethod("GetCameraRect", MethodAttributes.Public, typeof(Rect), new[] { typeof(Rect) });
            ILGenerator generator = method.GetILGenerator();*/

            // generator.Emit(OpCodes.Ldarg_1);
            //generator.EmitCall(OpCodes.Call, typeof(UnityEditor.Handles).GetMethod("GetCameraRect", BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic), tparams);
            /*
            var toCallInfo = typeof(UnityEditor.Handles).GetMethod("GetCameraRect", BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic);
            /*unsafe
            {
                var functionPointer = toCallInfo.MethodHandle.GetFunctionPointer();
                if (sizeof(IntPtr) == 4)
                    generator.Emit(OpCodes.Ldc_I4, (int)functionPointer);
                else
                    generator.Emit(OpCodes.Ldc_I8, (long)functionPointer);
            }*/
            /*
            //generator.Emit(OpCodes.Ldobj, Type.GetType("Handles"));

            //Agenerator.Emit(OpCodes.Ldc_I4);  // int parameter


            generator.Emit(OpCodes.Ldarg_1);
            generator.Emit(OpCodes.Ldftn, toCallInfo);
            generator.EmitCalli(OpCodes.Calli, toCallInfo.CallingConvention, typeof(Rect), new[] { typeof(Rect) }, null);
            generator.Emit(OpCodes.Ret);*/

            //PropertyTestGenerator(mbuilder);
#endregion

            Assembly[] assemblys = AppDomain.CurrentDomain.GetAssemblies();
            ParameterInfo[] param;
            Type[] paramers;
            Type modulType;

            Assembly assembly = Assembly.GetAssembly(typeof(UnityEngine.Object));
            assemblys = new Assembly[2];

            assemblys[0] = assembly;//UnityEngine
            assemblys[1] = assembly = Assembly.GetAssembly(typeof(BodyPart));//UnityEditor

            BindingFlags flags = BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance;

            for (int i = 0; i < assemblys.Length; i++)
            //if (assemblys[i].GetName().Name == "UnityEngine")
            {
                Type[] types = assemblys[i].GetTypes();//.GetModules();
                List<string> methodnames = new List<string>();
                for (int m = 0; m < types.Length; m++)
                {

                    if(types[m].FullName== string.Empty)
                    {
                        Debug.LogError("CLASS NAME EMPTY");
                        continue;
                    }


                    //Debug.Log("Class " + types[m].FullName);
                    methodnames.Clear();
                    //Debug.Log("Modul " + types[m].Name);

                    //Delegates
                    TypeBuilder tbuilder = GetType(mbuilder, "UnityHelper." + types[m].FullName, TypeAttributes.Class | TypeAttributes.Public | TypeAttributes.Sealed | TypeAttributes.NotPublic);// | TypeAttributes.Abstract);
                    ConstructorBuilder constructor = tbuilder.DefineDefaultConstructor(MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName);

                    //Methods
                    TypeBuilder methodbuilder = GetType(mbuilder, "UnityHelper.Methods." + types[m].FullName, TypeAttributes.Class | TypeAttributes.Public | TypeAttributes.Sealed | TypeAttributes.NotPublic);// | TypeAttributes.Abstract);
                    ConstructorBuilder methodconstructor = methodbuilder.DefineDefaultConstructor(MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName);

                    MethodInfo InitMethod;
                    ILGenerator StaticConstructor = GenerateStaticInitiator(methodbuilder, out InitMethod);


                    MethodInfo[] methods = types[m].GetMethods(flags | BindingFlags.DeclaredOnly);
                    List<MethodInfo> methodsToInit = new List<MethodInfo>();
                    modulType = types[m].GetType();
                    MethodInfo currentMethod;
                    for (int met = 0; met < methods.Length; met++)
                    {
                        //Debug.Log("\tMethod " + methods[met].Name);

                        currentMethod = methods[met];

                        object[] attributes = currentMethod.GetCustomAttributes(true);//Need to check for [MethodImpl(MethodImplOptions.InternalCall)]


                        IEnumerable<object> fields = attributes.Where(x => x.GetType().Name == "GeneratedByOldBindingsGeneratorAttribute");
                        if (fields == null || fields.Count() >0)
                        {
                            /*if (attributes != null && attributes.Length > 0)
                            {
                                for (int c = 0; c < attributes.Length; c++)
                                {
                                    //Debug.Log("\t\tAttribute: " + attributes[c].GetType().FullName);
                                }
                            }*/
                        }
                        else
                        {
                            //if (!methods[met].IsPublic)
                            {
                                if (methods[met].IsGenericMethod)
                                    continue;

                                param = methods[met].GetParameters();
                                paramers = new Type[param.Length];

                                for (int p = 0; p < param.Length; p++)
                                    paramers[p] = param[p].ParameterType;

                                string mName = methods[met].Name;
                                //if (methodnames.IndexOf(methods[met].Name) != -1)
                                //    continue;
                                try
                                {
                                    if (mName.StartsWith("op_") || mName.StartsWith("get_") || mName.StartsWith("set_"))
                                        continue;

                                    //Prevent Dictionary Duplicate Key
                                    if (methodnames.Contains(methods[met].Name)){
                                        for (int cc = 0; cc < paramers.Length; cc++)
                                            mName += paramers[cc].Name;
                                    }


                                    string Mname = GetUniqueName( mbuilder,methods[met].Name);

                                    Type delegateType = CreateDynamicDelegatField(tbuilder, Mname, methods[met].ReturnType, paramers);//+"_"+methods[met].MetadataToken
                                    FieldBuilder delegateStaticField = CreateDynamicStaticDelegateField(methodbuilder, methods[met].Name, delegateType);

                                    AddDynamicMethodInitorToStaticConstructor(StaticConstructor, methods[met].Name, modulType, methods[met].ReturnType, paramers, delegateType, delegateStaticField);
                                    //MethodBuilder dyndelmeth = CreateDynamicDelegateMethod(met, tbuilder, methods[met].Name, modulType, methods[met].ReturnType, paramers, delegateType, delegateStaticField);
                                    //methodsToInit.Add(dyndelmeth);

                                    methodnames.Add(methods[met].Name);
                                }
                                catch(Exception e)
                                {
                                    Debug.LogWarning(" ERROR " + types[m].FullName+ " : " + mName + " can't be build" + "\n"+ e.Message+"\n"+e.StackTrace);
                                    //string s = " ERROR " + types[m].FullName + " : " + mName + " can't be build" + "\n" + e.Message + "\n" + e.StackTrace;
                                    //System.Console.WriteLine(s);
                                }
                            }
                        }
                    }
                    //GenerateStaticConstuctor(tbuilder, methodsToInit);

                    GenerateStaticConstuctorCaller(tbuilder, InitMethod);

                    EndStaticConstuctor(StaticConstructor);
                    tbuilder.CreateType();
                    methodbuilder.CreateType();
                    //break;
                }
                //break;
            }

            /*
             Type delegateType = CreateDynamicDelegatField(tbuilder, "GetCameraRect", typeof(Rect), new[] { typeof(Rect) });
                                FieldBuilder delegateStaticField = CreateDynamicStaticDelegateField(tbuilder, "GetCameraRect", delegateType);
                                MethodBuilder dyndelmeth = CreateDynamicDelegateMethod(tbuilder, "GetCameraRect", typeof(Handles), typeof(Rect), new[] { typeof(Rect) }, delegateType, delegateStaticField);
                             */

            //EventBuilder eb = tbuilder.DefineEvent("EVENTTEST", EventAttributes.RTSpecialName | EventAttributes.SpecialName, typeof(Rect));
            //EventBuilder eb2 = tbuilder.def("EVENTTEST", EventAttributes.None, typeof(Func<Rect,Rect>));
            //eb2.SetRaiseMethod(null);
            //Assembly.GetCallingAssembly().GetModule("TeamProjectEditor.DynamicMethodBinder").GetField("voiddelegate").

            /*MethodBuilder method2 = tbuilder.DefineMethod("GetCameraRect",
                 MethodAttributes.Public,
                 typeof(Rect),
                 new[] { typeof(Rect) });
            ILGenerator generator2 = method2.GetILGenerator();
            generator2.Emit(OpCodes.Ret);
            */



            SetAssemblyData(ref asmbuilder, name);
            Debug.Log("SAVE ASSEMBLY ");
            asmbuilder.Save("UnityHelper.dll");

            // Method getter2 = (Method)dm.CreateDelegate(typeof(Method));
            //getter2(new Rect(0,0,1000,100));
        }

        public static void SignAssembly(AssemblyName name)
        {
            System.IO.FileStream publicKeyStream = System.IO.File.Open(@"P:\VisualStudio\TeamProjectEditor\sn1.snk", System.IO.FileMode.Open);
            byte[] publicKey = new byte[publicKeyStream.Length];
            publicKeyStream.Read(publicKey, 0, (int)publicKeyStream.Length);
            name.SetPublicKey(publicKey);

            System.IO.FileStream publicKeyTokenStream = System.IO.File.Open(@"P:\VisualStudio\TeamProjectEditor\snpub1.snk", System.IO.FileMode.Open);
            byte[] publicKeyToken = new Byte[publicKeyTokenStream.Length];
            publicKeyTokenStream.Read(publicKeyToken, 0, (int)publicKeyToken.Length);
            // Provide the assembly with a public key token.
            name.SetPublicKeyToken(publicKeyToken);

            System.Console.WriteLine("Public Key: " + System.Text.Encoding.ASCII.GetString(publicKeyToken));
            System.Console.WriteLine("Public Token: " + System.Text.Encoding.ASCII.GetString(publicKeyToken));
            //Debug.Log("Public Key: " + name.GetPublicKeyToken)
        }

        public static ILGenerator GenerateStaticInitiator(TypeBuilder builder, out MethodInfo info)
        {
            //ConstructorBuilder constructor = builder.DefineConstructor(MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName | MethodAttributes.Static, CallingConventions.Standard, null);
            MethodBuilder constructor = builder.DefineMethod("Init", MethodAttributes.Public | MethodAttributes.Static);
            ILGenerator gen = constructor.GetILGenerator();
            gen.DeclareLocal(typeof(ILGenerator));//Generator
            gen.DeclareLocal(typeof(DynamicMethod));
            gen.DeclareLocal(typeof(Type[]));//Parameter

            gen.Emit(OpCodes.Ldstr, "Init Static Constructor ");
            gen.EmitCall(OpCodes.Call, typeof(UnityEngine.Debug).GetMethod("Log", new[] { typeof(object)}), null);

            //for (int i =0; i< methods.Count; i++)
            //    gen.EmitCall(OpCodes.Call, methods[i], null);

            info = constructor;

            return gen;
        }


        public static void GenerateStaticConstuctorCaller(TypeBuilder builder, MethodInfo TypeToCall)
        {
            ConstructorBuilder constructor = builder.DefineConstructor(MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName | MethodAttributes.Static, CallingConventions.Standard, null);
            ILGenerator gen = constructor.GetILGenerator();
            //gen.DeclareLocal(typeof(ILGenerator));//Generator
            //gen.DeclareLocal(typeof(DynamicMethod));
            //gen.DeclareLocal(typeof(Type[]));//Parameter

            gen.EmitCall(OpCodes.Call, TypeToCall, null);
            gen.Emit(OpCodes.Ret);
        }

        public static void EndStaticConstuctor(ILGenerator gen)
        {
            gen.Emit(OpCodes.Ret);

        }

        public static void AddDynamicMethodInitorToStaticConstructor(ILGenerator gen, string MethodName, Type orginalmethodclass, Type returnType, Type[] param, Type delegateType, FieldBuilder staticfield)
        {
            CreateDynamicDelegate(gen, MethodName, returnType, param);
            gen.Emit(OpCodes.Stloc_0);//ILLGENERATOR
            gen.Emit(OpCodes.Ldloc_0);
            gen.Emit(OpCodes.Ldsfld, typeof(OpCodes).GetField("Ldarg_0"));
            gen.EmitCall(OpCodes.Callvirt, typeof(System.Reflection.Emit.ILGenerator).GetMethod("Emit", new[] { typeof(OpCode) }), null);

            CreateDynamicTypeArray(gen, param);

            gen.Emit(OpCodes.Stloc_2);//Parameter
            gen.Emit(OpCodes.Ldloc_0);//ILGenerator
            gen.Emit(OpCodes.Ldsfld, typeof(OpCodes).GetField("Call"));
            gen.Emit(OpCodes.Ldtoken, orginalmethodclass);
            gen.EmitCall(OpCodes.Call, typeof(System.Type).GetMethod("GetTypeFromHandle"), new[] { typeof(System.RuntimeTypeHandle) });

            gen.Emit(OpCodes.Ldstr, MethodName);
            BindingFlags bind = (BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic);
            gen.Emit(OpCodes.Ldc_I4_S, (byte)bind);//BindingFlags
            gen.EmitCall(OpCodes.Call, typeof(System.Type).GetMethod("GetMethod", new[] { typeof(string), typeof(BindingFlags) }), null);

            gen.Emit(OpCodes.Ldloc_2);
            gen.EmitCall(OpCodes.Callvirt, typeof(System.Reflection.Emit.ILGenerator).GetMethod("EmitCall", new[] { typeof(OpCode), typeof(MethodInfo), typeof(Type[]) }), null);
            gen.Emit(OpCodes.Ldloc_0);
            gen.Emit(OpCodes.Ldsfld, typeof(OpCodes).GetField("Ret"));
            gen.EmitCall(OpCodes.Callvirt, typeof(System.Reflection.Emit.ILGenerator).GetMethod("Emit", new[] { typeof(OpCode) }), null);

            //Assign DynamicMethod to Delegate
            gen.Emit(OpCodes.Ldloc_1);
            gen.Emit(OpCodes.Ldtoken, delegateType);
            gen.EmitCall(OpCodes.Call, typeof(System.Type).GetMethod("GetTypeFromHandle"), new[] { typeof(System.RuntimeTypeHandle) });
            gen.EmitCall(OpCodes.Callvirt, typeof(System.Reflection.Emit.DynamicMethod).GetMethod("CreateDelegate", new[] { typeof(Type) }), null);
            gen.Emit(OpCodes.Castclass, delegateType);
            gen.Emit(OpCodes.Stsfld, staticfield);
        }

        public static void PropertyTestGenerator(ModuleBuilder mbuilder)
        {
            //FieldBuilder fb=  tbuilder.DefineField("k_BoneThickness", typeof(float), FieldAttributes.Public);

           /* string ModulName = "";
            FieldInfo fi = typeof(Handles).GetField("k_BoneThickness");
            RuntimeFieldHandle fh = fi.FieldHandle;
            RuntimeTypeHandle th = fi.DeclaringType.TypeHandle;*/

            //currentDomain.GetAssemblies().Single(x=> x.FullName.Contains("UnityEditor")).GetModule(ModulName).ModuleHandle.GetRuntimeFieldHandleFromMetadataToken

            string parentProperty = "PARENT";
            string depthProperty = "Prop";
            string typeName = "NewType";
            TypeBuilder typeBuilder = mbuilder.DefineType(typeName, TypeAttributes.Public);

            FieldBuilder parentField = typeBuilder.DefineField($"_{parentProperty}", typeBuilder, FieldAttributes.Private);
            PropertyBuilder propertyBuilder = typeBuilder.DefineProperty(parentProperty, PropertyAttributes.None, parentField.FieldType, Type.EmptyTypes);
            MethodAttributes getSetAttr = MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig;

            MethodBuilder getParentMethod = typeBuilder.DefineMethod($"get_{propertyBuilder.Name}", getSetAttr, parentField.FieldType, Type.EmptyTypes);
            ILGenerator il = getParentMethod.GetILGenerator();
            il.Emit(OpCodes.Ldarg_0);
            il.Emit(OpCodes.Ldfld, parentField);
            il.Emit(OpCodes.Ret);
            propertyBuilder.SetGetMethod(getParentMethod);

            MethodBuilder setParentMethod = typeBuilder.DefineMethod($"set_{propertyBuilder.Name}", getSetAttr, null, new[] { propertyBuilder.PropertyType });
            il = setParentMethod.GetILGenerator();
            il.Emit(OpCodes.Ldarg_0);
            il.Emit(OpCodes.Ldarg_1);
            il.Emit(OpCodes.Stfld, parentField);
            il.Emit(OpCodes.Ret);
            propertyBuilder.SetSetMethod(setParentMethod);


            /*parentField = typeBuilder.DefineField($"_{depthProperty}", typeBuilder, FieldAttributes.Private);
            propertyBuilder = typeBuilder.DefineProperty(depthProperty, PropertyAttributes.None, parentField.FieldType, Type.EmptyTypes);
            MethodBuilder getDepthMethod = typeBuilder.DefineMethod($"get_{depthProperty}", getSetAttr, parentField.FieldType, Type.EmptyTypes);
            il = getDepthMethod.GetILGenerator();
            LocalBuilder lb = il.DeclareLocal(typeof(bool));*/

            typeBuilder.CreateType();
        }

#region TestDebug
        delegate Rect CreateCtor(int i, string s);
        static CreateCtor createdCtorDelegate;

        public delegate Rect VoidDelegate(Rect r);
        public static VoidDelegate voiddelegate;

        public static void CreateDelegate()
        {
            DynamicMethod method = new DynamicMethod("GetCameraRect",
                                        typeof(Rect),
                                        new[] { typeof(Rect) },
                                        true);

            method.DefineParameter(1, ParameterAttributes.None, "rect");

            ILGenerator gen= method.GetILGenerator();
            gen.Emit(OpCodes.Ldarg_1);
            Type[] tparams = { typeof(Rect) };
            gen.EmitCall(OpCodes.Call, typeof(UnityEditor.Handles).GetMethod("GetCameraRect", BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic), tparams);
            gen.Emit(OpCodes.Ret);


            voiddelegate = (VoidDelegate)method.CreateDelegate(typeof(VoidDelegate));
        }
        public static void TEST()
        {
            typeof(UnityEditor.Handles).GetMethod("GetCameraRect", BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic);
        }

        public static void TestArray()
        {
            Type[] data = new[] { typeof(string), typeof(int), typeof(float) };

        }
#endregion

        public static MethodBuilder CreateDynamicDelegateMethod(int index, TypeBuilder tbuilder, string MethodName, Type orginalmethodclass, Type returnType, Type[] param, Type delegateType, FieldBuilder staticfield)
        {
            MethodBuilder method2 = tbuilder.DefineMethod("DynamicMethod_"+ index, MethodAttributes.Public | MethodAttributes.Static, typeof(void), null);
            ILGenerator gen = method2.GetILGenerator();

            gen.DeclareLocal(typeof(ILGenerator));//Generator
            gen.DeclareLocal(typeof(DynamicMethod));
            gen.DeclareLocal(typeof(Type[]));//Parameter


            CreateDynamicDelegate(gen, MethodName, returnType, param);
            gen.Emit(OpCodes.Stloc_0);//ILLGENERATOR
            gen.Emit(OpCodes.Ldloc_0);
            gen.Emit(OpCodes.Ldsfld, typeof(OpCodes).GetField("Ldarg_0"));
            gen.EmitCall(OpCodes.Callvirt, typeof(System.Reflection.Emit.ILGenerator).GetMethod("Emit", new[] { typeof(OpCode) }),null);


            CreateDynamicTypeArray(gen, param);

            /*
            gen.Emit(OpCodes.Ldc_I4_1); //Array fo 1 Element
            gen.Emit(OpCodes.Newarr, typeof(System.Type));
            gen.Emit(OpCodes.Dup);
            gen.Emit(OpCodes.Ldc_I4_0);//Element 2 Index:0
            gen.Emit(OpCodes.Ldtoken, typeof(Rect));//Element Type
            gen.EmitCall(OpCodes.Call, typeof(System.Type).GetMethod("GetTypeFromHandle"), new[] { typeof(System.RuntimeTypeHandle) });
            gen.Emit(OpCodes.Stelem_Ref);
            */

            gen.Emit(OpCodes.Stloc_2);//Parameter
            gen.Emit(OpCodes.Ldloc_0);//ILGenerator
            gen.Emit(OpCodes.Ldsfld, typeof(OpCodes).GetField("Call"));
            gen.Emit(OpCodes.Ldtoken, orginalmethodclass);
            gen.EmitCall(OpCodes.Call, typeof(System.Type).GetMethod("GetTypeFromHandle"), new[] { typeof(System.RuntimeTypeHandle) });
            
            gen.Emit(OpCodes.Ldstr, MethodName);
            BindingFlags bind = (BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic);
            gen.Emit(OpCodes.Ldc_I4_S, (byte)bind);//BindingFlags
            gen.EmitCall(OpCodes.Call, typeof(System.Type).GetMethod("GetMethod", new[] { typeof(string), typeof(BindingFlags) }), null);
            
            gen.Emit(OpCodes.Ldloc_2);
            gen.EmitCall(OpCodes.Callvirt, typeof(System.Reflection.Emit.ILGenerator).GetMethod("EmitCall", new[] { typeof(OpCode), typeof(MethodInfo) , typeof(Type[])}), null);
            gen.Emit(OpCodes.Ldloc_0);
            gen.Emit(OpCodes.Ldsfld, typeof(OpCodes).GetField("Ret"));
            gen.EmitCall(OpCodes.Callvirt, typeof(System.Reflection.Emit.ILGenerator).GetMethod("Emit", new[] { typeof(OpCode) }), null);

            //Assign DynamicMethod to Delegate
            gen.Emit(OpCodes.Ldloc_1);
            gen.Emit(OpCodes.Ldtoken, delegateType);
            gen.EmitCall(OpCodes.Call, typeof(System.Type).GetMethod("GetTypeFromHandle"), new[] { typeof(System.RuntimeTypeHandle) });
            gen.EmitCall(OpCodes.Callvirt, typeof(System.Reflection.Emit.DynamicMethod).GetMethod("CreateDelegate", new[] { typeof(Type)}), null);
            gen.Emit(OpCodes.Castclass, delegateType);
            gen.Emit(OpCodes.Stsfld, staticfield);

            gen.Emit(OpCodes.Ret);
            return method2;
        }


        private static string GetUniqueName(ModuleBuilder mbuilder, string nameBase)
        {
            int number = 2;
            string name = nameBase;
            while (mbuilder.GetType(name) != null)
                name = nameBase + number++;
            return name;
        }

        public static Type CreateDynamicDelegatField(TypeBuilder tbuilder, string fieldname, Type returnType, Type[] param)
        {
            //char[] rc = new char[] { '[', ']', '<', '>', '\t', '\n' };
            //string[] temp = fieldname.Split(rc, StringSplitOptions.RemoveEmptyEntries);
            //string field_upper = String.Join("\n", temp);

            //string field_upper = fieldname;// char.ToUpper(fieldname[0]) + fieldname.Substring(1);
            //Debug.Log("\t\tField " + field_upper);
            string field_upper = fieldname;
            string param_name;

            TypeBuilder t = tbuilder.DefineNestedType(field_upper, TypeAttributes.Class | TypeAttributes.AnsiClass | TypeAttributes.Sealed | TypeAttributes.NestedPublic, typeof(System.MulticastDelegate));
            //Constructor
            ConstructorBuilder cb = t.DefineConstructor(MethodAttributes.SpecialName | MethodAttributes.RTSpecialName | MethodAttributes.HideBySig | MethodAttributes.Public, CallingConventions.Standard, new[] { typeof(object), typeof(IntPtr) });
            cb.DefineParameter(1, ParameterAttributes.None, "object");
            cb.DefineParameter(2, ParameterAttributes.None, "method");
            cb.SetImplementationFlags(MethodImplAttributes.Runtime | MethodImplAttributes.Managed);


            //Invoke()
            MethodBuilder vm1 = t.DefineMethod("Invoke", MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.NewSlot | MethodAttributes.Virtual, CallingConventions.Standard, returnType, param);
            for (int i = 0; i < param.Length; i++)
            {
                param_name = param[i].ToString();
                param_name = param_name.Split('.').Last();
                vm1.DefineParameter(1 + i, ParameterAttributes.None, param_name + (i + 1));
            }
            vm1.SetImplementationFlags(MethodImplAttributes.Runtime | MethodImplAttributes.Managed);


            //BeginInvoke()
            // Type[] tparams2 = { typeof(Rect), typeof(System.AsyncCallback), typeof(object) };
            Type[] tparams2 = new Type[param.Length + 2];
            for (int i = 0; i < param.Length; i++)
                tparams2[i] = param[i];
            tparams2[param.Length] = typeof(System.AsyncCallback);
            tparams2[param.Length+1] = typeof(object);

            MethodBuilder vm2 = t.DefineMethod("BeginInvoke", MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.NewSlot | MethodAttributes.Virtual, CallingConventions.Standard, typeof(System.IAsyncResult), tparams2);
            for (int i = 0; i < param.Length; i++)
            {
                param_name = param[i].ToString();
                param_name= param_name.Split('.').Last();
                vm2.DefineParameter(1 + i, ParameterAttributes.None, param_name + (i + 1));
            }
            vm2.DefineParameter(param.Length, ParameterAttributes.None, "callback");
            vm2.DefineParameter(param.Length+1, ParameterAttributes.None, "object");
            vm2.SetImplementationFlags(MethodImplAttributes.Runtime | MethodImplAttributes.Managed);


            //EndInvoke()
            MethodBuilder vm3 = t.DefineMethod("EndInvoke", MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.NewSlot | MethodAttributes.Virtual, CallingConventions.Standard, returnType, new[] { typeof(System.IAsyncResult) });
            vm3.DefineParameter(1, ParameterAttributes.None, "result");
            vm3.SetImplementationFlags(MethodImplAttributes.Runtime | MethodImplAttributes.Managed);
            Type tt = t.CreateType();

            return tt;
        }

        public static FieldBuilder CreateDynamicStaticDelegateField(TypeBuilder tbuilder, string fieldname, Type FieldType)
        {
            string field_lower = char.ToLower(fieldname[0]) + fieldname.Substring(1);
            return tbuilder.DefineField(field_lower, FieldType, FieldAttributes.Public | FieldAttributes.Static);
        }

        public static void CreateDynamicDelegate(ILGenerator generator, string MethodName, Type ReturnType, Type[] inputTypes)
        {
           // ConstructorInfo[] vctors = typeof(System.Reflection.Emit.DynamicMethod).GetConstructors();

            generator.Emit(OpCodes.Ldstr, MethodName);
            generator.Emit(OpCodes.Ldtoken, ReturnType);
            generator.EmitCall(OpCodes.Call, typeof(System.Type).GetMethod("GetTypeFromHandle"), null);// new[] { typeof(System.RuntimeTypeHandle) });

            CreateDynamicTypeArray(generator, inputTypes);

            generator.Emit(OpCodes.Ldc_I4_1);
            generator.Emit(OpCodes.Newobj, typeof(System.Reflection.Emit.DynamicMethod).GetConstructor(new [] { typeof(string), typeof(Type), typeof(Type[]), typeof(bool)}));
            generator.Emit(OpCodes.Stloc_1);
            generator.Emit(OpCodes.Ldloc_1);
            generator.EmitCall(OpCodes.Callvirt, typeof(System.Reflection.Emit.DynamicMethod).GetMethod("GetILGenerator",new Type[] {}), new[] {typeof(System.Reflection.Emit.ILGenerator)});
        }

        public static void CreateDynamicTypeArray(ILGenerator generator, Type[] array)
        {
            if (array != null && array.Length > 0)
            {
                switch (array.Length)//Number of Elements of the Parameter field
                {
                    case 1: generator.Emit(OpCodes.Ldc_I4_1); break;
                    case 2: generator.Emit(OpCodes.Ldc_I4_2); break;
                    case 3: generator.Emit(OpCodes.Ldc_I4_3); break;
                    case 4: generator.Emit(OpCodes.Ldc_I4_4); break;
                    case 5: generator.Emit(OpCodes.Ldc_I4_5); break;
                    case 6: generator.Emit(OpCodes.Ldc_I4_6); break;
                    case 7: generator.Emit(OpCodes.Ldc_I4_7); break;
                    case 8: generator.Emit(OpCodes.Ldc_I4_8); break;
                    default: generator.Emit(OpCodes.Ldc_I4_S, (byte)array.Length); break;
                }

                generator.Emit(OpCodes.Newarr, typeof(System.Type));//New Object Reference

                for (int i = 0; i < array.Length; i++)
                {
                    generator.Emit(OpCodes.Dup);//Copy Top Value to Stack

                    switch (i)//Set Field Index
                    {
                        case 0: generator.Emit(OpCodes.Ldc_I4_0); break;
                        case 1: generator.Emit(OpCodes.Ldc_I4_1); break;
                        case 2: generator.Emit(OpCodes.Ldc_I4_2); break;
                        case 3: generator.Emit(OpCodes.Ldc_I4_3); break;
                        case 4: generator.Emit(OpCodes.Ldc_I4_4); break;
                        case 5: generator.Emit(OpCodes.Ldc_I4_5); break;
                        case 6: generator.Emit(OpCodes.Ldc_I4_6); break;
                        case 7: generator.Emit(OpCodes.Ldc_I4_7); break;
                        case 8: generator.Emit(OpCodes.Ldc_I4_8); break;
                        default: generator.Emit(OpCodes.Ldc_I4_S, (byte)i); break;
                    }
                    generator.Emit(OpCodes.Ldtoken, array[i]);
                    generator.EmitCall(OpCodes.Call, typeof(System.Type).GetMethod("GetTypeFromHandle"), null);
                    generator.Emit(OpCodes.Stelem_Ref);
                }
            }
            else
            {
                generator.Emit(OpCodes.Ldnull);//Null Array
            }
        }

        public static void SetAssemblyData(ref AssemblyBuilder myAssembly, AssemblyName assemName)
        {
            Type attributeType = typeof(AssemblyFileVersionAttribute);

            // To identify the constructor, use an array of types representing
            // the constructor's parameter types. This ctor takes a string.
            //
            Type[] ctorParameters = { typeof(string) };

            // Get the constructor for the attribute.
            //
            ConstructorInfo ctor = attributeType.GetConstructor(ctorParameters);

            // Pass the constructor and an array of arguments (in this case,
            // an array containing a single string) to the 
            // CustomAttributeBuilder constructor.
            //
            object[] ctorArgs = { "1.0.0001.0" };
            CustomAttributeBuilder attribute =
               new CustomAttributeBuilder(ctor, ctorArgs);

            // Finally, apply the attribute to the assembly.
            //
            myAssembly.SetCustomAttribute(attribute);
            // The pattern described above is used to create and apply
            // several more attributes. As it happens, all these attributes
            // have a constructor that takes a string, so the same ctorArgs
            // variable works for all of them.


            // The AssemblyTitleAttribute sets the Description field on
            // the General tab and the Version tab of the Windows file 
            // properties dialog.
            //
            attributeType = typeof(AssemblyTitleAttribute);
            ctor = attributeType.GetConstructor(ctorParameters);
            ctorArgs = new object[] { "UnityHelper" };
            attribute = new CustomAttributeBuilder(ctor, ctorArgs);
            myAssembly.SetCustomAttribute(attribute);

            // The AssemblyCopyrightAttribute sets the Copyright field on
            // the Version tab.
            //
            attributeType = typeof(AssemblyCopyrightAttribute);
            ctor = attributeType.GetConstructor(ctorParameters);
            ctorArgs = new object[] { "Ⓒ MassiveStudio 2015-2018" };
            attribute = new CustomAttributeBuilder(ctor, ctorArgs);
            myAssembly.SetCustomAttribute(attribute);

            // The AssemblyDescriptionAttribute sets the Comment item.
            //
            /*attributeType = typeof(AssemblyDescriptionAttribute);
            ctor = attributeType.GetConstructor(ctorParameters);
            attribute = new CustomAttributeBuilder(ctor,
               new object[] { "This is a comment." });
            myAssembly.SetCustomAttribute(attribute);*/

            // The AssemblyCompanyAttribute sets the Company item.
            //
            attributeType = typeof(AssemblyCompanyAttribute);
            ctor = attributeType.GetConstructor(ctorParameters);
            attribute = new CustomAttributeBuilder(ctor,
               new object[] { "MassiveStudio" });
            myAssembly.SetCustomAttribute(attribute);

            // The AssemblyProductAttribute sets the Product Name item.
            //
            attributeType = typeof(AssemblyProductAttribute);
            ctor = attributeType.GetConstructor(ctorParameters);
            attribute = new CustomAttributeBuilder(ctor,
               new object[] { "UnityInternalWrapper" });
            myAssembly.SetCustomAttribute(attribute);

            //myAssembly.DefineVersionInfoResource();
        }
    }

    public class FuncDel
    {
        MethodInfo m;

        const BindingFlags f = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance;

        public FuncDel(Type t, string func)
        {
            m = t.GetMethod(func, f);
        }


        public FuncDel(Type t, string func, Type[] funcparamters)
        {
            m = t.GetMethod(func, f,null, funcparamters, null);
        }

        public object _(object obj = null, params object[] par)
        {
            return m.Invoke(obj, par);
        }

        public T _<T>(object obj = null, params object[] par)
        {
            return (T)m.Invoke(obj, par);
        }
    }

    public static class InternalMethods
    {
        public static FuncDel LocalizationDatabase = new FuncDel(ReflectDllMethods.GetClassType("UnityEditor", "LocalizationDatabase"), "GetLocalizedString(string original)");

        public static FuncDel GUIClip_Pop = new FuncDel(ReflectDllMethods.GetClassType("UnityEngine","GUIClip"),"Pop");

    }
}

/*
class DynamicMethodLoader
{
    void GetCameraRect()
    {
        Type accesstype;//Type to access
        Type returntype;
        string thismethod;
        IntializeData(accesstype, returntype, thismethod);
    }


    void IntializeData(Type accesstype, Type returntype, string method)
    {
        


    }

    private void WrapMethod(Type t, MethodInfo m, string method)
    {
        // Generate ILasm for delegate.
        byte[] il = typeof(DynamicMethodLoader).GetMethod(method).GetMethodBody().GetILAsByteArray();

        // Pin the bytes in the garbage collection.
        GCHandle h = GCHandle.Alloc((object)il, GCHandleType.Pinned);
        IntPtr addr = h.AddrOfPinnedObject();
        int size = il.Length;

        // Swap the method.
        MethodRental.SwapMethodBody(t, m.MetadataToken, addr, size, MethodRental.JitImmediate);
    }

    public DTask<bool> ReplacedSolve(int n, DEvent<bool> callback)
    {
        Console.WriteLine("This was executed instead!");
        return true;
    }

}
*/

//DYNAMIC RUNTIOME REPLAEMENT
///https://stackoverflow.com/questions/7299097/dynamically-replace-the-contents-of-a-c-sharp-method
///


/*
Debug.Log("CREATE METHOD");
MethodInfo info=null;
foreach (var a in typeof(DynamicMethodBinder).GetMethods())
{
if (a.Name == "GetAssemblyBuilder")
{
    //Debug.Log("R    METHOD FOUND: " + a.Name);
    info = a;
    break;
}
//Debug.Log("METHOD FOUND: " + a.Name);
}


//info = typeof(DynamicMethodBinder).GetMethod("GetAssemblyBuilder");
if (info == null)
Debug.LogError("NULL METHODINFO");

MethodFlow.GetCallingMethods(info);
return;*/
