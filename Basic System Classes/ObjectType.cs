﻿using System;

namespace MassiveStudio.TeamProjectEditor
{
    [Serializable]
    public sealed class ObjectType
    {
        private string _type;
        public Type type
        {
            get
            {
                return ReflectDllMethods.GetType(this._type);
            }
            set
            {
                _type = value.FullName;
            }
        }
        public ObjectType(System.Type t)
        {
            this.type = t;
        }
    }
}
