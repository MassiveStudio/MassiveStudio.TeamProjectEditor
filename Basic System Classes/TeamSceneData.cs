﻿using System;
using System.Collections.Generic;

namespace MassiveStudio.TeamProjectEditor
{
    [Serializable]
    public class TeamSceneData
    {
        public string SceneName;
        public List<ModelContainer> assets;
        public int id;

        public TeamSceneData(string name)
        {
            assets = new List<ModelContainer>();
            this.SceneName = name;
        }
    }

    [Serializable]
    public class TeamSceneList
    {
        public List<SceneID> sceneNames;
        public TeamSceneList()
        {
            sceneNames= new List<SceneID>();
        }
    }

    [Serializable]
    public class SceneID
    {
        public string sceneName;
        public int sceneID;

        public SceneID(string _name, int _id)
        {
            this.sceneName = _name;
            this.sceneID = _id;
        }
    }
}
