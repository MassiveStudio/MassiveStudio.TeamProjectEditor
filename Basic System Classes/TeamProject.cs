﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MassiveStudio.TeamProjectEditor
{
    [System.Serializable]
    public class TeamProject
    {
        public string ProjectName = "";

        public string IP = "";//SERVER DATA
        public int PORT = 2554;//Standard UserTransport Port
        public int Transport_Port = 2555;//Standard PacketStream Port

        public UserAccount user = new UserAccount();

        public int currentScene = 0;

        public TeamProject(){
            this.user = new UserAccount();
        }
    }
}