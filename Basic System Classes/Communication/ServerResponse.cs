﻿using System;

namespace MassiveStudio.TeamProjectEditor
{
    [Serializable]
    public class ServerResponse
    {
        public ServerResponseType type;
        public object data;
        public ServerResponse(ServerResponseType _type, object _data)
        {
            this.type = _type;
            this.data = _data;
        }
    }

    public enum ServerResponseType
    {
        SceneChangeSuccess,
        SceneChangeFailed,
        SceneCreateSuccess,
        SceneCreateFailed,
        SceneListUpdate,//GET SCENE LIST

        SceneAssetUpdateFailed,
        AssetDatabaseUpdateFailed,

        MaterialRegistered,//GET SERVER ID
        TextureRegistered,//GET SERVER ID
        ShaderRegistered,
    }
}
