﻿using System;


namespace MassiveStudio.TeamProjectEditor
{
    [Serializable]
    public class ClientRequest
    {
        public object value;
        public RequestType type;
        public int sceneID;
        public long synctime;

        public ClientRequest(RequestType _type, object _value, long _synctime=0)
        {
            this.type = _type;
            this.value = _value;
            this.sceneID = TeamServerConnection.currentServer.currentScene;
            this.synctime = _synctime;
        }

        public ClientRequest (RequestType _type, object _value, int _sceneID)
        {
            this.type = _type;
            this.value = _value;
            this.sceneID = _sceneID;
        }
    }

    [Serializable]
    public enum RequestType : byte
    {
        TeamSceneListUpdate,
        TeamSceneChange,
        TeamSceneCreate,
         TeamViewRegister,

        MaterialRegister,
        MaterialUpdate,
        ShaderRegister,
        ShaderUpdate,
        TextureRegister,
        TextureUpdate,

        ModelContainerRequest,
        ComponentUpdate,//NEEDED?????
    }
}
