﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

using MassiveStudio.TeamProjectEditor.Chat;
using MassiveStudio.TeamProjectEditor.EditorWindows.Console;
using MassiveStudio.TeamProjectEditor.Attributes;

namespace MassiveStudio.TeamProjectEditor
{
    public static class TeamServerConnection
    {
        #region Fields
        public static TeamProject currentServer;

        //Main Stream
        public static System.Net.Sockets.TcpClient clientSocket = new System.Net.Sockets.TcpClient();
        public static NetworkStream serverStream;
        public static Thread thread;
        public static Thread Writer;
        public static bool WriteThreadRun = false;
        public static bool connected = false;
        public static bool authenticated = false;
        public static List<object> dataToWrite = new List<object>();
        public static Ping ping;
        public static byte[] bytesFrom = new byte[4];
        public static bool readed = false;

        //TRANSPORT STREAM
        public static System.Net.Sockets.TcpClient transportSocket = new System.Net.Sockets.TcpClient();
        public static NetworkStream transportStream;
        public static bool treaded = false;
        public static List<object> transportData = new List<object>();
        public static Thread TransportStreamWriter;
        public static Thread TransportStreamReader;
        public static bool TransportWriteThreadRun = false;
        public static byte[] transportbytesFrom = new byte[4];
        public static bool tconnected = false;
        #endregion

        #region Delegates
        public delegate void CallbackFunction();

        public static CallbackFunction OnServerStarted;
        public static CallbackFunction OnServerStopped;
        public static CallbackFunction OnServerAuthentificationSucceed;
        public static CallbackFunction OnServerAuthenfificationFailed;
        #endregion

        #region Methods
        #region Connection Basic
        public static bool Connect(TeamProject server)
        {
            currentServer = server;
            if (TeamProjectEditor.TeamSettings.isDebug)
                Debug.Log("Client Started...");

            TeamAssetSystem.TeamAssetCore.InitDelegate();

            if (InitServerStream(server))
            {
                if (InitTransportStream(server))
                {
                    TeamSceneManager.OnStart();
                    //try
                    //{
                    //   if (OnServerStarted.GetInvocationList() != null)
                    //        OnServerStarted();
                    //}
                    //catch (Exception) { }
                    return true;
                }
                else
                {
                    if (TeamProjectEditor.TeamSettings.isDebug) TeamProjectEditor.Console.TeamConsole.Message("ERROR INTIALISATION OF TRANSPORTSTREAM -> Disconnect", "TCP", Color.red);
                    Disconnect();
                    return false;
                }
            }
            else
            {
                if (TeamProjectEditor.TeamSettings.isDebug) TeamProjectEditor.Console.TeamConsole.Message("ERROR INTIALISATION OF SERVERSTREAM -> Disconnect", "TCP", Color.red);
                Disconnect();
                return false;
            }
        }

        public static void Disconnect()
        {
            Utility.Dispatcher.ToBackground(() =>
            {
                if (TeamProjectEditor.TeamSettings.isDebug) TeamProjectEditor.Console.TeamConsole.Message("Disconnect", "TCP", Color.yellow);
                TeamSceneManager.OnShutdown();
                if (connected)
                {
                    connected = false;
                    if (TeamProjectEditor.TeamSettings.isDebug) TeamProjectEditor.Console.TeamConsole.Message("Server Stream Disconnected", "TCP", Color.yellow);
                    clientSocket.Close();
                    serverStream.Close();
                    thread.Abort();
                    Writer.Abort();
                    dataToWrite.Clear();
                    WriteThreadRun = false;
                    authenticated = false;
                }
                if (tconnected)
                {
                    if (TeamProjectEditor.TeamSettings.isDebug) TeamProjectEditor.Console.TeamConsole.Message("Transport Stream Disconnected", "TCP", Color.yellow);
                    tconnected = false;
                    transportSocket.Close();
                    transportStream.Close();
                    TransportStreamWriter.Abort();
                    TransportStreamReader.Abort();
                    transportData.Clear();
                    TransportWriteThreadRun = false;
                }

                try
                {
                    if (OnServerStopped.GetInvocationList() != null)
                        OnServerStopped();
                }
                catch (Exception) { }
            });
        }

        public static void DoPing()
        {
            ping = new Ping(currentServer.IP);
        }

        public static bool InitServerStream(TeamProject server)
        {
            clientSocket = new TcpClient();
            try
            {
                clientSocket.Connect(server.IP, server.PORT);
                if (clientSocket.Connected)
                {
                    if (TeamProjectEditor.TeamSettings.isDebug)
                        TeamProjectEditor.Console.TeamConsole.Message("Server Connection successful", "TCP", Color.green);
                    //if (TeamProjectEditor.TeamSettings.isDebug) Debug.Log("Server Connection successful");

                    if (clientSocket.GetStream().CanWrite == false) { Debug.LogError("NOT WRITABLE"); return false; }
                    else
                    {
                        connected = true;
                        if (TeamProjectEditor.TeamSettings.isDebug) Debug.Log("WRITE ACCEESS: " + clientSocket.GetStream().CanWrite.ToString());

                        serverStream = clientSocket.GetStream();

                        //Writer
                        if (Writer != null && Writer.IsAlive) { if (TeamProjectEditor.TeamSettings.isDebug) TeamProjectEditor.Console.TeamConsole.Message("Open ServerWriter Aborted", "Writer", Color.red); Writer.Abort(); }

                        dataToWrite = new List<object>();
                        Writer = new Thread(ServerStreamWriter);
                        Writer.IsBackground = true;
                        Writer.Start();
                        EditorWindows.Console.Console.RegisterThread(Writer, "Stream Writer");

                        //READER
                        thread = new Thread(ServerStreamReader);
                        thread.IsBackground = true;
                        thread.Start();

                        EditorWindows.Console.Console.RegisterThread(thread, "Stream Reader");

                        if (TeamProjectEditor.TeamSettings.isDebug) Debug.Log("Writer UserData");

                        DirectWrite(serverStream, server.user);
                    }
                }
                else
                {
                    Debug.LogError("NOT CONNECTED");
                    return false;
                }

                return true;
            }
            catch (System.Exception e)
            {
                Debug.Log(e.Message);
                return false;
            }
        }

        public static bool InitTransportStream(TeamProject server)
        {
            transportSocket = new TcpClient();
            try
            {
                //TCP STREAM
                transportSocket.Connect(server.IP, server.Transport_Port);
                if (transportSocket.Connected)
                {
                    if (TeamProjectEditor.TeamSettings.isDebug)
                        TeamProjectEditor.Console.TeamConsole.Message("Transport Stream Connection successful", "TCP", Color.green);
                    //if (TeamProjectEditor.TeamSettings.isDebug) { Debug.Log("Transport Stream Connection successfull"); }

                    if (transportSocket.GetStream().CanWrite == false) { Debug.LogError("NOT WRITABLE"); return false; }
                    else
                    {
                        tconnected = true;
                        Debug.Log("TRANSPORTSTREAM WRITE ACCEESS: " + true);
                        //if (TeamProjectEditor.TeamSettings.isDebug) Debug.Log("WRITE ACCEESS: " + transportSocket.GetStream().CanWrite.ToString());

                        transportStream = transportSocket.GetStream();

                        //Writer
                        if (TransportStreamWriter != null && TransportStreamWriter.IsAlive) { if (TeamProjectEditor.TeamSettings.isDebug) TeamProjectEditor.Console.TeamConsole.Message("Open TRANSPORT STREAM Writer Abort", "Writer", Color.red); TransportStreamWriter.Abort(); }
                        transportData = new List<object>();

                        TransportStreamWriter = new Thread(ServerTransportStreamWriter);
                        TransportStreamWriter.IsBackground = true;
                        TransportStreamWriter.Start();

                        EditorWindows.Console.Console.RegisterThread(TransportStreamWriter, "Transport Writer");

                        //READER
                        TransportStreamReader = new Thread(ServerTransporStreamReader);
                        TransportStreamReader.IsBackground = true;
                        TransportStreamReader.Start();

                        EditorWindows.Console.Console.RegisterThread(TransportStreamReader, "Transport Reader");

                        DirectWrite(transportStream, server.user);
                    }
                }
                else
                {
                    Debug.LogError("NOT CONNECTED");
                    return false;
                }

                return true;
            }
            catch (System.Exception e)
            {
                Debug.Log(e.Message);
                return false;
            }
        }

        public static void OnDestroy()
        {
            if (TeamProjectEditor.TeamSettings.isDebug) TeamProjectEditor.Console.TeamConsole.Message("Writer Abort", "Writer", Color.red);
            Disconnect();
        }
        #endregion

        #region Network IO
        [RunOnBackground]
        public static void ServerStreamReader()
        {
            if (TeamProjectEditor.TeamSettings.isDebug)
                Debug.Log("READ STARTED");
            while (connected)
            {
                readed = true;
                NetworkStream networkdata = clientSocket.GetStream();

                //if( networkdata.DataAvailable)
                try
                {
                    bytesFrom = new byte[4];
                    networkdata.Read(bytesFrom, 0, 4);//File Header 4 Bytes - Contains the size of the following data
                    int lenge = BitConverter.ToInt32(bytesFrom, 0);
                    if (lenge > 1)
                    {
                        if (TeamProjectEditor.TeamSettings.isDebug)
                            Debug.Log("DATA FROM SERVER");
                        byte[] readbytes = new byte[lenge];

                        int dataRead = 0;
                        do
                        {
                            dataRead += networkdata.Read(readbytes, dataRead, lenge - dataRead);
                        }
                        while (dataRead < lenge);
                        if (TeamProjectEditor.TeamSettings.isDebug)
                            Debug.Log("READED");

                        //Header header = new Header(readbytes);
                        if (TeamProjectEditor.TeamSettings.isDebug)
                            Debug.Log("HEADER CREATED");
                        object returndata = Serializer.ToType<object>(readbytes);
                        //UnityEngine.Object returndata = Serializer.Deserialize(header);
                        if (TeamProjectEditor.TeamSettings.isDebug)
                            Debug.Log("READED DATA: " + returndata.GetType().ToString());
                        if (returndata.GetType() == typeof(AccSecurityResponse))
                        {
                            AccSecurityResponse asr = (AccSecurityResponse)returndata;
                            if (asr.valid == 1)
                            {
                                //TeamAssetSystem.TeamAssetCore.OnStart();
                                currentServer.user.UserID = ((AccSecurityResponse)returndata).userid;
                                Debug.Log("USER ID: " + "[" + currentServer.user.UserID + "]");

                                authenticated = true;
                                Debug.LogWarning("Authenticated");

                                Debug.Log("START ASSETCORE SYSTEM");


                                TeamAssetSystem.TeamAssetCore.CheckAssetVersions();
                                TeamAssetSystem.TeamAssetCore.CheckSceneAssetVersions();

                                //OnServerAuthentificationSucceed();

                            }
                            else
                            {
                                Debug.LogError("Authentication Error - Username or Password incorrect");

                                //OnServerAuthenfificationFailed();
                                TeamProjectEditor.Console.TeamConsole.Message("Username or Password incorrect", "User Authentication", Color.red);
                            }
                            //Debug.LogError("Username or Password incorrect");
                        }
                        else if (returndata.GetType() == typeof(TeamMessage))
                        {
                            TeamChat.ReciveData((TeamMessage)returndata);
                        }
                        else if (returndata.GetType() == typeof(ChatMembers))
                        {
                            TeamChat.ReciveChatMemberlist((ChatMembers)returndata);
                        }
                        else
                            TeamSceneManager.AddSceneAsset(returndata);
                    }
                }
                catch (Exception e)
                {
                    if (connected)
                    {
                        TeamProjectEditor.Console.TeamConsole.Message("TEAMSERVERCONNECTION STREAMREADER " + e.Message + "\n" + e.StackTrace, "STREAMREAD", Color.red);
                        Debug.LogError("TEAMSERVERCONNECTION STREAMREADER" + e.Message + "\n" + e.StackTrace);
                    }
                }
            }
            //Debug.LogError("Stream Reader Ended cause of Connection FALSE");
            readed = false;
        }

        [RunOnBackground]
        public static void ServerStreamWriter()
        {
            if (TeamProjectEditor.TeamSettings.isDebug)
                TeamProjectEditor.Console.TeamConsole.Message("Server Writer Started", "Writer", Color.green);

            WriteThreadRun = true;
            while (connected)
            {
                //EditorWindows.Console.Console.Message("Idling    Count: " + dataToWrite.Count + " STATE:"+ Writer.ThreadState.ToString(), "Writer", Color.yellow);
                while (dataToWrite.Count > 0)
                {
                    if (TeamProjectEditor.TeamSettings.isDebug)
                        Debug.Log("Cache : " + dataToWrite.Count);
                    int i = 0;
                    foreach (object o in dataToWrite.ToArray())
                    {
                        //if (TeamProjectEditor.TeamSettings.isDebug)
                        //    TeamProjectEditor.Console.TeamConsole.Message("Writting", "Writer", Color.green);
                        byte[] bytes = null;
                        byte[] length = null;
                        try
                        {
                            // Debug.Log("WRITE OBJECT TO SERVER");
                            bytes = Serializer.TypeToByte<object>(o);
                            //if (TeamProjectEditor.TeamSettings.isDebug)
                            //    TeamProjectEditor.Console.TeamConsole.Message("Serialized", "Writer", Color.yellow);
                            length = System.BitConverter.GetBytes(bytes.Length);
                            //if (TeamProjectEditor.TeamSettings.isDebug)
                            //    TeamProjectEditor.Console.TeamConsole.Message("Header Size Calc " + bytes.Length, "Writer", Color.yellow);

                            if (bytes.Length == 0)
                                Debug.LogError("NULL BYTES SENDING");

                            //if (TeamProjectEditor.TeamSettings.isDebug)
                            //    TeamProjectEditor.Console.TeamConsole.Message("Next Step", "Writer", Color.yellow);

                            if (!serverStream.CanWrite)
                            {
                                TeamProjectEditor.Console.TeamConsole.Message("Stream not Writeable", "Writer", Color.red);
                                break;
                            }
                        }
                        catch (Exception e)
                        {
                            Debug.LogError("SERVER SENDING FORMAT ERROR" + "\n" + e.Message);
                            break;
                        }
                        //if (TeamProjectEditor.TeamSettings.isDebug)
                        //    TeamProjectEditor.Console.TeamConsole.Message("Next Step", "Writer", Color.yellow);


                        //if (TeamProjectEditor.TeamSettings.isDebug)
                        //    TeamProjectEditor.Console.TeamConsole.Message("Next Step", "Writer", Color.yellow);
                        try
                        {
                            //if (TeamProjectEditor.TeamSettings.isDebug)
                            //    TeamProjectEditor.Console.TeamConsole.Message("Start Writing to Server", "Writer", Color.yellow);
                            serverStream.Write(length, 0, 4);
                            //serverStream.Flush();

                            //if (TeamProjectEditor.TeamSettings.isDebug)
                            //     TeamProjectEditor.Console.TeamConsole.Message("Header Written", "Writer", Color.yellow);

                            serverStream.Write(bytes, 0, bytes.Length);
                            //serverStream.Flush();

                            if (TeamProjectEditor.TeamSettings.isDebug)
                                TeamProjectEditor.Console.TeamConsole.Message("Data Written Size " + bytes.Length + " bytes", "Writer", Color.yellow);
                        }
                        catch (System.Exception e)
                        {
                            Debug.LogError(e.Message + "\n" + e.StackTrace);
                        }
                        //if (TeamProjectEditor.TeamSettings.isDebug)
                        //    TeamProjectEditor.Console.TeamConsole.Message("Written - ok", "Writer", Color.green);
                        //Debug.Log("END WRITE");
                        i++;
                    }
                    dataToWrite.RemoveRange(0, i);
                }
                Thread.Sleep(100);
            }
            TeamProjectEditor.Console.TeamConsole.Message("Abort -> Not connected: " + connected, "Writer", Color.red);
            Debug.LogWarning("WRITER THREAD ABORT");
            WriteThreadRun = false;
            Writer.Abort();
        }

        [RunOnBackground]
        public static void ServerTransportStreamWriter()
        {
            if (TeamProjectEditor.TeamSettings.isDebug)
                TeamProjectEditor.Console.TeamConsole.Message("Transport Writer Started", "Writer", Color.green);

            TransportWriteThreadRun = true;
            while (tconnected)
            {
                while (transportData.Count > 0)
                {
                    int i = 0;
                    foreach (object o in transportData.ToArray())
                    {
                        byte[] bytes = null;
                        byte[] length = null;
                        try
                        {
                            bytes = Serializer.TypeToByte<object>(o);
                            length = System.BitConverter.GetBytes(bytes.Length);
                            if (bytes.Length == 0)
                                Debug.LogError("NULL BYTES SENDING");

                            if (!transportStream.CanWrite) { TeamProjectEditor.Console.TeamConsole.Message("Stream not Writeable", "Writer", Color.red); break; }
                        }
                        catch (Exception e)
                        {
                            Debug.LogError("SERVER SENDING FORMAT ERROR" + "\n" + e.Message);
                            break;
                        }

                        try
                        {
                            transportStream.Write(length, 0, 4);
                            transportStream.Flush();
                            transportStream.Write(bytes, 0, bytes.Length);
                            transportStream.Flush();
                        }
                        catch (System.Exception e)
                        {
                            Debug.LogError(e.Message + "\n" + e.StackTrace);
                        }
                        i++;
                    }
                    transportData.RemoveRange(0, i);
                }
                Thread.Sleep(100);
            }
            TeamProjectEditor.Console.TeamConsole.Message("Abort -> Not connected: " + connected, "Writer", Color.red);
            Debug.LogWarning("WRITER THREAD ABORT");
            WriteThreadRun = false;
            Writer.Abort();
        }

        [RunOnBackground]
        public static void ServerTransporStreamReader()
        {
            if (TeamProjectEditor.TeamSettings.isDebug)
                Debug.Log("READ STARTED");
            while (connected)
            {
                treaded = true;
                transportStream = transportSocket.GetStream();

                transportbytesFrom = new byte[4];
                transportStream.Read(transportbytesFrom, 0, 4);//File Header 4 Bytes - Contains the size of the following data
                int lenge = BitConverter.ToInt32(transportbytesFrom, 0);
                if (lenge > 1)
                {
                    if (TeamProjectEditor.TeamSettings.isDebug)
                        Debug.Log("DATA FROM SERVER");
                    byte[] readbytes = new byte[lenge];

                    int dataRead = 0;
                    do
                    {
                        dataRead += transportStream.Read(readbytes, dataRead, lenge - dataRead);
                    }
                    while (dataRead < lenge);
                    object returndata = Serializer.ToType<object>(readbytes);

                    if (TeamProjectEditor.TeamSettings.isDebug)
                        Debug.Log("READED DATA: " + returndata.GetType().ToString());

                    if (returndata.GetType() == typeof(AccSecurityResponse))
                    {
                        AccSecurityResponse asr = (AccSecurityResponse)returndata;
                        if (asr.valid == 1)
                        {
                            currentServer.user.UserID = ((AccSecurityResponse)returndata).userid;
                            Debug.Log("USER ID: " + "[" + currentServer.user.UserID + "]");

                            authenticated = true;
                            //Debug.LogWarning("Transport Stream Authenticated, Flag set: " + ((AccSecurityResponse)returndata).transportflag);
                            TeamProjectEditor.Console.TeamConsole.Message("Transport Stream Authenticated, Flag set: " + ((AccSecurityResponse)returndata).transportflag, Color.green);

                        }
                        else
                        {
                            Debug.LogError("Authentication Error - Username or Password incorrect");
                            TeamProjectEditor.Console.TeamConsole.Message("Username or Password incorrect", "User Authentication", Color.red);
                        }
                        //Debug.LogError("Username or Password incorrect");
                    }
                    else
                        TeamSceneManager.AddSceneAsset(returndata);
                }
            }
            treaded = false;
        }
        #endregion

        #region AsyncList
        //List for bigger datas which cannot be shared in real time (Vertex data, texture, ...)
        public static void AddToTransportStreamWriterList(object data)
        {
            if (connected)
            {
                transportData.Add(data);
                if (TeamProjectEditor.TeamSettings.isDebug)
                    Debug.Log("Added to write TransportStream Query Count: " + transportData.Count);
                if (!TransportWriteThreadRun)
                {
                    Debug.LogError("WRITE THREAD DISENABLED");
                    TeamProjectEditor.Console.TeamConsole.Message("State: " + TransportStreamWriter.ThreadState.ToString(), "Writer", Color.cyan);
                }
            }
            else
                Debug.LogError("Not Connected to the server");
        }
        //Realtime stream (only for small data)
        public static void AddToWriterList(object data)
        {
            if (connected)
            {
                dataToWrite.Add(data);
                if (TeamProjectEditor.TeamSettings.isDebug)
                    Debug.Log("Added to write Query Count: " + dataToWrite.Count);
                if (!WriteThreadRun)
                {
                    Debug.LogError("WRITE THREAD DISENABLED");
                    TeamProjectEditor.Console.TeamConsole.Message("State: " + Writer.ThreadState.ToString(), "Writer", Color.cyan);
                }
            }
            else
                Debug.LogError("Not Connected to the server");
        }

        internal static void DirectWrite(ServerStream stream, object data)
        {
            switch (stream)
            {
                case ServerStream.Main:
                    DirectWrite(serverStream, data);
                    break;
                case ServerStream.Background:
                    DirectWrite(transportStream, data);
                    break;
            }
        }

        public enum ServerStream
        {
            Main = 0,
            Background = 1
        }

        internal static void DirectWrite(NetworkStream stream, object data)
        {
            if (!stream.CanWrite && data != null)
                return;

            byte[] bytes = null;
            byte[] length = null;
            try
            {
                bytes = Serializer.TypeToByte<object>(data);
                length = System.BitConverter.GetBytes(bytes.Length);
                if (bytes.Length == 0)
                    Debug.LogError("NULL BYTES SENDING");
            }
            catch (Exception e)
            {
                Debug.LogError("SERVER SENDING FORMAT ERROR" + "\n" + e.Message);
            }

            try
            {
                stream.Write(length, 0, 4);
                stream.Flush();
                stream.Write(bytes, 0, bytes.Length);
                stream.Flush();
            }
            catch (System.Exception e)
            {
                Debug.LogError(e.Message + "\n" + e.StackTrace);
            }
            Debug.Log("Written");
        }
        #endregion

        #endregion
    }
}