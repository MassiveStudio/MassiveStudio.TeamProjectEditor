﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor
{
    //Comming Soon....
    [System.Serializable]
    public class TeamProjectList
    {
        public List<TeamProject> projects;
    }
}
