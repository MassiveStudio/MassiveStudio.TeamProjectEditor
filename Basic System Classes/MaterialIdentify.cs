﻿using System;

namespace MassiveStudio.TeamProjectEditor
{
    [Serializable]
    public class ShareAssetIdentify
    {
        public AssetUUID uid;

        public ShareAssetIdentify(AssetUUID id)
        {
            this.uid = id;
        }
    }
}
