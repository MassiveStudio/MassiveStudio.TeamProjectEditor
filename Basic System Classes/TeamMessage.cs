﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MassiveStudio.TeamProjectEditor
{
    [System.Serializable]
    public class TeamMessage
    {
        public string text;
        public string sender;
        public string probertys;
        public TeamMessage(string m, string u, string p)
        {
            this.text = m;
            this.sender = u;
            this.probertys = p;
        }
    }
}
