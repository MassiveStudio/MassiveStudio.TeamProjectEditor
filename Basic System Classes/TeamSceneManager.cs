﻿using System;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using System.Linq;
using System.Threading;
using System.Collections.Generic;


using MassiveStudio.TeamProjectEditor.TeamAssetSystem;
using MassiveStudio.TeamProjectEditor.Utility;
using MassiveStudio.TeamProjectEditor.Attributes;

namespace MassiveStudio.TeamProjectEditor
{
    internal static class TeamSceneManager
    {
        #region Fields
        public static bool state = false;
        public static bool updater_state = false;
        public static bool load = false;
        public static List<object> incomingData = new List<object>();

        public static int Objectregistercounter
        {
            get
            {
                _Objectregistercounter++;
                return _Objectregistercounter - 1;
            }
            set
            {
                _Objectregistercounter = value;
            }
        }
        private static int _Objectregistercounter;

        public static Dictionary<string, TeamView> ObjectRegisterQuene;
        public static Dictionary<string, ShareAsset> AssetRegisterQuene;

        //SCENE OBJECTS
        public static List<TeamView> TeamObjects = new List<TeamView>();
        public static bool TeamObject_isDirty = false;
        //LAST UPDATE TIME FOR ALL TEAM SCENE ASSETS
        public static long lt;

        public static bool backgroundloaderLED = false;

        public static TeamSceneList scenes;
        public static int loadedScene = -1;

        public static bool NeedsReloadSceneAssets = false;

        private static string _DataPath;
        public static string DataPath
        {
            get
            {
                if (_DataPath == string.Empty)
                {
                    Utility.Dispatcher.ToMainThread(() =>
                    {
                        _DataPath = Application.dataPath;
                    });

                }
                return _DataPath;
            }
            set
            {
                _DataPath = value;
            }
        }

        private static string _RootPath;
        public static string RootPath
        {
            get
            {
                if (string.IsNullOrEmpty(_RootPath))
                    _RootPath = Path.GetFullPath(TeamSceneManager.DataPath + @"\..\");

                return _RootPath;
            }
        }
        //Used for Identify Current Thread on Dispatcher class
        public static int MAINTHREADID = 0;

        public static Thread SceneAssetLoadingThread;//NOW WITH DISPATCHER
        public static Thread SceneReloadingThread;
        public static Thread DirtyObjectFinderThread;
        public static bool threadstate = false;
        #endregion

        #region Methods
        [InitializeOnLoadMethod]
        public static void SetMainThreadID()
        {
            MAINTHREADID = Thread.CurrentThread.ManagedThreadId;
            DataPath = Application.dataPath;
        }

        public static void Init()
        {
            threadstate = true;
            //MAINTHREADID = Thread.CurrentThread.ManagedThreadId;

            if (SceneReloadingThread == null)
            {
                SceneReloadingThread = new Thread(SceneNeedsReloadChecker);
                SceneReloadingThread.Start();
                Console.TeamConsole.RegisterThread(SceneReloadingThread, "SceneReloadingThread");
            }
            if(DirtyObjectFinderThread==null)
            {
                DirtyObjectFinderThread = new Thread(DirtyObjectChecker);
                DirtyObjectFinderThread.Start();
                Console.TeamConsole.RegisterThread(DirtyObjectFinderThread, "DirtyObjectChecker");
            }

            //EditorApplication.update -= Update;
            //EditorApplication.update += Update;

            ObjectRegisterQuene = new Dictionary<string, TeamView>();

            if (!Directory.Exists("Assets/ServerLibraryFiles/"))
                Directory.CreateDirectory("Assets/ServerLibraryFiles/");
        }

        public static void SceneNeedsReloadChecker()
        {
            while (true)
            {
                if (NeedsReloadSceneAssets)
                {
                    Debug.Log("Reload SceneAssets");
                    NeedsReloadSceneAssets = false;
                    ReloadSceneAssets();
                }
                Thread.Sleep(200);
            }
            Debug.LogWarning("Scene Reloading Thread End()");
        }

        /*
        [Obsolete]
        public static void Update()
        {
            if (NeedsReloadSceneAssets)
            {
                Debug.Log("Reload SceneAssets");
                NeedsReloadSceneAssets = false;
                ReloadSceneAssets();
            }
        }*/

        public static void OnStart()
        {
            Debug.LogWarning("TeamSceneManager - Intialized");
            ActivateSceneAssetUploader();
            AktivateBackgroundTeamAssetImporter();
            //ActivateSceneAssetUploader();

            state = true;
            updater_state = true;

            if (scenes == null)
                scenes = new TeamSceneList();
        }

        [RunOnBackground]
        public static void OnShutdown()
        {
            DeaktivateBackgroundTeamSceneLoader();
            DeactivateSceneAssetUploader();

            incomingData.Clear();

            int cc = TeamObjects.Count;
            for (int i = 0; i < cc; i++)
                Unregister(TeamObjects[0]);

            /*TeamObjects.ForEach(t =>
            {
                if (t != null)
                    Unregister(t);
            });*/

            Objectregistercounter = 0;
            ObjectRegisterQuene.Clear();
            TeamObjects.Clear();
            OnDestroy();
            TeamAssetCore.OnDestroy();
        }

        [RunOnBackground]
        internal static long GetCurrentTime()
        {
            return DateTime.Now.Ticks;
        }

        #region TeamSceneView
        [RunOnMainThread]
        public static void GetSceneList()
        {
            if (!TeamServerConnection.connected)
                return;
            ClientRequest request = new ClientRequest(RequestType.TeamSceneListUpdate, null);
            TeamServerConnection.dataToWrite.Add(request);
        }

        [RunOnMainThread]
        public static void CreateNewTeamScene(string name)
        {
            if (!TeamServerConnection.connected)
                return;
            TeamServerConnection.dataToWrite.Add(new ClientRequest(RequestType.TeamSceneCreate, name));
        }

        [RunOnMainThread]
        internal static void ChangeScene(int value)
        {
            if (!TeamServerConnection.connected)
                return;
            //SET NEW SCENE ID -> after Response from Server
            //TeamServerConnection.currentServer.currentScene = value;
            Debug.Log("Request to Change Scene to " + value);
            ClientRequest request = new ClientRequest(RequestType.TeamSceneChange, value);
            TeamServerConnection.dataToWrite.Add(request);
        }

        [RunOnBackground]
        internal static void OnTeamSceneChanged(int scene)
        {
            Debug.Log("OnSceneChanged  oldScene " + TeamServerConnection.currentServer.currentScene + "  newScene " + scene);
            TeamServerConnection.currentServer.currentScene = scene;
            TeamAssetCore.CheckSceneAssetVersions();
        }
        #endregion

        public static void OnDestroy()
        {
            threadstate = false;
            if (TeamProjectEditor.TeamSettings.isDebug)
                Debug.Log("TeamSceneManager - Destroyed");

            //EditorApplication.update -= LoadSceneAssets;

            //EditorApplication.update -= DirtyObjectChecker;
            //Later
            //EditorApplication.hierarchyWindowChanged -= OnObjectMetaChanged;
            //EditorApplication.update -= Update;

            SceneAssetLoadingThread.Abort();
            SceneReloadingThread.Abort();

            DirtyObjectFinderThread.Interrupt();
            DirtyObjectFinderThread.Abort();

            // DeactivateSceneAssetUploader();
            state = false;
        }

        [RunOnBackground]
        public static void Register(TeamView view)
        {
            if (view == null)
                return;

            if (!ContainsView(view))//!TeamObjects.Contains(view))
            {
                if (TeamProjectEditor.TeamSettings.isDebug)
                    Debug.Log("REGISTER " + view.id);
                int p;
                lock (TeamObjects)
                {
                    TeamObjects.Add(view);
                    //p = (TeamObjects.Count - 1);
                    p = Objectregistercounter;
                    Objectregistercounter++;
                }
                if (TeamProjectEditor.TeamSettings.isDebug)
                    Debug.Log("Count " + TeamObjects.Count);

                if (string.IsNullOrEmpty(view.id.ServerID))//NEW OBJECT
                {
                    view.id.ObjectID = p.ToString();    //ONLY TO IDENTIFY OBJECT
                                                        //SEND ServerID REQUEST
                    TeamServerConnection.AddToWriterList(LibaryClasses.CreateTeamViewRegisterRequest(view));
                    //ModelContainer mod = Converter.GameObjectToModel3D(view.transform);
                    //int p = TeamAssetCore.UpdateSceneAssetList(mod, (ObjectIdentify)null);
                    Debug.Log("Add To Register queue " + view.id.ObjectID + " Pos " + p);
                    TeamSceneManager.ObjectRegisterQuene.Add(p.ToString(), view);

                    CheckReferencedAssets(view);
                }
                else
                    Debug.LogError("ID ISNt EMPTY" + "\n" + "ID: " + view.id.ServerID);
            }
            else
                Debug.LogWarning("OBJ ALREADY REGISTERED");
        }

        [RunOnBackground]
        public static bool ContainsView(TeamView view)
        {
            int id = view.instance_id;
            for (int i = 0; i < TeamObjects.Count; i++)
                if (TeamObjects[i].instance_id == id)
                    return true;
            return false;
        }

        [OptimizedForBackroundMainDispatch]
        public static void CheckReferencedAssets(TeamView view)
        {
            DependencyCheckList checklist = new DependencyCheckList();
            Debug.LogWarning("CHECK REFERENCES");
            Dispatcher.ToMainThread(() =>
            {
                foreach (Transform t in view.transform)
                {
                    Debug.Log(" Add Child to CheckList " + t.gameObject.name);
                    checklist.Add(ReflectDllMethods.GetInstanceID(t), t);
                }
                checklist.Add(ReflectDllMethods.GetInstanceID(view.transform), view.transform);//Root
            });

            Transform tr = null;
            TeamView view2 = null;
            int p;
            for (int i =0; i< checklist.checklist.Count; i++)
            {
                tr = checklist.checklist.Values.ElementAt(i) as Transform;

                Dispatcher.ToMainThread(() =>
                {
                    if (tr.GetComponent<TeamView>() == null && tr.root == tr)
                    {
                        view2 = tr.gameObject.AddComponent<TeamView>();
                        view2.hideFlags = HideFlags.DontSaveInEditor | HideFlags.DontSaveInBuild;
                        lock (TeamObjects)
                        {
                            //p = -TeamObjects.Count;
                            TeamObjects.Add(view2);
                            p = Objectregistercounter;
                            Objectregistercounter++;
                        }
                        view2.id.ObjectID =p.ToString();
                        TeamServerConnection.AddToWriterList(LibaryClasses.CreateTeamViewRegisterRequest(view2));
                        Debug.Log("Add To Register queue " + view2.id.ObjectID + " Pos " + p);
                        TeamSceneManager.ObjectRegisterQuene.Add(p.ToString(), view2);
                    }

                    foreach (Component c in tr.GetComponents<Component>())
                    {
                        DataConverter.CheckComponent(c, (c.GetType().ToString().StartsWith("Unity")) ? false : true,  checklist);
                    }
                });
            }

            Debug.LogWarning(checklist.checklist.Count + " Objects are checked");
        }

        [RunOnBackground]
        public static void Unregister(TeamView view)
        {
            if (view != null)
            {
                Dispatcher.ToMainThread(() =>
                {
                    //if (TeamProjectEditor.TeamSettings.isDebug)
                    //    Debug.Log("Unregister " + view);
                    TeamObjects.Remove(view);
                    Utility.Dispatcher.ToMainThread(() =>
                    {
                        TeamObject o = null;
                        foreach (Transform b in view.transform)
                        {
                            o = b.GetComponent<TeamObject>();
                            if (o != null)
                            {
                                if (TeamProjectEditor.TeamSettings.isDebug)
                                    Debug.Log("REMOVE " + o);
                                GameObject.DestroyImmediate(o);
                            }
                        }
                        //if (TeamProjectEditor.TeamSettings.isDebug)
                        //    Debug.Log("REMOVE " + view);
                        GameObject.DestroyImmediate(view);
                    });
                });
            }
            else
                Debug.LogError("VIEW TO UNREGISTER IS NULL");
        }

        [Obsolete("HAVE TO BE REIMPLEMENTED -> THREAD")]
        public static void ActivateSceneAssetUploader()
        {
            /*if (DirtyAssetUpdater != null)
                DirtyAssetUpdater.Abort();

            DirtyAssetUpdater = new Thread(WaitForDirtyAssetThread);
            DirtyAssetUpdater.IsBackground = true;
            DirtyAssetUpdater.Start();*/

            //EditorApplication.update -= DirtyObjectChecker;
            //EditorApplication.update += DirtyObjectChecker;
            TeamProjectEditor.Console.TeamConsole.Message("TeamAsset Dirty Inspector Activated", "TeamSceneManager", Color.magenta);
            updater_state = true;
        }

        [Obsolete("HAVE TO BE REIMPLEMENTED -> THREAD")]
        public static void DeactivateSceneAssetUploader()
        {
            /* if (DirtyAssetUpdater != null)
                 DirtyAssetUpdater.Abort();*/
            TeamProjectEditor.Console.TeamConsole.Message("TeamAsset Dirty Inspector Deactivated", "TeamSceneManager", Color.magenta);
            //EditorApplication.update -= DirtyObjectChecker;
            updater_state = false;
        }
        [RunOnBackground]
        public static void DeaktivateBackgroundTeamSceneLoader()
        {
            SceneAssetLoadingThread.Abort();

            Debug.Log("SceneAssetLoader deactivated");

            //EditorApplication.update -= LoadSceneAssets;
            TeamProjectEditor.Console.TeamConsole.Message("SceneAssetLoader deactivated", "TeamSceneLoader", Color.magenta);
            state = false;
        }

        [RunOnBackground]
        public static void AktivateBackgroundTeamAssetImporter()
        {
            //EditorApplication.update -= LoadSceneAssets;
            //EditorApplication.update += LoadSceneAssets;

            SceneAssetLoadingThread = new Thread(LoadSceneAssetRoutine);

            Console.TeamConsole.RegisterThread(SceneAssetLoadingThread, "SceneAssetLoading");

            SceneAssetLoadingThread.Start();


            TeamProjectEditor.Console.TeamConsole.Message("SceneAssetLoader activated", "TeamSceneLoader", Color.magenta);
            state = true;
        }

        [RunOnBackground]
        public static void LoadSceneAssetRoutine()
        {
            while (true)
            {
                try
                {
                    LoadSceneAssets();
                }
                catch(Exception e)
                {
                    Debug.LogError("ERROR TEAMSCENEMANAGER " + e.Message + "\n" + e.StackTrace);
                }

                Thread.Sleep(200);
            }

            Debug.Log(" THREAD EXIT FROM WHILE");
        }

        [RunOnBackground]
        public static void AddSceneAsset(object SceneAsset)
        {
            incomingData.Add(SceneAsset);
        }

        [RunOnBackground]
        public static void LoadSceneAssets()
        {
            if (!TeamServerConnection.connected)
                return;

            if ( incomingData.Count > 0)//!load &&
            {
                Console.TeamConsole.Message("Load incoming Data", "TeamAssetLoader");
                Debug.Log("Load Incoming Data");

                object asset = incomingData[0];
                if (asset.GetType() == typeof(ModelContainer))//STILL IN USE?? ->ModelContainerUpdate
                {
                    if (TeamProjectEditor.TeamSettings.isDebug)
                        Debug.Log("ModelContainer");
                    LoadNetworkObject(asset as ModelContainer);
                }
                else if (asset.GetType() == typeof(ObjectIdentify))
                {
                    if (TeamProjectEditor.TeamSettings.isDebug)
                        Debug.Log("MODEL SET SERVER ID");
                    try
                    {
                        SetObjectServerID(asset as ObjectIdentify);
                    }
                    catch (Exception e)
                    {
                        Debug.LogError(e.Message + "\n" + e.StackTrace);
                    }
                }
                else if (asset.GetType() == typeof(ShareAssetIdentify))
                {
                    if (TeamProjectEditor.TeamSettings.isDebug)
                        Debug.Log("ShareAssetIdentify SET SERVER ID");
                    SetAssetServerID(asset as ShareAssetIdentify);
                }
                else if (asset.GetType() == typeof(ServerAssetUpdate))
                {
                    Debug.Log("SERVER ASSET UPDATE");

                    ServerAssetUpdate update = asset as ServerAssetUpdate;

                    switch (update.assettype)
                    {
                        case ShareAssetType.Material:
                            MaterialData md = (MaterialData)update.asset;
                            Material m = Converter.ToMaterial(md);
                            TeamAssetCore.UpdateAssetDatabase(md.mtUUID, update.assettype, m);
                            break;
                        case ShareAssetType.Texture:
                            TextureData td = (TextureData)update.asset;
                            Texture2D t = Converter.ToTexture(td);
                            TeamAssetCore.UpdateAssetDatabase(td.uuid, update.assettype, t);
                            break;
                        case ShareAssetType.Shader:
                            ShaderData sd = (ShaderData)update.asset;
                            Shader s = Converter.ToShader(sd);
                            TeamAssetCore.UpdateAssetDatabase(sd.uuid, update.assettype, s);
                            break;
                    }
                }
                else if (asset.GetType() == typeof(SceneAssetUpdate))
                {
                    Debug.Log("SCENE ASSET UPDATE");
                    SceneAssetUpdate sau = (SceneAssetUpdate)asset;
                    TeamAssetCore.UpdateSceneAssetList(sau.asset, sau.serverID);
                }
                else if (asset.GetType() == typeof(ServerResponse))
                {
                    ServerResponse response = asset as ServerResponse;
                    switch (response.type)
                    {
                        //Scene
                        case ServerResponseType.SceneListUpdate:
                            scenes = (TeamSceneList)response.data;
                            if (TeamSceneView.window != null)
                                TeamSceneView.window.Repaint();
                            break;
                        case ServerResponseType.SceneChangeSuccess:
                            OnTeamSceneChanged((int)response.data);
                            Debug.Log("CHANGED SCENE TO " + ((int)response.data));
                            if (TeamSceneView.window != null)
                                TeamSceneView.window.Repaint();
                            break;
                        case ServerResponseType.SceneChangeFailed:
                            throw new System.Exception("Couldn't change server " + (string)response.data);
                            break;
                        case ServerResponseType.SceneCreateSuccess:
                            Debug.Log("New Scene " + (int)response.data + " created");
                            break;

                        //Register
                        case ServerResponseType.MaterialRegistered:
                        case ServerResponseType.TextureRegistered:
                        case ServerResponseType.ShaderRegistered:
                            TeamAssetCore.SetServerID((ShareAssetIdentify)response.data);
                            break;
                        //AssetUpdate
                        case ServerResponseType.AssetDatabaseUpdateFailed:
                            //-> Back to Lobby
                            break;
                        case ServerResponseType.SceneAssetUpdateFailed:
                            //-> Clear AssetDatabase, Redownload
                            break;
                        default:
                            throw new NotImplementedException("This Client doesn't have a implementation for Type: " + response.type.ToString() + ". Check TeamEditor Version of Server");
                            break;
                    }
                }
                else if (asset.GetType() == typeof(DataClass))
                {
                    DataClass da = asset as DataClass;

                    if (da.scene == TeamServerConnection.currentServer.currentScene)
                    {
                        asset = da.data;
                        Type assetType = asset.GetType();

                        switch (da.type)
                        {
                            case DataType.ComponentUpdate:
                                if (TeamProjectEditor.TeamSettings.isDebug)
                                    Debug.Log("RECIVED Component CHANGES");
                                UdpateSceneModels(asset as List<ObjectComponent>, da.info);
                                TeamAssetCore.UpdateSceneAssetList(asset as List<ObjectComponent>, da.info, da.syncTime);
                                goto END;
                            case DataType.GameObjectUpdate:
                                if (TeamProjectEditor.TeamSettings.isDebug)
                                    Debug.Log("RECIVED GAMEOBJECT CHANGES");
                                UpdateGameobjectData(asset as GameObjectData, da.info);
                                TeamAssetCore.UpdateSceneAssetList(asset as GameObjectData, da.info, da.syncTime);
                                goto END;
                            case DataType.TransformUpdate:
                                Debug.Log("RECIVED TRANSFORM CHANGES");
                                UpdateTransformData(asset as List<TransformData>, da.info);
                                TeamAssetCore.UpdateSceneAssetList(asset as List<TransformData>, da.info, da.syncTime);
                                goto END;
                            case DataType.ModelContainerUpdate:
                                if (TeamProjectEditor.TeamSettings.isDebug)
                                    Debug.Log("RECIVED Modelcontainer");
                                UpdateModelContainer((ModelContainer)asset, da.info);
                                TeamAssetCore.UpdateSceneAssetList((ModelContainer)asset, da.info);
                                goto END;
                        }
                        if (assetType == typeof(RectTransformData))
                        {
                            if (TeamProjectEditor.TeamSettings.isDebug)
                                Debug.LogWarning("RECIEVE RECTTRANSFORM -------------------------   WHERE/WHY?    ------------------------------------------------");
                            UpdateRectTransformData((RectTransformData)asset, da.info);
                        }
                        else if (assetType == typeof(MonoScriptUpdateRequest))
                        {
                            MonoScriptUpdateRequest sc = (MonoScriptUpdateRequest)asset;
                            UpdateMonoScript(sc);
                        }
                        else if (assetType == typeof(MaterialListUpdate))
                        {
                            MaterialListUpdate mu = (MaterialListUpdate)asset;
                            Debug.LogError("MaterialListUpdate  NOT IMPLEMENTED");
                        }
                        else
                        {
                            Debug.LogError("NOT IMPLEMENTED: " + asset.GetType());
                        }
                    }
                    else
                    {
                        Debug.LogError("INCOMING UNKNOWN TYPE " + asset.GetType());
                    }
                }
                END:
                incomingData.RemoveRange(0, 1);
            }
        }

        [RunOnBackground]
        public static void DirtyObjectChecker()
        {
            while (threadstate)
            {
                if (TeamObject_isDirty)// && System.DateTime.Now.Ticks > (lt + 500))
                {
                    //lt = DateTime.Now.Ticks;
                    UploadSceneAssets();
                    Thread.Sleep(500);
                }
            }
        }

        [RunOnBackground]
        public static void UploadSceneAssets()
        {
            TeamProjectEditor.Console.TeamConsole.Message("TeamAssetUploader Idle Dirty:", "DirtyUploader", Color.magenta);
            /*if (!TeamServerConnection.connected)
                return;*/

            /* if (!TeamObject_isDirty)
                 return;*/

            TeamObject_isDirty = false;
            Transform trans = null;
            Transform transb = null;

            List<DataClass> temp = new List<DataClass>();
            foreach (TeamView a in TeamObjects)
            {
                if (a.id.ServerID == String.Empty)//NOT REGISTERED ON SERVER
                    break;

                Dispatcher.ToMainThread(() =>
                {
                    trans = a.transform;
                });


                bool n = false;
                bool t = false;
                if (a.isnew)
                {
                    Dispatcher.ToMainThread(() =>
                    {
                        if (TeamProjectEditor.TeamSettings.isDebug)
                            Debug.Log("SEND new OBJECT DATA " + a.name);
                    });

                    TeamServerConnection.AddToWriterList(LibaryClasses.CreateModelContainerUpdateRequest(trans));
                    a.isnew = false;
                    n = true;
                }
                else
                {
                    if (a.transformupdate)
                    {
                        //if (a.GetComponent<RectTransform>() == null)
                        //{
                        if (TeamProjectEditor.TeamSettings.isDebug)
                            Debug.LogWarning("CREATE Transform UPDATE REQUEST");
                        temp.Add(LibaryClasses.CreateTransformUpdateRequest(trans));
                        /* }
                         else
                         {
                             Debug.LogWarning("CREATE Transform UPDATE REQUEST");
                             temp.Add(LibaryClasses.CreateRectTransformUpdateRequest(a.transform));
                         }*/
                        a.transformupdate = false;
                        t = true;
                    }

                    if (a.gameobjectupdate)
                    {
                        if (TeamProjectEditor.TeamSettings.isDebug)
                            Debug.LogWarning("CREATE GameObject UPDATE REQUEST");
                        temp.Add(LibaryClasses.CreateGameObjectUpdateRequest(trans));
                        a.gameobjectupdate = false;
                    }

                    if (a.update && a.updateComponent.Count > 0)
                    {
                        if (TeamProjectEditor.TeamSettings.isDebug)
                            Debug.LogWarning("CREATE Component UPDATE REQUEST");
                        temp.Add(LibaryClasses.CreateComponentUpdateRequest(trans, a.updateComponent.ToArray()));
                        a.updateComponent.Clear();
                        a.update = false;
                    }

                    foreach (TeamObject b in a.childObjs)
                    {
                        Dispatcher.ToMainThread(() =>
                        {
                            transb = b.transform;
                        });

                        if (b.isnew)
                        {
                            n = true;
                            b.isnew = false;
                            b.transformupdate = false;
                            b.gameobjectupdate = false;
                            b.updateComponent.Clear();
                            b.update = false;
                            temp.Clear();
                            break;
                        }
                        else if (!n)//Time.realtimeSinceStartup > lt + 0.2f &&
                        {
                            if (b.transformupdate && b.id.ServerID != String.Empty && !t)
                            {
                                if (TeamProjectEditor.TeamSettings.isDebug)
                                    Debug.LogWarning("SEND Transform UPDATE REQUEST");
                                temp.Add(LibaryClasses.CreateTransformUpdateRequest(transb));
                                b.transformupdate = false;
                            }

                            if (b.gameobjectupdate && b.id.ServerID != String.Empty)
                            {
                                if (TeamProjectEditor.TeamSettings.isDebug)
                                    Debug.LogWarning("SEND GameObject UPDATE REQUEST");
                                temp.Add(LibaryClasses.CreateGameObjectUpdateRequest(transb));
                                b.gameobjectupdate = false;
                            }

                            if (b.update && b.id.ServerID != String.Empty)
                            {
                                if (TeamProjectEditor.TeamSettings.isDebug)
                                    Debug.LogWarning("SEND Component UPDATE REQUEST");
                                temp.Add(LibaryClasses.CreateComponentUpdateRequest(transb, b.updateComponent.ToArray()));
                                b.updateComponent.Clear();
                                b.update = false;
                            }
                        }
                    }
                    if (n)
                    {
                        if (TeamProjectEditor.TeamSettings.isDebug)
                            Debug.Log("SEND OBJECT DATA " + a.name);
                        TeamServerConnection.AddToWriterList(LibaryClasses.CreateModelContainerUpdateRequest(trans));
                        temp.Clear();
                    }
                    else if (temp.Count > 0)
                    {
                        // Debug.Log("Add " + temp.Count + " DataClasses to Stream Writer");
                        foreach (DataClass c in temp)
                            TeamServerConnection.AddToWriterList(c);
                    }
                }
            }
            //lt = Time.realtimeSinceStartup;
        }

        public static bool CheckifObjectExits(ObjectIdentify id)
        {
            foreach (TeamView a in TeamObjects)
                if (a.id.ServerID == id.ServerID)
                    return true;
            return false;
        }

        public static void SetObjectServerID(ObjectIdentify response)
        {
            //Debug.Log("SET OBJ SERVER ID");
            //Debug.LogWarning("FIND KEY: " + response.ObjectID);

            //string keys = "";
            //foreach(KeyValuePair<string, TeamView> key in  ObjectRegisterQuene)
            //    keys += key.Key + "\n";
            //Debug.Log("KEYS: \n" + keys);

            if(ObjectRegisterQuene.ContainsKey(response.ObjectID))
            {
                TeamView view = ObjectRegisterQuene[response.ObjectID];
                view.id.ServerID = response.ServerID;
                view.id.ObjectID = "";
                view.isnew = true;
                TeamObject_isDirty = true;

                Dispatcher.ToMainThread(() =>
                {
                    //Debug.LogWarning("GAMEOBJECT: " + view.gameObject.name + " has ID " + response.ObjectID);
                    view.Start();//SET SERVERID TO ALL CHILDS, Calculate Bounds, Get InstanceID
                });

                //Debug.Log("Update Local Model Database");
                ModelContainer mod = Converter.GameObjectToModel3D(view);
                int p = TeamAssetCore.UpdateSceneAssetList(mod, response);

                ObjectRegisterQuene.Remove(response.ObjectID);
                //TeamAssetCore.SceneAssets[c].serverid = response.ServerID;
                //if (TeamProjectEditor.TeamSettings.isDebug)
                //    Debug.Log("SERVER ID SETTED");
                // ToUpdateModels.Add(a);
                //a.Update(); -> Transform Error in Sub Thread
                Debug.Log("SET SERVER OBJECT ID -  OK");

                //Send object to server
                DataClass data = new DataClass(Converter.ToObjectInfo(view.obj), (object)mod);
                data.type = DataType.ModelContainerUpdate;
                TeamServerConnection.AddToWriterList(data);
                return;
            }
            else
            {
                Debug.LogError("Object Key Not Found in ObjectRegisterQuene!!");
            }
            /*
            int c = 0;
            foreach (TeamView a in TeamObjects)
            {
                if (a.id.ObjectID == response.ObjectID)
                {
                    if (TeamProjectEditor.TeamSettings.isDebug)
                        Debug.Log("OBJ WITH ID: " + a.id.ObjectID + "/" + a.id.ServerID + " FOUND");
                    a.id.ObjectID = "";
                    a.id.ServerID = response.ServerID;

                    a.isnew = true;
                    TeamObject_isDirty = true;

                    Dispatcher.ToMainThread(() =>
                    {
                        a.SetIDs();//SET SERVERID TO ALL CHILDS
                    

                    //Set ID to Model data for later loading

                    Debug.Log("Get Registered (temp) data " + response.ObjectID + "  " + ObjectRegisterQuene.Count + "   " + ObjectRegisterQuene.ContainsKey(response.ObjectID) + " LIST POS: " + ObjectRegisterQuene[response.ObjectID]);
                    //ModelContainer m= TeamAssetCore.SceneAssets[ObjectRegisterQuene[response.ObjectID]];
                    //if(m!=null)
                    //   m.serverid = response.ServerID;

                    TeamProjectEditor.TeamServerConnection.AddToTransportStreamWriterList(LibaryClasses.CreateModelContainerUpdateRequest(a.transform));
                    });

                    Debug.Log("Update GAMEOBJECT DATA");
                    ModelContainer mod = Converter.GameObjectToModel3D(a);
                    int p = TeamAssetCore.UpdateSceneAssetList(mod, response);

                    ObjectRegisterQuene.Remove(response.ObjectID);
                    //TeamAssetCore.SceneAssets[c].serverid = response.ServerID;

                    if (TeamProjectEditor.TeamSettings.isDebug)
                        Debug.Log("SERVER ID SETTED");
                    // ToUpdateModels.Add(a);
                    //a.Update(); -> Transform Error in Sub Thread
                    return;
                }
                c++;
            }
            Debug.LogError("OBJ NOT FOUND  "  + response.ObjectID +":"+ response.ServerID);*/
        }
    
        public static void SetAssetServerID(ShareAssetIdentify response)
        {
            Debug.Log("SET ID");
            TeamAssetSystem.TeamAssetCore.SetServerID(response.uid);
        }

        [RunOnBackground]
        public static void LoadNetworkObject(ModelContainer obj)
        {
            if (CheckifObjectExits(obj.models[0].id))//Update Last
            {
                int index = GetRootIndexID(obj.models[0].id.ServerID);
                if (TeamObjects[index] != null)
                {
                    UnityEngine.Object.DestroyImmediate(TeamObjects[index].gameObject);
                    TeamObjects[index] = Converter.Model3DToGameObject(obj);
                }
            }
            else
                Register(Converter.Model3DToGameObject(obj));
            //  TeamObjects.Add(Converter.Model3DToGameObject(obj));

            load = false;
        }

        public static int GetRootIndexID(string id)
        {
            int b = 0;
            foreach (TeamView a in TeamObjects)
            {
                if (a.id.ServerID == id)
                    return b;

                b++;
            }
            return -1;
        }

        public static void UpdateModelContainer(ModelContainer data, ObjectIdentify info)
        {
            if (CheckifObjectExits(info))//Update Last
            {
                int index = GetRootIndexID(info.ServerID);
                if (TeamObjects[index] != null)
                {
                    UnityEngine.Object.DestroyImmediate(TeamObjects[index].gameObject);
                    TeamObjects[index] = Converter.Model3DToGameObject(data);
                }
            }
            else
                Register(Converter.Model3DToGameObject(data));
            //  TeamObjects.Add(Converter.Model3DToGameObject(data.data));
        }

        public static void UpdateTransformData(List<TransformData> data, ObjectIdentify info)
        {
            List<TransformData> transforms = data;
            string serverid = info.ServerID;// data.id.ServerID;

            TeamView rootobj = RootTransformTeamView(serverid);
            if (rootobj != null)
            {
                foreach (TransformData a in transforms)
                {
                    if (a.parent.Length > 0)
                    {
                        foreach (TeamObject b in rootobj.childObjs)
                        {
                            if (b.id.ObjectID == a.parent)//Parent == ObjectID
                            {
                                if (TeamProjectEditor.TeamSettings.isDebug)
                                    Debug.LogWarning("PARENT FOUND");
                                if (a.GetType() == typeof(RectTransformData))
                                {
                                    //Debug.LogWarning("RECT TRANSFORM FOUND");
                                    Converter.ToRectTransform((RectTransformData)a, b.transform.GetComponent<RectTransform>());
                                }
                                else
                                    Converter.ToTransform(a, b.transform);
                                break;
                            }
                        }
                    }
                    else//The Root Object self
                    {
                        if (a.GetType() == typeof(RectTransformData))
                        {
                            // Debug.LogWarning("RECT TRANSFORM FOUND");
                            Converter.ToRectTransform((RectTransformData)a, rootobj.transform.GetComponent<RectTransform>());
                        }
                        else
                            Converter.ToTransform(a, rootobj.transform);
                        //Converter.ToTransform(a, rootobj.transform);
                    }
                }
            }
            else
            {
                TeamServerConnection.AddToWriterList(LibaryClasses.CreateModelContainerRequest(info));
                //TeamServerConnection.AddToWriterList(new ModelContainerRequest(info));
            }
        }

        public static void UpdateGameobjectData(GameObjectData data, ObjectIdentify info)
        {
            if (CheckifObjectExits(info))
            {
                TeamView rootobj = RootTransformTeamView(info.ServerID);

                if (info.ObjectID.Length == 0)
                {
                    Converter.ToGameObject(data, rootobj.gameObject);
                }
                else
                {
                    foreach (TeamObject b in rootobj.childObjs)
                    {
                        if (b.id.ObjectID == info.ObjectID)
                        {
                            Converter.ToGameObject(data, b.gameObject);
                            break;
                        }
                    }
                }
            }
            else
            {
                Debug.LogError("FALSCHER GAMEOBJECT UPDATE DATENSATZ");
            }
        }

        [RunOnBackground]
        public static void UpdateRectTransformData(RectTransformData data, ObjectIdentify info)
        {
            Transform t = GetTransformWithIdentify(data.parent, info);
            RectTransform r = t.GetComponent<RectTransform>();
            if (r == null)
                r = t.gameObject.AddComponent<RectTransform>();
            Converter.ToRectTransform(data, r);
            //data.ToRectTransform(r);
        }

        public static TeamView RootTransformTeamView(string serverid)
        {
            foreach (TeamView a in TeamObjects)
                if (a.id.ServerID == serverid)
                    return a;
            return null;
        }

        public static void UdpateSceneModels(List<ObjectComponent> data, ObjectIdentify info)
        {
            GameObject obj = null;
            foreach (TeamView a in TeamObjects)
                if (a.id.ServerID == info.ServerID)
                {
                    if (info.ObjectID == "" || info.ObjectID == String.Empty)
                    {
                        obj = a.transform.gameObject;
                    }
                    else
                    {
                        foreach (TeamObject b in a.childObjs)
                        {
                            if (b.id.ObjectID == info.ObjectID)
                            {
                                obj = b.transform.gameObject;
                                break;
                            }
                        }
                    }
                    DataConverter.UnBakeComponents(obj, data);
                    break;
                }
        }

        public static Transform GetTransformWithIdentify(string parentid, ObjectIdentify info)
        {
            string serverid = info.ServerID;// data.id.ServerID;

            TeamView rootobj = RootTransformTeamView(serverid);
            if (rootobj != null)
            {
                // foreach (TransformData a in transforms)
                //{
                if (parentid.Length > 0)
                {
                    foreach (TeamObject b in rootobj.childObjs)
                    {
                        if (b.id.ObjectID == parentid)//Parent == ObjectID
                        {
                            if (TeamProjectEditor.TeamSettings.isDebug)
                                Debug.LogWarning("PARENT FOUND");
                            return b.transform;
                        }
                    }
                }
                else//The Root Object self
                {
                    return rootobj.transform;
                }
                // }
            }
            else
            {
                return null;
            }
            return null;
        }

        public static void ReloadSceneAssets()//Needs to run on Main thread
        {
            Debug.Log("RELOAD SCENE ASSETS");

            Utility.Dispatcher.ToMainThread(() =>
            {
                for (int i = 0; i < TeamObjects.Count; i++)
                    if (TeamObjects[i] != null)
                        GameObject.DestroyImmediate(TeamObjects[i].gameObject, false);
            });

            TeamObjects.Clear();

            if (TeamAssetCore.SceneAssets==null || TeamServerConnection.currentServer.currentScene != loadedScene)
                TeamAssetCore.SceneAssets = TeamAssetDatabase.LoadSceneAssets(TeamServerConnection.currentServer.currentScene);

            Debug.LogWarning("Loaded and Initialized GameObjects: " + TeamAssetCore.SceneAssets.Count);

            TeamView view = null;
            int cc = 0;
            foreach (ModelContainer data in TeamAssetCore.SceneAssets)
            {
                Debug.Log("OBJ " + cc);
                view = Converter.Model3DToGameObject(data);
                cc++;
            }
            Debug.LogWarning("New SceneAssets Loaded");

            Utility.Dispatcher.ToMainThread(() =>
            {
                if (SceneView.currentDrawingSceneView != null)
                    SceneView.currentDrawingSceneView.Repaint();
            });
        }
        #endregion

        #region Implementation Later

        public static void AktivateGameObjectEventHandler()
        {
            EditorApplication.hierarchyWindowChanged -= OnObjectMetaChanged;
            EditorApplication.hierarchyWindowChanged += OnObjectMetaChanged;

        }
        //TO Catch Transform parent changing -> later
        public static void OnObjectMetaChanged()
        {

        }

        public static void UpdateMonoScript(MonoScriptUpdateRequest m)
        {
            MonoEvent e = m.monoevent;
            if (e == MonoEvent.Changed || e == MonoEvent.Created)
            {
                File.WriteAllBytes(TeamSceneManager.DataPath + "/TeamServer/Script/" + m.script.scriptname, m.script.data);
            }
        }

        #endregion
    }
}