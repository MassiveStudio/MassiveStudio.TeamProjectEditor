﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

using UnityEngine;

using MassiveStudio.TeamProjectEditor.TeamAssetSystem;
using MassiveStudio.TeamProjectEditor.Attributes;

namespace MassiveStudio.TeamProjectEditor
{
    public static class LibaryClasses
    {
        [RunOnBackground]
        public static DataClass CreateModelContainerUpdateRequest(Transform t)//Has already ID
        {
            DataClass data = null;
            ModelContainer mod = Converter.GameObjectToModel3D(t);
            data = new DataClass(Converter.ToObjectInfo(t), (object)mod);
            data.type = DataType.ModelContainerUpdate;
            TeamAssetCore.UpdateSceneAssetList(mod, data.info);
            return data;
        }

        [RunOnBackground]
        public static DataClass CreateMonoScriptUpdateRequest(string file, MonoEvent ev)
        {
            MonoScriptUpdateRequest ms = new MonoScriptUpdateRequest();
            ms.script = Converter.ToMonoScript(file);
            ms.monoevent = ev;
            return new DataClass(file, ms);
        }

        [Obsolete]
        public static DataClass CreateMaterialLibaryUpload(Material m)
        {
            MaterialData mu = new MaterialData();
            mu = Converter.ToMaterialData(m);
            return new DataClass((ObjectIdentify)null, (object)mu);
        }

        [RunOnBackground]
        public static DataClass CreateGameObjectUpdateRequest(Transform t)
        {
            DataClass data = null;
            GameObjectData obj = Converter.ToGameObjectData(t.gameObject);
            //GameObjectUpdate gu = new GameObjectUpdate();
            //gu.data = Converter.ToGameObjectData(t.gameObject);
            //gu.id = Converter.ToObjectInfo(t);
            data = new DataClass(Converter.ToObjectInfo(t), (object)obj);
            data.type = DataType.GameObjectUpdate;

            TeamAssetCore.UpdateSceneAssetList(obj, data.info, data.syncTime);
            // Debug.LogWarning("GameObjectData: " + gu.data.name);
            return data;
        }

        [RunOnBackground]
        public static DataClass CreateTransformUpdateRequest(Transform t)
        {
            DataClass data = null;
            List<TransformData> list = Converter.ToRectTransformDataList(t);
            if (TeamProjectEditor.TeamSettings.isDebug)
                Debug.Log("Transforms: " + list.Count);
            if (list.Count == 0)
            {
                Debug.LogError("NULL TRANSFORM UPDATE LIST");
                return null;
            }
            else
            {
                data = new DataClass(Converter.ToObjectInfo(t), (object)list);
                data.type = DataType.TransformUpdate;

                TeamAssetCore.UpdateSceneAssetList(list, data.info, data.syncTime);
                return data;
            }
        }

        [Obsolete]
        public static DataClass CreateRectTransformUpdateRequest(Transform r)
        {
            DataClass data;
            RectTransformUpdate rt = new RectTransformUpdate();

            rt.datas.AddRange(Converter.ToTransformDataList(r));
            if (TeamProjectEditor.TeamSettings.isDebug)
                Debug.Log("Transforms: " + rt.datas.Count);
            //tu.id = Converter.ToObjectInfo(t);
            if (rt.datas.Count == 0)
            {
                Debug.LogError("NULL TRANSFORM UPDATE LIST");
                return null;
            }
            else
            {
                data = new DataClass(Converter.ToObjectInfo(r), (object)rt);
                return data;
            }
        }

        [RunOnBackground]
        public static DataClass CreateComponentUpdateRequest(Transform t, Component[] comps)
        {
            DataClass data = null;
            List<ObjectComponent> list = DataConverter.BakeComponents(comps, t);
            data = new DataClass(Converter.ToObjectInfo(t), (object)list);
            data.type = DataType.ComponentUpdate;
            TeamAssetCore.UpdateSceneAssetList(list, data.info, data.syncTime);
            return data;
        }
        
        [RunOnBackground]
        public static ClientRequest CreateMaterialRegisterRequest(Material m)
        {
            if (TeamProjectEditor.TeamSettings.isDebug)
                Debug.LogWarning("CREATE MATERIAL REGISTER REQUEST");
            AssetUUID data = new AssetUUID(m);
            Debug.LogWarning("MATERIAL REGISTER DATA: " + data.ToString());
            return new ClientRequest(RequestType.MaterialRegister, data);
            //return new MaterialRegisterRequest(Converter.ToMaterialData(m));
        }

        [RunOnBackground]
        public static ClientRequest CreateMaterialUpdateRequest(Material m)
        {
            if (TeamProjectEditor.TeamSettings.isDebug)
                Debug.LogWarning("CREATE MATERIAL UPDATE REQUEST");
            return new ClientRequest(RequestType.MaterialUpdate, Converter.ToMaterialData(m));
        }

        [RunOnBackground]
        public static ClientRequest CreateShaderRegisterRequest(Shader s)
        {
            if (TeamProjectEditor.TeamSettings.isDebug)
                Debug.LogWarning("CREATE SHADER REGISTER REQUEST");
            AssetUUID data = new AssetUUID(s);
            Debug.LogWarning("SHADER REGISTER DATA: " + data.ToString());
            return new ClientRequest(RequestType.ShaderRegister, data);
            //return new MaterialRegisterRequest(Converter.ToMaterialData(m));
        }

        [RunOnBackground]
        public static ClientRequest CreateShaderUpdateRequest(Shader s)
        {
            if (TeamProjectEditor.TeamSettings.isDebug)
                Debug.LogWarning("CREATE SHADER UPDATE REQUEST");
            return new ClientRequest(RequestType.ShaderUpdate, Converter.ToShaderData(s));
        }

        [RunOnBackground]
        public static ClientRequest CreateTextureRegisterRequest(Texture2D t)
        {
            if (TeamProjectEditor.TeamSettings.isDebug)
                Debug.LogWarning("CREATE TEXTURE REGISTER REQUEST");
            return new ClientRequest(RequestType.TextureRegister, new AssetUUID(t));
        }

        [RunOnBackground]
        public static ClientRequest CreateTextureUpdateRequest(Texture2D t)
        {
            if (TeamProjectEditor.TeamSettings.isDebug)
                Debug.LogWarning("CREATE Texture UPDATE REQUEST");
            return new ClientRequest(RequestType.TextureUpdate, Converter.ToTextureData(t));
        }

        [RunOnBackground]
        public static ClientRequest CreateTeamViewRegisterRequest(TeamView t)
        {
            if (TeamProjectEditor.TeamSettings.isDebug)
                Debug.LogWarning("CREATE TEAMVIEW REGISTER REQUEST");
            return new ClientRequest(RequestType.TeamViewRegister, t.id.ObjectID);
            //return new TeamViewRegisterRequest(t.id.ObjectID);
        }

        [RunOnBackground]
        public static ClientRequest CreateModelContainerRequest(ObjectIdentify i)
        {
            return new ClientRequest(RequestType.ModelContainerRequest, i);
        }
    }
}
