﻿using System;

namespace MassiveStudio.TeamProjectEditor
{
    [Serializable]
    public class MonoScript
    {
        public string scriptname = string.Empty;
        public byte[] data = null;
        public string creator = string.Empty;
    }
}