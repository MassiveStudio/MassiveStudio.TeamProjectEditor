﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor.Basic_System_Classes
{

    public struct GLGUIRect
    {
        private Rect m_Rect;
        public GLGUIRect(Rect aRect)
        {
            var P = GUIUtility.GUIToScreenPoint(new Vector2(aRect.x, aRect.y));
            aRect.x = P.x;
            aRect.y = Screen.height - P.y - aRect.height;
            m_Rect = aRect;
        }

        public static implicit operator Rect(GLGUIRect aObj)
        {
            return aObj.m_Rect;
        }
        public static implicit operator GLGUIRect(Rect aRect)
        {
            return new GLGUIRect(aRect);
        }
    }


    public static class Drawing
    {
        public static Texture2D aaLineTex = null;
        public static Texture2D lineTex = null;
        public static Material glLineMat = null;




        public static void GUIViewport(Rect aR)
        {
            var P = GUIUtility.GUIToScreenPoint(new Vector2(aR.x, aR.y));
            aR.x = P.x;
            aR.y = Screen.height - P.y - aR.height;
            GL.Viewport(aR);
            GL.LoadPixelMatrix(0, aR.width, aR.height, 0);
        }

        public static void SetViewport(GLGUIRect aR)
        {
            Rect R = aR;
            GL.Viewport(R);
            GL.LoadPixelMatrix(0, R.width, R.height, 0);
        }


        public static void DrawLine(Vector2 pointA, Vector2 pointB, Color color, float width, bool antiAlias)
        {
            Color savedColor = GUI.color;
            Matrix4x4 savedMatrix = GUI.matrix;

            if (!lineTex)
            {
                lineTex = new Texture2D(1, 1, TextureFormat.ARGB32, true);
                lineTex.SetPixel(0, 1, Color.white);
                lineTex.Apply();
            }
            if (!aaLineTex)
            {
                aaLineTex = new Texture2D(1, 3, TextureFormat.ARGB32, true);
                aaLineTex.SetPixel(0, 0, new Color(1, 1, 1, 0));
                aaLineTex.SetPixel(0, 1, Color.white);
                aaLineTex.SetPixel(0, 2, new Color(1, 1, 1, 0));
                aaLineTex.Apply();
            }
            if (antiAlias) width *= 3;
            float angle = Vector3.Angle(pointB - pointA, Vector2.right) * (pointA.y <= pointB.y ? 1 : -1);
            float m = (pointB - pointA).magnitude;
            if (m > 0.01f)
            {
                Vector3 dz = new Vector3(pointA.x, pointA.y, 0);

                GUI.color = color;
                GUI.matrix = translationMatrix(dz) * GUI.matrix;
                GUIUtility.ScaleAroundPivot(new Vector2(m, width), new Vector3(-0.5f, 0, 0));
                GUI.matrix = translationMatrix(-dz) * GUI.matrix;
                GUIUtility.RotateAroundPivot(angle, Vector2.zero);
                GUI.matrix = translationMatrix(dz + new Vector3(width / 2, -m / 2) * Mathf.Sin(angle * Mathf.Deg2Rad)) * GUI.matrix;

                if (!antiAlias)
                    GUI.DrawTexture(new Rect(0, 0, 1, 1), lineTex);
                else
                    GUI.DrawTexture(new Rect(0, 0, 1, 1), aaLineTex);
            }
            GUI.matrix = savedMatrix;
            GUI.color = savedColor;
        }

        public static void bezierLine(Vector2 start, Vector2 startTangent, Vector2 end, Vector2 endTangent, Color color, float width, bool antiAlias, int segments)
        {
            Vector2 lastV = cubeBezier(start, startTangent, end, endTangent, 0);
            for (int i = 1; i <= segments; ++i)
            {
                Vector2 v = cubeBezier(start, startTangent, end, endTangent, i / (float)segments);

                Drawing.DrawLine(
                    lastV,
                    v,
                    color, width, antiAlias);
                lastV = v;
            }
        }

        /*
        -+++           4
        ************* 13(9 float +4 vector == 17float)
        */
        private static Vector2 cubeBezier(Vector2 s, Vector2 st, Vector2 e, Vector2 et, float t)
        {
            float rt = 1 - t;
            float rtt = rt * t;
            return rt * rt * rt * s + 3 * rt * rtt * st + 3 * rtt * t * et + t * t * t * e;
            //return rt*rt*(rt * s + 3 * t * st) + (3 * rt * et + t * e) *t*t;
        }
        //********** (4 float + 6 Vector == 16 float )
        //-+++       (1 float + 3 vector == 7 float)


        /*
        +-+++-+-+ 9   (18 float additions)
        ******* 7     (14 float mult.)
        */
        private static Vector2 cubeBezier2(Vector2 s, Vector2 st, Vector2 e, Vector2 et, float t)
        {
            return (((-s + 3 * (st - et) + e) * t + (3 * (s + et) - 6 * st)) * t + 3 * (st - s)) * t + s;
        }



        private static Matrix4x4 translationMatrix(Vector3 v)
        {
            return Matrix4x4.TRS(v, Quaternion.identity, Vector3.one);
        }

        public static void BeginGL(Color color, int mode)
        {
            if (glLineMat == null)
            {
                glLineMat = new Material("Shader \"Lines/Colored Blended\" {" +
                                       "SubShader { Pass {" +
                                       "	BindChannels { Bind \"Color\",color }" +
                                       "	Blend SrcAlpha OneMinusSrcAlpha" +
                                       "	ZWrite Off Cull Off Fog { Mode Off }" +
                                       "} } }");
                glLineMat.hideFlags = HideFlags.HideAndDontSave;
                glLineMat.shader.hideFlags = HideFlags.HideAndDontSave;

            }
            glLineMat.SetPass(0);
            GL.Color(color);
            GL.Begin(mode);
        }

        public static void DrawGLLine(Vector2 pointA, Vector2 pointB, Color color)
        {
            BeginGL(color, GL.LINES);
            GL.Vertex3(pointA.x, pointA.y, 0);
            GL.Vertex3(pointB.x, pointB.y, 0);
            GL.End();
        }
        public static void BezierLineGL(Vector2 start, Vector2 startTangent, Vector2 end, Vector2 endTangent, Color color, int segments)
        {
            if (Event.current != null && Event.current.type != EventType.Repaint)
                return;

            Vector2 lastV = cubeBezier(start, startTangent, end, endTangent, 0);
            BeginGL(color, GL.LINES);
            for (int i = 1; i <= segments; ++i)
            {
                Vector2 v = cubeBezier(start, startTangent, end, endTangent, i / (float)segments);
                GL.Vertex3(lastV.x, lastV.y, 0);
                GL.Vertex3(v.x, v.y, 0);
                lastV = v;
            }
            GL.End();
        }

        public static void BezierLineGL2(Vector2 start, Vector2 startTangent, Vector2 end, Vector2 endTangent, Color color, int segments)
        {
            if (Event.current != null && Event.current.type != EventType.Repaint)
                return;
            Vector2 lastV = cubeBezier2(start, startTangent, end, endTangent, 0);
            BeginGL(color, GL.LINES);
            for (int i = 1; i <= segments; ++i)
            {
                Vector2 v = cubeBezier2(start, startTangent, end, endTangent, i / (float)segments);
                GL.Vertex3(lastV.x, lastV.y, 0);
                GL.Vertex3(v.x, v.y, 0);
                lastV = v;
            }
            GL.End();
        }

        // Calculates the normalized normal vector (90° clockwise)
        public static Vector2 norm(this Vector2 dir)
        {
            Vector2 res = new Vector2(-dir.y, dir.x);
            return res.normalized;
        }

        public static void BezierLineGL(Vector2 start, Vector2 startTangent, Vector2 end, Vector2 endTangent, Color color, int segments, float width)
        {
            if (Event.current != null && Event.current.type != EventType.Repaint)
                return;
            Vector2 lastV = cubeBezier(start, startTangent, end, endTangent, 0);
            BeginGL(color, GL.QUADS);
            Vector2 lastNormal = Vector2.zero;
            for (int i = 1; i <= segments; ++i)
            {
                float stylefactor = 1;//(1-Mathf.Abs(1-(i*2.0f)/(float)segments));
                Vector2 v = cubeBezier(start, startTangent, end, endTangent, i / (float)segments);
                Vector2 normal = norm(v - lastV);
                Vector2 n = (normal + lastNormal).normalized * (width * 0.5f * stylefactor);
                if (i == 1)
                {
                    GL.Vertex3(lastV.x + n.x, lastV.y + n.y, 0);
                    GL.Vertex3(lastV.x - n.x, lastV.y - n.y, 0);
                }
                if (i == segments)
                    n = lastNormal * (width * 0.5f * stylefactor);

                GL.Vertex3(v.x - n.x, v.y - n.y, 0);
                GL.Vertex3(v.x + n.x, v.y + n.y, 0);
                if (i != segments)
                {
                    GL.Vertex3(v.x + n.x, v.y + n.y, 0);
                    GL.Vertex3(v.x - n.x, v.y - n.y, 0);
                }
                lastV = v;
                lastNormal = normal;
            }
            GL.End();
        }

        public static void DrawRect(Rect R, Color color)
        {
            BeginGL(color, GL.QUADS);
            GL.Vertex3(R.xMin, R.yMin, 0);
            GL.Vertex3(R.xMax, R.yMin, 0);
            GL.Vertex3(R.xMax, R.yMax, 0);
            GL.Vertex3(R.xMin, R.yMax, 0);
            GL.End();
        }
    }
}
