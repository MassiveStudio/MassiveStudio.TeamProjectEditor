﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MassiveStudio.TeamProjectEditor
{
    [System.Serializable]
    internal class UserPermission
    {
        Dictionary<string, AccessModify> permission = new Dictionary<string, TeamProjectEditor.AccessModify>();

        public void AddChangePermission(string name, AccessModify a)
        {
            if (permission.ContainsKey(name))
            {
                permission[name] = a;
            }
            else
            {
                permission.Add(name, a);
            }
        }

        public void RemovePermission(string name)
        {
            if (permission.ContainsKey(name))
                permission.Remove(name);
        }
    }

    [System.Serializable]//Later
    internal class AccessModify : System.Object
    {
        bool read =false;
        bool write=false;
    }
}
