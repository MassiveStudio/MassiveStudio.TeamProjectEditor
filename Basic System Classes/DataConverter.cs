﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

using MassiveStudio.TeamProjectEditor.EditorWindows.Console;
using MassiveStudio.TeamProjectEditor.TeamAssetSystem;
using MassiveStudio.TeamProjectEditor.Attributes;


namespace MassiveStudio.TeamProjectEditor
{
    public static class DataConverter
    {
        [OptimizedForBackroundMainDispatch]
        public static List<ObjectComponent> BakeComponents(Component[] comps, Transform t)
        {
            //Debug.LogWarning("BAKE COMPONENTS");
            List<ObjectComponent> compcontainer = new List<ObjectComponent>();
            foreach (Component a in comps)
            {
                /*
                if (TeamProjectEditor.TeamSettings.isDebug)
                    Debug.Log("BAKE COMPONENT: " + a.name + "  " + a.GetType().ToString());
                */
                if (a.GetType() == typeof(MeshFilter) || a.GetType() == typeof(Transform) || a.GetType() == typeof(RectTransform) || a.GetType() == typeof(TeamView) || a.GetType() == typeof(TeamObject))
                    continue;

                ObjectComponent comp;
                System.Type CompType = a.GetType().BaseType;
                //UNITY'S STANDARD COMPONENTS AREN'T SERIALIZABLE
                if (CompType.ToString().StartsWith("Unity"))
                {
                    Debug.Log("Compo: " + CompType.ToString() + "T: " + a.GetType());
                    if (a.GetType() == typeof(MeshRenderer))
                    {
                        //if (TeamProjectEditor.TeamSettings.isDebug)
                        //    Debug.Log("Renderer Data: "+t.gameObject.name+"  "+Converter.ToObjectInfo(t).ToString());
                        comp = RendererPacker(a);
                    }
                    else if (a.GetType() == typeof(Renderer))
                    {
                        throw new Exception("RENDERER FOUND " + a.name + " Type: " + a.GetType());
                    }
                    else
                        comp = PackComponent(a, false);
                    //Debug.Log("----------------------------------------------------------------------------");

                    try
                    {
                        //Debug.LogWarning("Try Format " + a.GetType().BaseType.ToString() + " to Behaviour");//Behaviour Base Type needed to access field "enabled" 
                        Utility.Dispatcher.ToMainThread(() =>
                        {
                            if (CompType == typeof(Collider))
                                comp.state = (a as Collider).enabled;
                            else if (CompType == typeof(Renderer))
                                comp.state = (a as Renderer).enabled;
                            else if (CompType == typeof(Behaviour))
                                comp.state = (a as Behaviour).enabled;
                            else if (CompType == typeof(UIBehaviour))
                                comp.state = (a as UIBehaviour).enabled;
                            else
                            {
                                if (TeamProjectEditor.TeamSettings.isDebug)
                                    Debug.LogWarning("Currently non Support for " + a.GetType() + " Type");
                                comp.state = false;
                            }
                        });
                    }
                    catch (Exception e)
                    {
                        if (TeamProjectEditor.TeamSettings.isDebug)
                            Debug.LogError("NOT ACCESS MONO " + e.Data + "  " + e.StackTrace + "  " + e.Message);
                    }
                    compcontainer.Add(comp);
                }
                else //MonoBehaviour
                {
                    if (a.GetType() == typeof(MeshRenderer))
                    {
                        throw new InvalidOperationException("MESH RENDERER SERIALISATION");
                    }
                    else if (a.GetType() != typeof(TeamView) && a.GetType() != typeof(TeamObject))
                    {
                        //Debug.Log("Compo: " + a.ToString());
                        comp = PackComponent(a, true);
                        //Debug.Log("----------------------------------------------------------------------------");

                        //COMP STATE
                        MonoBehaviour cc = (MonoBehaviour)a;

                        Utility.Dispatcher.ToMainThread(() =>
                        {
                            if (cc != null)
                                comp.state = cc.enabled;
                            else
                                comp.state = true;
                        });

                        compcontainer.Add(comp);
                    }
                }
            }
            return compcontainer;
        }

        [OptimizedForBackroundMainDispatch]
        public static ObjectComponent RendererPacker(object o)
        {
            MeshRenderer mr = (MeshRenderer)o;

            if (TeamProjectEditor.TeamSettings.isDebug)
                Debug.Log("PACK RENDERER");
            ObjectComponent comp = new ObjectComponent();
            Utility.Dispatcher.ToMainThread(() =>
            {
                comp.fields.Add("sharedMaterials", Converter.ToMaterialDataList(((MeshRenderer)o).sharedMaterials));
                comp.fields.Add("shadowCastingMode", (int)((MeshRenderer)o).shadowCastingMode);
                comp.fields.Add("receiveShadows", ((MeshRenderer)o).receiveShadows);
                comp.fields.Add("lightProbeUsage", (int)((MeshRenderer)o).lightProbeUsage);
                comp.fields.Add("reflectionProbeUsage", (int)((MeshRenderer)o).reflectionProbeUsage);
                comp.state = ((MeshRenderer)o).enabled;
            });
            comp.comtype = new ObjectType(mr.GetType());
            comp.componentName = "UnityEngine.MeshRenderer";
            return comp;
        }

        [Obsolete("NOT USE")]
        public static void RendererUnpacker(ObjectComponent o, Type m, MeshRenderer t)
        {
            foreach (KeyValuePair<string, System.Object> p in o.fields)
            {
                if (p.Value != null)
                {
                    PropertyInfo info = m.GetProperty(p.Key);
                    try
                    {
                        if (p.Value.GetType() == typeof(AssetUUIDs))//REPLACEMENT ASSETUUID
                        {
                            Debug.Log("MaterialList " + ((AssetUUIDs)p.Value).uids.Count);
                            Material[] d = Converter.ToMaterialList((AssetUUIDs)p.Value);
                            Debug.Log("MaterialList Set Value " + d.Length);
                            info.SetValue(t, d, null);
                            Debug.Log("MaterialList Set End");
                        }
                        /*else if(p.Value.GetType() == typeof(AssetUUID))
                        {
                            object oo = TeamAssetSystem.TeamAssetCore.GetAsset((AssetUUID)p.Value);
                            if (oo != null)
                            {
                                if (oo.GetType() == typeof(List<MaterialData>))
                                {
                                    Material[] d = Converter.ToMaterialList((List<MaterialData>)oo);
                                    Debug.Log("MaterialList " + d.Length);
                                    info.SetValue(t, d, null);
                                    Debug.Log("MaterialList Set End");
                                }
                            }
                        }*/
                        else
                        {
                            //Debug.LogWarning("Set Value");
                            //info.SetValue(t, Convert.ChangeType(p.Value, info.PropertyType), null);
                            if (info.PropertyType.BaseType == typeof(Enum))
                            {
                                Debug.LogError("Try To Format to Enum " + info.PropertyType.ToString()+"  "+ Enum.ToObject(info.PropertyType, p.Value).GetType());
                                info.SetValue(t, Enum.ToObject(info.PropertyType, p.Value), null);
                            }
                            else
                                info.SetValue(t, p.Value, null);
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("VALUE SET ERROR " + e.Message);
                    }
                }
            }
        }

        [OptimizedForBackroundMainDispatch]
        public static void UnBakeComponents(GameObject obj, List<ObjectComponent> comps)
        {
            foreach (ObjectComponent a in comps)
            {
                if (TeamProjectEditor.TeamSettings.isDebug)
                    Debug.Log("OBJECT COMPONENT " + a.componentName);
                if (!a.isMono)
                {
                    Debug.LogWarning("MONO");
                    /*
                    try
                    {
                        Debug.LogError("COMTYPE == NULL  " + (a.comtype == null?"TRUE": "FALSE"));
                        Debug.LogError("COMTYPE _type == EMTPY  " + (a.comtype._type == string.Empty?"TRUE":"FALSE"));

                        Debug.LogError("COMTYPE _type  " + a.comtype._type);

                        Debug.LogError("COMTYPE TYPE == NULL  " + (a.comtype.type == null ? "TRUE" : "FALSE"));
                        Debug.LogError("COMTYPE TYPE " + Type.GetType(a.comtype._type) == null ? "TRUE" : "FAlse");

                        Debug.LogError("COMTYPE REFLECT TYPE " + ReflectDllMethods.GetType(a.comtype._type));

                        Debug.LogError("COMTYPE TYPE " + Type.GetType(a.comtype._type).Name);
                        

                        if (a.comtype.type == null)
                            Debug.LogError("COMTYPE == NULL  " + a.comtype._type + "  GetTYPE " + Type.GetType(a.comtype._type).FullName);
                        else
                        if (TeamProjectEditor.TeamSettings.isDebug)
                            Debug.Log("CompType:" + a.comtype._type);
                    }
                    catch(Exception e)
                    {
                        Debug.LogError("ERROR UnBakeComponents " + e.Message +"\n" + e.StackTrace);

                    }*/

                    Type componentType = null;
                    ///try
                    //{
                    componentType = a.comtype.type;//ReflectDllMethods.GetType(a.comtype._type);//
                                                   //}
                                                   //catch (Exception e)
                                                   //{
                                                   // Debug.LogError("ERROR DATAVCONVERTER: " + e.Message + "\n" + e.StackTrace);
                                                   //}
                                                   //Type.GetType(a.componentName);
                    if (TeamProjectEditor.TeamSettings.isDebug)
                        Debug.Log("TYPE " + componentType.FullName + "  " + (componentType == null ? "NULL" : "NOTNULL"));
                    /*
                    if (componentType == null && a.comtype.type != null)
                    {
                        Debug.Log("NOT NULL");
                        componentType = a.comtype.type;
                    }*/
                    if (componentType == null)
                    {
                        TeamServerConnection.Disconnect();
                        throw new System.Exception("Could not Unbaked " + a.componentName + " 'cause cant find Sytem.Type");
                    }

                    Debug.Log("Next Check");

                    if (componentType == null)
                        throw new ArgumentNullException("TYPE " + a.componentName + " couldn't be found / Type is Null");

                    Debug.Log("Add Component " + componentType.FullName);

                    Component t = null;
                    Utility.Dispatcher.ToMainThread(() =>
                    {
                        t = obj.GetComponent(componentType);
                        //ADD COMPONENT IF NOT EXISTS
                        if (t == null)
                            t = obj.AddComponent(componentType);


                        // componentType = t.GetType();

                        //if (componentType == typeof(MeshRenderer))
                        //    RendererUnpacker(a, componentType, (MeshRenderer) t);
                        Debug.Log("Set Fields");

                        //SET VALUES
                        foreach (KeyValuePair<string, System.Object> p in a.fields)
                        {
                            if (p.Value != null)
                            {
                                Debug.Log("Try Get Prop");
                                PropertyInfo info = componentType.GetProperty(p.Key);
                                if (info != null)
                                {
                                    try
                                    {
                                        if (p.Value.GetType() == typeof(AssetUUIDs))
                                        {
                                            if (TeamProjectEditor.TeamSettings.isDebug)
                                                Debug.Log("MaterialList " + ((AssetUUIDs)p.Value).uids.Count);
                                            Material[] d = Converter.ToMaterialList((AssetUUIDs)p.Value);
                                            if (TeamProjectEditor.TeamSettings.isDebug)
                                                Debug.Log("MaterialList Set Value " + d.Length);
                                            info.SetValue(t, d, null);
                                            if (TeamProjectEditor.TeamSettings.isDebug)
                                                Debug.Log("MaterialList Set End");
                                        }
                                        else if (p.Value.GetType() == typeof(MaterialData))
                                        {
                                            Debug.LogError("MATERIAL DATA INCOMMING IN UNBACKECOMPONENTS - OBSOLTED");
                                            Material m = Converter.ToMaterial((MaterialData)p.Value);
                                            info.SetValue(t, m, null);
                                        }
                                        else
                                        {
                                            if (info.PropertyType.BaseType == typeof(Enum))
                                                info.SetValue(t, Enum.ToObject(info.PropertyType, p.Value), null);
                                            else
                                                info.SetValue(t, p.Value, null);
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        if (TeamProjectEditor.TeamSettings.isDebug)
                                            Debug.LogError("VALUE SET ERROR " + e.Message);
                                    }
                                }
                                else
                                   if (TeamProjectEditor.TeamSettings.isDebug)
                                    Debug.LogWarning("FIELD " + p.Key + " does not exist");
                            }
                        }


                        //SET STATE
                        if (componentType == typeof(MeshRenderer))
                            (t as MeshRenderer).enabled = a.state;
                        else if (componentType == typeof(Collider))
                            (t as Collider).enabled = a.state;
                        else if (componentType == typeof(Renderer))
                            (t as Renderer).enabled = a.state;
                        else if (componentType == typeof(Behaviour))
                            (t as Behaviour).enabled = a.state;
                    });
                }
                else
                {
                    if (TeamProjectEditor.TeamSettings.isDebug)
                        Debug.Log("MONOBEHAIVUR");
                    UnpackComponent(ref obj, a);
                }
            }
        }

        [RunOnBackground]
        public static ObjectComponent PackComponent(object component, bool ismono)
        {
            //Debug.Log("COMP: " + component + "  " + component.GetType().ToString());
            ObjectComponent newObjectComponent = new ObjectComponent();
            newObjectComponent.fields = new Dictionary<string, object>();

            BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetField;//BindingFlags.NonPublic |

            var componentType = component.GetType();
            newObjectComponent.componentName = componentType.ToString();
            newObjectComponent.comtype = new ObjectType(componentType);

            if (ismono)
            {
                //Debug.Log("MONOSCRIPT");
                FieldInfo[] fields = componentType.GetFields(flags);
                foreach (FieldInfo field in fields)
                {
                    if (TeamProjectEditor.TeamSettings.isDebug)
                        Debug.Log("FIELD " + field.Name + "   " + field.GetType());
                    if (field != null)
                    {
                        if (field.FieldType.IsSerializable == false)
                        {
                            //Debug.Log(field.Name + " (Type: " + field.FieldType + ") is not marked as serializable. Continue loop.");
                            continue;
                        }
                        if (TypeSystem.IsEnumerableType(field.FieldType) == true || TypeSystem.IsCollectionType(field.FieldType) == true)
                        {
                            Type elementType = TypeSystem.GetElementType(field.FieldType);
                            //Debug.Log(field.Name + " -> " + elementType);

                            if (elementType.IsSerializable == false)
                            {
                                continue;
                            }
                        }

                        Type valueType;
                        bool isnull = ((field.GetValue(component) != null) ? true : false);
                        if (field.GetValue(component) != null)
                        {
                            valueType = field.GetValue(component).GetType();
                            if (valueType == typeof(MonoBehaviour))
                            {
                                if (TeamProjectEditor.TeamSettings.isDebug)
                                    Debug.LogWarning("FIELD COULD NOT SAVED " + field.Name);

                                newObjectComponent.fields.Add(field.Name, new MonoReferenze());
                            }
                            else if (valueType == typeof(Material))
                            {
                                if (TeamProjectEditor.TeamSettings.isDebug)
                                    Debug.Log("MATERIAL FIELD");
                                newObjectComponent.fields.Add(field.Name, isnull ? null : Converter.ToMaterialData((Material)field.GetValue(component)));
                                if (TeamProjectEditor.TeamSettings.isDebug)
                                    Debug.Log("MATERIAL FIELD ADDED");
                            }
                            else if (valueType == typeof(Font))
                            {
                                if (TeamProjectEditor.TeamSettings.isDebug)
                                    Debug.LogWarning("FONTFIELD " + field.Name + " could not serialized");
                                //currently no serialization
                            }
                            else if (valueType == typeof(Sprite))
                            {
                                if (TeamProjectEditor.TeamSettings.isDebug)
                                    Debug.LogWarning("FONTFIELD " + field.Name + " could not serialized");
                                //currently no serialization for sprite
                            }
                            else if (valueType == typeof(int) || valueType == typeof(string) || valueType == typeof(float) || valueType == typeof(bool) || valueType == typeof(object))
                            {
                                //Debug.LogWarning("Field VALUE " + field.Name + " " + field.GetValue(component).GetType().ToString());
                                var val = field.GetValue(component);//(UnityEngine.Object)
                                if (val != null)
                                    newObjectComponent.fields.Add(field.Name, isnull ? null : val);
                            }
                        }
                    }
                }
                return newObjectComponent;
            }
            else//FOR non Serilizable Scripts (UnityEngine.dll Methods and Fields doesn't support Serialization)
            {
                Utility.Dispatcher.ToMainThread(() =>
                {
                    //Debug.Log("NON MONOSCRIPT");
                    PropertyInfo[] props = componentType.GetProperties();
                    //Debug.Log("Prop Count: " + props.Count());
                    int c = 0;
                    foreach (PropertyInfo prop in props)
                    {
                        object[] attributes = prop.GetCustomAttributes(true);
                        bool access = true;
                        foreach (object t in attributes)
                        {
                            //Debug.Log(prop.Name+"                             Attribute:" + t.GetType());
                            if (t.GetType() == typeof(System.ObsoleteAttribute))
                            {
                                access = false;
                                break;
                            }
                        }

                        if (access)
                        {
                            if (!prop.CanRead || !prop.CanWrite)
                            {
                                //Debug.Log(prop.PropertyType.Name + "  " + prop.Name);
                                //Debug.LogError("CANNOT" + (prop.CanRead ? "" : "READ") + (prop.CanWrite ? "" : "/ WRITE"));
                                continue;
                            }
                            else if (prop.GetSetMethod(true).IsPublic)
                            {
                                object value = prop.GetValue(component, null);

                                //Debug.Log("Property " + prop.Name + "\t"+ "Type: " + (value!=null?value.GetType().ToString()+"\t"+"BASE:"+value.GetType().BaseType+"\t"+ "BASE:" + value.GetType().BaseType.BaseType: "NULL") + "\t" + prop.DeclaringType);
                                if (value != null)
                                {
                                    Type valueType = value.GetType();
                                    if (valueType == typeof(Material))
                                    {
                                        newObjectComponent.fields.Add(prop.Name, Converter.ToMaterialData((Material)value));
                                    }
                                    else if (valueType == typeof(Font))
                                    {

                                    }
                                    else if (valueType.BaseType.BaseType == typeof(UnityEngine.Events.UnityEventBase))
                                    {
                                        Debug.LogError("NOT SRIALIZABLE EVENT Type " + value.GetType());
                                        //Debug.LogWarning("UNITY EVENT FIELD");
                                    }
                                    else if (valueType == typeof(Sprite))
                                    {
                                        Debug.LogError("Not Serializable Value : Sprite");
                                    }
                                    else
                                    {
                                        //Debug.LogWarning("Fieldname: " + prop.Name + " Fieldtype: " + prop.PropertyType.Name + " Value: " + value);
                                        newObjectComponent.fields.Add(prop.Name, (object)value);
                                    }
                                }
                                else
                                {
                                    newObjectComponent.fields.Add(prop.Name, null);
                                }
                            }
                            else
                            {
                                object value = prop.GetValue(component, null);
                                Debug.LogError("Property " + prop.Name + "  Type: " + (value != null ? value.GetType().ToString() : "NULL") + "  " + prop.DeclaringType);
                            }
                        }
                        c++;
                    }
                    newObjectComponent.isMono = false;
                    Debug.Log("Set ComponentType");
                    newObjectComponent.comtype = new ObjectType(componentType);
                });
                return newObjectComponent;
            }
        }

        [OptimizedForBackroundMainDispatch]
        public static void CheckComponent(object component, bool ismono, DependencyCheckList list)
        {
            //Debug.Log("     Check Component " + (component as Component).GetType().ToString());
            //Debug.Log("COMP: " + component + "  " + component.GetType().ToString());
            BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;//BindingFlags.NonPublic |

            System.Type componentType = component.GetType();
            if (ismono)
            {
                //Debug.Log("Monobehaivoure");
                FieldInfo[] fields = componentType.GetFields(flags);
                //Debug.LogWarning("FIELDs " + fields.Length);

                foreach (FieldInfo field in fields)
                {
                    if (field != null)
                    {
                        if (field.FieldType.IsSubclassOf(typeof(UnityEngine.Object)))
                        {
                            //Debug.LogWarning("FieldType: " + field.FieldType);
                            object value = field.GetValue(component);
                            bool isnull = (value == null ? true : false);
                            Transform tr = null;
                            int id;
                            Type valueType;
                            if (!isnull)
                            {
                                valueType = value.GetType();
                                //Debug.Log("     VALUETYPE:" + value.ToString());
                                if (valueType.IsSubclassOf(typeof(Component)) || valueType == typeof(GameObject))
                                {
                                    Utility.Dispatcher.ToMainThread(() =>
                                    {
                                        tr = ((Component)value).transform.root;//Root only cause TeamView will be registered on server
                                    });

                                    id = ReflectDllMethods.GetInstanceID(tr);
                                    if (!list.HasItem(id))
                                    {
                                        list.Add(id, tr);
                                        Debug.Log("         Add New Reference " + tr.name);
                                    }
                                }
                                else //Material/Texture/Shader
                                {
                                    Debug.Log("Register Local Asset " + value.GetType().ToString());
                                    TeamAssetCore.ChecknRegisterAsset((UnityEngine.Object)value);
                                }
                            }
                            else
                            {
                                Debug.Log("NULL VALUE: " + field.Name);
                            }
                        }
                    }
                    else
                        Debug.LogError("FIELD NULL");
                }
            }
            else//FOR non Serializable Scripts (UnityEngine.dll Methods and Fields doesn't support Serialization)
            {
                //Debug.Log("Non Monobehaivoure");
                PropertyInfo[] props = componentType.GetProperties(flags);
                Type valueType;
                int c = 0;
                Transform tr = null;
                int id;

                foreach (PropertyInfo prop in props)
                {
                    object[] attributes = prop.GetCustomAttributes(true);
                    bool access = true;
                    foreach (object t in attributes)
                        if (t.GetType() == typeof(System.ObsoleteAttribute))
                        {
                            access = false;
                            break;
                        }

                    if (access)
                    {
                        if (!prop.CanRead || !prop.CanWrite)
                            continue;
                        else if (prop.GetSetMethod(true).IsPublic)
                        {
                            object value = prop.GetValue(component, null);
                            if (value != null)
                            {
                                valueType = value.GetType();
                                if (valueType == typeof(UnityEngine.Object))
                                {
                                    if (valueType == typeof(Component))
                                    {
                                        Utility.Dispatcher.ToMainThread(() =>
                                        {
                                            tr = ((Component)value).transform.root;//Root only cause TeamView will be registered on server
                                        });

                                        id = ReflectDllMethods.GetInstanceID(tr);
                                        if (!list.HasItem(id))
                                        {
                                            list.Add(id, tr);
                                            //Debug.Log("         Add New Reference " + tr.name);
                                        }
                                    }
                                    else //Material/Texture/Shader
                                    {
                                        Debug.Log("Register Local Asset " + value.GetType().ToString());
                                        TeamAssetCore.ChecknRegisterAsset((UnityEngine.Object)value);
                                    }
                                }
                                else
                                    continue;
                            }
                        }
                        else
                        {
                            object value = prop.GetValue(component, null);
                            Debug.LogError("Property " + prop.Name + "  Type: " + (value != null ? value.GetType().ToString() : "NULL") + "  " + prop.DeclaringType);
                        }
                    }
                    c++;
                }
            }
        }

        [OptimizedForBackroundMainDispatch]
        public static void UnpackComponent(ref GameObject gos, ObjectComponent obc)
        {
            //Go through the stored object's component list and reassign all values in each component, and add components that are missing			
            Type componentType = Type.GetType(obc.componentName);
            if (TeamProjectEditor.TeamSettings.isDebug)
                Debug.Log("Component " + obc.componentName + " Type" + componentType + "  " + (componentType == null ? "Null" : "NotNull"));
            componentType = obc.comtype.type;

            object obj = null;
            GameObject go = gos;
            Utility.Dispatcher.ToMainThread(() =>
            {
                if (go.GetComponent(obc.componentName) == null)
                    obj = go.AddComponent(componentType);
                else
                    //Debug.Log("ADD COMP: " + obc.componentName + "     " + componentType);
                    obj = go.GetComponent(componentType) as object;
            });

            var tp = obj.GetType();
            foreach (KeyValuePair<string, object> p in obc.fields)
            {
                var fld = tp.GetField(p.Key,
                                      BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic |
                                      BindingFlags.SetField);
                if (fld != null)
                {
                    object value = p.Value;
                    if (value.GetType() == typeof(MaterialData))
                    {
                        if ((MaterialData)value != null)
                            fld.SetValue(obj, Converter.ToMaterial((MaterialData)value));
                    }
                    else
                        fld.SetValue(obj, value);
                }
            }
        }

        [OptimizedForBackroundMainDispatch]
        public static Dictionary<string, object> PackMaterial(Material m) {
            Dictionary<string, object> matdata = new Dictionary<string, object>();

            Utility.Dispatcher.ToMainThread(() =>
            {
                int count = ShaderUtil.GetPropertyCount(m.shader);

                for (int a = 0; a < count; a++)
                {
                    string name = ShaderUtil.GetPropertyName(m.shader, a);
                    ShaderUtil.ShaderPropertyType type = ShaderUtil.GetPropertyType(m.shader, a);

                    //Debug.Log(name+" " + type);

                    switch (type)
                    {
                        case ShaderUtil.ShaderPropertyType.Vector:
                            matdata.Add(name, (Vector4)m.GetVector(name));
                            break;
                        case ShaderUtil.ShaderPropertyType.Color:
                            matdata.Add(name, (Color)m.GetColor(name));
                            break;
                        case ShaderUtil.ShaderPropertyType.Float:
                            matdata.Add(name, (float)m.GetFloat(name));
                            break;
                        case ShaderUtil.ShaderPropertyType.TexEnv:
                            if (m.GetTexture(name) != null)
                                matdata.Add(name, TeamAssetSystem.TeamAssetCore.GetAssetUUID(m.GetTexture(name)));
                            //matdata.Add(name, (AssetUUID)new AssetUUID(m.GetTexture(name)));
                            break;
                    }
                }
            });

            //Debug.Log("All Properties found");
            return matdata;
        }

        [OptimizedForBackroundMainDispatch]
        public static Material UnpackMaterial(Dictionary<string, object> data, Material mat) {
            System.Type type;
            Utility.Dispatcher.ToMainThread(() =>
            {
                foreach (KeyValuePair<string, object> a in data)
                {
                    if (a.Value != null)
                    {
                        type = a.Value.GetType();
                        if (type == typeof(Vector4))
                            mat.SetVector(a.Key, (Vector4)a.Value);
                        else if (type == typeof(Color))
                            mat.SetColor(a.Key, (Color)a.Value);
                        else if (type == typeof(float))
                            mat.SetFloat(a.Key, (float)a.Value);
                        else if (type == typeof(AssetUUID))
                            mat.SetTexture(a.Key, TeamAssetSystem.TeamAssetCore.GetAsset<Texture2D>(((AssetUUID)a.Value).ServerID, ShareAssetType.Texture, ((AssetUUID)a.Value)));
                    }
                    else
                    {
                        //Debug.LogError ("EMPTY FIELD");
                    }
                }
            });
            return mat;
        }
    }

    internal static class TypeSystem
    {
        internal static Type GetElementType(Type seqType)
        {
            Type ienum = FindIEnumerable(seqType);
            if (ienum == null) return seqType;
            return ienum.GetGenericArguments()[0];
        }
        private static Type FindIEnumerable(Type seqType)
        {
            if (seqType == null || seqType == typeof(string))
                return null;
            if (seqType.IsArray)
                return typeof(IEnumerable<>).MakeGenericType(seqType.GetElementType());
            if (seqType.IsGenericType)
            {
                foreach (Type arg in seqType.GetGenericArguments())
                {
                    Type ienum = typeof(IEnumerable<>).MakeGenericType(arg);
                    if (ienum.IsAssignableFrom(seqType))
                    {
                        return ienum;
                    }
                }
            }
            Type[] ifaces = seqType.GetInterfaces();
            if (ifaces != null && ifaces.Length > 0)
            {
                foreach (Type iface in ifaces)
                {
                    Type ienum = FindIEnumerable(iface);
                    if (ienum != null) return ienum;
                }
            }
            if (seqType.BaseType != null && seqType.BaseType != typeof(object))
            {
                return FindIEnumerable(seqType.BaseType);
            }
            return null;
        }
        //is a type a collection?
        public static bool IsEnumerableType(Type type)
        {
            return (type.GetInterface("IEnumerable") != null);
        }
        public static bool IsCollectionType(Type type)
        {
            return (type.GetInterface("ICollection") != null);
        }
    }
}