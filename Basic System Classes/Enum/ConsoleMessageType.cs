﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MassiveStudio.TeamProjectEditor
{
    public enum ConsoleMessageType : int
    {
        System_Error = 0,
        System_Warn = 1,
        Sytem_Log = 2,
        Team = 3
    };
}
