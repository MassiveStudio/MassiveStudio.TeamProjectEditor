﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MassiveStudio.TeamProjectEditor
{
    public enum AssetSearchOption : int
    {
        UUID = 0,
        ServerID = 1,
        LocalPath = 2
    }
}
