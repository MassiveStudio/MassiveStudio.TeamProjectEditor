﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MassiveStudio.TeamProjectEditor
{
    public enum MonoEvent
    {
        Created = 0,
        Changed = 1,
        Deleted = 2,
        Renamed = 3
    };
}
