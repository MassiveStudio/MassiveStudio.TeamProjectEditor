﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MassiveStudio.TeamProjectEditor
{
    [System.Serializable]
    public enum ShareAssetType : byte
    {
        Material=0,
        Script=1,
        Shader=2,
        Texture=3,
        GUISkin=4,
        None =255
    }
}
