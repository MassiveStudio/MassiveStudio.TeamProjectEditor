﻿using System;
using System.Collections.Generic;

namespace MassiveStudio.TeamProjectEditor
{
    [Serializable]
    public class DependencyCheckList
    {
        public Dictionary<int, object> checklist;

        public DependencyCheckList()
        {
            this.checklist = new Dictionary<int, object>();
        }

        public bool HasItem(int id)
        {
            return checklist.ContainsKey(id);
        }

        public void Add(int id, object obj)
        {
            checklist.Add(id, obj);
        }
    }
}
