﻿using System.Security.Cryptography;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using UnityEditor;
using UnityEngine;
using System.IO;
using System;

using MassiveStudio.TeamProjectEditor.SystemUtility;
using MassiveStudio.TeamProjectEditor.Attributes;
using MassiveStudio.TeamProjectEditor;

namespace MassiveStudio.TeamProjectEditor.TeamAssetSystem
{
    //[InitializeOnLoad] -> TeamServerConnection will start Init() after successfull connect
    public static class TeamAssetCore
    {
        public static TeamAssets TeamAssetList;

        public static List<ModelContainer> SceneAssets;

        private static AssetUUID _tempUUID;

        public static MD5 md5;

        public static void InitDelegate()
        {
            ClientRequest request;/* = new ClientRequest(RequestType.TeamSceneChange, 0);//StartScene 0
            TeamServerConnection.dataToWrite.Add(request);*/

            request = new ClientRequest(RequestType.TeamSceneListUpdate, null);
            TeamServerConnection.dataToWrite.Add(request);

            EditorEventSystem.OnDeserilization -= OnDestroy;
            EditorEventSystem.OnDeserilization += OnDestroy;

            //EditorApplication.update -= Update;
            //EditorApplication.update += Update;

            TeamSceneManager.Init();

            Init();
        }

        public static void Init()
        {
            Console.TeamConsole.Message("TEAM ASSET CODE INIT", "TeamAssetCore", Color.white, true, false);
            //Debug.Log("TEAM ASSET CODE INIT");
            Console.TeamConsole.Message("Load AssetDatabase", "TeamAssetCore", Color.white, true, false);
            //Debug.Log("Load AssetDatabase");
            Load(0);//->Main Scene/Lobby

            if (md5 == null)
                md5 = MD5.Create();
        }
        
        [Obsolete]
        public static void Update()
        {
            //LATER
           /* try
            {
                if (TeamAssetSystem.TeamAssetCore.AssetList != null && AssetList.Count > 0)
                    foreach (KeyValuePair<UnityEngine.Object, ShareAsset> val in AssetList)
                        if (val.Value !=null && val.Value.obj!=null)
                            CheckDirtyState(val.Value.obj);
            }
            catch (System.Exception e)
            {
            Debug.LogError("ERROR DIRTY UPDATER CHECKER  " + e.Message + "\n" + e.StackTrace);
            }*/
        }

        #region Asset / Scene Version Check
        [RunOnBackground]
        public static void CheckAssetVersions()
        {
            Console.TeamConsole.Message("Create AssetVersionCheck", "TeamAssetCore", Color.white, true, false);
            //Debug.Log("Create AssetVersionCheck");

            if (TeamAssetList == null)
                TeamAssetList = TeamAssetDatabase.Load();

            ServerAssetCheckList list = new ServerAssetCheckList(TeamServerConnection.currentServer.currentScene);

            for (int c = 0; c < 3; c++)
            {
                Console.TeamConsole.Message("LIST CHECK: " + (ShareAssetType)c, "TeamAssetCore", Color.white, true, false);
                //Debug.Log("LIST CHECK: " + (ShareAssetType)c);
                list.assetlists.Add(new ServerAssetCheckListElement((ShareAssetType)c));

                foreach (ShareAsset asset in TeamAssetList.GetList((ShareAssetType)c))
                {
                    if (asset.uuid.ServerID != string.Empty)
                        list.assetlists[c].AddAsset(asset.uuid);
                    else
                    {
                        //Debug.LogWarning("Asset: " + ((ShareAssetType)c) + " Located at " + asset.localpath + " isn't registered / hasn't got a Server ID");
                        Console.TeamConsole.Message("Asset: " + ((ShareAssetType)c) + " Located at " + asset.localpath + " isn't registered / hasn't got a Server ID", "TeamAssetCore", Color.yellow, true, false);
                    }
                }
                list.Sort();
            }
            //TeamServerConnection.AddToWriterList(list);
            Console.TeamConsole.Message("AssetVersionCheck send to writer", "TeamAssetCore", Color.white, true, false);
            //Debug.Log("AssetVersionCheck send to writer");
            TeamServerConnection.DirectWrite(TeamServerConnection.ServerStream.Main, list);
        }

        [RunOnBackground]
        public static void CheckSceneAssetVersions()
        {
            Console.TeamConsole.Message("Create SceneAssetVersionCheck", "TeamAssetCore", Color.yellow, true, false);
            //Debug.LogWarning("Create SceneAssetVersionCheck");

            if (SceneAssets == null || TeamSceneManager.loadedScene != TeamServerConnection.currentServer.currentScene)
                SceneAssets = TeamAssetDatabase.LoadSceneAssets(TeamServerConnection.currentServer.currentScene);

            SceneCheckList list = new SceneCheckList(TeamServerConnection.currentServer.currentScene);
            foreach(ModelContainer mc in SceneAssets)
                if(mc!=null)
                    list.sceneObjects.Add(new SceneCheckListItem(mc));
            Console.TeamConsole.Message("SceneAssetVersionCheck send to writer", "TeamAssetCore", Color.white, true, false);
            //Debug.Log("SceneAssetVersionCheck send to writer");
            TeamServerConnection.DirectWrite(TeamServerConnection.ServerStream.Main, list);
        }
        #endregion

        #region [REWRITTEN/CHECKED]
        [RunOnBackground]
        public static void UpdateAssetDatabase(AssetUUID uuid, ShareAssetType type, UnityEngine.Object obj)
        {
            if(TeamAssetList==null)
                TeamAssetList = TeamAssetDatabase.Load();

            ShareAsset asset = CheckExists(uuid.ServerID, type, AssetSearchOption.ServerID);
            if (asset != null)
            {
                asset.uuid = uuid;
                asset.obj = obj;
            }
            else
                AddAssetLocal(uuid, obj);
        }

        internal static void RegisterAsset_Global(UnityEngine.Object obj, AssetUUID uuid)
        {
            uuid.Creator = TeamServerConnection.currentServer.user.UserID;
            ClientRequest rr = null;
            switch (uuid.type)
            {
                case ShareAssetType.Material:
                    rr = LibaryClasses.CreateMaterialRegisterRequest((Material)obj);
                    break;
                case ShareAssetType.Script:
                    //MonoScriptUpdateRequest msur = LibaryClasses.CreateMonoScriptUpdateRequest()
                    break;
                case ShareAssetType.Texture:
                    rr = LibaryClasses.CreateTextureRegisterRequest((Texture2D)obj);
                    break;
                case ShareAssetType.Shader:
                    rr = LibaryClasses.CreateShaderRegisterRequest((Shader)obj);
                    break;
            }
            if (rr != null)
                TeamServerConnection.AddToWriterList(rr);
        }

        public static T GetAsset<T>(string objectID, ShareAssetType type, AssetUUID uid = null, bool CreateNewIfNull = true)
        {
            Debug.Log("Get Asset");
            ShareAsset uuid;
            if (objectID.Contains("_"))
                uuid = CheckExists(objectID, type, AssetSearchOption.UUID);
            else
                uuid = CheckExists(objectID, type, AssetSearchOption.ServerID);

            if (!CreateNewIfNull)
                if (uuid != null)
                    return (T)(object)uuid.obj;
                else
                    return (T)(object)null;

            if (uuid == null)
            {
                Console.TeamConsole.Message("Asset Not Found", "TeamAssetCore", Color.white, true, false);
                //Debug.Log("Asset Not Found");
                UnityEngine.Object obj = CreateDefault(type);
                Console.TeamConsole.Message("Register New Asset", "TeamAssetCore", Color.white, true, false);
                //Debug.Log("Register New Asset");

                uuid = CreateNewShareAsset(obj as UnityEngine.Object, new AssetUUID(obj as UnityEngine.Object, objectID), true);

                RegisterNewAsset_Local(uuid, false);//Temporary Asset -> Replaced with ServerAsset after Loading
                // obj as UnityEngine.Object, new AssetUUID(obj as UnityEngine.Object, serverID));
                return (T)(object)obj;
            }
            else
            {
                Console.TeamConsole.Message("Asset Found in Database", "TeamAssetCore", Color.white, true, false);
                //Debug.Log("Asset Found in Database");
                if (!uuid.HasObject())
                {
                    Console.TeamConsole.Message("Object Field Empty", "TeamAssetCore", Color.white, true, false);
                    //Debug.Log("Object Field Empty");
                    UnityEngine.Object obj = CreateDefault(type);
                    uuid.obj = obj as UnityEngine.Object;
                    return (T)(object)obj;
                }

                return (T)(object)uuid.obj;
            }
        }

        #endregion
        [RunOnBackground]
        public static bool CheckDirtyState(UnityEngine.Object obj)
        {
            try
            {
               //Debug.LogFormat("CheckState");
                System.Type type = typeof(EditorUtility);
                MethodInfo methodInfo = type.GetMethod("IsDirty", BindingFlags.Static | BindingFlags.NonPublic); // Get the method IsDirty  
                                                                                                                 //UnityEngine.Object obj = AssetDatabase.LoadAssetAtPath("Assets/Prefab.prefab", typeof(UnityEngine.Object)); // Get the Prefab Object from the assets
                int instanceID = ReflectDllMethods.GetInstanceID(obj);//.GetInstanceID(); // Get the Prefab Object's instance ID;
                return (bool)methodInfo.Invoke(obj, new System.Object[1] { instanceID }); // Execute the referenced method
                //bool isDirty =
                /*if (isDirty)
                    Debug.Log(obj.name + "  Is dirty"); // States true when prefab is modified
                else
                    Debug.Log(obj.name + "  is Not Dirty");*/
            }
            catch(System.Exception e)
            {
                throw new System.Exception("ERROR "+e.Message + "\n" + e.StackTrace);
            }
        }

        public static void AddAssetLocal(AssetUUID uuid, UnityEngine.Object obj)
        {
            ShareAsset asset = CreateNewShareAsset(obj, uuid, false);

            int id = int.Parse(uuid.ServerID);

            int Count = TeamAssetList.GetList(asset.uuid.type).Count;
            if (Count == id)
                TeamAssetList.GetList(asset.uuid.type).Add(asset);
            else
            {
                if (Count > id)
                    TeamAssetList.GetList(asset.uuid.type)[id] = asset;
                else
                {
                    while (Count <= id)
                    {
                        TeamAssetList.GetList(asset.uuid.type).Add(ShareAsset.empty);
                        Count++;
                    }
                }
            }
        }

        public static object UpdateAssetLocal(UnityEngine.Object obj, AssetUUID uuid)
        {
            if (TeamAssetSystem.TeamAssetCore.TeamAssetList == null)
                TeamAssetList = TeamAssetDatabase.Load();

            ShareAsset asset = null;

            if (uuid.ServerID != string.Empty)
                asset = CheckExists(uuid.ServerID, uuid.type, AssetSearchOption.ServerID);
            else
                asset = CheckExists(uuid.UUID, uuid.type);

            if (asset != null)
            {
                Debug.Log("Object ID Found in Database");
                if (asset.obj != null)
                {
                    Debug.Log("Return Object from Database");
                    return asset.obj;
                }
                else
                {
                    Debug.Log("Object in Database is Empty -> New Object created and saved in Database");
                    asset.obj = obj;
                    Debug.Log("Return Object from Database");
                    return asset.obj;
                }
            }
            else
            {
                ShareAsset tempasset = null;
                //ReCheckLocalFiles
                if (uuid.UUID != string.Empty)
                {
                    UnityEngine.Object o = CheckFileExistsAndLoadItIntoDatabase(uuid);
                    if (o != null)
                    {
                        obj = o;
                        Debug.Log("REPLACED OBJ BY LOCAL OBJ");
                        goto V2;
                    }
                    else
                        goto V1;
                }
                else
                    goto V1;

                V1:
                Debug.Log("Object ID Not Found in Database");
                tempasset = CreateNewShareAsset(obj, uuid, true);
                TeamAssetList.Add(obj, tempasset);
                Debug.Log("New ShareAsset Created and Saved to Database");
                Debug.Log("Return new Object");
                return obj;

                V2:
                Debug.Log("Object ID Not Found in Database");
                tempasset = CreateNewShareAsset(obj, uuid, false);
                TeamAssetList.Add(obj, tempasset);
                Debug.Log("New ShareAsset Created and Saved to Database");
                return obj;
            }
        }

        public static void RegisterNewAsset_Local(ShareAsset asset, bool RegisterOnline = true)
        {
            Console.TeamConsole.Message("RegisterNewAsset_Local", "TeamAssetCore", Color.yellow, true, false);


            if (TeamAssetSystem.TeamAssetCore.TeamAssetList == null)
                TeamAssetList = TeamAssetDatabase.Load();

            TeamAssetList.Add(asset.obj, asset);

            if (RegisterOnline)
            {
                if (asset.HasToBeRegisteredOnline() && asset.uuid.ServerID.Length == 0)
                {
                    Console.TeamConsole.Message("REGISTER ONLINE", "TeamAssetCore", Color.yellow, true, false);
                    //Debug.LogWarning("REGISTER ONLINE");
                    RegisterAsset_Global(asset.obj, asset.uuid);
                }
                else
                {
                    Console.TeamConsole.Message("OBJECT "+ asset.obj.name+" DONT NEED TO BE REGISTERED ONLINE", "TeamAssetCore", Color.yellow, true, false);
                }
            }
            //return asset.obj;
        }

        /*
        [Obsolete]
        public static object RegisterAsset_Local(UnityEngine.Object obj, AssetUUID uuid)
        {
            if (TeamAssetSystem.TeamAssetCore.AssetList == null)
                Init();

            ShareAsset asset = CheckExists(uuid);
            Debug.Log("UUID Loaded : " + (uuid == null ? "FALSE" : "TRUE"));
            Debug.Log("ASSET UUID Loaded : " + ((asset != null && asset.uuid != null) ? "TRUE" + asset.uuid.ToString() : "FALSE"));

            if (asset != null)
            {
                Debug.Log("SHAREDASSET NOT NULL");
                if (asset.obj != null)
                    return asset.obj;
                else
                {
                    Debug.Log("SHARED ASSET OBJ NULL");
                    asset.LoadObject();
                    return asset.obj;
                }
            }
            else
            {
                Debug.Log("SHARED ASSET NULL");
                ShareAsset tempasset = CreateNewShareAsset(obj, uuid);

                if (uuid.ServerID.Length == 0)//is from local
                    RegisterAsset_Global(obj, uuid);
                AssetList.Add(obj, tempasset);
                return obj;
            }
        }*/

        public static ShareAsset CreateNewShareAsset(UnityEngine.Object obj, AssetUUID uuid, bool save = false)
        {
            Console.TeamConsole.Message("CREATE NEW SHARE ASSET", "TeamAssetCore", Color.white, true, false);
            //Debug.Log("CREATE NEW SHARE ASSET");
            ShareAsset asset = new ShareAsset(obj, uuid);

            if (save)
            {
                Console.TeamConsole.Message("Save", "TeamAssetCore", Color.white, true, false);
                //Debug.Log("Save");
                asset.localpath = SaveAssetToFile(obj, uuid);
                //Debug.Log("Saved");
            }
            else
            {
                Utility.Dispatcher.ToMainThread(() =>
                {
                    asset.localpath = AssetDatabase.GetAssetPath(obj);
                });
            }

            return asset;
        }

        [RunOnBackground]
        public static AssetUUID GetAssetUUID(UnityEngine.Object obj)
        {
            AssetUUID uuid = null;

            Console.TeamConsole.Message("Search Asset in Database", "TeamAssetCore", Color.white, true, false);
            //Debug.Log("Search Asset in Database");
            ShareAsset asset = GetAssetFromObject(obj);

            if (asset != null)
            {
                Console.TeamConsole.Message("Asset Found in Database", "TeamAssetCore", Color.white, true, false);
                //Debug.Log("Asset Found in Database");
                return asset.uuid;
            }
            else
            {
                string path = AssetUUID.GetUUIDFromPath(obj);
                Console.TeamConsole.Message("ASSET UUID PATH  " + path, "TeamAssetCore", Color.white, true, false);
                //Debug.Log("ASSET UUID PATH  " + path);

                if (path.StartsWith("Standard"))
                {
                    Console.TeamConsole.Message("Standard unity Asset of type " + obj.GetType().ToString() + " at path " + path, "TeamAssetCore", Color.white, true, false);
                    //Debug.Log("Standard unity Asset of type " + obj.GetType().ToString() + " at path " + path);
                    return new AssetUUID(obj, path, true);
                }
                else
                {
                    uuid = FindAssetFromPath(path, GetAssetType(obj));
                    Console.TeamConsole.Message("FOUND ASSET: " + (uuid == null ? "FALSE" : "TRUE"), "TeamAssetCore", Color.white, true, false);
                    //Debug.Log("FOUND ASSET: " + (uuid == null ? "FALSE" : "TRUE"));
                    if (uuid == null)
                    {
                        Console.TeamConsole.Message("CREATE NEW ASSET UUID", "TeamAssetCore", Color.white, true, false);
                        //Debug.Log("CREATE NEW ASSET UUID");
                        uuid = new AssetUUID(obj);

                        Console.TeamConsole.Message("UUID Loaded : " + (uuid == null ? "FALSE" : "TRUE"), "TeamAssetCore", Color.white, true, false);
                        //Debug.Log("UUID Loaded : " + (uuid == null ? "FALSE" : "TRUE"));

                        asset = CreateNewShareAsset(obj, uuid);

                        Console.TeamConsole.Message("REGISTER NEW UUID", "TeamAssetCore", Color.white, true, false);
                        //Debug.Log("REGISTER NEW UUID");
                        RegisterNewAsset_Local(asset);
                    }
                }
                return uuid;
            }
        }

        [OptimizedForBackroundMainDispatch]
        public static ShareAsset GetAssetFromObject(UnityEngine.Object obj)
        {
            int id=0;
            //Utility.Dispatcher.ToMainThread(() => 
            //{
            id = ReflectDllMethods.GetInstanceID(obj);//.GetInstanceID();
            //});

            List<ShareAsset> list;
            if (TeamAssetList != null)
            {
                Debug.Log("GET List of Tye: " + obj.GetType());
                list = TeamAssetList.GetList(obj);
                Debug.Log("FOUND? " + (list==null?"FALSE":"TRUE"));
                    for (int i = 0; i < list.Count; i++)
                    {
                        if (list[i].InstanceID == id)
                            return list[i];
                    }
                    return null;
            }
            else
                return null;
        }

        [RunOnBackground]
        public static AssetUUID FindAssetFromPath(string path, ShareAssetType type)
        {
            if (TeamAssetSystem.TeamAssetCore.TeamAssetList == null)
                TeamAssetList = TeamAssetDatabase.Load();

            foreach (ShareAsset c in TeamAssetList.GetList(type))
            {
                if (c.localpath == path)
                {
                    return c.uuid;// assets[c].uuid;
                }
            }

            /*for (int c = 0; c < AssetList.Count; c++)
            {
                if (AssetList[c].localpath == path)
                {
                    return assets[c].uuid;
                }
            }*/
            return null;
        }

        [RunOnBackground]
        public static string ToHash(string tohash)
        {
            return System.BitConverter.ToString(md5.ComputeHash(System.Text.Encoding.ASCII.GetBytes(tohash)));
        }

        public static ShareAsset CheckExists(string server_local_uuid, ShareAssetType type, AssetSearchOption option = AssetSearchOption.UUID)
        {
            if (TeamAssetList == null)
                TeamAssetList = TeamAssetDatabase.Load();

            Debug.Log("Database Loaded : " + (TeamAssetList == null ? "FALSE" : "TRUE  with " + TeamAssetList.ShareAssetCount + " Elements"));
            Debug.Log("UUID Loaded : " + (server_local_uuid == null ? "FALSE" : "TRUE"));

            switch (option)
            {
                case AssetSearchOption.LocalPath:
                    if (server_local_uuid != string.Empty && server_local_uuid.Length > 0)
                    {
                        Debug.Log("AssetUUID with LocalPathID " + server_local_uuid);
                        foreach (ShareAsset c in TeamAssetList.GetList(type))
                        {
                            Debug.Log("CHECK ELEMENT: " + c.uuid.UUID + " / " + server_local_uuid);
                            if (c.localpath == server_local_uuid)
                                return c;
                        }
                    }
                    break;
                case AssetSearchOption.ServerID:
                    if (server_local_uuid != string.Empty && server_local_uuid.Length > 0)
                    {
                        Debug.Log("AssetUUID with ServerID " + server_local_uuid);
                        foreach (ShareAsset c in TeamAssetList.GetList(type))
                            if (c.uuid.ServerID == server_local_uuid)
                                return c;
                    }
                    break;
                case AssetSearchOption.UUID:
                    if (server_local_uuid != string.Empty && server_local_uuid.Length > 0)
                    {
                        Debug.Log("AssetUUID with UUID " + server_local_uuid);
                        foreach (ShareAsset c in TeamAssetList.GetList(type))
                        {
                            Debug.Log("CHECK ELEMENT: " + c.uuid.UUID + " / " + server_local_uuid);
                            if (c.uuid.UUID == server_local_uuid)
                                return c;
                        }
                    }
                    break;
            }

            Debug.Log("AssetUUID return null");
            return null;
        }

        [RunOnBackground]
        public static bool CheckExists(UnityEngine.Object obj)
        {
            ShareAssetType type = GetAssetType(obj);
            List<ShareAsset> assets = TeamAssetList.GetList(type);
            int id = ReflectDllMethods.GetInstanceID(obj);
            for (int i = 0; i < assets.Count; i++)
                if (assets[i].InstanceID == id)
                    return true;
            return false;
        }

        [RunOnBackground]
        public static void ChecknRegisterAsset(UnityEngine.Object obj)
        {
            if (CheckExists(obj))
                return;
            else
            {
                Console.TeamConsole.Message("Register new Asset on Server", "TeamAssetCore", Color.white, true, false);
                //Debug.Log("Register new Asset on Server");
                AssetUUID uuid = new AssetUUID(obj);
                ShareAsset asset = CreateNewShareAsset(obj, uuid);
                List<ShareAsset> assets = TeamAssetList.GetList(uuid.type);
                assets.Add(asset);
                RegisterAsset_Global(obj, uuid);
            }
        }

        /*
         * IF File already exists but isnt loaded into Database -> will lose unity Reference of object
         */
        public static UnityEngine.Object CheckFileExistsAndLoadItIntoDatabase(AssetUUID uuid)
        {
            if(File.Exists("Assets/ServerLibraryFiles/" + uuid.UUID))
            {
                return (UnityEngine.Object)AssetDatabase.LoadAssetAtPath("Assets/ServerLibraryFiles/" + uuid.UUID, uuid.GetAssetType());
            }
            return null;
        }

        public static bool IsDefault<T>(this T value) where T : struct
        {
            bool isDefault = value.Equals(default(T));
            return isDefault;
        }

        [OptimizedForBackroundMainDispatch]
        public static UnityEngine.Object CreateDefault(ShareAssetType type)
        {
            UnityEngine.Object o = null;

            Utility.Dispatcher.ToMainThread(() =>
            {
                switch (type)
                {
                    case ShareAssetType.Material:
                        o = new Material(Shader.Find("Standard"));//Replace with "loading" material
                        break;
                    case ShareAssetType.Script:
                        break;
                    case ShareAssetType.Shader:
                        o = Shader.Find("Standard");
                        break;
                    case ShareAssetType.Texture:
                        o = new Texture2D(10, 10);//Replace with a "loading" ICON
                        break;
                    case ShareAssetType.GUISkin:
                        break;
                    case ShareAssetType.None:
                        break;
                    default:
                        break;
                }
            //return null;
            });
            Console.TeamConsole.Message("Return Data from MainThread " + (o == null ? "NULL" : "N"), "TeamAssetCore", Color.yellow, true, false);
           //Debug.LogWarning("Return Data from MainThread " + (o == null ? "NULL" : "N"));

            return o;
        }

        [Obsolete]
        public static T CreateInstanceOfObject<T>()
        {
            Debug.Log("Create instance");
            System.Type t = default(T).GetType();

            if (t != null)
            {
                Debug.Log("of Type " + t.ToString());
                if (t == typeof(Material))
                    return (T)(object)new Material(Shader.Find("Standard"));
                else if (t == typeof(Shader))
                    return (T)(object)Shader.Find("Standard");
                else
                {
                    return default(T);
                }
            }
            else//Shader doesnt have a default
            {
                return (T)(object)Shader.Find("Standard");
            }
        }

        internal static bool IsTeamAsset(UnityEngine.Object o, ShareAssetType type)
        {
            _tempUUID = new AssetUUID(o);
            if (CheckExists(_tempUUID.UUID, type) != null)
                return true;
            else
                return false;
        }

        internal static string GetServerID(UnityEngine.Object o, ShareAssetType type)
        {
            //string p = AssetDatabase.GetAssetPath(o);
            _tempUUID = new AssetUUID(o);
            ShareAsset a = CheckExists(_tempUUID.UUID, type);
            if (a != null)
                return a.uuid.ServerID;
            else
                return string.Empty;

            /*
            if(IsDefault(CheckIfUUIDExists(_tempUUID)))
                return false;
            return true;*/
        }

        /*public static AssetUUID CreateGetUUID(UnityEngine.Object o)
        {
            Init();            
            foreach(KeyValuePair<AssetUUID, UnityEngine.Object> a in registeredUUID)
            {
                if (a.Key.inid == o.GetInstanceID() || a.Value == o)
                    return a.Key;
            }
            return new AssetUUID(o);

        }*/

        public static void SetServerID(AssetUUID uid)
        {
            ShareAsset asset = CheckExists(uid.UUID, uid.type, AssetSearchOption.UUID);//Search after lcoal path
            Debug.Log(uid.UUID + "  " + uid.ServerID);

            if (asset != null)
            {
                Console.TeamConsole.Message(" SETID - TRUE", "TeamAssetCore", Color.white, true, false);
                //Debug.Log(" SETID - TRUE");
                asset.uuid.ServerID = uid.ServerID;
            }
            else if (TeamProjectEditor.TeamSettings.isDebug)
                Console.TeamConsole.Message("ASSET NOT FOUND IN UUID LIST", "TeamAssetCore", Color.red, true, false);
            //Debug.LogError("ASSET NOT FOUND IN UUID LIST");
        }

        public static void SetServerID(ShareAssetIdentify info, bool update_it=true)
        {
            ShareAsset asset=null;
            //Debug.LogError("SET SERVER ID " + info.uid.type+" || " + info.uid.ToString());
            Console.TeamConsole.Message("SET SERVER ID " + info.uid.type + " || " + info.uid.ToString(), "TeamAssetCore", Color.yellow, true, false);

            asset = CheckExists(info.uid.UUID, info.uid.type, AssetSearchOption.UUID);//Search after lcoal path
            Debug.Log(info.uid.UUID + "  " + info.uid.ServerID);
            if (asset != null)
                asset.uuid.ServerID = info.uid.ServerID;
            else 
                if (TeamProjectEditor.TeamSettings.isDebug)
                Console.TeamConsole.Message("SET SERVER ID " + info.uid.type + " || " + info.uid.ToString(), "TeamAssetCore", Color.red, true, false);
            //Debug.LogError("ASSET NOT FOUND IN UUID LIST");

            //Sent Asset
            switch (info.uid.type)
            {
                case ShareAssetType.Material:
                    if(update_it)
                        TeamProjectEditor.TeamServerConnection.AddToTransportStreamWriterList(LibaryClasses.CreateMaterialUpdateRequest((Material)asset.obj));
                    break;
                case ShareAssetType.Texture:
                    if (update_it)
                        TeamProjectEditor.TeamServerConnection.AddToTransportStreamWriterList(LibaryClasses.CreateTextureUpdateRequest((Texture2D)asset.obj));
                    break;
                case ShareAssetType.Shader:
                    if (update_it)
                        TeamProjectEditor.TeamServerConnection.AddToTransportStreamWriterList(LibaryClasses.CreateShaderUpdateRequest((Shader)asset.obj));
                    break;
            }
        }

        public static void Save()
        {
            TeamAssetDatabase.Save(TeamAssetList);
            if(TeamServerConnection.currentServer.currentScene!= -1)
                TeamAssetDatabase.SaveSceneAssets(SceneAssets, TeamServerConnection.currentServer.currentScene);
        }

        public static void Load(int SceneID)
        {
            if (TeamAssetList == null)
                TeamAssetList = TeamAssetDatabase.Load();

            SceneAssets = TeamAssetDatabase.LoadSceneAssets(SceneID);
        }

        [OptimizedForBackroundMainDispatch]
        public static string SaveAssetToFile(UnityEngine.Object o, AssetUUID u)//Return localpath
        {
            if (o.GetType() == typeof(Shader))//Saved by Converter.ToShader
                return "Assets/ServerLibraryFiles/" + u.UUID;
            else if (o.GetType() == typeof(Texture2D))//Saved by Converter.ToTexture
                return "Assets/ServerLibraryFiles/" + u.UUID;

            if (!Directory.Exists(TeamSceneManager.DataPath + "/ServerLibraryFiles/"))
                Directory.CreateDirectory(TeamSceneManager.DataPath + "/ServerLibraryFiles/");

            if (TeamProjectEditor.TeamSettings.isDebug)
                Console.TeamConsole.Message("ASSET UID: " + "[" + u.UUID + "]", "TeamAssetCore", Color.white, true, false);
            //Debug.Log("ASSET UID: " + "[" + u.UUID + "]");

            if (TeamProjectEditor.TeamSettings.isDebug)
                //Debug.LogWarning("SAVED ASSET: " + (TeamSceneManager.DataPath + "/ServerLibraryFiles/" + u.UUID));// (u.UUID.Split(":"[0])[1])));
                Console.TeamConsole.Message("SAVED ASSET: " + (TeamSceneManager.DataPath + "/ServerLibraryFiles/" + u.UUID), "TeamAssetCore", Color.yellow, true, false);

            Console.TeamConsole.Error("Save ASSET TYPE " + o.GetType().ToString(), "TeamAssetCore");

            Utility.Dispatcher.ToMainThread(() =>
            {
                if (!AssetDatabase.Contains(o))
                {
                    AssetDatabase.CreateAsset(o, u.UUID);// (u.UUID.Split(":"[0])[1]));
                    AssetDatabase.SaveAssets();
                }
            });

            return "Assets/ServerLibraryFiles/" + u.UUID;
        }

        public static void UpdateSceneAssetList(ModelContainer mc, string serverID)
        {
            int index = GetModelContainerIndex(serverID);
            if (index == -1)
                SceneAssets.Add(mc);
            else
                SceneAssets[index] = mc;
        }

        public static int UpdateSceneAssetList(ModelContainer mc, ObjectIdentify info)
        {
            if (mc == null)//New Temp
            {
                Console.TeamConsole.Message("Updated new Temp ModelContaienr", "TeamAssetCore", Color.white, true, false);
                //Debug.Log("Updated new Temp ModelContaienr");
                SceneAssets.Add(null);
                return SceneAssets.Count-1;
            }

            if (info == null || info.ServerID == string.Empty)
            {
                Console.TeamConsole.Message("Updated ModelContainer without ID", "TeamAssetCore", Color.white, true, false);
                //Debug.Log("Updated ModelContainer without ID");
                SceneAssets.Add(mc);
            }

            int index = GetModelContainerIndex(info);
            if (index == -1)
            {
                Console.TeamConsole.Message("Added new ModelContainer", "TeamAssetCore", Color.white, true, false);
                //Debug.Log("Added new ModelContainer");
                SceneAssets.Add(mc);
                return SceneAssets.Count - 1;
            }
            else
            {
                Console.TeamConsole.Message("Updated ModelContainer", "TeamAssetCore", Color.white, true, false);
                //Debug.Log("Updated ModelContainer");
                SceneAssets[index] = mc;
            }
            return index;
        }

        public static void UpdateSceneAssetList(List<TransformData> tu, ObjectIdentify info, long synctime)
        {
            ModelContainer mc;

            int index = GetModelContainerIndex(info);
            if (index != -1)
            {
                for (int models = 0; models < tu.Count; models++)
                {
                    mc = SceneAssets[index];
                    mc.syncTime = synctime;
                    int tindex = GetModelIndex(mc, info.ObjectID);
                    if(tindex !=-1)
                        mc.models[tindex].tran = tu[models];
                }
            }
            else
            {
                throw new System.ArgumentException("Object to Update not Found!!");
            }
        }

        public static void UpdateSceneAssetList(GameObjectData gu, ObjectIdentify info, long synctime)
        {
            int index = GetModelContainerIndex(info);
            if (index != -1)
            {
                ModelContainer mc = SceneAssets[index];
                mc.syncTime = synctime;
                int tindex = GetModelIndex(mc, info.ObjectID);
                    if (tindex != -1)
                        mc.models[tindex].game = gu;
            }
            else
                throw new System.ArgumentException("Object to Update not Found!!");
        }

        public static void UpdateSceneAssetList(List<ObjectComponent> cur, ObjectIdentify info, long synctime)
        {
            int index = GetModelContainerIndex(info);
            if (index != -1)
            {
                ModelContainer mc = SceneAssets[index];
                int tindex = GetModelIndex(mc, info.ObjectID);
                if (tindex != -1)
                {
                    mc.syncTime = synctime;

                    for (int components = 0; components < cur.Count; components++)
                    {
                        for(int i=0; i< mc.models[tindex].comp.Count; i++)
                        {
                            if(mc.models[tindex].comp[i].comtype == cur[components].comtype)
                            {
                                mc.models[tindex].comp[i] = cur[components];
                                break;
                            }
                        }
                    }
                }
            }
            else
                throw new System.ArgumentException("Object to Update not Found!!");
        }

        public static int GetModelIndex(ModelContainer mc, string id)
        {
            for (int i = 0; i < mc.models.Count; i++)
                if (mc.models[i].id.ObjectID == id)
                    return i;
            return -1;
        }

        public static int GetModelContainerIndex(ObjectIdentify info)
        {
            try
            {
                if (info == null || string.IsNullOrEmpty(info.ServerID) || info.ServerID.Length == 0)
                    return -1;

                if (SceneAssets == null)
                {
                    Console.TeamConsole.Message("SCENE ASSET LIST NULL  -- LOAD", "TeamAssetCore", Color.yellow, true, false);
                    //Debug.LogWarning("SCENE ASSET LIST NULL  -- LOAD");
                    Load(TeamServerConnection.currentServer.currentScene);
                }


                for (int i = 0; i < SceneAssets.Count; i++)
                    if (SceneAssets[i] != null)
                    {
                        if (SceneAssets[i].serverid == info.ServerID)
                            return i;
                    }
                return -1;
            }
            catch(System.Exception e)
            {
                Debug.LogError("ERRO MS: " + e.Message + " " + e.StackTrace);
            }
            return -1;
        }

        [RunOnBackground]
        public static int GetModelContainerIndex(string serverID)
        {
            for (int i = 0; i < SceneAssets.Count; i++)
                if (SceneAssets[i].serverid == serverID)
                    return i;
            return -1;
        }

        public static void OnDestroy()
        {
            try
            {
                Save();
            }
            catch(Exception e)
            {
                Debug.LogError("Save Error " + e.Message + "\n" + e.StackTrace);
            }
            //EditorApplication.update -= Update;
            EditorEventSystem.OnDeserilization -= OnDestroy;
        }

        [RunOnBackground]
        public static ShareAssetType GetAssetType(UnityEngine.Object obj)
        {
            System.Type type = obj.GetType();
            if (type == typeof(Material))
                return ShareAssetType.Material;
            else if (type == typeof(Texture2D))
                return  ShareAssetType.Texture;
            else if (type == typeof(Shader))
                return ShareAssetType.Shader;
            return ShareAssetType.None;
        }

        public static bool CheckForMainThread()
        {
            if (Thread.CurrentThread.GetApartmentState() == ApartmentState.STA && !Thread.CurrentThread.IsThreadPoolThread && Thread.CurrentThread.IsAlive)
            {
                MethodInfo correctEntryMethod = Assembly.GetEntryAssembly().EntryPoint;
                System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace();
                System.Diagnostics.StackFrame[] frames = trace.GetFrames();
                for (int i = frames.Length - 1; i >= 0; i--)
                {
                    MethodBase method = frames[i].GetMethod();
                    if (correctEntryMethod == method)
                    {
                        return true;
                    }
                }
            }
            return false;
            // throw exception, the current thread is not the main thread...
        }

        [OptimizedForBackroundMainDispatch]
        public static void LoadAssetIDs(TeamAssets assets)
        {
            Utility.Dispatcher.ToMainThread(() =>
            {
                List<ShareAsset> list;
                for (int l = 0; l < TeamAssets.listCount; l++)
                {
                    list = assets.GetList((ShareAssetType)l);
                    for (int i = 0; i < list.Count; i++)
                        list[i].LoadObject();
                }
            });
        }
    }
}
