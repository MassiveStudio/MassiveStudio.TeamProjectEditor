﻿using System.IO;
using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

using MassiveStudio.TeamProjectEditor.Attributes;

namespace MassiveStudio.TeamProjectEditor.TeamAssetSystem
{
    public static class TeamAssetDatabase
    {
        [RunOnBackground]
        internal static void Save(TeamAssets data)
        {
            if (!Directory.Exists(TeamSceneManager.RootPath + "/TeamEditor"))
                Directory.CreateDirectory(TeamSceneManager.RootPath + "/TeamEditor");

            Debug.LogWarning("Save AssetDatabase");
            if (TeamAssetSystem.TeamAssetCore.TeamAssetList != null)
            {
                using (FileStream stream = new FileStream(TeamSceneManager.RootPath + "/TeamEditor/TeamAssets.db", FileMode.OpenOrCreate, FileAccess.ReadWrite))
                {
                    BinaryFormatter format = new BinaryFormatter();
                    format.SurrogateSelector = Serializer.getsurrogateselector;
                    format.Serialize(stream, data);
                }
                Debug.Log("ASSET UUIDS Saved");
            }
            else
                Debug.LogError("Asset List is NULL");
        }

        [RunOnBackground]
        internal static TeamAssets Load()
        {
            Debug.LogWarning("Load AssetDatabase");

            TeamAssets uuids = null;
            Debug.LogWarning("INIT VARIABLES");

            //DeBug
            //return new Dictionary<UnityEngine.Object, ShareAsset>();
            string path = TeamSceneManager.RootPath + "/TeamEditor/TeamAssets.db";
            if (File.Exists(path))
            {
                //Debug.Log("FILE EXISTS" + "\n" + path);
                using (FileStream stream = new FileStream(path, FileMode.Open))
                {
                    if (!stream.CanRead)
                    {
                        Debug.LogError("TEAMASSET DATABASE Open Stream not readable");
                        return new TeamAssets();
                    }

                    BinaryFormatter format = new BinaryFormatter();
                    try
                    {
                        uuids = format.Deserialize(stream) as TeamAssets;//as Dictionary<UnityEngine.Object, ShareAsset>;
                    }
                    catch (System.Exception e)
                    {
                        Debug.Log("TEAM ASSETDATABSE " + e.Message + "\n"+ e.StackTrace);
                        if(stream!=null)
                            stream.Close();
                    }
                }
                if(uuids!=null)
                    Debug.Log("Loaded " + uuids.ShareAssetCount + " ASSET UUIDS");
            }
            else
                Debug.LogWarning("Haven't found File");

            if (uuids == null)
                uuids = new TeamAssets();

            Debug.Log("Created AssetDatabase");
            TeamAssetSystem.TeamAssetCore.LoadAssetIDs(uuids);
            Debug.Log("Loaded Instance IDs");

            return uuids;
        }

        [RunOnBackground]
        internal static void SaveSceneAssets(List<ModelContainer> assetList, int currentScene)
        {
            if (!Directory.Exists(TeamSceneManager.RootPath + "/TeamEditor"))
                Directory.CreateDirectory(TeamSceneManager.RootPath + "/TeamEditor");

            Debug.LogWarning("Save SceneAssets " + TeamSceneManager.loadedScene);
            if (TeamAssetSystem.TeamAssetCore.SceneAssets != null)
            {
                using (FileStream stream = new FileStream(TeamSceneManager.RootPath + "/TeamEditor/SceneAssets" + currentScene + ".db", FileMode.OpenOrCreate, FileAccess.ReadWrite))
                {
                    BinaryFormatter format = new BinaryFormatter();
                    format.SurrogateSelector = Serializer.getsurrogateselector;
                    try
                    {
                        Utility.Dispatcher.ToMainThread(() =>
                        {
                            format.Serialize(stream, assetList);
                        });
                    }
                    catch (System.Exception e)
                    {
                        Debug.LogError("ERROR TEAMASSETDATABASE SAVESCENEASSETS: " + e.Message + "\n" + e.StackTrace + "\n" + e.Source);
                    }
                }
                Debug.Log("Scene Assets Saved");
            }
            else
            {
                if (File.Exists(TeamSceneManager.RootPath + "/TeamEditor/SceneAssets" + currentScene + ".db"))
                    File.Delete(TeamSceneManager.RootPath + "/TeamEditor/SceneAssets" + currentScene + ".db");
                Debug.LogError("Asset List is NULL");
            }
            //Clear Cache
            TeamAssetSystem.TeamAssetCore.SceneAssets = null;
        }

        [RunOnBackground]
        internal static List<ModelContainer> LoadSceneAssets(int sceneID)
        {
            //Save Current Scene
            Debug.Log("Save current Scene " + TeamSceneManager.loadedScene + "  Load SceneAssets " + sceneID);

            if (TeamAssetCore.SceneAssets != null && TeamSceneManager.loadedScene!=-1)
                SaveSceneAssets(TeamAssetCore.SceneAssets, TeamSceneManager.loadedScene);

            List<ModelContainer> list = null;
        
            string path = TeamSceneManager.RootPath + "/TeamEditor/SceneAssets" + sceneID + ".db";
            if (File.Exists(path))
            {
                //Debug.Log("FILE EXISTS" + "\n" + path);
                using (FileStream stream = new FileStream(path, FileMode.Open))
                {
                    if (!stream.CanRead)
                    {
                        Debug.Log("TEAMASSET DATABASE Open Stream not readable");
                        return new List<ModelContainer>();
                    }

                    BinaryFormatter format = new BinaryFormatter();
                    format.SurrogateSelector = Serializer.getsurrogateselector;

                    if(format.SurrogateSelector == null)
                        Debug.LogWarning("SUROSEL is NULL");

                    try
                    {
                        Utility.Dispatcher.ToMainThread(() =>
                        {
                            list = format.Deserialize(stream) as List<ModelContainer>;//as Dictionary<UnityEngine.Object, ShareAsset>;
                        });
                    }
                    catch (System.Exception e)
                    {
                        Debug.Log("TEAM ASSETDATABSE " + e.Message + "\n" + e.StackTrace);
                        if (stream != null)
                            stream.Close();
                    }
                }
                if (list != null)
                    Debug.Log("Loaded " + list.Count + " Scene Assets");
                else
                    Debug.LogError("NULL LIST");
            }

            if (list == null)
                list = new List<ModelContainer>();

            Debug.Log("Created AssetDatabase");
            TeamSceneManager.loadedScene = sceneID;
            TeamSceneManager.NeedsReloadSceneAssets = true;
            return list;
        }
    }
}
