﻿using System;
using System.IO;
using UnityEngine;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace MassiveStudio.TeamProjectEditor
{
    public static class Serializer
    {
        private static SurrogateSelector sselector;
        public static SurrogateSelector getsurrogateselector
        {
            get
            {
                if (sselector == null)
                {
                    sselector = new SurrogateSelector();
                    SurrogateSelectoren(sselector);
                }
                return sselector;
            }
         }

        [Obsolete]
        static public Header Serialize(UnityEngine.Object anySerializableObject)
        {
            MemoryStream memoryStream = new MemoryStream();
            BinaryFormatter formater = new BinaryFormatter();

            /*SurrogateSelector ss = new SurrogateSelector();
            SurrogateSelectoren(ss);
            formater.SurrogateSelector = ss;*/
            formater.SurrogateSelector = getsurrogateselector;// ss;

            formater.Serialize(memoryStream, anySerializableObject);
            Header h = new Header(memoryStream.ToArray());
            return h;
        }

        [Obsolete]
        static public byte[] ToByte(UnityEngine.Object anySerializableObject)
	    {
		    MemoryStream memoryStream = new MemoryStream();
            BinaryFormatter formater  = new BinaryFormatter();
            //SurrogateSelector ss = new SurrogateSelector();
            //SurrogateSelectoren(ss);
            //formater.SurrogateSelector = ss;
            formater.SurrogateSelector = getsurrogateselector;// ss;
            formater.Serialize(memoryStream, anySerializableObject);
		    return (memoryStream.ToArray());
	    }

        static public byte[] TypeToByte<T>(object anySerializableObject)
        {

            MemoryStream memoryStream = new MemoryStream();
            BinaryFormatter formater = new BinaryFormatter();
            /*SurrogateSelector ss = new SurrogateSelector();
            SurrogateSelectoren(ss);
            formater.SurrogateSelector = ss;*/
            formater.SurrogateSelector = getsurrogateselector;// ss;

            try
            {
                formater.Serialize(memoryStream, anySerializableObject);
            }
            catch (System.Exception e)
            {
                Debug.LogError("SERILISATION FORMAT ERROR " + e.Message + "\n" + "Type: " + anySerializableObject.GetType());
            }

            return (memoryStream.ToArray());
        }

        static public void TypeToFile<T>(FileStream stream, object anySerializableObject)
        {
            //MemoryStream memoryStream = new MemoryStream();
            BinaryFormatter formater = new BinaryFormatter();
            /*SurrogateSelector ss = new SurrogateSelector();
            SurrogateSelectoren(ss);
            formater.SurrogateSelector = ss;*/
            formater.SurrogateSelector = getsurrogateselector;// ss;

            try
            {
                formater.Serialize(stream, anySerializableObject);
            }
            catch (System.Exception e)
            {
                Debug.LogError("SERILISATION FORMAT ERROR " + e.Message);
            }
        }

        [Obsolete]
        static public UnityEngine.Object Deserialize(Header message)
        {
            MemoryStream memoryStream = new MemoryStream(message.data);
            BinaryFormatter formater = new BinaryFormatter();
            /*SurrogateSelector ss = new SurrogateSelector();
            SurrogateSelectoren(ss);
            formater.SurrogateSelector = ss;*/
            formater.SurrogateSelector = getsurrogateselector;
            UnityEngine.Object obj = null;
            try
            {
                obj = (UnityEngine.Object)formater.Deserialize(memoryStream);
            }
            catch (System.Exception e)
            {
                Debug.LogError(e.Message);
            }
            Debug.Log("DESERIALIZED");
            return obj;

        }

        [Obsolete]
        static public UnityEngine.Object ToObject(byte[] message)
	    {
            MemoryStream memoryStream = new MemoryStream(message);
            BinaryFormatter formater  = new BinaryFormatter();
            /*SurrogateSelector ss = new SurrogateSelector();
            SurrogateSelectoren(ss);
            formater.SurrogateSelector = ss;*/
            formater.SurrogateSelector = getsurrogateselector;// ss;
            return (UnityEngine.Object)formater.Deserialize(memoryStream);
	    }

        static public T ToType<T>(byte[] message)
        {
            try
            {
                MemoryStream memoryStream = new MemoryStream(message);
                BinaryFormatter formater = new BinaryFormatter();
                //SurrogateSelector ss = new SurrogateSelector();
                //SurrogateSelectoren(ss);
                formater.SurrogateSelector = getsurrogateselector;// ss;
                return (T)formater.Deserialize(memoryStream);
            }
            catch(System.Exception e)
            {
                Debug.LogError("ERROR DESERIALISATION " + e.Message);
            }
            return default(T);
        }

        static public T ToType<T>(FileStream stream)
        {
            try
            {
                //MemoryStream memoryStream = new MemoryStream(message);
                BinaryFormatter formater = new BinaryFormatter();
                // SurrogateSelector ss = new SurrogateSelector();
                //SurrogateSelectoren(ss);
                //formater.SurrogateSelector = ss;
                formater.SurrogateSelector = getsurrogateselector;// ss;
                return (T)formater.Deserialize(stream);
            }
            catch (System.Exception e)
            {
                Debug.LogError("ERROR DESERIALISATION " + e.Message);
            }
            return default(T);
        }

        public static void SurrogateSelectoren(SurrogateSelector ss)
        {
            if (ss == null)
                Debug.LogError("NULL REFERENCE INPUT OF SURROGATESELECTOR");

            Vector3Surrogate Vector3_SS = new Vector3Surrogate();
            ss.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), Vector3_SS);

            Vector2Surrogate Vector2_SS = new Vector2Surrogate();
            ss.AddSurrogate(typeof(Vector2), new StreamingContext(StreamingContextStates.All), Vector2_SS);
            
            /* ->Replaced with TextureData
            Texture2DSurrogate Texture2D_SS = new Texture2DSurrogate();
            ss.AddSurrogate(typeof(Texture2D), new StreamingContext(StreamingContextStates.All), Texture2D_SS);
            */

            ColorSurrogate Color_SS = new ColorSurrogate();
            ss.AddSurrogate(typeof(Color), new StreamingContext(StreamingContextStates.All), Color_SS);

            QuaternionSurrogate Quaternion_SS = new QuaternionSurrogate();
            ss.AddSurrogate(typeof(Quaternion), new StreamingContext(StreamingContextStates.All), Quaternion_SS);

            RectSurrogate Rect_SS = new RectSurrogate();
            ss.AddSurrogate(typeof(Rect), new StreamingContext(StreamingContextStates.All), Rect_SS);

            //Internal - mainthread only
            PhysicMaterialSurrogate PhysicMaterial_SS = new PhysicMaterialSurrogate();
            ss.AddSurrogate(typeof(PhysicMaterial), new StreamingContext(StreamingContextStates.All), PhysicMaterial_SS);

            /*

                        //		var GameObject_SS :GameObjectSurrogate  = new GameObjectSurrogate();
                        //		ss.AddSurrogate(typeof(GameObject),new StreamingContext(StreamingContextStates.All),GameObject_SS);

                        //		var Transform_SS:TransformSurrogate  = new TransformSurrogate();
                        //		ss.AddSurrogate(typeof(Transform), new StreamingContext(StreamingContextStates.All), Transform_SS);


                        MaterialSurrogate Material_SS = new MaterialSurrogate();
                        ss.AddSurrogate(typeof(Material), new StreamingContext(StreamingContextStates.All), Material_SS);

            //COLLIDER
                        CapsuleColliderSurrogate CapsuleCollider_SS = new CapsuleColliderSurrogate();
                        ss.AddSurrogate(typeof(CapsuleCollider), new StreamingContext(StreamingContextStates.All), CapsuleCollider_SS);

                        BoxColliderSurrogate BoxCollider_SS = new BoxColliderSurrogate();
                        ss.AddSurrogate(typeof(CapsuleCollider), new StreamingContext(StreamingContextStates.All), BoxCollider_SS);

                        MeshColliderSurrogate MeshCollider_SS = new MeshColliderSurrogate();
                        ss.AddSurrogate(typeof(CapsuleCollider), new StreamingContext(StreamingContextStates.All), MeshCollider_SS);

                        SphereColliderSurrogate SphereCollider_SS = new SphereColliderSurrogate();
                        ss.AddSurrogate(typeof(CapsuleCollider), new StreamingContext(StreamingContextStates.All), SphereCollider_SS);

                */
            //		var Mono_SS:MonobehaivurSurrogate  = new MonobehaivurSurrogate();
            //		ss.AddSurrogate(typeof(MonoBehaviour),  new StreamingContext(StreamingContextStates.All),Mono_SS);

        }
    }
}
