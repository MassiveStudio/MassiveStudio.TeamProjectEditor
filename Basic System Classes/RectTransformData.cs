﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace MassiveStudio.TeamProjectEditor
{
    [System.Serializable]
    public class RectTransformData : TransformData
    {
        /*public Vector3 pos;
        public Vector3 rot;
        public Vector3 scal;*/
        //public string parent; //TEAMVIEW / TEAMOBJECT . OBJECTID

        public Vector2 AnMin;
        public Vector2 AnMax;
        public Vector2 AnPos;

        public Vector2 SizDel;
        public Vector2 Piv;

        public RectTransformData()
        {

        }
        /*
        public void ToRectTransform(RectTransform t)
        {
            t.position = pos;
            t.rotation = Quaternion.Euler(rot);
            t.localScale = scal;

            t.anchorMin = AnMin;
            t.anchorMax = AnMax;
            t.anchoredPosition = AnPos;

            t.sizeDelta = SizDel;
            t.pivot = Piv;
        }*/
    }
}
