﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using MassiveStudio.TeamProjectEditor;

namespace MassiveStudio.TeamProjectEditor.Utility
{
    public static class UtilityGUI
    {
        public static Texture2D DownArrow
        {
            get
            {
                if (m_downarrow == null)
                    m_downarrow = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.GUI_Arrow_Down);

                return m_downarrow;
            }
        }
        private static Texture2D m_downarrow;

        public static Texture2D UpArrow
        {
            get
            {
                if (m_uparrow == null)
                    m_uparrow = Converter.ToTexture(MassiveStudio.TeamProjectEditor.Properties.Resources.GUI_Arrow_Up);

                return m_uparrow;
            }
        }
        public static Texture2D m_uparrow;

        public static GUIStyle ConsoleMenuStyle
        {
            get
            {
                if (m_consolemenustyle == null)
                {
                    m_consolemenustyle = new GUIStyle("Box");
                    m_consolemenustyle.imagePosition = ImagePosition.ImageLeft;
                }
                return m_consolemenustyle;
            }
        }
        public static GUIStyle m_consolemenustyle;

        internal static string[] GetNameAndTooltipString(string nameAndTooltip)
        {
            nameAndTooltip = (string)InternalMethods.LocalizationDatabase._(nameAndTooltip);// GetLocalizedString
            string[] array = new string[3];
            string[] array2 = nameAndTooltip.Split(new char[]
            {
                '|'
            });
            switch (array2.Length)
            {
                case 0:
                    array[0] = string.Empty;
                    array[1] = string.Empty;
                    break;
                case 1:
                    array[0] = array2[0].Trim();
                    array[1] = array[0];
                    break;
                case 2:
                    array[0] = array2[0].Trim();
                    array[1] = array[0];
                    array[2] = array2[1].Trim();
                    break;
                default:
                    Debug.LogError("Error in Tooltips: Too many strings in line beginning with '" + array2[0] + "'");
                    break;
            }
            return array;
        }

        private static Hashtable s_TextGUIContents;

        internal static GUIContent TextContent(string textAndTooltip)
        {
            if (textAndTooltip == null)
            {
                textAndTooltip = string.Empty;
            }
            GUIContent gUIContent = (GUIContent)s_TextGUIContents[textAndTooltip];
            if (gUIContent == null)
            {
                string[] nameAndTooltipString = GetNameAndTooltipString(textAndTooltip);
                gUIContent = new GUIContent(nameAndTooltipString[1]);
                if (nameAndTooltipString[2] != null)
                {
                    gUIContent.tooltip = nameAndTooltipString[2];
                }
                s_TextGUIContents[textAndTooltip] = gUIContent;
            }
            return gUIContent;
        }

        internal static void MultiIntField(Rect rec, GUIContent[] subLabels, int[] values, float labelWidth)
        {
            int num = values.Length;
            float num2 = (rec.width - (float)(num - 1) * 2f) / (float)num;
            Rect position2 = new Rect(rec);
            position2.width = num2;
            float labelWidth2 = EditorGUIUtility.labelWidth;
            int indentLevel = EditorGUI.indentLevel;
            EditorGUIUtility.labelWidth = labelWidth;
            EditorGUI.indentLevel = 0;
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = EditorGUI.IntField(position2, subLabels[i], values[i]);
                position2.x += num2 + 2f;
            }
            EditorGUIUtility.labelWidth = labelWidth2;
            EditorGUI.indentLevel = indentLevel;
        }

        private static GUIContent[] s_TBLabels = new GUIContent[]
        {
            new GUIContent("T"),//TOP
            new GUIContent("B")//BOTTOM
        };

        private static GUIContent[] s_RLLabels = new GUIContent[]
        {
            new GUIContent("R"),//RIGHT
            new GUIContent("L")//LEFT
        };

        private static int[] s_Vector2Ints = new int[2];
        public static RectOffset RectOffsetField(Rect rec, RectOffset value)//Modify from UnityEditor.EditorGUI.RectField
        {
            rec.height = 16f;
            s_Vector2Ints[0] = value.right;
            s_Vector2Ints[1] = value.left;
            EditorGUI.BeginChangeCheck();
            MultiIntField(rec, s_RLLabels, s_Vector2Ints, 13f);
            if (EditorGUI.EndChangeCheck())
            {
                value.right = s_Vector2Ints[0];
                value.left = s_Vector2Ints[1];
            }
            rec.y += 16f;
            s_Vector2Ints[0] = value.top;
            s_Vector2Ints[1] = value.bottom;
            EditorGUI.BeginChangeCheck();
            MultiIntField(rec, s_TBLabels, s_Vector2Ints, 13f);
            if (EditorGUI.EndChangeCheck())
            {
                value.top = s_Vector2Ints[0];
                value.bottom = s_Vector2Ints[1];
            }
            return value;
        }

        public static Font FontField(Rect rec, Font font)
        {
            return EditorGUI.ObjectField(rec, font, typeof(Font), false) as Font;
        }

        /*public static void DrawTableRow(Rect rec, TableRow row, string[] content, GUIStyle style = null)
        {

        }*/
        public static bool DrawTableRow(Rect rec, ref TableRow row, string[] content, Color col, GUIStyle style, bool titlebar = false)
        {
            Event e = Event.current;
            if (content.Length != row.columnssizes.Length)
                throw new IndexOutOfRangeException("Content Length is not same as the Initialized column Length");
            if (row == null)
                throw new NullReferenceException("TableRow is null or not Initialized");

            Vector2 size = new Vector2(rec.width, rec.height);
            if (size != row.size)
            {
                row.size = size;
                row.RecalculateColumns();
            }
            int c = 0;
            GUI.BeginGroup(rec);
            Rect trec = new Rect();

            GUIContent temp_conent = new GUIContent();

            foreach (Rect r in row.rects)
            {
                /* if (style != null)
                 {
                     GUI.Box(r, "", style);
                     GUI.color = col;
                     GUI.Label(r, content[c]);
                 }
                 else
                 {*/

                //if (Mathf.CeilToInt(style.CalcHeight(new GUIContent(content[c]), r.width)) > r.width)
                //   Debug.LogError("TO SMALL");

                trec.x = r.x;
                trec.y = r.y;
                trec.width = r.width;
                trec.height = rec.height;
                GUI.color = col;

                temp_conent.text = content[c];

                if (row.sortsel[0] == c)
                    temp_conent.image = (row.sortsel[1] == 0 ? DownArrow : UpArrow);
                else
                    temp_conent.image = null;

                if (titlebar)
                {
                    if (GUI.Button(trec, temp_conent, style))
                    {
                        int t = row.sortsel[0];
                        row.sortsel[0] = (byte)c;
                        if (t == row.sortsel[0])
                        {
                            if (row.sortsel[1] == 0)
                                row.sortsel[1] = 1;
                            else
                                row.sortsel[1] = 0;

                            row._dirty = true;
                        }
                        else
                        {
                            row.sortsel[1] = 0;
                            row._dirty = true;
                        }
                    }
                }
                else
                    GUI.Box(trec, content[c], style);
                //}
                // GUI.color = Color.white;

                c++;
            }
            GUI.EndGroup();

            if (rec.Contains(e.mousePosition) && e.type == EventType.MouseDown && e.button == 0)
            {
                // Debug.Log("SELECT");
                return true;
            }
            else
                return false;
        }

        public static int DrawTable<T>(Rect rec, int TitleBarHeight, ref TableRow row, ref List<T> list, string[] title, GUIStyle TitlebarStyle, GUIStyle ItemStyle, ref int max, ref float scroll, ref bool calc, bool autoScroll, int select, string[] order_var) where T : Utility.ListType
        {
            Event e = Event.current;
            TableRow r = row;

            GUI.BeginGroup(rec);
            //Header
            //DrawTableRow()

            //int max = sort_output.Count * 25;

            if (max < (rec.height - TitleBarHeight))
                scroll = 0f;// GUI.VerticalScrollbar(new Rect(rec.width - 15f, 40, 15f, rec.height - 40), 0f, (rec.height - 40), 0f, 0f);
            else
                scroll = GUI.VerticalScrollbar(new Rect(rec.width - 15f, TitleBarHeight, 15f, rec.height - TitleBarHeight), scroll, (rec.height - TitleBarHeight), 0, max);

            if (e.isMouse || e.isKey || calc || e.type == EventType.Repaint)
            {
                //TITELBAR
                UtilityGUI.DrawTableRow(new Rect(0, 0, rec.width - 16, TitleBarHeight), ref row, title, Color.white, TitlebarStyle, true);
                if (row._dirty)
                {
                    row._dirty = false;
                    list = list.OrderBy(rs => rs.GetType().GetField(order_var[r.sortsel[0]], BindingFlags.GetField | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance).GetValue(rs)).ToList();
                    if (row.sortsel[1] == 1)
                        list.Reverse();
                }

                int c = 0;
                GUI.BeginGroup(new Rect(0, TitleBarHeight, rec.width - 16, rec.height - TitleBarHeight));
                int y = 0;

                int h = 20;
                float s = scroll;
                Color col = Color.white;
                list.ForEach(cm =>
                {
                    /*if (select == c)
                        h = GetMinConsoleContentHeigth(cm);
                    else
                        h = 20;*/
                    try
                    {
                        if (UtilityGUI.DrawTableRow(new Rect(0, y - s, rec.width - 16, h), ref r, cm.ToData(), col, ItemStyle))//, cm.type == ConsoleMessageType.Team?style:null))
                        {
                            if (select == c)
                                select = -1;
                            else
                                select = c;

                            //h = GetMinConsoleContentHeigth(cm);
                            //JumpToPos(y - scroll, h);
                        }
                    }
                    catch (System.Exception exc)
                    {
                        Debug.LogError("Team Console: " + exc.Message);
                    }
                    c++;
                    y += h;
                });

                max = y;

                if (calc)
                {
                    calc = false;
                    if (autoScroll)
                        scroll = max;
                    //Repaint();
                }

                GUI.color = Color.white;
                GUI.EndGroup();
            }
            if (e.type == EventType.ScrollWheel)
            {
                scroll += e.delta.y * 20;
                calc = true;
            }

            if (scroll < 0)
                scroll = 0f;
            else if (scroll > max)
                scroll = max;

            GUI.EndGroup();
            return select;
        }

        internal static bool ButtonMouseDown(Rect rec, GUIContent content, FocusType focusType, GUIStyle style)
        {
            //int controlID = GUIUtility.GetControlID(EditorGUI.s_ButtonMouseDownHash, focusType, rec);
            return ButtonMouseDown(0, rec, content, style);
        }

        internal static bool ButtonMouseDown(int id, Rect rec, GUIContent content, GUIStyle style)
        {
            Event current = Event.current;
            EventType type = current.type;
            switch (type)
            {
                case EventType.KeyDown:
                    if (GUIUtility.keyboardControl == id && current.character == ' ')
                    {
                        Event.current.Use();
                        return true;
                    }
                    return false;
                case EventType.KeyUp:
                case EventType.ScrollWheel:
                    if (type != EventType.MouseDown)
                    {
                        return false;
                    }
                    if (rec.Contains(current.mousePosition) && current.button == 0)
                    {
                        Event.current.Use();
                        return true;
                    }
                    return false;
                case EventType.Repaint:
                    if (EditorGUI.showMixedValue)
                    {
                        BeginHandleMixedValueContentColor();
                        style.Draw(rec, s_MixedValueContent, id, false);
                        EndHandleMixedValueContentColor();
                    }
                    else
                    {
                        style.Draw(rec, content, id, false);
                    }
                    return false;
            }

            if (type != EventType.MouseDown)
            {
                return false;
            }
            if (rec.Contains(current.mousePosition) && current.button == 0)
            {
                Event.current.Use();
                return true;
            }
            return false;
        }

        public static Color s_MixedValueContentColorTemp;

        public static Color s_MixedValueContentColor;

        private static GUIContent s_MixedValueContent = new GUIContent("—|Mixed Values");
        internal static void BeginHandleMixedValueContentColor()
        {
            s_MixedValueContentColorTemp = GUI.contentColor;
            GUI.contentColor = ((!EditorGUI.showMixedValue) ? GUI.contentColor : (GUI.contentColor * s_MixedValueContentColor));
        }
        internal static void EndHandleMixedValueContentColor()
        {
            GUI.contentColor = s_MixedValueContentColorTemp;
        }
        internal static string SearchField(Rect rec, string text)
        {
            //int controlID = GUIUtility.GetControlID(GUIUtility.GetControlID(FocusType.Passive), FocusType.Keyboard, rec);
            Rect position2 = rec;
            position2.width -= 15f;
            //bool flag;
            text = EditorGUI.TextField(position2, text, EditorStyles.textField);
            Rect position3 = rec;
            position3.x += rec.width - 15f;
            position3.width = 15f;
            if (text.Length > 0)
                if (GUI.Button(position3, new GUIContent("X")))// GUIContent.none, (!(text != string.Empty)) ? EditorStyles. : EditorStyles.searchFieldCancelButton) && text != string.Empty)
                {
                    text = string.Empty;
                    GUIUtility.keyboardControl = 0;
                }

            /*if(Event.current.type == EventType.KeyDown && Event.current.isKey && !Event.current.functionKey && Event.current.keyCode != KeyCode.None && Event.current.modifiers == EventModifiers.None)//Input Char
            {
                if(Event.current.keyCode == KeyCode.Delete)
                {
                    if (text.Length > 0)
                        text = text.Substring(0, text.Length - 2);
                }
                else
                    text += Event.current.keyCode.ToString();
            }*/

            return text;
        }
        // UnityEditor.EditorGUI
        internal static Rect GUIToScreenRect(Rect guiRect)
        {
            Vector2 vector = GUIUtility.GUIToScreenPoint(new Vector2(guiRect.x, guiRect.y));
            guiRect.x = vector.x;
            guiRect.y = vector.y;
            return guiRect;
        }
        internal static UnityEngine.Object CreateScriptAssetFromTemplate(string pathName, string resourceFile)
        {
            string fullPath = Path.GetFullPath(pathName);
            StreamReader streamReader = new StreamReader(resourceFile);
            string text = streamReader.ReadToEnd();
            streamReader.Close();
            string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(pathName);
            text = Regex.Replace(text, "#NAME#", fileNameWithoutExtension);
            string text2 = Regex.Replace(fileNameWithoutExtension, " ", string.Empty);
            text = Regex.Replace(text, "#SCRIPTNAME#", text2);
            if (char.IsUpper(text2, 0))
            {
                text2 = char.ToLower(text2[0]) + text2.Substring(1);
                text = Regex.Replace(text, "#SCRIPTNAME_LOWER#", text2);
            }
            else
            {
                text2 = "my" + char.ToUpper(text2[0]) + text2.Substring(1);
                text = Regex.Replace(text, "#SCRIPTNAME_LOWER#", text2);
            }
            bool encoderShouldEmitUTF8Identifier = true;
            bool throwOnInvalidBytes = false;
            UTF8Encoding encoding = new UTF8Encoding(encoderShouldEmitUTF8Identifier, throwOnInvalidBytes);
            bool append = false;
            StreamWriter streamWriter = new StreamWriter(fullPath, append, encoding);
            streamWriter.Write(text);
            streamWriter.Close();
            AssetDatabase.ImportAsset(pathName);
            return AssetDatabase.LoadAssetAtPath(pathName, typeof(UnityEngine.Object));
        }
    }

    public class TableRow
    {
        public Vector2 size;
        public float[] columnssizes;
        public Rect[] rects;
        // public string[] values;
        public bool[] isfixed;
        public byte[] sortsel = new byte[2];//0- Element  / 1-Direction (0-1)
        public bool _dirty = false;

        public TableRow(Rect rec, int columns, string[] content)
        {
            if (content.Length != columns)
                throw new System.IndexOutOfRangeException("Content Length is not same as columns Length");
            else
            {
                this.size = new Vector2(rec.width, rec.height);
                if (this.columnssizes.Length != columns)
                    columnssizes = new float[columns];
                if (this.isfixed.Length != columns)
                    this.isfixed = new bool[columns];

                rects = new Rect[columns];
                // values = content;
                RecalculateColumns();
            }
        }

        public TableRow(Rect rec, float[] sizes)
        {
            this.size = new Vector2(rec.width, rec.height);
            this.isfixed = new bool[sizes.Length];
            columnssizes = sizes;
            rects = new Rect[sizes.Length];
            RecalculateColumns();
        }

        public TableRow(Rect rec, float[] sizes, bool[] @fixed)
        {
            this.size = new Vector2(rec.width, rec.height);
            columnssizes = sizes;
            this.isfixed = @fixed;
            rects = new Rect[sizes.Length];
            RecalculateColumns();
        }

        public void RecalculateColumns()
        {
            float m = 0;// columnssizes.Sum();

            float max = size.x;

            for (int i = 0; i < columnssizes.Length; i++)
            {
                if (isfixed[i])
                    max -= columnssizes[i];
                else
                    m += columnssizes[i];
            }

            float x = 0;
            float pp = size.x / m;
            for (int a = 0; a < columnssizes.Length; a++)
            {
                if (isfixed[a])
                    rects[a] = new Rect(x, 0, columnssizes[a], size.y);
                else
                    rects[a] = new Rect(x, 0, pp * columnssizes[a], size.y);

                x += rects[a].width;
            }
        }
    }

    public class TableSelection
    {
        int row_selected;
        int column_selected;

        public TableSelection()
        {
            row_selected = 0;
            column_selected = 0;
        }

        public TableSelection(int row, int col)
        {
            row_selected = row;
            column_selected = col;
        }
    }

    public interface ListType
    {
        string[] ToData();
    }
}