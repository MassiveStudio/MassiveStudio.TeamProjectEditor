﻿using System.Collections.Generic;
using System.IO;

using MassiveStudio.TeamProjectEditor;
namespace MassiveStudio.TeamProjectEditor.Utility
{
    public static class ProjectPref//For ThreadSafe Interaction
    {
        static internal Dictionary<string, object> prefdata;
        static internal bool init = false;

        internal static void WriteKey(string key)
        {
            if (!init)
            {
                if (!Directory.Exists(TeamSceneManager.RootPath + "/Prefs"))
                    Directory.CreateDirectory(TeamSceneManager.RootPath + "/Prefs");

                if (prefdata == null)
                    prefdata = new Dictionary<string, object>();

                init = true;
            }
            File.WriteAllBytes(TeamSceneManager.RootPath+"/Prefs/"+ key+".key", Serializer.TypeToByte<object>(prefdata[key]));
        }

        internal static object LoadKey(string key)
        {
            if(File.Exists(TeamSceneManager.RootPath + "/Prefs/" + key + ".key"))
            {
                using (FileStream fs = new FileStream(TeamSceneManager.RootPath + "/Prefs/" + key + ".key", FileMode.Open))
                    prefdata[key] = Serializer.ToType<object>(fs);
                return prefdata[key];
            }
            else
            {
                prefdata.Add(key, null);
                return null;
            }
        }

        public static void SetData(string key, object data)
        {
            lock (prefdata)
            {
                if (prefdata.ContainsKey(key))
                    prefdata[key] = data;
                else
                {
                    prefdata.Add(key, data);
                }
            }

            WriteKey(key);
        }

        public static object GetData(string key)
        {
            if (!init)
            {
                if (!Directory.Exists(TeamSceneManager.RootPath + "/Prefs"))
                    Directory.CreateDirectory(TeamSceneManager.RootPath + "/Prefs");

                if (prefdata == null)
                    prefdata = new Dictionary<string, object>();
                init = true;
            }

            if (prefdata.ContainsKey(key))
                return prefdata[key];
            else
                return LoadKey(key);
        }

        public static void RemoveKey(string key)
        {
            if (!init)
            {
                if (!Directory.Exists(TeamSceneManager.RootPath + "/Prefs"))
                    Directory.CreateDirectory(TeamSceneManager.RootPath + "/Prefs");

                if (prefdata == null)
                    prefdata = new Dictionary<string, object>();
                init = true;
            }

            if (File.Exists(TeamSceneManager.RootPath + "/Prefs/" + key + ".key"))
                File.Delete(TeamSceneManager.RootPath + "/Prefs/" + key + ".key");

            if (prefdata.ContainsKey(key))
                prefdata.Remove(key);
        }
    }
}
