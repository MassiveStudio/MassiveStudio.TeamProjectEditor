﻿using System;
using System.Collections.Generic;

using UnityEngine;
using System.Reflection;
using UnityEditor;

using MassiveStudio.TeamProjectEditor.EditorWindows.SceneWindow;

namespace MassiveStudio.TeamProjectEditor.Utility
{

    public class GUIContexMenu : IDisposable
    {
        public List<ContexMenuItem> menuItems;

        //public static GUISkin ServerSkin;
        //public static GUISkin ServerSkin2;

        private static GUISkin defaultSkin;
        public bool open = false;


        ~GUIContexMenu()
        {
            _4ViewScene.onSceneGUIDelegate -= DrawOnSceneGUI;
        }

        // Use this for initialization
        public void Init()
        {
            //greenStyle = new GUIStyle("Box");
            //greenStyle.hover. = Color.green;

            //Load MenuItems
            menuItems = new List<ContexMenuItem>();

            menuItems.Add(new ContexMenuItem("File"));
            menuItems[0].subitems.Add(new ContexMenuItem("File 1"));
            //TEXTURE TEST
            menuItems[0].subitems[0].subitems.Add(new ContexMenuItem("File 1_1", EditorGUIUtility.FindTexture("cs Script Icon")));
            menuItems[0].subitems[0].subitems.Add(new ContexMenuItem("File 1_2"));
            menuItems[0].subitems[0].subitems.Add(new ContexMenuItem("File 1_3"));
            menuItems[0].subitems.Add(new ContexMenuItem("File 2"));
            menuItems[0].subitems[1].subitems.Add(new ContexMenuItem("File 2_1"));
            menuItems[0].subitems[1].subitems.Add(new ContexMenuItem("File 2_2"));
            menuItems[0].subitems[1].subitems.Add(new ContexMenuItem("File 2_3"));
            menuItems[0].subitems.Add(new ContexMenuItem("File 3"));
            menuItems[0].subitems.Add(new ContexMenuItem("File 4"));
            menuItems[0].subitems.Add(new ContexMenuItem("File 5"));
            menuItems[0].subitems.Add(new ContexMenuItem("File 6"));

            menuItems.Add(new ContexMenuItem("Edit"));
            menuItems[1].subitems.Add(new ContexMenuItem("Edit 1"));
            menuItems[1].subitems[0].subitems.Add(new ContexMenuItem("Edit 1_1"));
            menuItems[1].subitems[0].subitems.Add(new ContexMenuItem("Edit 1_2"));
            menuItems[1].subitems[0].subitems.Add(new ContexMenuItem("Edit 1_3"));
            menuItems[1].subitems.Add(new ContexMenuItem("Edit 2"));
            menuItems[1].subitems.Add(new ContexMenuItem("Edit 3"));
            menuItems[1].subitems.Add(new ContexMenuItem("Edit 4"));
            menuItems[1].subitems.Add(new ContexMenuItem("Edit 5"));
            menuItems[1].subitems.Add(new ContexMenuItem("Edit 6"));

           //METHOD INVOKE TEST
            menuItems.Add(new ContexMenuItem("Project"));
            menuItems[2].subitems.Add(new ContexMenuItem("Import Settings", "ImportProjectSettings"));
            menuItems[2].subitems.Add(new ContexMenuItem("Export Settings", "ExportProjectSettings"));


        }

        public Event e;
        public Vector2 pos;
        public void ShowContexMenu()
        {
            defaultSkin = GUI.skin;
            //GUI.skin = ServerSkin;
            e = Event.current;
            pos = e.mousePosition + new Vector2(10, 10);

            CloseActiveMenues();
            open = true;
            //GUI.skin = defaultSkin;

            Init();

            _4ViewScene.onSceneGUIDelegate -= DrawOnSceneGUI;
            _4ViewScene.onSceneGUIDelegate += DrawOnSceneGUI;
        }
        public void ShowContexMenu(Vector2 pos)
        {
            defaultSkin = GUI.skin;
            e = Event.current;
            this.pos = pos;
            Init();

            CloseActiveMenues();
            open = true;

            _4ViewScene.onSceneGUIDelegate -= DrawOnSceneGUI;
            _4ViewScene.onSceneGUIDelegate += DrawOnSceneGUI;
        }

        public int selectedMenu = -1;
        bool Menuchanged = false;

        int dir_x = 1;
        int dir_y = 1;

        public Vector2 elementSize = new Vector2(200, 25);

        public void DrawOnSceneGUI(_4ViewScene view)
        {
            Handles.BeginGUI();
            GUI.depth = -1;
            Menuchanged = false;//NEED OVERY FRAME SET TO FALSE
            Rect currentPos = new Rect(pos, elementSize);
            if (menuItems != null)
            {
                GUI.Box(new Rect(currentPos.x - 5, currentPos.y - 5, elementSize.x + 10, (elementSize.y * menuItems.Count) + 10), "");//, ServerSkin.customStyles[1]);
                //GUI.Window(99999999, new Rect(currentPos.x - 5, currentPos.y - 5, elementSize.x + 10, (elementSize.y * menuItems.Count) + 10), DrawWindow, "", GUIStyle.none);

                for (int menucount = 0; menucount < menuItems.Count; menucount++)
                {
                    if (menuItems[menucount].subitems.Count > 0)
                    {
                        GUI.Button(currentPos, menuItems[menucount].title);
                        if (currentPos.Contains(e.mousePosition) || menuItems[menucount].active)
                        {
                            //Debug.Log("CLICKED ON MENU ITEM " + menucount);
                            if (!menuItems[menucount].active)
                            {
                                ClearOtherPaths(menucount);
                                menuItems[menucount].active = true;
                                Menuchanged = true;
                            }
                            menuItems[menucount].Box = new Rect(currentPos.x + elementSize.x, currentPos.y, elementSize.x, menuItems[menucount].subitems.Count * elementSize.y);
                            DrawSubMenuItem(menuItems[menucount], ref Menuchanged);
                        }

                        if (!currentPos.Contains(e.mousePosition) && !Menuchanged)
                        {
                            if (menuItems[menucount].active)
                            {
                                // Debug.LogWarning("NOTHING CHNAGED-> CLOSE");
                                menuItems[menucount].active = false;
                            }
                        }
                    }
                    else
                    {
                        if (GUI.Button(currentPos, menuItems[menucount].title) || menuItems[menucount].active)
                        {
                            //Debug.Log("CLICKED ON MENU ITEM " + menucount);
                            ClearOtherPaths(menucount);
                            if (!menuItems[menucount].active)
                            {
                                menuItems[menucount].active = true;
                                Menuchanged = true;
                            }
                            menuItems[menucount].Box = new Rect(currentPos.x + elementSize.x, currentPos.y, elementSize.x, menuItems[menucount].subitems.Count * elementSize.y);
                            DrawSubMenuItem(menuItems[menucount], ref Menuchanged);
                        }
                    }
                    currentPos.y += elementSize.y;
                }
                if ((e.button == 0 || e.button == 1) && e.type == EventType.MouseDown)
                    if (!Menuchanged)
                    {
                        Debug.Log("CLOSE ALL");
                        CloseActiveMenues();
                        open = false;
                        Dispose();
                    }
            }
            Handles.EndGUI();
        }

        private void CloseActiveMenues()
        {
            //Debug.Log("CLOSE ACTIVE MENU");
            for (int menucount = 0; menucount < menuItems.Count; menucount++)
                menuItems[menucount].Close();
            open = false;
        }

        private void ClearOtherPaths(int path)
        {
            //Debug.Log("CLEAR OTHER PATHS " + path);
            for (int menucount = 0; menucount < menuItems.Count; menucount++)
                if (menucount != path)
                    menuItems[menucount].Close();
        }

        private void ClearOtherPaths(List<ContexMenuItem> items, int path)
        {
            //Debug.Log("CLEAR OTHER PATHS " + path);
            for (int menucount = 0; menucount < items.Count; menucount++)
                if (menucount != path)
                    items[menucount].Close();
        }

        public void DrawSubMenuItem(ContexMenuItem item, ref bool changed)
        {
            //GUI.Window(99999999, item.Box, DrawWindow, "", GUIStyle.none);
            //GUI.Box(item.Box, "");
            Rect currentPos = new Rect(item.Box.position, elementSize);
            for (int i = 0; i < item.subitems.Count; i++)
            {
                if (item.subitems[i].subitems.Count > 0)
                {
                    GUILayout.BeginHorizontal();
                    if (item.subitems[i].texture != null)
                        GUI.Button(new Rect(currentPos.position + new Vector2(50, 0), elementSize - new Vector2(50, 0)), new GUIContent(item.subitems[i].title, item.subitems[i].texture));
                    else
                        GUI.Button(currentPos, item.subitems[i].title);

                    if (currentPos.Contains(e.mousePosition) || item.subitems[i].active)
                    {
                        if (!item.subitems[i].active)
                        {
                            ClearOtherPaths(item.subitems, i);
                            item.subitems[i].active = true;
                        }
                        if (currentPos.Contains(e.mousePosition))
                            changed = true;

                        item.subitems[i].Box = new Rect(currentPos.x + elementSize.x, currentPos.y, elementSize.x, elementSize.y * item.subitems[i].subitems.Count);
                        DrawSubMenuItem(item.subitems[i], ref changed);
                    }

                    if (item.subitems[i].active && !changed)
                        ClearOtherPaths(item.subitems, -1);

                    GUILayout.EndHorizontal();
                }
                else
                {

                    if (currentPos.Contains(e.mousePosition))
                    {
                        ClearOtherPaths(item.subitems, -1);
                        item.subitems[i].active = true;
                        changed = true;
                    }

                    bool button;
                    if (item.subitems[i].texture != null)
                    {
                        GUI.Box(currentPos, "", new GUIStyle("button"));
                        GUI.Box(new Rect(currentPos.position, new Vector2(50, elementSize.y)), item.subitems[i].texture, GUIStyle.none);
                        button = GUI.Button(new Rect(currentPos.position + new Vector2(50, 0), elementSize - new Vector2(50, 0)), item.subitems[i].title, new GUIStyle("label"));
                    }
                    else
                        button = GUI.Button(currentPos, item.subitems[i].title);

                    if (button)//GUI.Button(currentPos, item.subitems[i].title))
                    {
                        changed = true;
                        //MethodToInvoke
                        MethodInfo info = typeof(GUIContexMenu).GetType().GetMethod(item.subitems[i].MethodToInvoke);
                        if (info != null)
                            info.Invoke(null, null);
                        CloseActiveMenues();
                    }
                }
                currentPos.y += elementSize.y;
            }
            // Debug.Log(" CC: " + changed);
        }

        public void DrawWindow(int id)
        {

        }

        public static void ImportProjectSettings()
        {
            Debug.Log("IMPORT");
        }

        public static void ExportProjectSettings()
        {
            Debug.Log("EXPORT");
        }

        public void Dispose()
        {
            _4ViewScene.onSceneGUIDelegate -= DrawOnSceneGUI;
        }

        public void AddItem(string name_path)
        {
            string[] path = name_path.Split("/"[0]);
        }
    }

    [System.Serializable]
    public class ContexMenuItem
    {
        public string title;
        public Texture2D texture;

        public List<ContexMenuItem> subitems;
        public string MethodToInvoke;
        [System.NonSerialized]
        public bool active = false;
        public string condition;
        [System.NonSerialized]
        public Rect Box = new Rect();

        public ContexMenuItem(string _title)
        {
            title = _title;
            subitems = new List<ContexMenuItem>();
            MethodToInvoke = "";
        }

        public ContexMenuItem(string _title, string _method)
        {
            title = _title;
            subitems = new List<ContexMenuItem>();
            MethodToInvoke = _method;
        }

        public ContexMenuItem(string _title, string _method, string _condition)
        {
            title = _title;
            subitems = new List<ContexMenuItem>();
            MethodToInvoke = _method;
            condition = _condition;
        }

        public ContexMenuItem(string _title, Texture2D _texture, string _method = "", string _condition = "")
        {
            title = _title;
            texture = _texture;
            subitems = new List<ContexMenuItem>();
            MethodToInvoke = _method;
            condition = _condition;
        }

        public void Close()
        {
            if (active)
            {
                this.active = false;
                for (int i = 0; i < subitems.Count; i++)
                    subitems[i].Close();
            }
        }

    }
}