﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using UnityEditor;
using UnityEngine;

using MassiveStudio.TeamProjectEditor;

namespace MassiveStudio.TeamProjectEditor.Utility
{
    public static class Dispatcher
    {
        public static void ToMainThread(Action t)
        {
            if (Thread.CurrentThread.ManagedThreadId == TeamSceneManager.MAINTHREADID)
                t();
            else
            {
                //Debug.Log("ToMainThread()");

                //Add To mainthread execution List
                MainThread.actions.Add(new ThreadAction(Thread.CurrentThread, t, 0));
                MainThread.work = true;
               // if (sync)
                //{
                try
                {
                    //Debug.LogWarning("THREAD GO TO ENDLESS SLEEP");
                    Thread.Sleep(Timeout.Infinite);
                }
                catch (ThreadInterruptedException) { }
                finally { }
                //}
            }
        }

        internal static void ToBackground(Action a)
        {
            Thread t = new Thread(()=>DoAction(a));
            t.Start();
        }

        private static void DoAction(Action a)
        {
            Debug.Log("Do Background Stuff from Mainthread");
            a();
            Debug.Log("END doing Background Stuff");

        }

        public static void OnDestroy()
        {

        }
    }

    public class ThreadAction
    {
        public Thread thread;
        public Action action;
        public byte prior;//implementation later

        public ThreadAction(Thread @t, Action @a, byte @prior =0)
        {
            this.thread = t;
            this.action = a;
            this.prior = prior;
        }
    }

    public class MainThread
    {
        public static List<ThreadAction> _actions;
        public static List<ThreadAction> actions
        {
            get
            {
                if (_actions == null)
                    _actions = new List<ThreadAction>();

                return _actions;
            }
            set
            {
                if (_actions == null)
                    _actions = new List<ThreadAction>();

                _actions = value;
            }
        }

        public static bool work = false;

        [SerializeField]
        public GameObject temp;

        public static System.Diagnostics.Stopwatch watch;

        public MainThread()
        {
            actions = new List<ThreadAction> ();
        }

        [UnityEditor.InitializeOnLoadMethod]
        public static void Init_Editor()
        {
            //Debug.Log("IS RUNNING IN EDITOR MODE");
            UnityEditor.EditorApplication.update -= Update;
            UnityEditor.EditorApplication.update += Update;

            if (watch == null)
                watch = new System.Diagnostics.Stopwatch();

        }

        public void Init()
        {
            temp = new GameObject();
            temp.AddComponent<DispatcherManager>().thread = this;
            watch = new System.Diagnostics.Stopwatch();
        }

        public static void Update()
        {
          //  if (watch == null)
          //      watch = new System.Diagnostics.Stopwatch();

            if (work)
            {
                //Debug.Log("DISPATCHER MAIN IS EXECUTING");
                work = false;
                lock (actions)
                {
                    //watch.Start();
                    int c = 0;
                    for(int i =0; i< actions.Count; i++)
                    {
                        //if (watch.ElapsedMilliseconds < 200)
                        {
                            //Debug.Log("DO ACTION " + i + "  Count todo: " + actions.Count);
                            actions[i].action();
                            //Debug.Log("THREAD SLEEP INTERRUPT");
                            actions[i].thread.Interrupt();
                            c++;
                        }
                        //else
                        //    break;
                    }
                    //watch.Stop();

                    if (c == actions.Count)
                        actions.Clear();
                    else
                        actions.RemoveRange(0, c);
                }
            }
        }

        public void OnDestroy()
        {
            UnityEditor.EditorApplication.update -= Update;

            if (temp != null)
                GameObject.Destroy(temp);

            for (int i = 0; i < actions.Count; i++)
                actions[i].thread.Abort();

            actions.Clear();

        }
    }

    public class DispatcherManager : MonoBehaviour
    {
        public MainThread thread;

        public void Start()
        {
            thread = new MainThread();
            thread.Init();
        }

        public void Update()
        {
            if (thread != null)
                MainThread.Update();
        }
    }

    [Obsolete]
    public static class DispatcherTester
    {
        //[UnityEditor.InitializeOnLoadMethod]
        public static void Init()
        {
            Thread t = new Thread(Run);
            t.Start();
        }

        public static void Run()
        {
            Debug.Log("START THREAD TEST");

            Debug.LogError("THE TESTTHREAD THREAD IS ON  " + (TeamAssetSystem.TeamAssetCore.CheckForMainThread() ? "MainThread" : "SUBTHREAD"));


            try
            {
                GameObject a = new GameObject("Test");
            }
            catch (Exception)
            {

            }

            Dispatcher.ToMainThread(() =>
            {
                Debug.LogError("THE TESTTHREAD THREAD IS NOW ON  " + (TeamAssetSystem.TeamAssetCore.CheckForMainThread() ? "MainThread" : "SUBTHREAD"));

                try
                {
                    GameObject a = new GameObject("Test2");
                }
                catch (Exception)
                {

                }
            });

            Debug.Log("Thread Exit");
        }
    }
}
