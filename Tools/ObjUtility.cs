﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;
using UnityEditor.SceneManagement;
using UnityEditor;
using System.Collections.Generic;


namespace MassiveStudio.TeamProjectEditor.Utility
{
    public class ObjUtility
    {
        public class ObjImporter
        {
            private struct meshStruct
            {
                public Vector3[] vertices;
                public Vector3[] normals;
                public Vector2[] uv;
                public Vector2[] uv1;
                public Vector2[] uv2;
                public int[] triangles;
                public int[] faceVerts;
                public int[] faceUVs;
                public Vector3[] faceData;
                public string name;
                public string fileName;
            }

            // Use this for initialization
            public static Mesh ImportFile(Stream stream, string filename)
            {
                meshStruct newMesh = createMeshStruct(stream, filename);
                populateMeshStruct(ref newMesh);

                Vector3[] newVerts = new Vector3[newMesh.faceData.Length];
                Vector2[] newUVs = new Vector2[newMesh.faceData.Length];
                Vector3[] newNormals = new Vector3[newMesh.faceData.Length];
                int i = 0;
                /* The following foreach loops through the facedata and assigns the appropriate vertex, uv, or normal
                 * for the appropriate Unity mesh array.
                 */
                foreach (Vector3 v in newMesh.faceData)
                {
                    newVerts[i] = newMesh.vertices[(int)v.x - 1];
                    if (v.y >= 1)
                        newUVs[i] = newMesh.uv[(int)v.y - 1];

                    if (v.z >= 1)
                        newNormals[i] = newMesh.normals[(int)v.z - 1];
                    i++;
                }

                Mesh mesh = new Mesh();

                mesh.vertices = newVerts;
                mesh.uv = newUVs;
                mesh.normals = newNormals;
                mesh.triangles = newMesh.triangles;

                mesh.RecalculateBounds();
                //mesh.Optimize();

                return mesh;
            }

            private static meshStruct createMeshStruct(Stream filestream, string filename)
            {
                int triangles = 0;
                int vertices = 0;
                int vt = 0;
                int vn = 0;
                int face = 0;
                meshStruct mesh = new meshStruct();
                mesh.fileName = filename;
                StreamReader stream = new StreamReader(filestream);
                string entireText = stream.ReadToEnd();
                stream.Close();
                using (StringReader reader = new StringReader(entireText))
                {
                    string currentText = reader.ReadLine();
                    char[] splitIdentifier = { ' ' };
                    string[] brokenString;
                    while (currentText != null)
                    {
                        if (!currentText.StartsWith("f ") && !currentText.StartsWith("v ") && !currentText.StartsWith("vt ")
                            && !currentText.StartsWith("vn "))
                        {
                            currentText = reader.ReadLine();
                            if (currentText != null)
                            {
                                currentText = currentText.Replace("  ", " ");
                            }
                        }
                        else
                        {
                            currentText = currentText.Trim();                           //Trim the current line
                            brokenString = currentText.Split(splitIdentifier, 50);      //Split the line into an array, separating the original line by blank spaces
                            switch (brokenString[0])
                            {
                                case "v":
                                    vertices++;
                                    break;
                                case "vt":
                                    vt++;
                                    break;
                                case "vn":
                                    vn++;
                                    break;
                                case "f":
                                    face = face + brokenString.Length - 1;
                                    triangles = triangles + 3 * (brokenString.Length - 2); /*brokenString.Length is 3 or greater since a face must have at least
                                                                                     3 vertices.  For each additional vertice, there is an additional
                                                                                     triangle in the mesh (hence this formula).*/
                                    break;
                            }
                            currentText = reader.ReadLine();
                            if (currentText != null)
                            {
                                currentText = currentText.Replace("  ", " ");
                            }
                        }
                    }
                }
                mesh.triangles = new int[triangles];
                mesh.vertices = new Vector3[vertices];
                mesh.uv = new Vector2[vt];
                mesh.normals = new Vector3[vn];
                mesh.faceData = new Vector3[face];
                return mesh;
            }

            private static void populateMeshStruct(ref meshStruct mesh)
            {
                StreamReader stream = File.OpenText(mesh.fileName);
                string entireText = stream.ReadToEnd();
                stream.Close();
                using (StringReader reader = new StringReader(entireText))
                {
                    string currentText = reader.ReadLine();

                    char[] splitIdentifier = { ' ' };
                    char[] splitIdentifier2 = { '/' };
                    string[] brokenString;
                    string[] brokenBrokenString;
                    int f = 0;
                    int f2 = 0;
                    int v = 0;
                    int vn = 0;
                    int vt = 0;
                    int vt1 = 0;
                    int vt2 = 0;
                    while (currentText != null)
                    {
                        if (!currentText.StartsWith("f ") && !currentText.StartsWith("v ") && !currentText.StartsWith("vt ") &&
                            !currentText.StartsWith("vn ") && !currentText.StartsWith("g ") && !currentText.StartsWith("usemtl ") &&
                            !currentText.StartsWith("mtllib ") && !currentText.StartsWith("vt1 ") && !currentText.StartsWith("vt2 ") &&
                            !currentText.StartsWith("vc ") && !currentText.StartsWith("usemap "))
                        {
                            currentText = reader.ReadLine();
                            if (currentText != null)
                            {
                                currentText = currentText.Replace("  ", " ");
                            }
                        }
                        else
                        {
                            currentText = currentText.Trim();
                            brokenString = currentText.Split(splitIdentifier, 50);
                            switch (brokenString[0])
                            {
                                case "g":
                                    break;
                                case "usemtl":
                                    break;
                                case "usemap":
                                    break;
                                case "mtllib":
                                    break;
                                case "v":
                                    mesh.vertices[v] = new Vector3(System.Convert.ToSingle(brokenString[1]), System.Convert.ToSingle(brokenString[2]),
                                                             System.Convert.ToSingle(brokenString[3]));
                                    v++;
                                    break;
                                case "vt":
                                    mesh.uv[vt] = new Vector2(System.Convert.ToSingle(brokenString[1]), System.Convert.ToSingle(brokenString[2]));
                                    vt++;
                                    break;
                                case "vt1":
                                    mesh.uv[vt1] = new Vector2(System.Convert.ToSingle(brokenString[1]), System.Convert.ToSingle(brokenString[2]));
                                    vt1++;
                                    break;
                                case "vt2":
                                    mesh.uv[vt2] = new Vector2(System.Convert.ToSingle(brokenString[1]), System.Convert.ToSingle(brokenString[2]));
                                    vt2++;
                                    break;
                                case "vn":
                                    mesh.normals[vn] = new Vector3(System.Convert.ToSingle(brokenString[1]), System.Convert.ToSingle(brokenString[2]),
                                                            System.Convert.ToSingle(brokenString[3]));
                                    vn++;
                                    break;
                                case "vc":
                                    break;
                                case "f":

                                    int j = 1;
                                    List<int> intArray = new List<int>();
                                    while (j < brokenString.Length && ("" + brokenString[j]).Length > 0)
                                    {
                                        Vector3 temp = new Vector3();
                                        brokenBrokenString = brokenString[j].Split(splitIdentifier2, 3);    //Separate the face into individual components (vert, uv, normal)
                                        temp.x = System.Convert.ToInt32(brokenBrokenString[0]);
                                        if (brokenBrokenString.Length > 1)                                  //Some .obj files skip UV and normal
                                        {
                                            if (brokenBrokenString[1] != "")                                    //Some .obj files skip the uv and not the normal
                                            {
                                                temp.y = System.Convert.ToInt32(brokenBrokenString[1]);
                                            }
                                            temp.z = System.Convert.ToInt32(brokenBrokenString[2]);
                                        }
                                        j++;

                                        mesh.faceData[f2] = temp;
                                        intArray.Add(f2);
                                        f2++;
                                    }
                                    j = 1;
                                    while (j + 2 < brokenString.Length)     //Create triangles out of the face data.  There will generally be more than 1 triangle per face.
                                    {
                                        mesh.triangles[f] = intArray[0];
                                        f++;
                                        mesh.triangles[f] = intArray[j];
                                        f++;
                                        mesh.triangles[f] = intArray[j + 1];
                                        f++;

                                        j++;
                                    }
                                    break;
                            }
                            currentText = reader.ReadLine();
                            if (currentText != null)
                            {
                                currentText = currentText.Replace("  ", " ");       //Some .obj files insert double spaces, this removes them.
                            }
                        }
                    }
                }
            }
        }

        public class ObjExporter
        {
            public static string MeshToString(MeshFilter mf)
            {
                Mesh m = mf.mesh;
                Material[] mats = mf.GetComponent<MeshRenderer>().sharedMaterials;

                StringBuilder sb = new StringBuilder();

                sb.Append("g ").Append(mf.name).Append("\n");
                foreach (Vector3 v in m.vertices)
                {
                    sb.Append(string.Format("v {0} {1} {2}\n", v.x, v.y, v.z));
                }
                sb.Append("\n");
                foreach (Vector3 v in m.normals)
                {
                    sb.Append(string.Format("vn {0} {1} {2}\n", v.x, v.y, v.z));
                }
                sb.Append("\n");
                foreach (Vector3 v in m.uv)
                {
                    sb.Append(string.Format("vt {0} {1}\n", v.x, v.y));
                }
                for (int material = 0; material < m.subMeshCount; material++)
                {
                    sb.Append("\n");
                    sb.Append("usemtl ").Append(mats[material].name).Append("\n");
                    sb.Append("usemap ").Append(mats[material].name).Append("\n");

                    int[] triangles = m.GetTriangles(material);
                    for (int i = 0; i < triangles.Length; i += 3)
                    {
                        sb.Append(string.Format("f {0}/{0}/{0} {1}/{1}/{1} {2}/{2}/{2}\n",
                            triangles[i] + 1, triangles[i + 1] + 1, triangles[i + 2] + 1));
                    }
                }
                return sb.ToString();
            }

            public static void MeshToFile(MeshFilter mf, string filename)
            {
                using (StreamWriter sw = new StreamWriter(filename))
                {
                    sw.Write(MeshToString(mf));
                }
            }
        }

        struct ObjMaterial
        {
            public string name;
            public string textureName;
        }

        public class EditorObjExporter : ScriptableObject
        {
            private static int vertexOffset = 0;
            private static int normalOffset = 0;
            private static int uvOffset = 0;

            //User should probably be able to change this. It is currently left as an excercise for
            //the reader.
            private static string targetFolder = "ExportedObj";

            private static string MeshToString(MeshFilter mf, Dictionary<string, ObjMaterial> materialList)
            {
                Mesh m = mf.sharedMesh;
                Material[] mats = mf.GetComponent<Renderer>().sharedMaterials;

                StringBuilder sb = new StringBuilder();

                sb.Append("g ").Append(mf.name).Append("\n");
                foreach (Vector3 lv in m.vertices)
                {
                    Vector3 wv = mf.transform.TransformPoint(lv);

                    //This is sort of ugly - inverting x-component since we're in
                    //a different coordinate system than "everyone" is "used to".
                    sb.Append(string.Format("v {0} {1} {2}\n", -wv.x, wv.y, wv.z));
                }
                sb.Append("\n");

                foreach (Vector3 lv in m.normals)
                {
                    Vector3 wv = mf.transform.TransformDirection(lv);

                    sb.Append(string.Format("vn {0} {1} {2}\n", -wv.x, wv.y, wv.z));
                }
                sb.Append("\n");

                foreach (Vector3 v in m.uv)
                {
                    sb.Append(string.Format("vt {0} {1}\n", v.x, v.y));
                }

                for (int material = 0; material < m.subMeshCount; material++)
                {
                    sb.Append("\n");
                    sb.Append("usemtl ").Append(mats[material].name).Append("\n");
                    sb.Append("usemap ").Append(mats[material].name).Append("\n");

                    //See if this material is already in the materiallist.
                    try
                    {
                        ObjMaterial objMaterial = new ObjMaterial();

                        objMaterial.name = mats[material].name;

                        if (mats[material].mainTexture)
                            objMaterial.textureName = AssetDatabase.GetAssetPath(mats[material].mainTexture);
                        else
                            objMaterial.textureName = null;

                        materialList.Add(objMaterial.name, objMaterial);
                    }
                    catch (System.ArgumentException)
                    {
                        //Already in the dictionary
                    }


                    int[] triangles = m.GetTriangles(material);
                    for (int i = 0; i < triangles.Length; i += 3)
                    {
                        //Because we inverted the x-component, we also needed to alter the triangle winding.
                        sb.Append(string.Format("f {1}/{1}/{1} {0}/{0}/{0} {2}/{2}/{2}\n",
                            triangles[i] + 1 + vertexOffset, triangles[i + 1] + 1 + normalOffset, triangles[i + 2] + 1 + uvOffset));
                    }
                }

                vertexOffset += m.vertices.Length;
                normalOffset += m.normals.Length;
                uvOffset += m.uv.Length;

                return sb.ToString();
            }

            private static void Clear()
            {
                vertexOffset = 0;
                normalOffset = 0;
                uvOffset = 0;
            }

            private static Dictionary<string, ObjMaterial> PrepareFileWrite()
            {
                Clear();

                return new Dictionary<string, ObjMaterial>();
            }

            private static void MaterialsToFile(Dictionary<string, ObjMaterial> materialList, string folder, string filename)
            {
                using (StreamWriter sw = new StreamWriter(folder + Path.PathSeparator + filename + ".mtl"))
                {
                    foreach (KeyValuePair<string, ObjMaterial> kvp in materialList)
                    {
                        sw.Write("\n");
                        sw.Write("newmtl {0}\n", kvp.Key);
                        sw.Write("Ka  0.6 0.6 0.6\n");
                        sw.Write("Kd  0.6 0.6 0.6\n");
                        sw.Write("Ks  0.9 0.9 0.9\n");
                        sw.Write("d  1.0\n");
                        sw.Write("Ns  0.0\n");
                        sw.Write("illum 2\n");

                        if (kvp.Value.textureName != null)
                        {
                            string destinationFile = kvp.Value.textureName;


                            int stripIndex = destinationFile.LastIndexOf(Path.PathSeparator);

                            if (stripIndex >= 0)
                                destinationFile = destinationFile.Substring(stripIndex + 1).Trim();


                            string relativeFile = destinationFile;

                            destinationFile = folder + Path.PathSeparator + destinationFile;

                            Debug.Log("Copying texture from " + kvp.Value.textureName + " to " + destinationFile);

                            try
                            {
                                //Copy the source file
                                File.Copy(kvp.Value.textureName, destinationFile);
                            }
                            catch
                            {

                            }


                            sw.Write("map_Kd {0}", relativeFile);
                        }

                        sw.Write("\n\n\n");
                    }
                }
            }

            private static void MeshToFile(MeshFilter mf, string folder, string filename)
            {
                Dictionary<string, ObjMaterial> materialList = PrepareFileWrite();

                using (StreamWriter sw = new StreamWriter(folder + Path.PathSeparator + filename + ".obj"))
                {
                    sw.Write("mtllib ./" + filename + ".mtl\n");

                    sw.Write(MeshToString(mf, materialList));
                }

                MaterialsToFile(materialList, folder, filename);
            }

            private static void MeshesToFile(MeshFilter[] mf, string folder, string filename)
            {
                Dictionary<string, ObjMaterial> materialList = PrepareFileWrite();

                if (!Directory.Exists(Application.dataPath + "/" + folder + "/"))
                    Directory.CreateDirectory(Application.dataPath + "/" + folder + "/");

                Debug.Log("SAVED ON " + Application.dataPath + "/" + folder + "/" + filename + ".obj");

                FileStream stream = new FileStream(Application.dataPath + "/" + folder + "/" + filename + ".obj", FileMode.OpenOrCreate);
                using (StreamWriter sw = new StreamWriter(stream))
                {
                    sw.Write("mtllib ./" + filename + ".mtl\n");

                    for (int i = 0; i < mf.Length; i++)
                    {
                        sw.Write(MeshToString(mf[i], materialList));
                    }
                    sw.Close();
                }
                stream.Close();

                MaterialsToFile(materialList, folder, filename);
            }

            private static bool CreateTargetFolder()
            {
                try
                {
                    System.IO.Directory.CreateDirectory(targetFolder);
                }
                catch
                {
                    EditorUtility.DisplayDialog("Error!", "Failed to create target folder!", "");
                    return false;
                }

                return true;
            }

            [MenuItem("Custom/Export/Export all MeshFilters in selection to separate OBJs")]
            static void ExportSelectionToSeparate()
            {
                if (!CreateTargetFolder())
                    return;

                Transform[] selection = Selection.GetTransforms(SelectionMode.Editable | SelectionMode.ExcludePrefab);

                if (selection.Length == 0)
                {
                    EditorUtility.DisplayDialog("No source object selected!", "Please select one or more target objects", "");
                    return;
                }

                int exportedObjects = 0;

                for (int i = 0; i < selection.Length; i++)
                {
                    Component[] meshfilter = selection[i].GetComponentsInChildren(typeof(MeshFilter));

                    for (int m = 0; m < meshfilter.Length; m++)
                    {
                        exportedObjects++;
                        MeshToFile((MeshFilter)meshfilter[m], targetFolder, selection[i].name + "_" + i + "_" + m);
                    }
                }

                if (exportedObjects > 0)
                    EditorUtility.DisplayDialog("Objects exported", "Exported " + exportedObjects + " objects", "");
                else
                    EditorUtility.DisplayDialog("Objects not exported", "Make sure at least some of your selected objects have mesh filters!", "");
            }

            [MenuItem("Custom/Export/Export whole selection to single OBJ")]
            static void ExportWholeSelectionToSingle()
            {
                if (!CreateTargetFolder())
                    return;


                Transform[] selection = Selection.GetTransforms(SelectionMode.Editable | SelectionMode.ExcludePrefab);

                if (selection.Length == 0)
                {
                    EditorUtility.DisplayDialog("No source object selected!", "Please select one or more target objects", "");
                    return;
                }

                int exportedObjects = 0;

                ArrayList mfList = new ArrayList();

                for (int i = 0; i < selection.Length; i++)
                {
                    Component[] meshfilter = selection[i].GetComponentsInChildren(typeof(MeshFilter));

                    for (int m = 0; m < meshfilter.Length; m++)
                    {
                        exportedObjects++;
                        mfList.Add(meshfilter[m]);
                    }
                }

                if (exportedObjects > 0)
                {
                    MeshFilter[] mf = new MeshFilter[mfList.Count];

                    for (int i = 0; i < mfList.Count; i++)
                    {
                        mf[i] = (MeshFilter)mfList[i];
                    }

                    string filename = EditorSceneManager.GetActiveScene().name + "_" + exportedObjects;

                    int stripIndex = filename.LastIndexOf(Path.PathSeparator);

                    if (stripIndex >= 0)
                        filename = filename.Substring(stripIndex + 1).Trim();

                    MeshesToFile(mf, targetFolder, filename);


                    EditorUtility.DisplayDialog("Objects exported", "Exported " + exportedObjects + " objects to " + filename, "");
                }
                else
                    EditorUtility.DisplayDialog("Objects not exported", "Make sure at least some of your selected objects have mesh filters!", "");
            }

            [MenuItem("Custom/Export/Export each selected to single OBJ")]
            public static void ExportEachSelectionToSingle()
            {
                ExportEachSelectionToSingle(Application.dataPath + "/" + targetFolder + "/");
            }

            public static void ExportEachSelectionToSingle(string path)
            {
                if (!CreateTargetFolder())
                    return;

                Transform[] selection = Selection.GetTransforms(SelectionMode.Editable | SelectionMode.ExcludePrefab);

                if (selection.Length == 0)
                {
                    EditorUtility.DisplayDialog("No source object selected!", "Please select one or more target objects", "");
                    return;
                }

                int exportedObjects = 0;


                for (int i = 0; i < selection.Length; i++)
                {
                    Component[] meshfilter = selection[i].GetComponentsInChildren(typeof(MeshFilter));

                    MeshFilter[] mf = new MeshFilter[meshfilter.Length];

                    for (int m = 0; m < meshfilter.Length; m++)
                    {
                        exportedObjects++;
                        mf[m] = (MeshFilter)meshfilter[m];
                    }

                    MeshesToFile(mf, path, selection[i].name + "_" + i);
                }

                if (exportedObjects > 0)
                {
                    EditorUtility.DisplayDialog("Objects exported", "Exported " + exportedObjects + " objects", "");
                }
                else
                    EditorUtility.DisplayDialog("Objects not exported", "Make sure at least some of your selected objects have mesh filters!", "");
            }

        }
    }
}
