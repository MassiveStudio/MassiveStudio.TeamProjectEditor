﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MassiveStudio.TeamProjectEditor.Tools
{
    public class Extensions
    {
        public class String
        {
            public string ToLower(string s)
            {
                char[] ns = new char[s.Length];
                for (int i = 0; i < s.Length; i++)
                    ns[i] = char.ToLower(s[i]);
                return ns.ToString();
            }

            public string ToUpper(string s)
            {
                char[] ns = new char[s.Length];
                for (int i = 0; i < s.Length; i++)
                    ns[i] = char.ToUpper(s[i]);
                return ns.ToString();
            }
        }
    }
}
