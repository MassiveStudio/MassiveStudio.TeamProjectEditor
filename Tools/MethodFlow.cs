﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Reflection;
using UnityEditor;
using UnityEngine;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace MassiveStudio.TeamProjectEditor.Tools
{
    public class MethodFlow
    {
        public static AssemblyDefinition assembly;

        Mono.Cecil.Cil.MethodBody body;

        public static List<MethodInfo> GetCallingMethods(MethodInfo startmethod)
        {
            List<MethodInfo> found_methods = new List<MethodInfo>();

            Debug.Log("ASSE: " + startmethod.Module.Assembly.FullName);

            string path = startmethod.Module.Assembly.Location;
            assembly = AssemblyDefinition.ReadAssembly(path);


            Debug.LogWarning("ASSEMBLY " + path+ "   "+ (assembly==null?"NULL":"NOTNULL"));


            Debug.Log("SEARCH MODULE - " + startmethod.Module.Name + "  " + startmethod.GetType().ToString());


            /*foreach (var a in assembly.Modules)
            {
                if (a.Name == startmethod.Module.Name)
                {
                    Debug.Log("R    MODUL FOUND: " + a.Name);
                    break;
                }
                Debug.Log("MODULE: " + a.Name+"   /    " + startmethod.Module.Name);
            }*/


            ModuleDefinition mod = assembly.Modules.First(x => x.Name == startmethod.Module.Name);
            Debug.Assert(mod != null, "MODULE NOT FOUND");
            if(mod!=null)
                Debug.Log("MODULE FOUND " + mod.Name);


            Debug.Log("TYPE " + startmethod.DeclaringType);


            /*foreach (var a in mod.Types)
            {
                if (a.Name == startmethod.DeclaringType.Name)
                    Debug.LogWarning("TYPE: " + a.Name + "   /    " + startmethod.DeclaringType.Name);
                else
                    Debug.Log("TYPE: " + a.Name + "   /    " + startmethod.DeclaringType.Name);
            }*/

            TypeDefinition typ = mod.Types.First(y => y.Name == startmethod.DeclaringType.Name);

            Debug.Assert(typ != null, "TYPE NOT FOUND");
            if (typ != null)
                Debug.Log("TYPE FOUND " + typ.Name);


            Debug.Log("METHOD " + startmethod.Name);
            /*
            foreach (var a in typ.Methods)
            {
                //if (a.Name == startmethod.DeclaringType.Name)
                //    Debug.LogWarning("TYPE: " + a.Name + "   /    " + startmethod.DeclaringType.Name);
                //else
                    Debug.Log("METHOD: " + a.Name + "   /    " + startmethod.Name);
            }*/

            MethodDefinition met = typ.Methods.First(z => z.Name == startmethod.Name);
            Debug.Assert(met != null, "METHOD NOT FOUND");


            ILProcessor worker = met.Body.GetILProcessor();
            foreach (var a in worker.Body.Instructions)
            {
                if (a.OpCode == OpCodes.Call || a.OpCode == OpCodes.Callvirt)
                {
                    string[] data = a.ToString().Split(' ');

                    Debug.Log("CALL " + a.ToString());
                    Debug.Log("\tPos: " + data[1]+"\t Return: " +data[2]+"\t Method: "+ data[3] );

                    if (data[3].Contains("::get_") || data[3].Contains("::set_"))//GET/SET
                        continue;


                    int pos = data[3].IndexOf("::");
                    string type = data[3].Substring(0,pos);
                    string method = data[3].Substring(pos+2);

                    //method = method.Substring(0,method.IndexOf("("));

                    Debug.Log("T: " + type + "  /  " + method);

                    Type methodtype = ReflectDllMethods.GetType(type);
                    MethodInfo methodinfo = methodtype.GetMethod(method);

                    found_methods.Add(methodinfo);
                }
            }
            /*foreach (TypeDefinition type in assembly.MainModule.Types)
            {
                if (type.Name == "ClassB")
                {
                    //TypeReference returntype = assembly.MainModule.Import(typeof(void));
                    MethodDefinition met = new MethodDefinition("Test",
                        Mono.Cecil.MethodAttributes.Private | Mono.Cecil.MethodAttributes.Static,
                        assembly.MainModule.TypeSystem.Void);
                    type.Methods.Add(met);


                    Instruction msg = worker.Create(OpCodes.Ldstr, "Hello!");
                    MethodReference writeline = assembly.MainModule.Import(typeof(Console).GetMethod("WriteLine", new Type[] { typeof(string) }));
                    met.Body.Instructions.Insert(0, msg);
                    met.Body.Instructions.Insert(1, Instruction.Create(OpCodes.Call, writeline));
                }
            }*/
            return found_methods;
        }
    }

    public class FlowChart
    {
        public List<FlowChartNode> nodes;


        public FlowChart()
        {
            nodes = new List<FlowChartNode>();
        }
    }

    public class FlowChartNode
    {
        public string dll;
        public string @namespace;
        public string method;
        public string returnType;

        public MethodInfo method_info;

        public List<FlowChartNode> connections;

        public FlowChartNode(MethodInfo info)
        {
            connections = new List<FlowChartNode>();
            method_info = info;

            method = info.Name;
            returnType = info.ReturnType.ToString();
        }
    }

    public class FlowChartViewer : EditorWindow
    {
        public FlowChart chart;
        private Dictionary<MethodInfo, FlowChartNode> temp;


        public static FlowChartViewer window;

        public static void Init()
        {
            window = GetWindow<FlowChartViewer>();
            window.Show();
        }

        public void OnEnable()
        {
            if (chart == null)
            {
                GenerateChart("TeamProjectEditor.TeamSceneManager", "Register(TeamView view)");
            }
        }

        public void OnGUI()
        {
            if (chart == null)
            {
                Vector2 pos= Vector2.zero;
                Vector2 siz = new Vector2(100, 25);
                foreach(FlowChartNode a in chart.nodes)
                {
                    GUI.Box(new Rect(pos, siz), a.method);
                }
            }
        }

        public void GenerateChart(string type, string method)
        {
            chart = new FlowChart();
            temp = new Dictionary<MethodInfo, FlowChartNode>();//Clear

            MethodInfo info = ReflectDllMethods.GetType(type).GetMethod(method);
            List<MethodInfo> methods = MethodFlow.GetCallingMethods(info);

            FlowChartNode tempNode = new FlowChartNode(info);
            chart.nodes.Add(tempNode);
            temp.Add(info, tempNode);

            FlowChartNode ParentNode = tempNode;//StartNode
            while (methods.Count > 0)
            {
                int numOfNodesToCompile = methods.Count;
                for (int i = 0; i < numOfNodesToCompile; i++)
                {
                    ParentNode = temp[methods[i]];

                    //Get New Connections
                    List<MethodInfo> methods2 = MethodFlow.GetCallingMethods(methods[i]);

                    for (int x = 0; x < methods2.Count; x++)
                    {
                        if (!methods2[x].Module.Name.Contains("TeamProjectEditor"))//No future connection
                        { 
                            if (!temp.ContainsKey(methods2[x]))
                            {
                                methods2.RemoveRange(x, 1);
                                x--;
                            }
                            else
                            {
                                tempNode = new FlowChartNode(methods2[x]);
                                ParentNode.connections.Add(tempNode);

                                methods.Add(methods2[x]);
                            }
                        }
                        else
                        {
                            methods2.RemoveRange(x, 1);
                            x--;
                        }
                    }
                }
                //Remove old ones
                methods.RemoveRange(0, numOfNodesToCompile);
            }
            temp.Clear();
        }
    }
}